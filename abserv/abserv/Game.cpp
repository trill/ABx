/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Game.h"
#include "Application.h"
#include "AreaOfEffect.h"
#include "ConfigManager.h"
#include "Group.h"
#include "DataProvider.h"
#include "Effect.h"
#include "EffectsComp.h"
#include "EffectManager.h"
#include "GameManager.h"
#include "IOGame.h"
#include "IOMap.h"
#include "ItemDrop.h"
#include "ItemFactory.h"
#include "MessageFilter.h"
#include "Npc.h"
#include "Player.h"
#include "PlayerManager.h"
#include "Projectile.h"
#include "ProtocolGame.h"
#include "Script.h"
#include "Skill.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <AB/ProtocolCodes.h>
#include <libcommon/MessageClient.h>
#include <libcommon/Random.h>
#include <libcommon/ThreadPool.h>
#include <libcommon/Scheduler.h>
#include <libcommon/Profiler.h>
#include <sa/Assert.h>
#include <sa/EAIterator.h>
#include <sa/Container.h>
#include <AB/Entities/GameInstanceList.h>

//#define DEBUG_GAME

namespace Game {

Game::Game() :
    lua_(*this),
    gameState_(*this)
{
    instanceData_.uuid = Utils::Uuid::New();
}

Game::~Game()
{
    instanceData_.running = false;
    instanceData_.stopTime = sa::time::tick();
    UpdateEntity(instanceData_);
    auto* client = GetSubsystem<IO::DataClient>();
    client->Invalidate(instanceData_);
    AB::Entities::GameInstanceList il;
    client->Invalidate(il);
    players_.clear();
    objects_.clear();
    GetSubsystem<Chat>()->Remove(ChatType::Map, id_);
}

GameObject* Game::_LuaGetObjectById(uint32_t objectId) const
{
    return GetObject<GameObject>(objectId);
}

Actor* Game::_LuaGetActorById(uint32_t objectId) const
{
    return GetObject<Actor>(objectId);
}

Npc* Game::_LuaAddNpc(const std::string& script)
{
    auto o = AddNpc(script);
    if (o)
        return o.get();
    return nullptr;
}

Npc* Game::_LuaAddSlave(const std::string& script, Actor* master, Actor* corpse, SlaveKind kind, int level)
{
    auto o = AddSlave(script, (master ? master->GetPtr<Actor>() : ea::shared_ptr<Actor>()),
        (corpse ? corpse->GetPtr<Actor>() : ea::shared_ptr<Actor>()), kind, level);
    if (o)
        return o.get();
    return nullptr;
}

AreaOfEffect* Game::_LuaAddAreaOfEffect(const std::string& script,
    Actor* source, uint32_t index,
    const Math::StdVector3& pos)
{
    auto o = AddAreaOfEffect(script,
        source ? source->GetPtr<Actor>() : ea::shared_ptr<Actor>(),
        index, pos);
    if (o)
        return o.get();
    return nullptr;
}

void Game::_LuaAddProjectile(const std::string& itemUuid,
    Actor* source,
    Actor* target)
{
    ASSERT(source);
    ASSERT(target);
    AddProjectile(itemUuid, source->GetPtr<Actor>(), target->GetPtr<Actor>());
}

ItemDrop* Game::_LuaAddItemDrop(Actor* dropper)
{
    auto o = AddRandomItemDrop(dropper);
    if (o)
        return o.get();
    return nullptr;
}

void Game::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Game"].setClass(std::move(kaguya::UserdataMetatable<Game>()
        .addFunction("GetDefaultLevel", &Game::GetDefaultLevel)
        // Get any game object by ID
        .addFunction("GetObject", &Game::_LuaGetObjectById)
        // Get an Actor, if it does not exist or is not an Actor, returns nullptr
        .addFunction("GetActor", &Game::_LuaGetActorById)
        // Get player of game by ID or name
        .addFunction("GetPlayer", &Game::GetPlayerById)
        .addFunction("GetParties", &Game::_LuaGetParties)
        .addFunction("GetGroups", &Game::_LuaGetGroups)
        .addFunction("GetGroup", &Game::GetGroup)
        .addFunction("AddGroup", &Game::AddGroup)

        .addFunction("GetTerrainHeight", &Game::_LuaGetTerrainHeight)
        .addFunction("GetStartTime", &Game::_LuaGetStartTime)
        .addFunction("GetInstanceTime", &Game::GetInstanceTime)

        .addFunction("AddNpc", &Game::_LuaAddNpc)
        // Add a minion or a spirit. Usually Actor::SpawnSlave() should be called,
        // but we keep it to be able to add master less minions.
        .addFunction("AddSlave", &Game::_LuaAddSlave)
        .addFunction("AddAreaOfEffect", &Game::_LuaAddAreaOfEffect)
        .addFunction("AddProjectile", &Game::_LuaAddProjectile)
        .addFunction("AddRandomItemDrop", &Game::_LuaAddItemDrop)

        .addProperty("Id", &Game::_LuaGetGameId)
        .addProperty("Name", &Game::GetName)
        .addProperty("Type", &Game::_LuaGetType)
    ));
    // clang-format on
}

void Game::Start()
{
    // Game, load thread
    if (state_ != ExecutionState::Startup)
    {
#ifdef DEBUG_GAME
        LOG_DEBUG << "Game state is not Startup it is " << static_cast<int>(state_.load()) << std::endl;
#endif // DEBUG_GAME
        return;
    }

    auto* config = GetSubsystem<ConfigManager>();
    startTime_ = sa::time::tick();
    instanceData_.startTime = startTime_;
    instanceData_.serverUuid = Application::Instance->GetServerId();
    instanceData_.gameUuid = data_.uuid;
    instanceData_.name = map_->name_;
    LOG_INFO << "Starting game " << id_ << ", " << map_->name_ << std::endl;

    if ((*config)[ConfigManager::Key::RecordGames])
    {
        gameState_.SetRecordingDir((*config)[ConfigManager::Key::RecordingsDir]);
        instanceData_.recording = gameState_.GetRecordingFilename();
    }
    instanceData_.running = true;
    CreateEntity(instanceData_);

    auto* client = GetSubsystem<IO::DataClient>();
    client->Invalidate(instanceData_);

    AB::Entities::GameInstanceList il;
    client->Invalidate(il);

    lastUpdate_ = 0;
    SetState(ExecutionState::Running);

    // Initial game update
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(&Game::Update, shared_from_this()))
    );
}

void Game::Update()
{
    const uint32_t frequency = GetUpdateFrequency();
    // Dispatcher Thread
    if (state_ != ExecutionState::Terminated)
    {
        int64_t tick = sa::time::tick();
        if (firstUpdate_)
        {
            lua_.Call("onStart");

            // Add start tick at the beginning
            AB::Packets::Server::GameStart startPacket = { startTime_ };
            gameState_.AddPacket(AB::GameProtocol::ServerPacketType::GameStart, startPacket);
            noplayerTime_ = 0;
            firstUpdate_ = false;
        }
        // Now that we are running we can spawn the queued players
        if (!queuedObjects_.empty())
        {
#ifdef DEBUG_GAME
            LOG_DEBUG << queuedObjects_.size() << " queued objects" << std::endl;
#endif
            size_t sentCount = 0;
            auto it = queuedObjects_.begin();
            while (it != queuedObjects_.end())
            {
                if (!Is<ItemDrop>(*(*it))) [[likely]]
                    SpawnObject((*it));
                else [[unlikely]]
                    // Item drop needs some additional information
                    SpawnItemDrop((*it)->GetPtr<ItemDrop>());
                it = queuedObjects_.erase(it);
                ++sentCount;
                // Can not send too much at once
                if (sentCount > 5)
                    break;
            }
        }

        uint32_t delta = (lastUpdate_ != 0) ? static_cast<uint32_t>(tick - lastUpdate_) : frequency;

        // Add timestamp
        AB::Packets::Server::GameUpdate updatePacket = { tick };
        gameState_.AddPacket(AB::GameProtocol::ServerPacketType::GameUpdate, updatePacket);

        // First Update all objects
        for (const auto& o : objects_)
        {

            if (!o.second)
                return;
            if (o.second->HasGame())
                o.second->Update(delta, gameState_);
        }

        // Update Octree
        map_->UpdateOctree(delta);

        // Then call Lua Update function
        lua_.Call("onUpdate", delta);

        // Send game status to players
        SendStatus();

        if (GetPlayerCount() == 0)
            noplayerTime_ += delta;
        else
            noplayerTime_ = 0;

        if (!removedObjects_.empty())
        {
            for (auto rmId : removedObjects_)
                objects_.erase(rmId);
            removedObjects_.clear();
        }
        lastUpdate_ = tick;
    }

    switch (state_)
    {
    case ExecutionState::Running:
    case ExecutionState::Shutdown:
    {
        if (state_ == ExecutionState::Shutdown && IsInactive())
        {
            // If all players left the game, delete it. Actually just mark as
            // terminated, it'll be deleted in the next update.
            // Keep empty games for 10 seconds
            LOG_INFO << "Shutting down game " << id_ << ", " << map_->name_ << " no players for " << noplayerTime_ << std::endl;
            SetState(ExecutionState::Terminated);
            lua_.Call("onStop");
        }

        // Schedule next update
        const int64_t end = sa::time::tick();
        const uint32_t duration = static_cast<uint32_t>(end - lastUpdate_);
        const uint32_t sleepTime = frequency > duration ?
            frequency - duration :
            0;
        GetSubsystem<Asynch::Scheduler>()->Add(
            Asynch::CreateScheduledTask(sleepTime, std::bind(&Game::Update, shared_from_this()))
        );

        break;
    }
    case ExecutionState::Terminated:
        // Delete this game
        LOG_INFO << "Stopping game " << id_ << ", " << map_->name_ << std::endl;
        GetSubsystem<Asynch::Scheduler>()->Add(
            Asynch::CreateScheduledTask(500, std::bind(&GameManager::DeleteGameTask,
                GetSubsystem<GameManager>(), id_))
        );
        break;
    case ExecutionState::Startup:
        // Do nothing
        break;
    }
}

void Game::SendStatus()
{
    gameState_.Send();
    gameState_.Reset();
}

Player* Game::GetPlayerById(uint32_t playerId) const
{
    auto it = players_.find(playerId);
    if (it == players_.end())
        return nullptr;
    return GetObject<Player>(*it);
}

Player* Game::GetPlayerByName(const std::string& name) const
{
    const uint32_t playerId = GetSubsystem<PlayerManager>()->GetPlayerIdByName(name);
    if (playerId != 0)
        return GetPlayerById(playerId);
    return nullptr;
}

void Game::AddObject(ea::shared_ptr<GameObject> object)
{
    AddObjectInternal(object);
    if (auto* actor = To<Actor>(object.get()))
    {
        // GameObjects and even Players live only in Games, so when a player changes the Game we automatically
        // unsubscribe from these events, because the Player object is destroyed.
        actor->SubscribeEvent<OnActorDiedSignature>(OnDiedEvent, [this](Actor* a, Actor* killer)
        {
            events_.CallAll<OnGameActorDiedSignature>(OnGameActorDiedEvent, a, killer);
            lua_.Call("onActorDied", a, killer);
        });
        actor->SubscribeEvent<OnResurrectedSignature>(OnResurrectedEvent, [this, source = actor](int /* health */, int /* energy */)
        {
            events_.CallAll<OnGameActorResurrectedSignature>(OnGameActorResurrectedEvent, source);
            lua_.Call("onActorResurrected", source);
        });
        events_.CallAll<OnGameActorAddedSignature>(OnGameActorAddedEvent, actor);
    }
    lua_.Call("onAddObject", object.get());
}

void Game::AddObjectInternal(ea::shared_ptr<GameObject> object)
{
    ASSERT(object);
    objects_.emplace(object->id_, object);
    object->SetGame(shared_from_this());
}

void Game::AddSceneObject(ea::shared_ptr<GameObject> object)
{
    // Only use it for static scene objects
    ASSERT(object->GetType() < AB::GameProtocol::GameObjectType::__SentToPlayer);
    AddObjectInternal(std::move(object));
}

void Game::InternalRemoveObject(GameObject* object)
{
    ASSERT(object);
    const auto it = objects_.find(object->id_);
    if (it == objects_.end())
        return;
    lua_.Call("onRemoveObject", object);
    object->SetGame(ea::shared_ptr<Game>());
    removedObjects_.emplace(object->id_);
}

ea::shared_ptr<Npc> Game::AddNpc(const std::string& script)
{
    ea::shared_ptr<Npc> result = ea::make_shared<Npc>();
    if (!result->LoadScript(script))
        return {};

    // This object will spawn in the next Game::Update()
    queuedObjects_.push_back(result);

    return result;
}

ea::shared_ptr<Npc> Game::AddSlave(const std::string& script,
    ea::shared_ptr<Actor> master, ea::shared_ptr<Actor> corpse,
    SlaveKind kind, int level)
{
    if (script.empty())
    {
        LOG_ERROR << "Script is empty" << std::endl;
        return {};
    }
    ea::shared_ptr<Npc> result = ea::make_shared<Npc>();
    // Set these before loading the script
    result->SetSlaveKind(kind);
    result->SetMaster(std::move(master));
    result->SetCorpse(std::move(corpse));
    if (level > 0)
        result->SetLevel((uint32_t)level);
    else
        result->SetLevel(1u);
    if (!result->LoadScript(script))
        return {};
    // Scale the model to the level
    const float scale = GetScaleForLevel(result->GetLevel());
    result->transformation_.scale_ = { scale, scale, scale };

    // This object will spawn in the next Game::Update()
    queuedObjects_.push_back(result);

    return result;
}

ea::shared_ptr<AreaOfEffect> Game::AddAreaOfEffect(const std::string& script,
    ea::shared_ptr<Actor> source,
    uint32_t index,
    const Math::Vector3& pos)
{
    ea::shared_ptr<AreaOfEffect> result = ea::make_shared<AreaOfEffect>();
    result->SetSource(std::move(source));
    result->SetIndex(index);
    result->transformation_.position_ = pos;
    if (Math::Equals(pos.y_, 0.0f))
        result->transformation_.position_.y_ = map_->GetTerrainHeight(pos);
    if (!result->LoadScript(script))
        return {};

    // This object will spawn in the next Game::Update()
    queuedObjects_.push_back(result);

    return result;
}

ea::shared_ptr<Projectile> Game::AddProjectile(const std::string& itemUuid,
    ea::shared_ptr<Actor> source,
    ea::shared_ptr<Actor> target)
{
    ea::shared_ptr<Projectile> result = ea::make_shared<Projectile>(itemUuid);
    result->SetSource(std::move(source));
    result->SetTarget(std::move(target));
    // Speed is set by the script
    if (!result->Load())
        return {};

    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(std::bind(&Game::SpawnObject,
            shared_from_this(), result))
    );
    return result;
}

ea::shared_ptr<ItemDrop> Game::AddRandomItemDropFor(Actor* dropper, Actor* target)
{
    if (state_ != ExecutionState::Running || !dropper)
        return {};
    if (AB::Entities::IsOutpost(data_.type))
        // No drops in outposts
        return {};

    if (target->GetType() != AB::GameProtocol::GameObjectType::Player)
        return {};

    const Player& targetPlayer = To<Player>(*target);

    auto* factory = GetSubsystem<ItemFactory>();
    auto* rng = GetSubsystem<Crypto::Random>();
    uint32_t itemId = factory->CreateDropItem(instanceData_.uuid, data_.uuid, dropper->GetLevel(), targetPlayer);
    if (itemId == 0)
        return {};

    ea::shared_ptr<ItemDrop> result = ea::make_shared<ItemDrop>(itemId);
    result->transformation_.position_ = dropper->transformation_.position_;
    // Random pos around dropper
    result->transformation_.position_.y_ += 0.2f;
    result->transformation_.position_.x_ += rng->Get<float>(-RANGE_TOUCH, RANGE_TOUCH);
    result->transformation_.position_.z_ += rng->Get<float>(-RANGE_TOUCH, RANGE_TOUCH);
    result->SetSource(dropper->GetPtr<Actor>());
    result->targetId_ = target->id_;

    // This object will spawn in the next Game::Update()
    queuedObjects_.push_back(result);
    return result;
}

ea::shared_ptr<ItemDrop> Game::AddRandomItemDrop(Actor* dropper)
{
    if (state_ != ExecutionState::Running || !dropper)
        return {};
    if (AB::Entities::IsOutpost(data_.type) || data_.type == AB::Entities::GameTypePvPCombat)
        // No drops in outposts and PvP games
        return {};

    auto* rng = GetSubsystem<Crypto::Random>();
    const float rnd = rng->GetFloat();
    auto p = sa::SelectRandomly(players_.begin(), players_.end(), rnd);

    if (p == players_.end())
        return {};

    auto* target = GetObject<Player>(*p);
    if (!target)
        return {};

    return AddRandomItemDropFor(dropper, target);
}

void Game::SpawnItemDrop(ea::shared_ptr<ItemDrop> item)
{
    item->SetGame(shared_from_this());
    // Also adds it to the objects array
    SpawnObject(item);

    const Item* pItem = item->GetItem();
    ASSERT(pItem);
    AB::Packets::Server::ObjectDroppedItem packet = {
        item->GetSourceId(),
        item->targetId_,
        item->id_,
        item->GetItemIndex(),
        static_cast<uint32_t>(pItem ? pItem->concreteItem_.count : 1u),
        static_cast<uint16_t>(pItem ? pItem->concreteItem_.value : 0u),
        pItem->stats_.ToString()
    };

    gameState_.AddPacket(AB::GameProtocol::ServerPacketType::ObjectDroppedItem, packet);
}

std::vector<Party*> Game::_LuaGetParties() const
{
    auto* partyMngr = GetSubsystem<PartyManager>();
    return partyMngr->GetByGame(id_);
}

std::vector<Group*> Game::_LuaGetGroups() const
{
    auto* partyMngr = GetSubsystem<PartyManager>();
    auto parties = partyMngr->GetByGame(id_);

    std::vector<Group*> result;
    std::copy(parties.begin(), parties.end(), std::back_inserter(result));
    for (const auto& group : groups_)
    {
        result.push_back(group.second.get());
    }
    return result;
}

void Game::CallLuaEvent(const std::string& name, GameObject* sender, GameObject* data)
{
    lua_.Call(name, sender, data);
}

void Game::SetState(ExecutionState state)
{
    if (state_ != state)
        state_ = state;
}

void Game::InternalLoad()
{
    // Game::Load() Thread

    if (!IO::LoadMap(*map_))
    {
        LOG_ERROR << "Error loading map with name " << map_->name_ << std::endl;
        return;
    }

    // Loading done -> start it
    Start();
}

void Game::Load(const std::string& mapUuid)
{
    state_ = ExecutionState::Startup;
    AB_PROFILE;
    // Dispatcher Thread
    if (!IO::LoadGameByUuid(*this, mapUuid))
    {
        LOG_ERROR << "Error loading game " << mapUuid << std::endl;
        return;
    }
    map_ = ea::make_unique<Map>(shared_from_this());
    map_->name_ = data_.name;
    map_->directory_ = data_.directory;

    // Must be executed here because the player doesn't wait to fully load the game to join
    // Execute initialization code if any
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(data_.script);
    if (!script)
    {
        LOG_ERROR << "Unable to get script " << data_.script << std::endl;
        return;
    }
    if (!lua_.Load(*script))
    {
        LOG_ERROR << "Error executing script " << data_.script << std::endl;
        return;
    }

    auto* thPool = GetSubsystem<Asynch::ThreadPool>();
    // Load Game Assets
    thPool->Enqueue(&Game::InternalLoad, shared_from_this());
    // Load item drop chances on this map
    auto* factory = GetSubsystem<ItemFactory>();
    thPool->Enqueue(&ItemFactory::LoadDropChances, factory, mapUuid);
}

void Game::SpawnObject(ea::shared_ptr<GameObject> object)
{
    AB::GameProtocol::GameObjectType objectType = object->GetType();
    if (objectType < AB::GameProtocol::GameObjectType::__SentToPlayer)
        return;
    if (objectType == AB::GameProtocol::GameObjectType::Player)
    {
        // Spawn points are loaded now
        const SpawnPoint p = map_->GetFreeSpawnPoint("Player");
#ifdef DEBUG_GAME
//        LOG_DEBUG << "Spawn point: " << p.group << "; Pos: " << p.position << std::endl;
#endif
        object->transformation_.position_ = p.position;
        object->transformation_.SetYRotation(p.rotation.EulerAngles().y_);
    }

    // Add object first, because the script may modify the object
    AddObject(object);
    // Make sure current message has at least 200 byte free space
    auto& msg = gameState_.GetMessage(200);
    msg.AddByte(AB::GameProtocol::ServerPacketType::ObjectSpawn);
    object->WriteSpawnData(gameState_);
}

void Game::SendLeaveObject(uint32_t objectId)
{
    gameState_.AddPacket<AB::Packets::Server::ObjectDespawn>(
        AB::GameProtocol::ServerPacketType::ObjectDespawn,
        { objectId });
}

void Game::SendInitStateToPlayer(Player& player)
{
    if (MustQueueObjects())
        return;

    PlayerMessageStream stream(player);
    // Send all already existing objects to the player, excluding the player itself.
    // This is sent to all players when they enter a game.
    // Only called when the player enters a game. All spawns during the game are sent
    // when they happen.
    for (const auto& oPair : objects_)
    {
        GameObject& o = *oPair.second;

        if (o.GetType() < AB::GameProtocol::GameObjectType::__SentToPlayer)
            // No need to send terrain patches etc. to client
            continue;
        if (o.id_ == player.id_)
            // Don't send spawn of our self
            continue;

        // Make sure current message has at least 200 byte free space
        auto& msg = stream.GetMessage(200);
        msg.AddByte(AB::GameProtocol::ServerPacketType::ObjectSpawnExisting);
        o.WriteSpawnData(stream);
    }
}

void Game::PlayerJoin(uint32_t playerId)
{
#ifdef DEBUG_NET
    ASSERT(GetSubsystem<Asynch::Dispatcher>()->IsDispatcherThread());
#endif
    ea::shared_ptr<Player> player = GetSubsystem<PlayerManager>()->GetPlayerById(playerId);
    if (!player)
        return;

    if (AB::Entities::IsOutpost(data_.type))
        player->data_.lastOutpostUuid = data_.uuid;
    player->data_.instanceUuid = instanceData_.uuid;
    UpdateEntity(player->data_);

    SendInitStateToPlayer(*player);

    if (!MustQueueObjects())
        SpawnObject(player);
    else
        // We didn't run Update() yet so we must queue the object. It is spawned in the first Update()
        queuedObjects_.push_back(player);

    // From now on the player receives the game status
    players_.emplace(player->id_);
    lua_.Call("onPlayerJoin", player.get());

    // Notify other servers that a player joined, e.g. for friend list
    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(std::bind(&PlayerManager::BroadcastPlayerLoggedIn, player))
    );
}

void Game::RemoveObject(GameObject* object)
{
    if (!object)
        return;
    const auto it = objects_.find(object->id_);
    if (it == objects_.end())
        return;
    if (auto* actor = To<Actor>(object))
        events_.CallAll<OnGameActorRemovedSignature>(OnGameActorRemovedEvent, actor);

    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(std::bind(&Game::SendLeaveObject, shared_from_this(), object->id_))
    );
    InternalRemoveObject(object);
}

void Game::PlayerLeave(uint32_t playerId)
{
#ifdef DEBUG_NET
    ASSERT(GetSubsystem<Asynch::Dispatcher>()->IsDispatcherThread());
#endif
    Player* player = GetPlayerById(playerId);
    if (!player)
        return;

    player->SetGame(ea::shared_ptr<Game>());
    const auto it = players_.find(playerId);
    if (it != players_.end())
    {
        lua_.Call("onPlayerLeave", player);
        players_.erase(it);
    }
    player->data_.instanceUuid = "";
    UpdateEntity(player->data_);

    auto* sched = GetSubsystem<Asynch::Scheduler>();
    sched->Add(
        Asynch::CreateScheduledTask(std::bind(&Game::SendLeaveObject, shared_from_this(), playerId))
    );
    // Notify other servers that a player left, e.g. for friend list
    sched->Add(
        Asynch::CreateScheduledTask(std::bind(&PlayerManager::BroadcastPlayerLoggedOut, player->GetPtr<Player>()))
    );
    InternalRemoveObject(player);
}

Group* Game::AddGroup()
{
    ea::unique_ptr<Group> group = ea::make_unique<Group>(Group::GetNewId());
    Group* result = group.get();
    groups_.emplace(group->GetId(), ea::move(group));
    return result;
}

Group* Game::GetGroup(uint32_t id) const
{
    const auto it = groups_.find(id);
    if (it != groups_.end())
        return (*it).second.get();
    auto* pMngr = GetSubsystem<PartyManager>();
    auto party = pMngr->Get(id);
    if (party)
        return party.get();
    return nullptr;
}

}
