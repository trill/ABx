/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "NavigationMesh.h"
#include "IOAsset.h"

namespace IO {

class IONavMesh final : public IOAssetImpl<Navigation::NavigationMesh>
{
public:
    bool Import(Navigation::NavigationMesh& asset, const std::string& name) override;
};

}

