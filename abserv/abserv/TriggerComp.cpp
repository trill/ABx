/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TriggerComp.h"
#include "GameObject.h"
#include "Game.h"
#include <sa/time.h>

 //#define DEBUG_COLLISION

namespace Game {
namespace Components {

TriggerComp::TriggerComp(GameObject& owner) :
    owner_(owner)
{
    owner_.SubscribeEvent<OnCollideSignature>(OnCollideEvent, std::bind(&TriggerComp::OnCollide, this, std::placeholders::_1));
}

void TriggerComp::DoTrigger(GameObject* other)
{
    if (!other)
        return;

#ifdef DEBUG_COLLISION
    LOG_DEBUG << owner_.GetName() << ": " << *other << std::endl;
#endif
    const int64_t tick = sa::time::tick();
    const auto it = triggered_.find(other->id_);
    const int64_t lastTrigger = (it != triggered_.end()) ? (*it).second : 0;
    if (lastTrigger == 0 || sa::time::time_elapsed(lastTrigger) > retriggerTimeout_)
    {
        triggered_[other->id_] = tick;
        owner_.CallEvent<OnTriggerSignature>(OnTriggerEvent, other);
    }
}

void TriggerComp::Update(uint32_t timeElapsed)
{
    if (triggered_.empty())
        return;

    lastCheck_ += timeElapsed;
    // Keep objects at least 2 ticks inside
    if (lastCheck_ < NETWORK_TICK * 2)
        return;

    // Check if objects are still inside
    auto game = owner_.GetGame();
    if (!game)
    {
        // Game over
        triggered_.clear();
    }
    else
    {
        Math::Vector3 move;
        for (auto it = triggered_.begin(); it != triggered_.end(); )
        {
            auto* o = game->GetObject<GameObject>((*it).first);
            // If not inside remove from triggered list
            if (!o)
            {
                // This object is gone
                triggered_.erase(it++);
            }
            else if (!owner_.Collides(o, Math::Vector3_Zero, move))
            {
                // No longer collides
                triggered_.erase(it++);
                owner_.CallEvent<OnLeftAreaSignatre>(OnLeftAreaEvent, o);
            }
            else
                ++it;
        }
    }
    lastCheck_ = 0;
}

void TriggerComp::OnCollide(GameObject* other)
{
    if (trigger_ && other)
        DoTrigger(other);
}

}
}
