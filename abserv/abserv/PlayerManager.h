/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <memory>
#include <limits>
#include <libcommon/Utils.h>
#include <eastl.hpp>
#include <sa/Iteration.h>
#include <multi_index_container.hpp>
#include <multi_index/hashed_index.hpp>
#include <multi_index/ordered_index.hpp>
#include <multi_index/member.hpp>
#include <sa/time.h>
#include <robin_hood.h>

namespace Net {
class ProtocolGame;
class NetworkMessage;
}

namespace Game {

class Player;

class PlayerManager
{
private:
    struct PlayerIndexItem
    {
        uint32_t id;
        std::string playerUuid;
        std::string playerName;
        std::string accountUuid;
    };

    struct IdIndexTag {};
    struct PlayerUuidIndexTag {};
    struct PlayerNameIndexTag {};
    struct AccountUuidIndexTag {};

    using PlayerIndex = multi_index::multi_index_container <
        PlayerIndexItem,
        multi_index::indexed_by <
            multi_index::hashed_unique<
                multi_index::tag<IdIndexTag>,
                multi_index::member<PlayerIndexItem, uint32_t, &PlayerIndexItem::id>
            >,
            multi_index::hashed_unique<
                multi_index::tag<PlayerUuidIndexTag>,
                multi_index::member<PlayerIndexItem, std::string, &PlayerIndexItem::playerUuid>
            >,
            multi_index::hashed_unique<
                multi_index::tag<PlayerNameIndexTag>,
                multi_index::member<PlayerIndexItem, std::string, &PlayerIndexItem::playerName>
            >,
            multi_index::hashed_unique<
                multi_index::tag<AccountUuidIndexTag>,
                multi_index::member<PlayerIndexItem, std::string, &PlayerIndexItem::accountUuid>
            >
        >
    >;

    /// Index to lookup players
    PlayerIndex playerIndex_;
    /// Time with no players
    int64_t idleTime_;
    /// The owner of players
    robin_hood::unordered_map<uint32_t, ea::shared_ptr<Player>> players_;
public:
    // Inform all game servers that some player status change, to update e.g. the friend list.
    static void BroadcastPlayerLoggedIn(ea::shared_ptr<Player> player);
    static void BroadcastPlayerLoggedOut(ea::shared_ptr<Player> player);
    static void BroadcastPlayerChanged(const Player& player, uint32_t fields);

    PlayerManager() :
        idleTime_(sa::time::tick())
    {}
    ~PlayerManager()
    {
        playerIndex_.clear();
        players_.clear();
    }

    [[nodiscard]] ea::shared_ptr<Player> GetPlayerByName(const std::string& name) const;
    /// Get player by player UUID
    [[nodiscard]] ea::shared_ptr<Player> GetPlayerByUuid(const std::string& uuid) const;
    /// Get player by in game ID
    [[nodiscard]] ea::shared_ptr<Player> GetPlayerById(uint32_t id) const;
    [[nodiscard]] ea::shared_ptr<Player> GetPlayerByAccountUuid(const std::string& uuid) const;
    /// Get player ID by name
    [[nodiscard]] uint32_t GetPlayerIdByName(const std::string& name) const;
    ea::shared_ptr<Player> CreatePlayer(std::shared_ptr<Net::ProtocolGame> client);
    void UpdatePlayerIndex(const Player& player);
    void RemovePlayer(uint32_t playerId);
    void CleanPlayers();
    void RefreshAuthTokens();
    void KickPlayer(uint32_t playerId);
    void KickAllPlayers();
    void SaveAllPlayers();
    void BroadcastNetMessage(const Net::NetworkMessage& msg);

    [[nodiscard]] size_t GetPlayerCount() const
    {
        return players_.size();
    }

    [[nodiscard]] int64_t GetIdleTime() const
    {
        if (!players_.empty())
            return 0;
        return sa::time::tick() - idleTime_;
    }
    template<typename Callback>
    void VisitPlayers(Callback&& callback)
    {
        for (auto& player : players_)
        {
            if (player.second)
            {
                if (callback(*player.second) != Iteration::Continue)
                    break;
            }
        }
    }
};

}
