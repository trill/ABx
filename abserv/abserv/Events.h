/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <sa/Events.h>
#include <sa/StringHash.h>
#include <libmath/Vector3.h>

namespace Net {
class NetworkMessage;
}

namespace AB::Entities {
enum SkillType : uint64_t;
}

namespace Game {

enum class DamageType;
class GameObject;
class Actor;
class Npc;
class Player;
class AreaOfEffect;
class Projectile;
class Skill;
class Effect;
class Game;
class MessageStream;

// Events sent by a GameObject
inline constexpr sa::event_t OnClickedEvent = sa::StringHash("OnClicked");
inline constexpr sa::event_t OnCollideEvent = sa::StringHash("OnCollide");
inline constexpr sa::event_t OnLeftAreaEvent = sa::StringHash("OnLeftArea");
inline constexpr sa::event_t OnSelectedEvent = sa::StringHash("OnSelected");
inline constexpr sa::event_t OnTriggerEvent = sa::StringHash("OnTrigger");
inline constexpr sa::event_t OnStateChangeEvent = sa::StringHash("OnStateChange");
inline constexpr sa::event_t OnInteractEvent = sa::StringHash("OnInteract");
inline constexpr sa::event_t OnCancelAllEvent = sa::StringHash("OnCancelAll");

// Actor
inline constexpr sa::event_t OnPostSpawnEvent = sa::StringHash("OnPostSpawn");
inline constexpr sa::event_t OnArrivedEvent = sa::StringHash("OnArrived");
inline constexpr sa::event_t OnInterruptedAttackEvent = sa::StringHash("OnInterruptedAttack");
inline constexpr sa::event_t OnInterruptedSkillEvent = sa::StringHash("OnInterruptedSkill");
inline constexpr sa::event_t OnKnockedDownEvent = sa::StringHash("OnKnockedDown");
inline constexpr sa::event_t OnHealedEvent = sa::StringHash("OnHealed");
// Arguments: Actor* (who died), Actor* (who killed the poor victim)
inline constexpr sa::event_t OnDiedEvent = sa::StringHash("OnDied");
inline constexpr sa::event_t OnResurrectedEvent = sa::StringHash("OnResurrected");
inline constexpr sa::event_t OnPingObjectEvent = sa::StringHash("OnPingObject");
inline constexpr sa::event_t OnInventoryFullEvent = sa::StringHash("OnInventoryFull");
inline constexpr sa::event_t OnChestFullEvent = sa::StringHash("OnChestFull");

// We start attacking a target
inline constexpr sa::event_t OnSourceAttackStartEvent = sa::StringHash("OnSourceAttackStart");
// We attacked successfully the target
inline constexpr sa::event_t OnSourceAttackSuccessEvent = sa::StringHash("OnSourceAttackSuccess");
// We are the target of an attack and the attack cycle starts
inline constexpr sa::event_t OnTargetAttackStartEvent = sa::StringHash("OnTargetAttackStart");
// We were being attacked and the attack cycle was finished
inline constexpr sa::event_t OnTargetAttackEndEvent = sa::StringHash("OnTargetAttackEnd");
// We were attacked, but blocked the attack
inline constexpr sa::event_t OnTargetAttackBlockedEvent = sa::StringHash("OnTargetAttackBlocked");
inline constexpr sa::event_t OnTargetAttackSuccessEvent = sa::StringHash("OnTargetAttackSuccess");

inline constexpr sa::event_t OnCanUseSkillEvent = sa::StringHash("OnCanUseSkill");
inline constexpr sa::event_t OnGettingSkillTargetEvent = sa::StringHash("OnGettingSkillTarget");
inline constexpr sa::event_t OnSkillTargetedEvent = sa::StringHash("OnSkillTargeted");
inline constexpr sa::event_t OnEndUseSkillEvent = sa::StringHash("OnEndUseSkill");
inline constexpr sa::event_t OnStartUseSkillEvent = sa::StringHash("OnStartUseSkill");
inline constexpr sa::event_t OnGetCriticalHitEvent = sa::StringHash("OnGetCriticalHit");
inline constexpr sa::event_t OnInterruptingAttackEvent = sa::StringHash("OnInterruptingAttack");
inline constexpr sa::event_t OnInterruptingSkillEvent = sa::StringHash("OnInterruptingSkill");
inline constexpr sa::event_t OnKnockingDownEvent = sa::StringHash("OnKnockingDown");
inline constexpr sa::event_t OnHealingEvent = sa::StringHash("OnHealing");
inline constexpr sa::event_t OnHealingTargetEvent = sa::StringHash("OnHealingTarget");
inline constexpr sa::event_t OnStuckEvent = sa::StringHash("OnStuck");
inline constexpr sa::event_t OnIncMoraleEvent = sa::StringHash("OnIncMorale");
inline constexpr sa::event_t OnDecMoraleEvent = sa::StringHash("OnDecMorale");
inline constexpr sa::event_t OnKilledFoeEvent = sa::StringHash("OnKilledFoe");
inline constexpr sa::event_t OnXPAddedEvent = sa::StringHash("OnXPAdded");
inline constexpr sa::event_t OnSkillPointAddedEvent = sa::StringHash("OnSkillPointAdded");
inline constexpr sa::event_t OnAdvanceLevelEvent = sa::StringHash("OnAdvanceLevel");
inline constexpr sa::event_t OnAddEffectEvent = sa::StringHash("OnAddEffect");
inline constexpr sa::event_t OnRemoveEffectEvent = sa::StringHash("OnRemoveEffect");
inline constexpr sa::event_t OnKnockingDownTargetEvent = sa::StringHash("OnKnockingDownTarget");
inline constexpr sa::event_t OnInterruptingTargetEvent = sa::StringHash("OnInterruptingTarget");
inline constexpr sa::event_t OnDealingDamageEvent = sa::StringHash("OnDealingDamage");
inline constexpr sa::event_t OnGettingDamageEvent = sa::StringHash("OnGettingDamage");
inline constexpr sa::event_t OnGotDamageEvent = sa::StringHash("OnGotDamage");
inline constexpr sa::event_t OnShadowStepEvent = sa::StringHash("OnShadowStep");
inline constexpr sa::event_t OnWasInterruptedEvent = sa::StringHash("OnWasInterrupted");
inline constexpr sa::event_t OnTargetSkillSuccessEvent = sa::StringHash("OnTargetSkillSuccess");
inline constexpr sa::event_t OnInterruptedEvent = sa::StringHash("OnInterrupted");
inline constexpr sa::event_t OnHealedTargetEvent = sa::StringHash("OnHealedTarget");
inline constexpr sa::event_t OnEnergyZeroEvent = sa::StringHash("OnEnergyZero");
inline constexpr sa::event_t OnSpawnedSlaveEvent = sa::StringHash("OnSpawnedSlave");
inline constexpr sa::event_t OnExploitedCorpseEvent = sa::StringHash("OnExploitedCorpse");
inline constexpr sa::event_t OnSkillRechargedEvent = sa::StringHash("OnSkillRecharged");

using VoidVoidSignature = void(void);
using VoidIntSignature = void(int);
using ActorSignature = void(Actor*);
using VoidUInt32signature = void(uint32_t);
using ActorActorSignature = void(Actor*, Actor*);
using ActorSkillSignature = void(Actor*, Skill*);
using OnCollideSignature = void(GameObject*);
using OnTriggerSignature = OnCollideSignature;
using OnLeftAreaSignatre = OnCollideSignature;
using OnInteractSignature = ActorSignature;
using OnResurrectSignature = ActorSignature;
using OnClickedSignature = ActorSignature;
using OnSelectedSignature = ActorSignature;

using OnPostSpawnSignature = void(Game* game);
// OnStateChange(old, new)
using OnStateChangeSignature = void(AB::GameProtocol::CreatureState, AB::GameProtocol::CreatureState);
// OnAddEffect(source, effect, success)
using OnAddEffectSignature = void(Actor*, Effect*, bool& success, uint32_t& time);
using OnRemoveEffectSignature = void(Effect*, bool& success);
using OnInterruptingSkillSignature = void(Actor*, AB::Entities::SkillType, Skill*, bool&);
using OnPingObjectSignature = void(uint32_t, AB::GameProtocol::ObjectCallType, int);
using OnCanUseSkillSignature = void(Actor*, Skill*, bool&);
using OnGettingSkillTargetSignature = OnCanUseSkillSignature;
using OnInterruptedSkillSignature = void(Skill*);
using OnSkillRechargedSignature = void(int pos, Skill*);
using OnStartUseSkillSignature = ActorSkillSignature;
using OnSkillTargetedSignature = ActorSkillSignature;
using OnEndUseSkillSignature = ActorSkillSignature;
using OnWasInterruptedSignature = ActorSkillSignature;
// Someone used successfully a skill on us
using OnTargetSkillSuccessSignature = ActorSkillSignature;
using OnEnergyZeroSignature = VoidVoidSignature;
using OnInterruptedSignature = void(Actor* target, Skill*, bool success);
using OnDealingDamageSignature = void(Actor* target, DamageType, int32_t& value, bool& success);
using OnGettingDamageSignature = void(Actor* source, Skill*, DamageType, int32_t& value);
using OnGotDamageSignature = void(Actor* source, Skill*, DamageType, int32_t value);
// who died, killer
using OnActorDiedSignature = ActorActorSignature;
// killer, who was killed
using OnKilledFoeSignature = ActorActorSignature;
using OnKnockingDownSignature = void(Actor*, uint32_t, bool&);
using OnKnockedDownSignature = VoidUInt32signature;
using OnHealingSignature = void(Actor*, int&);
using OnHealingTargetSignature = OnHealingSignature;
using OnHealedTargetSignature = void(Actor* target, Skill* skill, int value);

using OnAttackStartSignature = void(Actor*, bool&);
using OnAttackEndSignature = void(Actor*, DamageType, int32_t, AB::GameProtocol::AttackError&);
using OnAttackSuccessSignature = void(Actor*, DamageType, int32_t);
using OnAttackBlockedSignature = ActorSignature;

using OnGetCriticalHitSignature = OnAttackStartSignature;
using OnInterruptingAttackSignature = void(bool&);
using OnResurrectedSignature = void(int, int);
using OnKnockingDownTargetSignature = void(Actor* source, uint32_t time, bool& success);
using OnInterruptingTargetSignature = void(Actor* source, bool& success);
using OnShadowStepSignature = void(const Math::Vector3& pos, bool& success);
using OnSpawnedSlaveSignature = ActorSignature;
using OnExploitedCorpseSignature = ActorActorSignature;

using GameObjectEvents = sa::Events<
    VoidVoidSignature,
    VoidUInt32signature,
    VoidIntSignature,
    ActorActorSignature,
    ActorSkillSignature,
    OnInterruptingAttackSignature,
    OnStateChangeSignature,
    OnResurrectedSignature,
    OnCollideSignature,
    ActorSignature,
    OnInterruptedSkillSignature,
    OnKnockingDownSignature,
    OnAttackSuccessSignature,
    OnAttackEndSignature,
    OnInterruptingSkillSignature,
    OnPingObjectSignature,
    OnCanUseSkillSignature,
    OnAttackStartSignature,
    OnHealingSignature,
    OnAddEffectSignature,
    OnKnockingDownTargetSignature,
    OnInterruptingTargetSignature,
    OnDealingDamageSignature,
    OnGettingDamageSignature,
    OnShadowStepSignature,
    OnInterruptedSignature,
    OnHealedTargetSignature,
    OnGotDamageSignature,
    OnRemoveEffectSignature,
    OnPostSpawnSignature,
    OnSkillRechargedSignature
>;

// Events sent by the game
inline constexpr sa::event_t OnGameActorDiedEvent = sa::StringHash("OnGameActorDied");
inline constexpr sa::event_t OnGameActorResurrectedEvent = sa::StringHash("OnGameActorResurrected");
inline constexpr sa::event_t OnGameActorAddedEvent = sa::StringHash("OnGameActorAdded");
inline constexpr sa::event_t OnGameActorRemovedEvent = sa::StringHash("OnGameActorRemoved");

// who died, killer
using OnGameActorDiedSignature = ActorActorSignature;
using OnGameActorResurrectedSignature = ActorSignature;
using OnGameActorAddedSignature = ActorSignature;
using OnGameActorRemovedSignature = ActorSignature;

using GameEvents = sa::Events<
    ActorActorSignature,
    ActorSignature
>;

}
