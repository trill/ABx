/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <libcommon/Utils.h>
#include <sa/Noncopyable.h>
#include <sa/time.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class GameObject;
class MessageStream;

namespace Components {

class StateComp
{
    NON_COPYABLE(StateComp)
    NON_MOVEABLE(StateComp)
private:
    static constexpr uint32_t LAST_KD_TIME = 5000;
    GameObject& owner_;
    AB::GameProtocol::CreatureState currentState_{ AB::GameProtocol::CreatureState::Idle };
    AB::GameProtocol::CreatureState newState_{ AB::GameProtocol::CreatureState::Idle };
    int64_t lastStateChange_;
    int64_t knockdownEndTime_{ 0 };
    int64_t knockdownTick_{ 0 };
    void OnCancelAll();
public:
    explicit StateComp(GameObject& owner);
    ~StateComp() = default;

    void SetState(AB::GameProtocol::CreatureState state, bool apply = false);
    AB::GameProtocol::CreatureState GetState() const
    {
        return currentState_;
    }
    bool IsStateChanged() const
    {
        return newState_ != currentState_;
    }
    bool IsDead() const { return newState_ == AB::GameProtocol::CreatureState::Dead; }
    bool IsKnockedDown() const { return newState_ == AB::GameProtocol::CreatureState::KnockedDown; }
    bool IsMoving() const { return newState_ == AB::GameProtocol::CreatureState::Moving; }
    bool WasKnockedDown() const { return knockdownTick_ != 0 && sa::time::time_elapsed(knockdownTick_) < LAST_KD_TIME; }
    bool KnockDown(uint32_t time);
    void Apply();
    /// Reset to idle state
    void Reset();
    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);
};

}
}

