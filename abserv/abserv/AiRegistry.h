/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Registry.h>

namespace AI {

class AiRegistry final : public Registry
{
public:
    AiRegistry();
    void Initialize() override;
};

}
