/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <eastl.hpp>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>

namespace Game {

class GameObject;

namespace Components {

/// NPCs can be used a trigger box. This component calls Actor::OnTrigger() when it collides with the collision shape.
class TriggerComp
{
    NON_COPYABLE(TriggerComp)
    NON_MOVEABLE(TriggerComp)
private:
    GameObject& owner_;
    ea::map<uint32_t, int64_t> triggered_;
    uint32_t lastCheck_{ 0 };
    void OnCollide(GameObject* other);
public:
    TriggerComp() = delete;
    explicit TriggerComp(GameObject& owner);
    ~TriggerComp() = default;

    // Manually trigger. Takes care to not call OnTrigger twice in case of an additional real collision.
    void DoTrigger(GameObject* other);
    void Update(uint32_t timeElapsed);
    /// Get all object IDs inside the collision shape
    template <typename Callback>
    void VisitObjectInside(Callback&& callback)
    {
        for (const auto& i : triggered_)
        {
            if (callback(i.first) != Iteration::Continue)
                break;
        }
    }

    bool trigger_{ false };
    /// Time in ms the same Actor can retrigger
    uint32_t retriggerTimeout_{ std::numeric_limits<uint32_t>::max() };   // By default never retrigger
};

}
}

