/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Skill.h"
#include "Actor.h"
#include "DataProvider.h"
#include "ResourceComp.h"
#include "Script.h"
#include "EffectsComp.h"
#include <libcommon/DataClient.h>
#include <libcommon/Subsystems.h>
#include <sa/Checked.h>

namespace Game {

void Skill::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Skill"].setClass(std::move(kaguya::UserdataMetatable<Skill>()
        .addFunction("GetProfession", &Skill::_LuaGetProfession)
        .addFunction("Disable",      &Skill::Disable)
        .addFunction("Interrupt",    &Skill::Interrupt)
        .addFunction("AddRecharge",  &Skill::AddRecharge)
        .addFunction("SetRecharged", &Skill::SetRecharged)
        .addFunction("IsUsing",      &Skill::IsUsing)
        .addFunction("IsRecharged",  &Skill::IsRecharged)
        .addFunction("GetType",      &Skill::_LuaGetType)
        .addFunction("IsElite",      &Skill::_LuaIsElite)
        .addFunction("IsInRange",    &Skill::IsInRange)
        .addFunction("HasEffect",    &Skill::HasEffect)
        .addFunction("HasTarget",    &Skill::HasTarget)
        .addFunction("IsType",       &Skill::IsType)
        .addFunction("IsMaintainable", &Skill::_LuaIsMaintainable)
        .addFunction("GetAttribute", &Skill::GetAttribute)
        .addFunction("GetLastError", &Skill::GetLastError)
        // Manually execute skill. This only calls onSuccess
        .addFunction("Execute",      &Skill::Execute)
        .addFunction("GetRange",     &Skill::GetRange)
        .addFunction("GetEnergy",              &Skill::_LuaGetEnergy)
        .addFunction("GetAdrenaline",          &Skill::_LuaGetAdrenaline)
        .addFunction("GetActivation",          &Skill::_LuaGetActivation)
        .addFunction("GetOvercast",            &Skill::_LuaGetOvercast)
        .addFunction("GetHp",                  &Skill::_LuaGetHp)
        .addFunction("GetRecharge",            &Skill::_LuaGetRecharge)
        .addFunction("GetEffectiveEnergy",     &Skill::GetEffectiveEnergy)
        .addFunction("GetEffectiveAdrenaline", &Skill::GetEffectiveAdrenaline)
        .addFunction("GetEffectiveActivation", &Skill::GetEffectiveActivation)
        .addFunction("GetEffectiveOvercast",   &Skill::GetEffectiveOvercast)
        .addFunction("GetEffectiveHp",         &Skill::GetEffectiveHp)
        .addFunction("GetEffectiveRecharge",   &Skill::GetEffectiveRecharge)

        .addProperty("Index", &Skill::GetIndex)
        .addProperty("Name", &Skill::_LuaGetName)
        .addProperty("Source", &Skill::GetSource)
        .addProperty("Target", &Skill::GetTarget)
    ));
    // clang-format on
}

Skill::Skill(AB::Entities::Skill skill) :
    lua_(*this),
    data_(std::move(skill))
{ }

bool Skill::InternalLoadScript(const Script& script)
{
    if (!lua_.Load(script))
        return false;

    auto* dataClient = GetSubsystem<IO::DataClient>();
    auto getProfessionUuid = [&](AB::Entities::ProfessionIndex index) -> std::string
    {
        if (index == AB::Entities::ProfessionIndexNone)
            return AB::Entities::PROFESSION_NONE_UUID;

        AB::Entities::Profession prof;
        prof.index = static_cast<uint32_t>(index);
        if (dataClient->Read(prof))
        {
            return prof.uuid;
        }
        LOG_ERROR << "Error reading profession with index " << index << std::endl;
        return AB::Entities::PROFESSION_NONE_UUID;
    };

    if (lua_.IsNumber("skillType"))
        skillType_ = static_cast<AB::Entities::SkillType>(lua_["skillType"]);
    else
        LOG_WARNING << "Missing skillType field in " << script.GetFileName() << std::endl;
    if (lua_.IsNumber("index"))
        skillIndex_ = lua_["index"];
    else
        LOG_WARNING << "Missing index field in " << script.GetFileName() << std::endl;
    if (lua_.IsNumber("attribute"))
        attribute_ = static_cast<Attribute>(lua_["attribute"]);
    else
        LOG_WARNING << "Missing attribute field in " << script.GetFileName() << std::endl;
    if (lua_.IsBool("isElite"))
        elite_ = lua_["isElite"];
    else
        elite_ = false;
    if (lua_.IsNumber("profession"))
    {
        profession_ = static_cast<AB::Entities::ProfessionIndex>(lua_["profession"]);
        professionUuid_ = getProfessionUuid(profession_);
    }
    else
    {
        LOG_WARNING << "Missing profession field in " << script.GetFileName() << std::endl;
        professionUuid_ = AB::Entities::PROFESSION_NONE_UUID;
    }

    if (lua_.IsString("name"))
        name_ = static_cast<const char*>(lua_["name"]);
    else
        LOG_WARNING << "Missing name field in " << script.GetFileName() << std::endl;

    if (lua_.IsBool("maintainable"))
        maintainable_ = lua_["maintainable"];
    if (lua_.IsBool("easyInterruptable"))
        easyInterruptable_ = lua_["easyInterruptable"];
    energy_ = lua_["costEnergy"];
    adrenaline_ = lua_["costAdrenaline"];
    activation_ = lua_["activation"];
    recharge_ = lua_["recharge"];

    overcast_ = lua_["overcast"];
    if (lua_.IsNumber("hp"))
        hp_ = lua_["hp"];

    if (lua_.IsNumber("range"))
        range_ = static_cast<Range>(lua_["range"]);
    if (lua_.IsNumber("targetType"))
        targetType_ = static_cast<SkillTargetType>(lua_["targetType"]);
    if (lua_.IsNumber("effect"))
        skillEffect_ = static_cast<uint32_t>(lua_["effect"]);
    if (lua_.IsNumber("effectTarget"))
        effectTarget_ = static_cast<uint32_t>(lua_["effectTarget"]);
    if (lua_.IsNumber("canInterrupt"))
        canInterrupt_ = lua_["canInterrupt"];
    if (lua_.IsBool("activatesCompanion"))
        activatesCompanion_ = lua_["activatesCompanion"];

    return true;
}

bool Skill::LoadScript(const std::string& fileName)
{
    auto* dp = GetSubsystem<IO::DataProvider>();
    auto script = dp->GetAsset<Script>(fileName);
    if (!script)
        return false;
    return InternalLoadScript(*script);
}

AB::GameProtocol::SkillError Skill::Execute()
{
    auto source = source_.lock();
    auto target = target_.lock();
    return lua_["onSuccess"](source.get(), target.get());
}

void Skill::Update(uint32_t)
{
    if (!rechargedFired_ && rechargedAt_ < sa::time::tick())
        FireOnRecharged();

    if ((startUse_ == 0) || IsUsing())
        return;

    effectiveRecharge_ = CalculateRecharge(recharge_);
    if (effectiveRecharge_ < (uint32_t)-1)
        SetRecharged(sa::ClampedAdd<int64_t>(sa::time::tick(), effectiveRecharge_));
    else
        SetRecharged(std::numeric_limits<int64_t>::max());
    // A Skill may even fail here, e.g. when resurrecting an already resurrected target
    auto source = source_.lock();
    auto target = target_.lock();
    lastError_ = Execute();
    startUse_ = 0;
    if (lastError_ != AB::GameProtocol::SkillError::None)
        SetRecharged(0);
    else
    {
        // On success sacrifice the HP
        effectiveHp_ = static_cast<int>(GetHpSacrifies(effectiveHp_));
        source->resourceComp_->SetHealth(Components::SetValueType::DecreasePercent, effectiveHp_);
        lastUse_ = sa::time::tick();
    }
    if (source)
        source->CallEvent<OnEndUseSkillSignature>(OnEndUseSkillEvent, target.get(), this);
    if (target)
        target->CallEvent<OnTargetSkillSuccessSignature>(OnTargetSkillSuccessEvent, source.get(), this);
    EndUse();
}

bool Skill::CanUseSkill(Actor& source, Actor* target)
{
    bool success = true;
    source.CallEvent<OnCanUseSkillSignature>(OnCanUseSkillEvent, target, this, success);
    if (!success)
    {
        lastError_ = AB::GameProtocol::SkillError::CannotUseSkill;
        return false;
    }

    if (targetType_ != SkillTargetTypeNone)
    {
        if (!target)
        {
            lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
            return false;
        }

        switch (targetType_)
        {
        case SkillTargetTypeAny:
            return true;
        case SkillTargetTypeSelf:
            if (target->id_ != source.id_)
            {
                lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
                return false;
            }
            break;
        case SkillTargetTypeAllyAndSelf:
            if (!source.IsAlly(target) && source.id_ != target->id_)
            {
                lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
                return false;
            }
            break;
        case SkillTargetTypeAllyWithoutSelf:
            if (!source.IsAlly(target) || source.id_ == target->id_)
            {
                lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
                return false;
            }
            break;
        case SkillTargetTypeFoe:
            if (!source.IsEnemy(target))
            {
                lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
                return false;
            }
            break;
        default:
            break;
        }

        if (target->IsUndestroyable())
        {
            lastError_ = AB::GameProtocol::SkillError::TargetUndestroyable;
            return false;
        }
        // Finally check the target is not immune or something
        bool targetSuccess = true;
        target->CallEvent<OnGettingSkillTargetSignature>(OnGettingSkillTargetEvent, &source, this, targetSuccess);
        if (!targetSuccess)
        {
            lastError_ = AB::GameProtocol::SkillError::InvalidTarget;
            return false;
        }
    }

    if (IsUsing() || !IsRecharged())
        lastError_ = AB::GameProtocol::SkillError::Recharging;
    return !IsSkillError(lastError_);
}

uint32_t Skill::CalculateRecharge(uint32_t recharge)
{
    // This becomes recharged only with a morale boost.
    if (recharge == (uint32_t)-1)
        return recharge;

    uint32_t result = recharge;
    if (lua_.IsFunction("getRecharge"))
        result = lua_["getRecharge"](result);

    if (auto s = source_.lock())
        GetSkillRecharge(*s, this, result);
    return result;
}

uint32_t Skill::GetActivation(uint32_t activation)
{
    if (auto s = source_.lock())
        GetSkillActivation(*s, this, activation);
    return activation;
}

uint32_t Skill::GetHpSacrifies(uint32_t hp)
{
    uint32_t result = hp;
    if (lua_.IsFunction("getSacrifies"))
        result = lua_["getSacrifies"](result);
    if (auto s = source_.lock())
        GetSkillHpSacrifies(*s, this, result);
    return result;
}

AB::GameProtocol::SkillError Skill::CanUse(Actor* source, Actor* target)
{
    if (lua_.IsFunction("canUse"))
        return lua_["canUse"](source, target);
    return lua_["onStartUse"](source, target);
}

AB::GameProtocol::SkillError Skill::StartUse(ea::shared_ptr<Actor> source, ea::shared_ptr<Actor> target)
{
    lastError_ = AB::GameProtocol::SkillError::None;
    bool ret = CanUseSkill(*source, target.get());
    if (!ret && lastError_ == AB::GameProtocol::SkillError::InvalidTarget)
    {
        // This allows us to e.g. heal ourself while keeping a foe selected
        if (targetType_ == SkillTargetTypeSelf || targetType_ == SkillTargetTypeAllyAndSelf)
        {
            lastError_ = AB::GameProtocol::SkillError::None;
            target = source;
            ret = CanUseSkill(*source, target.get());
        }
    }
    if (!ret)
        return lastError_;
    lastError_ = AB::GameProtocol::SkillError::None;

    effectiveEnergy_ = energy_;
    effectiveAdrenaline_ = adrenaline_;
    effectiveActivation_ = GetActivation(activation_);
    effectiveOvercast_ = overcast_;
    effectiveHp_ = hp_;
    // Get real skill cost, which depends on the effects and equipment of the source
    GetSkillCost(*source, this, effectiveActivation_, effectiveEnergy_, effectiveAdrenaline_, effectiveOvercast_, effectiveHp_);
    if (target)
        target->effectsComp_->GetTargetSkillCost(source.get(), this, effectiveActivation_, effectiveEnergy_, effectiveAdrenaline_, effectiveOvercast_, effectiveHp_);

    if (source->resourceComp_->GetEnergy() < effectiveEnergy_)
        lastError_ = AB::GameProtocol::SkillError::NoEnergy;
    if (IsSkillError(lastError_))
        return lastError_;
    if (source->resourceComp_->GetAdrenaline() < effectiveAdrenaline_)
        lastError_ = AB::GameProtocol::SkillError::NoAdrenaline;
    if (IsSkillError(lastError_))
        return lastError_;

    startUse_ = sa::time::tick();

    source_ = source;
    target_ = target;

    lastError_ = lua_["onStartUse"](source.get(), target.get());
    if (IsSkillError(lastError_))
    {
        startUse_ = 0;
        SetRecharged(0);
        source_.reset();
        target_.reset();
        return lastError_;
    }
    if (target)
        target->CallEvent<OnSkillTargetedSignature>(OnSkillTargetedEvent, source.get(), this);
    source->resourceComp_->SetEnergy(Components::SetValueType::Decrease, effectiveEnergy_);
    source->resourceComp_->SetAdrenaline(Components::SetValueType::Decrease, effectiveAdrenaline_);
    source->resourceComp_->SetOvercast(Components::SetValueType::Increase, effectiveOvercast_);
    source->CallEvent<OnStartUseSkillSignature>(OnStartUseSkillEvent, target.get(), this);
    return lastError_;
}

void Skill::CancelUse()
{
    auto source = source_.lock();
    auto target = target_.lock();
    lua_.Call("onCancelled", source.get(), target.get());
    if (source)
        source->CallEvent<OnEndUseSkillSignature>(OnEndUseSkillEvent, target.get(), this);
    startUse_ = 0;
    // No recharging when canceled
    EndUse();
    SetRecharged(0);
}

bool Skill::Interrupt()
{
    if (!IsUsing() || !IsChangingState())
        return false;

    auto source = source_.lock();
    auto target = target_.lock();
    lua_.Call("onInterrupted", source.get(), target.get());
    if (source)
    {
        source->CallEvent<OnInterruptedSkillSignature>(OnInterruptedSkillEvent, this);
        source->CallEvent<OnEndUseSkillSignature>(OnEndUseSkillEvent, target.get(), this);
    }
    startUse_ = 0;
    EndUse();
    // rechargedAt_ remains
    SetRecharged(sa::time::tick() + CalculateRecharge(recharge_));
    return true;
}

void Skill::SetRecharged(int64_t ticks)
{
    if (rechargedAt_ == ticks)
        return;

    rechargedAt_ = ticks;
    rechargedFired_ = false;
}

void Skill::FireOnRecharged()
{
    if (onRecharged_)
        onRecharged_(*this);
    rechargedFired_ = true;
}

void Skill::EndUse()
{
    startUse_ = 0;

    auto source = source_.lock();
    auto target = target_.lock();

    lua_.Call("onEndUse", source.get(), target.get());
    source_.reset();
    target_.reset();

    rechargedFired_ = false;
}

float Skill::CalculateCost(const std::function<float(CostType)>& importanceCallback) const
{
    float result = 0.0f;
    int c = 0;
    if (activation_ > 0)
    {
        result += importanceCallback(CostType::Activation) * activation_;
        ++c;
    }
    if (energy_ > 0)
    {
        result += importanceCallback(CostType::Energy) * energy_;
        ++c;
    }
    if (adrenaline_ > 0)
    {
        result += importanceCallback(CostType::Adrenaline) * adrenaline_;
        ++c;
    }
    if (hp_ > 0)
    {
        result += importanceCallback(CostType::HpSacrify) * hp_;
        ++c;
    }
    if (overcast_ > 0)
    {
        result += importanceCallback(CostType::HpSacrify) * overcast_;
        ++c;
    }
    // When this skill doesn't recharge, don't add it to skill costs, the AI would never use it
    if (recharge_ > 0 && recharge_ != (uint32_t)-1)
    {
        result += importanceCallback(CostType::Recharge) * recharge_;
        ++c;
    }

    return result / static_cast<float>(c);
}

bool Skill::IsInRange(const Actor* target) const
{
    if (!target)
        return false;
    if (auto s = source_.lock())
    {
        if (s->id_ == target->id_)
            // Self is always in range
            return true;
        return s->IsInRange(range_, target);
    }
    return false;
}

Actor* Skill::GetSource() const
{
    if (auto s = source_.lock())
        return s.get();
    return nullptr;
}

Actor* Skill::GetTarget() const
{
    if (auto t = target_.lock())
        return t.get();
    return nullptr;
}

void Skill::AddRecharge(int32_t ms)
{
    if (ms > 0)
    {
        SetRecharged(sa::ClampedAdd(rechargedAt_, static_cast<int64_t>(ms)));
        return;
    }

    const uint32_t absms = static_cast<uint32_t>(abs(ms));
    if (absms < rechargedAt_)
        SetRecharged(rechargedAt_ - absms);
    else
        SetRecharged(0);
}

bool Skill::CanUseOnTarget(const Actor& source, const Actor* target) const
{
    switch (targetType_)
    {
    case SkillTargetTypeNone:
        return true;
    case SkillTargetTypeAny:
        return target;
    case SkillTargetTypeFoe:
        return source.IsEnemy(target);
    case SkillTargetTypeSelf:
        return target && source.id_ == target->id_;
    case SkillTargetTypeAllyAndSelf:
        if (!target)
            return false;
        return source.IsAlly(target) || source.id_ == target->id_;
    case SkillTargetTypeAllyWithoutSelf:
        if (!target)
            return false;
        return source.IsAlly(target) && source.id_ != target->id_;
    }
    return false;
}

}
