/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Actor.h"
#include "AiComp.h"
#include "Chat.h"
#include "SkillDefs.h"
#include <eastl.hpp>
#include <sa/Bits.h>
#include <set>
#include "LuaObject.h"

namespace Game {

class Map;

namespace Components {
class TriggerComp;
class WanderComp;
}

class Npc final : public Actor
{
private:
    LuaObject lua_;
    /// This NPC exists only on the server, i.e. is not spawned on the client, e.g. a trigger box.
    bool serverOnly_{ false };
    uint32_t level_{ 1 };
    uint32_t xp_{ 0 };
    uint32_t skillPoints_{ 0 };
    uint32_t itemIndex_{ 0 };
    AB::Entities::CharacterSex sex_{ AB::Entities::CharacterSex::Unknown };
    // An animal that can become a pet
    bool tameable_{ false };
    Range interactionRange_{ Range::Adjecent };
    /// Quests this NPC may have for the player
    ea::set<uint32_t> quests_;
    ea::set<AB::Entities::ItemType> sellItemTypes_;
    std::string scriptFile_;
    std::string GetQuote(int index);
    void _LuaAddWanderPoint(const Math::StdVector3& point);
    void _LuaAddWanderPoints(const std::vector<Math::StdVector3>& points);
    void _LuaAddQuest(uint32_t index);
    void _LuaSetNpcType(int type);
    int _LuaGetNpcType() const;
    int _LuaGetCombatMode() const { return static_cast<int>(combatMode_); }
    void _LuaSetCombatMode(int value);
    std::string _LuaGetScriptFile() const;
    bool LoadCompationInfo(CompanioInfo& info) override;
private:
    // Events
    void OnArrived();
    void OnInterruptedAttack();
    void OnInterruptedSkill(Skill* skill);
    void OnKnockedDown(uint32_t time);
    void OnHealed(int hp);
    void OnResurrected(int health, int energy);
    void OnClicked(Actor* selector);
    void OnCollide(GameObject* other);
    void OnSelected(Actor* selector);
    void OnTrigger(GameObject* other);
    void OnLeftArea(GameObject* other);
    void OnDied(Actor*, Actor*);
    void OnEndUseSkill(Actor* target, Skill* skill);
    void OnStartUseSkill(Actor* target, Skill* skill);
    void OnSourceAttackStart(Actor* target, bool& canAttack);
    void OnSourceAttackSuccess(Actor* target, DamageType type, int32_t damage);
    void OnTargetAttackStart(Actor* source, bool& canGetAttacked);
    void OnTargetAttackEnd(Actor* source, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error);
    void OnInterruptingAttack(bool& success);
    void OnInterruptingSkill(Actor* source, AB::Entities::SkillType type, Skill* skill, bool& success);
    void OnCanUseSkill(Actor* target, Skill* skill, bool& success);
    void OnGettingSkillTarget(Actor* source, Skill* skill, bool& succcess);
    void OnInteract(Actor* actor);
    void OnActorDied(Actor* actor, Actor* killer) override;
    void OnActorResurrected(Actor* actor) override;
    void OnMasterGettingDamage(Actor* source, Skill*, DamageType, int32_t& value) override;
    void OnMasterAttackTarget(Actor* target, bool&) override;

    void ObjectEnteredRange(Range range, uint32_t objectId) override;
    void ObjectLeftRange(Range range, uint32_t objectId) override;
public:
    static void RegisterLua(kaguya::State& state);
    enum class CombatMode
    {
        Fight,
        Guard,
        AvoidCombat
    };

    Npc();
    ~Npc() override;

    bool LoadScript(const std::string& fileName);
    AB::GameProtocol::GameObjectType GetType() const override
    {
        return AB::GameProtocol::GameObjectType::Npc;
    }

    void Update(uint32_t timeElapsed, MessageStream& message) override;

    uint32_t GetLevel() const override { return level_; }
    bool CanAttack() const override { return true; }
    bool CanUseSkill() const override { return true; }
    void SetLevel(uint32_t value) override;
    void AddXp(int value) override;
    uint32_t GetXp() const override { return xp_; }
    void SetXp(uint32_t value) { xp_ = value; }
    void AddSkillPoint() override;
    uint32_t GetSkillPoints() const override { return skillPoints_; }
    void SetSkillPoints(uint32_t value) { skillPoints_ = value; }
    void AdvanceLevel() override;
    CompanioInfo GetCompanionInfo() const;

    uint32_t GetItemIndex() const override
    {
        return itemIndex_;
    }
    // Can only be called during onInit()
    void SetItemIndex(uint32_t value) { itemIndex_ = value; }
    AB::Entities::CharacterSex GetSex() const override
    {
        return sex_;
    }
    Range GetInteractionRange() const { return interactionRange_; }
    bool SetBehavior(const std::string& name);
    float GetAggro(const Actor* other);
    int GetBestSkillIndex(SkillEffect effect, SkillEffectTarget target,
        AB::Entities::SkillType interrupts = AB::Entities::SkillTypeAll,
        const Actor* targetActor = nullptr);
    bool GetSkillCandidates(
        ea::vector<int>& results,
        SkillEffect effect, SkillEffectTarget target,
        AB::Entities::SkillType interrupts = AB::Entities::SkillTypeAll,
        const Actor* targetActor = nullptr);
    bool IsServerOnly() const { return serverOnly_; }
    void SetServerOnly(bool value) { serverOnly_ = value; }
    bool IsWander() const;
    void SetWander(bool value);
    uint32_t GetAttackSpeed() override;
    DamageType GetAttackDamageType() override;
    int32_t GetAttackDamage(bool critical) override;
    float GetArmorEffect(DamageType damageType, DamagePos pos, float penetration) override;
    int GetBaseArmor(DamageType damageType, DamagePos pos) override;

    void WriteSpawnData(MessageStream& msg) override;

    void Say(ChatType channel, const std::string& message);
    bool SayQuote(ChatType channel, int index);
    void Whisper(Player* player, const std::string& message);
    /// Shooting a projectile without having a weapon that can spawn projectiles
    void ShootAt(const std::string& itemUuid, Actor* target);
    ea::set<uint32_t> GetQuestsForPlayer(const Player& player) const;
    bool HaveQuestsForPlayer(const Player& player) const;
    bool IsSellingItemType(AB::Entities::ItemType type) const;
    bool IsSellingItem(uint32_t itemIndex);
    bool IsTameable() const { return tameable_; }
    void SetTameable(bool value) { tameable_ = value; }

    ea::unique_ptr<Components::AiComp> aiComp_;
    ea::unique_ptr<Components::WanderComp> wanderComp_;
    CombatMode combatMode_{ CombatMode::Guard };
};

template <>
inline bool Is<Npc>(const GameObject& obj)
{
    return obj.GetType() == AB::GameProtocol::GameObjectType::Npc;
}

}
