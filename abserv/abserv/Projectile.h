/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Actor.h"
#include <libcommon/Utils.h>
#include <sa/Bits.h>
#include <eastl.hpp>
#include <sa/time.h>
#include "LuaObject.h"

namespace Game {

class Item;

/// Projectile is an Actor and has an Item. Both have a script. This is similar to ItemDrop:
/// it is a GameObject and wraps an Item.
class Projectile final : public Actor
{
private:
    static const uint32_t DEFAULT_LIFETIME = 5000;
    ea::unique_ptr<Item> item_;
    LuaObject lua_;
    bool startSet_{ false };
    Math::Vector3 startPos_;
    Math::Vector3 targetPos_;
    uint32_t targetMoveDir_{ AB::GameProtocol::MoveDirectionNone };
    float startDistance_{ std::numeric_limits<float>::max() };
    float minDistance_{ std::numeric_limits<float>::max() };
    ea::weak_ptr<Actor> source_;
    ea::weak_ptr<Actor> target_;
    bool started_{ false };
    uint32_t itemIndex_{ 0 };
    std::string itemUuid_;
    int64_t startTick_{ 0 };
    uint32_t lifeTime_{ DEFAULT_LIFETIME };
    AB::GameProtocol::AttackError error_{ AB::GameProtocol::AttackError::None };
    bool hitted_{ false };
    bool LoadScript(const std::string& fileName);
    void SetError(AB::GameProtocol::AttackError error);
    Actor* _LuaGetSource();
    Actor* _LuaGetTarget();
    bool DoStart();
    bool IsExpired() const
    {
        if (startTick_ == 0)
            return false;
        return sa::time::is_expired(startTick_ + lifeTime_);
    }
private:
    void OnCollide(GameObject* other);
public:
    static void RegisterLua(kaguya::State& state);

    explicit Projectile(const std::string& itemUuid);
    ~Projectile() override;

    bool Load();
    AB::GameProtocol::GameObjectType GetType() const override
    {
        return AB::GameProtocol::GameObjectType::Projectile;
    }
    void SetSource(ea::shared_ptr<Actor> source);
    ea::shared_ptr<Actor> GetSource() const { return source_.lock(); }
    void SetTarget(ea::shared_ptr<Actor> target);
    ea::shared_ptr<Actor> GetTarget() const { return target_.lock(); }
    void Update(uint32_t timeElapsed, MessageStream& message) override;

    uint32_t GetItemIndex() const override { return itemIndex_; }
    uint32_t GetLevel() const override;
    void SetLifeTime(uint32_t value) { lifeTime_ = value; }
    uint32_t GetLifeTime() const { return lifeTime_; }
};

template <>
inline bool Is<Projectile>(const GameObject& obj)
{
    return obj.GetType() == AB::GameProtocol::GameObjectType::Projectile;
}

}
