/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/MailList.h>
#include <AB/Entities/Mail.h>
#include <optional>

namespace IO {

bool LoadMailList(AB::Entities::MailList& ml, const std::string& accountUuid);
std::optional<std::string> SendMailToPlayer(const std::string& playerName, const std::string& fromAcc,
    const std::string& fromName,
    const std::string& subject, const std::string& message, const std::vector<std::string>& attachments);
std::optional<std::string> SendMailToAccount(const std::string& accountUuid, const std::string& fromAcc,
    const std::string& fromName, const std::string& toName,
    const std::string& subject, const std::string& message, const std::vector<std::string>& attachments);
bool ReadMail(AB::Entities::Mail& mail);
bool DeleteMail(AB::Entities::Mail& mail);

}
