/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProtocolGame.h"
#include "ConfigManager.h"
#include "Game.h"
#include "GameManager.h"
#include <libaccount/Account.h>
#include "IOGame.h"
#include "IOPlayer.h"
#include "Player.h"
#include "PlayerManager.h"
#include <AB/DHKeys.hpp>
#include <AB/Entities/FriendList.h>
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <libcommon/BanManager.h>
#include <libcommon/StringUtils.h>
#include <libcommon/OutputMessage.h>
#include <libcommon/Scheduler.h>
#include <sa/time.h>

//#define DEBUG_NET

namespace Net {

std::string ProtocolGame::serverId_ = Utils::Uuid::EMPTY_UUID;

ProtocolGame::ProtocolGame(std::shared_ptr<Connection> connection) :
    Protocol(std::forward<std::shared_ptr<Connection>>(connection))
{
    encryptionEnabled_ = ENABLE_GAME_ENCRYTION;
    SetEncKey(AB::ENC_KEY);
}

ProtocolGame::~ProtocolGame() = default;

inline void ProtocolGame::AddPlayerInput(Game::InputType type, Utils::VariantMap&& data)
{
    if (auto p = GetPlayer())
        p->AddInput(type, std::forward<Utils::VariantMap>(data));
}

inline void ProtocolGame::AddPlayerInput(Game::InputType type)
{
    if (auto p = GetPlayer())
        p->AddInput(type);
}

void ProtocolGame::Login(AB::Packets::Client::GameLogin packet)
{
    // Pass packet by value because it's called by the Scheduler
#ifdef DEBUG_NET
    ASSERT(GetSubsystem<Asynch::Dispatcher>()->IsDispatcherThread());
#endif
#ifdef DEBUG_NET
    LOG_DEBUG << "Player " << packet.charUuid << " logging in" << std::endl;
#endif

    auto* playerMan = GetSubsystem<Game::PlayerManager>();
    {
        ea::shared_ptr<Game::Player> foundPlayer = playerMan->GetPlayerByUuid(packet.charUuid);
        if (foundPlayer)
        {
            // In rare cases it can happen that the player was not logged out correctly when changing instances.
            // Maybe the disconnect/connect process could need some redesign to make it less racy,
            // but it is complex and works mostly.
            foundPlayer->Logout(false);
        }
    }

    if (Auth::BanManager::IsAccountBanned(uuids::uuid(packet.accountUuid)))
    {
        LOG_INFO << "Login attempt from banned account " << packet.accountUuid << std::endl;
        DelayedDisconnectClient(AB::ErrorCodes::AccountBanned);
        return;
    }

    ea::shared_ptr<Game::Player> player = playerMan->CreatePlayer(GetPtr());
    ASSERT(player);

    // Load player and account data from DB
    if (!IO::LoadPlayerByUuid(*player, packet.charUuid))
    {
        LOG_ERROR << "Error loading player " << packet.charUuid << std::endl;
        DisconnectClient(AB::ErrorCodes::ErrorLoadingCharacter);
        return;
    }
    // Account and player is loaded create the index for the player
    playerMan->UpdatePlayerIndex(*player);

    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    // Check if game exists.
    AB::Entities::Game g;
    g.uuid = packet.mapUuid;
    if (!client->Read(g))
    {
        LOG_ERROR << "Invalid game with map " << packet.mapUuid << std::endl;
        DisconnectClient(AB::ErrorCodes::InvalidGame);
        return;
    }

    player->account_.onlineStatus = AB::Entities::OnlineStatus::OnlineStatusOnline;
    player->account_.currentCharacterUuid = player->data_.uuid;
    player->account_.currentServerUuid = ProtocolGame::serverId_;
    client->Update(player->account_);

    player->Initialize();
    player->data_.currentMapUuid = packet.mapUuid;
    player->data_.lastLogin = sa::time::tick();
    if (!uuids::uuid(packet.instanceUuid).nil())
    {
        player->data_.instanceUuid = packet.instanceUuid;
#ifdef DEBUG_NET
        LOG_DEBUG << "Player entering instance " << packet.instanceUuid << std::endl;
#endif
    }
    client->Update(player->data_);
    OutputMessagePool::Instance()->AddToAutoSend(shared_from_this());
    LOG_INFO << "User " << player->account_.name << " logged in with " << player->data_.name << " entering " << g.name << std::endl;
    player_ = player;
#ifdef DEBUG_NET
    LOG_DEBUG << "Connecting player " << (player ? player->account_.name : "(null)") << std::endl;
#endif
    Connect();
#ifdef DEBUG_NET
    LOG_DEBUG << "Player " << (player ? player->account_.name : "(null)") << " connected" << std::endl;
#endif
    connected_ = true;
}

void ProtocolGame::Logout()
{
    std::scoped_lock lock(lock_);
    auto player = GetPlayer();
#ifdef DEBUG_NET
    LOG_DEBUG << "Logging out user " << (player ? player->account_.name : "(null)") << std::endl;
#endif

    if (!player)
    {
        // If the player is null this was already called by ProtocolGame::Release(), because you never
        // know what happens first client or server disconnect.
        return;
    }
    player_.reset();
    player->logoutTime_ = sa::time::tick();
    player->data_.instanceUuid = Utils::Uuid::EMPTY_UUID;
    Account::AccountLogout(player->data_.accountUuid);
    IO::SavePlayer(*player);
    GetSubsystem<Game::PlayerManager>()->RemovePlayer(player->id_);

    OutputMessagePool::Instance()->RemoveFromAutoSend(shared_from_this());
    Disconnect();

    LOG_INFO << "User " << player->account_.name << " logged out" << std::endl;
}

void ProtocolGame::ParsePacket(NetworkMessage& message)
{
    auto* gameMan = GetSubsystem<Game::GameManager>();
    if (!acceptPackets_ ||
        gameMan->GetState() != Game::GameManager::State::Running ||
        message.IsEmpty())
    {
        if (!acceptPackets_)
            LOG_ERROR << "Not accepting packets at this time" << std::endl;
        if (gameMan->GetState() != Game::GameManager::State::Running)
            LOG_ERROR << "GameManager state is not running, it is " << static_cast<int>(gameMan->GetState()) << std::endl;
        return;
    }

    using namespace AB::GameProtocol;

    const ClientPacketTypes recvByte = static_cast<ClientPacketTypes>(message.GetByte());

    switch (recvByte)
    {
    case ClientPacketTypes::Ping:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::Ping>(message);
        AddPlayerTask(&Game::Player::CRQPing, packet.tick);
        break;
    }
    case ClientPacketTypes::Logout:
    {
        // Don't add a task to logout, because when changing the map the client may be more quickly reconnect
        // before it was logged out.
        if (auto player = GetPlayer())
            player->CRQLogout();
        break;
    }
    case ClientPacketTypes::ChangeMap:
    {
        // Called by the client when clicking on the map
        const auto packet = AB::Packets::Get<AB::Packets::Client::ChangeMap>(message);
        AddPlayerTask(&Game::Player::CRQChangeMap, packet.mapUuid);
        break;
    }
    case ClientPacketTypes::SendMail:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SendMail>(message);
        AddPlayerTask(&Game::Player::CRQSendMail, packet.recipient, packet.subject, packet.body, packet.attachments);
        break;
    }
    case ClientPacketTypes::GetMailHeaders:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::GetMailHeaders>(message);
        AddPlayerTask(&Game::Player::CRQGetMailHeaders);
        break;
    }
    case ClientPacketTypes::GetMail:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetMail>(message);
        AddPlayerTask(&Game::Player::CRQGetMail, packet.mailUuid);
        break;
    }
    case ClientPacketTypes::DeleteMail:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::DeleteMail>(message);
        AddPlayerTask(&Game::Player::CRQDeleteMail, packet.mailUuid);
        break;
    }
    case ClientPacketTypes::GetInventory:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::GetInventory>(message);
        AddPlayerTask(&Game::Player::CRQGetInventory);
        break;
    }
    case ClientPacketTypes::InventoryDestroyItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::InventoryDestroyItem>(message);
        AddPlayerTask(&Game::Player::CRQDestroyInventoryItem, packet.pos);
        break;
    }
    case ClientPacketTypes::InventoryDropItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::InventoryDropItem>(message);
        AddPlayerTask(&Game::Player::CRQDropInventoryItem, packet.pos, packet.count);
        break;
    }
    case ClientPacketTypes::SetItemPos:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetItemPos>(message);
        AddPlayerTask(&Game::Player::CRQSetItemPos,
            static_cast<AB::Entities::StoragePlace>(packet.currentPlace),
            packet.currentPos,
            static_cast<AB::Entities::StoragePlace>(packet.soragePlace),
            packet.storagePos,
            packet.count);
        break;
    }
    case ClientPacketTypes::TakeMailItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::TakeMailItem>(message);
        AddPlayerTask(&Game::Player::CRQTakeMailAttachment, packet.mailUuid, packet.pos);
        break;
    }
    case ClientPacketTypes::WithdrawMoney:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::DespositWithdrawMoney>(message);
        AddPlayerTask(&Game::Player::CRQWithdrawMoney, packet.amount);
        break;
    }
    case ClientPacketTypes::DepositMoney:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::DespositWithdrawMoney>(message);
        AddPlayerTask(&Game::Player::CRQDepositMoney, packet.amount);
        break;
    }
    case ClientPacketTypes::SellItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SellItem>(message);
        AddPlayerTask(&Game::Player::CRQSellItem, packet.npcId, packet.pos, packet.count);
        break;
    }
    case ClientPacketTypes::BuyItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::BuyItem>(message);
        AddPlayerTask(&Game::Player::CRQBuyItem, packet.npcId, packet.id, packet.count);
        break;
    }
    case ClientPacketTypes::GetItemsPrice:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetItemPrice>(message);
        AddPlayerTask(&Game::Player::CRQGetItemPrice, packet.items);
        break;
    }
    case ClientPacketTypes::GetMerchantItems:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetMerchantItems>(message);
        AddPlayerTask(&Game::Player::CRQGetMerchantItems, packet.npcId,
            static_cast<AB::Entities::ItemType>(packet.itemType),
            packet.searchName, packet.page);
        break;
    }
    case ClientPacketTypes::GetCraftsmanItems:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetCraftsmanItems>(message);
        AddPlayerTask(&Game::Player::CRQGetCraftsmanItems, packet.npcId,
            static_cast<AB::Entities::ItemType>(packet.itemType),
            packet.searchName, packet.page);
        break;
    }
    case ClientPacketTypes::CraftItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::CraftItem>(message);
        AddPlayerTask(&Game::Player::CRQCraftItem, packet.npcId, packet.index, packet.count, packet.attribute);
        break;
    }
    case ClientPacketTypes::SalvageItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SalageItem>(message);
        AddPlayerTask(&Game::Player::CRQSalvageItem, packet.kitPos, packet.pos);
        break;
    }
    case ClientPacketTypes::ItemDoubleClick:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::ItemDoubleClick>(message);
        AddPlayerTask(&Game::Player::CRQItemDoubleClick, static_cast<AB::Entities::StoragePlace>(packet.storagePlace), packet.storagePos);
        break;
    }
    case ClientPacketTypes::GetChest:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::GetChest>(message);
        AddPlayerTask(&Game::Player::CRQGetChest);
        break;
    }
    case ClientPacketTypes::ChestDestroyItem:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::ChestDestroyItem>(message);
        AddPlayerTask(&Game::Player::CRQDestroyChestItem, packet.pos);
        break;
    }
    case ClientPacketTypes::PartyInvitePlayer:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PartyInvitePlayer>(message);
        AddPlayerTask(&Game::Player::CRQPartyInvitePlayer, packet.targetId);
        break;
    }
    case ClientPacketTypes::PartyKickPlayer:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PartyKickPlayer>(message);
        AddPlayerTask(&Game::Player::CRQPartyKickPlayer, packet.targetId);
        break;
    }
    case ClientPacketTypes::PartyLeave:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::PartyLeave>(message);
        AddPlayerTask(&Game::Player::CRQPartyLeave);
        break;
    }
    case ClientPacketTypes::PartyAcceptInvite:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PartyAcceptInvite>(message);
        AddPlayerTask(&Game::Player::CRQPartyAccept, packet.inviterId);
        break;
    }
    case ClientPacketTypes::PartyRejectInvite:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PartyRejectInvite>(message);
        AddPlayerTask(&Game::Player::CRQPartyRejectInvite, packet.inviterId);
        break;
    }
    case ClientPacketTypes::GetPartyMembers:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PartyGetMembers>(message);
        AddPlayerTask(&Game::Player::CRQPartyGetMembers, packet.partyId);
        break;
    }
    case ClientPacketTypes::Move:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::Move>(message);
        Utils::VariantMap data;
        data[Game::InputDataDirection] = packet.direction;
        AddPlayerInput(Game::InputType::Move, std::move(data));
        break;
    }
    case ClientPacketTypes::Turn:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::Turn>(message);
        Utils::VariantMap data;
        data[Game::InputDataDirection] = packet.direction;   // None | Left | Right
        AddPlayerInput(Game::InputType::Turn, std::move(data));
        break;
    }
    case ClientPacketTypes::SetDirection:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetDirection>(message);
        Utils::VariantMap data;
        data[Game::InputDataDirection] = packet.rad;   // World angle Rad
        AddPlayerInput(Game::InputType::Direction, std::move(data));
        break;
    }
    case ClientPacketTypes::SetState:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetPlayerState>(message);
        Utils::VariantMap data;
        data[Game::InputDataState] = packet.newState;
        AddPlayerInput(Game::InputType::SetState, std::move(data));
        break;
    }
    case ClientPacketTypes::Goto:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GotoPos>(message);
        Utils::VariantMap data;
        data[Game::InputDataVertexX] = packet.pos[0];
        data[Game::InputDataVertexY] = packet.pos[1];
        data[Game::InputDataVertexZ] = packet.pos[2];
        AddPlayerInput(Game::InputType::Goto, std::move(data));
        break;
    }
    case ClientPacketTypes::PingPos:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PingPos>(message);
        Utils::VariantMap data;
        data[Game::InputDataVertexX] = packet.pos[0];
        data[Game::InputDataVertexY] = packet.pos[1];
        data[Game::InputDataVertexZ] = packet.pos[2];
        AddPlayerInput(Game::InputType::PingPos, std::move(data));
        break;
    }
    case ClientPacketTypes::PingInfo:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::PingInfo>(message);
        Utils::VariantMap data;
        data[Game::InputTypePingInfoType] = (int)packet.type;
        data[Game::InputTypePingInfoData] = packet.data;
        data[Game::InputTypePingInfoData2] = packet.data2;
        AddPlayerInput(Game::InputType::PingInfo, std::move(data));
        break;
    }
    case ClientPacketTypes::UseSkill:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::UseSkill>(message);
        // 1 based -> convert to 0 based
        if (packet.index > 0 && packet.index <= Game::PLAYER_MAX_SKILLS)
        {
            Utils::VariantMap data;
            data[Game::InputDataSkillIndex] = packet.index - 1;
            data[Game::InputDataSuppress] = packet.supress;
            data[Game::InputDataPingTarget] = packet.ping;
            AddPlayerInput(Game::InputType::UseSkill, std::move(data));
        }
        break;
    }
    case ClientPacketTypes::RemoveMaintainableEnch:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::RemoveMaintainableEnch>(message);
        Utils::VariantMap data;
        data[Game::InputDataSkillIndex] = packet.index;
        data[Game::InputDataObjectId] = packet.targetId;
        AddPlayerInput(Game::InputType::RemoveMaintainableEnch, std::move(data));
        break;
    }
    case ClientPacketTypes::Cancel:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::Cancel>(message);
        AddPlayerInput(Game::InputType::Cancel);
        break;
    }
    case ClientPacketTypes::Interact:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::Interact>(message);
        Utils::VariantMap data;
        data[Game::InputDataSuppress] = packet.suppress;
        data[Game::InputDataPingTarget] = packet.ping;
        AddPlayerInput(Game::InputType::Interact, std::move(data));
        break;
    }
    case ClientPacketTypes::Select:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SelectObject>(message);
        Utils::VariantMap data;
        data[Game::InputDataObjectId] = packet.sourceId;    // Source
        data[Game::InputDataObjectId2] = packet.targetId;   // Target
        AddPlayerInput(Game::InputType::Select, std::move(data));
        break;
    }
    case ClientPacketTypes::ClickObject:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::ClickObject>(message);
        Utils::VariantMap data;
        data[Game::InputDataObjectId] = packet.sourceId;    // Source
        data[Game::InputDataObjectId2] = packet.targetId;   // Target
        AddPlayerInput(Game::InputType::ClickObject, std::move(data));
        break;
    }
    case ClientPacketTypes::Queue:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::QueueMatch>(message);
        AddPlayerTask(&Game::Player::CRQQueueForMatch);
        break;
    }
    case ClientPacketTypes::Unqueue:
    {
        [[maybe_unused]] const auto packet =  AB::Packets::Get<AB::Packets::Client::UnqueueMatch>(message);
        AddPlayerTask(&Game::Player::CRQUnqueueForMatch);
        break;
    }
    case ClientPacketTypes::TradeRequest:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::TradeRequest>(message);
        AddPlayerTask(&Game::Player::CRQTradeRequest, packet.targetId);
        break;
    }
    case ClientPacketTypes::TradeCancel:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::TradeCancel>(message);
        AddPlayerTask(&Game::Player::CRQTradeCancel);
        break;
    }
    case ClientPacketTypes::TradeOffer:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::TradeOffer>(message);
        AddPlayerTask(&Game::Player::CRQTradeOffer, packet.money, packet.items);
        break;
    }
    case ClientPacketTypes::TradeAccept:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::TradeAccept>(message);
        AddPlayerTask(&Game::Player::CRQTradeAccept);
        break;
    }
    case ClientPacketTypes::Command:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::Command>(message);
        Utils::VariantMap data;
        data[Game::InputDataCommandType] = packet.type;
        data[Game::InputDataCommandData] = packet.data;
        AddPlayerInput(Game::InputType::Command, std::move(data));
        break;
    }
    case ClientPacketTypes::GetFriendList:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::UpdateFriendList>(message);
        AddPlayerTask(&Game::Player::CRQGetFriendList);
        break;
    }
    case ClientPacketTypes::AddFriend:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::AddFriend>(message);
        const AB::Entities::FriendRelation rel = static_cast<AB::Entities::FriendRelation>(packet.relation);
        AddPlayerTask(&Game::Player::CRQAddFriend, packet.name, rel);
        break;
    }
    case ClientPacketTypes::RemoveFriend:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::RemoveFriend>(message);
        AddPlayerTask(&Game::Player::CRQRemoveFriend, packet.accountUuid);
        break;
    }
    case ClientPacketTypes::RenameFriend:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::RenameFriend>(message);
        AddPlayerTask(&Game::Player::CRQChangeFriendNick, packet.accountUuid, packet.newName);
        break;
    }
    case ClientPacketTypes::GetPlayerInfoByAccount:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetPlayerInfoByAccount>(message);
        AddPlayerTask(&Game::Player::CRQGetPlayerInfoByAccount, packet.accountUuid, packet.fields);
        break;
    }
    case ClientPacketTypes::GetPlayerInfoByName:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::GetPlayerInfoByName>(message);
        AddPlayerTask(&Game::Player::CRQGetPlayerInfoByName, packet.name, packet.fields);
        break;
    }
    case ClientPacketTypes::GetGuildInfo:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::GuildInfo>(message);
        AddPlayerTask(&Game::Player::CRQGetGuildInfo);
        break;
    }
    case ClientPacketTypes::GetGuildMembers:
    {
        [[maybe_unused]] const auto packet = AB::Packets::Get<AB::Packets::Client::GuildMembers>(message);
        AddPlayerTask(&Game::Player::CRQGetGuildMembers);
        break;
    }
    case ClientPacketTypes::SetOnlineStatus:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetOnlineStatus>(message);
        const AB::Entities::OnlineStatus status = static_cast<AB::Entities::OnlineStatus>(packet.newStatus);
        AddPlayerTask(&Game::Player::CRQSetOnlineStatus, status);
        break;
    }
    case ClientPacketTypes::SetSecondaryProfession:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetSecondaryProfession>(message);
        AddPlayerTask(&Game::Player::CRQSetSecondaryProfession, packet.profIndex);
        break;
    }
    case ClientPacketTypes::SetAttributeValue:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::SetAttributeValue>(message);
        AddPlayerTask(&Game::Player::CRQSetAttributeValue, packet.attribIndex, packet.value);
        break;
    }
    case ClientPacketTypes::EquipSkill:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::EquipSkill>(message);
        AddPlayerTask(&Game::Player::CRQEquipSkill, packet.skillIndex, packet.pos);
        break;
    }
    case ClientPacketTypes::LoadSkillTemplate:
    {
        const auto packet = AB::Packets::Get<AB::Packets::Client::LoadSkillTemplate>(message);
        AddPlayerTask(&Game::Player::CRQLoadSkillTemplate, packet.templ);
        break;
    }
    default:
    {
        auto player = GetPlayer();
        LOG_ERROR << Utils::ConvertIPToString(GetIP()) << ": Player " << (player ? player->GetName() : "null") <<
            " sent an unknown packet header: 0x" <<
            std::hex << static_cast<uint16_t>(recvByte) << std::dec << std::endl;
        break;
    }
    }
}

void ProtocolGame::OnRecvFirstMessage(NetworkMessage& msg)
{
#ifdef DEBUG_NET
    LOG_DEBUG << "Receiving first message" << std::endl;
#endif
    if (encryptionEnabled_)
    {
        if (!XTEADecrypt(msg))
        {
#ifdef DEBUG_NET
            LOG_ERROR << "Failed to decrypt message" << std::endl;
#endif
            Disconnect();
            return;
        }
    }

    auto packet = AB::Packets::Get<AB::Packets::Client::GameLogin>(msg);
    if (packet.protocolVersion != AB::PROTOCOL_VERSION)
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Wrong protocol version" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::WrongProtocolVersion);
        return;
    }
    if (Utils::Uuid::IsEmpty(packet.accountUuid))
    {
        LOG_ERROR << "Invalid account " << packet.accountUuid << std::endl;
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }

    if (!Account::GameWorldAuth(packet.accountUuid, packet.authToken, packet.charUuid))
    {
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    for (int i = 0; i < DH_KEY_LENGTH; ++i)
        clientKey_[i] = packet.key[i];
    auto* keys = GetSubsystem<Crypto::DHKeys>();
    // Switch now to the shared key
    keys->GetSharedKey(clientKey_, encKey_);

    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(
            std::bind(&ProtocolGame::Login, GetPtr(), std::move(packet))
        )
    );
}

void ProtocolGame::OnConnect()
{
    if (acceptPackets_)
    {
        LOG_ERROR << "Not accepting packets at this time. This really shouldn't happen!" << std::endl;
        return;
    }

    // Dispatcher thread
#ifdef DEBUG_NET
    LOG_DEBUG << "Sending KeyExchange" << std::endl;
#endif
    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::GameProtocol::ServerPacketType::KeyExchange);
    AB::Packets::Server::KeyExchange packet = {};
    const auto& key = GetSubsystem<Crypto::DHKeys>()->GetPublickKey();
    for (size_t i = 0; i < DH_KEY_LENGTH; ++i)
        packet.key[i] = key[i];
    AB::Packets::Add(packet, *output);
    Send(std::move(output));
}

void ProtocolGame::DisconnectClient(AB::ErrorCodes error)
{
    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::GameProtocol::ServerPacketType::ProtocolError);
    AB::Packets::Server::ProtocolError packet = { static_cast<uint8_t>(error) };
    AB::Packets::Add(packet, *output);
    Send(std::move(output));
    Disconnect();
}

void ProtocolGame::DelayedDisconnectClient(AB::ErrorCodes error)
{
    auto self = GetPtr();
    GetSubsystem<Asynch::Scheduler>()->Add(Asynch::CreateScheduledTask(2000, [self, error]()
    {
        self->DisconnectClient(error);
    }));
}

void ProtocolGame::Connect()
{
    // Dispatcher thread
#ifdef DEBUG_NET
    LOG_DEBUG << "Connecting..." << std::endl;
#endif
    if (IsConnectionExpired())
    {
        // This can happen when Connection::ParseHeader() gets an error,
        // e.g. when the client closes the connection. But why is it doing this???
        LOG_ERROR << "Connection expired" << std::endl;
        if (auto player = GetPlayer())
        {
            LOG_INFO << "Logging out player " << player->GetName() << std::endl;
            player->Logout(true);
        }
        else
            LOG_INFO << "Player also expired" << std::endl;

        return;
    }

    auto player = GetPlayer();
    if (!player)
    {
        LOG_ERROR << "Player expired" << std::endl;
        return;
    }

    player->loginTime_ = sa::time::tick();

    acceptPackets_ = true;

    EnterGame(player);
}

void ProtocolGame::WriteToOutput(const NetworkMessage& message)
{
#ifdef DEBUG_NET
    ASSERT(GetSubsystem<Asynch::Dispatcher>()->IsDispatcherThread());
#endif
    GetOutputBuffer(message.GetSize())->Append(message);
}

void ProtocolGame::EnterGame(ea::shared_ptr<Game::Player> player)
{
#ifdef DEBUG_NET
    ASSERT(GetSubsystem<Asynch::Dispatcher>()->IsDispatcherThread());
#endif
    if (!player)
    {
        LOG_ERROR << "player is null" << std::endl;
        DisconnectClient(AB::ErrorCodes::CannotEnterGame);
        return;
    }

    auto* gameMan = GetSubsystem<Game::GameManager>();
    ea::shared_ptr<Game::Game> instance;
    if (!Utils::Uuid::IsEmpty(player->data_.instanceUuid))
    {
#ifdef DEBUG_NET
        LOG_DEBUG << "Entering existing instance " << player->data_.instanceUuid << std::endl;
#endif
        // Enter an existing instance
        instance = gameMan->GetInstance(player->data_.instanceUuid);
        if (!instance)
            LOG_ERROR << "Game instance not found " << player->data_.instanceUuid << std::endl;
    }
    else
    {
#ifdef DEBUG_NET
        LOG_DEBUG << "Getting instance of map " << player->data_.currentMapUuid << std::endl;
#endif
        // Create new instance
        instance = gameMan->GetGame(player->data_.currentMapUuid, true);
        if (!instance)
            LOG_ERROR << "Unable to create instance for game " << player->data_.currentMapUuid << std::endl;
    }

    if (!instance)
    {
        DisconnectClient(AB::ErrorCodes::CannotEnterGame);
        return;
    }

    {
        // (1) First send the EnterWorld message
        auto output = OutputMessagePool::GetOutputMessage();
#ifdef DEBUG_NET
        LOG_DEBUG << "Sending EnterWorld message to player " << player->GetName() << std::endl;
#endif
        AB::Packets::Server::EnterWorld packet = {
            ProtocolGame::serverId_,
            player->data_.currentMapUuid,
            player->data_.instanceUuid,
            player->id_,
            static_cast<uint8_t>(instance->data_.type),
            instance->data_.partySize
        };
        output->AddByte(AB::GameProtocol::ServerPacketType::EnterWorld);
        AB::Packets::Add(packet, *output);
        // Don't use WriteToOutput() it would append it to existing messages
        Send(std::move(output));
    }

    // (2) Then we can send all the rest that happens when entering an game
    instance->PlayerJoin(player->id_);
}

void ProtocolGame::ChangeServerInstance(const std::string& serverUuid,
    const std::string& mapUuid, const std::string& instanceUuid)
{
    auto player = GetPlayer();
    if (!player)
    {
        LOG_ERROR << "Player == null" << std::endl;
        return;
    }

#ifdef DEBUG_NET
    LOG_DEBUG << "Player " << player->GetName() << " changing to map " << mapUuid << " instance " << instanceUuid << std::endl;
#endif

    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::GameProtocol::ServerPacketType::ChangeInstance);
    AB::Packets::Server::ChangeInstance packet = {
        serverUuid, mapUuid, instanceUuid, player->data_.uuid
    };
    AB::Packets::Add(packet, *output);
    // Don't use WriteToOutput() it would append it to existing messages
    Send(std::move(output));
}

void ProtocolGame::ChangeInstance(const std::string& mapUuid, const std::string& instanceUuid)
{
    ChangeServerInstance(ProtocolGame::serverId_, mapUuid, instanceUuid);
}

void ProtocolGame::Release()
{
    // If this is called while we still have a player (e.g. due to a sudden disconnect)
    // logout the player now.
    if (auto p = GetPlayer())
        p->Logout(true);
}

}
