/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SceneViewer.h"
#include <libcommon/Dispatcher.h>
#include <libcommon/Scheduler.h>
#include <libcommon/Subsystems.h>
#include <libmath/Line.h>
#include <sa/Assert.h>
#include "GameManager.h"
#include "Player.h"

#if defined(SCENE_VIEWER)

namespace Debug {

SceneViewer* SceneViewer::instance_ = nullptr;

static constexpr std::array<float, 3> GREEN = { 0.0f, 1.0f, 0.0f };
static constexpr std::array<float, 3> RED = { 1.0f, 0.0f, 0.0f };
static constexpr std::array<float, 3> BLUE = { 0.0f, 0.0f, 1.0f };
static constexpr std::array<float, 3> WHITE = { 1.0f, 1.0f, 1.0f };
static constexpr std::array<float, 3> GREY = { 0.4f, 0.4f, 0.4f };
static constexpr std::array<float, 3> TERRAIN = { 0.2f, 0.2f, 0.0f };

static constexpr Math::Line XLine = { Math::Vector3_Zero, Math::Vector3_UnitX };
static constexpr Math::Line YLine = { Math::Vector3_Zero, Math::Vector3_UnitY };
static constexpr Math::Line ZLine = { Math::Vector3_Zero, Math::Vector3_UnitZ };

// Most simple vertex shader, just transform the vertices
static constexpr const char* vertexShaderString = R"glsl(
#version 330 core
uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
in vec3 position;
void main()
{
  gl_Position = proj * view * model * vec4(position, 1.0);
}
)glsl";
// Most simple fragment shader, just draw the given color
static constexpr const char* fragmentShaderString = R"glsl(
#version 330 core
uniform vec3 my_color;
out vec4 color;
void main()
{
  color = vec4(my_color, 1.0);
}
)glsl";

SceneViewer::SceneViewer()
{
    ASSERT(SceneViewer::instance_ == nullptr);
    SceneViewer::instance_ = this;
}

SceneViewer::~SceneViewer()
{
    Stop();
    SceneViewer::instance_ = nullptr;
}

void SceneViewer::StaticRenderScene()
{
    ASSERT(SceneViewer::instance_ != nullptr);
    SceneViewer::instance_->Render();
}

void SceneViewer::StaticChangeSize(GLsizei w, GLsizei h)
{
    ASSERT(SceneViewer::instance_ != nullptr);
    SceneViewer::instance_->ChangeSize(w, h);
}

void SceneViewer::StaticMouse(int button, int state, int x, int y)
{
    ASSERT(SceneViewer::instance_ != nullptr);
    SceneViewer::instance_->Mouse(button, state, x, y);
}

void SceneViewer::StaticMouseMove(int x, int y)
{
    ASSERT(SceneViewer::instance_ != nullptr);
    SceneViewer::instance_->MouseMove(x, y);
}

void SceneViewer::StaticKeyboard(unsigned char key, int x, int y)
{
    ASSERT(SceneViewer::instance_ != nullptr);
    SceneViewer::instance_->Keyboard(key, x, y);
}

void SceneViewer::InternalInitialize()
{
    int argc = 0;
    glutInit(&argc, nullptr);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitContextVersion(3, 2);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);

    glutInitWindowSize(1280, 720);
    camera_.Resize(1280, 720);
    camera_.flipZ_ = true;
    glutCreateWindow("ABx Scene Viewer");
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glewExperimental = GL_TRUE;
    glewInit();

    const auto compile_shader = [](const GLint type, const char* str) -> GLuint
    {
        GLuint id = glCreateShader(type);
        glShaderSource(id, 1, &str, NULL);
        glCompileShader(id);
        return id;
    };
    GLuint vid = compile_shader(GL_VERTEX_SHADER, vertexShaderString);
    GLuint fid = compile_shader(GL_FRAGMENT_SHADER, fragmentShaderString);
    shaderProgram_ = glCreateProgram();
    glAttachShader(shaderProgram_, vid);
    glAttachShader(shaderProgram_, fid);
    glLinkProgram(shaderProgram_);
    GLint status = 0;
    glGetProgramiv(shaderProgram_, GL_LINK_STATUS, &status);
    glDeleteShader(vid);
    glDeleteShader(fid);

    glutDisplayFunc(SceneViewer::StaticRenderScene);
    glutReshapeFunc(SceneViewer::StaticChangeSize);
    glutMouseFunc(SceneViewer::StaticMouse);
    glutKeyboardFunc(SceneViewer::StaticKeyboard);
    glutMotionFunc(SceneViewer::StaticMouseMove);

    glGenVertexArrays(1, &VAO_);

    initialized_ = true;
}

void SceneViewer::Update()
{
    if (!running_)
        return;

    if (initialized_)
    {
        if (GetSubsystem<Game::GameManager>()->GetGameCount() != 0)
        {
            if (game_.expired())
            {
                const auto& games = GetSubsystem<Game::GameManager>()->GetGames();
                game_ = (*games.rbegin()).second;
            }
        }
        else if (!game_.expired())
            game_.reset();

        if (ea::shared_ptr<Game::Game> g = game_.lock())
        {
            if (g->GetState() == Game::Game::ExecutionState::Running && g->GetPlayerCount() != 0)
            {
                cameraObject_ = nullptr;

                g->VisitPlayers(
                    [&](const Game::Player& player)
                    {
                        cameraObject_ = &player;
                        return Iteration::Break;
                    });

                if (cameraObject_)
                {
                    camera_.transformation_ = { cameraObject_->transformation_.position_ +  Math::Vector3(0.0f, 1.5f, 0.0f),
                        cameraObject_->transformation_.orientation_.Inverse(),
                        cameraObject_->transformation_.scale_ };
                }
            }
        }
        glutMainLoopStep();
        Render();
    }

    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(16, std::bind(&SceneViewer::Update, shared_from_this())));
}

void SceneViewer::DrawScene()
{
    if (auto g = game_.lock())
    {
        if (g->GetState() == Game::Game::ExecutionState::Running)
        {
            glUseProgram(shaderProgram_);
            GLint proj_loc = glGetUniformLocation(shaderProgram_, "proj");
            // Let it transpose the matrix
            glUniformMatrix4fv(proj_loc, 1, GL_TRUE, camera_.GetProjection().Data());
            GLint view_loc = glGetUniformLocation(shaderProgram_, "view");
            glUniformMatrix4fv(view_loc, 1, GL_TRUE, camera_.GetView().Data());

            // This looks weird without any sorting...
            if (terrain_)
            {
                if (g->map_->terrain_)
                    DrawTerrain(*g->map_->terrain_);
            }
            if (terrainPatches_)
            {
                g->VisitObjects(
                    [&](const Game::GameObject& current)
                    {
                        if (Game::Is<Game::TerrainPatch>(current))
                            DrawObject(current);
                        return Iteration::Continue;
                    });
            }
            g->VisitObjects(
                [&](const Game::GameObject& current)
                {
                    if (!Game::Is<Game::TerrainPatch>(current))
                        DrawObject(current);
                    return Iteration::Continue;
                });
        }

        DrawAxis();
    }
}

void SceneViewer::DrawTerrain(const Game::Terrain& terrain) const
{
    if (!terrain.GetHeightMap())
        return;
    const Math::Mesh& mesh = terrain.GetHeightMap()->GetMesh();
    Math::Matrix4 matrix = terrain.transformation_.GetMatrix();
    DrawMesh(mesh, matrix, TERRAIN, wireframe_);
}

void SceneViewer::DrawObject(const Game::GameObject& object) const
{
    Math::Mesh mesh;
    Math::Matrix4 matrix;

    // Generating the mesh for the objects is very heavy, and in a real world
    // scenario it probably would be cached, because the meshes wouldn't change.
    // But this is just for debugging.

    if (!Game::Is<Game::TerrainPatch>(object))
    {
        auto* collShape = object.GetCollisionShape();
        if (!collShape)
            return;
        const Math::Transformation& trans = object.transformation_;
        mesh = collShape->GetMesh();

        if (collShape->shapeType_ == Math::ShapeType::BoundingBox)
        {
            // There is 1 special case, an oriented BB
            using BBoxShape = Math::CollisionShape<Math::BoundingBox>;
            BBoxShape* shape = static_cast<BBoxShape*>(collShape);
            const auto& obj = shape->Object();
            if (obj.IsOriented())
                matrix = trans.GetMatrix(obj.orientation_);
            else
                matrix = trans.GetMatrix();
        }
        else
            matrix = trans.GetMatrix();
    }
    else
    {
        mesh = Game::To<Game::TerrainPatch>(object).GetMesh();
        matrix = object.transformation_.GetMatrix();
    }
    if (mesh.indexCount_ == 0)
        return;
    ASSERT(mesh.IsTriangles());

    const std::array<float, 3>* color = nullptr;
    if (&object == cameraObject_)
        color = &GREEN;
    else if (Game::Is<Game::Actor>(object))
        color = &RED;
    else if (Game::Is<Game::TerrainPatch>(object))
        color = &GREY;
    else
        color = &WHITE;
    DrawMesh(mesh, matrix, *color, wireframe_);
}

void SceneViewer::DrawMesh(const Math::Mesh& mesh, const Math::Matrix4& matrix, const std::array<float, 3>& color, bool wireframe) const
{
    GLuint VBO = 0;
    GLuint EBO = 0;
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, mesh.VertexDataSize(), mesh.VertexData(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.IndexDataSize(), mesh.IndexData(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    GLint model_loc = glGetUniformLocation(shaderProgram_, "model");
    // Let it transpose the matrix
    glUniformMatrix4fv(model_loc, 1, GL_TRUE, matrix.Data());
    GLint color_location = glGetUniformLocation(shaderProgram_, "my_color");
    glUniform3fv(color_location, 1, color.data());

    // Draw mesh as wireframe or filled
    glPolygonMode(GL_FRONT_AND_BACK, wireframe ? GL_LINE : GL_FILL);
    glBindVertexArray(VAO_);
    glDrawElements(GL_TRIANGLES, (int)mesh.indexCount_, GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);

    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void SceneViewer::DrawAxis() const
{
    DrawAxisLine(XLine, Math::Matrix4_Identity, RED);
    DrawAxisLine(YLine, Math::Matrix4_Identity, GREEN);
    DrawAxisLine(ZLine, Math::Matrix4_Identity, BLUE);
}

void SceneViewer::DrawAxisLine(const Math::Line& line,
    const Math::Matrix4& matrix,
    const std::array<float, 3>& color) const
{
    GLuint VBO = 0;
    GLuint EBO = 0;
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, line.VertexDataSize(), line.VertexData(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    GLint model_loc = glGetUniformLocation(shaderProgram_, "model");
    // Let it transpose the matrix
    glUniformMatrix4fv(model_loc, 1, GL_TRUE, matrix.Data());
    GLint color_location = glGetUniformLocation(shaderProgram_, "my_color");
    glUniform3fv(color_location, 1, color.data());

    glBindVertexArray(VAO_);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);

    glDeleteBuffers(1, &VBO);
}

void SceneViewer::Mouse(int button, int state, int x, int y)
{
    if (button == GLUT_RIGHT_BUTTON)
    {
        mouseDown_ = state == GLUT_DOWN;
        mouseDownPos_ = { x, y };
    }
    else if (button == GLUT_MIDDLE_BUTTON)
    {
        if (state == GLUT_DOWN)
        {
            // Reset
            camera_.yaw_ = 0.0f;
            camera_.pitch_ = 0.0f;
            camera_.SetZoom(1.0f);
        }
    }
    else if (button == 3 || button == 4)
    {
        // Mouse Wheel -> Zoom in/out
        if (button == 3)
            camera_.SetZoom(camera_.GetZoom() + 0.1f);
        else
            camera_.SetZoom(camera_.GetZoom() - 0.1f);
    }
}

void SceneViewer::MouseMove(int x, int y)
{
    if (!mouseDown_)
        return;
    // Mouse look
    camera_.yaw_ -= Math::DegToRad((float)(mouseDownPos_.x_ - x)) * 0.05f;
    Math::NormalizeAngle(camera_.yaw_);
    camera_.pitch_ -= Math::DegToRad((float)(mouseDownPos_.y_ - y)) * 0.05f;
    camera_.pitch_ = Math::Clamp(camera_.pitch_, -Math::M_PIFOURTH, Math::M_PIFOURTH);
    mouseDownPos_ = { x, y };
}

void SceneViewer::Keyboard(unsigned char key, int, int)
{
    switch (key)
    {
    case 'w':
        wireframe_ = !wireframe_;
        break;
    case 't':
        terrain_ = !terrain_;
        break;
    case 'p':
        terrainPatches_ = !terrainPatches_;
        break;
    case 'r':
        game_.reset();
        break;
    default:
        break;
    }
}

bool SceneViewer::Initialize()
{
    //  Create the window on the thread you want to process its messages.
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(&SceneViewer::InternalInitialize, shared_from_this())));
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(500ms);

    return true;
}

void SceneViewer::Run()
{
    running_ = true;

    glutMainLoopEnter();
    GetSubsystem<Asynch::Scheduler>()->Add(
        Asynch::CreateScheduledTask(20, std::bind(&SceneViewer::Update, shared_from_this())));
}

void SceneViewer::Stop()
{
    initialized_ = false;
    glutMainLoopExit();
    running_ = false;
    glDeleteVertexArrays(1, &VAO_);
    glDeleteProgram(shaderProgram_);
}

void SceneViewer::Render()
{
    if (!initialized_ || !running_)
        return;

    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    DrawScene();

    glutSwapBuffers();
    glutPostRedisplay();
}

void SceneViewer::ChangeSize(GLsizei w, GLsizei h)
{
    glViewport(0, 0, w, h);
    camera_.Resize(w, h);
}

}

#endif
