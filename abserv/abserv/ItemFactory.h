/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <mutex>
#include "Item.h"
#include <libcommon/UuidUtils.h>
#include <libcommon/StringUtils.h>
#include <libshared/Mechanic.h>
#include <sa/WeightedSelector.h>
#include <eastl.hpp>

namespace Game {

class Player;

struct CreateItemInfo
{
    std::string itemUuid;
    std::string instanceUuid;
    std::string mapUuid;
    uint32_t level{ LEVEL_CAP };
    /// When buying the item from a merchant these are always with max stats. Dropped Items have random stats.
    bool maxStats{ false };
    std::string accUuid{ Utils::Uuid::EMPTY_UUID };
    std::string playerUuid{ Utils::Uuid::EMPTY_UUID };
    uint32_t count{ 0 };
    uint16_t value{ 0 };
    AB::Entities::StoragePlace storagePlace{ AB::Entities::StoragePlace::Scene };
    std::string stats{ "" };
};

class ItemFactory
{
private:
    using ItemSelector = sa::WeightedSelector<std::string>;
    using TypedListValue = std::pair<std::string, AB::Entities::ItemType>;
    std::map<std::string, std::unique_ptr<ItemSelector>> dropChances_;
    /// List of upgrades. Upgrades are not specific to maps
    std::map<AB::Entities::ItemType, std::vector<TypedListValue>> typedItems_;
    std::mutex lock_;
    ea::map<std::string, AB::Entities::ConcreteItem> pendingCreates_;

    static bool CreateDBItem(const AB::Entities::ConcreteItem& item);
    /// Create temporary item, does not create a concrete item.
    static ea::unique_ptr<Item> CreateTempItem(const std::string& itemUuid);
    static ea::unique_ptr<Item> LoadConcrete(const std::string& concreteUuid);
    static void CalculateValue(const AB::Entities::Item& item, uint32_t level, AB::Entities::ConcreteItem& result);
    static uint32_t GetItemIndexFromUuid(const std::string& uuid);

    void CreatePendingItems();
    void IdentifyArmor(Item& item, Player& player);
    void IdentifyWeapon(Item& item, Player& player);
    void IdentifyOffHandWeapon(Item& item, Player& player);
    uint32_t CreateModifier(AB::Entities::ItemType modType, Item& forItem,
        uint32_t level, bool maxStats, const std::string& playerUuid);
    void InternalLoadDropChances(std::string mapUuid, bool force);
public:
    static uint32_t GetConcreteId(const std::string& concreteUuid);
    // Used to craft items with a certain attribute
    static std::string GetMaxItemStats(const std::string& itemUuid, uint32_t level,
        int attrib = -1, int attribRank = -1, int damageType = -1);

    ItemFactory();
    ~ItemFactory() = default;

    /// Load items that drop on all maps
    void Initialize();
    /// Creates a new concrete item of the item and saves it to DB
    uint32_t CreateItem(const CreateItemInfo& info);
    Item* GetConcrete(const std::string& concreteUuid);
    void IdentiyItem(Item& item, Player& player);
    /// Deletes a concrete item from the database, e.g. when an item was not picked up. Also removes it from cache.
    void DeleteConcrete(const std::string& uuid);
    /// Deletes an Item with all attached modifiers. Removes them from cache
    void DeleteItem(Item* item);
    void DeleteItemById(uint32_t itemId);
    /// mapUuid is not a reference because it's called asynchronously
    void LoadDropChances(std::string mapUuid);
    void ReloadDropChances();
    /// Delete drop chances for this map
    void DeleteMap(const std::string& uuid);
    uint32_t CreateDropItem(const std::string& instanceUuid, const std::string& mapUuid,
        uint32_t level, const Player& player);
    uint32_t CreatePlayerMoneyItem(const Player& forPlayer, uint32_t count);
    uint32_t CreatePlayerItem(const Player& forPlayer, const std::string& itemUuid,
        AB::Entities::StoragePlace place, uint32_t count = 1, const std::string& stats = Utils::EMPTY_STRING);
    bool MoveToMerchant(Item* item, uint32_t count);
};

}
