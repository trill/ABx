/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Noncopyable.h>
#include <libshared/Mechanic.h>

namespace Game {

class Player;
class Actor;
class GameObject;

namespace Components {

class InteractionComp
{
    NON_COPYABLE(InteractionComp)
    NON_MOVEABLE(InteractionComp)
private:
    enum class Type
    {
        None,
        Generic,
        Skill,
    };
    Player& owner_;
    Type type_{ Type::None };
    int skillIndex_{ -1 };
    Range skillRange_{ Range::Aggro };
    ea::weak_ptr<GameObject> target_;
    void OnCancelAll();
    void UpdateGeneric();
    void UpdateSkill();
public:
    explicit InteractionComp(Player& owner);
    void Update(uint32_t timeElapsed);
    void Interact(bool suppress, bool ping);
    void UseSkill(bool suppress, int skillIndex, bool ping);
};

}
}
