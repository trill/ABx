/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StateComp.h"
#include "GameObject.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include "Actor.h"
#include "MessageStream.h"
#include <sa/time.h>
#include <libcommon/NetworkMessage.h>

namespace Game {
namespace Components {

StateComp::StateComp(GameObject& owner) :
    owner_(owner),
    lastStateChange_(sa::time::tick())
{
    owner_.SubscribeEvent<VoidVoidSignature>(OnCancelAllEvent, std::bind(&StateComp::OnCancelAll, this));
}

void StateComp::OnCancelAll()
{
    Reset();
}

void StateComp::SetState(AB::GameProtocol::CreatureState state, bool apply /* = false */)
{
    if (state != newState_)
    {
        auto oldState = newState_;
        newState_ = state;
        if (apply)
            Apply();
        owner_.CallEvent<OnStateChangeSignature>(OnStateChangeEvent, oldState, newState_);
    }
}

bool StateComp::KnockDown(uint32_t time)
{
    if (IsKnockedDown())
        return false;
    knockdownTick_ = sa::time::tick();
    knockdownEndTime_ = knockdownTick_ + time;
    SetState(AB::GameProtocol::CreatureState::KnockedDown);
    return true;
}

void StateComp::Apply()
{
    lastStateChange_ = sa::time::tick();
    currentState_ = newState_;
}

void StateComp::Reset()
{
    SetState(AB::GameProtocol::CreatureState::Idle);
}

void StateComp::Update(uint32_t)
{
    if (IsKnockedDown())
    {
        if (knockdownEndTime_ <= sa::time::tick())
        {
            SetState(AB::GameProtocol::CreatureState::Idle);
        }
        return;
    }
    using namespace sa::time::literals;
    if ((currentState_ > AB::GameProtocol::CreatureState::__EmoteStart &&
        currentState_ < AB::GameProtocol::CreatureState::__EmoteEnd)
        && lastStateChange_ + 4_s < sa::time::tick())
    {
        // Reset some emotes after 4 seconds
        SetState(AB::GameProtocol::CreatureState::Idle);
    }
}

void StateComp::Write(MessageStream& message)
{
    if (IsStateChanged())
    {
        Apply();
        AB::Packets::Server::ObjectStateChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(GetState())
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectStateChanged, packet);
    }
}

}
}
