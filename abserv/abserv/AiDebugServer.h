/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libipc/IpcServer.h>
#include <AB/IPC/AI/ClientMessages.h>
#include <AB/IPC/AI/ServerMessages.h>
#include <sa/Noncopyable.h>
#include <mutex>
#include <eastl.hpp>

namespace Game {
class Game;
class Actor;
}

namespace AI {

class DebugServer
{
    NON_COPYABLE(DebugServer)
private:
    ea::unique_ptr<IPC::Server> server_;
    bool active_{ false };
    ea::vector<ea::weak_ptr<Game::Game>> games_;
    ea::map<uint32_t, uint32_t> selectedGames_;
    std::mutex lock_;
    void BroadcastGame(const Game::Game& game);
    void BroadcastGameAdded(const Game::Game& game);
    void BroadcastGameRemoved(uint32_t id);
    void HandleGetGames(IPC::ServerConnection& client, const GetGames&);
    void HandleSelectGame(IPC::ServerConnection& client, const SelectGame&);
    void HandleGetTrees(IPC::ServerConnection& client, const GetTrees&);
    ea::set<uint32_t> GetSubscribedClients(uint32_t gameId);
public:
    DebugServer(asio::io_service& ioService, uint32_t ip, uint16_t port);
    DebugServer() = default;
    void AddGame(ea::shared_ptr<Game::Game> game);
    void RemoveGame(uint32_t id);
    void Update();
    [[nodiscard]] bool IsActive() const { return active_; }
};

}
