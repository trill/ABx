/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/CommonConfig.h>

#define SERVER_VERSION_MAJOR 0
#define SERVER_VERSION_MINOR 1
#define SERVER_YEAR CURRENT_YEAR
#define SERVER_PRODUCT_NAME "Forgotten Wars Game Server"

#define AB_SERVER_VERSION AB_VERSION_CREATE(SERVER_VERSION_MAJOR, SERVER_VERSION_MINOR)
