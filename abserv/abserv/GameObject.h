/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libshared/Damage.h>
#include <libmath/OctreeObject.h>
#include <libmath/OctreeQuery.h>
#include "StateComp.h"
#include "Events.h"
#include "RangesObjects.h"
#include <AB/Entities/Character.h>
#include <AB/Entities/Skill.h>
#include <AB/ProtocolCodes.h>
#include <eastl.hpp>
#include <sa/PropStream.h>
#include <libcommon/Variant.h>
#include <libshared/Mechanic.h>
#include <libmath/BoundingBox.h>
#include <libmath/CollisionShape.h>
#include <libmath/Vector3.h>
#include <libmath/Matrix4.h>
#include <libmath/Transformation.h>
#include <kaguya/kaguya.hpp>
#include <mutex>
#include <sa/Events.h>
#include <sa/IdGenerator.h>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>
#include <sa/StringHash.h>
#include "MessageStream.h"

namespace Net {
class NetworkMessage;
}

namespace Game {

namespace Components {
class TriggerComp;
class CollisionComp;
}

enum ObjerctAttr : uint8_t
{
    ObjectAttrName = 1,

    // For serialization
    ObjectAttrEnd = 254
};

class Game;
class Actor;
class Model;
class Npc;
class Player;
class AreaOfEffect;
class Projectile;
class Skill;

inline const float AVERAGE_BB_EXTENDS = 0.3f;

class GameObject : public Math::OctreeObject, public ea::enable_shared_from_this<GameObject>
{
    NON_COPYABLE(GameObject)
public:
    static sa::IdGenerator<uint32_t> objectIds_;
private:
    int64_t removeAt_{ 0 };
    bool hasGame_{ false };
    ea::unique_ptr<Math::AbstractCollisionShape> collisionShape_;
    ea::shared_ptr<Model> model_;
    std::vector<GameObject*> _LuaQueryObjects(float radius);
    std::vector<GameObject*> _LuaRaycast(const Math::StdVector3& direction);
    /// Raycast to destination point
    std::vector<GameObject*> _LuaRaycastTo(const Math::StdVector3& destination);
    std::vector<GameObject*> _LuaRaycastToObject(GameObject* object);
    Actor* _LuaAsActor();
    Npc* _LuaAsNpc();
    Player* _LuaAsPlayer();
    AreaOfEffect* _LuaAsAOE();
    Projectile* _LuaAsProjectile();
    void _LuaSetPosition(const Math::StdVector3& pos);
    void _LuaSetRotation(float y);
    void _LuaSetScale(const Math::StdVector3& scale);
    void _LuaSetScaleSimple(float value);
    Math::StdVector3 _LuaGetPosition() const;
    float _LuaGetRotation() const;
    Math::StdVector3 _LuaGetScale() const;
    void _LuaSetBoundingBox(const Math::StdVector3& min, const Math::StdVector3& max);
    void _LuaSetBoundingSize(const Math::StdVector3& size);
    std::string _LuaGetVarString(const std::string& name) const;
    void _LuaSetVarString(const std::string& name, const std::string& value);
    float _LuaGetVarNumber(const std::string& name) const;
    void _LuaSetVarNumber(const std::string& name, float value);
    Game* _LuaGetGame() const;
    int _LuaGetState() const;
    void _LuaSetState(int state);
    /// Call a method of the game script. data is optional
    void _LuaCallGameEvent(const std::string& name, GameObject* data);
    Actor* _LuaGetClosestActor(bool undestroyable, bool unselectable) const;
    /// Returns all object inside the collision shape. This object must be a Trigger, i.e. it returns only objects when SetTrigger(true) was called before.
    std::vector<GameObject*> _LuaGetObjectsInside() const;
    bool _LuaIsObjectInSight(const GameObject* object) const;
    std::vector<Actor*> _LuaGetActorsInRange(Range range) const;
    uint32_t _LuaGetCollisionMask() const { return GetCollisionMask(); }
    void _LuaSetCollisionMask(uint32_t value) { return SetCollisionMask(value); }
    uint32_t _LuaGetCollisionLayer() const { return GetCollisionLayer(); }
    void _LuaSetCollisionLayer(uint32_t value) { return SetCollisionLayer(value); }
protected:
    std::string name_;
    Utils::VariantMap variables_;
    ea::weak_ptr<Game> game_;
    GameObjectEvents events_;
    RangesObjects ranges_;
    void UpdateRanges();
    virtual void ObjectEnteredRange(Range /* range */, uint32_t /* objectId */) { }
    virtual void ObjectLeftRange(Range /* range */, uint32_t /* objectId */) { }
    uint32_t GetNewId()
    {
        return objectIds_.Next();
    }
    void AddToOctree();
    void RemoveFromOctree();
public:
    static void RegisterLua(kaguya::State& state);
    /// Let's make the head 1.7m above the ground
    static constexpr Math::Vector3 HeadOffset{ 0.0f, 1.7f, 0.0f };
    /// Center of body
    static constexpr Math::Vector3 BodyOffset{ 0.0f, 0.8f, 0.0f };

    GameObject();
    ~GameObject() override;

    // Return smart pointer
    template <typename T>
    inline ea::shared_ptr<T> GetPtr();

    virtual void Update(uint32_t timeElapsed, MessageStream& message);

    void SetBoundingSize(const Math::Vector3& size);
    void SetCollisionShape(ea::unique_ptr<Math::AbstractCollisionShape> shape)
    {
        collisionShape_ = std::move(shape);
    }
    void SetModel(ea::shared_ptr<Model> model);
    uint32_t GetId() const { return id_; }
    ea::shared_ptr<Game> GetGame() const
    {
        return game_.lock();
    }
    // Returns true when the object is currently in an outpost
    bool IsInOutpost() const;
    bool IsInBattle() const;
    virtual void SetGame(ea::shared_ptr<Game> game)
    {
        RemoveFromOctree();
        game_ = game;
        if (game)
            AddToOctree();
        hasGame_ = !!game;
    }
    bool HasGame() const { return hasGame_; }
    bool IsSelectable() const { return selectable_; }
    void SetSelectable(bool value) { selectable_ = value; }
    Math::AbstractCollisionShape* GetCollisionShape() const
    {
        if (!collisionShape_)
            return nullptr;
        return collisionShape_.get();
    }
    bool Collides(const Math::OctreeObject* other, const Math::Vector3& velocity, Math::Vector3& move) const;
    void Collides(Math::OctreeObject** others, size_t count, const Math::Vector3& velocity,
        const std::function<Iteration(GameObject& other, const Math::Vector3& move, bool& updateTrans)>& callback) const;
    /// Get the distance to another object
    float GetDistance(const GameObject* other) const
    {
        if (!other)
            return Math::M_INFINITE;
        return transformation_.position_.Distance(other->transformation_.position_);
    }

    /// Test if object is in our range
    bool IsInRange(Range range, const GameObject* object) const;
    /// Check if the object is within maxDist
    bool IsCloserThan(float maxDist, const GameObject* object) const;
    /// Allows to execute a functor/lambda on the objects in range
    void VisitInRange(Range range, const std::function<Iteration(GameObject&)>& func) const;
    /// Returns the closest actor or nullptr
    /// @param[in] undestroyable If true include undestroyable actors
    /// @param[in] unselectable If true includes unselectable actors
    Actor* GetClosestActor(bool undestroyable, bool unselectable) const;
    Actor* GetClosestActor(const std::function<bool(const Actor& actor)>& callback) const;

    virtual AB::GameProtocol::GameObjectType GetType() const
    {
        return AB::GameProtocol::GameObjectType::Static;
    }
    bool IsActorType() const { return GetType() >= AB::GameProtocol::GameObjectType::Projectile; }
    bool IsPlayerOrNpcType() const
    {
        const auto t = GetType();
        return t == AB::GameProtocol::GameObjectType::Npc || t == AB::GameProtocol::GameObjectType::Player;
    }
    const Math::Vector3& GetPosition() const
    {
        return transformation_.position_;
    }

    const Utils::Variant& GetVar(const std::string& name) const;
    void SetVar(const std::string& name, const Utils::Variant& val);
    /// Process octree raycast.
    void ProcessRayQuery(const Math::RayOctreeQuery& query, ea::vector<Math::RayQueryResult>& results) override;

    bool Raycast(ea::vector<GameObject*>& result, const Math::Vector3& direction, float maxDist = Math::M_INFINITE, const Math::OctreeMatcher* matcher = nullptr) const;
    bool Raycast(ea::vector<GameObject*>& result, const Math::Vector3& position, const Math::Vector3& direction, float maxDist = Math::M_INFINITE, const Math::OctreeMatcher* matcher = nullptr) const;
    bool RaycastWithResult(ea::vector<Math::RayQueryResult>& result, const Math::Vector3& position, const Math::Vector3& direction, float maxDist = Math::M_INFINITE, const Math::OctreeMatcher* matcher = nullptr) const;
    bool IsObjectInSight(const GameObject& object) const;
    /// Remove this object from scene
    void Remove();
    /// Remove this object in time ms
    void RemoveIn(uint32_t time);

    template <typename Signature>
    size_t SubscribeEvent(sa::event_t id, std::function<Signature>&& func)
    {
        return events_.Subscribe<Signature>(id, std::move(func));
    }
    template <typename Signature>
    void UnsubscribeEvent(sa::event_t id, size_t index)
    {
        events_.Unsubscribe<Signature>(id, index);
    }
    /// Call the first subscriber
    template <typename Signature, typename... _CArgs>
    auto CallEventOne(sa::event_t id, _CArgs&& ... _Args)
    {
        return events_.CallOne<Signature, _CArgs...>(id, std::forward<_CArgs>(_Args)...);
    }
    /// Calls all subscribers
    template <typename Signature, typename... _CArgs>
    auto CallEvent(sa::event_t id, _CArgs&& ... _Args)
    {
        return events_.CallAll<Signature, _CArgs...>(id, std::forward<_CArgs>(_Args)...);
    }

    virtual bool Serialize(sa::PropWriteStream& stream);

    virtual const std::string& GetName() const { return name_; }
    virtual void SetName(const std::string& name) { name_ = name; }
    virtual void SetState(AB::GameProtocol::CreatureState state);

    Math::Transformation transformation_;
    /// Auto ID, not DB ID
    uint32_t id_;
    /// Can not be selected by a player. Majority of objects is not selectable by the player so lets make it true by default.
    bool selectable_{ false };
    Components::StateComp stateComp_;
    ea::unique_ptr<Components::TriggerComp> triggerComp_;
    Math::BoundingBox GetWorldBoundingBox() const override
    {
        if (!collisionShape_)
            return {};
        return collisionShape_->GetWorldBoundingBox(transformation_.GetMatrix());
    }
    virtual Math::BoundingBox GetBoundingBox() const
    {
        if (!collisionShape_)
            return {};
        return collisionShape_->GetBoundingBox();
    }
    bool QueryObjects(ea::vector<Math::OctreeObject*>& result, float radius, const Math::OctreeMatcher* matcher = nullptr);
    bool QueryObjects(ea::vector<Math::OctreeObject*>& result, const Math::BoundingBox& box, const Math::OctreeMatcher* matcher = nullptr);

    uint32_t GetRetriggerTimout() const;
    void SetRetriggerTimout(uint32_t value);
    bool IsTrigger() const;
    void SetTrigger(bool value);

    virtual void WriteSpawnData(MessageStream& msg);

    friend std::ostream& operator << (std::ostream& os, const GameObject& value)
    {
        return os << "GameObject{" << static_cast<int>(value.GetType()) << "} id " << value.id_ << ": " << value.GetName();
    }
};

template <typename T>
inline bool Is(const GameObject&)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    return false;
}

/// Returns false when obj is null
template <typename T>
inline bool Is(const GameObject* obj)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    return obj && Is<T>(*obj);
}

template <>
inline bool Is<GameObject>(const GameObject&)
{
    return true;
}

template <typename T>
inline const T& To(const GameObject& obj)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    ASSERT(Is<T>(obj));
    return static_cast<const T&>(obj);
}

template <typename T>
inline T& To(GameObject& obj)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    ASSERT(Is<T>(obj));
    return static_cast<T&>(obj);
}

template <typename T>
inline const T* To(const GameObject* obj)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    if (!Is<T>(obj))
        return nullptr;
    return static_cast<const T*>(obj);
}

template <typename T>
inline T* To(GameObject* obj)
{
    static_assert(std::is_base_of<GameObject, T>::value);
    if (!Is<T>(obj))
        return nullptr;
    return static_cast<T*>(obj);
}

template <typename T>
inline ea::shared_ptr<T> GameObject::GetPtr()
{
    static_assert(std::is_base_of<GameObject, T>::value);
    if (Is<T>(*this))
        return ea::static_pointer_cast<T>(shared_from_this());
    return {};
}

template<typename T>
class ObjectTypeMatcher final : public Math::OctreeMatcher
{
public:
    bool Matches(const Math::OctreeObject* object) const override
    {
        return object && Is<T>(static_cast<const GameObject*>(object));
    }
};

class SentToPlayerMatcher final : public Math::OctreeMatcher
{
public:
    bool Matches(const Math::OctreeObject* object) const override
    {
        return object && static_cast<const GameObject*>(object)->GetType() > AB::GameProtocol::GameObjectType::__SentToPlayer;
    }
};

}
