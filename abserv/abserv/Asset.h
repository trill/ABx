/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <sa/Compiler.h>

namespace IO {

class Asset
{
    friend class DataProvider;
protected:
    std::string fileName_;
    virtual void OnChanged() {}
public:
    Asset();
    virtual ~Asset();

    void SetFileName(const std::string& value);
    const std::string& GetFileName() const { return fileName_; }
};

}
