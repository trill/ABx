/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PlayerManager.h"
#include "Player.h"
#include "IOPlayer.h"
#include <libcommon/Logger.h>
#include <libcommon/StringUtils.h>
#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <libcommon/Subsystems.h>

namespace Game {

void PlayerManager::BroadcastPlayerLoggedIn(ea::shared_ptr<Player> player)
{
    if (!player)
        return;

    if (player->account_.onlineStatus == AB::Entities::OnlineStatusInvisible ||
        player->account_.onlineStatus == AB::Entities::OnlineStatusOffline)
        return;

    BroadcastPlayerChanged(*player, AB::GameProtocol::PlayerInfoFieldOnlineStatus |
            AB::GameProtocol::PlayerInfoFieldCurrentName | AB::GameProtocol::PlayerInfoFieldCurrentMap);
}

void PlayerManager::BroadcastPlayerLoggedOut(ea::shared_ptr<Player> player)
{
    if (!player)
        return;

    if (player->account_.onlineStatus == AB::Entities::OnlineStatusInvisible ||
        player->account_.onlineStatus == AB::Entities::OnlineStatusOffline)
        return;

    BroadcastPlayerChanged(*player, AB::GameProtocol::PlayerInfoFieldOnlineStatus);
}

void PlayerManager::BroadcastPlayerChanged(const Player& player, uint32_t fields)
{
    auto* client = GetSubsystem<Net::MessageClient>();
    Net::MessageMsg msg;
    msg.type_ = Net::MessageType::PlayerChanged;

    sa::PropWriteStream stream;
    stream.Write<uint32_t>(fields);              // What has changed
    stream.WriteString(player.account_.uuid);    // Account
    msg.SetPropStream(stream);
    client->Write(msg);
}

ea::shared_ptr<Player> PlayerManager::GetPlayerByName(const std::string& name) const
{
    return GetPlayerById(GetPlayerIdByName(name));
}

ea::shared_ptr<Player> PlayerManager::GetPlayerByUuid(const std::string& uuid) const
{
    const auto& index = playerIndex_.get<PlayerUuidIndexTag>();
    const auto accountIt = index.find(uuid);
    if (accountIt == index.end())
        return {};
    return GetPlayerById((*accountIt).id);
}

ea::shared_ptr<Player> PlayerManager::GetPlayerById(uint32_t id) const
{
    const auto it = players_.find(id);
    if (it != players_.end())
        return (*it).second;

    return {};
}

ea::shared_ptr<Player> PlayerManager::GetPlayerByAccountUuid(const std::string& uuid) const
{
    const auto& index = playerIndex_.get<AccountUuidIndexTag>();
    const auto accountIt = index.find(uuid);
    if (accountIt == index.end())
        return {};
    return GetPlayerById((*accountIt).id);
}

uint32_t PlayerManager::GetPlayerIdByName(const std::string& name) const
{
    const auto& index = playerIndex_.get<PlayerNameIndexTag>();
    // Player names are case insensitive
    const auto accountIt = index.find(Utils::Utf8ToLower(name));
    if (accountIt == index.end())
        return 0;
    return (*accountIt).id;
}

ea::shared_ptr<Player> PlayerManager::CreatePlayer(std::shared_ptr<Net::ProtocolGame> client)
{
    ea::shared_ptr<Player> result = ea::make_shared<Player>(client);
    players_[result->id_] = result;

    return result;
}

void PlayerManager::UpdatePlayerIndex(const Player& player)
{
    playerIndex_.insert({
        player.id_,
        player.data_.uuid,
        Utils::Utf8ToLower(player.GetName()),
        player.account_.uuid
    });
}

void PlayerManager::RemovePlayer(uint32_t playerId)
{
    auto it = players_.find(playerId);
    if (it != players_.end())
    {
        auto& idIndex = playerIndex_.get<IdIndexTag>();
        auto indexIt = idIndex.find((*it).second->id_);
        if (indexIt != idIndex.end())
            idIndex.erase(indexIt);

        players_.erase(it);

        if (players_.empty())
            idleTime_ = sa::time::tick();
    }
}

void PlayerManager::CleanPlayers()
{
    if (players_.empty())
        return;

    // Logout all inactive players
    auto i = players_.begin();
    while ((i = ea::find_if(i, players_.end(), [](const auto& current) -> bool
    {
        // Disconnect after 10sec
        return current.second->GetInactiveTime() > PLAYER_INACTIVE_TIME_KICK;
    })) != players_.end())
    {
        ea::shared_ptr<Player> p = (*i).second;
        LOG_INFO << "No ping from player " << p->GetName() << " for " << p->GetInactiveTime() << "ms, logging out now" << std::endl;
        ++i;
        // Calls PlayerManager::RemovePlayer()
        p->Logout(true);
   }
}

void PlayerManager::RefreshAuthTokens()
{
    // No inactive players here
    auto* client = GetSubsystem<IO::DataClient>();
    int64_t tick = sa::time::tick();
    for (const auto& player : players_)
    {
        if (tick - player.second->account_.authTokenExpiry < Auth::AUTH_TOKEN_EXPIRES_IN / 2)
        {
            player.second->account_.authTokenExpiry = tick + Auth::AUTH_TOKEN_EXPIRES_IN;
            client->Update(player.second->account_);
        }
    }
}

void PlayerManager::KickPlayer(uint32_t playerId)
{
    auto it = players_.find(playerId);
    if (it != players_.end())
    {
        ea::shared_ptr<Player> p = (*it).second;
        LOG_INFO << "Kicking player " << p->GetName() << std::endl;
        p->Logout(true);
    }
}

void PlayerManager::KickAllPlayers()
{
    if (players_.empty())
        return;

    LOG_INFO << "Kicking all players" << std::endl;
    while (!players_.empty())
    {
        auto it = players_.begin();
        ea::shared_ptr<Player> p = (*it).second;
        p->Logout(true);
    }
}

void PlayerManager::SaveAllPlayers()
{
    for (const auto& p : players_)
    {
        IO::SavePlayer(*p.second);
    }
}

void PlayerManager::BroadcastNetMessage(const Net::NetworkMessage& msg)
{
    for (const auto& p : players_)
    {
        p.second->WriteToOutput(msg);
    }
}

}
