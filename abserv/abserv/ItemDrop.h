/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "GameObject.h"
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Game {

class Item;
class Actor;

class ItemDrop final : public GameObject
{
    NON_COPYABLE(ItemDrop)
private:
    uint32_t itemId_;
    int64_t dropTick_;
    uint32_t itemIndex_{ 0 };
    std::string concreteUuid_;
    bool pickedUp_{ false };
    /// Dropper
    ea::weak_ptr<Actor> source_;
    Actor* _LuaGetSource() const;
    Actor* _LuaGetTarget() const;
    Item* _LuaGetItem() const;
    void Update(uint32_t timeElapsed, MessageStream& message) override;
    void OnClicked(Actor* actor);
public:
    static void RegisterLua(kaguya::State& state);

    explicit ItemDrop(uint32_t itemId);
    ItemDrop() = delete;

    ~ItemDrop() override;

    AB::GameProtocol::GameObjectType GetType() const override
    {
        return AB::GameProtocol::GameObjectType::ItemDrop;
    }
    uint32_t GetItemIndex() const;
    const Item* GetItem() const;
    /// ID of dropper
    uint32_t GetSourceId();

    void SetSource(ea::shared_ptr<Actor> source);
    bool Serialize(sa::PropWriteStream& stream) override;
    void WriteSpawnData(MessageStream& msg) override;
    void PickUp(Actor* actor);

    uint32_t targetId_{ 0 };
};

template <>
inline bool Is<ItemDrop>(const GameObject& obj)
{
    return obj.GetType() == AB::GameProtocol::GameObjectType::ItemDrop;
}

}
