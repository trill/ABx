/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <sa/Iteration.h>
#include <sa/IdGenerator.h>
#include <kaguya/kaguya.hpp>
#include <libshared/Mechanic.h>
#include <libshared/Team.h>
#include <sa/Noncopyable.h>

namespace Game {

class Actor;

class Group
{
    NON_COPYABLE(Group)
    NON_MOVEABLE(Group)
private:
    bool _LuaAdd(Actor* actor);
    bool _LuaRemove(Actor* actor);
    Actor* _LuaGetMember(int index);
    int _LuaGetMemberCount() const;
    std::vector<Actor*> _LuaGetMembers();
protected:
    ea::vector<ea::weak_ptr<Actor>> members_;
    TeamColor color_{ TeamColor::Default };
    uint32_t id_;
    std::string name_;
    int64_t defeatedTick_{ 0 };
    bool defeated_{ false };
public:
    static sa::IdGenerator<uint32_t> groupIds_;
    /// Returns a new Party/Group ID
    static uint32_t GetNewId()
    {
        return groupIds_.Next();
    }
    static void RegisterLua(kaguya::State& state);

    explicit Group(uint32_t id);

    TeamColor GetColor() const { return color_; }
    void SetColor(TeamColor value);
    virtual std::string GetName() const { return name_; }
    void SetName(const std::string& value) { name_ = value; }
    uint32_t GetId() const { return id_; }
    bool IsEnemy(const Group* other) const;
    bool IsAlly(const Group* other) const;
    bool IsAttacked() const;
    int GetMorale() const;
    void IncreaseMorale();
    void DecreaseMorale();
    void Resurrect(int precentHealth, int percentEnergy);
    void KillAll();
    size_t GetMemberCount() const { return members_.size(); }

    bool Add(ea::shared_ptr<Actor> actor);
    bool Remove(uint32_t id);
    Actor* GetLeader() const;
    bool IsLeader(const Actor& actor) const;

    Actor* GetRandomMember() const;
    Actor* GetRandomMemberInRange(const Actor* actor, Range range) const;
    virtual bool IsResigned() const { return false; }
    bool IsDefeated() const { return defeated_; }
    void Defeat();

    virtual void Update(uint32_t);

    template <typename Callback>
    void VisitMembers(Callback&& callback) const
    {
        for (auto& m : members_)
        {
            if (auto sm = m.lock())
            {
                if (callback(*sm) != Iteration::Continue)
                    break;
            }
        }
    }

};

}
