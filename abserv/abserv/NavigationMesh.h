/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <DetourNavMesh.h>
#include <DetourNavMeshQuery.h>
#include "Asset.h"
#include <libmath/Vector3.h>
#include <libcommon/NavigationDef.h>

namespace Navigation {

struct FindPathData;

/// Navigation Mesh constructed from Map and Obstacles
class NavigationMesh final : public IO::Asset
{
private:
    dtNavMesh* navMesh_{ nullptr };
    dtNavMeshQuery* navQuery_;
    ea::unique_ptr<dtQueryFilter> queryFilter_;
    ea::unique_ptr<FindPathData> pathData_;
public:
    NavigationMesh();
    ~NavigationMesh() override;

    static std::string GetStatusString(dtStatus status);

    void SetNavMesh(dtNavMesh* value);

    dtNavMesh* GetNavMesh() const { return navMesh_; }
    dtNavMeshQuery* GetNavQuery() const { return navQuery_; }

    /// Find a path between world space points. Return non-empty list of points if successful.
    /// Extents specifies how far off the navigation mesh the points can be.
    bool FindPath(ea::vector<Math::Vector3>& dest, const Math::Vector3& start, const Math::Vector3& end,
        const Math::Vector3& extends = Math::Vector3_One, const dtQueryFilter* filter = nullptr) const;
    /// Find the nearest point on the navigation mesh to a given point. Extents specifies how far out from the specified point to check along each axis.
    Math::Vector3 FindNearestPoint(const Math::Vector3& point, const Math::Vector3& extents = Math::Vector3_One,
        const dtQueryFilter* filter = nullptr, dtPolyRef* nearestRef = nullptr) const;
    bool FindRandomPoint(Math::Vector3& result, const Math::Vector3& point, float radius, const Math::Vector3& extents = Math::Vector3_One,
        const dtQueryFilter* filter = nullptr) const;
    // Check if an actor can step on a point on the navigation mesh.
    bool CanStepOn(const Math::Vector3& point, const Math::Vector3& extents = Math::Vector3_One,
        const dtQueryFilter* filter = nullptr, dtPolyRef* nearestRef = nullptr, Math::Vector3* nearestPoint = nullptr) const;
    bool GetHeight(float& result, const Math::Vector3& point, const Math::Vector3& extents = Math::Vector3_One,
        const dtQueryFilter* filter = nullptr, dtPolyRef* nearestRef = nullptr) const;
};

}
