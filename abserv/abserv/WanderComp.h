/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/Vector3.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Game {

class Npc;

namespace Components {

class WanderComp
{
    NON_COPYABLE(WanderComp)
    NON_MOVEABLE(WanderComp)
private:
    enum class Direction
    {
        Forward,
        Backward
    };
    Npc& owner_;

    ea::vector<Math::Vector3> route_;
    Direction direction_{ Direction::Forward };
    int currentIndex_{ -1 };
    bool wandering_{ false };
    bool initialized_{ false };
    int FindCurrentPointIndex() const;
    int GetNextIndex();
    bool GotoCurrentPoint();
    bool CheckDestination() const;
    void Initialize();
public:
    WanderComp() = delete;
    explicit WanderComp(Npc& owner);
    ~WanderComp() = default;

    void AddRoutePoint(const Math::Vector3& point);
    bool HaveRoute() const { return route_.size() != 0; }
    void Update(uint32_t timeElapsed);
    bool IsWandering() const { return wandering_; }
    bool Wander(bool value);
};

}
}
