/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProgressComp.h"
#include "Player.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <libcommon/NetworkMessage.h>

namespace Game {
namespace Components {

ProgressComp::ProgressComp(Actor& owner) :
    owner_(owner)
{
    owner_.SubscribeEvent<OnActorDiedSignature>(OnDiedEvent, std::bind(&ProgressComp::OnDied, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnKilledFoeSignature>(OnKilledFoeEvent, std::bind(&ProgressComp::OnKilledFoe,
        this, std::placeholders::_1, std::placeholders::_2));
}

void ProgressComp::Initiazlize()
{
    int xp = (int)owner_.GetXp();
    if (xp != 0)
        items_.push_back({ .type = ProgressType::XPInitial, .intValue = xp });
    int sp = (int)owner_.GetSkillPoints();
    if (sp != 0)
        items_.push_back({ .type = ProgressType::SPInitial, .intValue = sp });
}

void ProgressComp::Write(MessageStream& message)
{
    if (items_.empty())
        return;

    for (const auto& i : items_)
    {
        switch (i.type)
        {
        case ProgressType::XPIncrease:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::XPIncreased),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::XPInitial:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::XPInitial),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::SPInitial:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::SPInitial),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::GotSkillPoint:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::GotSkillPoint),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::LevelAdvance:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::LevelAdvance),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::TitleAdvance:
            // TODO:
            break;
        case ProgressType::AttributePointGain:
        {
            AB::Packets::Server::ObjectProgress packet = {
                owner_.id_,
                static_cast<uint8_t>(AB::GameProtocol::ObjectProgressType::AttribPointsGain),
                i.intValue
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectProgress, packet);
            break;
        }
        case ProgressType::ScaleChanged:
        {
            AB::Packets::Server::ObjectPropertyChanged packet;
            packet.objectId = owner_.id_;
            packet.fields = AB::Packets::Server::ObjectPropertyChanged::FieldScale;
            packet.newScale = i.floatValue;
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectPropertyChanged, packet);
            break;
        }
        }
    }
    items_.clear();
}

void ProgressComp::OnKilledFoe(Actor* foe, Actor*)
{
    // Get only XP for the first death of NPCs.
    if (foe && !owner_.IsDead() && (foe->GetDeaths() == 0 || foe->GetType() == AB::GameProtocol::GameObjectType::Player))
        // When the owner is not dead add some XP for killing this foe
        AddXpForKill(*foe);
}

void ProgressComp::OnDied(Actor*, Actor* killer)
{
    if (Is<Player>(owner_))
    {
        auto& player = To<Player>(owner_);
        player.deathStats_[AB::Entities::DeathStatIndexCount] =
            player.deathStats_[AB::Entities::DeathStatIndexCount].GetInt() + 1;
        player.deathStats_[AB::Entities::DeathStatIndexAtXp] = player.GetXp();
    }
    // When we die all Enemies in range get XP for that
    owner_.VisitEnemiesInRange(Range::Aggro, [&](const Actor& actor)
    {
        const_cast<Actor&>(actor).CallEvent<OnKilledFoeSignature>(OnKilledFoeEvent, &owner_, killer);
        return Iteration::Continue;
    });
    ++deaths_;
}

void ProgressComp::AddXp(int value)
{
    // Max XP
    if (value > static_cast<int>(SKILLPOINT_ADVANCE_XP))
        value = static_cast<int>(SKILLPOINT_ADVANCE_XP);

    const uint32_t oldXp = owner_.GetXp();
    owner_.AddXp(value);
    owner_.CallEvent<VoidIntSignature>(OnXPAddedEvent, value);
    items_.push_back({ .type = ProgressType::XPIncrease, .intValue = value });

    const uint32_t level = owner_.GetLevel();
    if (level < LEVEL_CAP)
    {
        const uint32_t nextLevelXp = (level + 1) * SKILLPOINT_ADVANCE_XP;
        if (owner_.GetXp() >= nextLevelXp)
        {
            AdvanceLevel();
            AddSkillPoint();
        }
    }
    else
    {
        const uint32_t x = oldXp / SKILLPOINT_ADVANCE_XP;
        const uint32_t y = owner_.GetXp() / SKILLPOINT_ADVANCE_XP;
        const uint32_t z = y - x;
        if (z > 0)
            AddSkillPoint();
    }
}

void ProgressComp::AddXpForKill(const Actor& victim)
{
    if (victim.GetSlaveKind() != SlaveKind::NoSlave)
        return;
    const size_t allyCount = owner_.GetAllyCountInRange(Range::Aggro);
    const int a = static_cast<int>(victim.GetLevel());
    const int b = static_cast<int>(owner_.GetLevel());
    int xp = (100 + 4 * std::abs(a - b) + 16 * (a - b)) / static_cast<int>(allyCount);
    if (xp < 0)
        // Negative XP is possible for low level foes
        xp = 0;
    AddXp(xp);
}

void ProgressComp::AddSkillPoint()
{
    if (owner_.GetType() == AB::GameProtocol::GameObjectType::Player)
        items_.push_back({ .type = ProgressType::GotSkillPoint, .intValue = 1 });
    owner_.AddSkillPoint();
    owner_.CallEvent<VoidIntSignature>(OnSkillPointAddedEvent, owner_.GetSkillPoints());
}

void ProgressComp::AdvanceLevel()
{
    if (owner_.GetLevel() >= LEVEL_CAP)
        // Max level reached
        return;

    const uint32_t oldLevel = owner_.GetLevel();

    const uint32_t oldAttribPoints = owner_.GetAttributePoints();
    owner_.AdvanceLevel();
    items_.push_back({ .type = ProgressType::LevelAdvance, .intValue = (int32_t)owner_.GetLevel() });
    if (oldLevel >= 2 && oldLevel < LEVEL_CAP)
    {
        const int attribPointsChange = static_cast<int>(owner_.GetAttributePoints()) - static_cast<int>(oldAttribPoints);
        if (attribPointsChange > 0)
            items_.push_back({ .type = ProgressType::AttributePointGain, .intValue = attribPointsChange });
    }
    if (oldLevel < LEVEL_CAP && owner_.GetSlaveKind() == SlaveKind::AnimalCompanion)
    {
        ScaleToLevel();
    }
    owner_.CallEvent<VoidIntSignature>(OnAdvanceLevelEvent, owner_.GetLevel());
}

void ProgressComp::ScaleToLevel()
{
    const float scale = GetScaleForLevel(owner_.GetLevel());
    const Math::Vector3 newScale = { scale, scale, scale };
    if (owner_.transformation_.scale_ != newScale)
    {
        owner_.transformation_.scale_ = newScale;
        auto it = ea::find_if(items_.begin(), items_.end(), [](const auto& current)
        {
            return current.type == ProgressType::ScaleChanged;
        });
        if (it == items_.end())
            items_.push_back({ .type = ProgressType::ScaleChanged, .floatValue = scale });
        else
            it->floatValue = scale;
    }
}

}
}
