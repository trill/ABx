/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Scene.h"

namespace Game {

Scene::Scene() = default;
Scene::~Scene() = default;

}
