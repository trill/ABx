/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Noncopyable.h>
#include <libshared/Team.h>

namespace Game {

class Actor;
class MessageStream;

namespace Components {

class GroupComp
{
    NON_COPYABLE(GroupComp)
    NON_MOVEABLE(GroupComp)
private:
    Actor& owner_;
    TeamColor groupColor_;
    /// Friend foe identification. Upper 16 bit is foe mask, lower 16 bit friend mask.
    /// This gives us 3 types of relation: (1) friend, (2) foe and (3) neutral
    /// and (4) in theory, both but that would be silly.
    uint32_t groupMask_{ 0 };
    // Group/Party ID
    uint32_t groupId_{ 0 };
    bool groupMaskChanged_{ false };
    bool groupColorChanged_{ false };
    bool groupIdChanged_{ false };
    bool resignedDirty_{ false };
    bool defeatedDirty_{ false };
    bool IsOwnerLeader() const;
public:
    explicit GroupComp(Actor& owner);
    ~GroupComp();

    TeamColor GetGroupColor() const { return groupColor_; }
    void SetGroupColor(TeamColor value);
    uint32_t GetGroupMask() const { return groupMask_; }
    void SetGroupMask(uint32_t value);
    uint32_t GetGroupId() const { return groupId_; }
    void SetGroupId(uint32_t value);
    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);
};

}

}
