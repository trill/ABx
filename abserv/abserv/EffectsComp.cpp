/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "EffectsComp.h"
#include "Actor.h"
#include "Effect.h"
#include "EffectManager.h"
#include "Item.h"
#include "Skill.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <libcommon/NetworkMessage.h>
#include <libcommon/Subsystems.h>

namespace Game {
namespace Components {

EffectsComp::EffectsComp(Actor& owner) :
    owner_(owner)
{
    owner_.SubscribeEvent<OnAttackEndSignature>(OnTargetAttackEndEvent, std::bind(
        &EffectsComp::OnTargetAttackEnd, this,
        std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnAttackStartSignature>(OnTargetAttackStartEvent, std::bind(&EffectsComp::OnTargetAttackStart, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnAttackStartSignature>(OnSourceAttackStartEvent, std::bind(&EffectsComp::OnSourceAttackStart, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnAttackSuccessSignature>(OnSourceAttackSuccessEvent, std::bind(&EffectsComp::OnSourceAttackSuccess, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnAttackBlockedSignature>(OnTargetAttackBlockedEvent, std::bind(&EffectsComp::OnTargetAttackBlocked, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnAttackSuccessSignature>(OnTargetAttackSuccessEvent, std::bind(&EffectsComp::OnTargetAttackSuccess, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

    owner_.SubscribeEvent<OnGetCriticalHitSignature>(OnGetCriticalHitEvent, std::bind(&EffectsComp::OnGetCriticalHit, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnInterruptingAttackSignature>(OnInterruptingAttackEvent, std::bind(&EffectsComp::OnInterruptingAttack, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnInterruptingSkillSignature>(OnInterruptingSkillEvent, std::bind(&EffectsComp::OnInterruptingSkill, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnCanUseSkillSignature>(OnCanUseSkillEvent, std::bind(&EffectsComp::OnCanUseSkill, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnGettingSkillTargetSignature>(OnGettingSkillTargetEvent, std::bind(&EffectsComp::OnGettingSkillTarget, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnSkillTargetedSignature>(OnSkillTargetedEvent, std::bind(&EffectsComp::OnSkillTargeted, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnKnockingDownSignature>(OnKnockingDownEvent, std::bind(&EffectsComp::OnKnockingDown, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnKnockedDownSignature>(OnKnockedDownEvent, std::bind(&EffectsComp::OnKnockedDown, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnHealingSignature>(OnHealingEvent, std::bind(&EffectsComp::OnHealing, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnHealingTargetSignature>(OnHealingTargetEvent, std::bind(&EffectsComp::OnHealingTarget, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnStartUseSkillSignature>(OnStartUseSkillEvent, std::bind(&EffectsComp::OnStartUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnEndUseSkillSignature>(OnEndUseSkillEvent, std::bind(&EffectsComp::OnEndUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<VoidIntSignature>(OnIncMoraleEvent, std::bind(&EffectsComp::OnMorale, this, std::placeholders::_1));
    owner_.SubscribeEvent<VoidIntSignature>(OnDecMoraleEvent, std::bind(&EffectsComp::OnMorale, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnAddEffectSignature>(OnAddEffectEvent, std::bind(&EffectsComp::OnAddEffect, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnRemoveEffectSignature>(OnRemoveEffectEvent, std::bind(&EffectsComp::OnRemoveEffect, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnKnockingDownTargetSignature>(OnKnockingDownTargetEvent, std::bind(&EffectsComp::OnKnockingDownTarget, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnInterruptingTargetSignature>(OnInterruptingTargetEvent, std::bind(&EffectsComp::OnInterruptingTarget, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnDealingDamageSignature>(OnDealingDamageEvent, std::bind(&EffectsComp::OnDealingDamage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnActorDiedSignature>(OnDiedEvent, std::bind(&EffectsComp::OnDied, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnGettingDamageSignature>(OnGettingDamageEvent, std::bind(&EffectsComp::OnGettingDamage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnGotDamageSignature>(OnGotDamageEvent, std::bind(&EffectsComp::OnGotDamage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    owner_.SubscribeEvent<OnShadowStepSignature>(OnShadowStepEvent, std::bind(&EffectsComp::OnShadowStep, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnWasInterruptedSignature>(OnWasInterruptedEvent, std::bind(&EffectsComp::OnWasInterrupted, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnInterruptedSignature>(OnInterruptedEvent, std::bind(&EffectsComp::OnInterrupted, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnHealedTargetSignature>(OnHealedTargetEvent, std::bind(&EffectsComp::OnHealedTarget, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    owner_.SubscribeEvent<OnEnergyZeroSignature>(OnEnergyZeroEvent, std::bind(&EffectsComp::OnEnergyZero, this));
    owner_.SubscribeEvent<OnSpawnedSlaveSignature>(OnSpawnedSlaveEvent, std::bind(&EffectsComp::OnSpawnedSlave, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnExploitedCorpseSignature>(OnExploitedCorpseEvent, std::bind(&EffectsComp::OnExploitedCorpse, this, std::placeholders::_1));
}

EffectsComp::~EffectsComp()
{
    RemoveAll();
}

void EffectsComp::MaintainEffect(ea::shared_ptr<Effect> effect)
{
    ASSERT(effect);
    maintainedEffects_.push_back(effect);

    auto* target = effect->GetTarget();
    uint32_t targetId = target ? target->id_ : 0;
    newMaintainingEffects_.emplace_back(effect->GetIndex(), targetId, true);
}

void EffectsComp::OnMorale(int morale)
{
    if (morale != 0)
        AddEffect(ea::shared_ptr<Actor>(), AB::Entities::EFFECT_INDEX_MORALE, 0);
    else
        RemoveEffect(AB::Entities::EFFECT_INDEX_MORALE);
}

size_t EffectsComp::RemoveAllOfCategory(AB::Entities::EffectCategory category)
{
    const auto check = [category](const ea::shared_ptr<Effect>& current)
    {
        return current->category_ == category;
    };

    size_t result = 0;
    auto it = ea::find_if(effects_.begin(), effects_.end(), check);
    while (it != effects_.end())
    {
        ++result;
        auto& seffect = (*it);
        removedEffects_.push_back(seffect);
        seffect->DoRemove();
        effects_.erase(it);
        it = ea::find_if(effects_.begin(), effects_.end(), check);
    }
    return result;
}

bool EffectsComp::HasEffectOf(AB::Entities::EffectCategory category)
{
    auto it = ea::find_if(effects_.begin(), effects_.end(), [category](const ea::shared_ptr<Effect>& current)
    {
        return current->category_ == category;
    });
    return it != effects_.end();
}

bool EffectsComp::AddEffect(ea::shared_ptr<Actor> source, uint32_t index, uint32_t time)
{
    auto effect = GetSubsystem<EffectManager>()->Get(index);
    if (!effect)
        return false;

    bool succ = true;
    // source adds an effect to my owner, see if an effect on us may let that fail
    owner_.CallEvent<OnAddEffectSignature>(OnAddEffectEvent, source.get(), effect.get(), succ, time);
    if (!succ)
        return false;

    // Effects are not stackable:
    /*
        * Eine Fertigkeit kann nicht mit sich selbst gestapelt werden, beispielsweise
        * kann man einen Gegner also nicht zweimal mit der gleichen Verhexung belegen,
        * sich selbst mehrfach durch ein und dieselbe Verzauberung schützen oder
        * den Bonus eines Schreis mehrfach erhalten.
        * https://www.guildwiki.de/wiki/Effektstapelung
        */
    RemoveEffect(index, true);
    // E.g. only one stance allowed
    if (effect->category_ >= SINGLEEFFECT_START && effect->category_ <= SINGLEEFFECT_END)
        RemoveAllOfCategory(effect->category_);
    if (effect->Start(source, owner_.GetPtr<Actor>(), time))
    {
        effects_.push_back(effect);
        addedEffects_.push_back(effect);
        if (source)
        {
            if (auto* skill = effect->GetCausingSkill())
            {
                if (skill->maintainable_)
                    source->effectsComp_->MaintainEffect(effect);
            }
        }
        return true;
    }
    return false;
}

bool EffectsComp::RemoveEffect(uint32_t index, bool force)
{
    auto it = ea::find_if(effects_.begin(), effects_.end(), [index](const ea::shared_ptr<Effect>& current)
    {
        return current->data_.index == index;
    });
    if (it != effects_.end())
    {
        (*it)->cancelled_ = true;
        (*it)->forceRemove_ = force;
        return true;
    }
    return false;
}

bool EffectsComp::HasEffect(uint32_t index) const
{
    const auto it = ea::find_if(effects_.begin(), effects_.end(), [index](const ea::shared_ptr<Effect>& current)
    {
        return current->data_.index == index;
    });
    return it != effects_.end();
}

Effect* EffectsComp::GetEffect(uint32_t index) const
{
    const auto it = ea::find_if(effects_.rbegin(), effects_.rend(), [index](const ea::shared_ptr<Effect>& current)
    {
        return current->data_.index == index;
    });
    if (it != effects_.rend())
        return (*it).get();
    return nullptr;
}


Effect* EffectsComp::GetLast(AB::Entities::EffectCategory category)
{
    for (auto i = effects_.rbegin(); i != effects_.rend(); ++i)
    {
        if ((*i)->category_ == category)
            return (*i).get();
    }
    return nullptr;
}

bool EffectsComp::RemoveLastEffectOf(AB::Entities::EffectCategory category)
{
    if (auto* e = GetLast(category))
        return RemoveEffect(e->GetIndex());
    return false;
}

void EffectsComp::RemoveAllEffectsFrom(uint32_t sourceId, uint32_t categories, bool force)
{
    const auto check = [sourceId, categories](const ea::shared_ptr<Effect>& current)
    {
        return (sourceId == 0 || current->GetSourceId() == sourceId) && (categories & (uint32_t)current->category_);
    };

    auto it = ea::find_if(effects_.begin(), effects_.end(), check);
    while (it != effects_.end())
    {
        auto& seffect = (*it);
        bool result = true;
        owner_.CallEvent<OnRemoveEffectSignature>(OnRemoveEffectEvent, seffect.get(), result);
        if (force || result)
        {
            removedEffects_.push_back(seffect);
            seffect->DoRemove();
            effects_.erase(it);
        }

        it = ea::find_if(effects_.begin(), effects_.end(), check);
    }
}

void EffectsComp::RemoveAll()
{
    {
        auto it = effects_.begin();
        while (it != effects_.end())
        {
            auto seffect = (*it);
            bool result = true;
            owner_.CallEvent<OnRemoveEffectSignature>(OnRemoveEffectEvent, seffect.get(), result);
            removedEffects_.push_back(seffect);
            seffect->DoRemove();
            it = effects_.erase(it);
        }
    }
    {
        auto it = maintainedEffects_.begin();
        while (it != maintainedEffects_.end())
        {
            auto seffect = (*it);
            seffect->DoRemove();
            it = maintainedEffects_.erase(it);
        }
    }
}

bool EffectsComp::RemoveMaintainedEffect(uint32_t effectIndex, uint32_t targetId)
{
    auto* effect = GetMaintainedEffect(effectIndex, targetId);
    if (!effect)
        return false;
    effect->cancelled_ = true;
    effect->forceRemove_ = true;
    return true;
}

Effect* EffectsComp::GetMaintainedEffect(uint32_t effectIndex, uint32_t targetId) const
{
    const auto it = ea::find_if(maintainedEffects_.begin(), maintainedEffects_.end(), [effectIndex, targetId](const ea::shared_ptr<Effect>& current)
    {
        if (current->data_.index != effectIndex)
            return false;
        auto* target = current->GetTarget();
        if (!target)
            return false;
        return target->id_ == targetId;
    });
    if (it == maintainedEffects_.end())
        return nullptr;
    return (*it).get();
}

void EffectsComp::Update(uint32_t timeElapsed)
{
    {
        auto it = effects_.begin();
        while (it != effects_.end())
        {
            auto seffect = (*it);
            if (seffect->cancelled_ || seffect->ended_)
            {
                bool result = true;
                owner_.CallEvent<OnRemoveEffectSignature>(OnRemoveEffectEvent, seffect.get(), result);
                if (result || seffect->forceRemove_)
                {
                    removedEffects_.push_back(seffect);
                    if (seffect->cancelled_)
                        seffect->DoRemove();
                    it = effects_.erase(it);
                    continue;
                }
            }
            seffect->Update(timeElapsed);
            ++it;
        }
    }
    {
        auto it = maintainedEffects_.begin();
        while (it != maintainedEffects_.end())
        {
            auto seffect = (*it);
            if (seffect->cancelled_ || seffect->ended_)
            {
                auto* target = seffect->GetTarget();
                uint32_t targetId = target ? target->id_ : 0;
                newMaintainingEffects_.emplace_back(seffect->GetIndex(), targetId, false);
                it = maintainedEffects_.erase(it);
            }
            else
                ++it;
        }
    }
}

void EffectsComp::Write(MessageStream& message)
{
    if (!removedEffects_.empty())
    {
        for (const auto& effect : removedEffects_)
        {
            if (effect->IsInternal())
                continue;
            AB::Packets::Server::ObjectEffectRemoved packet = {
                owner_.id_,
                effect->data_.index
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectEffectRemoved, packet);
        }
        removedEffects_.clear();
    }

    if (!addedEffects_.empty())
    {
        for (const auto& effect : addedEffects_)
        {
            if (effect->IsInternal())
                continue;
            auto* source = effect->GetSource();
            AB::Packets::Server::ObjectEffectAdded packet = {
                owner_.id_,
                source ? source->id_ : 0,
                effect->data_.index,
                effect->GetTicks()
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectEffectAdded, packet);
        }
        addedEffects_.clear();
    }

    if (!newMaintainingEffects_.empty())
    {
        for (const auto& effect : newMaintainingEffects_)
        {
            AB::Packets::Server::ObjectMaintainingEffect packet = {
                owner_.id_,
                effect.effectIndex,
                effect.targetId,
                effect.maintained
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectMaintainingEffect, packet);
        }
        newMaintainingEffects_.clear();
    }
}

void EffectsComp::GetSkillRecharge(Skill* skill, uint32_t& recharge)
{
    for (const auto& effect : effects_)
        effect->GetSkillRecharge(skill, recharge);
}

void EffectsComp::GetSkillActivation(Skill* skill, uint32_t& activation)
{
    for (const auto& effect : effects_)
        effect->GetSkillActivation(skill, activation);
}

void EffectsComp::GetSkillHpSacrifies(Skill* skill, uint32_t& hp)
{
    for (const auto& effect : effects_)
        effect->GetSkillHpSacrifies(skill, hp);
}

void EffectsComp::GetSkillCost(Skill* skill,
    int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp)
{
    for (const auto& effect : effects_)
        effect->GetSkillCost(skill, activation, energy, adrenaline, overcast, hp);
}

void EffectsComp::GetTargetSkillCost(Actor* source, Skill* skill, int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp)
{
    for (const auto& effect : effects_)
        effect->GetTargetSkillCost(source, skill, activation, energy, adrenaline, overcast, hp);
}

void EffectsComp::GetDamage(DamageType type, int32_t& value, bool& critical)
{
    for (const auto& effect : effects_)
        effect->GetDamage(type, value, critical);
}

void EffectsComp::GetAttackSpeed(Item* weapon, uint32_t& value)
{
    for (const auto& effect : effects_)
        effect->GetAttackSpeed(weapon, value);
}

void EffectsComp::GetAttackDamageType(DamageType& type)
{
    for (const auto& effect : effects_)
        effect->GetAttackDamageType(type);
}

void EffectsComp::GetAttackDamage(int32_t& value)
{
    for (const auto& effect : effects_)
        effect->GetAttackDamage(value);
}

void EffectsComp::GetArmor(DamageType type, int& value)
{
    for (const auto& effect : effects_)
        effect->GetArmor(type, value);
}

void EffectsComp::GetArmorPenetration(DamageType damageType, float& value)
{
    for (const auto& effect : effects_)
        effect->GetArmorPenetration(damageType, value);
}

void EffectsComp::GetSourceArmorPenetration(Actor* target, DamageType damageType, float& value)
{
    for (const auto& effect : effects_)
        effect->GetSourceArmorPenetration(target, damageType, value);
}

void EffectsComp::GetAttributeRank(Attribute index, int32_t& value)
{
    for (const auto& effect : effects_)
        effect->GetAttributeRank(index, value);
}

void EffectsComp::GetResources(int& maxHealth, int& maxEnergy)
{
    for (const auto& effect : effects_)
        effect->GetRecources(maxHealth, maxEnergy);
}

void EffectsComp::GetRegeneration(int& healthRegen, int& energyRegen)
{
    for (const auto& effect : effects_)
        effect->GetRegeneration(healthRegen, energyRegen);
    for (const auto& effect : maintainedEffects_)
        effect->GetMaintainerRegeneration(healthRegen, energyRegen);
}

void EffectsComp::GetSpeedFactor(float& speedFactor)
{
    for (const auto& effect : effects_)
    {
        bool stop = false;
        effect->GetSpeedFactor(speedFactor, stop);
        if (stop)
            break;
    }
}

void EffectsComp::ActorDied(Actor* actor, Actor* killer)
{
    if (actor && actor != &owner_)
    {
        // If an actor dies, all their effects on us are removed
        RemoveAllEffectsFrom(actor->id_,
            (uint32_t)EffectCategories::Enchantment | (uint32_t)EffectCategories::Hex | (uint32_t)EffectCategories::Shout |
            (uint32_t)EffectCategories::Skill | (uint32_t)EffectCategories::Preparation | (uint32_t)EffectCategories::Stance |
            (uint32_t)EffectCategories::Form | (uint32_t)EffectCategories::Glyphe | (uint32_t)EffectCategories::WeaponSpell |
            (uint32_t)EffectCategories::Signet, true);
    }

    for (const auto& effect : effects_)
        effect->OnActorDied(actor, killer);
}

void EffectsComp::OnSourceAttackStart(Actor* target, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnSourceAttackStart(&owner_, target, value);
    }
}

void EffectsComp::OnSourceAttackSuccess(Actor* target, DamageType type, int32_t damage)
{
    for (const auto& effect : effects_)
        effect->OnSourceAttackSuccess(&owner_, target, type, damage);
}

void EffectsComp::OnTargetAttackSuccess(Actor* source, DamageType type, int32_t damage)
{
    for (const auto& effect : effects_)
        effect->OnTargetAttackSuccess(source, &owner_, type, damage);
}

void EffectsComp::OnTargetAttackEnd(Actor* source, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error)
{
    for (const auto& effect : effects_)
    {
        if (error != AB::GameProtocol::AttackError::None)
            return;
        effect->OnTargetAttackEnd(source, &owner_, type, damage, error);
    }
}

void EffectsComp::OnTargetAttackBlocked(Actor* source)
{
    for (const auto& effect : effects_)
        effect->OnTargetAttackBlocked(source, &owner_);
}

void EffectsComp::OnTargetAttackStart(Actor* source, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnTargetAttackStart(source, &owner_, value);
    }
}

void EffectsComp::OnCanUseSkill(Actor* target, Skill* skill, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnCanUseSkill(&owner_, target, skill, value);
    }
}

void EffectsComp::OnGettingSkillTarget(Actor* source, Skill* skill, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnGettingSkillTarget(source, &owner_, skill, value);
    }
}

void EffectsComp::OnWasInterrupted(Actor* source, Skill* skill)
{
    for (const auto& effect : effects_)
    {
        effect->OnWasInterrupted(source, &owner_, skill);
    }
}

void EffectsComp::OnInterruptingAttack(bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnInterruptingAttack(value);
    }
}

void EffectsComp::OnInterruptingSkill(Actor* source, AB::Entities::SkillType type, Skill* skill, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnInterruptingSkill(source, &owner_, type, skill, value);
    }
}

void EffectsComp::OnSkillTargeted(Actor* source, Skill* skill)
{
    for (const auto& effect : effects_)
        effect->OnSkillTargeted(source, &owner_, skill);
}

void EffectsComp::OnKnockingDown(Actor* source, uint32_t time, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnKnockingDown(source, &owner_, time, value);
    }
}

void EffectsComp::OnKnockedDown(uint32_t time)
{
    for (const auto& effect : effects_)
        effect->OnKnockedDown(&owner_, time);
}

void EffectsComp::OnHealing(Actor* source, int& value)
{
    for (const auto& effect : effects_)
        effect->OnHealing(source, &owner_, value);
}

void EffectsComp::OnHealingTarget(Actor* source, int& value)
{
    for (const auto& effect : effects_)
        effect->OnHealingTarget(source, &owner_, value);
}

void EffectsComp::OnStartUseSkill(Actor* target, Skill* skill)
{
    for (const auto& effect : effects_)
        effect->OnStartUseSkill(&owner_, target, skill);
}

void EffectsComp::OnEndUseSkill(Actor* target, Skill* skill)
{
    for (const auto& effect : effects_)
        effect->OnEndUseSkill(&owner_, target, skill);
}

void EffectsComp::OnTargetSkillSuccess(Actor* source, Skill* skill)
{
    for (const auto& effect : effects_)
        effect->OnTargetSkillSuccess(source, &owner_, skill);
}

void EffectsComp::OnGetCriticalHit(Actor* source, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnGetCriticalHit(source, &owner_, value);
    }
}

void EffectsComp::OnAddEffect(Actor* source, Effect* e, bool& value, uint32_t time)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnAddEffect(source, &owner_, e, value, time);
    }
}

void EffectsComp::OnRemoveEffect(Effect* e, bool& value)
{
    for (const auto& effect : effects_)
    {
        if (!value)
            return;
        effect->OnRemoveEffect(&owner_, e, value);
    }
}

void EffectsComp::OnKnockingDownTarget(Actor* source, uint32_t time, bool& success)
{
    for (const auto& effect : effects_)
    {
        if (!success)
            return;
        effect->OnKnockingDownTarget(source, &owner_, time, success);
    }
}

void EffectsComp::OnInterruptingTarget(Actor* source, bool& success)
{
    for (const auto& effect : effects_)
    {
        if (!success)
            return;
        effect->OnInterruptingTarget(source, &owner_, success);
    }
}

void EffectsComp::OnGettingDamage(Actor* source, Skill* skill, DamageType type, int32_t& value)
{
    for (const auto& effect : effects_)
        effect->OnGettingDamage(source, &owner_, skill, type, value);
}

void EffectsComp::OnGotDamage(Actor* source, Skill* skill, DamageType type, int32_t value)
{
    for (const auto& effect : effects_)
        effect->OnGotDamage(source, &owner_, skill, type, value);
}

void EffectsComp::OnShadowStep(const Math::Vector3& pos, bool& success)
{
    for (const auto& effect : effects_)
    {
        if (!success)
            break;
        effect->OnShadowStep(&owner_, pos, success);
    }
}

void EffectsComp::OnInterrupted(Actor* target, Skill* skill, bool success)
{
    for (const auto& effect : effects_)
        effect->OnInterrupted(&owner_, target, skill, success);
}

void EffectsComp::OnHealedTarget(Actor* target, Skill* skill, int value)
{
    for (const auto& effect : effects_)
        effect->OnHealedTarget(&owner_, target, skill, value);
}

void EffectsComp::OnEnergyZero()
{
    for (const auto& effect : effects_)
        effect->OnEnergyZero(&owner_);
    for (const auto& effect : maintainedEffects_)
        effect->OnMaintainerEnergyZero(&owner_);
}

void EffectsComp::OnSpawnedSlave(Actor* slave)
{
    for (const auto& effect : effects_)
        effect->OnSpawnedSlave(&owner_, slave);
}

void EffectsComp::OnExploitedCorpse(Actor* corpse)
{
    for (const auto& effect : effects_)
        effect->OnExploitedCorpse(&owner_, corpse);
}

void EffectsComp::OnDealingDamage(Actor* target, DamageType type, int32_t& value, bool& success)
{
    for (const auto& effect : effects_)
    {
        if (!success)
            return;
        effect->OnDealingDamage(&owner_, target, type, value, success);
    }
}

void EffectsComp::OnDied(Actor* actor, Actor* killer)
{
    // We (the actor) died
    for (const auto& effect : effects_)
        effect->OnDied(actor, killer);
    // If we die, all effects on us are removed
    RemoveAllEffectsFrom(0,
        (uint32_t)EffectCategories::Enchantment | (uint32_t)EffectCategories::Hex | (uint32_t)EffectCategories::Shout |
        (uint32_t)EffectCategories::Skill | (uint32_t)EffectCategories::Preparation | (uint32_t)EffectCategories::Stance |
        (uint32_t)EffectCategories::Form | (uint32_t)EffectCategories::Glyphe | (uint32_t)EffectCategories::WeaponSpell |
        (uint32_t)EffectCategories::Signet, true);
}

void EffectsComp::GetSkillDamageBonus(Actor* target, Skill* skill, DamageType type, int& value)
{
    for (const auto& effect : effects_)
        effect->GetSkillDamageBonus(&owner_, target, skill, type, value);
}

void EffectsComp::GetEnergyFromSoulReaping(Actor* target, int& value)
{
    for (const auto& effect : effects_)
        effect->GetEnergyFromSoulReaping(&owner_, target, value);
}

void EffectsComp::VisitEffectsOf(AB::Entities::EffectCategory category, const std::function<Iteration(Effect*)>& callback)
{
    for (const auto& effect : effects_)
    {
        if (effect->category_ == category)
        {
            if (callback(effect.get()) == Iteration::Break)
                break;
        }
    }
}
void EffectsComp::VisitEffects(const std::function<Iteration(Effect*)>& callback)
{
    for (const auto& effect : effects_)
    {
        if (callback(effect.get()) == Iteration::Break)
            break;
    }
}

}
}
