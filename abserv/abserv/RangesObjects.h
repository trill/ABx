/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libshared/Mechanic.h>
#include <eastl.hpp>
#include <sa/Iteration.h>
#include <robin_hood.h>

namespace Game {

class RangesObjects
{
private:
    ea::array<robin_hood::unordered_set<uint32_t>, static_cast<size_t>(Range::Map) + 1> objects_;
public:
    void Add(Range range, uint32_t id);
    void Clear();
    bool Contains(Range range, uint32_t id) const;
    const robin_hood::unordered_set<uint32_t>& operator[](Range range) const
    {
        return objects_[static_cast<size_t>(range)];
    }
    template<typename Callback>
    void VisitRanges(Callback&& callback) const
    {
        // Last range is Range::Map, it does not contain objects
        for (size_t i = 0; i < objects_.size() - 1; ++i)
        {
            if (callback(static_cast<Range>(i), objects_[i]) != Iteration::Continue)
                break;
        }
    }
    template<typename Callback>
    void VisitRangesReverse(Callback&& callback) const
    {
        // Last range is Range::Map, it does not contain objects
        for (ssize_t i = (ssize_t)objects_.size() - 2; i >= 0; --i)
        {
            if (callback(static_cast<Range>(i), objects_[i]) != Iteration::Continue)
                break;
        }
    }
    Range GetRangeOfObject(uint32_t id) const;
};

}
