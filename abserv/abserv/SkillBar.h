/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillDefs.h"
#include <queue>
#include <AB/Entities/Profession.h>
#include <AB/Entities/Skill.h>
#include <AB/ProtocolCodes.h>
#include <libshared/Mechanic.h>
#include <libshared/TemplEncoder.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Game {

class Actor;
class Skill;

typedef ea::array<ea::shared_ptr<Skill>, PLAYER_MAX_SKILLS> SkillsArray;

class SkillBar
{
    NON_COPYABLE(SkillBar)
    NON_MOVEABLE(SkillBar)
private:
    SkillsArray skills_;
    Attributes attributes_;
    Actor& owner_;
    int currentBarIndex_{ -1 };
    int _LuaAddSkill(uint32_t skillIndex);
    bool _LuaSetSkill(int pos, uint32_t skillIndex);
    std::vector<uint32_t> _LuaGetSkillsWithEffect(uint32_t effect) const
    {
        const auto res = GetSkillsWithEffect(static_cast<SkillEffect>(effect));
        std::vector<uint32_t> luares;
        ea::copy(res.begin(), res.end(), std::back_inserter(luares));
        return luares;
    }
    std::vector<uint32_t> _LuaGetSkillsWithTarget(uint32_t target) const
    {
        const auto res = GetSkillsWithTarget(static_cast<SkillEffectTarget>(target));
        std::vector<uint32_t> luares;
        ea::copy(res.begin(), res.end(), std::back_inserter(luares));
        return luares;
    }
    std::vector<uint32_t> _LuaGetSkillsWithEffectTarget(uint32_t effect, uint32_t target) const
    {
        const auto res = GetSkillsWithEffectTarget(static_cast<SkillEffect>(effect), static_cast<SkillEffectTarget>(target));
        std::vector<uint32_t> luares;
        ea::copy(res.begin(), res.end(), std::back_inserter(luares));
        return luares;
    }
    std::vector<uint32_t> _LuaGetSkillsOfType(AB::Entities::SkillType type) const
    {
        const auto res = GetSkillsOfType(type);
        std::vector<uint32_t> luares;
        ea::copy(res.begin(), res.end(), std::back_inserter(luares));
        return luares;
    }
    Skill* _LuaGetSkill(int pos);
    std::vector<Skill*> _LuaGetSkills();
    std::vector<Attribute> _LuaGetAttributeIndices();
    Skill* _LuaGetSkillBySkillIndex(uint32_t skillIndex);
    int _LuaGetSkillCount() const;
    void SetAttributes(const Attributes& attributes);
    void ResetAttributes();
    bool HaveAttribute(uint32_t index);
public:
    static void RegisterLua(kaguya::State& state);

    explicit SkillBar(Actor& owner) :
        owner_(owner)
    { }
    ~SkillBar() = default;

    /// 0 Based
    AB::GameProtocol::SkillError UseSkill(int index, ea::shared_ptr<Actor> target);
    Skill* GetCurrentSkill() const;
    uint32_t GetCurrentSkillIndex() const;
    void Update(uint32_t timeElapsed);
    std::string Encode() const;
    // Load skills from a template. if locked = true also locked skills are allowed.
    bool Load(const std::string& str, bool locked);
    bool SetSecondaryProfession(uint32_t index);

    void ResetSkills();
    ea::shared_ptr<Skill> GetSkill(int pos);
    ea::shared_ptr<Skill> GetSkillBySkillIndex(uint32_t skillIndex);
    uint32_t GetIndexOfSkill(int pos);
    int GetPosOfSkill(uint32_t index);
    bool SetSkill(int pos, ea::shared_ptr<Skill> skill);
    bool RemoveSkill(int pos)
    {
        if (pos < 0 || pos >= PLAYER_MAX_SKILLS)
            return false;
        skills_[static_cast<size_t>(pos)].reset();
        return true;
    }

    void InitAttributes();
    // Check for a skill the cases the Companion to spwan.
    bool ActivatesCompanion() const;
    /// Get an attribute
    /// @param index The index of the attribute, not the index in the array
    /// @return The attribute or nullptr
    const AttributeValue* GetAttribute(Attribute index) const;
    const Attributes& GetAttributes() const { return attributes_; }
    bool SetAttributeRank(Attribute index, uint32_t value);
    uint32_t GetAttributeRank(Attribute index) const;
    int GetUsedAttributePoints() const;
    int GetAvailableAttributePoints() const;
    const SkillsArray& GetArray() const { return skills_; }
    ea::vector<uint32_t> GetSkillsWithEffect(SkillEffect effect, bool rechargedOnly = false) const;
    ea::vector<uint32_t> GetSkillsWithTarget(SkillEffectTarget target, bool rechargedOnly = false) const;
    ea::vector<uint32_t> GetSkillsWithEffectTarget(SkillEffect effect, SkillEffectTarget target, bool rechargedOnly = false) const;
    ea::vector<uint32_t> GetSkillsOfType(AB::Entities::SkillType type, bool rechargedOnly = false) const;
    bool HasSkillsOfType(AB::Entities::SkillType type, bool rechargedOnly = false) const;
    void RechargeAll();

    std::string GetClasses() const;

    Skill* operator[](uint32_t index)
    {
        return skills_[index].get();
    }
    template <typename Callback>
    void VisitSkills(Callback&& callback) const
    {
        int index = 0;
        for (const auto& skill : skills_)
        {
            if (skill)
                if (callback(index, *skill) != Iteration::Continue)
                    break;
            ++index;
        }
    }

    AB::Entities::Profession prof1_;
    AB::Entities::Profession prof2_;
};

}
