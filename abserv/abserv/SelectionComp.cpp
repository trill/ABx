/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SelectionComp.h"
#include <AB/ProtocolCodes.h>
#include "Actor.h"
#include "Game.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <libshared/Mechanic.h>

namespace Game {
namespace Components {

SelectionComp::~SelectionComp() = default;

void SelectionComp::Update(uint32_t)
{
    auto* object = GetSelectedObject();
    if (object)
    {
        if (owner_.GetDistance(object) > RANGE_SELECT)
            SelectObject(0);
    }
}

void SelectionComp::Write(MessageStream& message)
{
    if (prevObjectId_ == currObjectId_)
        return;

    AB::Packets::Server::ObjectTargetSelected packet = {
        owner_.id_,
        currObjectId_
    };
    message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectTargetSelected, packet);

    prevObjectId_ = currObjectId_;
}

bool SelectionComp::SelectObject(uint32_t targetId)
{
    auto* target = owner_.GetGame()->GetObject<GameObject>(targetId);
    // 0 = unselecting object
    if ((targetId != 0) && (!target || !target->selectable_))
        return false;
    if (currObjectId_ == targetId)
        return true;

    if (targetId != 0)
    {
        if (owner_.GetDistance(target) > RANGE_SELECT)
            return false;
    }

    currObjectId_ = targetId;
    if (target)
        target->CallEvent<OnSelectedSignature>(OnSelectedEvent, &owner_);
    return true;
}

bool SelectionComp::ClickObject(uint32_t targetId)
{
    auto* clickedObj = owner_.GetGame()->GetObject<GameObject>(targetId);
    if (clickedObj)
    {
        clickedObj->CallEvent<OnClickedSignature>(OnClickedEvent, &owner_);
        return true;
    }
    return false;
}

GameObject* SelectionComp::GetSelectedObject() const
{
    if (currObjectId_ == 0)
        return nullptr;
    if (auto g = owner_.GetGame())
        return g->GetObject<GameObject>(currObjectId_);
    return nullptr;
}

}
}
