/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Actor.h"
#include "AreaOfEffect.h"
#include "AttackComp.h"
#include "AutoRunComp.h"
#include "CollisionComp.h"
#include "ConfigManager.h"
#include "DamageComp.h"
#include "EffectsComp.h"
#include "InputComp.h"
#include "InventoryComp.h"
#include "MoveComp.h"
#include "Effect.h"
#include "EffectManager.h"
#include "Group.h"
#include "GroupComp.h"
#include "Game.h"
#include "GameManager.h"
#include "Group.h"
#include "HealComp.h"
#include "SkillsComp.h"
#include "Item.h"
#include "ItemFactory.h"
#include "ItemsCache.h"
#include "Skill.h"
#include "SkillBar.h"
#include "SelectionComp.h"
#include <libmath/OctreeQuery.h>
#include "PartyManager.h"
#include "Player.h"
#include "ProgressComp.h"
#include <AB/ProtocolCodes.h>
#include <libshared/AttribAlgos.h>
#include <libshared/Attributes.h>
#include <libshared/CollisionLayers.h>
#include <libcommon/Dispatcher.h>
#include <libcommon/Scheduler.h>
#include <sa/Bits.h>
#include <sa/Checked.h>
#include "Npc.h"

namespace Game {

// This BB is really small for an Actor, but the Actor should stuck only when there is really no way.
static constexpr Math::Vector3 CREATURE_BB_MIN{ -0.1f, 0.0f, -0.1f };
static constexpr Math::Vector3 CREATURE_BB_MAX{ 0.1f, 1.7f, 0.1f };

void Actor::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Actor"].setClass(std::move(kaguya::UserdataMetatable<Actor, GameObject>()
        .addFunction("AddAOE", &Actor::_LuaAddAOE)
        .addFunction("AddEffect", &Actor::_LuaAddEffect)
        .addFunction("AddEnergy", &Actor::AddEnergy)
        .addFunction("AddFriendFoe", &Actor::AddFriendFoe)
        .addFunction("AddLife", &Actor::AddLife)
        .addFunction("AddSpeed", &Actor::AddSpeed)
        .addFunction("Attack", &Actor::Attack)
        .addFunction("CancelAction", &Actor::CancelAction)
        .addFunction("CanResurrect", &Actor::CanResurrect)
        .addFunction("Damage", &Actor::Damage)
        .addFunction("DecreaseMorale", &Actor::DecreaseMorale)
        .addFunction("Die", &Actor::Die)
        .addFunction("DrainEnergy", &Actor::DrainEnergy)
        .addFunction("DrainLife", &Actor::DrainLife)
        .addFunction("DropRandomItem", &Actor::DropRandomItem)
        .addFunction("ExploitedCorpse", &Actor::ExploitedCorpse)
        .addFunction("FaceObject", &Actor::FaceObject)
        .addFunction("FollowObject", &Actor::_LuaFollowObject)
        .addFunction("ForcePositionUpdate", &Actor::_LuaForcePositionUpdate)
        .addFunction("GetActorsInRange", &Actor::_LuaGetActorsInRange)
        .addFunction("GetAliveActorsInRange", &Actor::_LuaGetAliveActorsInRange)
        .addFunction("GetAlliesInRange", &Actor::_LuaGetAlliesInRange)
        .addFunction("GetAllyCountInRange", &Actor::GetAllyCountInRange)
        .addFunction("GetAttackDamage", &Actor::GetAttackDamage)
        .addFunction("GetAttackDamageType", &Actor::GetAttackDamageType)
        .addFunction("GetAttackSpeed", &Actor::GetAttackSpeed)
        .addFunction("GetAttributeRank", &Actor::GetAttributeRank)
        .addFunction("GetClosestAlly", &Actor::GetClosestAlly)
        .addFunction("GetClosestEnemy", &Actor::GetClosestEnemy)
        .addFunction("GetCompanion", &Actor::GetCompanion)
        .addFunction("GetCriticalChance", &Actor::GetCriticalChance)
        .addFunction("GetCurrentSkill", &Actor::GetCurrentSkill)
        .addFunction("GetDamagePos", &Actor::GetDamagePos)
        .addFunction("GetDeadActorsInRange", &Actor::_LuaGetDeadActorsInRange)
        .addFunction("GetDeaths", &Actor::GetDeaths)
        .addFunction("GetEffects", &Actor::_LuaGetEffects)
        .addFunction("GetEffectsOf", &Actor::_LuaGetEffectsOf)
        .addFunction("GetEnemiesInRange", &Actor::_LuaGetEnemiesInRange)
        .addFunction("GetEnemyCountInRange", &Actor::GetEnemyCountInRange)
        .addFunction("GetGroupMembersRange", &Actor::_LuaGetGroupMembersRange)
        .addFunction("GetHealthRatio", &Actor::_LuaGetHealthRatio)
        .addFunction("GetItemIndex", &Actor::GetItemIndex)
        .addFunction("GetLastEffect", &Actor::_LuaGetLastEffect)
        .addFunction("GetLevel", &Actor::GetLevel)
        .addFunction("GetMaxAttribPoints", &Actor::_LuaGetMaxAttribPoints)
        .addFunction("GetMinions", &Actor::_LuaGetMinions)
        .addFunction("GetMorale", &Actor::GetMorale)
        .addFunction("GetResource", &Actor::GetResource)
        .addFunction("GetSkillBar", &Actor::_LuaGetSkillBar)
        .addFunction("GetSkillPoints", &Actor::GetSkillPoints)
        .addFunction("GetSlaves", &Actor::_LuaGetSlaves)
        .addFunction("GetWeapon", &Actor::GetWeapon)
        .addFunction("GetXp", &Actor::GetXp)
        .addFunction("GotoHomePos", &Actor::GotoHomePos)
        .addFunction("GotoPosition", &Actor::_LuaGotoPosition)
        .addFunction("HasEffect", &Actor::_LuaHasEffect)
        .addFunction("HasEffectOf", &Actor::_LuaHasEffectOf)
        .addFunction("HeadTo", &Actor::_LuaHeadTo)
        .addFunction("Healing", &Actor::Healing)
        .addFunction("IncreaseMorale", &Actor::IncreaseMorale)
        .addFunction("Interrupt", &Actor::Interrupt)
        .addFunction("InterruptAttack", &Actor::InterruptAttack)
        .addFunction("InterruptSkill", &Actor::InterruptSkill)
        .addFunction("IsAlly", &Actor::IsAlly)
        .addFunction("IsAttacked", &Actor::IsAttacked)
        .addFunction("IsAttackingActor", &Actor::IsAttackingActor)
        .addFunction("IsControllingMinions", &Actor::_LuaIsControllingMinions)
        .addFunction("IsDead", &Actor::IsDead)
        .addFunction("IsEnemy", &Actor::IsEnemy)
        .addFunction("IsGroupAttacked", &Actor::IsGroupAttacked)
        .addFunction("IsGroupFighting", &Actor::IsGroupFighting)
        .addFunction("IsHitting", &Actor::IsHitting)
        .addFunction("IsImmobilized", &Actor::IsImmobilized)
        .addFunction("IsInWeaponRange", &Actor::IsInWeaponRange)
        .addFunction("IsKnockedDown", &Actor::IsKnockedDown)
        .addFunction("IsMoving", &Actor::IsMoving)
        .addFunction("IsMyMinion", &Actor::_LuaIsMyMinion)
        .addFunction("IsUsingSkillOfType", &Actor::IsUsingSkillOfType)
        .addFunction("KnockDown", &Actor::KnockDown)
        .addFunction("MakeCompanion", &Actor::MakeCompanion)
        .addFunction("RemoveAllEffectsOf", &Actor::_LuaRemoveAllEffectsOf)
        .addFunction("RemoveEffect", &Actor::RemoveEffect)
        .addFunction("RemoveFriendFoe", &Actor::RemoveFriendFoe)
        .addFunction("RemoveLastEffectOf", &Actor::_LuaRemoveLastEffectOf)
        .addFunction("Resurrect", &Actor::Resurrect)
        .addFunction("ScaleToLevel", &Actor::_LuaScaleToLevel)
        .addFunction("SetEnergyRegen", &Actor::SetEnergyRegen)
        .addFunction("SetHealthRegen", &Actor::SetHealthRegen)
        .addFunction("SetResource", &Actor::SetResource)
        .addFunction("SetSpawnPoint", &Actor::SetSpawnPoint)
        .addFunction("ShadowStep", &Actor::_LuaShadowStep)
        .addFunction("SpawnSlave", &Actor::_LuaSpawnSlave)
        .addFunction("UseSkill", &Actor::UseSkill)

        .addProperty("AttackError", &Actor::_LuaGetAttackError, &Actor::_LuaSetAttackError)
        .addProperty("ArmorAddition", &Actor::_LuaGetArmorAddition, &Actor::_LuaSetArmorAddition)
        .addProperty("Group", &Actor::GetGroup)
        .addProperty("GroupColor", &Actor::GetGroupColor, &Actor::SetGroupColor)
        .addProperty("GroupId", &Actor::GetGroupId, &Actor::SetGroupId)
        .addProperty("HomePos", &Actor::_LuaGetHomePos, &Actor::_LuaSetHomePos)
        .addProperty("Master", &Actor::_LuaGetMaster, &Actor::_LuaSetMaster)
        .addProperty("Recycleable", &Actor::IsRecycleable, &Actor::SetRecycleable)
        .addProperty("Resurrectable", &Actor::IsResurrectable, &Actor::SetResurrectable)
        .addProperty("SelectedObject", &Actor::_LuaGetSelectedObject, &Actor::_LuaSetSelectedObject)
        .addProperty("SlaveKind", &Actor::GetSlaveKind, &Actor::SetSlaveKind)
        .addProperty("Species", &Actor::GetSpecies, &Actor::SetSpecies)
        .addProperty("Speed", &Actor::GetSpeed, &Actor::SetSpeed)
        .addProperty("Target", &Actor::_LuaGetTarget)
        .addProperty("Undestroyable", &Actor::IsUndestroyable, &Actor::SetUndestroyable)
    ));
    // clang-format on
}

Actor::Actor() :
    skills_(ea::make_unique<SkillBar>(*this)),
    resourceComp_(ea::make_unique<Components::ResourceComp>(*this)),
    attackComp_(ea::make_unique<Components::AttackComp>(*this)),
    skillsComp_(ea::make_unique<Components::SkillsComp>(*this)),
    inputComp_(ea::make_unique<Components::InputComp>(*this)),
    damageComp_(ea::make_unique<Components::DamageComp>(*this)),
    healComp_(ea::make_unique<Components::HealComp>(*this)),
    autorunComp_(ea::make_unique<Components::AutoRunComp>(*this)),
    progressComp_(ea::make_unique<Components::ProgressComp>(*this)),
    effectsComp_(ea::make_unique<Components::EffectsComp>(*this)),
    inventoryComp_(ea::make_unique<Components::InventoryComp>(*this)),
    moveComp_(ea::make_unique<Components::MoveComp>(*this)),
    collisionComp_(ea::make_unique<Components::CollisionComp>(*this)),    // Actor always collides
    selectionComp_(ea::make_unique<Components::SelectionComp>(*this)),
    groupComp_(ea::make_unique<Components::GroupComp>(*this))
{
    events_.Subscribe<OnEndUseSkillSignature>(OnEndUseSkillEvent, std::bind(&Actor::OnEndUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnStartUseSkillSignature>(OnStartUseSkillEvent, std::bind(&Actor::OnStartUseSkill, this, std::placeholders::_1, std::placeholders::_2));

    SetCollisionMask(ACTOR_COLLISION_MASK);
    /*
     * Default BB for humans
     * <attribute name="Size" value="0.3 1.7 0.5"/>
     * <attribute name="Offset Position" value="0 0.85 0"/>
    */
    SetCollisionShape(
        ea::make_unique<Math::CollisionShape<Math::BoundingBox>>(Math::ShapeType::BoundingBox, CREATURE_BB_MIN, CREATURE_BB_MAX)
    );
    SetOccluder(true);
    // By default Actors can be selected
    selectable_ = true;
}

Actor::~Actor()
{
    UnsubscribeEvents();
}

void Actor::Initialize()
{
    resourceComp_->UpdateResources();
    resourceComp_->SetHealth(Components::SetValueType::Absolute, resourceComp_->GetMaxHealth());
    resourceComp_->SetEnergy(Components::SetValueType::Absolute, resourceComp_->GetMaxEnergy());
    progressComp_->Initiazlize();
    moveComp_->StoreOldPosition();
}

bool Actor::SetSpawnPoint(const std::string& group)
{
    if (!HasGame())
        return false;
    auto game = GetGame();
    ASSERT(game);
    const auto sps = game->map_->GetSpawnPoints(group);
    const auto sp = game->map_->GetFreeSpawnPoint(sps);
    if (sp.Empty())
        return false;

    transformation_.position_ = sp.position;
    transformation_.position_.y_ = game->map_->GetTerrainHeight(transformation_.position_);
    transformation_.SetYRotation(sp.rotation.EulerAngles().y_);
    return true;
}

bool Actor::GotoHomePos()
{
    if (!homePos_.Equals(transformation_.position_, AT_POSITION_THRESHOLD * 2.0f))
    {
        return GotoPosition(homePos_);
    }
    // Already there
    return false;
}

void Actor::SetGame(ea::shared_ptr<Game> game)
{
    auto oldGame = GetGame();
    if (oldGame == game)
        return;

    effectsComp_->RemoveAll();
    // Unsubscribe from old game events first
    if (actorDiedEvent_ != 0)
    {
        if (auto g = GetGame())
            g->UnsubscribeEvent<OnGameActorDiedSignature>(OnGameActorDiedEvent, actorDiedEvent_);
    }
    if (actorRecurrectedEvent_ != 0)
    {
        if (auto g = GetGame())
            g->UnsubscribeEvent<OnGameActorResurrectedSignature>(OnGameActorResurrectedEvent, actorRecurrectedEvent_);
    }
    GameObject::SetGame(game);
    actorDiedEvent_ = 0;
    if (auto g = GetGame())
    {
        skills_->RechargeAll();
        actorDiedEvent_ = g->SubscribeEvent<OnGameActorDiedSignature>(OnGameActorDiedEvent, std::bind(&Actor::OnActorDied, this, std::placeholders::_1, std::placeholders::_2));
        actorRecurrectedEvent_ = g->SubscribeEvent<OnGameActorResurrectedSignature>(OnGameActorResurrectedEvent, std::bind(&Actor::OnActorResurrected, this, std::placeholders::_1));
        GetSubsystem<Asynch::Dispatcher>()->Add(
            Asynch::CreateTask(std::bind(&Actor::PostSpawn, GetPtr<Actor>()))
        );
    }
}

void Actor::OnActorDied(Actor* actor, Actor* killer)
{
    if (actor && actor->slaveKind_ == SlaveKind::Minion)
        minions_.erase(actor->id_);
    resourceComp_->ActorDied(actor, killer);
    effectsComp_->ActorDied(actor, killer);
}

bool Actor::SelectObject(GameObject* object)
{
    if (object)
        return selectionComp_->SelectObject(object->GetId());
    return selectionComp_->SelectObject(0);
}

bool Actor::SelectObjectById(uint32_t id)
{
    return selectionComp_->SelectObject(id);
}

bool Actor::GotoPosition(const Math::Vector3& pos)
{
    if (!IsImmobilized())
    {
        autorunComp_->Reset();
        const bool succ = autorunComp_->Goto(pos);
        if (succ)
        {
            attackComp_->Cancel();
            stateComp_.SetState(AB::GameProtocol::CreatureState::Moving);
            autorunComp_->SetAutoRun(true);
            return true;
        }
    }
    return false;
}

bool Actor::FollowObject(GameObject* object, bool ping, float maxDist /* = RANGE_TOUCH */)
{
    if (!object)
        return false;
    if (IsImmobilized())
        return false;

    CancelAll();
    const bool result = autorunComp_->Follow(object->GetPtr<GameObject>(), ping, maxDist);
    if (result)
    {
        stateComp_.SetState(AB::GameProtocol::CreatureState::Moving);
        autorunComp_->SetAutoRun(true);

    }
    return result;
}

bool Actor::FollowObjectById(uint32_t objectId, bool ping)
{
    auto* target = GetGame()->GetObject<GameObject>(objectId);
    if (!target)
        return false;
    return FollowObject(target, ping);
}

bool Actor::Attack(Actor* target, bool ping)
{
    if (!target)
        return false;
    auto game = GetGame();
    if (!game)
        return false;
    if (!AB::Entities::IsBattle(game->data_.type))
        return false;
    if (!CanAttack() || IsImmobilized())
        return false;

    if (!IsEnemy(target))
        return false;
    if (attackComp_->IsAttackingTarget(target))
        return true;

    // First select the target
    if (!selectionComp_->SelectObject(target->GetId()))
        return false;
    // Then attack
    return attackComp_->Attack(target->GetPtr<Actor>(), ping);
}

bool Actor::AttackById(uint32_t targetId, bool ping)
{
    auto* target = GetGame()->GetObject<Actor>(targetId);
    if (!target)
        return false;

    return Attack(target, ping);
}

bool Actor::IsAttackingActor(const Actor* target) const
{
    if (!target)
        return false;
    if (attackComp_->IsAttackingTarget(target))
        return true;
    return false;
}

bool Actor::IsAttacked() const
{
    if (sa::time::time_elapsed(damageComp_->lastDamage_) < 1000)
    {
        if (auto attacker = damageComp_->GetLastDamager())
        {
            if (attacker->IsDead())
                return false;
            return true;
        }
    }
    return false;
}

bool Actor::IsGroupAttacked() const
{
    if (auto* g = GetGroup())
    {
        return g->IsAttacked();
    }
    return IsAttacked();
}

bool Actor::UseSkill(int pos, bool ping)
{
    auto game = GetGame();
    if (!game)
        return false;
    if (!AB::Entities::IsBattle(game->data_.type))
        return false;

    if (!CanUseSkill() || IsDead())
        return false;
    if (pos < 0 || pos >= PLAYER_MAX_SKILLS)
        return false;

    return !IsSkillError(skillsComp_->UseSkill(pos, ping));
}

void Actor::CancelAll()
{
    CallEvent<VoidVoidSignature>(OnCancelAllEvent);
}

bool Actor::RemoveMaintainableEnch(uint32_t index, uint32_t targetId)
{
    return effectsComp_->RemoveMaintainedEffect(index, targetId);
}

void Actor::CancelAction()
{
    inputComp_->Add(InputType::Cancel);
}

bool Actor::AddToInventory(uint32_t itemId)
{
    auto* cache = GetSubsystem<ItemsCache>();
    if (Item* item = cache->Get(itemId))
    {
        // By default just delete the item
        auto* factory = GetSubsystem<ItemFactory>();
        factory->DeleteItem(item);
    }
    return true;
}

void Actor::DropRandomItem()
{
    if (!HasGame())
        return;

    auto game = GetGame();
    if (auto killer = killedBy_.lock())
    {
        // Killed by some killer, get the party of the killer and chose a random player
        // of that party in range as drop target.
        auto* partyMngr = GetSubsystem<PartyManager>();
        auto party = partyMngr->Get(killer->GetGroupId());
        Actor* target = nullptr;
        if (party)
            target = party->GetRandomPlayerInRange(this, Range::HalfCompass);
        else
            target = killer.get();
        if (!target)
            // Drop nothing when no target
            return;

        GetSubsystem<Asynch::Scheduler>()->Add(
            Asynch::CreateScheduledTask(std::bind(&Game::AddRandomItemDropFor, game, this, target))
        );
    }
    else
    {
        // Not killed by an actor, drop for any player in game
        GetSubsystem<Asynch::Scheduler>()->Add(
            Asynch::CreateScheduledTask(std::bind(&Game::AddRandomItemDrop, game, this))
        );
    }
}

ea::shared_ptr<Projectile> Actor::AddProjectile(const std::string& itemUuid, ea::shared_ptr<Actor> target)
{
    return GetGame()->AddProjectile(itemUuid, GetPtr<Actor>(), std::move(target));
}

void Actor::SetState(AB::GameProtocol::CreatureState state)
{
    Utils::VariantMap data;
    data[InputDataState] = static_cast<uint8_t>(state);
    inputComp_->Add(InputType::SetState, std::move(data));
}

bool Actor::IsHitting() const
{
    return attackComp_->IsHitting();
}

void Actor::_LuaSetHomePos(const Math::StdVector3& pos)
{
    homePos_ = pos;
    if (Math::Equals(homePos_.y_, 0.0f))
        GetGame()->map_->UpdatePointHeight(homePos_);
}

bool Actor::_LuaShadowStep(const Math::StdVector3& pos)
{
    bool success = true;
    CallEvent<OnShadowStepSignature>(OnShadowStepEvent, pos, success);
    if (!success)
        return false;
    transformation_.position_ = pos;
    moveComp_->forcePosition_ = true;
    return true;
}

void Actor::_LuaHeadTo(const Math::StdVector3& pos)
{
    HeadTo(pos);
}

Math::StdVector3 Actor::_LuaGetHomePos() const
{
    return static_cast<Math::StdVector3>(homePos_);
}

void Actor::_LuaFollowObject(GameObject* object)
{
    if (object)
        FollowObject(object, false);
}

bool Actor::_LuaAddEffect(Actor* source, uint32_t index, uint32_t time)
{
#ifdef DEBUG_GAME
    LOG_DEBUG << "Effect " << index << " added to " << GetName() << std::endl;
#endif
    return effectsComp_->AddEffect(source ? source->GetPtr<Actor>() : ea::shared_ptr<Actor>(), index, time);
}

bool Actor::_LuaHasEffect(uint32_t index) const
{
    return effectsComp_->HasEffect(index);
}

bool Actor::_LuaHasEffectOf(AB::Entities::EffectCategory category) const
{
    return effectsComp_->HasEffectOf(category);
}

std::vector<Effect*> Actor::_LuaGetEffectsOf(AB::Entities::EffectCategory category) const
{
    std::vector<Effect*> result;
    effectsComp_->VisitEffectsOf(category, [&result](Effect* effect)
    {
        result.push_back(effect);
        return Iteration::Continue;
    });
    return result;
}

std::vector<Effect*> Actor::_LuaGetEffects() const
{
    std::vector<Effect*> result;
    effectsComp_->VisitEffects([&result](Effect* effect)
    {
        result.push_back(effect);
        return Iteration::Continue;
    });
    return result;
}

bool Actor::RemoveEffect(uint32_t index)
{
    return effectsComp_->RemoveEffect(index);
}

void Actor::_LuaSetArmorAddition(int value)
{
    armorAddition_ = value;
}

int Actor::_LuaGetArmorAddition() const
{
    return armorAddition_;
}

Actor* Actor::_LuaGetMaster() const
{
    if (auto m = master_.lock())
        return m.get();
    return nullptr;
}

void Actor::_LuaSetMaster(Actor* master)
{
    if (master)
        SetMaster(master->GetPtr<Actor>());
    else
        SetMaster(ea::shared_ptr<Actor>());
}

Npc* Actor::_LuaSpawnSlave(const std::string& script, Actor* corpse, SlaveKind kind, int level, const Math::StdVector3& pos)
{
    if (auto g = GetGame())
    {
        auto m = g->AddSlave(script, GetPtr<Actor>(), (corpse ? corpse->GetPtr<Actor>() : ea::shared_ptr<Actor>()), kind, level);
        if (m)
        {
            m->transformation_.position_ = { pos[0], pos[1], pos[2] };
            Npc* result = m.get();
            CallEvent<OnSpawnedSlaveSignature>(OnSpawnedSlaveEvent, result);
            return result;
        }
    }
    return nullptr;
}

int Actor::_LuaGetMaxAttribPoints() const
{
    return static_cast<int>(GetAttribPoints(GetLevel()));
}

float Actor::_LuaGetHealthRatio() const
{
    return resourceComp_->GetHealthRatio();
}

SkillBar* Actor::_LuaGetSkillBar() const
{
    return skills_.get();
}

std::vector<Actor*> Actor::_LuaGetMinions() const
{
    auto game = GetGame();
    ASSERT(game);
    std::vector<Actor*> result;
    for (const auto& mid : minions_)
    {
        if (auto* a = game->GetObject<Actor>(mid))
            result.push_back(a);
    }
    return result;
}

std::vector<Actor*> Actor::_LuaGetSlaves() const
{
    auto game = GetGame();
    ASSERT(game);
    std::vector<Actor*> result;
    game->VisitObjects<Actor>([&result, this](Actor& current)
    {
        if (auto m = current.GetMaster())
            if (m->id_ == id_)
                result.push_back(m.get());
        return Iteration::Continue;
    });
    return result;
}

bool Actor::_LuaIsMyMinion(Actor* actor) const
{
    return (actor && minions_.find(actor->id_) != minions_.end());
}

void Actor::_LuaScaleToLevel()
{
    progressComp_->ScaleToLevel();
}

Actor* Actor::_LuaGetTarget() const
{
    return attackComp_->GetCurrentTarget();
}

bool Actor::MakeCompanion(Npc* companion)
{
    if (companion_)
    {
        companion_->SetSlaveKind(SlaveKind::NoSlave);
        companion_->SetMaster({});
        companion_->Remove();
        companion_.reset();
    }
    if (!companion)
        return false;
    companion->SetMaster(GetPtr<Actor>());
    companion->SetSlaveKind(SlaveKind::AnimalCompanion);
    companion->SetTameable(false);
    companion_ = companion->GetPtr<Npc>();

    return true;
}

void Actor::SetSlaveKind(SlaveKind kind)
{
    if (slaveKind_ == kind)
        return;

    if (slaveKind_ == SlaveKind::Minion)
        minions_.erase(id_);
    slaveKind_ = kind;
    if (slaveKind_ == SlaveKind::Minion)
        minions_.emplace(id_);
}

SlaveKind Actor::GetSlaveKind() const
{
    return slaveKind_;
}

Npc* Actor::GetCompanion() const
{
    return companion_.get();
}

void Actor::SetMaster(ea::shared_ptr<Actor> master)
{
    if (slaveKind_ == SlaveKind::Minion)
    {
        // Don't add spirits
        if (auto oldMaster = master_.lock())
            oldMaster->RemoveMinion(*this);
    }
    if (auto m = master_.lock())
    {
        if (masterGettingDamageEvent_ != 0)
            m->UnsubscribeEvent<OnGettingDamageSignature>(OnGettingDamageEvent, masterGettingDamageEvent_);
        if (masterAttackTargetEvent_ != 0)
            m->UnsubscribeEvent<OnAttackStartSignature>(OnSourceAttackStartEvent, masterAttackTargetEvent_);
    }
    masterGettingDamageEvent_ = 0;
    masterAttackTargetEvent_ = 0;
    if (master)
    {
        if (slaveKind_ == SlaveKind::Minion)
            master->AddMinion(*this);
        groupComp_->SetGroupMask(master->groupComp_->GetGroupMask());
        groupComp_->SetGroupColor(master->groupComp_->GetGroupColor());
        masterGettingDamageEvent_ = master->SubscribeEvent<OnGettingDamageSignature>(OnGettingDamageEvent, std::bind(&Actor::OnMasterGettingDamage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
        masterAttackTargetEvent_ = master->SubscribeEvent<OnAttackStartSignature>(OnSourceAttackStartEvent, std::bind(&Actor::OnMasterAttackTarget, this, std::placeholders::_1, std::placeholders::_2));
    }
    else
    {
        // When we are a mninion and we don't have a master we attack everyone
        if (slaveKind_ == SlaveKind::Minion)
        {
            RemoveFriendFoe(0xFFFFFFFF, 0);
            AddFriendFoe(0, 0xFFFFFFFF);
            groupComp_->SetGroupColor(TeamColor::Default);
        }
    }
    master_ = std::move(master);
}

ea::shared_ptr<Actor> Actor::GetMaster()
{
    return master_.lock();
}

void Actor::SetCorpse(ea::shared_ptr<Actor> value)
{
    corpse_ = std::move(value);
}

size_t Actor::GetMaxMinions() const
{
    return static_cast<size_t>((const_cast<Actor*>(this)->GetAttributeRank(Attribute::Death) / 2) + 2);
}

void Actor::AddMinion(const Actor& minion)
{
    auto g = GetGame();
    const size_t maxMinions = GetMaxMinions();
    while (minions_.size() >= maxMinions)
    {
        auto first = *minions_.begin();
        if (g)
        {
            Actor* m = g->GetObject<Actor>(first);
            if (m && !m->IsDead())
                m->Die(nullptr);
        }
        minions_.erase(minions_.begin());
    }
    minions_.emplace(minion.id_);
}

void Actor::RemoveMinion(const Actor& minion)
{
    minions_.erase(minion.id_);
}

uint32_t Actor::GetFriendMask() const
{
    return groupComp_->GetGroupMask() & 0xffffu;
}

uint32_t Actor::GetFoeMask() const
{
    return groupComp_->GetGroupMask() >> 16u;
}

void Actor::AddFriendFoe(uint32_t frnd, uint32_t foe)
{
    uint32_t mask = groupComp_->GetGroupMask();
    mask |= (frnd | (foe << 16u));
    groupComp_->SetGroupMask(mask);
}

void Actor::RemoveFriendFoe(uint32_t frnd, uint32_t foe)
{
    uint32_t mask = groupComp_->GetGroupMask();
    mask &= ~(frnd | (foe << 16u));
    groupComp_->SetGroupMask(mask);
}

void Actor::AddInput(InputType type, Utils::VariantMap&& data)
{
    inputComp_->Add(type, std::forward<Utils::VariantMap>(data));
}

void Actor::AddInput(InputType type)
{
    inputComp_->Add(type);
}

uint32_t Actor::GetGroupId() const
{
    return groupComp_->GetGroupId();
}

void Actor::SetGroupId(uint32_t value)
{
    groupComp_->SetGroupId(value);
}

TeamColor Actor::GetGroupColor() const
{
    return groupComp_->GetGroupColor();
}

void Actor::SetGroupColor(TeamColor value)
{
    groupComp_->SetGroupColor(value);
}

AB::Entities::ProfessionIndex Actor::GetProfIndex() const
{
    return static_cast<AB::Entities::ProfessionIndex>(skills_->prof1_.index);
}

AB::Entities::ProfessionIndex Actor::GetProf2Index() const
{
    return static_cast<AB::Entities::ProfessionIndex>(skills_->prof2_.index);
}

Effect* Actor::_LuaGetLastEffect(AB::Entities::EffectCategory category) const
{
    return effectsComp_->GetLast(category);
}

int Actor::_LuaRemoveAllEffectsOf(AB::Entities::EffectCategory category)
{
    return static_cast<int>(effectsComp_->RemoveAllOfCategory(category));
}

bool Actor::_LuaRemoveLastEffectOf(AB::Entities::EffectCategory category)
{
    return effectsComp_->RemoveLastEffectOf(category);
}

GameObject* Actor::_LuaGetSelectedObject() const
{
    return selectionComp_->GetSelectedObject();
}

void Actor::_LuaSetSelectedObject(GameObject* object)
{
    Utils::VariantMap data;
    data[InputDataObjectId] = GetId();    // Source
    if (object)
        data[InputDataObjectId2] = object->GetId();   // Target
    else
        data[InputDataObjectId2] = 0;   // Target
    inputComp_->Add(InputType::Select, std::move(data));
}

void Actor::_LuaSetAttackError(int error)
{
    if (attackComp_->GetAttackError() == AB::GameProtocol::AttackError::None)
        attackComp_->SetAttackError(static_cast<AB::GameProtocol::AttackError>(error));
}

int Actor::_LuaGetAttackError() const
{
    return static_cast<int>(attackComp_->GetAttackError());
}

void Actor::_LuaForcePositionUpdate()
{
    moveComp_->forcePosition_ = true;
}

bool Actor::IsGroupFighting() const
{
    Group* group = GetGroup();
    if (!group)
    {
        return (sa::time::time_elapsed(damageComp_->lastDamage_) < 1000) ||
            attackComp_->IsAttackState();
    }

    bool result = false;
    group->VisitMembers([&result](const Actor& current)
    {
        result = (sa::time::time_elapsed(current.damageComp_->lastDamage_) < 1000) ||
            current.attackComp_->IsAttackState();
        if (result)
            return Iteration::Break;
        return Iteration::Continue;
    });

    return result;
}

std::vector<Actor*> Actor::_LuaGetActorsInRange(Range range)
{
    std::vector<Actor*> result;
    VisitInRange(range, [&result](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            auto* actor = To<Actor>(&o);
            if (actor)
                result.push_back(actor);
        }
        return Iteration::Continue;
    });
    return result;
}

std::vector<Actor*> Actor::_LuaGetDeadActorsInRange(Range range)
{
    std::vector<Actor*> result;
    VisitInRange(range, [&result](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            auto* actor = To<Actor>(&o);
            if (actor && actor->IsDead())
                result.push_back(actor);
        }
        return Iteration::Continue;
    });
    return result;
}

std::vector<Actor*> Actor::_LuaGetAliveActorsInRange(Range range)
{
    std::vector<Actor*> result;
    VisitInRange(range, [&result](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            auto* actor = To<Actor>(&o);
            if (actor && !actor->IsDead())
                result.push_back(actor);
        }
        return Iteration::Continue;
    });
    return result;
}

std::vector<Actor*> Actor::_LuaGetEnemiesInRange(Range range)
{
    std::vector<Actor*> result;
    VisitInRange(range, [&result, this](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            auto* actor = To<Actor>(&o);
            if (actor->IsEnemy(this))
                result.push_back(actor);
        }
        return Iteration::Continue;
    });
    return result;
}

std::vector<Actor*> Actor::_LuaGetGroupMembersRange(Range range)
{
    std::vector<Actor*> result;
    auto* party = GetGroup();
    party->VisitMembers([&](Actor& current)
    {
        if (IsInRange(range, &current))
            result.push_back(&current);
        return Iteration::Continue;
    });
    return result;
}

size_t Actor::GetEnemyCountInRange(Range range) const
{
    size_t result = 0;
    VisitInRange(range, [&result, this](const GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            const auto& actor = To<Actor>(o);
            if (actor.IsEnemy(this))
                ++result;
        }
        return Iteration::Continue;
    });
    return result;
}

std::vector<Actor*> Actor::_LuaGetAlliesInRange(Range range)
{
    std::vector<Actor*> result;
    VisitInRange(range, [&result, this](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            auto* actor = To<Actor>(&o);
            if (actor->IsAlly(this))
                result.push_back(actor);
        }
        return Iteration::Continue;
    });
    return result;
}

size_t Actor::GetAllyCountInRange(Range range) const
{
    // At least 1 ally that's we
    size_t result = 1;
    VisitInRange(range, [&result, this](const GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
        {
            const auto& actor = To<Actor>(o);
            if (actor.IsAlly(this))
                ++result;
        }
        return Iteration::Continue;
    });
    return result;
}

Actor* Actor::GetClosestEnemy(bool undestroyable, bool unselectable)
{
    return GetClosestActor([&](const Actor& actor) -> bool
    {
        if (!actor.IsSelectable() && !unselectable)
            return false;
        if ((actor.IsUndestroyable() || actor.IsDead()) && !undestroyable)
            return false;
        if (!IsEnemy(&actor))
            return false;
        return true;
    });
}

Actor* Actor::GetClosestAlly(bool undestroyable, bool unselectable)
{
    return GetClosestActor([&](const Actor& actor) -> bool
    {
        if (!actor.IsSelectable() && !unselectable)
            return false;
        if ((actor.IsUndestroyable() || actor.IsDead()) && !undestroyable)
            return false;
        if (!IsAlly(&actor))
            return false;
        return true;
    });
}

bool Actor::Serialize(sa::PropWriteStream& stream)
{
    using namespace AB::GameProtocol;
    uint32_t validFields = ObjectSpawnDataFieldName | ObjectSpawnDataFieldLevel |
        ObjectSpawnDataFieldSex | ObjectSpawnDataFieldProf | ObjectSpawnDataFieldModelIndex |
        ObjectSpawnDataFieldSkills;
    if (Is<Player>(*this))
    {
        validFields |= ObjectSpawnDataFieldPvpCharacter;
    }

    stream.Write<uint32_t>(validFields);

    if (!GameObject::Serialize(stream))
        return false;
    stream.Write<uint32_t>(GetLevel());
    if (Is<Player>(*this))
    {
        stream.Write<uint8_t>(To<Player>(*this).data_.pvp ? 1 : 0);
    }
    stream.Write<uint8_t>(static_cast<uint8_t>(GetSex()));
    stream.Write<uint32_t>(static_cast<uint32_t>(GetProfIndex()));
    stream.Write<uint32_t>(static_cast<uint32_t>(GetProf2Index()));
    stream.Write<uint32_t>(GetItemIndex());
    const std::string skills = skills_->Encode();
    stream.WriteString(skills);
    return true;
}

void Actor::WriteSpawnData(MessageStream& msg)
{
    auto& m = msg.GetMessage(128);
//    size_t oldSize = m.GetSize();
    GameObject::WriteSpawnData(msg);

    using namespace AB::GameProtocol;
    uint32_t validFields = ObjectSpawnFieldPos | ObjectSpawnFieldRot | ObjectSpawnFieldScale |
        ObjectSpawnFieldUndestroyable | ObjectSpawnFieldSelectable | ObjectSpawnFieldState |
        ObjectSpawnFieldSpeed | ObjectSpawnFieldGroupId | ObjectSpawnFieldGroupPos;

    if (groupComp_->GetGroupMask() != 0)
        validFields |= ObjectSpawnFieldGroupMask;
    if (Is<Npc>(this))
        validFields |= ObjectSpawnFieldNpcType;
    if (GetGroupColor() != TeamColor::Default)
        validFields |= ObjectSpawnFieldGroupColor;
    if (species_ != AB::GameProtocol::Species::Unknown)
        validFields |= ObjectSpawnFieldSpecies;

    m.Add<uint32_t>(validFields);

    m.Add<float>(transformation_.position_.x_);
    m.Add<float>(transformation_.position_.y_);
    m.Add<float>(transformation_.position_.z_);
    m.Add<float>(transformation_.GetYRotation());
    m.Add<float>(transformation_.scale_.x_);
    m.Add<float>(transformation_.scale_.y_);
    m.Add<float>(transformation_.scale_.z_);
    m.Add<bool>(undestroyable_);
    m.Add<bool>(selectable_);
    m.Add<uint8_t>(static_cast<uint8_t>(stateComp_.GetState()));
    m.Add<float>(GetSpeed());
    m.Add<uint32_t>(GetGroupId());
    m.Add<uint8_t>(static_cast<uint8_t>(GetGroupPos()));
    if (validFields & ObjectSpawnFieldGroupMask)
        m.Add<uint32_t>(groupComp_->GetGroupMask());
    if (validFields & ObjectSpawnFieldGroupColor)
        m.Add<uint8_t>(static_cast<uint8_t>(GetGroupColor()));
    if (validFields & ObjectSpawnFieldNpcType)
        m.Add<uint8_t>(static_cast<uint8_t>(npcType_));
    if (validFields & ObjectSpawnFieldSpecies)
        m.Add<uint8_t>(static_cast<uint8_t>(species_));
    sa::PropWriteStream data;
    size_t dataSize = 0;
    Serialize(data);
    const char* cData = data.GetStream(dataSize);
    m.Add(std::string(cData, dataSize));

    // Approx. 160 byte are needed to spawn an actor
//    LOG_INFO << "Message Size" << m.GetSize() - oldSize << std::endl;
    resourceComp_->Write(msg, true);
    effectsComp_->Write(msg);
}

Item* Actor::GetWeapon() const
{
    return inventoryComp_->GetWeapon();
}

void Actor::OnEndUseSkill(Actor*, Skill* skill)
{
    // These change the state
    if (skill->IsChangingState())
    {
        attackComp_->Pause(false);
        if (!stateComp_.IsDead())
            stateComp_.SetState(AB::GameProtocol::CreatureState::Idle);
    }
}

void Actor::OnStartUseSkill(Actor*, Skill* skill)
{
    // These change the state
    if (skill->IsChangingState())
    {
        attackComp_->Pause();
        autorunComp_->Reset();
        moveComp_->Cancel();
        stateComp_.SetState(AB::GameProtocol::CreatureState::UsingSkill);
    }
}

void Actor::HeadTo(const Math::Vector3& pos)
{
    if (!IsImmobilized())
        moveComp_->HeadTo(pos);
}

void Actor::FaceObject(const GameObject* object)
{
    if (object && !IsImmobilized() && object != this)
        HeadTo(object->transformation_.position_);
}

float Actor::GetSpeed() const
{
    return moveComp_->GetSpeedFactor();
}

void Actor::SetSpeed(float value)
{
    moveComp_->SetSpeedFactor(value);
}

void Actor::AddSpeed(float value)
{
    moveComp_->AddSpeed(value);
}

bool Actor::IsMoving() const
{
    return moveComp_->IsMoving();
}

bool Actor::IsInWeaponRange(Actor* actor) const
{
    Item* weapon = GetWeapon();
    if (!weapon)
        // If there is no weapon, use feasts!
        return (GetDistance(actor) - AVERAGE_BB_EXTENDS) <= RANGE_TOUCH;
    const float range = weapon->GetWeaponRange();
    if (range == 0.0f)
        return false;
    return (GetDistance(actor) - AVERAGE_BB_EXTENDS) <= range;
}

int Actor::GetClassArmorAddition(DamageType damageType) const
{
    int result = 0;
    switch (skills_->prof1_.index)
    {
    case AB::Entities::ProfessionIndexWarrior:
        result += 20;
        switch (damageType)
        {
        case DamageType::Blunt:
        case DamageType::Piercing:
        case DamageType::Slashing:
            result += 20;
            break;
        default:
            break;
        }
        break;
    case AB::Entities::ProfessionIndexRanger:
        result += 10;
        switch (damageType)
        {
        case DamageType::Fire:
        case DamageType::Cold:
        case DamageType::Lightning:
        case DamageType::Earth:
            result += 30;
            break;
        default:
            break;
        }
        break;
    }
    result += armorAddition_;
    return result;
}

void Actor::ExploitedCorpse(Actor* corpse)
{
    CallEvent<OnExploitedCorpseSignature>(OnExploitedCorpseEvent, this, corpse);
}

void Actor::UnsubscribeEvents()
{
    if (masterGettingDamageEvent_ != 0)
    {
        if (auto m = master_.lock())
            m->UnsubscribeEvent<OnGettingDamageSignature>(OnGettingDamageEvent, masterGettingDamageEvent_);
        masterGettingDamageEvent_ = 0;
    }
    if (masterAttackTargetEvent_ != 0)
    {
        if (auto m = master_.lock())
            m->UnsubscribeEvent<OnAttackStartSignature>(OnSourceAttackStartEvent, masterAttackTargetEvent_);
        masterAttackTargetEvent_ = 0;
    }
    if (actorDiedEvent_ != 0)
    {
        if (auto g = GetGame())
            g->UnsubscribeEvent<OnGameActorDiedSignature>(OnGameActorDiedEvent, actorDiedEvent_);
        actorDiedEvent_ = 0;
    }
    if (actorRecurrectedEvent_ != 0)
    {
        if (auto g = GetGame())
            g->UnsubscribeEvent<OnGameActorResurrectedSignature>(OnGameActorResurrectedEvent, actorRecurrectedEvent_);
        actorRecurrectedEvent_ = 0;
    }
}

bool Actor::LoadCompainion(const CompanioInfo& info)
{
    if (auto g = GetGame())
    {
        auto m = g->AddSlave(info.script, GetPtr<Actor>(), {}, SlaveKind::AnimalCompanion, info.level);
        if (!m)
            return false;
        Npc& result = *m;
        result.transformation_.position_ = transformation_.position_;
        result.SetXp(info.xp);
        result.SetSkillPoints(info.sp);
        result.SetName(info.name);
        result.SetLevel((uint32_t)info.level);
        return MakeCompanion(&result);
    }
    return false;
}

void Actor::PostSpawn()
{
    auto game = GetGame();
    if (!game)
    {
        // FIXME: This shouldn't happen, silly thing (or silly me)!
        LOG_WARNING << "game == null" << std::endl;
        return;
    }
    ASSERT(game);
    if (AB::Entities::IsBattle(game->data_.type))
    {
        // We spawned in a game. Now we can do stuff to setup us, e.g. spawning companions etc.
        if (skills_->ActivatesCompanion())
        {
            CompanioInfo info{};
            if (LoadCompationInfo(info))
            {
                if (!LoadCompainion(info))
                    LOG_ERROR << "Error loading companion " << info.script << std::endl;
            }
        }
    }
    CallEvent<OnPostSpawnSignature>(OnPostSpawnEvent, game.get());
}

int Actor::GetBaseArmor(DamageType damageType, DamagePos pos)
{
    // Armor base comes from the armor the actor wears, but NPCs do not wear armors.
    return inventoryComp_->GetArmor(damageType, pos);
}

float Actor::GetArmorEffect(DamageType damageType, DamagePos pos, float penetration)
{
    // To calculate the effect of the armor to the damage. The damage is multiplied by value
    switch (damageType)
    {
    case DamageType::Holy:
    case DamageType::Shadow:
    case DamageType::Chaos:
    case DamageType::LifeDrain:
    case DamageType::Typeless:
    case DamageType::Dark:
        // Ignoring armor -> full damage, armor no effect
        return 1.0f;
    default:
        break;
    }
    // Armor base comes from the armor the actor wears, but NPCs do not wear armors.
    int baseArmor = GetBaseArmor(damageType, pos);
    // Then there may be some class specific armor bonus
    baseArmor += GetClassArmorAddition(damageType);
    // Effective armor can not be negative
    if (baseArmor < 0)
        baseArmor = 0;
    int armorMod = 0;
    effectsComp_->GetArmor(damageType, armorMod);
    const float totalArmor = static_cast<float>(baseArmor) * (1.0f - penetration) + static_cast<float>(armorMod);
    return 0.5f + ((totalArmor - 60.0f) / 40.0f);
}

uint32_t Actor::GetAttackSpeed()
{
    Item* weapon = GetWeapon();
    if (!weapon)
        return ATTACK_SPEED_STAFF;
    const uint32_t speed = weapon->GetWeaponAttackSpeed();
    uint32_t modSpeed = speed;
    effectsComp_->GetAttackSpeed(weapon, modSpeed);

    // https://wiki.guildwars.com/wiki/Attack_speed
    // Max IAS 133%, max DAS 50%
    modSpeed = Math::Clamp(modSpeed,
        static_cast<uint32_t>(static_cast<float>(speed) * MAX_DAS),
        static_cast<uint32_t>(static_cast<float>(speed) * MAX_IAS));
    return modSpeed;
}

float Actor::GetAttackSpeedIncrease(uint32_t speed) const
{
    Item* weapon = GetWeapon();
    const uint32_t normalSpeed = weapon ? weapon->GetWeaponAttackSpeed() : ATTACK_SPEED_STAFF;
    return static_cast<float>(normalSpeed) / static_cast<float>(speed);
}

DamageType Actor::GetAttackDamageType()
{
    Item* weapon = GetWeapon();
    if (!weapon)
        // No weapon makes Slashing damage
        return DamageType::Slashing;
    DamageType type = DamageType::Unknown;
    weapon->GetWeaponDamageType(type);
    effectsComp_->GetAttackDamageType(type);
    return type;
}

int32_t Actor::GetAttackDamage(bool critical)
{
    Item* weapon = GetWeapon();
    int32_t damage = 0;
    if (weapon)
    {
        // Get weapon damage with mods
        weapon->GetWeaponDamage(damage, critical);
        const Attribute attrib = weapon->GetWeaponAttribute();
        const uint32_t req = weapon->GetWeaponRequirement();
        if (GetAttributeRank(attrib) < req)
            // If requirements are not met, damage is the half
            damage /= 2;
    }
    else
    {
        // make small damage without weapon, maybe with feasts :D
        const int32_t level = static_cast<int32_t>(GetLevel());
        // Level 20 actors make 5 damage. Lower actors make least 1 damage
        damage = Math::Clamp(level / 4, 1, level);
    }

    // Effects may modify the damage
    effectsComp_->GetAttackDamage(damage);
    return damage;
}

float Actor::GetArmorPenetration(DamageType damageType) const
{
    // 1. Attribute strength
    const float strength = static_cast<float>(GetAttributeRank(Attribute::Strength));
    float value = (strength * 0.01f);
    // 2. Weapons
    value += inventoryComp_->GetArmorPenetration();
    // 3. Effects
    float ea = 0.0f;
    effectsComp_->GetArmorPenetration(damageType, ea);
    value += ea;
    return value;
}

float Actor::GetCriticalChance(const Actor* other)
{
    if (!other)
        return 0.0f;
    // https://www.guildwiki.de/wiki/Kritische_Treffer

    // From behind is always critical
    const float diff = transformation_.GetYRotation() - other->transformation_.GetYRotation();
    if (fabs(diff) < BEHIND_ANGLE)
        // If rotation is < 30 deg it's from behind -> always critical even without weapon
        return 1.0f;

    Item* weapon = GetWeapon();
    if (!weapon)
        return 0.0f;

    const Attribute attrib = weapon->GetWeaponAttribute();
    const float attribVal = static_cast<float>(GetAttributeRank(attrib));
    const float myLevel = static_cast<float>(GetLevel());
    const float otherLevel = static_cast<float>(other->GetLevel());

    const float val1 = ((8.0f * myLevel) - (15.0f * otherLevel) + (4.0f * attribVal) + (6 * std::min(attribVal, ((myLevel + 4.0f) / 2.0f))) - 100.0f) / 40.0f;
    const float val2 = (1.0f - (attribVal / 100.0f));
    const float val3 = (attribVal / 100.0f);
    return 0.5f * (2.0f * val1) * val2 + val3;
}

void Actor::SetResource(Components::ResourceType type, Components::SetValueType t, int value)
{
    resourceComp_->SetValue(type, t, value);
}

int Actor::DrainLife(Actor* source, uint32_t index, int value)
{
    if (source)
    {
        bool success = true;
        source->CallEvent<OnDealingDamageSignature>(OnDealingDamageEvent, this, DamageType::LifeDrain, value, success);
        if (!success)
            return 0;
    }

    return damageComp_->DrainLife(source, index, value);
}

int Actor::AddEnergy(int value)
{
    return resourceComp_->AddEnergy(value);
}

int Actor::AddLife(int value)
{
    return resourceComp_->AddLife(value);
}

int Actor::DrainEnergy(int value)
{
    return resourceComp_->DrainEnergy(value);
}

void Actor::SetHealthRegen(int value)
{
    const Components::SetValueType vt = value < 0 ?
        Components::SetValueType::Decrease :
        Components::SetValueType::Increase;
    resourceComp_->SetHealthRegen(vt, abs(value));
}

void Actor::SetEnergyRegen(int value)
{
    const Components::SetValueType vt = value < 0 ?
        Components::SetValueType::Decrease :
        Components::SetValueType::Increase;
    resourceComp_->SetEnergyRegen(vt, abs(value));
}

bool Actor::InterruptAttack(Actor* source)
{
    bool success = true;
    CallEvent<OnInterruptingAttackSignature>(OnInterruptingAttackEvent, success);
    if (success && source)
        // We are getting interrupted by source
        source->CallEvent<OnInterruptingTargetSignature>(OnInterruptingTargetEvent, this, success);
    if (!success)
        return false;
    return attackComp_->Interrupt();
}

bool Actor::InterruptSkill(Actor* source, AB::Entities::SkillType type)
{
    Skill* skill = skillsComp_->GetCurrentSkill();
    if (!skill)
        return false;
    bool success = true;
    CallEvent<OnInterruptingSkillSignature>(OnInterruptingSkillEvent, source, type, skill, success);
    if (success && source)
        // We (source) are getting interrupted by source
        source->CallEvent<OnInterruptingTargetSignature>(OnInterruptingTargetEvent, this, success);
    if (!success)
    {
        if (source)
            source->CallEvent<OnInterruptedSignature>(OnInterruptedEvent, this, skill, false);
        return false;
    }
    bool result = skillsComp_->Interrupt(type);
    if (result)
    {
        CallEvent<OnWasInterruptedSignature>(OnWasInterruptedEvent, source, skill);
    }
    if (source)
        // The source successfully interrupted us
        source->CallEvent<OnInterruptedSignature>(OnInterruptedEvent, this, skill, result);
    return result;
}

bool Actor::Interrupt(Actor* source)
{
    bool ret = InterruptAttack(source);
    if (ret)
        return true;
    return InterruptSkill(source, AB::Entities::SkillTypeSkill);
}

std::string Actor::GetClassLevel() const
{
    if (!skills_)
        return "Lvl" + std::to_string(GetLevel());
    std::string classes = skills_->GetClasses();
    if (classes.empty())
        return "Lvl" + std::to_string(GetLevel());

    classes += std::to_string(GetLevel());
    return classes;
}

uint32_t Actor::GetAttributePoints() const
{
    return GetAttribPoints(GetLevel());
}

void Actor::AdvanceLevel()
{
    resourceComp_->UpdateResources();
}

bool Actor::IsUsingSkillOfType(AB::Entities::SkillType type, int32_t minActivationTime) const
{
    const auto* skill = skills_->GetCurrentSkill();
    if (skill && skill->IsUsing() && skill->IsType(type) &&
        (skill->activation_ > minActivationTime))
        return true;
    return false;
}

Skill* Actor::GetSkill(uint32_t index) const
{
    if (index < PLAYER_MAX_SKILLS)
        return (*skills_)[index];
    return nullptr;
}

SkillBar& Actor::GetSkillBar() const
{
    return *skills_;
}

Skill* Actor::GetCurrentSkill() const
{
    return skills_->GetCurrentSkill();
}

bool Actor::SetEquipment(const std::string& ciUuid)
{
    auto* factory = GetSubsystem<ItemFactory>();
    uint32_t itemId = factory->GetConcreteId(ciUuid);
    if (itemId == 0)
        return false;
    inventoryComp_->SetEquipment(itemId);
    return true;
}

bool Actor::SetInventory(const std::string& ciUuid)
{
    auto* factory = GetSubsystem<ItemFactory>();
    uint32_t itemId = factory->GetConcreteId(ciUuid);
    if (itemId == 0)
        return false;
    return inventoryComp_->SetInventoryItem(itemId, nullptr);
}

bool Actor::SetChest(const std::string& ciUuid)
{
    auto* factory = GetSubsystem<ItemFactory>();
    uint32_t itemId = factory->GetConcreteId(ciUuid);
    if (itemId == 0)
        return false;
    return inventoryComp_->SetChestItem(itemId, nullptr);
}

const std::string& Actor::GetPlayerUuid() const
{
    static const std::string empty(Utils::Uuid::EMPTY_UUID);
    return empty;
}

const std::string& Actor::GetAccountUuid() const
{
    static const std::string empty(Utils::Uuid::EMPTY_UUID);
    return empty;
}

bool Actor::_LuaGotoPosition(const Math::StdVector3& pos)
{
    if (IsImmobilized())
        return false;

    Math::Vector3 _pos(pos);
    if (Math::Equals(_pos.y_, 0.0f))
        GetGame()->map_->UpdatePointHeight(_pos);
    return GotoPosition(_pos);
}

void Actor::Update(uint32_t timeElapsed, MessageStream& message)
{
    GameObject::Update(timeElapsed, message);

    // Update all
    stateComp_.Update(timeElapsed);
    resourceComp_->Update(timeElapsed);
    inputComp_->Update(timeElapsed, message);
    selectionComp_->Update(timeElapsed);

    attackComp_->Update(timeElapsed);
    skillsComp_->Update(timeElapsed);
    effectsComp_->Update(timeElapsed);
    damageComp_->Update(timeElapsed);
    healComp_->Update(timeElapsed);
    uint32_t flags = Components::MoveComp::UpdateFlagTurn;
    if (!autorunComp_->IsAutoRun())
        flags |= Components::MoveComp::UpdateFlagMove;
    moveComp_->Update(timeElapsed, flags);
    autorunComp_->Update(timeElapsed);
    // After move/autorun resolve collisions
    collisionComp_->Update(timeElapsed);
    progressComp_->Update(timeElapsed);
    groupComp_->Update(timeElapsed);

    if (moveComp_->moved_ && GetOctant())
    {
        Math::Octree* octree = GetOctant()->GetRoot();
        octree->AddObjectUpdate(this);
    }

    // Write all
    stateComp_.Write(message);
    moveComp_->Write(message);
    selectionComp_->Write(message);

    skillsComp_->Write(message);
    attackComp_->Write(message);
    effectsComp_->Write(message);
    resourceComp_->Write(message);
    damageComp_->Write(message);
    healComp_->Write(message);
    progressComp_->Write(message);
    groupComp_->Write(message);
}

bool Actor::Die(Actor* killer)
{
    if (IsDead())
        return false;

    attackComp_->Cancel();
    skillsComp_->Cancel();
    // Our minions become master-less
    if (auto g = GetGame())
    {
        for (auto mid : minions_)
        {
            Actor* minion = g->GetObject<Actor>(mid);
            if (minion)
                minion->SetMaster({});
        }
        minions_.clear();
    }
    stateComp_.SetState(AB::GameProtocol::CreatureState::Dead);
    resourceComp_->SetHealth(Components::SetValueType::Absolute, 0);
    resourceComp_->SetEnergy(Components::SetValueType::Absolute, 0);
    resourceComp_->SetAdrenaline(Components::SetValueType::Absolute, 0);
    damageComp_->Touch();
    autorunComp_->SetAutoRun(false);
    DecreaseMorale();
    if (killer)
        killedBy_ = killer->GetPtr<Actor>();
    else
        killedBy_ = damageComp_->GetLastDamager();
    CallEvent<OnActorDiedSignature>(OnDiedEvent, this, killer ? killer : damageComp_->GetLastDamager().get());
    // If we are a minion remove it from the master
    if (auto master = master_.lock())
        master->RemoveMinion(*this);
    return true;
}

bool Actor::Resurrect(int percentHealth, int percentEnergy)
{
    if (!IsDead() || !CanResurrect())
        return false;

    const int health = GetPercent(resourceComp_->GetMaxHealth(), percentHealth);
    resourceComp_->SetHealth(Components::SetValueType::Absolute, health);
    const int energy = GetPercent(resourceComp_->GetMaxEnergy(), percentEnergy);
    resourceComp_->SetEnergy(Components::SetValueType::Absolute, energy);
    damageComp_->Touch();
    stateComp_.SetState(AB::GameProtocol::CreatureState::Idle);
    recycleable_ = true;
    CallEvent<OnResurrectedSignature>(OnResurrectedEvent, health, energy);
    return true;
}

bool Actor::CanResurrect() const
{
    // Game over when resigned or defeated
    if (auto* g = GetGroup())
    {
        if (g->IsResigned())
            return false;
        if (g->IsDefeated())
            return false;
    }
    return resurrectable_;
}

bool Actor::KnockDown(Actor* source, uint32_t time)
{
    if (IsDead())
        return false;
    if (IsKnockedDown())
        return false;

    if (time == 0)
        time = DEFAULT_KNOCKDOWN_TIME;
    bool ret = true;
    CallEvent<OnKnockingDownSignature>(OnKnockingDownEvent, source, time, ret);
    if (!ret)
        return false;
    if (source)
    {
        // We are getting KD'd by source
        source->CallEvent<OnKnockingDownTargetSignature>(OnKnockingDownTargetEvent, this, time, ret);
        if (!ret)
            return false;
    }

    ret = stateComp_.KnockDown(time);
    if (ret)
    {
        // KD interrupts all regardless of effects that may prevent interrupting.
        // The only way to prevent this is an effect that prevents KDs.
        attackComp_->Interrupt();
        skillsComp_->Interrupt(AB::Entities::SkillTypeSkill);
        autorunComp_->Reset();
        CallEvent<OnKnockedDownSignature>(OnKnockedDownEvent, time);
    }
    return ret;
}

int Actor::Healing(Actor* source, uint32_t index, int value)
{
    if (IsDead())
        return 0;
    int val = value;
    CallEvent<OnHealingSignature>(OnHealingEvent, source, val);
    if (source)
        source->CallEvent<OnHealingTargetSignature>(OnHealedTargetEvent, this, val);
    healComp_->Healing(source, index, val);
    CallEvent<VoidIntSignature>(OnHealedEvent, val);
    if (source)
    {
        auto skill = source->skills_->GetSkillBySkillIndex(index);
        source->CallEvent<OnHealedTargetSignature>(OnHealedTargetEvent, this, skill.get(), val);
    }
    return val;
}

int Actor::Damage(Actor* source, uint32_t index, DamageType type, int value, float penetration)
{
    if (IsDead())
        return 0;
    int val = value;
    bool crit = false;
    effectsComp_->GetDamage(type, val, crit);
    if (source)
    {
        bool success = true;
        source->CallEvent<OnDealingDamageSignature>(OnDealingDamageEvent, this, type, value, success);
        if (!success)
            return 0;
        source->effectsComp_->GetSourceArmorPenetration(this, type, penetration);
    }
    damageComp_->ApplyDamage(source, index, type, val, penetration, false);
    return val;
}

bool Actor::IsEnemy(const Actor* other) const
{
    if (!other)
        return false;

    if ((GetGroupId() != 0) && GetGroupId() == other->GetGroupId())
        // Same group members are always friends
        return false;
    // Return true if we have a matching bit of our foe mask in their friend mask
    return sa::bits::is_any_set(GetFoeMask(), other->GetFriendMask());
}

bool Actor::IsAlly(const Actor* other) const
{
    if (!other)
        return false;

    if ((GetGroupId() != 0) && GetGroupId() == other->GetGroupId())
        // Same group members are always friends
        return true;
    // Return true if they have matching bits in the friend mask
    return sa::bits::is_any_set(GetFriendMask(), other->GetFriendMask());
}

uint32_t Actor::GetAttributeRank(Attribute index) const
{
    uint32_t result = 0;
    // Skilled points
    const AttributeValue* val = skills_->GetAttribute(index);
    if (val != nullptr)
        result = val->value;
    // Increase by equipment
    result += inventoryComp_->GetAttributeRank(index);

    // Effects can in- or decrease the attribute rank
    int32_t value = 0;
    effectsComp_->GetAttributeRank(index, value);
    if (value == 0)
        return result;

    if (value < 0)
    {
        // It's not possible to have a negative attribute rank
        if (static_cast<uint32_t>(abs(value)) < result)
            result -= static_cast<uint32_t>(abs(value));
        else
            result = 0;
    }
    else
        result += static_cast<uint32_t>(value);
    return result;
}

AreaOfEffect* Actor::_LuaAddAOE(const std::string& script, uint32_t index,
    const Math::StdVector3& pos)
{
    auto result = GetGame()->AddAreaOfEffect(script, GetPtr<Actor>(), index, pos);
    if (result)
        return result.get();
    return nullptr;
}

Group* Actor::GetGroup() const
{
    if (auto g = GetGame())
        return g->GetGroup(GetGroupId());
    return nullptr;
}

int Actor::GetMorale() const
{
    return resourceComp_->GetMorale();
}

bool Actor::IncreaseMorale()
{
    if (!IsUndestroyable())
        return resourceComp_->IncreaseMorale();
    return false;
}

bool Actor::DecreaseMorale()
{
    if (!IsUndestroyable())
        return resourceComp_->DecreaseMorale();
    return false;
}

unsigned Actor::GetDeaths() const
{
    return progressComp_->GetDeaths();
}

DamagePos Actor::GetDamagePos() const
{
    return damageComp_->GetDamagePos();
}

GameObject* Actor::GetSelectedObject() const
{
    return selectionComp_->GetSelectedObject();
}

uint32_t Actor::GetSelectedObjectId() const
{
    return selectionComp_->GetSelectedObjectId();
}

bool Actor::CollisionNeedsAdjustment(const GameObject& other) const
{
    // If it's a triangle mesh then it's most likely a building and we may be able to step on it
    // FIXME: I think this isn't a great way doing this
    if (other.GetCollisionShape()->shapeType_ != Math::ShapeType::TriangleMesh)
        return true;
    return !moveComp_->CanStepOn(nullptr);
}

void GetSkillRecharge(const Actor& actor, Skill* skill, uint32_t& recharge)
{
    actor.inventoryComp_->GetSkillRecharge(skill, recharge);
    actor.effectsComp_->GetSkillRecharge(skill, recharge);
    actor.skillsComp_->GetSkillRecharge(skill, recharge);
}

void GetSkillActivation(const Actor& actor, Skill* skill, uint32_t& activation)
{
    //    actor.inventoryComp_->GetSkillRecharge(skill, recharge);
    actor.effectsComp_->GetSkillActivation(skill, activation);
    //    actor.skillsComp_->GetSkillRecharge(skill, recharge);
}

void GetSkillHpSacrifies(const Actor& actor, Skill* skill, uint32_t& hp)
{
    actor.effectsComp_->GetSkillHpSacrifies(skill, hp);
}

void GetSkillCost(const Actor& actor, Skill* skill,
    int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp)
{
    actor.skillsComp_->GetSkillCost(skill, activation, energy, adrenaline, overcast, hp);

    actor.inventoryComp_->GetSkillCost(skill, activation, energy, adrenaline, overcast, hp);
    actor.effectsComp_->GetSkillCost(skill, activation, energy, adrenaline, overcast, hp);
}

void GetResources(const Actor& actor, int& maxHealth, int& maxEnergy)
{
    // Attributtes first
    actor.skillsComp_->GetResources(maxHealth, maxEnergy);
    // Runes etc.
    actor.inventoryComp_->GetResources(maxHealth, maxEnergy);
    // Effects influencing
    actor.effectsComp_->GetResources(maxHealth, maxEnergy);
}

void GetRegeneration(const Actor& actor, int& healthRegen, int& energyRegen)
{
    // Runes etc.
    actor.inventoryComp_->GetRegeneration(healthRegen, energyRegen);
    // Effects influencing
    actor.effectsComp_->GetRegeneration(healthRegen, energyRegen);
}

}
