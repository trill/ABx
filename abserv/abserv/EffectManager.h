/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Effect.h>
#include <sa/StringHash.h>
#include <eastl.hpp>

namespace Game {

class Effect;

inline constexpr size_t EFFECTTCAT_CONDITION = sa::StringHash("Condition");
inline constexpr size_t EFFECTTCAT_ENCHANTMENT = sa::StringHash("Enchantment");
inline constexpr size_t EFFECTTCAT_HEX = sa::StringHash("Hex");
inline constexpr size_t EFFECTTCAT_SHOUT = sa::StringHash("Shout");
inline constexpr size_t EFFECTTCAT_SPIRIT = sa::StringHash("Spirit");
inline constexpr size_t EFFECTTCAT_WARD = sa::StringHash("Ward");
inline constexpr size_t EFFECTTCAT_WELL = sa::StringHash("Well");
inline constexpr size_t EFFECTTCAT_PREPARATION = sa::StringHash("Preparation");
inline constexpr size_t EFFECTTCAT_STANCE = sa::StringHash("Stance");
inline constexpr size_t EFFECTTCAT_FORM = sa::StringHash("Form");
inline constexpr size_t EFFECTTCAT_GLYPHE = sa::StringHash("Glyphe");
inline constexpr size_t EFFECTTCAT_PETATTTACK = sa::StringHash("PetAttack");
inline constexpr size_t EFFECTTCAT_WEAPONSPELL = sa::StringHash("WeaponSpell");

AB::Entities::EffectCategory EffectCatNameToEffectCat(const std::string& name);

class EffectManager
{
private:
    ea::map<uint32_t, AB::Entities::Effect> effects_;
public:
    EffectManager() = default;
    ~EffectManager() = default;

    ea::shared_ptr<Effect> Get(uint32_t index);
};

}
