/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MailBox.h"
#include "IOMail.h"
#include <libcommon/Subsystems.h>
#include "ItemFactory.h"
#include <libcommon/DataClient.h>

namespace Game {

bool MailBox::ReadMail(const std::string& uuid, AB::Entities::Mail& mail)
{
    mail.uuid = uuid;
    return IO::ReadMail(mail);
}

bool MailBox::DeleteMail(const std::string& uuid, AB::Entities::Mail& mail)
{
    mail.uuid = uuid;
    return IO::DeleteMail(mail);
}

uint32_t MailBox::TakeMailAttachment(const std::string& uuid, AB::Entities::ItemPos pos)
{
    AB::Entities::Mail mail;
    if (!ReadMail(uuid, mail))
        return 0;
    if (mail.attachments.size() <= pos)
        return 0;

    auto* factory = GetSubsystem<ItemFactory>();
    auto* item = factory->GetConcrete(mail.attachments[pos]);
    if (!item)
        return 0;
    mail.attachments.erase(mail.attachments.begin() + pos);
    auto* client = GetSubsystem<IO::DataClient>();
    client->Update(mail);
    return item->id_;
}

MailBox::MailBox(std::string accountUuid) :
    accountUuid_(std::move(accountUuid))
{}

MailBox::~MailBox() = default;

void MailBox::Update()
{
    IO::LoadMailList(mailList_, accountUuid_);
}

void MailBox::DeleteAll()
{
    for (const AB::Entities::MailHeader& mh : mailList_.mails)
    {
        AB::Entities::Mail m;
        m.uuid = mh.uuid;
        IO::DeleteMail(m);
    }
}

}
