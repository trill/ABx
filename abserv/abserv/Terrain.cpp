/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Terrain.h"
#include "TerrainPatch.h"

namespace Game {

Terrain::Terrain() = default;

Terrain::~Terrain() = default;

void HeightMap::OnChanged()
{
    ProcessData();
    ResetMesh();
}

void Terrain::SetSpacing(const Math::Vector3& value)
{
    if (heightMap_)
        heightMap_->spacing_ = value;
    if (heightMap2_)
        heightMap2_->spacing_ = value;
}

void Terrain::Initialize()
{
    if (heightMap_)
        heightMap_->ProcessData();
    if (heightMap2_)
        heightMap2_->ProcessData();
}

float Terrain::GetHeight1(const Math::Vector3& world) const
{
    if (!heightMap_)
        return 0.0f;
    if (matrixDirty_)
    {
        const Math::Matrix4 matrix = transformation_.GetMatrix();
        heightMap_->SetMatrix(matrix);
        if (heightMap2_)
            heightMap2_->SetMatrix(matrix);
        matrixDirty_ = false;
    }
    return heightMap_->GetHeight(world);
}

float Terrain::GetHeight2(const Math::Vector3& world) const
{
    if (!heightMap2_)
        return -std::numeric_limits<float>::infinity();
    if (matrixDirty_)
    {
        const Math::Matrix4 matrix = transformation_.GetMatrix();
        heightMap_->SetMatrix(matrix);
        if (heightMap2_)
            heightMap2_->SetMatrix(matrix);
        matrixDirty_ = false;
    }
    return heightMap2_->GetHeight(world);
}

float Terrain::GetHeight(const Math::Vector3& world) const
{
    if (!heightMap_)
        return 0.0f;
    if (matrixDirty_)
    {
        const Math::Matrix4 matrix = transformation_.GetMatrix();
        heightMap_->SetMatrix(matrix);
        if (heightMap2_)
            heightMap2_->SetMatrix(matrix);
        matrixDirty_ = false;
    }

    float result = heightMap_->GetHeight(world);
    if (!heightMap2_)
        return result;

    float result2 = heightMap2_->GetHeight(world);
    // Layer2 must be above layer1
    if (Math::IsNegInfinite(result2) || result2 < result)
        return result;

    // If the difference is smaller than the height of the character it can't be the lower height
    float diff12 = result2 - result;
    if (diff12 < 1.7f)
        return result2;

    // Otherwise use the closer value to the current height
    if (fabs(world.y_ - result) < fabs(world.y_ - result2))
        return result;
    return result2;
}

}
