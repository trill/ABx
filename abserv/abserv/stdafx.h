/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#include "targetver.h"
#include <sa/Compiler.h>

PRAGMA_WARNING_DISABLE_MSVC(4307)

#include <stdio.h>

#if !defined(ASIO_STANDALONE)
#define ASIO_STANDALONE
#endif

#include <sa/Assert.h>
#include <AB/CommonConfig.h>
#include <libcommon/ServiceConfig.h>
#include "Config.h"
#include <libcommon/DebugConfig.h>

#include <cmath>

// STL
#include <algorithm>
#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <ostream>
#include <sstream>
#include <string>
#include <thread>

#include <eastl.hpp>

#include <limits>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>

#define BRIGAND_NO_BOOST_SUPPORT
#include <libmath/MathConfig.h>
#include <pugixml.hpp>

PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4592)
    PRAGMA_WARNING_DISABLE_CLANG("-Wpadded")
    PRAGMA_WARNING_DISABLE_GCC("-Wpadded")
#   include <asio.hpp>
PRAGMA_WARNING_POP

PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4702 4127)
#   include <lua.hpp>
#   include <kaguya/kaguya.hpp>
PRAGMA_WARNING_POP

#include <libcommon/Logger.h>
#include <libcommon/Profiler.h>

#include <base64.h>
#include <sa/CallableTable.h>
#include <sa/CircularQueue.h>
#include <sa/Events.h>
#include <sa/IdGenerator.h>
#include <sa/Iteration.h>
#include <sa/PropStream.h>
#include <sa/StringHash.h>
#include <sa/StrongType.h>
#include <sa/TypeName.h>
#include <sa/WeightedSelector.h>
#include <uuid.h>
