/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "IOAsset.h"
#include "Script.h"

namespace IO {

/// Loads a Lua script and stores the compiled byte code.
class IOScript final : public IOAssetImpl<Game::Script>
{
public:
    bool Import(Game::Script& asset, const std::string& name) override;
};

}
