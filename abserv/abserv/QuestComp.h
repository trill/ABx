/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Quest.h"
#include <sa/Assert.h>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Player;
class MessageStream;

namespace Components {

class QuestComp
{
    NON_COPYABLE(QuestComp)
    NON_MOVEABLE(QuestComp)
private:
    Player& owner_;
    ea::map<uint32_t, ea::unique_ptr<Quest>> activeQuests_;
    // This can be a std::map because we keep this only to check if the player
    // has the requirements for another quest.
    ea::map<uint32_t, ea::unique_ptr<Quest>> doneQuests_;
    Quest* GetCompletedQuest(uint32_t index) const;
public:
    explicit QuestComp(Player& owner);
    ~QuestComp();

    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);

    // Player adds a quest
    bool PickupQuest(uint32_t index);
    bool DeleteQuest(uint32_t index);
    bool HaveQuest(uint32_t index) const;
    bool Add(const AB::Entities::Quest& q, AB::Entities::PlayerQuest&& pq);
    bool GetReward(uint32_t questIndex);
    bool SatisfyRequirements(const AB::Entities::Quest& q) const;
    bool SatisfyRequirements(uint32_t index) const;
    bool IsActive(uint32_t index) const;
    bool IsRewarded(uint32_t index) const;
    bool IsRepeatable(uint32_t index) const;
    /// Check if a quest is available for the player
    bool IsAvailable(uint32_t index) const;
    Quest* Get(uint32_t index) const;

    template<typename Callback>
    void VisitQuests(Callback&& callback)
    {
        for (const auto& q : doneQuests_)
        {
            ASSERT(q.second);
            if (callback(*q.second) != Iteration::Continue)
                break;
        }
        for (const auto& q : activeQuests_)
        {
            ASSERT(q.second);
            if (callback(*q.second) != Iteration::Continue)
                break;
        }
    }
    template<typename Callback>
    void VisitActiveQuests(Callback&& callback)
    {
        for (const auto& q : activeQuests_)
        {
            ASSERT(q.second);
            if (!q.second->IsActive())
                continue;
            if (callback(*q.second) != Iteration::Continue)
                break;
        }
    }
};

}
}

