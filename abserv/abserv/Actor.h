/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Events.h"
#include "GameObject.h"
#include "InputQueue.h"
#include <libcommon/UuidUtils.h>
#include <AB/Entities/Effect.h>
#include <AB/Entities/Profession.h>
#include <AB/ProtocolCodes.h>
#include <libshared/Team.h>
#include <libshared/Attributes.h>
#include "ResourceComp.h"

namespace Game {

class SkillBar;
class Item;

namespace Components {
class AttackComp;
class AutoRunComp;
class CollisionComp;
class DamageComp;
class EffectsComp;
class HealComp;
class InputComp;
class InventoryComp;
class MoveComp;
class ProgressComp;
class ResourceComp;
class SelectionComp;
class SkillsComp;
class GroupComp;
}

enum class TargetClass
{
    All,
    Friend,
    Foe
};

enum class SlaveKind
{
    NoSlave = 0,
    Minion,
    Spirit,
    MiniPet,
    AnimalCompanion
};

struct CompanioInfo
{
    std::string script;
    int level;
    int xp;
    int sp;
    std::string name;
};

class AreaOfEffect;
class Group;
class Npc;
class Projectile;

/// Player, NPC, Monster some such
class Actor : public GameObject
{
    friend class Components::MoveComp;              // Needed for accessing octand
    friend class Components::InputComp;             // HandleCommand()
private:
    bool _LuaGotoPosition(const Math::StdVector3& pos);
    void _LuaSetHomePos(const Math::StdVector3& pos);
    bool _LuaShadowStep(const Math::StdVector3& pos);
    void _LuaHeadTo(const Math::StdVector3& pos);
    Math::StdVector3 _LuaGetHomePos() const;
    void _LuaFollowObject(GameObject* object);
    bool _LuaAddEffect(Actor* source, uint32_t index, uint32_t time);
    Effect* _LuaGetLastEffect(AB::Entities::EffectCategory category) const;
    int _LuaRemoveAllEffectsOf(AB::Entities::EffectCategory category);
    bool _LuaRemoveLastEffectOf(AB::Entities::EffectCategory category);
    GameObject* _LuaGetSelectedObject() const;
    void _LuaSetSelectedObject(GameObject* object);
    void _LuaSetAttackError(int error);
    int _LuaGetAttackError() const;
    void _LuaForcePositionUpdate();
    std::vector<Actor*> _LuaGetActorsInRange(Range range);
    std::vector<Actor*> _LuaGetDeadActorsInRange(Range range);
    std::vector<Actor*> _LuaGetAliveActorsInRange(Range range);
    std::vector<Actor*> _LuaGetAlliesInRange(Range range);
    std::vector<Actor*> _LuaGetEnemiesInRange(Range range);
    std::vector<Actor*> _LuaGetGroupMembersRange(Range range);
    AreaOfEffect* _LuaAddAOE(const std::string& script,
        uint32_t index,
        const Math::StdVector3& pos);
    bool _LuaHasEffect(uint32_t index) const;
    bool _LuaHasEffectOf(AB::Entities::EffectCategory category) const;
    std::vector<Effect*> _LuaGetEffectsOf(AB::Entities::EffectCategory category) const;
    std::vector<Effect*> _LuaGetEffects() const;
    void _LuaSetArmorAddition(int value);
    int _LuaGetArmorAddition() const;
    Actor* _LuaGetMaster() const;
    void _LuaSetMaster(Actor* master);
    Npc* _LuaSpawnSlave(const std::string& script, Actor* corpse, SlaveKind kind, int level, const Math::StdVector3& pos);
    int _LuaGetMaxAttribPoints() const;
    float _LuaGetHealthRatio() const;
    SkillBar* _LuaGetSkillBar() const;
    std::vector<Actor*> _LuaGetMinions() const;
    // All salves, minions, spirits
    std::vector<Actor*> _LuaGetSlaves() const;
    bool _LuaIsControllingMinions() const { return !minions_.empty(); }
    bool _LuaIsMyMinion(Actor* actor) const;
    void _LuaScaleToLevel();
    Actor* _LuaGetTarget() const;
    /// Get lower 16 bits of the group mask
    uint32_t GetFriendMask() const;
    /// Get upper 16 bits of the group mask
    uint32_t GetFoeMask() const;
    int GetClassArmorAddition(DamageType damageType) const;
    // Does nothing except signaling exploited a corpse. Actual exploit (e.g. creating a minion must be done in a script)
    void ExploitedCorpse(Actor* corpse);
    size_t masterGettingDamageEvent_{ 0 };
    size_t masterAttackTargetEvent_{ 0 };
    void UnsubscribeEvents();
    // Take an existing NPC and make it our companion
    bool MakeCompanion(Npc* companion);
    bool LoadCompainion(const CompanioInfo& info);
    void PostSpawn();
protected:
    Math::Vector3 homePos_;
    ea::weak_ptr<Actor> killedBy_;
    AB::GameProtocol::NpcType npcType_{ AB::GameProtocol::NpcType::Default };
    int armorAddition_{ 0 };
    bool resurrectable_{ true };
    // When dead the corpse may be used for something
    bool recycleable_{ true };
    size_t actorDiedEvent_{ 0 };
    size_t actorRecurrectedEvent_{ 0 };
    // Actor may be owned by other actors, e.g. minions, spirits
    ea::weak_ptr<Actor> master_;
    // When we are a minion, this is the corpse used too create us
    ea::weak_ptr<Actor> corpse_;
    // All our minions. Since a set is ordered by the value, the oldest minions (which has the lowest ID) will be on top.
    ea::set<uint32_t> minions_;
    SlaveKind slaveKind_{ SlaveKind::NoSlave };
    AB::GameProtocol::Species species_{ AB::GameProtocol::Species::Unknown };
    ea::shared_ptr<Npc> companion_;
    virtual void OnActorDied(Actor* actor, Actor* killer);
    virtual void OnActorResurrected(Actor*) {}
    virtual void OnMasterGettingDamage(Actor*, Skill*, DamageType, int32_t&) {}
    virtual void OnMasterAttackTarget(Actor*, bool&) {}
    virtual bool LoadCompationInfo(CompanioInfo&) { return false; }
    virtual void HandleCommand(AB::GameProtocol::CommandType, const std::string&, MessageStream&) {}
private:
    // Events
    void OnEndUseSkill(Actor* target, Skill* skill);
    void OnStartUseSkill(Actor* target, Skill* skill);
public:
    static void RegisterLua(kaguya::State& state);

    Actor();
    ~Actor() override;

    /// Loading is done initialize properties
    virtual void Initialize();
    bool SetSpawnPoint(const std::string& group);
    void SetHomePos(const Math::Vector3& pos)
    {
        homePos_ = pos;
    }
    void SetGame(ea::shared_ptr<Game> game) override;

    const Math::Vector3& GetHomePos() const { return homePos_; }
    bool GotoHomePos();
    template<typename Func>
    void VisitActorsInRange(Range range, Func&& func) const
    {
        VisitInRange(range, [&](const GameObject& o)
        {
            if (o.IsPlayerOrNpcType())
                return func(To<Actor>(o));
            return Iteration::Continue;
        });
    }
    template<typename Func>
    void VisitEnemiesInRange(Range range, Func&& func) const
    {
        VisitInRange(range, [&](const GameObject& o)
        {
            if (o.IsPlayerOrNpcType())
            {
                const auto& actor = To<Actor>(o);
                if (this->IsEnemy(&actor))
                    return func(actor);
            }
            return Iteration::Continue;
        });
    }
    size_t GetEnemyCountInRange(Range range) const;
    /// Ally is always without self
    template<typename Func>
    void VisitAlliesInRange(Range range, Func&& func) const
    {
        VisitInRange(range, [&](const GameObject& o)
        {
            if (o.IsPlayerOrNpcType())
            {
                const auto& actor = To<Actor>(o);
                if (this->IsAlly(&actor))
                    return func(actor);
            }
            return Iteration::Continue;
        });
    }
    size_t GetAllyCountInRange(Range range) const;
    Actor* GetClosestEnemy(bool undestroyable, bool unselectable);
    Actor* GetClosestAlly(bool undestroyable, bool unselectable);
    virtual Group* GetGroup() const;
    Item* GetWeapon() const;
    Actor* GetKiller() const
    {
        if (auto k = killedBy_.lock())
            return k.get();
        return nullptr;
    }

    bool IsGroupFighting() const;
    void HeadTo(const Math::Vector3& pos);
    void FaceObject(const GameObject* object);
    /// Move speed: 1 = planeNormal speed
    float GetSpeed() const;
    void SetSpeed(float value);
    void AddSpeed(float value);
    bool IsMoving() const;
    void SetState(AB::GameProtocol::CreatureState state) override;
    /// Attack hit
    bool IsHitting() const;
    bool IsUndestroyable() const { return undestroyable_; }
    void SetUndestroyable(bool value) { undestroyable_ = value; }
    bool IsInWeaponRange(Actor* actor) const;
    virtual int GetBaseArmor(DamageType damageType, DamagePos pos);
    /// Get effect of armor. Armor is influenced by the equipment and effects
    /// Damage multiplier.
    /// \param penetration Value between 0..1: 0 = no penetration, 1 = 100% penetration -> no armor
    virtual float GetArmorEffect(DamageType damageType, DamagePos pos, float penetration);
    /// Attack speed in ms. One attack cycle (start hit -> hit -> apply damage) takes this ms to complete.
    virtual uint32_t GetAttackSpeed();
    /// Get increased attack speed relative to planeNormal weapon attack speed. > 1 = faster, < 1 slower
    float GetAttackSpeedIncrease(uint32_t speed) const;
    virtual DamageType GetAttackDamageType();
    virtual int32_t GetAttackDamage(bool critical);
    float GetArmorPenetration(DamageType damageType) const;
    /// Get chance for a critical hit. Value between 0..1
    float GetCriticalChance(const Actor* other);
    DamagePos GetDamagePos() const;
    int GetResource(Components::ResourceType type) const { return resourceComp_->GetValue(type); }
    void SetResource(Components::ResourceType type, Components::SetValueType t, int value);
    /// Steal life from this actor. The source must add the returned value to its life.
    int DrainLife(Actor* source, uint32_t index, int value);
    /// Steal energy from this actor. The source must add the returned value to its energy.
    int DrainEnergy(int value);
    int AddEnergy(int value);
    int AddLife(int value);
    void SetHealthRegen(int value);
    void SetEnergyRegen(int value);
    int GetMorale() const;
    bool IncreaseMorale();
    bool DecreaseMorale();
    void SetMaster(ea::shared_ptr<Actor> master);
    void SetCorpse(ea::shared_ptr<Actor> value);
    ea::shared_ptr<Actor> GetMaster();
    // Maximum number of minions this actor can control
    size_t GetMaxMinions() const;
    void AddMinion(const Actor& minion);
    void RemoveMinion(const Actor& minion);
    AB::GameProtocol::Species GetSpecies() const { return species_; }
    // Can only be set before sending the spawn message, e.g. in the scripts onInit() funnction
    void SetSpecies(AB::GameProtocol::Species value) { species_ = value; }
    void SetSlaveKind(SlaveKind kind);
    SlaveKind GetSlaveKind() const;
    Npc* GetCompanion() const;
    bool RemoveEffect(uint32_t index);

    bool InterruptAttack(Actor* source);
    bool InterruptSkill(Actor* source, AB::Entities::SkillType type);
    /// Interrupt everything
    bool Interrupt(Actor* source);

    virtual bool CanAttack() const { return false; }
    virtual bool CanUseSkill() const { return false; }
    virtual uint32_t GetLevel() const { return 0; }
    std::string GetClassLevel() const;
    virtual void SetLevel(uint32_t) { }
    virtual void AddXp(int) { }
    virtual uint32_t GetXp() const { return 0; }
    /// Get the number of attribute points according the level
    virtual uint32_t GetAttributePoints() const;
    virtual void AddSkillPoint() { }
    virtual uint32_t GetSkillPoints() const { return 0; }
    virtual void AdvanceLevel();
    virtual AB::Entities::CharacterSex GetSex() const
    {
        return AB::Entities::CharacterSex::Unknown;
    }
    virtual uint32_t GetItemIndex() const
    {
        return 0;
    }
    uint32_t GetGroupId() const;
    void SetGroupId(uint32_t value);
    TeamColor GetGroupColor() const;
    void SetGroupColor(TeamColor value);
    virtual size_t GetGroupPos() { return 0u; }
    AB::Entities::ProfessionIndex GetProfIndex() const;
    AB::Entities::ProfessionIndex GetProf2Index() const;
    /// 0 Based
    Skill* GetSkill(uint32_t index) const;
    SkillBar& GetSkillBar() const;
    Skill* GetCurrentSkill() const;
    bool IsUsingSkillOfType(AB::Entities::SkillType type, int32_t minActivationTime) const;
    bool SetEquipment(const std::string& ciUuid);
    bool SetInventory(const std::string& ciUuid);
    bool SetChest(const std::string& ciUuid);
    virtual const std::string& GetPlayerUuid() const;
    virtual const std::string& GetAccountUuid() const;

    void Update(uint32_t timeElapsed, MessageStream& message) override;

    virtual bool Die(Actor* killer);
    virtual bool Resurrect(int percentHealth, int percentEnergy);
    bool IsResurrectable() const { return resurrectable_; }
    void SetResurrectable(bool value) { resurrectable_ = value; }
    bool CanResurrect() const;
    bool IsRecycleable() const { return recycleable_; }
    void SetRecycleable(bool value) { recycleable_ = value; }
    bool IsDead() const { return stateComp_.IsDead(); }
    unsigned GetDeaths() const;
    bool IsKnockedDown() const { return stateComp_.IsKnockedDown(); }
    /// Returns true when the actor can't do anything
    bool IsImmobilized() const { return stateComp_.IsDead() || stateComp_.IsKnockedDown(); }
    /// Knock the Actor down caused by source. If time = 0 DEFAULT_KNOCKDOWN_TIME is used.
    bool KnockDown(Actor* source, uint32_t time);
    int Healing(Actor* source, uint32_t index, int value);
    int Damage(Actor* source, uint32_t index, DamageType type, int value, float penetration);
    bool IsEnemy(const Actor* other) const;
    bool IsAlly(const Actor* other) const;
    void AddFriendFoe(uint32_t frnd, uint32_t foe);
    void RemoveFriendFoe(uint32_t frnd, uint32_t foe);
    void AddInput(InputType type, Utils::VariantMap&& data);
    void AddInput(InputType type);
    // Get the effective attribute rank. This is attributes from Skills+Equipment+Effects
    uint32_t GetAttributeRank(Attribute index) const;

    GameObject* GetSelectedObject() const;
    uint32_t GetSelectedObjectId() const;
    bool SelectObject(GameObject* object);
    bool SelectObjectById(uint32_t id);
    bool GotoPosition(const Math::Vector3& pos);
    bool FollowObject(GameObject* object, bool ping, float maxDist = RANGE_TOUCH);
    bool FollowObjectById(uint32_t objectId, bool ping);
    bool UseSkill(int pos, bool ping);
    bool Attack(Actor* target, bool ping);
    bool AttackById(uint32_t targetId, bool ping);
    bool IsAttackingActor(const Actor* target) const;
    bool IsAttacked() const;
    bool IsGroupAttacked() const;
    /// Cancel attack, use skill, follow
    void CancelAction();
    void CancelAll();
    bool RemoveMaintainableEnch(uint32_t index, uint32_t targetId);

    virtual bool AddToInventory(uint32_t itemId);
    void DropRandomItem();
    ea::shared_ptr<Projectile> AddProjectile(const std::string& itemUuid, ea::shared_ptr<Actor> target);

    bool CollisionNeedsAdjustment(const GameObject& other) const;

    ea::unique_ptr<SkillBar> skills_;

    ea::unique_ptr<Components::ResourceComp> resourceComp_;
    ea::unique_ptr<Components::AttackComp> attackComp_;
    ea::unique_ptr<Components::SkillsComp> skillsComp_;
    ea::unique_ptr<Components::InputComp> inputComp_;
    ea::unique_ptr<Components::DamageComp> damageComp_;
    ea::unique_ptr<Components::HealComp> healComp_;
    ea::unique_ptr<Components::AutoRunComp> autorunComp_;
    ea::unique_ptr<Components::ProgressComp> progressComp_;
    ea::unique_ptr<Components::EffectsComp> effectsComp_;
    ea::unique_ptr<Components::InventoryComp> inventoryComp_;
    ea::unique_ptr<Components::MoveComp> moveComp_;
    ea::unique_ptr<Components::CollisionComp> collisionComp_;
    ea::unique_ptr<Components::SelectionComp> selectionComp_;
    ea::unique_ptr<Components::GroupComp> groupComp_;

    bool undestroyable_{ false };

    bool Serialize(sa::PropWriteStream& stream) override;
    void WriteSpawnData(MessageStream& msg) override;
};

template <>
inline bool Is<Actor>(const GameObject& obj)
{
    return obj.GetType() >= AB::GameProtocol::GameObjectType::Projectile;
}

inline bool TargetClassMatches(const Actor& actor, TargetClass _class, const Actor& target)
{
    return ((_class == TargetClass::All) ||
        (_class == TargetClass::Foe && actor.IsEnemy(&target)) ||
        (_class == TargetClass::Friend && actor.IsAlly(&target)));
}

void GetSkillRecharge(const Actor& actor, Skill* skill, uint32_t& recharge);
void GetSkillActivation(const Actor& actor, Skill* skill, uint32_t& activation);
void GetSkillCost(const Actor& actor, Skill* skill,
    int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
void GetSkillHpSacrifies(const Actor& actor, Skill* skill, uint32_t& hp);
void GetResources(const Actor& actor, int& maxHealth, int& maxEnergy);
void GetRegeneration(const Actor& actor, int& healthRegen, int& energyRegen);

constexpr float GetScaleForLevel(uint32_t level)
{
    // Scale the model to the level
    return Math::Lerp(0.5f, 1.0f, (float)level / (float)LEVEL_CAP);
}

}
