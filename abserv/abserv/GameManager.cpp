/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GameManager.h"
#include "AiDebugServer.h"
#include "Game.h"
#include "Group.h"
#include "IOGame.h"
#include "Npc.h"
#include "Player.h"
#include <sa/Assert.h>

namespace Game {

void GameManager::Start()
{
    // Main Thread
    std::scoped_lock lock(lock_);
    state_ = State::Running;
}

void GameManager::Stop()
{
    // Main Thread
    if (state_ != State::Running)
        return;

    {
        std::scoped_lock lock(lock_);
        state_ = State::Terminated;
    }
    for (const auto& g : games_)
    {
        g.second->SetState(Game::ExecutionState::Terminated);
    }
}

ea::shared_ptr<Game> GameManager::CreateGame(const std::string& mapUuid)
{
    ASSERT(state_ == State::Running);
#ifdef DEBUG_GAME
    LOG_DEBUG << "Creating game " << mapUuid << std::endl;
#endif
    ea::shared_ptr<Game> game = ea::make_shared<Game>();
    {
        std::scoped_lock lock(lock_);
        game->id_ = GetNewGameId();
        games_[game->id_] = game;
        maps_[mapUuid].push_back(game.get());
    }
    game->instanceData_.number = static_cast<uint16_t>(maps_[mapUuid].size());
    game->Load(mapUuid);
    {
        std::scoped_lock lock(lock_);
        GetSubsystem<AI::DebugServer>()->AddGame(game);
    }
#ifdef DEBUG_GAME
    LOG_DEBUG << "Created game " << mapUuid << " " << game->GetName() << std::endl;
#endif
    return game;
}

void GameManager::DeleteGameTask(uint32_t gameId)
{
#ifdef DEBUG_GAME
    LOG_DEBUG << "Deleting game " << gameId << std::endl;
#endif
    // Dispatcher Thread
    auto it = games_.find(gameId);
    if (it != games_.end())
    {
        // games_.size() may be called from another thread so lock it
        std::scoped_lock lock(lock_);
        GetSubsystem<AI::DebugServer>()->RemoveGame(gameId);
        maps_.erase((*it).second->data_.uuid);
        games_.erase(it);
    }
}

ea::shared_ptr<Game> GameManager::GetOrCreateInstance(const std::string& mapUuid, const std::string& instanceUuid)
{
    auto result = GetInstance(instanceUuid);
    if (result)
        return result;
    result = CreateGame(mapUuid);
    if (result)
    {
        result->instanceData_.uuid = instanceUuid;
    }
    return result;
}

bool GameManager::InstanceExists(const std::string& uuid)
{
    auto it = ea::find_if(games_.begin(), games_.end(), [&](auto const& current)
    {
        return Utils::Uuid::IsEqual(current.second->instanceData_.uuid, uuid);
    });
    return it != games_.end();
}

ea::shared_ptr<Game> GameManager::GetInstance(const std::string& instanceUuid)
{
    auto it = ea::find_if(games_.begin(), games_.end(), [&](auto const& current)
    {
        return Utils::Uuid::IsEqual(current.second->instanceData_.uuid, instanceUuid);
    });
    if (it != games_.end())
        return (*it).second;

    return {};
}

ea::shared_ptr<Game> GameManager::GetGame(const std::string& mapUuid, bool canCreate /* = false */)
{
    AB::Entities::GameType gType = GetGameType(mapUuid);
    if (gType >= AB::Entities::GameTypePvPCombat && canCreate)
        // These games are exclusive
        return CreateGame(mapUuid);

    const auto it = maps_.find(mapUuid);
    if (it == maps_.end())
    {
        // No instance of this map exists
        if (!canCreate)
            return {};
        return CreateGame(mapUuid);
    }
    // There are already some games with this map
    for (const auto& g : it->second)
    {
        if (g->GetPlayerCount() < GAME_MAX_PLAYER &&
            (g->GetState() == Game::ExecutionState::Running || g->GetState() == Game::ExecutionState::Startup))
        {
            const auto git = games_.find(g->id_);
            return (*git).second;
        }
    }
    if (canCreate)
        return CreateGame(mapUuid);
    LOG_WARNING << "No game with UUID " << mapUuid << std::endl;
    return {};
}

ea::shared_ptr<Game> GameManager::Get(uint32_t gameId)
{
    auto it = games_.find(gameId);
    if (it != games_.end())
    {
        return (*it).second;
    }
    return {};
}

void GameManager::CleanGames()
{
    if (games_.empty())
    {
        // If no games we can reset IDs
        gameIds_.Reset();
        GameObject::objectIds_.Reset();
        Group::groupIds_.Reset();
        return;
    }

    for (const auto& g : games_)
    {
        if (g.second->IsInactive())
            g.second->SetState(Game::ExecutionState::Terminated);
    }
}

AB::Entities::GameType GameManager::GetGameType(const std::string& mapUuid)
{
    return IO::GetGameType(mapUuid);
}

}
