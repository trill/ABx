/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Agent.h>
#include <libmath/Vector3.h>
#include <sa/StrongType.h>

namespace Game {
class Npc;
}

namespace AI {

using SkillIndexType = sa::StrongType<int, struct SkillIndexTag>;
using TargetIdType = sa::StrongType<Id, struct TargetTag>;
using DistanceType = sa::StrongType<float, struct DistanceTag>;

using AiAgentContext = Context<
    SkillIndexType,
    TargetIdType,
    DistanceType,
    Math::Vector3
>;

class AiAgent final : public Agent
{
private:
    Game::Npc& npc_;
public:
    explicit AiAgent(Game::Npc& npc);
    ~AiAgent() override;

    Game::Npc& GetNpc() { return npc_; }
    const Game::Npc& GetNpc() const { return npc_; }

    int selectedSkill_{ -1 };
    AiAgentContext aiContext_;
};

inline Game::Npc& GetNpc(Agent& agent)
{
    return static_cast<AiAgent&>(agent).GetNpc();
}
inline const Game::Npc& GetNpc(const Agent& agent)
{
    return static_cast<const AiAgent&>(agent).GetNpc();
}
inline AiAgent& GetAgent(Agent& agent)
{
    return static_cast<AiAgent&>(agent);
}
inline const AiAgent& GetAgent(const Agent& agent)
{
    return static_cast<const AiAgent&>(agent);
}

}
