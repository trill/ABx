/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "IOAsset.h"
#include "Terrain.h"

namespace IO {

class IOHeightMap final : public IOAssetImpl<Game::HeightMap>
{
public:
    bool Import(Game::HeightMap& asset, const std::string& name) override;
};

}
