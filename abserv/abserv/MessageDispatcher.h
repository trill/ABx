/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Net {
class MessageMsg;
}

void DispatchMessage(const Net::MessageMsg& msg);
