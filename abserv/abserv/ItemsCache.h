/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/IdGenerator.h>
#include <eastl.hpp>

namespace Game {

class Item;

// The owner of all items. Because Items move around alot, e.g. people trade items,
// store them in chest inventory, i.e. would change ownership, and changing ownerhip
// of unique_ptr's is a bit cumbersome, the real owners of the items store only the
// IDs in this cache.
class ItemsCache
{
private:
    static sa::IdGenerator<uint32_t> itemIds_;
    ea::unordered_map<uint32_t, ea::unique_ptr<Item>> itemCache_;
    ea::unordered_map<std::string, uint32_t> concreteIds_;
public:
    Item* Get(uint32_t id);
    uint32_t GetConcreteId(const std::string& uuid) const;
    uint32_t Add(ea::unique_ptr<Item>&& item);
    // Item IDs are not reused, so removing an item from the cache, the item also
    // disapperas from the players inventory/chest
    void Remove(uint32_t id);
    void RemoveConcrete(const std::string& uuid);
};

}
