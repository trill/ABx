/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ItemDrop.h"
#include "Item.h"
#include "Actor.h"
#include "ItemFactory.h"
#include "Game.h"
#include "ItemsCache.h"
#include "Player.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <sa/time.h>

namespace Game {

void ItemDrop::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["ItemDrop"].setClass(std::move(kaguya::UserdataMetatable<ItemDrop, GameObject>()
        .addProperty("Item", &ItemDrop::_LuaGetItem)
        .addProperty("Source", &ItemDrop::_LuaGetSource)
        .addProperty("Target", &ItemDrop::_LuaGetTarget)
    ));
    // clang-format on
}

ItemDrop::ItemDrop(uint32_t itemId) :
    itemId_(itemId),
    dropTick_(sa::time::tick())
{
    events_.Subscribe<OnClickedSignature>(OnClickedEvent, std::bind(&ItemDrop::OnClicked, this, std::placeholders::_1));
    // Drops can not hide other objects
    SetOccluder(false);
    selectable_ = true;
    auto* cache = GetSubsystem<ItemsCache>();
    auto* item = cache->Get(itemId);
    if (item)
    {
        name_ = item->data_.name;
        itemIndex_ = item->data_.index;
        concreteUuid_ = item->concreteItem_.uuid;
    }
}

ItemDrop::~ItemDrop()
{
    if (!pickedUp_ && itemId_ != 0)
    {
        // Not picked up delete it
        auto* factory = GetSubsystem<ItemFactory>();
        factory->DeleteItemById(itemId_);
    }
}

Actor* ItemDrop::_LuaGetSource() const
{
    if (auto sharedSource = source_.lock())
        return sharedSource.get();
    return nullptr;
}

Actor* ItemDrop::_LuaGetTarget() const
{
    if (targetId_ == 0)
        return nullptr;
    return GetGame()->GetObject<Actor>(targetId_);
}

Item* ItemDrop::_LuaGetItem() const
{
    return const_cast<Item*>(GetItem());
}

void ItemDrop::Update(uint32_t timeElapsed, MessageStream& message)
{
    GameObject::Update(timeElapsed, message);
    using namespace sa::time::literals;
    if ((targetId_ != 0) && sa::time::time_elapsed(dropTick_) >= 300_s)
    {
        // If it wasn't picked up in 5 minutes it becomes available for all
        targetId_ = 0;
        AB::Packets::Server::DropTargetChanged packet = {
            id_, targetId_
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::DropTargetChanged, packet);
    }
}

uint32_t ItemDrop::GetItemIndex() const
{
    return itemIndex_;
}

const Item* ItemDrop::GetItem() const
{
    auto* cache = GetSubsystem<ItemsCache>();
    return cache->Get(itemId_);
}

void ItemDrop::PickUp(Actor* actor)
{
    if (!actor)
        return;

    // if targetId_ == 0 all can pick it up
    if (targetId_ != 0 && targetId_ != actor->GetId())
    {
        if (Is<Player>(actor))
            To<Player>(actor)->SendError(AB::GameProtocol::PlayerErrorValue::DropForOtherPlayer);
        return;
    }

    if (actor->AddToInventory(itemId_))
    {
        pickedUp_ = true;
        if (actor->GetSelectedObjectId() == id_)
            actor->SelectObjectById(0);
        Remove();
    }
}

uint32_t ItemDrop::GetSourceId()
{
    if (auto s = source_.lock())
        return s->id_;
    return 0;
}

void ItemDrop::OnClicked(Actor* actor)
{
    if (!actor)
        return;
    if (!IsInRange(Range::Adjecent, actor))
        return;

    PickUp(actor);
}

void ItemDrop::SetSource(ea::shared_ptr<Actor> source)
{
    source_ = source;
}

bool ItemDrop::Serialize(sa::PropWriteStream& stream)
{
    using namespace AB::GameProtocol;

    auto* cache = GetSubsystem<ItemsCache>();
    auto* item = cache->Get(itemId_);
    if (!item)
        return false;

    static constexpr uint32_t validFields = ObjectSpawnDataFieldName | ObjectSpawnDataFieldModelIndex;
    stream.Write<uint32_t>(validFields);
    if (!GameObject::Serialize(stream))
        return false;
    stream.Write<uint32_t>(item->data_.index);                   // Model index
    return true;
}

void ItemDrop::WriteSpawnData(MessageStream& msg)
{
    auto& m = msg.GetMessage(64);
    GameObject::WriteSpawnData(msg);

    using namespace AB::GameProtocol;
    static constexpr uint32_t validFields = ObjectSpawnFieldPos | ObjectSpawnFieldRot | ObjectSpawnFieldScale |
        ObjectSpawnFieldUndestroyable | ObjectSpawnFieldSelectable | ObjectSpawnFieldState;
    m.Add<uint32_t>(validFields);

    m.Add<float>(transformation_.position_.x_);
    m.Add<float>(transformation_.position_.y_);
    m.Add<float>(transformation_.position_.z_);
    m.Add<float>(transformation_.GetYRotation());
    m.Add<float>(transformation_.scale_.x_);
    m.Add<float>(transformation_.scale_.y_);
    m.Add<float>(transformation_.scale_.z_);
    m.Add<bool>(true);                                  // not destroyable
    m.Add<bool>(selectable_);
    m.Add<uint8_t>(static_cast<uint8_t>(stateComp_.GetState()));
    sa::PropWriteStream data;
    size_t dataSize = 0;
    Serialize(data);
    const char* cData = data.GetStream(dataSize);
    m.Add(std::string(cData, dataSize));
}

}
