/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>

namespace AI {
namespace Filters {

class SelectLowHealth final : public Filter
{
    FILTER_CLASS(SelectLowHealth)
public:
    explicit SelectLowHealth(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
