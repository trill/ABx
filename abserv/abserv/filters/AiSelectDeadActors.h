/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>

namespace AI {
namespace Filters {

class SelectDeadActors final : public Filter
{
    FILTER_CLASS(SelectDeadActors)
public:
    explicit SelectDeadActors(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
