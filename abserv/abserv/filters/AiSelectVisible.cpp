/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectVisible.h"
#include "../Npc.h"

namespace AI {
namespace Filters {

SelectVisible::SelectVisible(const ArgumentsType& arguments) :
    Filter(arguments)
{
    std::string ff;
    if (GetArgument<std::string>(arguments, 0, ff))
    {
        if (ff.compare("friend") == 0)
            class_ = Game::TargetClass::Friend;
        else if (ff.compare("foe") == 0)
            class_ = Game::TargetClass::Foe;
    }
    GetArgument<Game::Range>(arguments, 1, range_);
}

void SelectVisible::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    const Game::Npc& chr = GetNpc(agent);
    chr.VisitInRange(range_, [&](const Game::GameObject& o)
    {
        if (!Game::Is<Game::Actor>(o))
            return Iteration::Continue;

        const Game::Actor& actor = Game::To<Game::Actor>(o);

        if (!Game::TargetClassMatches(chr, class_, actor))
            return Iteration::Continue;

        if (chr.IsObjectInSight(o))
            entities.push_back(o.id_);
        return Iteration::Continue;
    });
}

}
}
