/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSortByDistance.h"
#include "../Game.h"
#include "../Npc.h"
#include <sa/Assert.h>
#include <CleanupNs.h>

namespace AI {
namespace Filters {

void SortByDistance::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    if (entities.size() == 0)
        return;

    Game::Npc& chr = GetNpc(agent);
    ASSERT(chr.HasGame());

    auto& game = *chr.GetGame();
    std::map<uint32_t, float> sorting;

    for (auto id : entities)
    {
        auto* object = game.GetObject<Game::GameObject>(id);
        sorting[id] = chr.GetDistance(object);
    }
    std::sort(entities.begin(), entities.end(), [&sorting](uint32_t i, uint32_t j)
    {
        return sorting[i] < sorting[j];
    });
}

}

}
