/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>
#include <libshared/Mechanic.h>

namespace AI {
namespace Filters {

// Select the foe with most other foes around
class SelectMob final : public Filter
{
    FILTER_CLASS(SelectMob)
private:
    Game::Range range_{ Game::Range::Adjecent };
public:
    explicit SelectMob(const ArgumentsType& arguments) :
        Filter(arguments)
    {
        GetArgument<Game::Range>(arguments, 0, range_);
    }
    void Execute(Agent& agent) override;
};

}
}
