/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>

namespace AI {
namespace Filters {

// Select a target that is in attack state
class SelectTargetAttacking final : public Filter
{
    FILTER_CLASS(SelectTargetAttacking)
public:
    explicit SelectTargetAttacking(const ArgumentsType& arguments) :
      Filter(arguments)
    { }
    void Execute(Agent& agent) override;
};

}
}
