/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectAggro.h"
#include "../Npc.h"

namespace AI {
namespace Filters {

SelectAggro::SelectAggro(const ArgumentsType& arguments) :
    Filter(arguments)
{ }

void SelectAggro::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    Game::Npc& chr = GetNpc(agent);
    chr.VisitEnemiesInRange(Game::Range::Aggro, [&](const Game::Actor& o)
    {
        entities.push_back(o.id_);
        return Iteration::Continue;
    });
}

}
}
