/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>

namespace AI {
namespace Filters {

class SelectRandom final : public Filter
{
    FILTER_CLASS(SelectRandom)
private:
    uint32_t count_{ 1 };
public:
    explicit SelectRandom(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
