/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>
#include <libshared/Mechanic.h>
#include "../Actor.h"

namespace AI {
namespace Filters {

// Select all objects that are visible (not obstructed) to the owner.
class SelectVisible final : public Filter
{
    FILTER_CLASS(SelectVisible)
private:
    Game::Range range_{ Game::Range::Aggro };
    Game::TargetClass class_{ Game::TargetClass::All };
public:
    explicit SelectVisible(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
