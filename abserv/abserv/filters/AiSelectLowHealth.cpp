/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectLowHealth.h"
#include "../Npc.h"
#include <libcommon/Logger.h>

//#define DEBUG_AI

namespace AI {
namespace Filters {

SelectLowHealth::SelectLowHealth(const ArgumentsType& arguments) :
    Filter(arguments)
{ }

void SelectLowHealth::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    Game::Npc& chr = GetNpc(agent);
    std::map<uint32_t, std::pair<float, float>> sorting;

    chr.VisitAlliesInRange(Game::Range::HalfCompass, [&](const Game::Actor& o)
    {
        if (!o.IsDead() && o.resourceComp_->GetHealthRatio() < LOW_HP_THRESHOLD)
        {
            entities.push_back(o.id_);
            sorting[o.id_] = std::make_pair<float, float>(o.resourceComp_->GetHealthRatio(), o.GetDistance(&chr));
        }
        return Iteration::Continue;
    });
    std::sort(entities.begin(), entities.end(), [&sorting](uint32_t i, uint32_t j)
    {
        const std::pair<float, float>& p1 = sorting[i];
        const std::pair<float, float>& p2 = sorting[j];
        if (fabs(p1.first - p2.first) < 0.05f)
            // If same HP (max 5% difference) use shorter distance
            return p1.second < p2.second;
        return p1.first < p2.first;
    });
#ifdef DEBUG_AI
    if (chr.GetProfIndex() == 3)
        LOG_DEBUG << chr.GetName() << " " << entities.size() << std::endl;
#endif
}

}
}
