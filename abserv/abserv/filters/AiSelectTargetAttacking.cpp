/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectTargetAttacking.h"
#include "../AiAgent.h"
#include "../Npc.h"
#include "../Skill.h"
#include "../AttackComp.h"

namespace AI {
namespace Filters {

void SelectTargetAttacking::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    Game::Npc& chr = GetNpc(agent);
    chr.VisitEnemiesInRange(Game::Range::Casting, [&](const Game::Actor& current)
    {
        if (current.IsDead() || current.IsUndestroyable())
            return Iteration::Continue;

        if (current.attackComp_->IsHitting())
        {
            entities.push_back(current.id_);
            return Iteration::Break;
        }
        if (auto* skill = current.GetCurrentSkill())
        {
            if (skill->IsType(AB::Entities::SkillTypeAttack))
            {
                entities.push_back(current.id_);
                return Iteration::Break;
            }
        }
        return Iteration::Continue;
    });
}

}
}
