/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>
#include <libshared/Damage.h>
#include "../Actor.h"

namespace AI {
namespace Filters {

// Select actors that got damage of certain type.
class SelectGettingDamage final : public Filter
{
    FILTER_CLASS(SelectGettingDamage)
private:
    Game::TargetClass class_{ Game::TargetClass::All };
    Game::DamageTypeCategory category_{ Game::DamageTypeCategory::Any };
public:
    explicit SelectGettingDamage(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
