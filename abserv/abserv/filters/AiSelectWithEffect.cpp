/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectWithEffect.h"
#include "../Npc.h"
#include "../EffectManager.h"
#include "../EffectsComp.h"

namespace AI {
namespace Filters {

SelectWithEffect::SelectWithEffect(const ArgumentsType& arguments) :
    Filter(arguments)
{
    std::string eff;
    if (GetArgument<std::string>(arguments, 0, eff))
    {
        effectCat_ = Game::EffectCatNameToEffectCat(eff);
    }
    std::string ff;
    if (GetArgument<std::string>(arguments, 1, ff))
    {
        if (ff.compare("friend") == 0)
            class_ = Game::TargetClass::Friend;
        else if (ff.compare("foe") == 0)
            class_ = Game::TargetClass::Foe;
    }
}

void SelectWithEffect::Execute(Agent &agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    const Game::Npc& chr = GetNpc(agent);

    chr.VisitInRange(Game::Range::Aggro, [&](const Game::GameObject& current)
    {
        if (!Game::Is<Game::Actor>(current))
            return Iteration::Continue;

        const Game::Actor& actor = Game::To<Game::Actor>(current);
        if (Game::TargetClassMatches(chr, class_, actor) &&
            actor.effectsComp_->HasEffectOf(effectCat_))
            entities.push_back(actor.id_);

        return Iteration::Continue;
    });
}

}
}
