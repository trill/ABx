/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectAttackers.h"
#include "../Npc.h"
#include "../Group.h"
#include "../AttackComp.h"
#include "../DamageComp.h"
#include <libcommon/Logger.h>

//#define DEBUG_AI

namespace AI {
namespace Filters {

SelectAttackers::SelectAttackers(const ArgumentsType& arguments) :
    Filter(arguments)
{
}

void SelectAttackers::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();

    std::map<uint32_t, float> sorting;
    Game::Npc& chr = GetNpc(agent);

    auto getAttackersOfActor = [&sorting, &entities](const Game::Actor& actor)
    {
        actor.VisitEnemiesInRange(Game::Range::Compass, [&](const Game::Actor& o)
        {
            if (o.IsDead())
                return Iteration::Continue;
            if (o.attackComp_->IsTarget(&actor) || actor.damageComp_->IsLastDamager(o))
            {
                entities.push_back(o.id_);
                sorting[o.id_] = o.GetDistance(&actor);
            }
            return Iteration::Continue;
        });
    };

    if (const auto* group = chr.GetGroup())
    {
#ifdef DEBUG_AI
        LOG_DEBUG << "Selecting group attackers, group members " << group->GetMemberCount() << std::endl;
#endif
        group->VisitMembers([&](const Game::Actor& current) -> Iteration
        {
            getAttackersOfActor(current);
            return Iteration::Continue;
        });
    }
    else
    {
        getAttackersOfActor(chr);
    }

#ifdef DEBUG_AI
    LOG_DEBUG << "Selected " << entities.size() << std::endl;
#endif
    std::sort(entities.begin(), entities.end(), [&sorting](uint32_t i, uint32_t j)
        {
            const float& p1 = sorting[i];
            const float& p2 = sorting[j];
            return p1 < p2;
        });
}

}
}
