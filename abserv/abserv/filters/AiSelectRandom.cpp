/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectRandom.h"
#include <sa/Container.h>
#include <libcommon/Subsystems.h>
#include <libcommon/Random.h>

namespace AI {
namespace Filters {

SelectRandom::SelectRandom(const ArgumentsType& arguments) :
    Filter(arguments)
{
    if (arguments.size() != 0)
        count_ = static_cast<uint32_t>(atoi(arguments[0].c_str()));
    else
        count_ = 1;
}

void SelectRandom::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    if (count_ == 0)
        return;
    if (count_ >= entities.size())
        return;

    auto* rnd = GetSubsystem<Crypto::Random>();
    AI::AgentIds copy(entities);
    entities.clear();
    while (entities.size() < count_ && copy.size() > 0)
    {
        const float rn = rnd->GetFloat();
        auto it = sa::SelectRandomly(copy.begin(), copy.end(), rn);
        entities.push_back(*it);
        copy.erase(it);
    }
}

}
}
