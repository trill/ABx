/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectGettingDamage.h"
#include "../DamageComp.h"
#include "../Npc.h"
#include "../AiAgent.h"

namespace AI {
namespace Filters {

SelectGettingDamage::SelectGettingDamage(const ArgumentsType& arguments) :
    Filter(arguments)
{
    GetArgument<Game::DamageTypeCategory>(arguments, 0, category_);
    std::string ff;
    if (GetArgument<std::string>(arguments, 1, ff))
    {
        if (ff.compare("friend") == 0)
            class_ = Game::TargetClass::Friend;
        else if (ff.compare("foe") == 0)
            class_ = Game::TargetClass::Foe;
    }
}

void SelectGettingDamage::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    Game::Npc& chr = GetNpc(agent);
    chr.VisitInRange(Game::Range::Aggro, [&](const Game::GameObject& current)
    {
        if (!Game::Is<Game::Actor>(current))
            return Iteration::Continue;

        const Game::Actor& actor = Game::To<Game::Actor>(current);

        if (Game::TargetClassMatches(chr, class_, actor))
        {
            if (actor.damageComp_->GotDamageCategory(category_))
                entities.push_back(actor.id_);
        }
        return Iteration::Continue;
    });
}

}
}
