/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSelectTargetUsingSkill.h"
#include "../Npc.h"
#include <libcommon/Random.h>
#include <libcommon/Subsystems.h>

namespace AI {
namespace Filters {

SelectTargetUsingSkill::SelectTargetUsingSkill(const ArgumentsType& arguments) :
    Filter(arguments)
{
    GetArgument<AB::Entities::SkillType>(arguments, 0, type_);
    std::string ff;
    if (GetArgument<std::string>(arguments, 1, ff))
    {
        if (ff.compare("friend") == 0)
            class_ = Game::TargetClass::Friend;
        else if (ff.compare("foe") == 0)
            class_ = Game::TargetClass::Foe;
    }
    GetArgument<int32_t>(arguments, 2, minActivationTime_);
}

void SelectTargetUsingSkill::Execute(Agent& agent)
{
    auto& entities = agent.filteredAgents_;
    entities.clear();
    Game::Npc& chr = GetNpc(agent);
    auto& rng = *GetSubsystem<Crypto::Random>();
    chr.VisitInRange(Game::Range::Casting, [&](const Game::GameObject& current)
    {
        if (!Game::Is<Game::Actor>(current))
            return Iteration::Continue;

        const Game::Actor& actor = Game::To<Game::Actor>(current);
        if (!Game::TargetClassMatches(chr, class_, actor))
            return Iteration::Continue;
        if (!actor.IsSelectable() || actor.IsDead() || actor.IsUndestroyable())
            return Iteration::Continue;

        if (actor.IsUsingSkillOfType(type_, minActivationTime_))
        {
            if (rng.GetFloat() < 0.3f)
                return Iteration::Continue;
#ifdef DEBUG_AI
            LOG_DEBUG << "Selected " << actor.GetName() << std::endl;
#endif
            entities.push_back(actor.id_);
            return Iteration::Break;
        }
        return Iteration::Continue;
    });
}

}
}
