/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Filter.h>

namespace AI {
namespace Filters {

class SortByDistance final : public Filter
{
    FILTER_CLASS(SortByDistance)
public:
    SortByDistance(const ArgumentsType& arguments) :
        Filter(arguments)
    { }
    void Execute(Agent& agent) override;
};

}
}
