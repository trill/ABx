/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <memory>
#include <map>
#include <sa/StringHash.h>
#include <AB/ProtocolCodes.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Game {

class Game;
class Player;
class Party;
class Npc;

enum class ChatType
{
    /// Returns nullptr
    None = static_cast<int>(AB::GameProtocol::ChatChannel::Unknown),
    /// Local map chat
    Map = static_cast<int>(AB::GameProtocol::ChatChannel::General),     // ID = GameID
    /// Guild messages get all guild members on all servers
    Guild = static_cast<int>(AB::GameProtocol::ChatChannel::Guild),     // ID = StringHash(Guild.uuid)
    Party = static_cast<int>(AB::GameProtocol::ChatChannel::Party),     // ID = PartyID
    /// There may be allies on the map that do not belong to the party
    Allies = static_cast<int>(AB::GameProtocol::ChatChannel::Allies),   //
    /// Trade messages get all players on all servers
    Trade = static_cast<int>(AB::GameProtocol::ChatChannel::Trade),     // ID = 0
    Whisper = static_cast<int>(AB::GameProtocol::ChatChannel::Whisper), // ID = PlayerID
};

class ChatChannel
{
protected:
    uint64_t id_;
public:
    explicit ChatChannel(uint64_t id) :
        id_(id)
    {}
    virtual ~ChatChannel();
    virtual bool Talk(Player&, const std::string&)
    {
        return false;
    }
    virtual bool TalkNpc(Npc&, const std::string&)
    {
        return false;
    }
};

class GameChatChannel : public ChatChannel
{
private:
    ea::weak_ptr<Game> game_;
public:
    explicit GameChatChannel(uint64_t id);
    bool Talk(Player& player, const std::string& text) override;
    bool TalkNpc(Npc& npc, const std::string& text) override;
};

class PartyChatChannel : public ChatChannel
{
public:
    explicit PartyChatChannel(uint64_t id) :
        ChatChannel(id),
        party_(nullptr)
    { }
    bool Talk(Player& player, const std::string& text) override;
    bool TalkNpc(Npc& npc, const std::string& text) override;
    Party* party_;
};

class WhisperChatChannel : public ChatChannel
{
private:
    /// The recipient
    ea::weak_ptr<Player> player_;
    std::string playerUuid_;
public:
    explicit WhisperChatChannel(uint64_t id);
    WhisperChatChannel(const std::string& playerUuid);
    bool Talk(Player& player, const std::string& text) override;
    bool Talk(const std::string& playerName, const std::string& text);
    bool TalkNpc(Npc& npc, const std::string& text) override;
};

/// Trade channel, all players on all servers will get it
class TradeChatChannel : public ChatChannel
{
public:
    TradeChatChannel() :
        ChatChannel(0)
    { }
    bool Talk(Player& player, const std::string& text) override;
    void Broadcast(const std::string& playerName, const std::string& text);
};

class GuildChatChannel : public ChatChannel
{
private:
    std::string guildUuid_;
public:
    explicit GuildChatChannel(const std::string& guildUuid) :
        ChatChannel(sa::StringHash(guildUuid)),
        guildUuid_(guildUuid)
    { }
    bool Talk(Player& player, const std::string& text) override;
    void Broadcast(const std::string& playerName, const std::string& text);
};

class Chat
{
    NON_COPYABLE(Chat)
private:
    // Type | ID
    ea::map<ea::pair<ChatType, uint64_t>, ea::shared_ptr<ChatChannel>> channels_;
    ea::shared_ptr<ChatChannel> tradeChat_;
public:
    Chat();
    ~Chat() = default;

    ea::shared_ptr<ChatChannel> Get(ChatType type, uint64_t id);
    ea::shared_ptr<ChatChannel> Get(ChatType type, const std::string& uuid);
    void Remove(ChatType type, uint64_t id);
    void CleanChats();
};

}
