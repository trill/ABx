/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "RangesObjects.h"

namespace Game {

void RangesObjects::Add(Range range, uint32_t id)
{
    if (range < Range::Map)
        objects_[static_cast<size_t>(range)].emplace(id);
}

void RangesObjects::Clear()
{
    for (auto& r : objects_)
        r.clear();
}

bool RangesObjects::Contains(Range range, uint32_t id) const
{
    if (range == Range::Map)
        return true;
    const auto& r = objects_[static_cast<size_t>(range)];
    return r.contains(id);
}

Range RangesObjects::GetRangeOfObject(uint32_t id) const
{
    Range result = Range::Map;
    // When an object is in a range, it is also in all other closer ranges
    VisitRangesReverse([id, &result](Range range, const auto& objects)
    {
        if (objects.contains(id))
        {
            result = range;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });
    return result;
}

}
