/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class MessageStream;

namespace Components {

class ProgressComp
{
    NON_COPYABLE(ProgressComp)
    NON_MOVEABLE(ProgressComp)
private:
    enum class ProgressType
    {
        XPIncrease,
        GotSkillPoint,
        LevelAdvance,
        TitleAdvance,
        AttributePointGain,
        ScaleChanged,
        XPInitial,
        SPInitial,
    };
    struct ProgressItem
    {
        ProgressType type;
        union
        {
            int32_t intValue;
            float floatValue;
        };
    };
    ea::vector<ProgressItem> items_;
    Actor& owner_;
    unsigned deaths_{ 0 };
private:
    /// The owner died
    void OnDied(Actor*, Actor* killer);
    /// A foe was killed nearby
    void OnKilledFoe(Actor* foe, Actor* killer);
public:
    ProgressComp() = delete;
    explicit ProgressComp(Actor& owner);
    ~ProgressComp() = default;

    void Update(uint32_t /* timeElapsed */) { }
    void Write(MessageStream& message);

    void Initiazlize();
    void AddXp(int value);
    /// Somewhere close an enemy died, so we get some XP
    void AddXpForKill(const Actor& victim);
    void AddSkillPoint();
    void AdvanceLevel();
    unsigned GetDeaths() const { return deaths_; }
    void ScaleToLevel();
};

}
}
