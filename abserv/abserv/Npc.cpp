/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Npc.h"
#include "DamageComp.h"
#include "DataProvider.h"
#include "Game.h"
#include "Group.h"
#include "Player.h"
#include "QuestComp.h"
#include "Script.h"
#include "Skill.h"
#include "SkillBar.h"
#include "TriggerComp.h"
#include "WanderComp.h"
#include <libai/BevaviorCache.h>
#include <sa/Assert.h>
#include <sa/TemplateParser.h>
#include <libcommon/DataClient.h>
#include "GroupComp.h"
#include <libcommon/Subsystems.h>
#include <libcommon/Random.h>

namespace Game {

void Npc::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Npc"].setClass(std::move(kaguya::UserdataMetatable<Npc, Actor>()
        .addFunction("SetLevel", &Npc::SetLevel)     // Can only be used in onInit(), i.e. before it is sent to the clients
        .addFunction("Say", &Npc::Say)
        .addFunction("GetScriptFile", &Npc::_LuaGetScriptFile) // Script file is an indicator what type of NPC it is
        .addFunction("SayQuote", &Npc::SayQuote)
        .addFunction("Whisper", &Npc::Whisper)
        .addFunction("ShootAt", &Npc::ShootAt)
        .addFunction("SetBehavior", &Npc::SetBehavior)
        .addFunction("AddWanderPoint", &Npc::_LuaAddWanderPoint)
        .addFunction("AddWanderPoints", &Npc::_LuaAddWanderPoints)
        .addFunction("AddQuest", &Npc::_LuaAddQuest)
        .addFunction("SetItemIndex", &Npc::SetItemIndex)

        .addProperty("CombatMode", &Npc::_LuaGetCombatMode, &Npc::_LuaSetCombatMode)
        .addProperty("NpcType", &Npc::_LuaGetNpcType, &Npc::_LuaSetNpcType)
        .addProperty("ServerOnly", &Npc::IsServerOnly, &Npc::SetServerOnly)
        .addProperty("Tameable", &Npc::IsTameable, &Npc::SetTameable)
        .addProperty("Wander", &Npc::IsWander, &Npc::SetWander)
    ));
    // clang-format on
}

Npc::Npc() :
    lua_(*this)
{
    events_.Subscribe<OnAttackStartSignature>(OnSourceAttackStartEvent, std::bind(&Npc::OnSourceAttackStart, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnAttackSuccessSignature>(OnSourceAttackSuccessEvent, std::bind(&Npc::OnSourceAttackSuccess, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnAttackStartSignature>(OnTargetAttackStartEvent, std::bind(&Npc::OnTargetAttackStart, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnAttackEndSignature>(OnTargetAttackEndEvent, std::bind(
        &Npc::OnTargetAttackEnd, this,
        std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    events_.Subscribe<OnInterruptingAttackSignature>(OnInterruptingAttackEvent, std::bind(&Npc::OnInterruptingAttack, this, std::placeholders::_1));
    events_.Subscribe<OnInterruptingSkillSignature>(OnInterruptingSkillEvent,
        std::bind(&Npc::OnInterruptingSkill, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    events_.Subscribe<OnGettingSkillTargetSignature>(OnGettingSkillTargetEvent, std::bind(&Npc::OnGettingSkillTarget, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnCanUseSkillSignature>(OnCanUseSkillEvent, std::bind(&Npc::OnCanUseSkill, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnActorDiedSignature>(OnDiedEvent, std::bind(&Npc::OnDied, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnEndUseSkillSignature>(OnEndUseSkillEvent, std::bind(&Npc::OnEndUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnStartUseSkillSignature>(OnStartUseSkillEvent, std::bind(&Npc::OnStartUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnClickedSignature>(OnClickedEvent, std::bind(&Npc::OnClicked, this, std::placeholders::_1));
    events_.Subscribe<OnCollideSignature>(OnCollideEvent, std::bind(&Npc::OnCollide, this, std::placeholders::_1));
    events_.Subscribe<OnSelectedSignature>(OnSelectedEvent, std::bind(&Npc::OnSelected, this, std::placeholders::_1));
    events_.Subscribe<OnTriggerSignature>(OnTriggerEvent, std::bind(&Npc::OnTrigger, this, std::placeholders::_1));
    events_.Subscribe<OnLeftAreaSignatre>(OnLeftAreaEvent, std::bind(&Npc::OnLeftArea, this, std::placeholders::_1));
    events_.Subscribe<VoidVoidSignature>(OnArrivedEvent, std::bind(&Npc::OnArrived, this));
    events_.Subscribe<VoidVoidSignature>(OnInterruptedAttackEvent, std::bind(&Npc::OnInterruptedAttack, this));
    events_.Subscribe<OnInterruptedSkillSignature>(OnInterruptedSkillEvent, std::bind(&Npc::OnInterruptedSkill, this, std::placeholders::_1));
    events_.Subscribe<OnKnockedDownSignature>(OnKnockedDownEvent, std::bind(&Npc::OnKnockedDown, this, std::placeholders::_1));
    events_.Subscribe<VoidIntSignature>(OnHealedEvent, std::bind(&Npc::OnHealed, this, std::placeholders::_1));
    events_.Subscribe<OnResurrectedSignature>(OnResurrectedEvent, std::bind(&Npc::OnResurrected, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnInteractSignature>(OnInteractEvent, std::bind(&Npc::OnInteract, this, std::placeholders::_1));
    // Party and Groups must be unique, i.e. share the same ID pool.
    groupComp_->SetGroupId(Group::GetNewId());
}

Npc::~Npc()
{
    auto* group = GetGroup();
    if (group)
        group->Remove(id_);
}

bool Npc::LoadScript(const std::string& fileName)
{
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
    {
        LOG_ERROR << "Script " << fileName << " not found" << std::endl;
        return false;
    }

    if (!lua_.Load(*script))
        return false;
    scriptFile_ = fileName;

    if (!lua_.IsString("name"))
    {
        LOG_ERROR << "Name field missing in " << fileName << std::endl;
        return false;
    }
    name_ = static_cast<const char*>(lua_["name"]);
    if (lua_.IsNumber("level"))
        level_ = lua_["level"];
    if (level_ > 1 && xp_ == 0)
        xp_ = (level_ - 1) * SKILLPOINT_ADVANCE_XP;

    if (lua_.IsNumber("itemIndex"))
        itemIndex_ = lua_["itemIndex"];
    if (lua_.IsNumber("sex"))
        sex_ = lua_["sex"];
    if (lua_.IsNumber("interactionRange"))
        interactionRange_ = static_cast<Range>(lua_["interactionRange"]);
    if (lua_.IsNumber("group_id"))
        groupComp_->SetGroupId(lua_["group_id"]);
    if (lua_.IsBool("wander"))
        SetWander(lua_["wander"]);
    if (lua_.IsBool("tameable"))
        tameable_ = lua_["tameable"];

    if (lua_.IsNumber("creatureState"))
        stateComp_.SetState(lua_["creatureState"], true);
    else
        stateComp_.SetState(AB::GameProtocol::CreatureState::Idle, true);

    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    if (lua_.IsNumber("prof1Index"))
    {
        skills_->prof1_.index = lua_["prof1Index"];
        if (skills_->prof1_.index != 0)
        {
            if (!client->Read(skills_->prof1_))
            {
                LOG_WARNING << "Unable to read primary profession of " << GetName() << ", index = " << skills_->prof1_.index << std::endl;
            }
        }
    }
    if (lua_.IsNumber("prof2Index"))
    {
        skills_->prof2_.index = lua_["prof2Index"];
        if (skills_->prof2_.index != 0)
        {
            if (!client->Read(skills_->prof2_))
            {
                LOG_WARNING << "Unable to read secondary profession of " << GetName() << ", index = " << skills_->prof2_.index << std::endl;
            }
        }
    }

    std::string bt;
    if (lua_.IsString("behavior"))
        bt = static_cast<const char*>(lua_["behavior"]);

    if (lua_.IsFunction("getSellingItemTypes"))
    {
        const std::vector<uint32_t> types = lua_["getSellingItemTypes"]();
        for (auto type : types)
            sellItemTypes_.emplace(static_cast<AB::Entities::ItemType>(type));
    }

    GetSkillBar().InitAttributes();

    if (!bt.empty())
        SetBehavior(bt);

    // If we are a slave also pass master/corpse to the init function
    auto master = master_.lock();
    auto corpse = corpse_.lock();
    bool result = lua_["onInit"](master.get(), corpse.get());
    if (result)
        Initialize();
    return result;
}

void Npc::SetLevel(uint32_t value)
{
    level_ = value;
    resourceComp_->UpdateResources();
}

void Npc::AddXp(int value)
{
    ASSERT(value >= 0);
    Actor::AddXp(value);
    xp_ += static_cast<uint32_t>(value);
}

void Npc::AddSkillPoint()
{
    Actor::AddSkillPoint();
    ++skillPoints_;
}

void Npc::AdvanceLevel()
{
    if (level_ < LEVEL_CAP)
        ++level_;
    Actor::AdvanceLevel();
}

CompanioInfo Npc::GetCompanionInfo() const
{
    return {
        .script = scriptFile_,
        .level = (int)GetLevel(),
        .xp = (int)GetXp(),
        .sp = (int)GetSkillPoints(),
        .name = GetName()
    };
}

void Npc::ObjectEnteredRange(Range range, uint32_t objectId)
{
    if (!lua_.IsFunction("onEnterRange"))
        return;
    auto game = GetGame();
    if (!game)
        return;
    auto* actor = game->GetObject<Actor>(objectId);
    if (!actor)
        return;
    lua_["onEnterRange"](range, actor);
}

void Npc::ObjectLeftRange(Range range, uint32_t objectId)
{
    if (!lua_.IsFunction("onLeaveRange"))
        return;
    auto game = GetGame();
    if (!game)
        return;
    auto* actor = game->GetObject<Actor>(objectId);
    if (!actor)
        return;
    lua_["onLeaveRange"](range, actor);
}

void Npc::OnMasterGettingDamage(Actor* source, Skill* skill, DamageType type, int32_t& value)
{
    if (lua_.IsFunction("onMasterGettingDamage"))
        value = lua_["onMasterGettingDamage"](source, skill, type, value);
}

void Npc::OnMasterAttackTarget(Actor* target, bool&)
{
    if (lua_.IsFunction("onMasterAttackTarget"))
        lua_["onMasterAttackTarget"](target);
}

void Npc::Update(uint32_t timeElapsed, MessageStream& message)
{
    // I think first we should run the BT
    if (aiComp_)
        aiComp_->Update(timeElapsed);
    if (wanderComp_)
        wanderComp_->Update(timeElapsed);

    Actor::Update(timeElapsed, message);

    lua_.Call("onUpdate", timeElapsed);
}

bool Npc::SetBehavior(const std::string& name)
{
    auto* cache = GetSubsystem<AI::BevaviorCache>();
    auto root = cache->Get(name);
    if (!root)
    {
        aiComp_.reset();
        if (!name.empty())
            LOG_WARNING << "Behavior with name " << name << " not found in cache" << std::endl;
        return false;
    }
    aiComp_ = ea::make_unique<Components::AiComp>(*this);
    aiComp_->GetAgent().SetBehavior(root);
    return true;
}

float Npc::GetAggro(const Actor* other)
{
    if (!other || !IsEnemy(other))
        return 0.0f;

    auto* random = GetSubsystem<Crypto::Random>();
    const float dist = 1.0f / (GetPosition().Distance(other->GetPosition()) / RANGE_AGGRO);
    const float health = 1.0f - other->resourceComp_->GetHealthRatio();
    const float ld = damageComp_->IsLastDamager(*other) ? 2.0f : 1.0f;
    const float rval = random->GetFloat();
    return ((dist + health) * ld) * rval;
}

bool Npc::GetSkillCandidates(
    ea::vector<int>& results,
    SkillEffect effect, SkillEffectTarget target,
    AB::Entities::SkillType interrupts /* = AB::Entities::SkillTypeAll */,
    const Actor* targetActor /* = nullptr */)
{
    // skill index -> cost (smaller is better)
    ea::map<int, float> sorting;
    skills_->VisitSkills([&](int index, const Skill& current)
    {
        if (!current.CanUseOnTarget(*this, targetActor))
            return Iteration::Continue;
        if (current.NeedsTarget())
        {
            // If there is a target check if it's in range
            if (targetActor && !IsInRange(current.GetRange(), targetActor))
                return Iteration::Continue;
        }
        if (!current.HasEffect(effect))
            return Iteration::Continue;
        if (target != SkillTargetNone && !current.HasTarget(target))
            return Iteration::Continue;
        if (!current.IsRecharged())
            return Iteration::Continue;
        if (!resourceComp_->HaveEnoughResources(&current))
            return Iteration::Continue;
        if (effect == SkillEffect::SkillEffectInterrupt &&
            interrupts != AB::Entities::SkillTypeAll &&
            !current.CanInterrupt(interrupts))
            // If this is an interrupt skill, and interrupts argument was passed, check also that
            return Iteration::Continue;

        results.push_back(index);
        // Calculate some score, depending on activation time, costs.... Smaller is better
        sorting[index] = current.CalculateCost([this, &current](CostType type)
        {
            switch (type)
            {
            case CostType::Activation:
                return 0.2f;
            case CostType::Recharge:
                return 0.0f;
            case CostType::Energy:
            {
                const float er = resourceComp_->GetEnergyRatio();
                return 1.0f - er;
            }
            case CostType::Adrenaline:
            {
                const int a = resourceComp_->GetAdrenaline();
                return static_cast<float>(a) / static_cast<float>(current.adrenaline_);
            }
            case CostType::HpSacrify:
            {
                const float hr = resourceComp_->GetHealthRatio();
                return 1.0f - hr;
            }
            }
            return 0.0f;
        });
        // Prefer Elite skills
        if (current.elite_)
            sorting[index] = sorting[index] * 0.5f;
        else
        {
            auto* rng = GetSubsystem<Crypto::Random>();
            sorting[index] = sorting[index] * rng->Get<float>(0.6f, 1.2f);
        }
        return Iteration::Continue;
    });

    if (results.empty())
        return false;

    ea::sort(results.begin(), results.end(), [&sorting](int i, int j)
    {
        return sorting[i] < sorting[j];
    });
    return true;
}

int Npc::GetBestSkillIndex(SkillEffect effect, SkillEffectTarget target,
    AB::Entities::SkillType interrupts /* = AB::Entities::SkillTypeAll */,
    const Actor* targetActor /* = nullptr */)
{
    ea::vector<int> skillIndices;
    if (GetSkillCandidates(skillIndices, effect, target, interrupts, targetActor))
        return *skillIndices.begin();
    return -1;
}

bool Npc::IsWander() const
{
    return !!wanderComp_;
}

void Npc::SetWander(bool value)
{
    if (!value)
    {
        if (wanderComp_)
            wanderComp_.reset();
        return;
    }

    if (!wanderComp_)
        wanderComp_ = ea::make_unique<Components::WanderComp>(*this);
}

uint32_t Npc::GetAttackSpeed()
{
    if (lua_.IsFunction("getAttackSpeed"))
        return static_cast<uint32_t>(lua_["getAttackSpeed"]());
    return Actor::GetAttackSpeed();
}

DamageType Npc::GetAttackDamageType()
{
    if (lua_.IsBool("getDamageType"))
        return static_cast<DamageType>(lua_["getDamageType"]());
    return Actor::GetAttackDamageType();
}

int32_t Npc::GetAttackDamage(bool critical)
{
    if (lua_.IsFunction("getAttackDamage"))
        return lua_["getAttackDamage"](critical);
    return Actor::GetAttackDamage(critical);
}

float Npc::GetArmorEffect(DamageType damageType, DamagePos pos, float penetration)
{
    if (lua_.IsFunction("getArmorEffect"))
        return lua_["getArmorEffect"](damageType, pos, penetration);
    return Actor::GetArmorEffect(damageType, pos, penetration);
}

int Npc::GetBaseArmor(DamageType damageType, DamagePos pos)
{
    if (lua_.IsFunction("getBaseArmor"))
        return lua_["getBaseArmor"](damageType, pos);
    // Armor base comes from the armor the actor wears, but NPCs do not wear armors.
    return static_cast<int>(GetLevel()) * 3;
}

void Npc::WriteSpawnData(MessageStream& msg)
{
    if (!serverOnly_)
        Actor::WriteSpawnData(msg);
}

void Npc::Say(ChatType channel, const std::string& message)
{
    switch (channel)
    {
    case ChatType::Map:
    {
        ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Map, static_cast<uint64_t>(GetGame()->id_));
        if (ch)
            ch->TalkNpc(*this, message);
        break;
    }
    case ChatType::Party:
    {
        ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Party, GetGroupId());
        if (ch)
            ch->TalkNpc(*this, message);
        break;
    }
    default:
        // N/A
        break;
    }
}

std::string Npc::GetQuote(int index)
{
    if (!lua_.IsFunction("onGetQuote"))
        return "";
    const char* q = static_cast<const char*>(lua_["onGetQuote"](index));
    return q;
}

void Npc::_LuaAddWanderPoint(const Math::StdVector3& point)
{
    if (IsWander())
        wanderComp_->AddRoutePoint(point);
}

void Npc::_LuaAddWanderPoints(const std::vector<Math::StdVector3>& points)
{
    for (const auto& point : points)
        _LuaAddWanderPoint(point);
}

void Npc::_LuaSetNpcType(int type)
{
    npcType_ = static_cast<AB::GameProtocol::NpcType>(type);
}

int Npc::_LuaGetNpcType() const
{
    return static_cast<int>(npcType_);
}

void Npc::_LuaSetCombatMode(int value)
{
    if (value < static_cast<int>(CombatMode::Fight) || value > static_cast<int>(CombatMode::AvoidCombat))
        return;
    combatMode_ = static_cast<CombatMode>(value);
}

std::string Npc::_LuaGetScriptFile() const
{
    return scriptFile_;
}

bool Npc::LoadCompationInfo(CompanioInfo& info)
{
    if (!lua_.IsTable("companion"))
        return false;
    kaguya::LuaTable res = lua_["companion"];
    if (res["script"].type() != LUA_TSTRING)
    {
        LOG_ERROR << "Script for companion is empty" << std::endl;
        return false;
    }
    info.script = (const char*)res["script"];
    if (res["level"].type() == LUA_TNUMBER)
        info.level = res["level"];
    else
        info.level = COMPANION_START_LEVEL;
    if (res["xp"].type() == LUA_TNUMBER)
        info.xp = res["xp"];
    else
        info.xp = (info.level - 1) * (int)SKILLPOINT_ADVANCE_XP;
    if (res["sp"].type() == LUA_TNUMBER)
        info.sp = res["sp"];
    else
        info.sp = 0;
    if (res["name"].type() == LUA_TSTRING)
        info.name = (const char*)res["name"];
    else
        info.name = "Companion";
    return true;
}

bool Npc::SayQuote(ChatType channel, int index)
{
    const std::string quote = GetQuote(index);
    if (quote.empty())
        return false;

    const std::string t = sa::templ::Parser::Evaluate(quote, [this](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "selected_name")
            {
                if (auto* sel = GetSelectedObject())
                    return sel->GetName();
                return "";
            }
            LOG_WARNING << "Not implemented placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    Say(channel, t);
    return true;
}

void Npc::Whisper(Player* player, const std::string& message)
{
    if (!player)
        return;

    ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Whisper, static_cast<uint64_t>(player->id_));
    if (ch)
        ch->TalkNpc(*this, message);
}

void Npc::ShootAt(const std::string& itemUuid, Actor* target)
{
    ASSERT(HasGame());
    GetGame()->AddProjectile(itemUuid, GetPtr<Actor>(), target->GetPtr<Actor>());
}

void Npc::OnSelected(Actor* selector)
{
    if (selector)
        lua_.Call("onSelected", selector);
}

void Npc::OnClicked(Actor* selector)
{
    if (selector)
        lua_.Call("onClicked", selector);
    if (Is<Player>(selector))
    {
        if (!IsInRange(Range::Adjecent, selector))
            return;
        // Get quests for this player
        auto& player = To<Player>(*selector);
        const auto quests = GetQuestsForPlayer(player);
        player.TriggerQuestSelectionDialog(id_, quests);
    }
}

void Npc::OnArrived()
{
    lua_.Call("onArrived");
}

void Npc::OnCollide(GameObject* other)
{
    if (other)
        lua_.Call("onCollide", other);
}

void Npc::OnTrigger(GameObject* other)
{
    if (lua_.IsFunction("onTrigger"))
        lua_.Call("onTrigger", other);
}

void Npc::OnLeftArea(GameObject* other)
{
    if (lua_.IsFunction("onLeftArea"))
        lua_.Call("onLeftArea", other);
}

void Npc::OnEndUseSkill(Actor* target, Skill* skill)
{
    lua_.Call("onEndUseSkill", target, skill);
}

void Npc::OnStartUseSkill(Actor* target, Skill* skill)
{
    lua_.Call("onStartUseSkill", target, skill);
}

void Npc::OnSourceAttackStart(Actor* target, bool& canAttack)
{
    lua_.Call("onAttack", target, canAttack);
}

void Npc::OnSourceAttackSuccess(Actor* target, DamageType type, int32_t damage)
{
    lua_.Call("onAttackSuccess", target, type, damage);
}

void Npc::OnTargetAttackEnd(Actor* source, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error)
{
    lua_.Call("onAttacked", source, type, damage, error);
}

void Npc::OnTargetAttackStart(Actor* source, bool& canGetAttacked)
{
    lua_.Call("onTargetAttackStart", source, canGetAttacked);
}

void Npc::OnCanUseSkill(Actor* target, Skill* skill, bool& success)
{
    lua_.Call("onCanUseSkill", target, skill, success);
}

void Npc::OnGettingSkillTarget(Actor* source, Skill* skill, bool& success)
{
    lua_.Call("onGettingSkillTarget", source, skill, success);
}

void Npc::OnInteract(Actor* actor)
{
    lua_.Call("onInteract", actor);
}

void Npc::OnActorDied(Actor* actor, Actor* killer)
{
    Actor::OnActorDied(actor, killer);
    lua_.Call("onActorDied", actor, killer);
}

void Npc::OnActorResurrected(Actor* actor)
{
    Actor::OnActorResurrected(actor);
    lua_.Call("onActorResurrected", actor);
}

void Npc::OnInterruptingAttack(bool& success)
{
    lua_.Call("onInterruptingAttack", success);
}

void Npc::OnInterruptingSkill(Actor* source, AB::Entities::SkillType type, Skill* skill, bool& success)
{
    lua_.Call("onInterruptingSkill", source, type, skill, success);
}

void Npc::OnInterruptedAttack()
{
    lua_.Call("onInterruptedAttack");
}

void Npc::OnInterruptedSkill(Skill* skill)
{
    lua_.Call("onInterruptedSkill", skill);
}

void Npc::OnKnockedDown(uint32_t time)
{
    lua_.Call("onKnockedDown", time);
}

void Npc::OnHealed(int hp)
{
    lua_.Call("onHealed", hp);
}

void Npc::OnDied(Actor*, Actor* killer)
{
    lua_.Call("onDied", killer);
}

void Npc::OnResurrected(int, int)
{
    lua_.Call("onResurrected");
}

void Npc::_LuaAddQuest(uint32_t index)
{
    quests_.emplace(index);
}

ea::set<uint32_t> Npc::GetQuestsForPlayer(const Player& player) const
{
    ea::set<uint32_t> result;
    const auto& qc = *player.questComp_;
    for (auto qIndex : quests_)
    {
        if (qc.IsAvailable(qIndex))
            result.emplace(qIndex);
    }
    return result;
}

bool Npc::HaveQuestsForPlayer(const Player& player) const
{
    const auto& qc = *player.questComp_;
    for (auto qIndex : quests_)
    {
        if (qc.IsAvailable(qIndex))
            return true;
    }
    return false;
}

bool Npc::IsSellingItemType(AB::Entities::ItemType type) const
{
    return sellItemTypes_.find(type) != sellItemTypes_.end();
}

bool Npc::IsSellingItem(uint32_t itemIndex)
{
    if (!lua_.IsFunction("isSellingItem"))
        return false;
    const bool result = lua_["isSellingItem"](itemIndex);
    return result;
}

}
