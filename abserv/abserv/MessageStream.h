/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <AB/Packets/Packet.h>
#include <libcommon/NetworkMessage.h>
#include <memory>
#include <CleanupNs.h>
#include <sa/Compiler.h>

namespace IO {
class GameWriteStream;
}

namespace Game {

class Game;
class Player;
class Party;

// A class to take care that the network message buffer does not overflow
class MessageStream
{
protected:
    std::unique_ptr<Net::NetworkMessage> currentMessage_;
public:
    static void InitMessageFilter();

    virtual ~MessageStream();

    template<typename T>
    void AddPacket(AB::GameProtocol::ServerPacketType type, T& packet)
    {
        const size_t size = AB::Packets::GetSize(packet);
        Net::NetworkMessage& message = GetMessage(size + sizeof(type));
        message.AddByte(type);
        AB::Packets::Add(packet, message);
    }
    template<typename T>
    void AddPacket(AB::GameProtocol::ServerPacketType type, T&& packet)
    {
        AddPacket<T>(type, packet);
    }

    virtual void Send() = 0;
    void Reset();
    Net::NetworkMessage& GetMessage(size_t size = 64);
};

class GameMessageStream final : public MessageStream
{
private:
    Game& owner_;
    std::unique_ptr<IO::GameWriteStream> writeStream_;
public:
    explicit GameMessageStream(Game& owner);
    void Send() override;
    bool SetRecordingDir(const std::string& dir);
    std::string GetRecordingFilename() const;
};

// Message stream for a single player. This class sends pending messages on deconstruction.
class PlayerMessageStream final : public MessageStream
{
private:
    Player& owner_;
public:
    explicit PlayerMessageStream(Player& owner);
    ~PlayerMessageStream() override;
    void Send() override;
};

class PartyMessageStream final : public MessageStream
{
private:
    Party& owner_;
public:
    explicit PartyMessageStream(Party& owner);
    ~PartyMessageStream() override;
    void Send() override;
};

}
