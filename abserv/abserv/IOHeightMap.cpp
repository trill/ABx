/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOHeightMap.h"
#include <fstream>
#include <sa/StringTempl.h>
#include <libio/Heightmap.h>

namespace IO {

bool IOHeightMap::Import(Game::HeightMap& asset, const std::string& name)
{
    asset.heightData_ = IO::LoadHeightmap(name, asset.header_);

#ifdef _DEBUG
    LOG_DEBUG << name << ": " << "nX=" << asset.header_.numVertices.x_ << " nY=" << asset.header_.numVertices.y_ <<
        " minHeight=" << asset.header_.minHeight <<
        " maxHeight=" << asset.header_.maxHeight << std::endl;
#endif

    return true;
}

}
