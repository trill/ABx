/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Model.h"
#include "Asset.h"
#include <libmath/Transformation.h>
#include <libmath/CollisionShape.h>
#include <libmath/BoundingBox.h>

namespace Game {

struct SceneObject
{
    enum class Occlude
    {
        Unset,
        Yes,
        No
    };
    std::string name;
    Math::Transformation transformation;
    Occlude occluder = Occlude::Unset;
    Occlude occludee = Occlude::Unset;
    Math::BoundingBox boundingBox;
    Math::Vector3 size = Math::Vector3_One;
    Math::Vector3 offset = Math::Vector3_Zero;
    Math::Quaternion offsetRot = Math::Quaternion_Identity;
    uint32_t collisionLayer = 1;
    uint32_t collisionMask = 0xFFFFFFFF;
    Math::ShapeType collsionShapeType = Math::ShapeType::None;
    ea::shared_ptr<Model> model;
};

struct SpawnPoint
{
    Math::Vector3 position;
    Math::Quaternion rotation;
    std::string group;
    bool operator==(const SpawnPoint& rhs) const
    {
        return (position == rhs.position && rotation == rhs.rotation);
    }
    inline bool Empty() const;
};

static const SpawnPoint EmtpySpawnPoint{ Math::Vector3_Zero, Math::Quaternion_Identity, "" };
inline bool SpawnPoint::Empty() const
{
    return *this == EmtpySpawnPoint;
}

// Scene.xml file. Just a container for static scene objects to make it cacheable.
class Scene final : public IO::Asset
{
public:
    Scene();
    ~Scene() override;
    Math::Vector3 terrainSpacing_;
    ea::vector<ea::unique_ptr<SceneObject>> objects_;
    ea::vector<SpawnPoint> spawnPoints_;
};

}
