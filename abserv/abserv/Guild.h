/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Guild.h>
#include <AB/Entities/GuildMembers.h>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Game {

class Guild
{
    NON_COPYABLE(Guild)
    NON_MOVEABLE(Guild)
public:
    explicit Guild(AB::Entities::Guild&& data);
    ~Guild() = default;

    size_t GetAccounts(ea::vector<std::string>& uuids) const;
    bool GetMembers(AB::Entities::GuildMembers& members) const;
    bool IsMember(const std::string& accountUuid) const;
    bool GetMember(const std::string& accountUuid, AB::Entities::GuildMember& member) const;

    template <typename Callback>
    void VisitAll(Callback&& callback) const
    {
        AB::Entities::GuildMembers members;
        if (!GetMembers(members))
            return;

        for (const auto& member : members.members)
            if (callback(member) != Iteration::Continue)
                break;
    }

    AB::Entities::Guild data_;
};

}
