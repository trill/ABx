/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Player.h"
#include <AB/Entities/Character.h>
#include <AB/Entities/FriendList.h>
#include <sa/Assert.h>

namespace IO {

bool LoadPlayerByUuid(Game::Player& player, const std::string& uuid);
bool SavePlayer(Game::Player& player);

}
