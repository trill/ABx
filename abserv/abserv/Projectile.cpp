/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Projectile.h"
#include "DataProvider.h"
#include "AttackComp.h"
#include "Game.h"
#include "Item.h"
#include "Script.h"
#include "MoveComp.h"
#include <sa/ScopeGuard.h>

//#define DEBUG_COLLISION

namespace Game {

void Projectile::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Projectile"].setClass(std::move(kaguya::UserdataMetatable<Projectile, Actor>()
        .addProperty("LifeTime", &Projectile::GetLifeTime, &Projectile::SetLifeTime)
        .addProperty("Source", &Projectile::_LuaGetSource)
        .addProperty("Target", &Projectile::_LuaGetTarget)
    ));
    // clang-format on
}

Projectile::Projectile(const std::string& itemUuid) :
    lua_(*this)
{
    events_.Subscribe<OnCollideSignature>(OnCollideEvent, std::bind(&Projectile::OnCollide, this, std::placeholders::_1));
    SetCollisionShape(
        ea::make_unique<Math::CollisionShape<Math::Sphere>>(Math::ShapeType::Sphere,
            Math::Vector3_Zero, PROJECTILE_SIZE)
    );
    // Projectile can not hide other objects
    SetOccluder(false);
    undestroyable_ = true;
    selectable_ = false;
    itemUuid_ = itemUuid;
}

Projectile::~Projectile() = default;

bool Projectile::LoadScript(const std::string& fileName)
{
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
        return false;
    if (!lua_.Load(*script))
        return false;

    bool ret = lua_["onInit"]();
    return ret;
}

bool Projectile::Load()
{
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Item gameItem;
    gameItem.uuid = itemUuid_;
    if (!client->Read(gameItem))
    {
        LOG_ERROR << "Unable to read item with UUID " << itemUuid_ << std::endl;
        return false;
    }
    itemIndex_ = gameItem.index;
    return LoadScript(gameItem.actorScript);
}

void Projectile::SetSource(ea::shared_ptr<Actor> source)
{
    if (!startSet_)
    {
        source_ = source;
        SetGroupId(source->GetGroupId());
        SetGroupColor(source->GetGroupColor());
        startPos_ = source->transformation_.position_ + HeadOffset;
        transformation_.position_ = startPos_;
        moveComp_->moved_ = true;
    }
}

void Projectile::SetTarget(ea::shared_ptr<Actor> target)
{
    // Can not change target
    if (started_)
        return;
    target_ = target;
    targetMoveDir_ = target->moveComp_->moveDir_;
    targetPos_ = target->transformation_.position_ + BodyOffset;
    moveComp_->HeadTo(targetPos_);
    moveComp_->directionSet_ = true;

    startDistance_ = minDistance_ = GetPosition().DistanceXZ(targetPos_);
    float dist = GetPosition().Distance(targetPos_);

    ea::vector<GameObject*> objects;
    const auto& origin = GetPosition();
    const auto direction = targetPos_ - origin;
    if (Raycast(objects, origin, direction, dist))
    {
        // The target is also in this list
        for (const auto* o : objects)
        {
            if (o->id_ != target->id_)
            {
#ifdef DEBUG_COLLISION
                LOG_DEBUG << "Obstructed by " << *o << std::endl;
#endif
                // FIXME: TerrainPatch ray cast doesn't work great
//                SetError(AB::GameProtocol::AttackError::TargetObstructed);
                break;
            }
        }
    }

    started_ = DoStart();
    if (started_)
    {
        stateComp_.SetState(AB::GameProtocol::CreatureState::Moving, true);
    }
}

void Projectile::SetError(AB::GameProtocol::AttackError error)
{
    error_ = error;
    if (auto source = source_.lock())
        source->attackComp_->SetAttackError(error);
}

Actor* Projectile::_LuaGetSource()
{
    auto o = GetSource();
    if (o)
        return o.get();
    return nullptr;
}

Actor* Projectile::_LuaGetTarget()
{
    auto o = GetTarget();
    if (o)
        return o.get();
    return nullptr;
}

void Projectile::Update(uint32_t timeElapsed, MessageStream& message)
{
    if (!started_)
        return;

    if (IsExpired())
    {
        Remove();
        return;
    }
    auto target = target_.lock();
    if (!target)
    {
        Remove();
        return;

    }

    if (target->moveComp_->IsMoving())
    {
        if (targetMoveDir_ == target->moveComp_->moveDir_)
        {
            // If the target does not change direction we follow him.
            // Target can only dodge when changing the direction after we launched.
            const float newDist = GetPosition().DistanceXZ(targetPos_);
            if (newDist > 2.0f)
            {
                targetPos_ = target->GetPosition() + BodyOffset;
                minDistance_ = newDist;
            }
        }
        else
        {
            SetError(AB::GameProtocol::AttackError::TargetDodge);
        }
    }

    moveComp_->StoreOldPosition();
    if (minDistance_ > 2.0f)
    {
        moveComp_->HeadTo(targetPos_);
        moveComp_->directionSet_ = true;
    }

    const float speed = moveComp_->GetSpeed(timeElapsed, BASE_MOVE_SPEED);
    moveComp_->Move(speed, Math::Vector3_UnitZ);
    // Adjust Y, also try to make a somewhat realistic trajectory
    const float f = 1.0f - (minDistance_ / startDistance_);
    transformation_.position_.y_ = Math::Lerp(startPos_.y_, targetPos_.y_, f) +
        (sinf(f * Math::M_PIF) * ((startDistance_ / 10.0f) / speed));

    Actor::Update(timeElapsed, message);

    if (item_)
        item_->Update(timeElapsed);

    const float dist = GetPosition().DistanceXZ(targetPos_);
    if (error_ == AB::GameProtocol::AttackError::TargetDodge)
    {
        Remove();
    }
    else if (dist < (speed * static_cast<float>(timeElapsed)) / 110.0f)
    {
        // We may not really collide because of the low game update rate, so let's
        // approximate if we would collide
        if (error_ == AB::GameProtocol::AttackError::None)
            CallEvent<OnCollideSignature>(OnCollideEvent, target.get());
        else
            Remove();
    }
    else if (dist < minDistance_)
        minDistance_ = dist;
    else if (dist > startDistance_)
    {
        if (error_ == AB::GameProtocol::AttackError::None)
            SetError(AB::GameProtocol::AttackError::TargetMissed);
        Remove();
    }
}

uint32_t Projectile::GetLevel() const
{
    if (auto s = source_.lock())
        return s->GetLevel();
    return 0u;
}

void Projectile::OnCollide(GameObject* other)
{
    lua_.Call("onCollide", other);

    sa::ScopeGuard removeGuard([this]()
    {
        // Always remove when it collides with anything
        Remove();
    });
    if (!other)
        return;
    if (error_ != AB::GameProtocol::AttackError::None)
        return;
    if (hitted_)
        return;

    if (auto spt = target_.lock())
    {
        if (other->id_ == spt->id_)
        {
            hitted_ = true;
            lua_.Call("onHitTarget", other);
        }
    }
}

bool Projectile::DoStart()
{
    auto t = target_.lock();
    ASSERT(t);
    bool ret = true;
    if (lua_.IsFunction("onStart"))
        ret = lua_["onStart"](t.get());
    if (ret)
        startTick_ = sa::time::tick();
    return true;
}

}
