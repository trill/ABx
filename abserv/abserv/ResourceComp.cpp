/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ResourceComp.h"
#include "Actor.h"
#include "AttackComp.h"
#include "DamageComp.h"
#include "Skill.h"
#include "SkillsComp.h"
#include "EffectsComp.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <AB/ProtocolCodes.h>
#include <sa/time.h>
#include <libshared/Attributes.h>
#include <libcommon/NetworkMessage.h>

namespace Game {
namespace Components {

ResourceComp::ResourceComp(Actor& owner) :
    owner_(owner)
{
    owner_.SubscribeEvent<VoidIntSignature>(OnXPAddedEvent, std::bind(&ResourceComp::OnXPAdded, this, std::placeholders::_1));
}

void ResourceComp::OnXPAdded(int value)
{
    if (morale_ >= 0)
        return;
    lastDecreaseDPXP_ += value;
    // For each 75 XP decrease DP by 1%
    int dpdecrease = lastDecreaseDPXP_ / 75;
    if (dpdecrease != 0)
    {
        ++morale_;
        dirtyFlags_ |= ResourceDirty::DirtyMorale;
        UpdateResources();
        lastDecreaseDPXP_ -= dpdecrease * 75;
    }
}

void ResourceComp::ActorDied(Actor* actor, Actor*)
{
    if (actor == &owner_)
        return;
    // Not getting energy for spirits
    if (actor->GetSpecies() == AB::GameProtocol::Species::Spirit)
        return;

    uint32_t sr = owner_.GetAttributeRank(Attribute::SoulReaping);
    if (sr != 0)
    {
        if (owner_.IsInRange(Range::HalfCompass, actor))
        {
            int energy = static_cast<int>(sr);
            owner_.effectsComp_->GetEnergyFromSoulReaping(actor, energy);
            AddEnergy(energy);
        }
    }
}

bool ResourceComp::IncreaseMorale()
{
    bool result{ false };
    if (morale_ < MAX_MORALE)
    {
        morale_ += 2;
        morale_ = Math::Clamp(morale_, MIN_MORALE, MAX_MORALE);
        dirtyFlags_ |= ResourceDirty::DirtyMorale;
        result = true;
    }
    if (result)
        UpdateResources();
    // We should fire this event even when we reached the limit
    owner_.CallEvent<VoidIntSignature>(OnIncMoraleEvent, morale_);
    return result;
}

bool ResourceComp::DecreaseMorale()
{
    bool result{ false };
    if (morale_ > MIN_MORALE)
    {
        morale_ -= 15;
        morale_ = Math::Clamp(morale_, MIN_MORALE, MAX_MORALE);
        dirtyFlags_ |= ResourceDirty::DirtyMorale;
        result = true;
    }
    if (result)
        UpdateResources();
    owner_.CallEvent<VoidIntSignature>(OnDecMoraleEvent, morale_);
    return result;
}

int ResourceComp::GetBaseHealth() const
{
    const unsigned levelAdvance = owner_.GetLevel() - 1;
    int hp = 100 + (static_cast<int>(levelAdvance) * 20);
    if (morale_ != 0)
        hp = GetPercent<int>(hp, 100 + morale_);
    return hp;
}

int ResourceComp::GetBaseEnergy() const
{
    int energy = BASE_ENERGY;
    if (morale_ != 0)
        energy = GetPercent<int>(energy, 100 + morale_);
    return energy;
}

void ResourceComp::UpdateResources()
{
    // Update max HP and max energy
    int hp = GetBaseHealth();
    int energy = GetBaseEnergy();

    const int oldHp = maxHealth_;
    const int oldE = maxEnergy_;

    GetResources(owner_, hp, energy);

    SetMaxHealth(hp);
    SetMaxEnergy(energy);
    if (maxHealth_ != oldHp)
        dirtyFlags_ |= ResourceDirty::DirtyMaxHealth;
    if (maxEnergy_ != oldE)
        dirtyFlags_ |= ResourceDirty::DirtyMaxEnergy;
}

void ResourceComp::SetHealth(SetValueType t, int value)
{
    const float oldVal = health_;
    if (SetValue(t, static_cast<float>(value), static_cast<float>(maxHealth_), health_))
    {
        dirtyFlags_ |= ResourceDirty::DirtyHealth;
        if (health_ < oldVal)
            lastHpDecrease_ = sa::time::tick();
    }
}

void ResourceComp::SetEnergy(SetValueType t, int value)
{
    if (SetValue(t, static_cast<float>(value), static_cast<float>(maxEnergy_), energy_))
        dirtyFlags_ |= ResourceDirty::DirtyEnergy;
}

void ResourceComp::SetAdrenaline(SetValueType t, int value)
{
    if (SetValue(t, static_cast<float>(value), 0.0f, adrenaline_))
        dirtyFlags_ |= ResourceDirty::DirtyAdrenaline;
}

void ResourceComp::SetOvercast(SetValueType t, int value)
{
    if (SetValue(t, static_cast<float>(value), static_cast<float>(maxEnergy_), overcast_))
        dirtyFlags_ |= ResourceDirty::DirtyOvercast;
}

void ResourceComp::SetHealthRegen(SetValueType t, int value)
{
    if (SetValue(t, static_cast<float>(value), static_cast<float>(MAX_HEALTH_REGEN), healthRegen_))
        dirtyFlags_ |= ResourceDirty::DirtyHealthRegen;
}

void ResourceComp::SetEnergyRegen(SetValueType t, int value)
{
    if (SetValue(t, static_cast<float>(value), MAX_ENERGY_REGEN, energyRegen_))
        dirtyFlags_ |= ResourceDirty::DirtyEnergyRegen;
}

void ResourceComp::SetMaxHealth(int value)
{
    if (maxHealth_ != value)
    {
        maxHealth_ = value;
        dirtyFlags_ |= ResourceDirty::DirtyMaxHealth;
    }
}

void ResourceComp::SetMaxEnergy(int value)
{
    if (maxEnergy_ != value)
    {
        maxEnergy_ = value;
        dirtyFlags_ |= ResourceDirty::DirtyMaxEnergy;
    }
}

int ResourceComp::GetValue(ResourceType type) const
{
    switch (type)
    {
    case ResourceType::Energy:
        return GetEnergy();
    case ResourceType::Health:
        return GetHealth();
    case ResourceType::Adrenaline:
        return GetAdrenaline();
    case ResourceType::Overcast:
        return GetOvercast();
    case ResourceType::HealthRegen:
        return GetHealthRegen();
    case ResourceType::EnergyRegen:
        return GetEnergyRegen();
    case ResourceType::MaxHealth:
        return GetMaxHealth();
    case ResourceType::MaxEnergy:
        return GetMaxEnergy();
    default:
        return 0;
    }
}

void ResourceComp::SetValue(ResourceType type, SetValueType t, int value)
{
    switch (type)
    {
    case ResourceType::None:
        break;
    case ResourceType::Energy:
        SetEnergy(t, value);
        break;
    case ResourceType::Health:
        SetHealth(t, value);
        break;
    case ResourceType::Adrenaline:
        SetAdrenaline(t, value);
        break;
    case ResourceType::Overcast:
        SetOvercast(t, value);
        break;
    case ResourceType::HealthRegen:
        SetHealthRegen(t, value);
        break;
    case ResourceType::EnergyRegen:
        SetEnergyRegen(t, value);
        break;
    case ResourceType::MaxHealth:
    {
        int hp = GetMaxHealth();
        switch (t)
        {
        case SetValueType::Absolute:
            hp = value;
            break;
        case SetValueType::Increase:
            hp += value;
            break;
        case SetValueType::Decrease:
            hp -= value;
            break;
        default:
            return;
        }
        SetMaxHealth(hp);
        break;
    }
    case ResourceType::MaxEnergy:
    {
        int e = GetMaxEnergy();
        switch (t)
        {
        case SetValueType::Absolute:
            e = value;
            break;
        case SetValueType::Increase:
            e += value;
            break;
        case SetValueType::Decrease:
            e -= value;
            break;
        default:
            return;
        }
        SetMaxEnergy(e);
        break;
    }
    }
}

int ResourceComp::AddEnergy(int value)
{
    const int curr = GetEnergy();
    SetEnergy(Components::SetValueType::Increase, value);
    return GetEnergy() - curr;
}

int ResourceComp::AddLife(int value)
{
    const int curr = GetHealth();
    SetHealth(Components::SetValueType::Increase, value);
    return GetHealth() - curr;
}

int ResourceComp::DrainEnergy(int value)
{
    const int curr = GetEnergy();
    const int result = Math::Clamp(value, 0, curr);
    SetEnergy(Components::SetValueType::Absolute, curr - result);
    return result;
}

bool ResourceComp::HaveEnoughResources(const Skill* skill) const
{
    if (!skill)
        return false;
    if (skill->adrenaline_ > GetAdrenaline())
        return false;
    if (skill->energy_ > GetEnergy())
        return false;
    if (skill->hp_ > GetHealth())
        return false;
    return true;
}

void ResourceComp::UpdateRegen(uint32_t /* timeElapsed */)
{
    int oldHR = effectiveHealthRegen_;
    int oldER = effectiveEnergyRegen_;
    if (owner_.IsDead())
    {
        effectiveEnergyRegen_ = 0;
        effectiveHealthRegen_ = 0;
    }
    else
    {
        effectiveEnergyRegen_ = static_cast<int>(energyRegen_);
        effectiveHealthRegen_ = static_cast<int>(healthRegen_);
        GetRegeneration(owner_, effectiveHealthRegen_, effectiveEnergyRegen_);
    }
    if (oldHR != effectiveHealthRegen_)
        dirtyFlags_ |= ResourceDirty::DirtyHealthRegen;
    if (oldER != effectiveEnergyRegen_)
        dirtyFlags_ |= ResourceDirty::DirtyEnergyRegen;

    // When the actor didn't get damage or lost HP for 5 seconds, increase natural HP regen by 1 every 2 seconds.
    // Maximum regen through natural regen is 7.
    // https://wiki.guildwars.com/wiki/Health
    if (!owner_.IsDead() && static_cast<int>(health_) < maxHealth_)
    {
        // Time last HP was reduced
        uint32_t last = std::min(owner_.damageComp_->NoDamageTime(),
            GetLastHpDecrease());
        // Not using skills
        last = std::min(sa::time::time_elapsed(owner_.skillsComp_->GetLastSkillTime()), last);
        // Not attacking
        last = std::min(sa::time::time_elapsed(owner_.attackComp_->GetLastAttackTime()), last);
        if (last > 5000 && effectiveHealthRegen_ >= 0)
        {
            if ((sa::time::time_elapsed(lastRegenIncrease_) > 2000) && GetOverallHealthRegen() < 7)
            {
                ++naturalHealthRegen_;
                lastRegenIncrease_ = sa::time::tick();
                dirtyFlags_ |= ResourceDirty::DirtyHealthRegen;
            }
            return;
        }
    }

    // Either we lost HP or HP regen is smaller 0 (Hex or something) -> no natural regen
    if (naturalHealthRegen_ != 0)
    {
        naturalHealthRegen_ = 0;
        dirtyFlags_ |= ResourceDirty::DirtyHealthRegen;
    }
}

uint32_t ResourceComp::GetLastHpDecrease() const
{
    return sa::time::time_elapsed(lastHpDecrease_);
}

void ResourceComp::Update(uint32_t timeElapsed)
{
    if (owner_.undestroyable_ || owner_.IsDead())
        return;

    UpdateRegen(timeElapsed);
    // 2 regen per sec
    const float sec = static_cast<float>(timeElapsed) / 1000.0f;
    // Jeder Pfeil erhöht oder senkt die Lebenspunkte um genau zwei pro Sekunde.
    float hpVal = (static_cast<float>(GetOverallHealthRegen()) * 2.0f) * sec;
    if (SetValue(SetValueType::Increase, hpVal, static_cast<float>(maxHealth_), health_))
    {
        dirtyFlags_ |= ResourceDirty::DirtyHealth;
        if (hpVal < 0.0f)
            lastHpDecrease_ = sa::time::tick();
    }
    // Also bedeutet 1 Pfeil eine Regeneration (oder Degeneration) von 0,33 Energiepunkten pro Sekunde.
    if (SetValue(SetValueType::Increase, (static_cast<float>(GetEffectiveEnergyRegen()) * 0.33f) * sec, static_cast<float>(std::max(0, maxEnergy_ - (int)overcast_)), energy_))
        dirtyFlags_ |= ResourceDirty::DirtyEnergy;
    // Überzaubert wird alle drei Sekunden um einen Punkt abgebaut
    if (overcast_ > 0.0f)
    {
        if (SetValue(SetValueType::Decrease, (1.0f / 3.0f) * sec, static_cast<float>(maxEnergy_), overcast_))
            dirtyFlags_ |= ResourceDirty::DirtyOvercast;
    }
    else if (overcast_ < 0.0f)
        overcast_ = 0.0f;

    if (health_ <= 0.0f && !owner_.IsDead())
        owner_.Die(nullptr);
    if (energy_ <= 0.0f && !owner_.IsDead())
        owner_.CallEvent<OnEnergyZeroSignature>(OnEnergyZeroEvent);
}

void ResourceComp::Write(MessageStream& message, bool ignoreDirty /* = false */)
{
    if (!ignoreDirty && dirtyFlags_ == 0)
        return;

    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyHealth) == ResourceDirty::DirtyHealth)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::Health),
            static_cast<int16_t>(health_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyEnergy) == ResourceDirty::DirtyEnergy)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::Energy),
            static_cast<int16_t>(energy_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyAdrenaline) == ResourceDirty::DirtyAdrenaline)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::Adrenaline),
            static_cast<int16_t>(adrenaline_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyOvercast) == ResourceDirty::DirtyOvercast)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::Overcast),
            static_cast<int16_t>(overcast_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyHealthRegen) == ResourceDirty::DirtyHealthRegen)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::HealthRegen),
            static_cast<int16_t>(GetOverallHealthRegen())
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyEnergyRegen) == ResourceDirty::DirtyEnergyRegen)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::EnergyRegen),
            static_cast<int16_t>(GetEffectiveEnergyRegen())
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyMaxHealth) == ResourceDirty::DirtyMaxHealth)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::MaxHealth),
            static_cast<int16_t>(maxHealth_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyMaxEnergy) == ResourceDirty::DirtyMaxEnergy)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::MaxEnergy),
            static_cast<int16_t>(maxEnergy_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }
    if (ignoreDirty || (dirtyFlags_ & ResourceDirty::DirtyMorale) == ResourceDirty::DirtyMorale)
    {
        AB::Packets::Server::ObjectResourceChanged packet = {
            owner_.id_,
            static_cast<uint8_t>(AB::GameProtocol::ResourceType::Morale),
            static_cast<int16_t>(morale_)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectResourceChanged, packet);
    }

    if (!ignoreDirty)
        dirtyFlags_ = 0;
}

}
}
