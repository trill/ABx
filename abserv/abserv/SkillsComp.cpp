/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SkillsComp.h"
#include "Actor.h"
#include "Skill.h"
#include "SkillBar.h"
#include "Game.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <sa/time.h>
#include <libcommon/NetworkMessage.h>

//#define DEBUG_AI

namespace Game {
namespace Components {

#ifdef DEBUG_AI
static std::string GetSkillErrorString(AB::GameProtocol::SkillError error)
{
    switch (error)
    {
#define ENUMERATE_SKILL_ERROR_CODE(v) case AB::GameProtocol::SkillError::v: return #v;
        ENUMERATE_SKILL_ERROR_CODES
#undef ENUMERATE_SKILL_ERROR_CODE
    }
    return "(Unknown)";
}
#endif

SkillsComp::SkillsComp(Actor& owner) :
    owner_(owner),
    lastError_(AB::GameProtocol::SkillError::None),
    lastSkillIndex_(-1),
    startDirty_(false),
    endDirty_(false),
    usingSkill_(false),
    newRecharge_(0),
    lastSkillTime_(0)
{
    owner_.SubscribeEvent<VoidIntSignature>(OnIncMoraleEvent, std::bind(&SkillsComp::OnIncMorale, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnInterruptedSkillSignature>(OnInterruptedSkillEvent, std::bind(&SkillsComp::OnSkillInterrupted, this, std::placeholders::_1));
    owner_.SubscribeEvent<OnSkillRechargedSignature>(OnSkillRechargedEvent, std::bind(&SkillsComp::OnSkillRecharged, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<VoidVoidSignature>(OnCancelAllEvent, std::bind(&SkillsComp::OnCancelAll, this));
    owner_.SubscribeEvent<OnAttackSuccessSignature>(OnTargetAttackSuccessEvent, std::bind(&SkillsComp::OnTargetAttackSuccess, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void SkillsComp::OnIncMorale(int) const
{
    owner_.skills_->VisitSkills([](int, Skill& current)
    {
        current.SetRecharged(0);
        return Iteration::Continue;
    });
}

void SkillsComp::OnCancelAll()
{
    Cancel();
}

void SkillsComp::OnSkillInterrupted(Skill* skill)
{
    if (!skill)
        return;
    if (auto ls = lastSkill_.lock())
    {
        if (skill->skillIndex_ == ls->skillIndex_)
        {
            lastInterrupted_ = true;
            interruptedTick_ = sa::time::tick();
        }
    }
}

void SkillsComp::OnSkillRecharged(int pos, Skill* skill)
{
    ASSERT(skill);
    ASSERT(pos >= 0 && pos < PLAYER_MAX_SKILLS);
    rechargedSkills_ |= 1 << pos;
}

void SkillsComp::OnTargetAttackSuccess(Actor*, DamageType, int32_t)
{
    if (auto ls = lastSkill_.lock())
    {
        if (ls->IsUsing() && ls->easyInterruptable_)
            ls->Interrupt();
    }
}

void SkillsComp::Update(uint32_t timeElapsed)
{
    SkillBar& sb = owner_.GetSkillBar();
    sb.Update(timeElapsed);
    if (!usingSkill_)
        return;

    if (auto ls = lastSkill_.lock())
    {
        if (!ls->IsUsing())
        {
            usingSkill_ = false;
            endDirty_ = true;
            lastError_ = ls->GetLastError();
            newRecharge_ = ls->recharge_;
        }
    }
}

AB::GameProtocol::SkillError SkillsComp::UseSkill(int index, bool ping)
{
    if (index == -1)
    {
        lastError_ = AB::GameProtocol::SkillError::InvalidSkill;
        return lastError_;
    }
    SkillBar& sb = owner_.GetSkillBar();
    auto skill = sb.GetSkill(index);
    if (!skill || skill->skillIndex_ == 0)
    {
        lastError_ = AB::GameProtocol::SkillError::InvalidSkill;
        return lastError_;
    }
    ea::shared_ptr<Actor> target;
    if (auto selObj = owner_.GetSelectedObject())
        target = selObj->GetPtr<Actor>();

    // Can use skills only on Creatures not all GameObjects.
    // But a target is not mandatory, the Skill script will decide
    // if it needs a target, and may fail.
    lastError_ = sb.UseSkill(index, target);
    usingSkill_ = lastError_ == AB::GameProtocol::SkillError::None;
    lastSkillIndex_ = index;
    lastSkillTime_ = sa::time::tick();
    lastInterrupted_ = false;
    lastSkill_ = skill;
    startDirty_ = true;
    endDirty_ = false;
    if (usingSkill_ && ping)
    {
        uint32_t targetId = 0;
        if (skill->HasTarget(SkillTargetTarget))
            targetId = target ? target->id_ : 0;
        owner_.CallEvent<OnPingObjectSignature>(OnPingObjectEvent,
            targetId, AB::GameProtocol::ObjectCallType::UseSkill, lastSkillIndex_ + 1);
    }
#ifdef DEBUG_AI
    if (lastError_ != AB::GameProtocol::SkillError::None)
    {
        LOG_DEBUG << owner_ << " using invalid skill " <<
            (skill ? skill->data_.name : "(none)") <<
            " on target ";
        if (target)
            LOG_DEBUG << (*target);
        else
            LOG_DEBUG << "(none)";
        LOG_DEBUG << " error (" <<
            static_cast<int>(lastError_) << ") " << GetSkillErrorString(lastError_) << std::endl;
    }
#endif
    return lastError_;
}

void SkillsComp::Cancel()
{
    if (!usingSkill_)
        return;

    if (auto ls = lastSkill_.lock())
    {
        if (ls->IsUsing())
        {
            usingSkill_ = false;
            endDirty_ = true;
            newRecharge_ = 0;
            ls->CancelUse();
            lastError_ = AB::GameProtocol::SkillError::Cancelled;
        }
    }
}

void SkillsComp::CancelWhenChangingState()
{
    if (!usingSkill_)
        return;

    if (auto ls = lastSkill_.lock())
    {
        if (ls->IsUsing() && ls->IsChangingState())
        {
            usingSkill_ = false;
            endDirty_ = true;
            newRecharge_ = 0;
            ls->CancelUse();
            lastError_ = AB::GameProtocol::SkillError::Cancelled;
        }
    }
}

bool SkillsComp::IsUsing()
{
    if (auto ls = lastSkill_.lock())
        return ls->IsUsing();
    return false;
}

void SkillsComp::Write(MessageStream& message)
{
    // The server has 0 based skill indices but for the client these are 1 based
    if (startDirty_)
    {
        if (lastError_ == AB::GameProtocol::SkillError::None)
        {
            AB::Packets::Server::ObjectUseSkill packet = {};
            packet.id = owner_.id_;
            packet.skillIndex = static_cast<int8_t>(lastSkillIndex_ + 1);
            if (auto ls = lastSkill_.lock())
            {
                packet.energy = static_cast<uint16_t>(ls->GetEffectiveEnergy());
                packet.adrenaline = static_cast<uint16_t>(ls->GetEffectiveAdrenaline());
                packet.activation = static_cast<uint16_t>(ls->GetEffectiveActivation());
                packet.overcast = static_cast<uint16_t>(ls->GetEffectiveOvercast());
                packet.hp = static_cast<uint16_t>(ls->GetEffectiveHp());
            }
            else
            {
                packet.energy = 0;
                packet.adrenaline = 0;
                packet.activation = 0;
                packet.overcast = 0;
                packet.hp = 0;
            }
            message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectUseSkill, packet);
        }
        else
        {
            message.AddPacket<AB::Packets::Server::ObjectSkillFailure>(
                AB::GameProtocol::ServerPacketType::ObjectSkillFailure,
                { owner_.id_, static_cast<int8_t>(lastSkillIndex_ + 1), static_cast<uint8_t>(lastError_) });
        }
        startDirty_ = false;
    }

    if (endDirty_)
    {
        if (lastError_ == AB::GameProtocol::SkillError::None)
        {
            if (!lastInterrupted_)
            {
                message.AddPacket<AB::Packets::Server::ObjectSkillSuccess>(
                    AB::GameProtocol::ServerPacketType::ObjectSkillSuccess,
                    { owner_.id_, static_cast<int8_t>(lastSkillIndex_ + 1), newRecharge_ });
            }
            else
            {
                message.AddPacket<AB::Packets::Server::ObjectSkillInterrupted>(
                    AB::GameProtocol::ServerPacketType::ObjectSkillInterrupted,
                    { owner_.id_, static_cast<int8_t>(lastSkillIndex_ + 1), newRecharge_ });
            }
        }
        else
        {
            message.AddPacket<AB::Packets::Server::ObjectSkillFailure>(
                AB::GameProtocol::ServerPacketType::ObjectSkillFailure,
                { owner_.id_, static_cast<int8_t>(lastSkillIndex_ + 1), static_cast<uint8_t>(lastError_) });
        }
        endDirty_ = false;
    }

    if (rechargedSkills_ != 0)
    {
        message.AddPacket<AB::Packets::Server::ObjectSkillsRecharged>(
            AB::GameProtocol::ServerPacketType::ObjectSkillsRecharged,
            { owner_.id_, rechargedSkills_ });
        rechargedSkills_ = 0;
    }
}

bool SkillsComp::Interrupt(AB::Entities::SkillType type)
{
    if (auto ls = lastSkill_.lock())
    {
        if (ls->IsUsing() && ls->IsType(type))
            return ls->Interrupt();
    }
    return false;
}

Skill* SkillsComp::GetCurrentSkill()
{
    if (auto ls = lastSkill_.lock())
    {
        if (ls->IsUsing())
            return ls.get();
    }
    return nullptr;
}

void SkillsComp::GetResources(int&, int& maxEnergy)
{
    uint32_t estorage = owner_.GetAttributeRank(Attribute::EnergyStorage);
    maxEnergy += (int)(estorage * 3u);
}

void SkillsComp::GetSkillRecharge(Skill* skill, uint32_t& recharge)
{
    if (!skill->IsType(AB::Entities::SkillTypeSpell))
        return;
    // Recharge time of all Spells is decreased by the Fastcast attribute in PvE and PvP
    const uint32_t fastcast = owner_.GetAttributeRank(Attribute::FastCast);
    if (fastcast == 0)
        return;
    float reduce = static_cast<float>(recharge) / 100.0f * (3.0f * static_cast<float>(fastcast));
    if (reduce < recharge)
        recharge -= static_cast<uint32_t>(reduce);
    else
        recharge = 0;
}

void SkillsComp::GetSkillCost(Skill* skill,
    int32_t& activation, int32_t& energy, int32_t&, int32_t&, int32_t&)
{
    // Expertise reduces energy cost of ranger skills
    const uint32_t expertise = owner_.GetAttributeRank(Attribute::Expertise);
    if (expertise != 0)
    {
        // Must be a ranger
        if (Utils::Uuid::IsEqual(skill->professionUuid_, owner_.skills_->prof1_.uuid))
        {
            // Seems to be a ranger skill
            float reduce = static_cast<float>(energy) / 100.0f * (4.0f * static_cast<float>(expertise));
            energy -= static_cast<int32_t>(reduce);
            if (energy < 0)
                energy = 0;
        }
    }

    const uint32_t fastcast = owner_.GetAttributeRank(Attribute::FastCast);
    if (fastcast != 0)
    {
        if (skill->IsType(AB::Entities::SkillTypeSignet))
            activation = static_cast<int32_t>(static_cast<float>(activation) * (1.0f - (0.03f * static_cast<float>(fastcast))));
        else if (skill->IsType(AB::Entities::SkillTypeSpell))
            activation = static_cast<int32_t>(static_cast<float>(activation) * powf(0.5f, static_cast<float>(fastcast) / 15.0f));
    }
}

}
}
