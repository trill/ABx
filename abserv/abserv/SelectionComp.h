/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <memory>
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class GameObject;
class Actor;
class MessageStream;

namespace Components {

class SelectionComp
{
    NON_COPYABLE(SelectionComp)
    NON_MOVEABLE(SelectionComp)
private:
    Actor& owner_;
    uint32_t prevObjectId_{ 0 };
    uint32_t currObjectId_{ 0 };
public:
    SelectionComp() = delete;
    explicit SelectionComp(Actor& owner) :
        owner_(owner)
    { }
    ~SelectionComp();

    bool SelectObject(uint32_t targetId);
    bool ClickObject(uint32_t targetId);

    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);
    uint32_t GetSelectedObjectId() const { return currObjectId_; }
    GameObject* GetSelectedObject() const;
};

}
}
