/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/Variant.h>
#include <stdint.h>
#include <AB/Entities/Quest.h>
#include <AB/Entities/PlayerQuest.h>
#include <sa/Bits.h>
#include "LuaObject.h"
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Player;
class Actor;
class Game;
class MessageStream;

class Quest
{
    NON_COPYABLE(Quest)
    NON_MOVEABLE(Quest)
private:
    LuaObject lua_;
    Utils::VariantMap variables_;
    Player& owner_;
    uint32_t index_;
    bool repeatable_;
    bool internalRewarded_{ false };
    bool internalDeleted_{ false };
    std::string _LuaGetVarString(const std::string& name) const;
    void _LuaSetVarString(const std::string& name, const std::string& value);
    float _LuaGetVarNumber(const std::string& name) const;
    void _LuaSetVarNumber(const std::string& name, float value);
    Player* _LuaGetOwner();
    void LoadProgress();
    /// A foe was killed nearby
    void OnKilledFoe(Actor* foe, Actor* killer);
    void OnPostSpawn(Game* game);
public:
    static void RegisterLua(kaguya::State& state);

    Quest(Player& owner,
        const AB::Entities::Quest& q,
        AB::Entities::PlayerQuest&& playerQuest);
    ~Quest();

    bool LoadScript(const std::string& fileName);

    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);

    const Utils::Variant& GetVar(const std::string& name) const;
    void SetVar(const std::string& name, const Utils::Variant& val);

    bool IsCompleted() const { return playerQuest_.completed; }
    bool IsRewarded() const { return playerQuest_.rewarded; }
    bool IsRepeatable() const { return repeatable_; }
    void SaveProgress();
    bool CollectReward();
    bool Delete();
    bool IsActive() const;
    uint32_t GetIndex() const { return index_; }

    AB::Entities::PlayerQuest playerQuest_;
};

}
