/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SkillManager.h"
#include "DataProvider.h"
#include <AB/Entities/Skill.h>
#include <libcommon/DataClient.h>
#include <libcommon/Subsystems.h>
#include "Skill.h"

namespace Game {

SkillManager::SkillManager() = default;

ea::shared_ptr<Skill> SkillManager::Get(uint32_t index)
{
    if (index == 0)
        return ea::shared_ptr<Skill>();

    ea::shared_ptr<Skill> result;
    auto it = skillCache_.find(index);
    if (it != skillCache_.end())
    {
        result = ea::make_shared<Skill>((*it).second);
    }
    else
    {
        IO::DataClient* client = GetSubsystem<IO::DataClient>();
        AB::Entities::Skill skill;
        skill.index = index;
        if (!client->Read(skill))
        {
            LOG_ERROR << "Error reading skill with index " << index << std::endl;
            return ea::shared_ptr<Skill>();
        }
        result = ea::make_shared<Skill>(skill);
        // Move to cache
        skillCache_.emplace(index, skill);
    }

    if (result)
    {
        if (result->LoadScript(result->data_.script))
            return result;
    }

    return ea::shared_ptr<Skill>();
}

}
