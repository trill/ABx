/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiLoader.h"
#include "DataProvider.h"
#include <libcommon/Subsystems.h>
#include "Script.h"

namespace AI {

static void LuaErrorHandler(int errCode, const char* message)
{
    LOG_ERROR << "Lua Error (" << errCode << "): " << message << std::endl;
}

AiLoader::~AiLoader() = default;

bool AiLoader::ExecuteScript(kaguya::State& state, const std::string& file)
{
    // The AI library does not set an error handler
    state.setErrorHandler(LuaErrorHandler);
    auto* dp = GetSubsystem<IO::DataProvider>();
    auto script = dp->GetAsset<Game::Script>(file);
    if (!script)
        return false;
    return script->Execute(state);
}

void AiLoader::LoadError(const std::string& message)
{
    LOG_ERROR << message << std::endl;
}

}
