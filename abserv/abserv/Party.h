/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Group.h"
#include <AB/Entities/Party.h>
#include <libcommon/DataClient.h>
#include <libcommon/NetworkMessage.h>
#include <libcommon/Subsystems.h>
#include <libcommon/Variant.h>
#include <libshared/Mechanic.h>
#include <kaguya/kaguya.hpp>
#include <sa/IdGenerator.h>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Game {

class PartyChatChannel;
class Player;

class Party final : public Group, public ea::enable_shared_from_this<Party>
{
    NON_COPYABLE(Party)
private:
    /// Used when forming a group. If the player accepts it is added to the members.
    ea::vector<ea::weak_ptr<Player>> invited_;
    ea::shared_ptr<PartyChatChannel> chatChannel_;
    /// Depends on the map
    uint32_t maxMembers_{ 1 };
    size_t resignedCount_{ 0 };
    Utils::VariantMap variables_;
    template<typename E>
    bool UpdateEntity(const E& e)
    {
        IO::DataClient* cli = GetSubsystem<IO::DataClient>();
        return cli->Update(e);
    }
    /// 1-base position
    size_t GetDataPos(const Player& player);
    std::string _LuaGetVarString(const std::string& name) const;
    void _LuaSetVarString(const std::string& name, const std::string& value);
    float _LuaGetVarNumber(const std::string& name) const;
    void _LuaSetVarNumber(const std::string& name, float value);
public:
    static void RegisterLua(kaguya::State& state);

    Party();
    ~Party();

    /// Append player
    bool AddPlayer(ea::shared_ptr<Player> player);
    /// Insert at the position in data_
    bool SetPlayer(ea::shared_ptr<Player> player);
    bool RemovePlayer(Player& player, bool newParty = true);
    bool Invite(ea::shared_ptr<Player> player);
    bool RemoveInvite(ea::shared_ptr<Player> player);
    /// Clear all invites
    void ClearInvites();
    const ea::vector<ea::weak_ptr<Actor>>& GetMembers() const { return members_; }
    Player* GetRandomPlayer() const;
    Player* GetRandomPlayerInRange(const Actor* actor, Range range) const;

    void Update(uint32_t timeElapsed) override;
    void WriteToMembers(const Net::NetworkMessage& message);

    // Valid + not yet connected member count
    size_t GetAllMemberCount() const { return data_.members.size(); }
    void SetPartySize(size_t size);
    inline size_t GetValidPlayerCount() const;
    void VisitPlayers(const std::function<Iteration(Player& current)>& callback) const;
    bool IsFull() const { return static_cast<uint32_t>(members_.size()) >= maxMembers_; }
    bool IsMember(const Player& player) const;
    bool IsInvited(const Player& player) const;
    ea::shared_ptr<Player> GetAnyPlayer() const;
    bool IsResigned() const override;
    /// Bring all players back to their last outpost
    void TeleportBack();
    std::string GetName() const override;

    /// Get position of actor in party, 1-based, 0 = not found
    size_t GetPosition(const Actor* actor) const;
    /// Tells all members to change the instance. The client will disconnect and reconnect to enter the instance.
    void ChangeInstance(const std::string& mapUuid);
    void ChangeServerInstance(const std::string& serverUuid, const std::string& mapUuid, const std::string& instanceUuid);
    void NotifyPlayersQueued();
    void NotifyPlayersUnqueued();

    const Utils::Variant& GetVar(const std::string& name) const;
    void SetVar(const std::string& name, const Utils::Variant& val);

    AB::Entities::Party data_;
    /// Get the ID of the game the members are in
    uint32_t gameId_{ 0 };
};

}
