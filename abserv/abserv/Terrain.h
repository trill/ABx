/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Asset.h"
#include <libmath/Vector3.h>
#include <libmath/HeightMap.h>
#include <libmath/Transformation.h>
#include <libmath/Point.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Game {

inline constexpr int DEFAULT_PATCH_SIZE = 32;
inline constexpr int MIN_PATCH_SIZE = 4;
inline constexpr int MAX_PATCH_SIZE = 128;

class HeightMap final : public IO::Asset, public Math::HeightMap
{
private:
    void OnChanged() override;
};

class Terrain final : public IO::Asset
{
    NON_COPYABLE(Terrain)
    NON_MOVEABLE(Terrain)
private:
    // Must be shared_ptr CollisionShape may also own it
    ea::shared_ptr<HeightMap> heightMap_;
    ea::shared_ptr<HeightMap> heightMap2_;
    mutable bool matrixDirty_{ true };
public:
    Terrain();
    ~Terrain() override;

    void SetHeightMap(ea::shared_ptr<HeightMap> val)
    {
        heightMap_ = std::move(val);
    }
    void SetHeightMap2(ea::shared_ptr<HeightMap> val)
    {
        heightMap2_ = std::move(val);
    }
    const Math::HeightMap* GetHeightMap() const
    {
        if (heightMap_)
            return heightMap_.get();
        return nullptr;
    }
    const Math::HeightMap* GetHeightMap2() const
    {
        if (heightMap2_)
            return heightMap2_.get();
        return nullptr;
    }
    void Initialize();
    void SetSpacing(const Math::Vector3& value);
    float GetHeight(const Math::Vector3& world) const;
    float GetHeight1(const Math::Vector3& world) const;
    float GetHeight2(const Math::Vector3& world) const;

    Math::Transformation transformation_;
    int patchSize_{ DEFAULT_PATCH_SIZE };
    Math::Point<int> numPatches_;
};

}
