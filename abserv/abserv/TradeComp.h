/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Noncopyable.h>
#include <memory>
#include <AB/ProtocolCodes.h>
#include <functional>
#include <sa/Iteration.h>
#include <eastl.hpp>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Player;
class Item;

namespace Components {

class TradeComp
{
    NON_COPYABLE(TradeComp)
    NON_MOVEABLE(TradeComp)
public:
    enum class TradeError
    {
        None,
        TargetInvalid,
        TargetTrading,
        TargetQueing,
        InProgress,
    };
private:
    enum class TradeState
    {
        Idle,
        MoveToTarget,
        Trading,
        Offered,
    };
    Player& owner_;
    ea::weak_ptr<Player> target_;
    TradeState state_{ TradeState::Idle };
    bool accepted_{ false };
    std::vector<std::pair<uint16_t, uint32_t>> ourOffer_;
    uint32_t ourOfferedMoney_{ 0 };
    void MoveToTarget(Player* target);
    bool CheckRange();
    void StartTrading();
    void OnStuck();
    void OnStateChange(AB::GameProtocol::CreatureState oldState, AB::GameProtocol::CreatureState newState);
    TradeError TestTarget(const Player& target);
    void TradeReqeust(Player& source);
public:
    static void WriteError(TradeError error, Net::NetworkMessage& message);

    TradeComp() = delete;
    explicit TradeComp(Player& owner);
    ~TradeComp() = default;
    void Update(uint32_t timeElapsed);

    TradeError TradeWith(ea::shared_ptr<Player> target);
    void Reset();
    // One player requested to cancel the trading
    void Cancel();

    bool IsTrading() const { return state_ >= TradeState::Trading; }
    uint32_t GetTradePartnerId() const;
    void Offer(uint32_t money, std::vector<std::pair<uint16_t, uint32_t>>&& items);
    void Accept();
    bool IsAccepted() const { return accepted_; }
    void VisitOfferedItems(const std::function<Iteration(Item&, uint32_t)>& callback);
    size_t GetOfferedItemCount() const { return ourOffer_.size(); }
    uint32_t GetOfferedMoney() const { return ourOfferedMoney_; }
};

}
}
