/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/Vector3.h>
#include <AB/ProtocolCodes.h>
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class MessageStream;

namespace Components {

class MoveComp
{
    NON_COPYABLE(MoveComp)
    NON_MOVEABLE(MoveComp)
private:
    Actor& owner_;
    float speedFactor_{ 1.0f };
    Math::Vector3 oldPosition_;
    Math::Vector3 safePosition_;
    float lastSpeed_{ 0.0f };
    /// Move to moveDir_
    void UpdateMove(uint32_t timeElapsed);
    /// Turn to turnDir_
    void UpdateTurn(uint32_t timeElapsed);
    void StoreSafePosition();
    void OnCancelAll();
public:
    enum UpdateFlags : uint32_t
    {
        UpdateFlagMove = 1,
        UpdateFlagTurn = 1 << 1
    };
    MoveComp() = delete;
    explicit MoveComp(Actor& owner);
    ~MoveComp() = default;

    float GetSpeed(uint32_t timeElapsed, float baseSpeed);
    void Update(uint32_t timeElapsed, uint32_t flags);
    bool SetPosition(const Math::Vector3& pos);
    void StickToGround();
    bool CanStepOn(Math::Vector3* nearestPoint) const;
    void HeadTo(const Math::Vector3& pos);
    /// Move in direction of rotation
    void Move(float speed, const Math::Vector3& amount);
    void Turn(float angle);
    void SetDirection(float worldAngle);
    void Cancel();
    float GetSpeedFactor() const
    {
        return speedFactor_;
    }
    void SetSpeedFactor(float value)
    {
        if (Math::Equals(speedFactor_, value))
            return;

        speedFactor_ = value;
    }
    float GetEffectiveSpeedFactor() const;
    void AddSpeed(float value)
    {
        if (Math::Equals(value, 0.0f))
            return;

        speedFactor_ += value;
    }

    const Math::Vector3& GetOldPosition() const
    {
        return oldPosition_;
    }
    const Math::Vector3& GetSafePosition() const
    {
        return safePosition_;
    }
    bool IsMoving() const { return !velocity_.Equals(Math::Vector3_Zero); }
    void Write(MessageStream& message);
    void StoreOldPosition();
    Math::Vector3& CalculateVelocity(uint32_t timeElapsed);

    uint32_t moveDir_{ AB::GameProtocol::MoveDirectionNone };
    uint32_t turnDir_{ AB::GameProtocol::TurnDirectionNone };
    bool turned_{ false };
    bool moved_{ false };
    bool speedDirty_{ false };
    /// Manual direction set
    bool directionSet_{ false };
    bool newAngle_{ false };
    /// Sends a special message to the client to force the client to set the position.
    bool forcePosition_{ false };
    bool autoMove_{ false };
    /// Velocity in Units/s.
    Math::Vector3 velocity_;
    // If turned off, it doesn't check whether or not you can step on a poly of the nav mesh.
    bool checkStepOn_{ true };
};

}
}
