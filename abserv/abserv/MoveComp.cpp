/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MoveComp.h"
#include "Actor.h"
#include "AutoRunComp.h"
#include "CollisionComp.h"
#include "EffectsComp.h"
#include "Game.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>

namespace Game {
namespace Components {

MoveComp::MoveComp(Actor& owner) :
    owner_(owner)
{
    owner_.SubscribeEvent<VoidVoidSignature>(OnCancelAllEvent, std::bind(&MoveComp::OnCancelAll, this));
}

void MoveComp::OnCancelAll()
{
    Cancel();
}

float MoveComp::GetSpeed(uint32_t timeElapsed, float baseSpeed)
{
    return (static_cast<float>(timeElapsed) / baseSpeed) * GetEffectiveSpeedFactor();
}

void MoveComp::Update(uint32_t timeElapsed, uint32_t flags)
{
    if (owner_.stateComp_.GetState() != AB::GameProtocol::CreatureState::Moving)
        return;
    if (flags == 0 && !autoMove_)
        return;

    forcePosition_ = false;
    // Lets turn first
    if ((flags & UpdateFlagTurn) == UpdateFlagTurn)
        UpdateTurn(timeElapsed);
    if (autoMove_ || ((flags & UpdateFlagMove) == UpdateFlagMove))
    {
        StoreOldPosition();
        UpdateMove(timeElapsed);
        CalculateVelocity(timeElapsed);
        if (!velocity_.Equals(Math::Vector3_Zero))
            moved_ = true;
    }
}

bool MoveComp::SetPosition(const Math::Vector3& pos)
{
    StoreOldPosition();

    HeadTo(pos);
    owner_.transformation_.position_ = pos;

    if (owner_.GetType() != AB::GameProtocol::GameObjectType::Projectile)
    {
        // Keep on ground
        StickToGround();
    }
    if (owner_.collisionComp_)
        // We need to do it here because this is not called from Update()
        owner_.collisionComp_->ResolveCollisions();

    const bool moved = oldPosition_ != owner_.transformation_.position_;

    if (moved && owner_.GetOctant())
    {
        // We need to do it here because this is not called from Update()
        Math::Octree* octree = owner_.GetOctant()->GetRoot();
        octree->AddObjectUpdate(&owner_);
    }

    if (moved)
        moved_ = true;

    return moved;
}

void MoveComp::StickToGround()
{
    owner_.GetGame()->map_->UpdatePointHeight(owner_.transformation_.position_);
}

void MoveComp::HeadTo(const Math::Vector3& pos)
{
    const float worldAngle = -owner_.transformation_.position_.AngleY(pos) - Math::M_PIF;
    SetDirection(worldAngle);
}

bool MoveComp::CanStepOn(Math::Vector3* nearestPoint) const
{
    if (!checkStepOn_)
        return true;
    const auto& map = *owner_.GetGame()->map_;
    return map.CanStepOn(owner_.transformation_.position_, Math::Vector3_One, nullptr, nullptr, nearestPoint);
}

void MoveComp::Move(float speed, const Math::Vector3& amount)
{
/*
    // The client prediction calculates the position in the following way:
    const Math::Matrix4 m = Math::Matrix4::FromQuaternion(owner_.transformation_.oriention_.Inverse());
    const Math::Vector3 a = amount * speed;
    const Math::Vector3 v = m * a;
    Math::Vector3 oldPos = owner_.transformation_.position_;
    oldPos += v;
    LOG_INFO << "seepd " << speed << " amount " << amount << " v " << v << " new pos " << oldPos << std::endl;
*/

    owner_.transformation_.Move(speed, amount);

    if (owner_.GetType() != AB::GameProtocol::GameObjectType::Projectile)
    {
        Math::Vector3 nearestPoint = oldPosition_;
        auto& map = *owner_.GetGame()->map_;
        if (owner_.autorunComp_->IsAutoRun() || CanStepOn(&nearestPoint))
        {
            // Keep on ground, except projectiles, they usually fly...
            map.UpdatePointHeight(owner_.transformation_.position_);
        }
        else
        {
            map.UpdatePointHeight(nearestPoint);
            if (!owner_.transformation_.position_.Equals(nearestPoint))
            {
                owner_.transformation_.position_ = nearestPoint;
                forcePosition_ = true;
            }
        }
    }

    const bool moved = !oldPosition_.Equals(owner_.transformation_.position_);
    if (moved)
        moved_ = true;
}

void MoveComp::UpdateMove(uint32_t timeElapsed)
{
    if (moveDir_ == 0)
        return;

    StoreSafePosition();

    const float speed = GetSpeed(timeElapsed, BASE_MOVE_SPEED);
    if (!Math::Equals(speed, lastSpeed_))
    {
        speedDirty_ = true;
        lastSpeed_ = speed;
    }
    if ((moveDir_ & AB::GameProtocol::MoveDirectionNorth) == AB::GameProtocol::MoveDirectionNorth)
        Move(speed, Math::Vector3_UnitZ);
    if ((moveDir_ & AB::GameProtocol::MoveDirectionSouth) == AB::GameProtocol::MoveDirectionSouth)
        // Move slower backward
        Move(speed * MOVE_BACK_FACTOR, Math::Vector3_Back);
    if ((moveDir_ & AB::GameProtocol::MoveDirectionWest) == AB::GameProtocol::MoveDirectionWest)
        Move(speed * MOVE_BACK_FACTOR, Math::Vector3_Left);
    if ((moveDir_ & AB::GameProtocol::MoveDirectionEast) == AB::GameProtocol::MoveDirectionEast)
        Move(speed * MOVE_BACK_FACTOR, Math::Vector3_UnitX);
}

void MoveComp::Turn(float angle)
{
    owner_.transformation_.Turn(angle);
}

void MoveComp::UpdateTurn(uint32_t timeElapsed)
{
    const float speed = GetSpeed(timeElapsed, BASE_TURN_SPEED);
    if ((turnDir_ & AB::GameProtocol::TurnDirectionLeft) == AB::GameProtocol::TurnDirectionLeft)
    {
        Turn(speed);
        turned_ = true;
    }
    if ((turnDir_ & AB::GameProtocol::TurnDirectionRight) == AB::GameProtocol::TurnDirectionRight)
    {
        Turn(-speed);
        turned_ = true;
    }
}

void MoveComp::SetDirection(float worldAngle)
{
    const float ang = owner_.transformation_.GetYRotation();
    Math::NormalizeAngle(worldAngle);
    if (!Math::Equals(ang, worldAngle, 0.00001f))
    {
        owner_.transformation_.SetYRotation(worldAngle);
        directionSet_ = true;
    }
}

void MoveComp::Cancel()
{
    if (moveDir_ != AB::GameProtocol::MoveDirectionNone)
    {
        moveDir_ = AB::GameProtocol::MoveDirectionNone;
    }
    if (turnDir_ != AB::GameProtocol::TurnDirectionNone)
    {
        turnDir_ = AB::GameProtocol::TurnDirectionNone;
    }
}

float MoveComp::GetEffectiveSpeedFactor() const
{
    float result = GetSpeedFactor();
    owner_.effectsComp_->GetSpeedFactor(result);
    return result;
}

void MoveComp::Write(MessageStream& message)
{
    if (speedDirty_)
    {
        speedDirty_ = false;
        AB::Packets::Server::ObjectSpeedChanged packet = {
            owner_.id_,
            GetEffectiveSpeedFactor()
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectSpeedChanged, packet);
    }

    if (moved_ || forcePosition_)
    {
        AB::GameProtocol::ServerPacketType type = forcePosition_ ? AB::GameProtocol::ServerPacketType::ObjectForcePosition : AB::GameProtocol::ServerPacketType::ObjectPositionUpdate;
        AB::Packets::Server::ObjectPositionUpdate packet = {
            owner_.id_,
            {
                owner_.transformation_.position_.x_,
                owner_.transformation_.position_.y_,
                owner_.transformation_.position_.z_
            }
        };
        message.AddPacket(type, packet);
        moved_ = false;
        forcePosition_ = false;
    }

    // The rotation may change in 2 ways: Turn and SetWorldDirection
    if (turned_ || directionSet_)
    {
        AB::Packets::Server::ObjectRotationUpdate packet = {
            owner_.id_,
            owner_.transformation_.GetYRotation(),
            directionSet_
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectRotationUpdate, packet);
        turned_ = false;
        directionSet_ = false;
    }
}

void MoveComp::StoreOldPosition()
{
    oldPosition_ = owner_.transformation_.position_;
}

void MoveComp::StoreSafePosition()
{
    safePosition_ = owner_.transformation_.position_;
}

Math::Vector3& MoveComp::CalculateVelocity(uint32_t timeElapsed)
{
    velocity_ = ((oldPosition_ - owner_.transformation_.position_) / (static_cast<float>(timeElapsed) / 1000.0f));
    return velocity_;
}

}
}
