/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/MailList.h>
#include <AB/Entities/Mail.h>
#include <AB/Entities/ConcreteItem.h>

namespace Game {

class MailBox
{
private:
    std::string accountUuid_;
    AB::Entities::MailList mailList_;
public:
    static bool ReadMail(const std::string& uuid, AB::Entities::Mail& mail);
    static bool DeleteMail(const std::string& uuid, AB::Entities::Mail& mail);
    // Returns the ID for the attached item, and removes it as mail attachment.
    static uint32_t TakeMailAttachment(const std::string& uuid, AB::Entities::ItemPos pos);

    explicit MailBox(std::string accountUuid);
    ~MailBox();

    void Update();
    int GetTotalMailCount() const
    {
        return static_cast<int>(mailList_.mails.size());
    }
    const AB::Entities::MailList& GetMails() const
    {
        return mailList_;
    }
    void DeleteAll();
};

}
