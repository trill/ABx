/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/Variant.h>
#include <libcommon/SimpleConfigManager.h>

class ConfigManager : public IO::SimpleConfigManager
{
public:
    enum Key : size_t
    {
        Machine,
        ServerName,
        ServerID,
        Location,
        GamePort,
        GameHost,
        GameIP,
        ServerKeys,

        LogDir,
        DataDir,
        RecordingsDir,
        RecordGames,

        DataServerHost,
        DataServerPort,
        MessageServerHost,
        MessageServerPort,

        MaxPacketsPerSecond,

        Behaviours,
        AiServer,
        AiServerIp,
        AiServerPort,
        AiUpdateInterval,
        WatchAssets,
    };
public:
    ConfigManager();
    ~ConfigManager() = default;

    Utils::Variant& operator[](Key key)
    {
        return config_[key];
    }

    bool Load(const std::string& file);

    Utils::VariantMap config_;
};

