/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <libshared/Damage.h>
#include <AB/ProtocolCodes.h>
#include <sa/Noncopyable.h>
#include <eastl.hpp>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class MessageStream;

namespace Components {

class AttackComp
{
    NON_COPYABLE(AttackComp)
    NON_MOVEABLE(AttackComp)
private:
    Actor& owner_;
    /// Also includes running to the target
    bool attacking_{ false };
    bool hitting_{ false };
    bool pause_{ false };
    int64_t lastAttackTime_{ 0 };
    uint32_t attackSpeed_{ 0 };
    uint32_t lastAttackSpeed_{ 0 };
    bool attackSpeedDirty_{ false };
    DamageType damageType_{ DamageType::Unknown };
    AB::GameProtocol::AttackError lastError_{ AB::GameProtocol::AttackError::None };
    bool interrupted_{ false };
    ea::weak_ptr<Actor> target_;
    bool CheckRange();
    void StartHit(Actor& target);
    void Hit(Actor& target);
    void FireWeapon(Actor& target);
    void MoveToTarget(ea::shared_ptr<Actor> target);
    void OnCancelAll();
public:
    AttackComp(Actor& owner);
    ~AttackComp() = default;

    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);
    bool IsHitting() const { return hitting_; }
    void Cancel();
    bool Attack(ea::shared_ptr<Actor> target, bool ping);
    int64_t GetLastAttackTime() const { return lastAttackTime_; }
    bool IsAttackState() const;
    void SetAttackState(bool value);
    bool IsAttackingTarget(const Actor* target) const;
    bool Interrupt();
    void Pause(bool value = true);
    bool IsTarget(const Actor* target) const;
    Actor* GetCurrentTarget() const
    {
        if (auto t = target_.lock())
            return t.get();
        return nullptr;
    }
    void SetAttackError(AB::GameProtocol::AttackError error) { lastError_ = error; }
    AB::GameProtocol::AttackError GetAttackError() const { return lastError_; }
};

}
}
