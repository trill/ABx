/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/LuaLoader.h>

namespace AI {

class AiLoader final : public LuaLoader
{
protected:
    bool ExecuteScript(kaguya::State& state, const std::string& file) override;
    void LoadError(const std::string& message) override;
public:
    explicit AiLoader(Registry& reg) :
        LuaLoader(reg)
    { }
    ~AiLoader() override;
};

}
