/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Actor.h"
#include "AreaOfEffect.h"
#include "Projectile.h"
#include "CollisionComp.h"
#include "ConfigManager.h"
#include "Game.h"
#include "GameObject.h"
#include "Model.h"
#include "Npc.h"
#include "Player.h"
#include "TriggerComp.h"
#include <libcommon/Dispatcher.h>
#include <sa/Assert.h>
#include <sa/time.h>
#include <sa/EnumIterator.h>

//#define DEBUG_COLLISION

namespace Game {

sa::IdGenerator<uint32_t> GameObject::objectIds_;

void GameObject::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["GameObject"].setClass(std::move(kaguya::UserdataMetatable<GameObject>()
        .addFunction("QueryObjects",     &GameObject::_LuaQueryObjects)
        .addFunction("Raycast",          &GameObject::_LuaRaycast)
        .addFunction("RaycastTo",        &GameObject::_LuaRaycastTo)
        .addFunction("RaycastToObject",  &GameObject::_LuaRaycastToObject)
        .addFunction("SetBoundingBox",   &GameObject::_LuaSetBoundingBox)
        .addFunction("SetBoundingSize",  &GameObject::_LuaSetBoundingSize)
        .addFunction("GetVarString",     &GameObject::_LuaGetVarString)
        .addFunction("SetVarString",     &GameObject::_LuaSetVarString)
        .addFunction("GetVarNumber",     &GameObject::_LuaGetVarNumber)
        .addFunction("SetVarNumber",     &GameObject::_LuaSetVarNumber)
        .addFunction("SetScaleSimple",   &GameObject::_LuaSetScaleSimple)
        .addFunction("GetDistance",      &GameObject::GetDistance)
        // Can return empty if cast is not possible
        .addFunction("AsActor",          &GameObject::_LuaAsActor)
        .addFunction("AsNpc",            &GameObject::_LuaAsNpc)
        .addFunction("AsPlayer",         &GameObject::_LuaAsPlayer)
        .addFunction("AsAOE",            &GameObject::_LuaAsAOE)
        .addFunction("AsProjectile",     &GameObject::_LuaAsProjectile)

        .addFunction("GetActorsInRange", &GameObject::_LuaGetActorsInRange)
        .addFunction("GetClosestActor",  &GameObject::_LuaGetClosestActor)
        .addFunction("IsInRange",        &GameObject::IsInRange)
        .addFunction("IsCloserThan",     &GameObject::IsCloserThan)
        .addFunction("IsObjectInSight",  &GameObject::_LuaIsObjectInSight)
        .addFunction("GetObjectsInside", &GameObject::_LuaGetObjectsInside)
        .addFunction("CallGameEvent",    &GameObject::_LuaCallGameEvent)
        .addFunction("Remove",           &GameObject::Remove)
        .addFunction("RemoveIn",         &GameObject::RemoveIn)

        .addProperty("CollisionMask", &GameObject::_LuaGetCollisionMask, &GameObject::_LuaSetCollisionMask)
        .addProperty("CollisionLayer", &GameObject::_LuaGetCollisionLayer, &GameObject::_LuaSetCollisionLayer)
        .addProperty("Game", &GameObject::_LuaGetGame)
        .addProperty("Id", &GameObject::GetId)
        .addProperty("Name", &GameObject::GetName, &GameObject::SetName)
        .addProperty("Position", &GameObject::_LuaGetPosition, &GameObject::_LuaSetPosition)
        .addProperty("Rotation", &GameObject::_LuaGetRotation, &GameObject::_LuaSetRotation)
        .addProperty("Scale", &GameObject::_LuaGetScale, &GameObject::_LuaSetScale)
        .addProperty("Selectable", &GameObject::IsSelectable, &GameObject::SetSelectable)
        .addProperty("State", &GameObject::_LuaGetState, &GameObject::_LuaSetState)
        .addProperty("Trigger", &GameObject::IsTrigger, &GameObject::SetTrigger)
    ));
    // clang-format on
}

GameObject::GameObject() :
    name_("Unknown"),
    id_(GetNewId()),
    stateComp_(*this)
{ }

GameObject::~GameObject()
{
    RemoveFromOctree();
}

void GameObject::UpdateRanges()
{
    // Most objects are static objects or terrain patches, which do not have any logic,
    // so we can skip this, which is very expensive. It was always on top of profiles.
    if (GetType() <= AB::GameProtocol::GameObjectType::__SentToPlayer)
        return;

    const RangesObjects oldRanges(ranges_);
    ranges_.Clear();
    RangesObjects enteredRanges;

    auto addToRange = [&](Range range, const GameObject* object)
    {
        ranges_.Add(range, object->id_);

        if (!oldRanges.Contains(range, object->id_))
            enteredRanges.Add(range, object->id_);
    };

    ea::vector<Math::OctreeObject*> res;
    const SentToPlayerMatcher matcher;
    // Compass radius
    if (QueryObjects(res, RANGE_INTEREST, &matcher))
    {
        sa::EnumIterator<Range, Range::Touch, Range::Map> rangeIterator;

        const Math::Vector3& myPos = GetPosition();
        for (const auto* oo : res)
        {
            const auto* o = static_cast<const GameObject*>(oo);
            const Math::Vector3& objectPos = o->GetPosition();
            const float dist = myPos.Distance(objectPos) - AVERAGE_BB_EXTENDS;

            for (Range r : rangeIterator)
            {
                if (dist <= RangeDistances[static_cast<size_t>(r)])
                    addToRange(r, o);
            }
        }
    }

    // First leave the range
    oldRanges.VisitRanges([this](Range range, const auto& objects)
    {
        for (auto o : objects)
        {
            if (!ranges_.Contains(range, o))
                ObjectLeftRange(range, o);
        }
        return Iteration::Continue;
    });
    // Then enter a new range
    enteredRanges.VisitRanges([this](Range range, const auto& objects)
    {
        for (auto o : objects)
        {
            ObjectEnteredRange(range, o);
        }
        return Iteration::Continue;
    });
}

void GameObject::Update(uint32_t timeElapsed, MessageStream&)
{
    UpdateRanges();
    if (triggerComp_)
        triggerComp_->Update(timeElapsed);

    if (removeAt_ != 0 && removeAt_ <= sa::time::tick())
    {
        auto* disp = GetSubsystem<Asynch::Dispatcher>();
        disp->Add(Asynch::CreateTask(std::bind(&GameObject::Remove, shared_from_this())));
    }
}

void GameObject::SetModel(ea::shared_ptr<Model> model)
{
    model_ = std::move(model);
}

bool GameObject::Collides(const Math::OctreeObject* other, const Math::Vector3& velocity, Math::Vector3& move) const
{
    if (!collisionShape_ || !other || !static_cast<const GameObject*>(other)->GetCollisionShape())
        return false;

    bool result = false;
    Math::OctreeObject* o = const_cast<Math::OctreeObject*>(other);
    Collides(&o, 1, velocity, [&](GameObject&, const Math::Vector3& _move, bool&) -> Iteration {
        result = true;
        move = _move;
        return Iteration::Break;
    });
    return result;
}

void GameObject::Collides(Math::OctreeObject** others, size_t count, const Math::Vector3& velocity,
    const std::function<Iteration(GameObject& other, const Math::Vector3& move, bool& updateTrans)>& callback) const
{
    ea::unique_ptr<Math::AbstractCollisionShape> myTransformedShape = collisionShape_->GetTranformedShapePtr(transformation_.GetMatrix());

    bool transformationUpdated = false;

    for (size_t i = 0; i < count; ++i)
    {
        GameObject* other = static_cast<GameObject*>(*others);
        others++;

        if (other == this || !other->collisionShape_)
            continue;

        if (transformationUpdated)
        {
            myTransformedShape = collisionShape_->GetTranformedShapePtr(transformation_.GetMatrix());
            transformationUpdated = false;
        }

        Math::Vector3 move;
        if (myTransformedShape->Collides(*other->GetCollisionShape(), other->transformation_.GetMatrix(), velocity, move))
        {
#ifdef DEBUG_COLLISION
            LOG_DEBUG << "this(" << *this <<
                ") " << transformation_.position_ << " " <<
                "collides with that(" << *other << ") " <<
                std::endl;
#endif
            if (callback(*other, move, transformationUpdated) != Iteration::Continue)
                break;
        }
    }
}

const Utils::Variant& GameObject::GetVar(const std::string& name) const
{
    const auto it = variables_.find(sa::StringHash(name));
    if (it != variables_.end())
        return (*it).second;
    return Utils::Variant::Empty;
}

void GameObject::SetVar(const std::string& name, const Utils::Variant& val)
{
    variables_[sa::StringHash(name)] = val;
}

void GameObject::ProcessRayQuery(const Math::RayOctreeQuery& query, ea::vector<Math::RayQueryResult>& results)
{
    float distance = query.ray_.HitDistance(GetWorldBoundingBox());
    if (distance < query.maxDistance_)
    {
        Math::RayQueryResult result;
        result.position_ = query.ray_.origin_ + distance * query.ray_.direction_;
        result.normal_ = -query.ray_.direction_;
        result.distance_ = distance;
        result.object_ = this;
        results.push_back(std::move(result));
    }
}

bool GameObject::QueryObjects(ea::vector<Math::OctreeObject*>& result, float radius, const Math::OctreeMatcher* matcher)
{
    if (!GetOctant())
        return false;

    Math::Sphere sphere(transformation_.position_, radius);
    Math::SphereOctreeQuery query(result, sphere, this, matcher);
    Math::Octree* octree = GetOctant()->GetRoot();
    octree->GetObjects(query);
    return true;
}

bool GameObject::QueryObjects(ea::vector<Math::OctreeObject*>& result, const Math::BoundingBox& box, const Math::OctreeMatcher* matcher)
{
    if (!GetOctant())
        return false;

    Math::BoxOctreeQuery query(result, box, this, matcher);
    Math::Octree* octree = GetOctant()->GetRoot();
    octree->GetObjects(query);
    return true;
}

bool GameObject::Raycast(ea::vector<GameObject*>& result,
    const Math::Vector3& direction,
    float maxDist /* = Math::M_INFINITE */, const Math::OctreeMatcher* matcher /* = nullptr */) const
{
    return Raycast(result, transformation_.position_ + HeadOffset, direction, maxDist, matcher);
}

bool GameObject::Raycast(ea::vector<GameObject*>& result,
    const Math::Vector3& position, const Math::Vector3& direction,
    float maxDist /* = Math::M_INFINITE */, const Math::OctreeMatcher* matcher /* = nullptr */) const
{
    if (!GetOctant())
        return false;

    ea::vector<Math::RayQueryResult> res;
    if (!RaycastWithResult(res, position, direction, maxDist, matcher))
        return false;

    for (const auto& o : res)
        result.push_back(static_cast<GameObject*>(o.object_));
    return true;
}

bool GameObject::RaycastWithResult(ea::vector<Math::RayQueryResult>& result,
    const Math::Vector3& position, const Math::Vector3& direction,
    float maxDist /* = Math::M_INFINITE */, const Math::OctreeMatcher* matcher /* = nullptr */) const
{
    if (!GetOctant())
        return false;

    const Math::Ray ray(position, direction);
    Math::RayOctreeQuery query(result, ray, maxDist, this, matcher);
    Math::Octree* octree = GetOctant()->GetRoot();
    octree->Raycast(query);
    return true;
}

bool GameObject::IsObjectInSight(const GameObject& object) const
{
    const Math::CollisionMaskOctreeMatcher matcher(GetCollisionMask());
    ea::vector<GameObject*> result;
    const bool res = Raycast(result, object.transformation_.position_ + BodyOffset, Math::M_INFINITE, &matcher);
    if (!res)
        // Shouldn't happen
        return false;

    for (const auto* o : result)
    {
        if (!o->IsOccluder())
            continue;

        // result is sorted by distance
        if (o->id_ != object.id_)
        {
#ifdef DEBUG_COLLISION
            LOG_DEBUG << "Obstructed by " << *o << std::endl;
#endif
            return false;
        }
        // Can stop here it doesn't matter whats behind the target.
        return true;
    }
    return true;
}

void GameObject::Remove()
{
    if (auto game = GetGame())
        game->RemoveObject(this);
}

void GameObject::RemoveIn(uint32_t time)
{
    const int64_t newTick = sa::time::tick() + time;
    if (removeAt_ == 0 || newTick < removeAt_)
        removeAt_ = newTick;
}

void GameObject::SetState(AB::GameProtocol::CreatureState state)
{
    stateComp_.SetState(state, false);
}

std::vector<GameObject*> GameObject::_LuaQueryObjects(float radius)
{
    ea::vector<Math::OctreeObject*> res;
    std::vector<GameObject*> luares;
    if (QueryObjects(res, radius))
    {
        for (auto* oo : res)
            luares.push_back(static_cast<GameObject*>(oo));
        return luares;
    }
    return {};
}

void GameObject::_LuaCallGameEvent(const std::string& name, GameObject* data)
{
    if (auto game = GetGame())
        game->CallLuaEvent(name, this, data);
}

Actor* GameObject::_LuaGetClosestActor(bool undestroyable, bool unselectable) const
{
    return GetClosestActor(undestroyable, unselectable);
}

std::vector<GameObject*> GameObject::_LuaGetObjectsInside() const
{
    std::vector<GameObject*> result;
    if (!triggerComp_)
        return result;

    ASSERT(HasGame());
    auto game = GetGame();
    triggerComp_->VisitObjectInside([&](uint32_t id) -> Iteration
    {
        auto* object = game->GetObject<GameObject>(id);
        if (object)
            result.push_back(object);
        return Iteration::Continue;
    });
    return result;
}

bool GameObject::_LuaIsObjectInSight(const GameObject* object) const
{
    if (!object)
        return false;
    return IsObjectInSight(*object);
}

std::vector<GameObject*> GameObject::_LuaRaycast(const Math::StdVector3& direction)
{
    std::vector<GameObject*> result;

    auto* octand = GetOctant();
    if (!octand)
        return result;

    Math::Vector3 src = transformation_.position_;
    src.y_ += GetWorldBoundingBox().Extends().y_;
    const Math::Vector3 dest(direction);
    ea::vector<Math::RayQueryResult> res;
    const Math::Ray ray(src, dest);
    Math::RayOctreeQuery query(res, ray, src.Distance(dest));
    query.ignore_ = this;
    Math::Octree* octree = octand->GetRoot();
    octree->Raycast(query);
    result.reserve(query.result_.size());
    for (const auto& o : query.result_)
    {
        result.push_back(static_cast<GameObject*>(o.object_));
    }
    return result;
}

std::vector<GameObject*> GameObject::_LuaRaycastTo(const Math::StdVector3& destination)
{
    const Math::Vector3& src = transformation_.position_;
    const Math::Vector3 direction = Math::Vector3(destination) - src;
    return _LuaRaycast(direction);
}

std::vector<GameObject*> GameObject::_LuaRaycastToObject(GameObject* object)
{
    if (!object)
        return {};
    Math::Vector3 dest = object->GetPosition();
    const Math::BoundingBox bb = object->GetWorldBoundingBox();
    dest.y_ += bb.Extends().y_;
    return _LuaRaycastTo(dest);
}

bool GameObject::IsInRange(Range range, const GameObject* object) const
{
    if (!object)
        return false;
    if (object == this)
        return true;
    if (range == Range::Map)
        return true;
    // Don't calculate the distance now, but use previously calculated values.
    return ranges_.Contains(range, object->id_);
}

bool GameObject::IsCloserThan(float maxDist, const GameObject* object) const
{
    if (!object)
        return false;
    const Range range = GetRangeFromDistance(maxDist);
    return IsInRange(range, object);
}

std::vector<Actor*> GameObject::_LuaGetActorsInRange(Range range) const
{
    std::vector<Actor*> result;
    VisitInRange(range, [&](GameObject& o)
    {
        if (o.IsPlayerOrNpcType())
            result.push_back(To<Actor>(&o));
        return Iteration::Continue;
    });
    return result;
}

Actor* GameObject::GetClosestActor(const std::function<bool(const Actor& actor)>& callback) const
{
    Actor* result = nullptr;
    sa::EnumIterator<Range, Range::Touch, Range::Map> rangeIterator;
    for (auto r : rangeIterator)
    {
        VisitInRange(static_cast<Range>(r), [&](GameObject& o)
        {
            if (o.IsPlayerOrNpcType())
            {
                result = To<Actor>(&o);
                if (!callback(*result))
                {
                    result = nullptr;
                    return Iteration::Continue;
                }
                return Iteration::Break;
            }
            return Iteration::Continue;
        });
        if (result)
            break;
    }
    return result;
}

Actor* GameObject::GetClosestActor(bool undestroyable, bool unselectable) const
{
    return GetClosestActor([&](const Actor& actor) -> bool
    {
        if (!actor.IsSelectable() && !unselectable)
            return false;
        if ((actor.IsUndestroyable() || actor.IsDead()) && !undestroyable)
            return false;
        return true;
    });
}

Actor* GameObject::_LuaAsActor()
{
    return To<Actor>(this);
}

Npc* GameObject::_LuaAsNpc()
{
    return To<Npc>(this);
}

Player* GameObject::_LuaAsPlayer()
{
    return To<Player>(this);
}

AreaOfEffect* GameObject::_LuaAsAOE()
{
    return To<AreaOfEffect>(this);
}

Projectile* GameObject::_LuaAsProjectile()
{
    return To<Projectile>(this);
}

void GameObject::_LuaSetPosition(const Math::StdVector3& pos)
{
    transformation_.position_ = pos;
}

void GameObject::_LuaSetRotation(float y)
{
    float ang = Math::DegToRad(y);
    Math::NormalizeAngle(ang);
    transformation_.SetYRotation(ang);
}

void GameObject::_LuaSetScale(const Math::StdVector3& scale)
{
    transformation_.scale_ = scale;
}

void GameObject::_LuaSetScaleSimple(float value)
{
    transformation_.scale_ = {  value, value, value };
}

Math::StdVector3 GameObject::_LuaGetPosition() const
{
    return static_cast<Math::StdVector3>(transformation_.position_);
}

float GameObject::_LuaGetRotation() const
{
    return Math::RadToDeg(transformation_.GetYRotation());
}

Math::StdVector3 GameObject::_LuaGetScale() const
{
    return static_cast<Math::StdVector3>(transformation_.scale_);
}

void GameObject::_LuaSetBoundingBox(const Math::StdVector3& min, const Math::StdVector3& max)
{
    if (!collisionShape_)
        return;
    if (collisionShape_->shapeType_ == Math::ShapeType::BoundingBox)
    {
        using BBoxShape = Math::CollisionShape<Math::BoundingBox>;
        BBoxShape* shape = static_cast<BBoxShape*>(GetCollisionShape());
        auto& obj = shape->Object();
        obj.min_ = min;
        obj.max_ = max;
    }
}

void GameObject::SetBoundingSize(const Math::Vector3& size)
{
    if (!collisionShape_)
        return;
    switch (collisionShape_->shapeType_)
    {
    case Math::ShapeType::BoundingBox:
    {
        const Math::Vector3 halfSize = (size * 0.5f);
        using BBoxShape = Math::CollisionShape<Math::BoundingBox>;
        BBoxShape* shape = static_cast<BBoxShape*>(GetCollisionShape());
        auto& obj = shape->Object();
        obj.min_ = -halfSize;
        obj.max_ = halfSize;
        break;
    }
    case Math::ShapeType::Sphere:
    {
        using SphereShape = Math::CollisionShape<Math::Sphere>;
        SphereShape* shape = static_cast<SphereShape*>(GetCollisionShape());
        auto& obj = shape->Object();
        obj.radius_ = size.x_ * 0.5f;
        break;
    }
    default:
        // Not possible for other shapes
        LOG_WARNING << "Can not set size of shape type " << static_cast<int>(collisionShape_->shapeType_) << std::endl;
        break;
    }
}

void GameObject::_LuaSetBoundingSize(const Math::StdVector3& size)
{
    SetBoundingSize(size);
}

std::string GameObject::_LuaGetVarString(const std::string& name) const
{
    return GetVar(name).GetString();
}

void GameObject::_LuaSetVarString(const std::string& name, const std::string& value)
{
    SetVar(name, Utils::Variant(value));
}

float GameObject::_LuaGetVarNumber(const std::string& name) const
{
    return GetVar(name).GetFloat();
}

void GameObject::_LuaSetVarNumber(const std::string& name, float value)
{
    SetVar(name, Utils::Variant(value));
}

Game* GameObject::_LuaGetGame() const
{
    if (auto g = game_.lock())
        return g.get();
    return nullptr;
}

int GameObject::_LuaGetState() const
{
    return static_cast<int>(stateComp_.GetState());
}

void GameObject::_LuaSetState(int state)
{
    SetState(static_cast<AB::GameProtocol::CreatureState>(state));
}

void GameObject::AddToOctree()
{
    if (auto g = game_.lock())
    {
#ifdef DEBUG_OCTREE
        LOG_DEBUG << "Adding " << *this << " to Octree" << std::endl;
#endif
        g->map_->octree_->InsertObject(this);
        // Initial update.
        if (auto* o = GetOctant())
        {
            Math::Octree* octree = o->GetRoot();
            octree->AddObjectUpdate(this);
        }
#ifdef DEBUG_OCTREE
        else
        {
            LOG_WARNING << "octant_ == null" << std::endl;
        }
#endif
    }
}

void GameObject::RemoveFromOctree()
{
    if (auto* o = GetOctant())
    {
#ifdef DEBUG_OCTREE
        LOG_DEBUG << "Removing " << *this << " from Octree" << std::endl;
#endif
        o->RemoveObject(this);
    }
}

void GameObject::WriteSpawnData(MessageStream& msg)
{
    auto& m = msg.GetMessage();
    m.Add<uint32_t>(id_);
    m.Add<uint8_t>(static_cast<uint8_t>(GetType()));
}

bool GameObject::Serialize(sa::PropWriteStream& stream)
{
    stream.WriteString(GetName());
    return true;
}

bool GameObject::IsInOutpost() const
{
    if (auto g = game_.lock())
        return AB::Entities::IsOutpost(g->data_.type);
    return false;
}

bool GameObject::IsInBattle() const
{
    if (auto g = game_.lock())
        return AB::Entities::IsBattle(g->data_.type);
    return false;
}

uint32_t GameObject::GetRetriggerTimout() const
{
    if (!triggerComp_)
        return 0;
    return triggerComp_->retriggerTimeout_;
}

void GameObject::SetRetriggerTimout(uint32_t value)
{
    if (!triggerComp_)
        triggerComp_ = ea::make_unique<Components::TriggerComp>(*this);
    triggerComp_->retriggerTimeout_ = value;
}

bool GameObject::IsTrigger() const
{
    return triggerComp_ && triggerComp_->trigger_;
}

void GameObject::SetTrigger(bool value)
{
    if (!triggerComp_)
        triggerComp_ = ea::make_unique<Components::TriggerComp>(*this);
    triggerComp_->trigger_ = value;
}

void GameObject::VisitInRange(Range range, const std::function<Iteration(GameObject&)>& func) const
{
    auto game = GetGame();
    if (!game)
        return;

    const auto& r = ranges_[range];
    for (uint32_t objectId : r)
    {
        auto* object = game->GetObject<GameObject>(objectId);
        if (object)
        {
            if (func(*object) != Iteration::Continue)
                break;
        }
    }
}

}
