/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "IOAsset.h"
#include "Scene.h"
#include <pugixml.hpp>
#include <libmath/Matrix4.h>

namespace IO {

class IOScene final : public IOAssetImpl<Game::Scene>
{
private:
    static bool LoadSceneNode(Game::Scene& asset, const pugi::xml_node& node, const Math::Matrix4& parentMatrix);
public:
    bool Import(Game::Scene& asset, const std::string& name) override;
};

}
