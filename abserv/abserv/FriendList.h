/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/FriendList.h>
#include <sa/Iteration.h>
#include <AB/Entities/Character.h>

namespace Game {

class FriendList
{
public:
    enum class Error
    {
        Success,
        PlayerNotFound,
        AlreadyFriend,
        NoFriend,
        InternalError
    };
private:
    std::string accountUuid_;
    FriendList::Error AddFriendAccount(const AB::Entities::Character& ch,
        AB::Entities::FriendRelation relation);
    static void InvalidateList(const std::string& uuid);
public:
    FriendList(const std::string accountUuid) :
        accountUuid_(accountUuid),
        data_{}
    { }
    ~FriendList() = default;
    FriendList(const FriendList&) = delete;
    FriendList& operator=(const FriendList&) = delete;

    void Load();
    void Save(bool flush = false);
    FriendList::Error AddFriendByName(const std::string& playerName, AB::Entities::FriendRelation relation);
    FriendList::Error ChangeNickname(const std::string& accountUuid, const std::string& newName);
    FriendList::Error Remove(const std::string& accountUuid);

    bool Exists(const std::string& accountUuid) const;
    bool IsFriend(const std::string& accountUuid) const;
    bool IsIgnored(const std::string& accountUuid) const;
    bool IsIgnoredByName(const std::string& name) const;
    bool GetFriendByName(const std::string& name, AB::Entities::Friend& f) const;
    bool GetFriendByAccount(const std::string& accountUuid, AB::Entities::Friend& f) const;
    template <typename Callback>
    void VisitAll(Callback&& callback) const
    {
        for (auto& f : data_.friends)
            if (callback(f) != Iteration::Continue)
                break;
    }
    size_t Count() const { return data_.friends.size(); }

    AB::Entities::FriendList data_;
};

}
