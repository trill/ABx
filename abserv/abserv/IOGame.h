/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Game.h>

namespace Game {
class Game;
}

namespace IO {

bool LoadGame(AB::Entities::Game& game);
bool LoadGameByName(Game::Game& game, const std::string& name);
bool LoadGameByUuid(Game::Game& game, const std::string& uuid);
std::string GetLandingGameUuid();
AB::Entities::GameType GetGameType(const std::string& mapUuid);
std::vector<AB::Entities::Game> GetGameList();
std::string GetGameUuidFromName(const std::string& name);

}
