/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "GameObject.h"
#include <libmath/Point.h>
#include <libmath/BoundingBox.h>
#include <libmath/Mesh.h>
#include <libmath/MeshObject.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Game {

class Terrain;

class TerrainPatch final : public GameObject, public Math::MeshObject
{
    NON_COPYABLE(TerrainPatch)
    NON_MOVEABLE(TerrainPatch)
private:
    ea::weak_ptr<Terrain> owner_;
    Math::BoundingBox boundingBox_;
    Math::BoundingBox worldBoundingBox_;
    float CastRay(const Math::Vector3& origin, const Math::Vector3& direction, float maxDist, Math::Vector3& position) const;
    ea::shared_ptr<Math::Mesh> GenerateMesh() const override;
public:
    TerrainPatch(ea::shared_ptr<Terrain> owner,
        const Math::IntVector2& offset,
        const Math::IntVector2& size);
    ~TerrainPatch() override = default;

    /// Process octree raycast.
    void ProcessRayQuery(const Math::RayOctreeQuery& query, ea::vector<Math::RayQueryResult>& results) override;
    Math::BoundingBox GetWorldBoundingBox() const override
    {
        return worldBoundingBox_;
    }
    Math::BoundingBox GetBoundingBox() const override
    {
        return boundingBox_;
    }
    AB::GameProtocol::GameObjectType GetType() const override
    {
        return AB::GameProtocol::GameObjectType::TerrainPatch;
    }
    float GetHeight(const Math::Vector3& position) const;

    Math::IntVector2 offset_;
    Math::IntVector2 size_;
};

template <>
inline bool Is<TerrainPatch>(const GameObject& obj)
{
    return obj.GetType() == AB::GameProtocol::GameObjectType::TerrainPatch;
}

}
