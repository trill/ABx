/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libshared/Damage.h>
#include <sa/Noncopyable.h>
#include <libshared/Attributes.h>
#include <libmath/Vector3.h>
#include <eastl.hpp>
#include "EffectDefs.h"
#include <AB/Entities/Skill.h>
#include <AB/ProtocolCodes.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class Skill;
class Item;
class MessageStream;

namespace Components {

class EffectsComp
{
    NON_COPYABLE(EffectsComp)
    NON_MOVEABLE(EffectsComp)
private:
    static constexpr AB::Entities::EffectCategory SINGLEEFFECT_START = AB::Entities::EffectPreparation;
    static constexpr AB::Entities::EffectCategory SINGLEEFFECT_END = AB::Entities::EffectWeaponSpell;
    Actor& owner_;
    EffectList addedEffects_;
    EffectList removedEffects_;
    EffectList maintainedEffects_;
    struct MaintainingEffects
    {
        uint32_t effectIndex;
        uint32_t targetId;
        bool maintained;
    };
    ea::vector<MaintainingEffects> newMaintainingEffects_;
private:
    void MaintainEffect(ea::shared_ptr<Effect> effect);
    // Events
    void OnGetCriticalHit(Actor* source, bool& value);
    void OnAddEffect(Actor* source, Effect* effect, bool& value, uint32_t time);
    void OnRemoveEffect(Effect* e, bool& value);
    void OnSourceAttackStart(Actor* target, bool& value);
    void OnSourceAttackSuccess(Actor* target, DamageType type, int32_t damage);
    void OnTargetAttackSuccess(Actor* source, DamageType type, int32_t damage);
    void OnTargetAttackStart(Actor* source, bool& value);
    void OnTargetAttackEnd(Actor* source, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error);
    void OnTargetAttackBlocked(Actor* source);
    void OnInterruptingAttack(bool& value);
    void OnInterruptingSkill(Actor* source, AB::Entities::SkillType type, Skill* skill, bool& value);
    void OnCanUseSkill(Actor* target, Skill* skill, bool& value);
    void OnGettingSkillTarget(Actor* source, Skill* skill, bool& value);
    void OnWasInterrupted(Actor* source, Skill* skill);
    void OnSkillTargeted(Actor* source, Skill* skill);
    void OnKnockingDown(Actor* source, uint32_t time, bool& value);
    void OnKnockedDown(uint32_t time);
    void OnHealing(Actor* source, int& value);
    void OnHealingTarget(Actor* source, int& value);
    void OnStartUseSkill(Actor* target, Skill* skill);
    void OnEndUseSkill(Actor* target, Skill* skill);
    void OnTargetSkillSuccess(Actor* source, Skill* skill);
    void OnMorale(int morale);
    void OnDealingDamage(Actor* target, DamageType type, int32_t& value, bool& success);
    void OnDied(Actor* actor, Actor* killer);
    // We get KD'd by source
    void OnKnockingDownTarget(Actor* source, uint32_t time, bool& success);
    // We get interrupted by source
    void OnInterruptingTarget(Actor* source, bool& success);
    void OnGettingDamage(Actor* source, Skill*, DamageType, int32_t& value);
    void OnGotDamage(Actor* source, Skill*, DamageType, int32_t value);
    void OnShadowStep(const Math::Vector3& pos, bool& success);
    void OnInterrupted(Actor* target, Skill* skill, bool success);
    void OnHealedTarget(Actor* target, Skill* skill, int value);
    void OnEnergyZero();
    void OnSpawnedSlave(Actor* slave);
    void OnExploitedCorpse(Actor* corpse);
public:
    explicit EffectsComp(Actor& owner);
    ~EffectsComp();

    /// Add an effect.
    /// @param source The source of the effect, can be empty
    /// @param index The effect index
    /// @param time How long does the effect last. If 0 the script defines the duration.
    bool AddEffect(ea::shared_ptr<Actor> source, uint32_t index, uint32_t time);
    /// Remove effect before it ended
    bool RemoveEffect(uint32_t index, bool force = false);
    size_t RemoveAllOfCategory(AB::Entities::EffectCategory category);
    bool HasEffect(uint32_t index) const;
    Effect* GetEffect(uint32_t index) const;
    bool HasEffectOf(AB::Entities::EffectCategory category);
    Effect* GetLast(AB::Entities::EffectCategory category);
    bool RemoveLastEffectOf(AB::Entities::EffectCategory category);
    void RemoveAllEffectsFrom(uint32_t sourceId, uint32_t categories, bool force);
    void RemoveAll();
    bool RemoveMaintainedEffect(uint32_t effectIndex, uint32_t targetId);
    Effect* GetMaintainedEffect(uint32_t effectIndex, uint32_t targetId) const;
    void Update(uint32_t timeElapsed);
    void Write(MessageStream& message);
    /// Get real cost of a skill
    /// \param skill The Skill
    /// \param activation Activation time
    /// \param energy Energy cost
    /// \param adrenaline Adrenaline cost
    /// \param overcast Causes overcast
    /// \param hp HP scarifies in percent of max health
    void GetSkillCost(Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetTargetSkillCost(Actor* source, Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    void GetSkillActivation(Skill* skill, uint32_t& activation);
    void GetSkillHpSacrifies(Skill* skill, uint32_t& hp);
    void GetDamage(DamageType type, int32_t& value, bool& critical);
    void GetAttackSpeed(Item* weapon, uint32_t& value);
    void GetAttackDamageType(DamageType& type);
    void GetAttackDamage(int32_t& value);
    void GetArmor(DamageType type, int& value);
    // We get damage
    void GetArmorPenetration(DamageType damageType, float& value);
    // We deal damage. Maybe we have an effect on us that in-/decreases armor penetration of OUR attacks/spells
    void GetSourceArmorPenetration(Actor* target, DamageType damageType, float& value);
    void GetAttributeRank(Attribute index, int32_t& value);
    void GetResources(int& maxHealth, int& maxEnergy);
    void GetRegeneration(int& healthRegen, int& energyRegen);
    void GetSpeedFactor(float& speedFactor);
    void ActorDied(Actor* actor, Actor* killer);
    void GetSkillDamageBonus(Actor* target, Skill* skill, DamageType type, int& value);
    void GetEnergyFromSoulReaping(Actor* target, int& value);
    void VisitEffectsOf(AB::Entities::EffectCategory category, const std::function<Iteration(Effect*)>& callback);
    void VisitEffects(const std::function<Iteration(Effect*)>& callback);

    EffectList effects_;
};

}
}
