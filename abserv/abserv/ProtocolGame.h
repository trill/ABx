/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <uuid.h>
#include <libcommon/Protocol.h>
#include <libcommon/Connection.h>
#include <libcommon/Dispatcher.h>
#include <AB/ProtocolCodes.h>
#include <libcommon/Subsystems.h>
#include "InputQueue.h"
#include <AB/Packets/ClientPackets.h>
#include <eastl.hpp>

namespace Game {
class Player;
}

namespace Net {

class ProtocolGame final : public Protocol
{
    friend class Game::Player;
public:
    static constexpr bool ServerSendsFirst = true;
    static constexpr uint8_t ProtocolIdentifier = 0; // Not required as we send first
    static constexpr bool UseChecksum = true;
    static const char* ProtocolName() { return "Game Protocol"; }
    static std::string serverId_;
private:
    ea::weak_ptr<Game::Player> player_;
    DH_KEY clientKey_;
    bool connected_{ false };
    std::mutex lock_;
    ea::shared_ptr<Game::Player> GetPlayer()
    {
        return player_.lock();
    }
    inline void AddPlayerInput(Game::InputType type, Utils::VariantMap&& data);
    inline void AddPlayerInput(Game::InputType type);
    void Login(AB::Packets::Client::GameLogin packet);
    /// The client requests to enter a game. Find/create it, add the player and return success.
    void EnterGame(ea::shared_ptr<Game::Player> player);
    void Release() override;
public:
    explicit ProtocolGame(std::shared_ptr<Connection> connection);
    ~ProtocolGame() override;

    void Logout();
    /// Tells the client to change the instance. The client will disconnect and reconnect to enter the instance.
    void ChangeInstance(const std::string& mapUuid, const std::string& instanceUuid);
    void ChangeServerInstance(const std::string& serverUuid,
        const std::string& mapUuid, const std::string& instanceUuid);
    void WriteToOutput(const NetworkMessage& message);
private:
    template <typename Callable, typename... Args>
    void AddPlayerTask(Callable&& function, Args&&... args)
    {
        if (auto player = GetPlayer())
        {
            GetSubsystem<Asynch::Dispatcher>()->Add(
                Asynch::CreateTask(std::bind(std::move(function), player, std::forward<Args>(args)...))
            );
        }
    }

    std::shared_ptr<ProtocolGame> GetPtr()
    {
        return std::static_pointer_cast<ProtocolGame>(shared_from_this());
    }
    void ParsePacket(NetworkMessage& message) override;
    void OnRecvFirstMessage(NetworkMessage& msg) override;
    void OnConnect() override;

    void DisconnectClient(AB::ErrorCodes error);
    void DelayedDisconnectClient(AB::ErrorCodes error);
    void Connect();

    bool acceptPackets_{ false };
};

}
