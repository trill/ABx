/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <AB/ProtocolCodes.h>
#include <AB/Entities/Skill.h>
#include <sa/Noncopyable.h>
#include <libshared/Damage.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class Skill;
class Game;
class MessageStream;

namespace Components {

class SkillsComp
{
    NON_COPYABLE(SkillsComp)
    NON_MOVEABLE(SkillsComp)
private:
    static constexpr uint32_t LAST_INTERRUPTED_TIME = 5 * 60 * 1000;
    Actor& owner_;
    AB::GameProtocol::SkillError lastError_;
    /// Index in users skill bar, 0 based
    int lastSkillIndex_;
    bool startDirty_;
    bool endDirty_;
    bool usingSkill_;
    uint32_t newRecharge_;
    int64_t lastSkillTime_;
    ea::weak_ptr<Skill> lastSkill_;
    bool lastInterrupted_{ false };
    int64_t interruptedTick_{ 0 };
    // Great fit, 8 skills and 8 bits!
    uint8_t rechargedSkills_{ 0 };
    void OnIncMorale(int morale) const;
    void OnCancelAll();
    void OnSkillInterrupted(Skill* skill);
    void OnSkillRecharged(int pos, Skill* skill);
    void OnTargetAttackSuccess(Actor* source, DamageType type, int32_t damage);
public:
    explicit SkillsComp(Actor& owner);
    ~SkillsComp() = default;

    void Update(uint32_t timeElapsed);
    AB::GameProtocol::SkillError UseSkill(int index, bool ping);
    void Cancel();
    void CancelWhenChangingState();
    bool IsUsing();
    void Write(MessageStream& message);
    int64_t GetLastSkillTime() const { return lastSkillTime_; }
    bool Interrupt(AB::Entities::SkillType type);
    bool WasInterrupted() const { return interruptedTick_ != 0 && sa::time::time_elapsed(interruptedTick_) < LAST_INTERRUPTED_TIME; }
    /// Return currently using skill. Maybe nullptr.
    Skill* GetCurrentSkill();
    void GetResources(int& maxHealth, int& maxEnergy);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    void GetSkillCost(Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
};

}
}
