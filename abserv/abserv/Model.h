/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Asset.h"
#include <libmath/BoundingBox.h>

namespace Math {
class Mesh;
}

namespace Game {

/// 3D Model
class Model final : public IO::Asset
{
private:
    ea::unique_ptr<Math::Mesh> shape_;
public:
    Model();
    ~Model() override;
    void SetShape(ea::unique_ptr<Math::Mesh>&& shape);
    Math::BoundingBox GetBoundingBox() const
    {
        return boundingBox_;
    }
    Math::Mesh* GetMesh() const;

    Math::BoundingBox boundingBox_;
};

}
