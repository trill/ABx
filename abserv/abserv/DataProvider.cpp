/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DataProvider.h"
#include "ConfigManager.h"
#include "NavigationMesh.h"
#include "IONavMesh.h"
#include "IOTerrain.h"
#include "IOHeightMap.h"
#include "Terrain.h"
#include "Model.h"
#include "IOModel.h"
#include "IOScript.h"
#include "IOScene.h"
#include <libcommon/Utils.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>

namespace IO {

DataProvider::DataProvider()
{
    // Add Importer
    AddImporter<Game::Scene, IO::IOScene>();
    AddImporter<Navigation::NavigationMesh, IO::IONavMesh>();
    AddImporter<Game::Terrain, IO::IOTerrain>();
    AddImporter<Game::HeightMap, IO::IOHeightMap>();
    AddImporter<Game::Model, IO::IOModel>();
    AddImporter<Game::Script, IO::IOScript>();
}

DataProvider::~DataProvider() = default;

void DataProvider::Update()
{
    if (!watchFiles_)
        return;

    for (const auto& item : cache_)
    {
        if (item.second.watcher)
            item.second.watcher->Update();
    }
}

std::string DataProvider::GetDataFile(const std::string& name)
{
    return Utils::ConcatPath(GetDataDir(), name);
}

const std::string& DataProvider::GetDataDir()
{
    return (*GetSubsystem<ConfigManager>())[ConfigManager::DataDir].GetString();
}

bool DataProvider::FileExists(const std::string& name)
{
    return Utils::FileExists(name);
}

std::string DataProvider::GetFile(const std::string& name)
{
    if (Utils::FileExists(name))
        return name;
    std::string n = GetDataFile(name);
    if (Utils::FileExists(n))
        return n;
    return name;
}

void DataProvider::CleanCache()
{
    if (cache_.empty())
        return;

#ifdef _DEBUG
    LOG_DEBUG << "Cleaning cache" << std::endl;
#endif
    // Delete all assets that are only owned by the cache
    auto i = cache_.begin();
    while ((i = ea::find_if(i, cache_.end(), [](const auto& current) -> bool
    {
        if (current.second.asset.use_count() == 1)
            return sa::time::time_elapsed(current.second.lastUsed) >= CACHE_KEEP_UNUSED_ASSETS;
        return false;
    })) != cache_.end())
    {
        cache_.erase(i++);
    }
}

}
