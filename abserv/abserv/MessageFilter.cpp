/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageFilter.h"
#include <libcommon/MessageDecoder.h>
#include <sa/Assert.h>
#include <libcommon/Logger.h>
#include "Game.h"
#include "Player.h"

namespace Net {

void MessageFilter::Execute(const Game::Game& game, const Game::Player& player, const NetworkMessage& source, NetworkMessage& dest)
{
    using namespace AB::Packets::Server;
    MessageDecoder decoder(source);
    for (;;)
    {
        auto code = decoder.GetNext();
        if (!code.has_value())
            return;

        switch (code.value())
        {
#define ENUMERATE_SERVER_PACKET_CODE(v) case AB::GameProtocol::ServerPacketType::v:                 \
            {                                                                                       \
                v packet = AB::Packets::Get<v>(decoder);                                            \
                constexpr size_t id = sa::StringHash(sa::TypeName<v>::Get());                       \
                if (events_.HasSubscribers<bool(const Game::Game&, const Game::Player&, v&)>(id))   \
                {                                                                                   \
                    if (!events_.CallOne<bool(const Game::Game&, const Game::Player&, v&)>(id, game, player, packet))  \
                        continue;                                                                   \
                }                                                                                   \
                Insert(dest, AB::GameProtocol::ServerPacketType::v, packet);                        \
                break;                                                                              \
            }
            ENUMERATE_SERVER_PACKET_CODES
#undef ENUMERATE_SERVER_PACKET_CODE
        default:
            dest.Add(code.value());
            break;
        }
    }
}

}
