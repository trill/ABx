/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include "LuaObject.h"
#include <sa/PropStream.h>
#include <AB/Entities/Skill.h>
#include <AB/ProtocolCodes.h>
#include <libshared/Damage.h>
#include <libshared/Attributes.h>
#include <libmath/Vector3.h>
#include <sa/Noncopyable.h>
#include <sa/Bits.h>
#include <eastl.hpp>
#include "EffectDefs.h"

namespace Game {

class Actor;
class Skill;
class Item;

class Effect
{
    NON_COPYABLE(Effect)
private:
    LuaObject lua_;
    int64_t startTime_;
    int64_t endTime_;
    /// Duration
    uint32_t ticks_;
    ea::weak_ptr<Actor> target_;
    ea::weak_ptr<Actor> source_;
    uint32_t sourceId_{ 0 };
    bool persistent_{ false };
    /// Internal effects are not visible to the player.
    bool internal_{ false };
    bool UnserializeProp(EffectAttr attr, sa::PropReadStream& stream);
    int _LuaGetCategory() const;
    bool _LuaRemove();
public:
    static void RegisterLua(kaguya::State& state);

    Effect() = delete;
    explicit Effect(const AB::Entities::Effect& effect);
    ~Effect() = default;

    /// Gets saved to the DB when player logs out, e.g. Dishonored.
    [[nodiscard]] bool IsPersistent() const
    {
        return persistent_;
    }
    [[nodiscard]] bool IsInternal() const { return internal_; }
    [[nodiscard]] uint32_t GetIndex() const { return effectIndex_; }

    bool LoadScript(const std::string& fileName);
    void Update(uint32_t timeElapsed);
    bool Start(ea::shared_ptr<Actor> source, ea::shared_ptr<Actor> target, uint32_t time);
    Actor* GetTarget() const;
    Actor* GetSource() const;
    [[nodiscard]] uint32_t GetSourceId() const { return sourceId_; }
    /// Remove Effect before it ends
    void DoRemove();
    /// Get real cost of a skill
    /// \param skill The Skill
    /// \param activation Activation time
    /// \param energy Energy cost
    /// \param adrenaline Adrenaline cost
    /// \param overcast Causes overcast
    /// \param hp HP scarifies in percent of max health
    void GetSkillCost(Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetTargetSkillCost(Actor* source, Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    void GetSkillActivation(Skill* skill, uint32_t& activation);
    void GetSkillHpSacrifies(Skill* skill, uint32_t& hp);
    /// Get real damage. It may be in-/decreased by some effects on the *Target*. This is called when the damage is applied to the target.
    void GetDamage(DamageType type, int32_t& value, bool& critical);
    void GetAttackSpeed(Item* weapon, uint32_t& value);
    void GetAttackDamageType(DamageType& type);
    void GetArmor(DamageType type, int& value);
    void GetArmorPenetration(DamageType damageType, float& value);
    void GetSourceArmorPenetration(Actor* target, DamageType damageType, float& value);
    void GetAttributeRank(Attribute index, int32_t& value);
    /// Attack damage may be in-/decreased by effects on the *Source*. This is called when the source starts attacking.
    void GetAttackDamage(int32_t& value);
    void GetRecources(int& maxHealth, int& maxEnergy);
    void GetRegeneration(int& healthRegen, int& energyRegen);
    void GetMaintainerRegeneration(int& healthRegen, int& energyRegen);
    void GetSpeedFactor(float& speedFactor, bool& stop);
    void GetSkillDamageBonus(Actor* source, Actor* target, Skill* skill, DamageType type, int& value);
    void GetEnergyFromSoulReaping(Actor* source, Actor* target, int& value);
    /// Some effect may make the attacker unable to attack. The target is being attacked.
    void OnSourceAttackStart(Actor* source, Actor* target, bool& value);
    void OnSourceAttackSuccess(Actor* source, Actor* target, DamageType type, int32_t damage);
    void OnTargetAttackSuccess(Actor* source, Actor* target, DamageType type, int32_t damage);
    void OnTargetAttackEnd(Actor* source, Actor* target, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error);
    void OnTargetAttackBlocked(Actor* source, Actor* target);
    /// Checks whether the owner can be attacked
    void OnTargetAttackStart(Actor* source, Actor* target, bool& value);
    void OnCanUseSkill(Actor* source, Actor* target, Skill* skill, bool& value);
    void OnGettingSkillTarget(Actor* source, Actor* target, Skill* skill, bool& value);
    void OnSkillTargeted(Actor* source, Actor* target, Skill* skill);
    void OnInterruptingAttack(bool& value);
    void OnInterruptingSkill(Actor* source, Actor* target, AB::Entities::SkillType type, Skill* skill, bool& value);
    void OnKnockingDown(Actor* source, Actor* target, uint32_t time, bool& value);
    void OnKnockedDown(Actor* target, uint32_t time);
    void OnGetCriticalHit(Actor* source, Actor* target, bool& value);
    void OnAddEffect(Actor* source, Actor* target, Effect* effect, bool& value, uint32_t& time);
    void OnRemoveEffect(Actor* target, Effect* effect, bool& value);
    /// Targets gets healed by source
    void OnHealing(Actor* source, Actor* target, int& value);
    void OnHealingTarget(Actor* source, Actor* target, int& value);
    void OnStartUseSkill(Actor* source, Actor* target, Skill* skill);
    void OnEndUseSkill(Actor* source, Actor* target, Skill* skill);
    void OnTargetSkillSuccess(Actor* source, Actor* target, Skill* skill);
    void OnWasInterrupted(Actor* source, Actor* target, Skill* skill);
    void OnKnockingDownTarget(Actor* source, Actor* target, uint32_t time, bool& success);
    void OnInterruptingTarget(Actor* source, Actor* target, bool& success);
    void OnDealingDamage(Actor* source, Actor* target, DamageType type, int32_t& value, bool& success);
    void OnDied(Actor* source, Actor* killer);
    void OnGettingDamage(Actor* source, Actor* target, Skill*, DamageType, int32_t& value);
    void OnGotDamage(Actor* source, Actor* target, Skill*, DamageType, int32_t value);
    void OnShadowStep(Actor* source, const Math::Vector3& pos, bool& success);
    void OnInterrupted(Actor* source, Actor* target, Skill* skill, bool success);
    void OnHealedTarget(Actor* source, Actor* target, Skill* skill, int value);
    void OnEnergyZero(Actor* source);
    void OnMaintainerEnergyZero(Actor* source);
    void OnActorDied(Actor* actor, Actor* killer);
    void OnSpawnedSlave(Actor* actor, Actor* slave);
    void OnExploitedCorpse(Actor* actor, Actor* corpse);

    bool Serialize(sa::PropWriteStream& stream);
    bool Unserialize(sa::PropReadStream& stream);

    uint32_t effectIndex_{ 0 };
    AB::Entities::EffectCategory category_{ AB::Entities::EffectNone };
    AB::Entities::Effect data_;

    bool ended_;
    bool cancelled_;
    bool forceRemove_{ false };

    int64_t GetStartTime() const { return startTime_; }
    int64_t GetEndTime() const { return endTime_; }
    // Get base time
    uint32_t GetTicks() const { return ticks_; }
    uint32_t GetRemainingTime() const;
    Skill* GetCausingSkill() const;
};

}

