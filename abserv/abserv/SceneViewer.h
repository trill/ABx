/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#if defined(SCENE_VIEWER)

#include <sa/Compiler.h>
extern "C" {
#include <GL/glew.h>
}
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4505)
#include <GL/freeglut.h>
PRAGMA_WARNING_POP

#include <libmath/Vector3.h>
#include "Game.h"
#include <libmath/MathUtils.h>
#include <libmath/Camera.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Game {
class Player;
}

namespace Debug {

class SceneViewer : public std::enable_shared_from_this<SceneViewer>
{
    NON_COPYABLE(SceneViewer)
    NON_MOVEABLE(SceneViewer)
private:
    bool running_{ false };
    bool initialized_{ false };
    static SceneViewer* instance_;
    bool wireframe_{ true };
    bool terrain_{ true };
    bool terrainPatches_{ true };
    Math::Point<int> mousePos_{ 0, 0 };
    Math::Camera camera_;
    bool mouseDown_{ false };
    Math::IntVector2 mouseDownPos_;
    ea::weak_ptr<Game::Game> game_;
    GLuint shaderProgram_{ 0 };
    GLuint VAO_{ 0 };
    const Game::Player* cameraObject_{ nullptr };
    static void StaticRenderScene();
    static void StaticChangeSize(GLsizei w, GLsizei h);
    static void StaticMouse(int button, int state, int x, int y);
    static void StaticMouseMove(int x, int y);
    static void StaticKeyboard(unsigned char key, int x, int y);
    void Update();
    void UpdateMenu();
    void InternalInitialize();
    void DrawScene();
    void DrawTerrain(const Game::Terrain& terrain) const;
    void DrawObject(const Game::GameObject& object) const;
    void DrawMesh(const Math::Mesh& mesh, const Math::Matrix4& matrix, const std::array<float, 3>& color, bool wireframe) const;
    void DrawAxis() const;
    void DrawAxisLine(const Math::Line& line, const Math::Matrix4& matrix, const std::array<float, 3>& color) const;
    void Mouse(int button, int state, int x, int y);
    void MouseMove(int x, int y);
    void Menu(int id);
    void Keyboard(unsigned char key, int x, int y);
public:
    SceneViewer();
    ~SceneViewer();

    bool Initialize();
    void Run();
    void Stop();

    void Render();
    void ChangeSize(GLsizei w, GLsizei h);
};

}

#endif
