/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "GameObject.h"
#include "SkillDefs.h"
#include <sa/Bits.h>
#include <eastl.hpp>
#include "LuaObject.h"

namespace Game {

class Actor;

class AreaOfEffect final : public GameObject
{
private:
    enum Function : uint32_t
    {
        FunctionNone = 0,
        FunctionUpdate = 1,
        FunctionEnded = 1 << 1,
        FunctionOnTrigger = 1 << 2,
        FunctionOnLeftArea = 1 << 3,
        FunctionOnCollide = 1 << 4,
    };
    ea::weak_ptr<Actor> source_;
    LuaObject lua_;
    std::string scriptFile_;
    /// Effect or skill index
    uint32_t index_{ 0 };
    Range range_{ Range::Adjecent };
    uint32_t skillEffect_{ SkillEffectNone };
    uint32_t effectTarget_{ SkillTargetNone };
    uint32_t functions_{ FunctionNone };
    int64_t startTime_;
    // Lifetime
    uint32_t lifetime_{ std::numeric_limits<uint32_t>::max() };
    uint32_t itemIndex_{ 0 };
    void _LuaSetSource(Actor* source);
    Actor* _LuaGetSource() const;
    std::string _LuaGetScriptFile() const;
private:
    // Events
    void OnCollide(GameObject* other);
    void OnTrigger(GameObject* other);
    void OnLeftArea(GameObject* other);
public:
    static void RegisterLua(kaguya::State& state);

    AreaOfEffect();
    ~AreaOfEffect() override;

    bool LoadScript(const std::string& fileName);
    AB::GameProtocol::GameObjectType GetType() const override
    {
        return AB::GameProtocol::GameObjectType::AreaOfEffect;
    }
    void Update(uint32_t timeElapsed, MessageStream& message) override;

    /// Collision shape type
    void SetShapeType(Math::ShapeType shape);
    Math::ShapeType GetShapeType() const;
    void SetRange(Range range);
    Range GetRange() const { return range_; }
    void SetSource(ea::shared_ptr<Actor> source);
    ea::shared_ptr<Actor> GetSource() const;
    uint32_t GetLifetime() const { return lifetime_; }
    void SetLifetime(uint32_t value) { lifetime_ = value; }
    int64_t GetStartTime() const { return startTime_; }
    uint32_t GetIndex() const { return index_; }
    uint32_t GetRemainingTime() const;
    void SetIndex(uint32_t value) { index_ = value; }
    bool HasEffect(SkillEffect effect) const { return (skillEffect_ & effect) == effect; }
    bool HasTarget(SkillEffectTarget t) const { return (effectTarget_ & t) == t; }

    uint32_t GetGroupId() const;
    bool IsEnemy(const Actor* other) const;
    bool IsAlly(const Actor* other) const;
    bool IsInRange(const Actor* other) const;
    uint32_t GetItemIndex() const;
    void SetItemIndex(uint32_t value) { itemIndex_ = value; }

    bool Serialize(sa::PropWriteStream& stream) override;
    void WriteSpawnData(MessageStream& msg) override;
};

template <>
inline bool Is<AreaOfEffect>(const GameObject& obj)
{
    return obj.GetType() == AB::GameProtocol::GameObjectType::AreaOfEffect;
}

}
