/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>
#include "../Npc.h"

namespace AI {
namespace Conditions {

class IsCombatMode final : public Condition
{
    CONDITON_CLASS(IsCombatMode)
private:
    Game::Npc::CombatMode mode_{ Game::Npc::CombatMode::Guard };
public:
    explicit IsCombatMode(const ArgumentsType& arguments);
    bool Evaluate(Agent& agent, const Node& node) override;
};

}
}
