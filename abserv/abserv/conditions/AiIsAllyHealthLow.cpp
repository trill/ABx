/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsAllyHealthLow.h"
#include "../Game.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

bool IsAllyHealthLow::Evaluate(Agent& agent, const Node&)
{
    auto& npc = AI::GetNpc(agent);
    bool result = false;
    npc.VisitAlliesInRange(Game::Range::HalfCompass, [&result](const Game::Actor& o)
    {
        if (!o.IsDead() && o.resourceComp_->GetHealthRatio() < LOW_HP_THRESHOLD)
        {
            result = true;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });

    return result;
}

}
}
