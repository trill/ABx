/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>
#include <libshared/Mechanic.h>

namespace AI {
namespace Conditions {

class IsAtHome final : public Condition
{
    CONDITON_CLASS(IsAtHome)
private:
    Game::Range range_;
public:
    explicit IsAtHome(const ArgumentsType& arguments);
    bool Evaluate(AI::Agent&, const Node&) override;
};

}
}
