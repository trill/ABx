/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsCloseToSelection.h"
#include "../Game.h"
#include "../Npc.h"
#include "../GameObject.h"
#include "CleanupNs.h"
#include <sa/Assert.h>

namespace AI {
namespace Conditions {

IsCloseToSelection::IsCloseToSelection(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<float>(arguments, 0, distance_);
}

bool IsCloseToSelection::Evaluate(Agent& agent, const Node&)
{
    const auto& selection = agent.filteredAgents_;
    if (selection.empty())
        return false;

    auto& npc = AI::GetNpc(agent);
    ASSERT(npc.HasGame());

    auto game = npc.GetGame();

    const auto& ownPos = npc.GetPosition();
    for (auto id : selection)
    {
        auto* sel = game->GetObject<Game::GameObject>(id);
        const Math::Vector3& _pos = sel->GetPosition();
        const float distance = ownPos.Distance(_pos);
        if (distance > distance_)
            return false;
    }
    return true;
}

}
}
