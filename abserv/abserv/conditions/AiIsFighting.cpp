/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsFighting.h"
#include "../DamageComp.h"
#include "../Npc.h"
#include "../Group.h"
#include "../AttackComp.h"
#include <sa/time.h>

namespace AI {
namespace Conditions {

bool IsFighting::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    return npc.IsGroupFighting();
}

}

}
