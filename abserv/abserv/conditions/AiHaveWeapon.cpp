/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiHaveWeapon.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

bool HaveWeapon::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    auto* weapon = npc.GetWeapon();
    return !!weapon;
}

}

}
