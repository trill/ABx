/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiHaveWanderRoute.h"
#include "../Npc.h"
#include "../WanderComp.h"

namespace AI {
namespace Conditions {

bool HaveWanderRoute::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    if (!npc.IsWander())
        return false;

    return npc.wanderComp_->HaveRoute();
}

}

}
