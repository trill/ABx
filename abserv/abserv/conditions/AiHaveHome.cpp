/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


#include "AiHaveHome.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

bool HaveHome::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    return !npc.GetHomePos().Equals(Math::Vector3_Zero);
}

}
}
