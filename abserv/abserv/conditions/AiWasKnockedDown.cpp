/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiWasKnockedDown.h"
#include "../StateComp.h"
#include "../Game.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

bool WasKnockedDown::Evaluate(Agent& agent, const Node&)
{
    auto& npc = AI::GetNpc(agent);

    return npc.stateComp_.WasKnockedDown();
}

}
}
