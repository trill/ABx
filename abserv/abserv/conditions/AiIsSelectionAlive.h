/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

class IsSelectionAlive final : public Condition
{
    CONDITON_CLASS(IsSelectionAlive)
public:
    explicit IsSelectionAlive(const ArgumentsType& arguments) :
        Condition(arguments)
    { }
    bool Evaluate(AI::Agent&, const Node&) override;
};

}
}
