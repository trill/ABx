/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsAtHome.h"
#include "../Game.h"
#include "../Npc.h"
#include <libshared/Mechanic.h>
#include <libcommon/Utils.h>
#include <libcommon/Logger.h>

namespace AI {
namespace Conditions {

IsAtHome::IsAtHome(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<Game::Range>(arguments, 0, range_);
    if (static_cast<size_t>(range_) >= Utils::CountOf(Game::RangeDistances))
    {
        LOG_ERROR << "Range has an impossible value " << static_cast<size_t>(range_) << std::endl;
        range_ = Game::Range::Touch;
    }
}

bool IsAtHome::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    bool haveHome = !npc.GetHomePos().Equals(Math::Vector3_Zero);
    if (!haveHome)
        return true;
    if (range_ == Game::Range::Map)
        return true;

    return npc.GetHomePos().Distance(npc.GetPosition()) < Game::RangeDistances[static_cast<size_t>(range_)];
}

}
}
