/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsSelfHealthCritical.h"
#include "../Game.h"
#include "../Npc.h"

//#define DEBUG_AI

namespace AI {
namespace Conditions {

bool IsSelfHealthCritical::Evaluate(Agent& agent, const Node&)
{
    auto& npc = AI::GetNpc(agent);
    if (npc.IsDead())
        // Too late
        return false;
#ifdef DEBUG_AI
    LOG_DEBUG << npc.GetName() << " " << (npc.resourceComp_->GetHealth() < CRITICAL_HP_THRESHOLD) << std::endl;
#endif
    return npc.resourceComp_->GetHealth() < CRITICAL_HP_THRESHOLD;
}

}
}
