/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsAttacked.h"
#include "../DamageComp.h"
#include "../Game.h"
#include "../Npc.h"
#include <sa/time.h>

namespace AI {
namespace Conditions {

bool IsAttacked::Evaluate(Agent& agent, const Node&)
{
    auto& npc = AI::GetNpc(agent);
    return npc.IsGroupAttacked();
}

}
}
