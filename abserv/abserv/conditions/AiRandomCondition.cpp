/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiRandomCondition.h"
#include <libcommon/Subsystems.h>
#include <libcommon/Random.h>

namespace AI {
namespace Conditions {

RandomCondition::RandomCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<float>(arguments, 0, weight_);
}

bool RandomCondition::Evaluate(Agent&, const Node&)
{
    auto* rnd = GetSubsystem<Crypto::Random>();
    float rand = rnd->GetFloat() * weight_;
    return rand > 0.5f;
}

}
}
