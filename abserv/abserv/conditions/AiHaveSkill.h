/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>
#include "../SkillDefs.h"

namespace AI {
namespace Conditions {

class HaveSkill final : public Condition
{
    CONDITON_CLASS(HaveSkill)
private:
    Game::SkillEffect effect_{ Game::SkillEffectNone };
    Game::SkillEffectTarget effectTarget_{ Game::SkillTargetNone };
public:
    explicit HaveSkill(const ArgumentsType& arguments);
    bool Evaluate(Agent& agent, const Node& node) override;
};

}
}
