/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

class IsAllyHealthLow final : public AI::Condition
{
    CONDITON_CLASS(IsAllyHealthLow)
public:
    explicit IsAllyHealthLow(const ArgumentsType& arguments) :
        Condition(arguments)
    { }
    bool Evaluate(AI::Agent&, const Node&) override;
};

}
}
