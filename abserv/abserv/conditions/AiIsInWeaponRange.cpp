/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsInWeaponRange.h"
#include "../AiAgent.h"
#include "../Npc.h"
#include "../Item.h"
#include "../Game.h"
#include <libshared/Mechanic.h>

namespace AI {
namespace Conditions {

bool IsInWeaponRange::Evaluate(Agent& agent, const Node&)
{
    const auto& selection = agent.filteredAgents_;
    if (selection.empty())
        return true;

    const auto& npc = GetNpc(agent);
    auto* target = npc.GetGame()->GetObject<Game::GameObject>(selection[0]);
    if (!target)
        return true;

    auto* weapon = npc.GetWeapon();
    if (!weapon)
        return npc.IsInRange(Game::Range::Touch, target);

    float dist = weapon->GetWeaponRange();
    return (npc.GetDistance(target) <= dist);
}

}

}
