/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

class HaveResurrection final : public Condition
{
    CONDITON_CLASS(HaveResurrection)
public:
    explicit HaveResurrection(const ArgumentsType& arguments) :
        Condition(arguments)
    { }
    bool Evaluate(Agent& agent, const Node& node) override;
};

}
}
