/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiHaveResurrection.h"
#include "../AiAgent.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

bool HaveResurrection::Evaluate(Agent& agent, const Node&)
{
    Game::Npc& npc = GetNpc(agent);
    ea::vector<int> skills;
    return npc.GetSkillCandidates(skills, Game::SkillEffectResurrect, Game::SkillTargetTarget);
}

}
}
