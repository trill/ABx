/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

class IsCloseToSelection final : public Condition
{
    CONDITON_CLASS(IsCloseToSelection)
private:
    float distance_;
public:
    explicit IsCloseToSelection(const ArgumentsType& arguments);
    bool Evaluate(AI::Agent&, const Node&) override;
};

}
}
