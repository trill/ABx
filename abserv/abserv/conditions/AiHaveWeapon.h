/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

// Evaluates to true when the NPC has a weapon equipped.
class HaveWeapon final : public Condition
{
    CONDITON_CLASS(HaveWeapon)
public:
    explicit HaveWeapon(const ArgumentsType& arguments) :
        Condition(arguments)
    { }
    bool Evaluate(Agent& agent, const Node& node) override;
};

}
}
