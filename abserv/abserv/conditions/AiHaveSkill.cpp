/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiHaveSkill.h"
#include "../AiAgent.h"
#include "../Npc.h"

namespace AI {
namespace Conditions {

HaveSkill::HaveSkill(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<Game::SkillEffect>(arguments, 0, effect_);
    GetArgument<Game::SkillEffectTarget>(arguments, 1, effectTarget_);
}

bool HaveSkill::Evaluate(Agent& agent, const Node&)
{
    Game::Npc& npc = GetNpc(agent);
    ea::vector<int> skills;
    return npc.GetSkillCandidates(skills, effect_, effectTarget_);
}

}
}
