/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIsCombatMode.h"
#include "../AiAgent.h"
#include <libcommon/Logger.h>

namespace AI {
namespace Conditions {

IsCombatMode::IsCombatMode(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<Game::Npc::CombatMode>(arguments, 0, mode_);
}

bool IsCombatMode::Evaluate(Agent& agent, const Node&)
{
    Game::Npc& npc = GetNpc(agent);
//    LOG_DEBUG << (npc.combatMode_ == mode_) << std::endl;
    return npc.combatMode_ == mode_;
}

}
}
