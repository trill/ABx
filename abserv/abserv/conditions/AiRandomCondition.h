/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Condition.h>

namespace AI {
namespace Conditions {

class RandomCondition final : public Condition
{
    CONDITON_CLASS(RandomCondition)
private:
    float weight_{ 1.0f };
public:
    explicit RandomCondition(const ArgumentsType& arguments);
    bool Evaluate(Agent&, const Node&) override;
};

}
}
