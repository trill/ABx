/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOScript.h"
#include <lua.hpp>
#include <llimits.h>
#include <sa/ScopeGuard.h>

// For now don't strip debug info
#define LUA_STRIP 0

namespace IO {

static int writer(lua_State*, const void* p, size_t size, void* u)
{
    if (size == 0)
        return 1;
    const char* addr = reinterpret_cast<const char*>(p);
    ea::vector<char>& buffer = static_cast<Game::Script*>(u)->GetBuffer();
    ea::copy(addr, addr + size, ea::back_inserter(buffer));
    return 0;
}

bool IOScript::Import(Game::Script& asset, const std::string& name)
{
    // https://stackoverflow.com/questions/8936369/compile-lua-code-store-bytecode-then-load-and-execute-it
    // https://stackoverflow.com/questions/17597816/lua-dump-in-c
    lua_State* L = luaL_newstate();

    asset.GetBuffer().clear();
    sa::ScopeGuard luaGuard([&L]()
    {
        lua_close(L);
    });

    int ret = luaL_loadfile(L, name.c_str());
    if (ret != LUA_OK)
    {
        LOG_ERROR << "Compile error: " << lua_tostring(L, -1) << std::endl;
        return false;
    }
    lua_lock(L);
    lua_dump(L, writer, &asset, LUA_STRIP);
    lua_unlock(L);

    return true;
}

}
