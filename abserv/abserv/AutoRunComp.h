/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <libmath/Vector3.h>
#include <libmath/Quaternion.h>
#include <libshared/Mechanic.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Game {

class Actor;
class GameObject;

namespace Components {

#define MAX_OBSTACLE_AVOID_RECURSION_LEVEL 3

class AutoRunComp
{
    NON_COPYABLE(AutoRunComp)
    NON_MOVEABLE(AutoRunComp)
private:
    static constexpr uint32_t RECALCULATE_PATH_TIME = 1000;
    Actor& owner_;
    int64_t lastCalc_{ 0 };
    /// Maximum distance to consider being there
    float maxDist_{ RANGE_TOUCH };
    bool autoRun_{ false };
    ea::vector<Math::Vector3> wayPoints_;
    Math::Vector3 destination_;
    ea::weak_ptr<GameObject> following_;
    // Remove the first way points
    void Pop();
    // Get next waypoint
    Math::Vector3 Next();
    void MoveTo(uint32_t timeElapsed, const Math::Vector3& dest);
    bool FindPath(const Math::Vector3& dest);
    // Stop auto running and set state to idle
    void StopAutoRun();
    void OnCollide(GameObject* other);
    void OnStuck();
    void OnCancelAll();
    Math::Vector3 AvoidObstaclesInternal(const Math::Vector3& destination, unsigned recursionLevel);
    Math::Vector3 AvoidObstacles(const Math::Vector3& destination);
public:
    explicit AutoRunComp(Actor& owner);
    ~AutoRunComp() = default;

    bool Follow(ea::shared_ptr<GameObject> object, bool ping, float maxDist = RANGE_TOUCH);
    bool Goto(const Math::Vector3& dest);
    bool GotoDirection(const Math::Quaternion& direction, float distance);
    void Reset();
    bool HasWaypoints() const
    {
        return !wayPoints_.empty();
    }
    void Update(uint32_t timeElapsed);
    void SetAutoRun(bool value);
    bool IsAutoRun() const { return autoRun_; }
    bool IsFollowing(const GameObject& object) const;
};

}
}

