/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Effect.h"
#include "Actor.h"
#include "DataProvider.h"
#include "Item.h"
#include "Script.h"
#include "Skill.h"
#include "SkillBar.h"
#include <sa/time.h>
#include <sa/Checked.h>
#include <libcommon/Subsystems.h>

namespace Game {

void Effect::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Effect"].setClass(std::move(kaguya::UserdataMetatable<Effect>()
        .addFunction("GetStartTime", &Effect::GetStartTime)
        .addFunction("GetEndTime", &Effect::GetEndTime)
        .addFunction("GetTicks", &Effect::GetTicks)
        .addFunction("Remove", &Effect::_LuaRemove)
        .addFunction("GetCategory", &Effect::_LuaGetCategory)
        .addFunction("GetRemainingTime", &Effect::GetRemainingTime)
        .addFunction("GetCausingSkill", &Effect::GetCausingSkill)

        .addProperty("Index", &Effect::GetIndex)
        .addProperty("Source", &Effect::GetSource)
        .addProperty("Target", &Effect::GetTarget)
    ));
    // clang-format on
}

Effect::Effect(const AB::Entities::Effect& effect) :
    lua_(*this),
    startTime_(0),
    endTime_(0),
    ticks_(0),
    data_(effect),
    ended_(false),
    cancelled_(false)
{ }

bool Effect::LoadScript(const std::string& fileName)
{
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
        return false;
    if (!lua_.Load(*script))
        return false;

    if (lua_.IsNumber("index"))
        effectIndex_ = lua_["index"];
    else
        LOG_WARNING << "Missing index field in " << fileName << std::endl;
    if (lua_.IsNumber("category"))
        category_ = static_cast<AB::Entities::EffectCategory>(lua_["category"]);
    else
        LOG_WARNING << "Missing category field in " << fileName << std::endl;
    if (lua_.IsBool("isPersistent"))
        persistent_ = lua_["isPersistent"];
    if (lua_.IsBool("internal"))
        internal_ = lua_["internal"];

    return true;
}

Actor* Effect::GetTarget() const
{
    if (auto a = target_.lock())
        return a.get();
    return nullptr;
}

Actor* Effect::GetSource() const
{
    if (auto a = source_.lock())
        return a.get();
    return nullptr;
}

int Effect::_LuaGetCategory() const
{
    return static_cast<int>(category_);
}

bool Effect::_LuaRemove()
{
    if (!cancelled_)
    {
        cancelled_ = true;
        return true;
    }
    return false;
}

void Effect::Update(uint32_t timeElapsed)
{
    if (endTime_ == 0)
        return;
    if (cancelled_ || ended_)
        return;

    auto source = source_.lock();
    auto target = target_.lock();
    lua_.Call("onUpdate", source.get(), target.get(), timeElapsed);
    if (endTime_ <= sa::time::tick())
    {
        lua_.Call("onEnd", source.get(), target.get());
        ended_ = true;
    }
}

uint32_t Effect::GetRemainingTime() const
{
    if (IsPersistent())
        return std::numeric_limits<uint32_t>::max();
    if (endTime_ == 0)
        return 0;
    const int64_t tick = sa::time::tick();
    if (endTime_ < tick)
        return 0;
    return static_cast<uint32_t>(endTime_ - tick);
}

bool Effect::Start(ea::shared_ptr<Actor> source, ea::shared_ptr<Actor> target, uint32_t time)
{
    target_ = target;
    source_ = source;
    if (source)
        sourceId_ = source->id_;
    startTime_ = sa::time::tick();
    if (time == 0)
    {
        if (lua_.IsFunction("getDuration"))
            ticks_ = lua_["getDuration"](source.get(), target.get());
        else
        {
            // Keep at least for one update
            ticks_ = 20;
            LOG_WARNING << data_.script << " script does not have a getDuration() function" << std::endl;
        }
    }
    else
        ticks_ = time;
    endTime_ = sa::ClampedAdd<int64_t>(startTime_, ticks_);
    bool succ = true;
    if (lua_.IsFunction("onStart"))
        succ = lua_["onStart"](source.get(), target.get());
    if (!succ)
        endTime_ = 0;
    return succ;
}

void Effect::DoRemove()
{
    // The Effect was removed before it ended
    if (lua_.IsFunction("onRemove"))
    {
        auto source = source_.lock();
        auto target = target_.lock();
        lua_["onRemove"](source.get(), target.get());
    }
    cancelled_ = true;
}

void Effect::GetSkillRecharge(Skill* skill, uint32_t& recharge)
{
    if (lua_.IsFunction("getSkillRecharge"))
        recharge = lua_["getSkillRecharge"](skill, recharge);
}

void Effect::GetSkillActivation(Skill* skill, uint32_t& activation)
{
    if (lua_.IsFunction("getSkillActivation"))
        activation = lua_["getSkillActivation"](skill, activation);
}

void Effect::GetSkillHpSacrifies(Skill* skill, uint32_t& hp)
{
    if (lua_.IsFunction("getHpSacrifies"))
        hp = lua_["getHpSacrifies"](skill, hp);
}

void Effect::GetSkillCost(Skill* skill,
    int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp)
{
    if (lua_.IsFunction("getSkillCost"))
        kaguya::tie(activation, energy, adrenaline, overcast, hp) =
            lua_["getSkillCost"](skill, activation, energy, adrenaline, overcast, hp);
}

void Effect::GetTargetSkillCost(Actor* source, Skill* skill, int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp)
{
    if (lua_.IsFunction("getTargetSkillCost"))
        kaguya::tie(activation, energy, adrenaline, overcast, hp) =
            lua_["getTargetSkillCost"](source, skill, activation, energy, adrenaline, overcast, hp);
}

void Effect::GetDamage(DamageType type, int32_t& value, bool& critical)
{
    if (lua_.IsFunction("getDamage"))
        kaguya::tie(value, critical) = lua_["getDamage"](static_cast<int>(type), value, critical);
}

void Effect::GetAttackSpeed(Item* weapon, uint32_t& value)
{
    if (lua_.IsFunction("getAttackSpeed"))
        value = lua_["getAttackSpeed"](weapon, value);
}

void Effect::GetAttackDamageType(DamageType& type)
{
    if (lua_.IsFunction("getAttackDamageType"))
        type = lua_["getAttackDamageType"](type);
}

void Effect::GetArmor(DamageType type, int& value)
{
    if (lua_.IsFunction("getArmor"))
        value = lua_["getArmor"](type, value);
}

void Effect::GetArmorPenetration(DamageType damageType, float& value)
{
    if (lua_.IsFunction("getArmorPenetration"))
        value = lua_["getArmorPenetration"](damageType, value);
}

void Effect::GetSourceArmorPenetration(Actor* target, DamageType damageType, float& value)
{
    if (lua_.IsFunction("getSourceArmorPenetration"))
        value = lua_["getSourceArmorPenetration"](target, damageType, value);
}

void Effect::GetAttributeRank(Attribute index, int32_t& value)
{
    if (lua_.IsFunction("getAttributeRank"))
        value = lua_["getAttributeRank"](static_cast<uint32_t>(index), value);
}

void Effect::GetAttackDamage(int32_t& value)
{
    if (lua_.IsFunction("getAttackDamage"))
        value = lua_["getAttackDamage"](value);
}

void Effect::GetRecources(int& maxHealth, int& maxEnergy)
{
    if (lua_.IsFunction("getResources"))
        kaguya::tie(maxHealth, maxEnergy) = lua_["getResources"](maxHealth, maxEnergy);
}

void Effect::GetRegeneration(int& healthRegen, int& energyRegen)
{
    if (lua_.IsFunction("getRegeneration"))
        kaguya::tie(healthRegen, energyRegen) = lua_["getRegeneration"](healthRegen, energyRegen);
}

void Effect::GetMaintainerRegeneration(int& healthRegen, int& energyRegen)
{
    if (lua_.IsFunction("getMaintainerRegeneration"))
        kaguya::tie(healthRegen, energyRegen) = lua_["getMaintainerRegeneration"](healthRegen, energyRegen);
}

void Effect::GetSpeedFactor(float& speedFactor, bool& stop)
{
    if (lua_.IsFunction("getSpeedFactor"))
        kaguya::tie(speedFactor, stop) = lua_["getSpeedFactor"](speedFactor);
}

void Effect::GetSkillDamageBonus(Actor* source, Actor* target, Skill* skill, DamageType type, int& value)
{
    if (lua_.IsFunction("getSkillDamageBonus"))
        value = lua_["getSkillDamageBonus"](source, target, skill, type, value);
}

void Effect::GetEnergyFromSoulReaping(Actor* source, Actor* target, int& value)
{
    if (lua_.IsFunction("getEnergyFromSoulReaping"))
        value = lua_["getEnergyFromSoulReaping"](source, target, value);
}

void Effect::OnSourceAttackStart(Actor* source, Actor* target, bool& value)
{
    // We (the actor with this effect) are attacking target
    if (lua_.IsFunction("onSourceAttackStart"))
        value = lua_["onSourceAttackStart"](source, target);
}

void Effect::OnSourceAttackSuccess(Actor* source, Actor* target, DamageType type, int32_t damage)
{
    // We (the actor with this effect) attacked the target
    if (lua_.IsFunction("onSourceAttackSuccess"))
        lua_["onSourceAttackSuccess"](source, target, type, damage);
}

void Effect::OnTargetAttackSuccess(Actor* source, Actor* target, DamageType type, int32_t damage)
{
    // We were successfully attacked by source
    if (lua_.IsFunction("onTargetAttackSuccess"))
        lua_["onTargetAttackSuccess"](source, target, type, damage);
}

void Effect::OnTargetAttackEnd(Actor* source, Actor* target, DamageType type, int32_t damage, AB::GameProtocol::AttackError& error)
{
    // source attacked us (the actor with this effect), end attack cycle
    if (lua_.IsFunction("onTargetAttackEnd"))
        error = lua_["onTargetAttackEnd"](source, target, type, damage);
}

void Effect::OnTargetAttackBlocked(Actor* source, Actor* target)
{
    // source attacked us but we blocked the attack
    if (lua_.IsFunction("onTargetAttackBlocked"))
        lua_["onTargetAttackBlocked"](source, target);
}

void Effect::OnTargetAttackStart(Actor* source, Actor* target, bool& value)
{
    // source is attacking us (the actor with this effect), attack cycle starts
    if (lua_.IsFunction("onTargetAttackStart"))
        value = lua_["onTargetAttackStart"](source, target);
}

void Effect::OnCanUseSkill(Actor* source, Actor* target, Skill* skill, bool& value)
{
    if (lua_.IsFunction("onCanUseSkill"))
        value = lua_["onCanUseSkill"](source, target, skill);
}

void Effect::OnGettingSkillTarget(Actor* source, Actor* target, Skill* skill, bool& value)
{
    if (lua_.IsFunction("onGettingSkillTarget"))
        value = lua_["onGettingSkillTarget"](source, target, skill);
}

void Effect::OnSkillTargeted(Actor* source, Actor* target, Skill* skill)
{
    if (lua_.IsFunction("onSkillTargeted"))
        lua_["onSkillTargeted"](source, target, skill);
}

void Effect::OnInterruptingAttack(bool& value)
{
    if (lua_.IsFunction("onInterruptingAttack"))
        value = lua_["onInterruptingAttack"]();
}

void Effect::OnInterruptingSkill(Actor* source, Actor* target, AB::Entities::SkillType type, Skill* skill, bool& value)
{
    if (lua_.IsFunction("onInterruptingSkill"))
        value = lua_["onInterruptingSkill"](source, target, type, skill);
}

void Effect::OnKnockingDown(Actor* source, Actor* target, uint32_t time, bool& value)
{
    if (lua_.IsFunction("onKnockingDown"))
        value = lua_["onKnockingDown"](source, target, time);
}

void Effect::OnKnockedDown(Actor* target, uint32_t time)
{
    if (lua_.IsFunction("onKnockedDown"))
        lua_["onKnockedDown"](target, time);
}

void Effect::OnGetCriticalHit(Actor* source, Actor* target, bool& value)
{
    if (lua_.IsFunction("onGetCriticalHit"))
        value = lua_["onGetCriticalHit"](source, target);
}

void Effect::OnAddEffect(Actor* source, Actor* target, Effect* effect, bool& value, uint32_t& time)
{
    if (lua_.IsFunction("onAddEffect"))
        kaguya::tie(value, time) = lua_["onAddEffect"](source, target, effect, time);
}

void Effect::OnRemoveEffect(Actor* target, Effect* effect, bool& value)
{
    if (lua_.IsFunction("onRemoveEffect"))
        value = lua_["onRemoveEffect"](target, effect, value);
}

void Effect::OnHealing(Actor* source, Actor* target, int& value)
{
    if (lua_.IsFunction("onHealing"))
        value = lua_["onHealing"](source, target, value);
}

void Effect::OnHealingTarget(Actor* source, Actor* target, int& value)
{
    if (lua_.IsFunction("onHealingTarget"))
        value = lua_["onHealingTarget"](source, target, value);
}

void Effect::OnStartUseSkill(Actor* source, Actor* target, Skill* skill)
{
    if (lua_.IsFunction("onStartUseSkill"))
        lua_["onStartUseSkill"](source, target, skill);
}

void Effect::OnEndUseSkill(Actor* source, Actor* target, Skill* skill)
{
    if (lua_.IsFunction("onEndUseSkill"))
        lua_["onEndUseSkill"](source, target, skill);
}

void Effect::OnTargetSkillSuccess(Actor* source, Actor* target, Skill* skill)
{
    // Someone used successfully a skill on us
    if (lua_.IsFunction("onTargetSkillSuccess"))
        lua_["onTargetSkillSuccess"](source, target, skill);
}

void Effect::OnWasInterrupted(Actor* source, Actor* target, Skill* skill)
{
    if (lua_.IsFunction("onWasInterrupted"))
        lua_["onWasInterrupted"](source, target, skill);
}

void Effect::OnKnockingDownTarget(Actor* source, Actor* target, uint32_t time, bool& success)
{
    if (lua_.IsFunction("onKnockingDownTarget"))
        success = lua_["onKnockingDownTarget"](source, target, time);
}

void Effect::OnInterruptingTarget(Actor* source, Actor* target, bool& success)
{
    if (lua_.IsFunction("onKnockingDownTarget"))
        success = lua_["onKnockingDownTarget"](source, target);
}

void Effect::OnDealingDamage(Actor* source, Actor* target, DamageType type, int32_t& value, bool& success)
{
    if (lua_.IsFunction("onDealingDamage"))
        kaguya::tie(success, value) = lua_["onDealingDamage"](source, target, type, value);
}

void Effect::OnDied(Actor* source, Actor* killer)
{
    if (lua_.IsFunction("onDied"))
        lua_["onDied"](source, killer);
}

void Effect::OnGettingDamage(Actor* source, Actor* target, Skill* skill, DamageType type, int32_t& value)
{
    if (lua_.IsFunction("onGettingDamage"))
        value = lua_["onGettingDamage"](source, target, skill, type, value);
}

void Effect::OnGotDamage(Actor* source, Actor* target, Skill* skill, DamageType type, int32_t value)
{
    if (lua_.IsFunction("onGotDamage"))
        lua_["onGotDamage"](source, target, skill, type, value);
}

void Effect::OnShadowStep(Actor* source, const Math::Vector3& pos, bool& success)
{
    if (lua_.IsFunction("onShadowStep"))
        success = lua_["onShadowStep"](source, static_cast<Math::StdVector3>(pos));
}

void Effect::OnInterrupted(Actor* source, Actor* target, Skill* skill, bool success)
{
    if (lua_.IsFunction("onInterrupted"))
        lua_["onInterrupted"](source, target, skill, success);
}

void Effect::OnHealedTarget(Actor* source, Actor* target, Skill* skill, int value)
{
    if (lua_.IsFunction("onHealedTarget"))
        lua_["onHealedTarget"](source, target, skill, value);
}

void Effect::OnEnergyZero(Actor* source)
{
    if (lua_.IsFunction("onEnergyZero"))
        lua_["onEnergyZero"](source);
}

void Effect::OnMaintainerEnergyZero(Actor* source)
{
    if (lua_.IsFunction("onMaintainerEnergyZero"))
        lua_["onMaintainerEnergyZero"](source);
}

void Effect::OnActorDied(Actor* actor, Actor* killer)
{
    if (lua_.IsFunction("onActorDied"))
        lua_["onActorDied"](actor, killer);
}

void Effect::OnSpawnedSlave(Actor* actor, Actor* slave)
{
    if (lua_.IsFunction("onSpawnedSlave"))
        lua_["onSpawnedSlave"](actor, slave);
}

void Effect::OnExploitedCorpse(Actor* actor, Actor* corpse)
{
    if (lua_.IsFunction("onExploitedCorpse"))
        lua_["onExploitedCorpse"](actor, corpse);
}

bool Effect::Serialize(sa::PropWriteStream& stream)
{
    stream.Write<uint8_t>(EffectAttrId);
    stream.Write<uint32_t>(data_.index);

    stream.Write<uint8_t>(EffectAttrTicks);
    stream.Write<uint32_t>(ticks_);

    return true;
}

bool Effect::Unserialize(sa::PropReadStream& stream)
{
    uint8_t attr = 0;
    while (stream.Read<uint8_t>(attr) && attr != EffectAttrEnd)
    {
        if (!UnserializeProp(static_cast<EffectAttr>(attr), stream))
            return false;
    }
    return true;
}

bool Effect::UnserializeProp(EffectAttr attr, sa::PropReadStream& stream)
{
    switch (attr)
    {
    case EffectAttrId:
        break;
    case EffectAttrTicks:
        return stream.Read<uint32_t>(ticks_);
    default:
        break;
    }
    return false;
}

Skill* Effect::GetCausingSkill() const
{
    if (effectIndex_ == 0)
        return nullptr;
    if (auto source = source_.lock())
    {
        auto skill = source->skills_->GetSkillBySkillIndex(effectIndex_);
        return skill.get();
    }
    return nullptr;
}

}
