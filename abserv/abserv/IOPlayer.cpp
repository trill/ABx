/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOPlayer.h"
#include "ConfigManager.h"
#include "IOGame.h"
#include "SkillManager.h"
#include "QuestComp.h"
#include "Player.h"
#include "SkillBar.h"
#include "InventoryComp.h"
#include "Item.h"
#include "Npc.h"
#include <AB/Entities/Account.h>
#include <AB/Entities/AccountItemList.h>
#include <AB/Entities/FriendList.h>
#include <AB/Entities/FriendedMe.h>
#include <AB/Entities/GuildMembers.h>
#include <AB/Entities/PlayerItemList.h>
#include <AB/Entities/PlayerQuest.h>
#include <AB/Entities/PlayerQuestList.h>
#include <AB/Entities/Profession.h>
#include <AB/Entities/Quest.h>
#include <uuid.h>
#include <libcommon/DataClient.h>
#include <unordered_set>

namespace IO {

static bool LoadPlayerInventory(Game::Player& player)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    // Equipment
    AB::Entities::EquippedItems equipmenet;
    equipmenet.uuid = player.data_.uuid;
    if (client->Read(equipmenet))
    {
        for (const auto& e : equipmenet.itemUuids)
            player.SetEquipment(e);
    }

    // Inventory
    AB::Entities::InventoryItems inventory;
    inventory.uuid = player.data_.uuid;
    if (client->Read(inventory))
    {
        for (const auto& e : inventory.itemUuids)
            player.SetInventory(e);
    }

    // Chest
    AB::Entities::ChestItems chest;
    chest.uuid = player.account_.uuid;
    if (client->Read(chest))
    {
        for (const auto& e : chest.itemUuids)
            player.SetChest(e);
    }

    return true;
}

static bool SavePlayerInventory(Game::Player& player)
{
    ASSERT(player.inventoryComp_);
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    // Equipment
    player.inventoryComp_->VisitEquipement([&client](Game::Item& item)
    {
        item.concreteItem_.itemStats = item.stats_.ToString();
        client->Update(item.concreteItem_);
        client->Invalidate(item.concreteItem_);
        return Iteration::Continue;
    });

    // Inventory
    player.inventoryComp_->VisitInventory([&client](Game::Item& item)
    {
        item.concreteItem_.itemStats = item.stats_.ToString();
        client->Update(item.concreteItem_);
        client->Invalidate(item.concreteItem_);
        return Iteration::Continue;
    });
    AB::Entities::InventoryItems inventory;
    inventory.uuid = player.data_.uuid;
    client->Invalidate(inventory);

    // Chest
    player.inventoryComp_->VisitChest([&client](Game::Item& item)
    {
        item.concreteItem_.itemStats = item.stats_.ToString();
        client->Update(item.concreteItem_);
        client->Invalidate(item.concreteItem_);
        return Iteration::Continue;
    });

    AB::Entities::ChestItems chest;
    chest.uuid = player.account_.uuid;
    client->Invalidate(chest);
    return true;
}

static bool LoadQuestLog(Game::Player& player)
{
    AB_PROFILE;
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::PlayerQuestList ql;
    ql.uuid = player.data_.uuid;
    if (!client->Read(ql))
        return false;

    ASSERT(player.questComp_);
    Game::Components::QuestComp& questComp = *player.questComp_;
    for (const auto& q : ql.questUuids)
    {
        AB::Entities::PlayerQuest pq;
        pq.uuid = q;
        if (!client->Read(pq))
            continue;
        if (pq.deleted)
            continue;

        AB::Entities::Quest quest;
        quest.uuid = pq.questUuid;
        if (!client->Read(quest))
            continue;

        questComp.Add(quest, std::move(pq));
    }
    return true;
}

static bool SaveQuestLog(Game::Player& player)
{
    auto* client = GetSubsystem<IO::DataClient>();
    ASSERT(player.questComp_);
    Game::Components::QuestComp& questComp = *player.questComp_;
    questComp.VisitQuests([&client](Game::Quest& current)
    {
        current.SaveProgress();
        client->UpdateOrCreate(current.playerQuest_);
        return Iteration::Continue;
    });
    return true;
}

static bool LoadPlayer(Game::Player& player)
{
    AB_PROFILE;
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(player.data_))
    {
        LOG_ERROR << "Error reading player data" << std::endl;
        return false;
    }
    player.account_.uuid = player.data_.accountUuid;
    if (!client->Read(player.account_))
    {
        LOG_ERROR << "Error reading player account" << std::endl;
        return false;
    }

    if (Utils::Uuid::IsEmpty(player.data_.professionUuid))
    {
        LOG_ERROR << "Error primary profession is empty" << std::endl;
        return false;
    }
    player.skills_->prof1_.uuid = player.data_.professionUuid;
    if (!client->Read(player.skills_->prof1_))
    {
        LOG_ERROR << "Error reading player profession1" << std::endl;
        return false;
    }
    if (!Utils::Uuid::IsEmpty(player.data_.profession2Uuid))
    {
        player.skills_->prof2_.uuid = player.data_.profession2Uuid;
        if (!client->Read(player.skills_->prof2_))
        {
            LOG_ERROR << "Error reading player profession2" << std::endl;
            return false;
        }
    }

    if (!player.data_.skillTemplate.empty())
    {
        // After loading professions we can load the skills
        if (!player.skills_->Load(player.data_.skillTemplate, player.account_.type >= AB::Entities::AccountType::Gamemaster))
            LOG_WARNING << "Unable to decode skill template " << player.data_.skillTemplate << std::endl;
    }

    player.inventoryComp_->SetInventorySize(player.data_.inventorySize);
    player.inventoryComp_->SetChestSize(player.account_.chest_size);
    sa::PropReadStream stream;
    stream.Init(player.data_.deathStats.data(), player.data_.deathStats.length());
    Utils::VariantMapRead(player.deathStats_, stream);

    if (!LoadPlayerInventory(player))
        return false;
    if (!LoadQuestLog(player))
        return false;
    return true;
}

bool LoadPlayerByUuid(Game::Player& player, const std::string& uuid)
{
    player.data_.uuid = uuid;
    return LoadPlayer(player);
}

bool SavePlayer(Game::Player& player)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    player.data_.lastLogin = player.loginTime_;
    player.data_.lastLogout = player.logoutTime_;
    player.data_.profession2 = player.skills_->prof2_.abbr;
    player.data_.profession2Uuid = player.skills_->prof2_.uuid;
    player.data_.skillTemplate = player.skills_->Encode();
    player.data_.onlineTime += static_cast<int64_t>((player.logoutTime_ - player.loginTime_) / 1000);
    {
        sa::PropWriteStream stream;
        Utils::VariantMapWrite(player.deathStats_, stream);
        size_t ssize = 0;
        const char* s = stream.GetStream(ssize);
        player.data_.deathStats = std::string(s, ssize);
    }
    if (auto* c = player.GetCompanion())
    {
        // Whenever we have a companion with us, we must save the progress of it
        sa::PropWriteStream stream;
        const auto ci = c->GetCompanionInfo();
        stream.WriteString(ci.script);
        stream.Write(ci.level);
        stream.Write(ci.xp);
        stream.Write(ci.sp);
        stream.WriteString(ci.name);
        size_t ssize = 0;
        const char* s = stream.GetStream(ssize);
        player.data_.companion = std::string(s, ssize);
    }

    if (!client->Update(player.data_))
        return false;
    if (!SavePlayerInventory(player))
        return false;
    if (!SaveQuestLog(player))
        return false;
    return true;
}

}
