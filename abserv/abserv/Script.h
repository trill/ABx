/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Asset.h"

namespace kaguya {
class State;
}

namespace Game {

class Script final : public IO::Asset
{
private:
    ea::vector<char> buffer_;
public:
    Script();
    ~Script() override;

    ea::vector<char>& GetBuffer()
    {
        return buffer_;
    }

    /// Execute the script
    bool Execute(kaguya::State& luaState) const;
};

}
