/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <kaguya/kaguya.hpp>

namespace Game {

class Script;

class LuaObject
{
private:
    bool initialized_{ false };
    kaguya::State state_;
    void Register();
public:
    template<typename T>
    explicit LuaObject(T& owner)
    {
        state_.gc().setmode(kaguya::State::GCType::Mode::Generational);
        Register();
        state_["self"] = &owner;
    }

    bool Load(const Script& script);

    bool IsFunction(const std::string& name);
    bool IsVariable(const std::string& name);
    bool IsString(const std::string& name);
    bool IsBool(const std::string& name);
    bool IsNumber(const std::string& name);
    bool IsNil(const std::string& name);
    bool IsTable(const std::string& name);

    const kaguya::State& GetState() const { return state_; }
    kaguya::TableKeyReferenceProxy<std::string> operator[](const std::string& str)
    {
        return state_[str];
    }
    kaguya::TableKeyReferenceProxy<const char*> operator[](const char* str)
    {
        return state_[str];
    }

    template<typename... _CArgs>
    void Call(const std::string& name, _CArgs&& ... _Args)
    {
        if (IsFunction(name))
            state_[name](std::forward<_CArgs>(_Args)...);
    }
};

}
