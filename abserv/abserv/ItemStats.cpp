/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ItemStats.h"

namespace Game {

ItemStats::ItemStats() = default;

DamageType ItemStats::GetDamageType() const
{
    return static_cast<DamageType>(GetValue<int>(ItemStatIndex::DamageType, (int)DamageType::Unknown));
}

int32_t ItemStats::GetMinDamage() const
{
    return GetValue<int32_t>(ItemStatIndex::MinDamage, 0);
}

int32_t ItemStats::GetMaxDamage() const
{
    return GetValue<int32_t>(ItemStatIndex::MaxDamage, 0);
}

uint32_t ItemStats::GetRequirement() const
{
    return GetValue<uint32_t>(ItemStatIndex::AttributeValue, 0);
}

Attribute ItemStats::GetAttribute() const
{
    return static_cast<Attribute>(GetValue<int>(ItemStatIndex::Attribute, (int)Attribute::None));
}

int ItemStats::GetArmor(DamageType damageType) const
{
    int value = 0;
    // General
    value += GetValue(ItemStatIndex::Armor, 0);

    switch (damageType)
    {
    case DamageType::Unknown:
        break;
    case DamageType::Fire:
    {
        value += GetValue(ItemStatIndex::ArmorElemental, 0);
        value += GetValue(ItemStatIndex::ArmorFire, 0);
        break;
    }
    case DamageType::Cold:
    {
        value += GetValue(ItemStatIndex::ArmorElemental, 0);
        value += GetValue(ItemStatIndex::ArmorCold, 0);
        break;
    }
    case DamageType::Lightning:
    {
        value += GetValue(ItemStatIndex::ArmorElemental, 0);
        value += GetValue(ItemStatIndex::ArmorLightning, 0);
        break;
    }
    case DamageType::Earth:
    {
        value += GetValue(ItemStatIndex::ArmorElemental, 0);
        value += GetValue(ItemStatIndex::ArmorEarth, 0);
        break;
    }
    case DamageType::Blunt:
    case DamageType::Piercing:
    case DamageType::Slashing:
    {
        value += GetValue(ItemStatIndex::ArmorPhysical, 0);
        break;
    }
    case DamageType::Holy:
    case DamageType::Shadow:
    case DamageType::Chaos:
    case DamageType::LifeDrain:
    case DamageType::Typeless:
    case DamageType::Dark:
        // Ignoring armor
        break;
    default:
        ASSERT_FALSE();
    }

    return value;
}

uint32_t ItemStats::GetAttributeIncrease(Attribute index) const
{
    return GetValue(static_cast<size_t>(ItemStatIndex::AttributeOffset) + static_cast<size_t>(index), 0u);
}

int ItemStats::GetHealth() const
{
    return GetValue<int>(ItemStatIndex::Health, 0);
}

int ItemStats::GetEnergy() const
{
    return GetValue<int>(ItemStatIndex::Energy, 0);
}

int ItemStats::GetHealthRegen() const
{
    return GetValue<int>(ItemStatIndex::HealthRegen, 0);
}

int ItemStats::GetEnergyRegen() const
{
    return GetValue<int>(ItemStatIndex::EnergyRegen, 0);
}

int ItemStats::GetUsages() const
{
    return GetValue<int>(ItemStatIndex::Usages, 0);
}

void ItemStats::DescreaseUsages()
{
    int value = GetValue<int>(ItemStatIndex::Usages, 0);
    if (value < 1)
        return;
    SetValue<int>(ItemStatIndex::Usages, value - 1);
}

bool ItemStats::Load(sa::PropReadStream& stream)
{
    return Utils::VariantMapRead(stats_, stream);
}

void ItemStats::Save(sa::PropWriteStream& stream) const
{
    Utils::VariantMapWrite(stats_, stream);
}

bool ItemStats::LoadFromString(const std::string& value)
{
    sa::PropReadStream stream;
    stream.Init(value.data(), value.length());
    return Load(stream);
}

std::string ItemStats::ToString() const
{
    sa::PropWriteStream stream;
    Save(stream);
    size_t ssize = 0;
    const char* s = stream.GetStream(ssize);
    return std::string(s, ssize);
}

}
