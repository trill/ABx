/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Model.h"
#include "IOAsset.h"

namespace IO {

class IOModel final : public IOAssetImpl<Game::Model>
{
public:
    /// Import 3D Model file exported with the import program
    bool Import(Game::Model& asset, const std::string& name) override;
};

}
