/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HealComp.h"
#include "Actor.h"
#include "Skill.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <sa/time.h>
#include <libcommon/NetworkMessage.h>

namespace Game {
namespace Components {

void HealComp::Healing(Actor* source, uint32_t index, int value)
{
    if (owner_.IsDead())
        return;

    healings_.push_back({ source ? source->id_ : 0, index, value, sa::time::tick() });
    owner_.resourceComp_->SetHealth(Components::SetValueType::Increase, value);
}

void HealComp::Write(MessageStream& message)
{
    if (healings_.empty())
        return;
    for (const auto& d : healings_)
    {
        AB::Packets::Server::ObjectHealed packet = {
            owner_.id_,
            d.actorId,
            static_cast<uint16_t>(d.index),
            static_cast<int16_t>(d.value)
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectHealed, packet);
    }
    healings_.clear();
}

}
}
