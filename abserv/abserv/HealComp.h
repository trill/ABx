/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class Skill;
class MessageStream;

namespace Components {

class HealComp
{
    NON_COPYABLE(HealComp)
    NON_MOVEABLE(HealComp)
private:
    struct HealItem
    {
        uint32_t actorId;
        // Skill or Effect index
        uint32_t index;
        int value;
        int64_t tick;
    };
    Actor& owner_;
    ea::vector<HealItem> healings_;
public:
    HealComp() = delete;
    explicit HealComp(Actor& owner) :
        owner_(owner)
    { }
    ~HealComp() = default;

    void Healing(Actor* source, uint32_t index, int value);
    void Update(uint32_t /* timeElapsed */) { }
    void Write(MessageStream& message);
};

}
}

