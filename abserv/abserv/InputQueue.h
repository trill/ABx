/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/Variant.h>
#include <eastl.hpp>
#include <EASTL/queue.h>
#include <mutex>

namespace Game {

enum class InputType
{
    None = 0,
    Move,
    Turn,
    UseSkill,
    Attack,
    Cancel,        // Cancel skill/attack/follow
    Select,
    ClickObject,
    Direction,
    Command,
    Goto,
    Follow,
    SetState,
    Interact,
    PingPos,
    PingInfo,
    RemoveMaintainableEnch
};

enum InputDataType : size_t
{
    InputDataDirection = 1,
    InputDataSkillIndex = 3,    // The index of the skill in the skill bar
    InputDataObjectId = 4,
    InputDataObjectId2 = 5,
    InputDataCommandType = 6,
    InputDataCommandData = 7,
    InputDataVertexX = 8,
    InputDataVertexY = 9,
    InputDataVertexZ = 10,
    InputDataState = 11,
    InputDataPingTarget = 12,
    InputDataSuppress = 13,
    InputTypePingInfoType = 14,
    InputTypePingInfoData = 15,
    InputTypePingInfoData2 = 16
};

struct InputItem
{
    InputType type;
    Utils::VariantMap data;
};

class InputQueue
{
private:
    ea::queue<InputItem> queue_;
    std::mutex lock_;
public:
    InputQueue();
    ~InputQueue();

    void Add(InputType type, Utils::VariantMap&& data)
    {
        // Network and Dispatcher Threat
        std::scoped_lock lock(lock_);
        queue_.push({ type, std::move(data) });
    }
    void Add(InputType type)
    {
        // Network and Dispatcher Threat
        std::scoped_lock lock(lock_);
        queue_.push({ type, Utils::VariantMap_Empty });
    }
    bool Get(InputItem& item);
};

}

