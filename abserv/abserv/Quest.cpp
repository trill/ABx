/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Quest.h"
#include "DataProvider.h"
#include "ItemFactory.h"
#include "Player.h"
#include "ProgressComp.h"
#include "Script.h"
#include "Game.h"
#include "MessageStream.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <sa/time.h>
#include <libcommon/DataClient.h>
#include <libcommon/Subsystems.h>
#include <libcommon/NetworkMessage.h>

namespace Game {

void Quest::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Quest"].setClass(std::move(kaguya::UserdataMetatable<Quest>()
        .addFunction("GetOwner", &Quest::_LuaGetOwner)
        .addFunction("IsCompleted", &Quest::IsCompleted)
        .addFunction("IsRewarded", &Quest::IsRewarded)
        .addFunction("GetVarString", &Quest::_LuaGetVarString)
        .addFunction("SetVarString", &Quest::_LuaSetVarString)
        .addFunction("GetVarNumber", &Quest::_LuaGetVarNumber)
        .addFunction("SetVarNumber", &Quest::_LuaSetVarNumber)
    ));
    // clang-format on
}

Quest::Quest(Player& owner,
    const AB::Entities::Quest& q,
    AB::Entities::PlayerQuest&& playerQuest) :
    lua_(*this),
    owner_(owner),
    index_(q.index),
    repeatable_(q.repeatable),
    playerQuest_(std::move(playerQuest))
{
    owner_.SubscribeEvent<OnKilledFoeSignature>(OnKilledFoeEvent, std::bind(&Quest::OnKilledFoe, this, std::placeholders::_1, std::placeholders::_2));
    owner_.SubscribeEvent<OnPostSpawnSignature>(OnPostSpawnEvent, std::bind(&Quest::OnPostSpawn, this, std::placeholders::_1));
    LoadProgress();
}

Quest::~Quest() = default;

bool Quest::LoadScript(const std::string& fileName)
{
    if (fileName.empty())
        return false;

    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
        return false;
    if (!lua_.Load(*script))
        return false;
    return true;
}

void Quest::LoadProgress()
{
    internalRewarded_ = playerQuest_.rewarded;
    internalDeleted_ = playerQuest_.deleted;
    sa::PropReadStream stream;
    stream.Init(playerQuest_.progress.data(), playerQuest_.progress.length());
    if (!Utils::VariantMapRead(variables_, stream))
    {
        LOG_WARNING << "Error loading Quest progress" << std::endl;
    }
}

void Quest::SaveProgress()
{
    sa::PropWriteStream stream;
    Utils::VariantMapWrite(variables_, stream);
    size_t ssize = 0;
    const char* s = stream.GetStream(ssize);
    playerQuest_.progress = std::string(s, ssize);
}

bool Quest::CollectReward()
{
    if (internalRewarded_ || playerQuest_.rewarded)
        return false;

    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Quest q;
    q.uuid = playerQuest_.questUuid;
    if (!client->Read(q))
        return false;
    owner_.progressComp_->AddXp(q.rewardXp);
    if (q.rewardMoney > 0)
    {
        if (!owner_.AddMoney(static_cast<uint32_t>(q.rewardMoney)))
            return false;
    }

    auto* factory = GetSubsystem<ItemFactory>();
    for (const auto& item : q.rewardItems)
    {
        uint32_t id = factory->CreatePlayerItem(owner_, item, AB::Entities::StoragePlace::Inventory, 1);
        if (id != 0)
            owner_.AddToInventory(id);
    }

    internalRewarded_ = true;
    playerQuest_.rewardTime = sa::time::tick();
    return true;
}

void Quest::Update(uint32_t timeElapsed)
{
    if (playerQuest_.deleted)
        return;

    lua_.Call("onUpdate", timeElapsed);
}

void Quest::Write(MessageStream& message)
{
    if (playerQuest_.deleted)
        return;

    using namespace AB::GameProtocol;
    if (internalDeleted_ != playerQuest_.deleted)
    {
        playerQuest_.deleted = internalDeleted_;
        AB::Packets::Server::QuestDeleted packet = {
            index_,
            playerQuest_.deleted
        };
        message.AddPacket(ServerPacketType::QuestDeleted, packet);
    }
    if (internalRewarded_ != playerQuest_.rewarded)
    {
        playerQuest_.rewarded = internalRewarded_;
        AB::Packets::Server::QuestRewarded packet = {
            index_,
            playerQuest_.rewarded
        };
        message.AddPacket(ServerPacketType::QuestRewarded, packet);
    }
}

Player* Quest::_LuaGetOwner()
{
    return &owner_;
}

std::string Quest::_LuaGetVarString(const std::string& name) const
{
    return GetVar(name).GetString();
}

void Quest::_LuaSetVarString(const std::string& name, const std::string& value)
{
    SetVar(name, Utils::Variant(value));
}

float Quest::_LuaGetVarNumber(const std::string& name) const
{
    return GetVar(name).GetFloat();
}

void Quest::_LuaSetVarNumber(const std::string& name, float value)
{
    SetVar(name, Utils::Variant(value));
}

const Utils::Variant& Quest::GetVar(const std::string& name) const
{
    auto it = variables_.find(sa::StringHash(name));
    if (it != variables_.end())
        return (*it).second;
    return Utils::Variant::Empty;
}

void Quest::SetVar(const std::string& name, const Utils::Variant& val)
{
    variables_[sa::StringHash(name)] = val;
}

void Quest::OnKilledFoe(Actor* foe, Actor* killer)
{
    lua_.Call("onKilledFoe", foe, killer);
}

void Quest::OnPostSpawn(Game* game)
{
    if (playerQuest_.deleted)
        return;

    lua_.Call("onPostSpawn", game);
}

bool Quest::IsActive() const
{
    if (playerQuest_.deleted)
        return false;
    return !IsRewarded();
}

bool Quest::Delete()
{
    if (internalDeleted_ || playerQuest_.deleted ||
        internalRewarded_ || playerQuest_.rewarded)
        return false;
    internalDeleted_ = true;
    return true;
}

}
