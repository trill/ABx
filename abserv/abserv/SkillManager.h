/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <AB/Entities/Skill.h>

namespace Game {

class Skill;

class SkillManager
{
private:
    ea::unordered_map<uint32_t, AB::Entities::Skill> skillCache_;
public:
    SkillManager();
    ~SkillManager() = default;

    ea::shared_ptr<Skill> Get(uint32_t index);
};

}
