/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Model.h"
#include <libmath/Mesh.h>

namespace Game {

Model::Model() = default;
Model::~Model() = default;

void Model::SetShape(ea::unique_ptr<Math::Mesh>&& shape)
{
    shape_ = std::move(shape);
    if (shape_)
        boundingBox_.Merge(shape_->vertexData_.data(), shape_->vertexCount_);
}

Math::Mesh* Model::GetMesh() const
{
    return shape_.get();
}

}
