/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <libcommon/NetworkMessage.h>
#include <sa/Events.h>
#include <sa/StringHash.h>
#include <sa/TypeName.h>

namespace Game {
class Game;
class Player;
}

namespace Net {

// Oh, well, I'm not sure about this...

class MessageFilter
{
private:
    using FilterEvents = sa::Events<
#define ENUMERATE_SERVER_PACKET_CODE(v) bool(const Game::Game&, const Game::Player&, AB::Packets::Server::v&),
        ENUMERATE_SERVER_PACKET_CODES
#undef ENUMERATE_SERVER_PACKET_CODE
        bool(void)
    >;
    FilterEvents events_;
    template<typename T>
    static void Insert(NetworkMessage& dest, AB::GameProtocol::ServerPacketType type, T& packet)
    {
        dest.AddByte(type);
        AB::Packets::Add(packet, dest);
    }
public:
    MessageFilter()
    { }

    // Return true to keep the packet, false to drop it
    template<typename T, typename Callback>
    size_t Subscribe(Callback&& callback)
    {
        constexpr size_t id = sa::StringHash(sa::TypeName<T>::Get());
        return events_.Subscribe<bool(const Game::Game&, const Game::Player&, T&)>(id, std::move(callback));
    }
    void Execute(const Game::Game& game, const Game::Player& player, const NetworkMessage& source, NetworkMessage& dest);
};

}
