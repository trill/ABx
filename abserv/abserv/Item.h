/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ItemStats.h"
#include <AB/Entities/ConcreteItem.h>
#include <AB/Entities/Item.h>
#include <libshared/Attributes.h>
#include <libshared/Damage.h>
#include <eastl.hpp>
#include <kaguya/kaguya.hpp>
#include <sa/Bits.h>
#include <sa/Noncopyable.h>
#include <sa/Transaction.h>
#include "LuaObject.h"

namespace Game {

class Actor;
class Skill;

enum class ItemUpgrade
{
    Prefix = 0,          // Insignia for armor, prefix for weapon
    Suffix,              // Rune for armor, suffix for weapon
    Inscription,         // For lead/off hand weapons
    __Count
};

enum class EquipPos : uint32_t
{
    None = 0,
    ArmorHead = 1,
    ArmorChest,
    ArmorHands,
    ArmorLegs,
    ArmorFeet,

    WeaponLeadHand,
    WeaponOffHand,
    WeaponTwoHanded
};

typedef ea::map<ItemUpgrade, uint32_t> UpgradesMap;
typedef ea::map<EquipPos, uint32_t> EquipmentMap;

class Item
{
    NON_COPYABLE(Item)
private:
    LuaObject lua_;
    UpgradesMap upgrades_;
    int32_t baseMinDamage_{ 0 };
    int32_t baseMaxDamage_{ 0 };
    void CreateGeneralStats(uint32_t level, bool maxStats);
    void CreateAttributeStats(uint32_t level, bool maxStats);
    void CreateInsigniaStats(uint32_t level, bool maxStats);
    void CreateWeaponStats(uint32_t level, bool maxStats);
    void CreateFocusStats(uint32_t level, bool maxStats);
    void CreateShieldStats(uint32_t level, bool maxStats);
    void CreateConsumeableStats(uint32_t level, bool maxStats);
    int _LuaGetStoragePlace() const;
    int _LuaGetStoragePos() const;
    std::string _LuaGetName() const;
public:
    static void RegisterLua(kaguya::State& state);

    explicit Item(AB::Entities::Item item);
    ~Item();

    // Careful this will destroy the item.
    void RemoveFromCache();
    bool LoadConcrete(const AB::Entities::ConcreteItem& item);
    bool LoadScript(const std::string& fileName);
    bool GenerateConcrete(AB::Entities::ConcreteItem& ci, uint32_t level, bool maxStats, const std::string& stats = "");
    bool IsPossibleAttribute(Attribute attribute) const;
    void Update(uint32_t timeElapsed);
    /// Upgrade this item
    Item* SetUpgrade(ItemUpgrade type, uint32_t id);
    Item* GetUpgrade(ItemUpgrade type);
    void RemoveUpgrade(ItemUpgrade type);
    EquipPos GetEquipPos() const;
    float GetWeaponRange() const;
    uint32_t GetWeaponAttackSpeed() const;
    /// Return true when the weapon is throwing projectiles
    bool IsWeaponProjectile() const;
    void GetWeaponDamageType(DamageType& value) const;
    void GetWeaponDamage(int32_t& value, bool critical);
    Attribute GetWeaponAttribute() const;
    uint32_t GetWeaponRequirement() const;
    void GetArmor(DamageType damageType, int& value) const;
    void GetArmorPenetration(float& value) const;
    void GetResources(int& maxHealth, int& maxEnergy);
    void GetRegeneration(int& healthRegen, int& energyRegen);
    void GetSkillCost(Skill* skill, int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    void GetAttributeRank(Attribute index, uint32_t& value);
    void OnEquip(Actor* target);
    void OnUnequip(Actor* target);
    void OnDoubleClick(Actor* source);
    /// Get value of this item with all mods
    uint32_t GetValue() const;
    // Return random material from this item. item index - count
    ea::pair<uint32_t, uint32_t> GetSalvageMaterial() const;

    uint32_t GetIndex() const { return data_.index; }
    AB::Entities::ItemType GetType() const;
    bool IsStackable() const { return AB::Entities::IsItemStackable(data_.itemFlags); }
    bool IsTradeable() const { return AB::Entities::IsItemTradeable(data_.itemFlags); }
    bool IsResellable() const { return AB::Entities::IsItemResellable(data_.itemFlags); }
    bool IsSalvageable() const { return AB::Entities::IsItemSalvageable(data_.itemFlags); }
    bool IsArmor() const
    {
        return AB::Entities::IsArmorItem(data_.type);
    }
    bool IsWeapon() const
    {
        return AB::Entities::IsWeaponItem(data_.type);
    }
    bool IsConsumeable() const
    {
        return AB::Entities::IsConsumeableItem(data_.type);
    }
    bool Consume();
    template<typename Callback>
    void Visit(Callback&& callback)
    {
        callback(*this);
        for (size_t i = 0; i < static_cast<size_t>(ItemUpgrade::__Count); ++i)
        {
            if (auto* upgrade = GetUpgrade(static_cast<ItemUpgrade>(i)))
                callback(*upgrade);
        }
    }

    uint32_t id_{ std::numeric_limits<uint32_t>::min() };
    AB::Entities::Item data_;
    AB::Entities::ConcreteItem concreteItem_;
    ItemStats stats_;
};

class ItemTransaction
{
private:
    sa::Transaction<AB::Entities::ConcreteItem> ct_;
    sa::Transaction<ItemStats> st_;
public:
    ItemTransaction(Item& item) :
        ct_(item.concreteItem_),
        st_(item.stats_)
    {
    }
    void Commit()
    {
        ct_.Commit();
        st_.Commit();
    }
};

}
