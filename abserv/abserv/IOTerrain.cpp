/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOTerrain.h"
#include <fstream>
#include <sa/StringTempl.h>
#include <pugixml.hpp>
#include "DataProvider.h"
#include <libcommon/Subsystems.h>

namespace IO {

bool IOTerrain::Import(Game::Terrain& asset, const std::string& name)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_file(name.c_str());
    if (result.status != pugi::status_ok)
    {
        LOG_ERROR << "Error loading file " << name << ": " << result.description() << std::endl;
        return false;
    }
    const pugi::xml_node indexNode = doc.child("terrain");
    if (!indexNode)
    {
        LOG_ERROR << "File " << name << " does not have a terrain node" << std::endl;
        return false;
    }

    auto* dataProv = GetSubsystem<IO::DataProvider>();

    using namespace sa::literals;
    // Let's just assume heightmap files are relative to terrain files
    std::string dir = Utils::ExtractFileDir(name);
    for (const auto& fileNode : indexNode.children("file"))
    {
        const pugi::xml_attribute& typeAttr = fileNode.attribute("type");
        const size_t typeHash = sa::StringHash(typeAttr.as_string());
        const pugi::xml_attribute& srcAttr = fileNode.attribute("src");
        switch (typeHash)
        {
        case "Heightmap.0"_Hash:
            if (!asset.GetHeightMap())
                asset.SetHeightMap(dataProv->GetAsset<Game::HeightMap>(Utils::ConcatPath(dir, srcAttr.as_string())));
            break;
        case "Heightmap.1"_Hash:
            if (!asset.GetHeightMap2())
                asset.SetHeightMap2(dataProv->GetAsset<Game::HeightMap>(Utils::ConcatPath(dir, srcAttr.as_string())));
            break;
        default:
            LOG_WARNING << "Unknown type " << typeAttr.as_string() << std::endl;
            continue;
        }
    }
    // We need at least one heightmap
    const auto* hm1 = asset.GetHeightMap();
    if (!hm1)
        return false;
    const auto* hm2 = asset.GetHeightMap2();
    if (hm1 && hm2)
    {
        if (hm1->heightData_.size() != hm2->heightData_.size())
        {
            LOG_WARNING << "Height map layers have different sizes, " << hm1->heightData_.size() <<
                " != " << hm2->heightData_.size() << std::endl;
        }
    }
    asset.Initialize();

    return true;
}

}
