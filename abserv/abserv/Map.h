/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "NavigationMesh.h"
#include <libmath/Octree.h>
#include "Terrain.h"
#include <libmath/Vector3.h>
#include <pugixml.hpp>
#include "TerrainPatch.h"
#include <eastl.hpp>
#include "Scene.h"

namespace Game {

class Game;

/// Holds all the map data, static objects, NavMesh.
class Map
{
private:
    ea::weak_ptr<Game> game_;
    // TerrainPatches are also owned by the game
    ea::vector<ea::shared_ptr<TerrainPatch>> patches_;
    SpawnPoint CorrectedSpanwPoint(const SpawnPoint& world) const;
public:
    Map(ea::shared_ptr<Game> game);
    Map() = delete;
    ~Map();

    void CreatePatches();
    /// Return patch by index.
    TerrainPatch* GetPatch(size_t index) const;
    /// Return patch by patch coordinates.
    TerrainPatch* GetPatch(int x, int z) const;
    size_t GetPatchesCount() const
    {
        return patches_.size();
    }
    float GetTerrainHeight(const Math::Vector3& world) const;
    float GetTerrainHeight1(const Math::Vector3& world) const;
    float GetTerrainHeight2(const Math::Vector3& world) const;
    void UpdatePointHeight(Math::Vector3& world) const;
    ea::shared_ptr<Game> GetGame()
    {
        return game_.lock();
    }

    void AddGameObject(ea::shared_ptr<GameObject> object);
    void UpdateOctree(uint32_t delta);
    SpawnPoint GetFreeSpawnPoint() const;
    SpawnPoint GetFreeSpawnPoint(const std::string& group) const;
    SpawnPoint GetFreeSpawnPoint(const ea::vector<SpawnPoint>& points) const;
    SpawnPoint GetSpawnPoint(const std::string& group) const;
    ea::vector<SpawnPoint> GetSpawnPoints(const std::string& group) const;

    /// Find a path between world space points. Return non-empty list of points if successful.
    /// Extents specifies how far off the navigation mesh the points can be.
    bool FindPath(ea::vector<Math::Vector3>& dest, const Math::Vector3& start, const Math::Vector3& end,
        const Math::Vector3& extends = Math::Vector3_One, const dtQueryFilter* filter = nullptr) const;
    Math::Vector3 FindNearestPoint(const Math::Vector3& point, const Math::Vector3& extents,
        const dtQueryFilter* filter = nullptr, dtPolyRef* nearestRef = nullptr) const;
    bool CanStepOn(const Math::Vector3& point, const Math::Vector3& extents = Math::Vector3_One,
        const dtQueryFilter* filter = nullptr, dtPolyRef* nearestRef = nullptr, Math::Vector3* nearestPoint = nullptr) const;

    std::string name_;
    std::string directory_;
    ea::shared_ptr<Navigation::NavigationMesh> navMesh_;
    ea::shared_ptr<Terrain> terrain_;
    ea::unique_ptr<Math::Octree> octree_;
    ea::shared_ptr<Scene> scene_;
};

}
