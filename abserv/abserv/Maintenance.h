/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <mutex>
#include "Config.h"

class Maintenance
{
private:
    enum class Status
    {
        Runnig,
        Terminated
    };
    Status status_;
    void CleanCacheTask();
    void CleanGamesTask();
    void CleanPlayersTask();
    void CleanChatsTask();
    void SavePlayersTask();
    void UpdateServerLoadTask();
    void FileWatchTask();
    void CheckAutoTerminate();
    void UpdateAiServer();
    void UpdateGameInstances();
    std::mutex mutex_;
public:
    Maintenance() :
        status_(Status::Terminated)
    {}
    ~Maintenance() = default;

    void Run();
    void Stop();
    uint32_t aiUpdateInterval_{ AI_SERVER_UPDATE_INTERVAL };
};

