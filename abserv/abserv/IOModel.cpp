/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOModel.h"
#include <libmath/Mesh.h>
#include <libio/Mesh.h>
#include <eastl.hpp>

namespace IO {

bool IOModel::Import(Game::Model& asset, const std::string& name)
{
    AB_PROFILE;

    asset.SetShape(ea::make_unique<Math::Mesh>());
    return IO::LoadMesh(name, *asset.GetMesh(), asset.boundingBox_);
}

}
