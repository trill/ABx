/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Game {

enum SkillEffect : uint32_t
{
    SkillEffectNone = 0,
    SkillEffectResurrect = 1 << 1,
    SkillEffectHeal = 1 << 2,
    SkillEffectProtect = 1 << 3,
    SkillEffectDamage = 1 << 4,
    SkillEffectSpeed = 1 << 5,
    SkillEffectInterrupt = 1 << 6,
    SkillEffectGainEnergy = 1 << 7,
    SkillEffectRemoveEnchantment = 1 << 8,
    SkillEffectRemoveHex = 1 << 9,
    SkillEffectRemoveStance = 1 << 10,
    SkillEffectCounterInterrupt = 1 << 11,
    SkillEffectCounterKD = 1 << 12,
    SkillEffectBlock = 1 << 13,
    SkillEffectSummonMinion = 1 << 14,
    SkillEffectSnare = 1 << 15,
    SkillEffectEnergyLoss = 1 << 16,
    SkillEffectRemoveCondition = 1 << 17,
    SkillEffectIncAttributes = 1 << 18,
    SkillEffectKnockDown = 1 << 19,
    SkillEffectFasterCast = 1 << 20,
    SkillEffectDisableSkill = 1 << 21,
    SkillEffectDecAttributes = 1 << 22,
};

enum SkillEffectTarget : uint32_t
{
    SkillTargetNone = 0,
    SkillTargetSelf = 1 << 1,
    SkillTargetTarget = 1 << 2,              // Selected target
    SkillTargetAoe = 1 << 3,
    SkillTargetParty = 1 << 4,
};

// Skills may or may not require a target
enum SkillTargetType : uint32_t
{
    SkillTargetTypeNone = 0,
    SkillTargetTypeSelf = 1,
    SkillTargetTypeAllyAndSelf = 2,
    SkillTargetTypeAllyWithoutSelf = 3,
    SkillTargetTypeFoe = 4,
    SkillTargetTypeAny = 5
};

enum class CostType
{
    Activation,
    Energy,
    Adrenaline,
    HpSacrify,
    Recharge,
};

}
