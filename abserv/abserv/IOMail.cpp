/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOMail.h"
#include <libcommon/MessageClient.h>
#include <sa/time.h>
#include <libcommon/DataClient.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <libaccount/Character.h>
#include "ItemFactory.h"

namespace IO {

bool LoadMailList(AB::Entities::MailList& ml, const std::string& accountUuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    ml.uuid = accountUuid;
    return client->Read(ml);
}

std::optional<std::string> SendMailToPlayer(const std::string& playerName, const std::string& fromAcc,
    const std::string& fromName,
    const std::string& subject, const std::string& message,
    const std::vector<std::string>& attachments)
{
    AB::Entities::Character ch;
    ch.name = playerName;

    // Get recipient account
    if (!Account::LoadCharacter(ch))
    {
        LOG_DEBUG << "Character not found " << playerName << std::endl;
        return {};
    }

    return SendMailToAccount(ch.accountUuid, fromAcc, fromName, playerName, subject, message, attachments);
}

std::optional<std::string> SendMailToAccount(const std::string& accountUuid, const std::string& fromAcc,
    const std::string& fromName, const std::string& toName,
    const std::string& subject, const std::string& message,
    const std::vector<std::string>& attachments)
{
    // Can not send mails to self
    if (Utils::Uuid::IsEqual(accountUuid, fromAcc))
        return {};
    // Can not send mail to players that ignore me
    if (Account::IsIgnoringMe(fromAcc, accountUuid))
        return {};

    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::MailList ml;
    ml.uuid = accountUuid;
    // Check quota
    if (!client->Read(ml))
    {
        LOG_ERROR << "Failed to read mail box from " << accountUuid << std::endl;
        return {};
    }
    if (ml.mails.size() >= AB::Entities::MAX_MAIL_COUNT)
        return {};

    AB::Entities::Mail m;
    m.uuid = Utils::Uuid::New();
    m.fromAccountUuid = fromAcc;
    m.toAccountUuid = accountUuid;
    m.fromName = fromName;
    m.toName = toName;
    m.subject = subject;
    m.message = message;
    m.created = sa::time::tick();
    m.attachments = attachments;

    bool ret = client->Create(m);
    if (ret)
    {
        // Add to recipients mail list
        ml.mails.emplace_back(
            m.uuid,
            fromName,
            subject,
            m.created,
            false,
            (uint8_t)attachments.size()
        );
        client->Update(ml);

        // Notify receiver
        Net::MessageClient* msgCli = GetSubsystem<Net::MessageClient>();
        Net::MessageMsg msg;
        msg.type_ = Net::MessageType::NewMail;
        sa::PropWriteStream stream;
        stream.WriteString(accountUuid);
        msg.SetPropStream(stream);
        msgCli->Write(msg);
    }
    return m.uuid;
}

bool ReadMail(AB::Entities::Mail& mail)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    bool ret = client->Read(mail);
    if (ret)
    {
        mail.isRead = true;
        client->Update(mail);
        // Update read flag in mail list
        AB::Entities::MailList ml;
        ml.uuid = mail.toAccountUuid;
        if (client->Read(ml))
        {
            auto it = std::find_if(ml.mails.begin(), ml.mails.end(), [&mail](const AB::Entities::MailHeader& current)
            {
                return Utils::Uuid::IsEqual(current.uuid, mail.uuid);
            });
            if (it != ml.mails.end())
            {
                (*it).isRead = true;
                client->Update(ml);
            }
        }
    }
    return ret;
}

bool DeleteMail(AB::Entities::Mail& mail)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(mail))
        return false;

    if (!client->Delete(mail))
        return false;

    if (!mail.attachments.empty())
    {
        auto* factory = GetSubsystem<Game::ItemFactory>();
        for (const auto& itemUuid : mail.attachments)
        {
            if (auto* item = factory->GetConcrete(itemUuid))
            {
                if (item->concreteItem_.storagePlace == AB::Entities::StoragePlace::Mail)
                    factory->DeleteItem(item);
                else
                    LOG_ERROR << "Wrong storage place of " << item->concreteItem_.uuid << std::endl;
            }
        }
    }

    // Delete mail from mail list
    AB::Entities::MailList ml;
    ml.uuid = mail.toAccountUuid;
    if (client->Read(ml))
    {
        auto it = std::find_if(ml.mails.begin(), ml.mails.end(), [&mail](const AB::Entities::MailHeader& current)
        {
            return Utils::Uuid::IsEqual(current.uuid, mail.uuid);
        });
        if (it != ml.mails.end())
        {
            ml.mails.erase(it);
            client->Update(ml);
        }
    }

    return true;
}

}
