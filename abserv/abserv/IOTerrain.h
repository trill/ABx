/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "IOAsset.h"
#include "Terrain.h"

namespace IO {

class IOTerrain final : public IOAssetImpl<Game::Terrain>
{
public:
    bool Import(Game::Terrain& asset, const std::string& name) override;
};

}
