/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AreaOfEffect.h"
#include "Actor.h"
#include "DataProvider.h"
#include "Game.h"
#include "Script.h"
#include <sa/time.h>

namespace Game {

void AreaOfEffect::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["AreaOfEffect"].setClass(std::move(kaguya::UserdataMetatable<AreaOfEffect, GameObject>()
        .addFunction("GetScriptFile", &AreaOfEffect::_LuaGetScriptFile)
        .addFunction("GetStartTime", &AreaOfEffect::GetStartTime)
        .addFunction("GetRemainingTime", &AreaOfEffect::GetRemainingTime)
        .addFunction("IsAlly", &AreaOfEffect::IsAlly)
        .addFunction("IsEnemy", &AreaOfEffect::IsEnemy)

        .addFunction("GetItemIndex", &AreaOfEffect::GetItemIndex)
        .addFunction("SetItemIndex", &AreaOfEffect::SetItemIndex)

        .addProperty("Index", &AreaOfEffect::GetIndex)
        .addProperty("Lifetime", &AreaOfEffect::GetLifetime, &AreaOfEffect::SetLifetime)
        .addProperty("Range", &AreaOfEffect::GetRange, &AreaOfEffect::SetRange)
        .addProperty("ShapeType", &AreaOfEffect::GetShapeType, &AreaOfEffect::SetShapeType)
        .addProperty("Source", &AreaOfEffect::_LuaGetSource, &AreaOfEffect::_LuaSetSource)
    ));
    // clang-format on
}

AreaOfEffect::AreaOfEffect() :
    lua_(*this),
    startTime_(sa::time::tick())
{
    events_.Subscribe<OnCollideSignature>(OnCollideEvent, std::bind(&AreaOfEffect::OnCollide, this, std::placeholders::_1));
    events_.Subscribe<OnTriggerSignature>(OnTriggerEvent, std::bind(&AreaOfEffect::OnTrigger, this, std::placeholders::_1));
    events_.Subscribe<OnLeftAreaSignatre>(OnLeftAreaEvent, std::bind(&AreaOfEffect::OnLeftArea, this, std::placeholders::_1));
    // By default AOE has a sphere shape with the range as radius
    SetCollisionShape(
        ea::make_unique<Math::CollisionShape<Math::Sphere>>(Math::ShapeType::Sphere,
            Math::Vector3_Zero, RangeDistances[static_cast<int>(range_)] * 0.5f)
    );
    // AOE can not hide other objects
    SetOccluder(false);
    // AOE usually not colliding
    SetCollisionLayer(0);
    selectable_ = false;
}

AreaOfEffect::~AreaOfEffect() = default;

void AreaOfEffect::_LuaSetSource(Actor* source)
{
    if (source)
        source_ = source->GetPtr<Actor>();
    else
        source_.reset();
}

Actor* AreaOfEffect::_LuaGetSource() const
{
    return GetSource().get();
}

std::string AreaOfEffect::_LuaGetScriptFile() const
{
    return scriptFile_;
}

bool AreaOfEffect::LoadScript(const std::string& fileName)
{
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
        return false;
    if (!lua_.Load(*script))
        return false;
    scriptFile_ = fileName;

    if (lua_.IsNumber("itemIndex"))
        itemIndex_ = lua_["itemIndex"];
    else
        LOG_WARNING << "AOE " << fileName << " does not have an itemIndex" << std::endl;
    if (lua_.IsNumber("creatureState"))
        stateComp_.SetState(lua_["creatureState"], true);
    else
        stateComp_.SetState(AB::GameProtocol::CreatureState::Idle, true);
    if (lua_.IsNumber("effect"))
        skillEffect_ = lua_["effect"];
    if (lua_.IsNumber("effectTarget"))
        effectTarget_ = lua_["effectTarget"];

    bool ret = lua_["onInit"]();
    return ret;
}

void AreaOfEffect::Update(uint32_t timeElapsed, MessageStream& message)
{
    GameObject::Update(timeElapsed, message);

    stateComp_.Update(timeElapsed);

    stateComp_.Write(message);

    lua_.Call("onUpdate", timeElapsed);
    if (sa::time::time_elapsed(startTime_) > lifetime_)
    {
        lua_.Call("onEnded");
        Remove();
    }
}

void AreaOfEffect::OnCollide(GameObject* other)
{
    // Called from collisionComp_ of the moving object
    // AOE can also be a trap for example
    lua_.Call("onCollide", other);
}

void AreaOfEffect::OnTrigger(GameObject* other)
{
    // AOE can also be a trap for example
    lua_.Call("onTrigger", other);
}

void AreaOfEffect::OnLeftArea(GameObject* other)
{
    // AOE can also be a trap for example
    lua_.Call("onLeftArea", other);
}

Math::ShapeType AreaOfEffect::GetShapeType() const
{
    auto* shape = GetCollisionShape();
    if (!shape)
        return Math::ShapeType::None;
    return shape->shapeType_;
}

void AreaOfEffect::SetShapeType(Math::ShapeType shape)
{
    switch (shape)
    {
    case Math::ShapeType::BoundingBox:
    {
        const float rangeSize = RangeDistances[static_cast<int>(range_)] * 0.5f;
        const Math::Vector3 halfSize = { rangeSize, rangeSize, rangeSize };
        SetCollisionShape(
            ea::make_unique<Math::CollisionShape<Math::BoundingBox>>(Math::ShapeType::BoundingBox,
                -halfSize, halfSize)
        );
        break;
    }
    case Math::ShapeType::Sphere:
        SetCollisionShape(
            ea::make_unique<Math::CollisionShape<Math::Sphere>>(Math::ShapeType::Sphere,
                Math::Vector3_Zero, RangeDistances[static_cast<int>(range_)] * 0.5f)
        );
        break;
    case Math::ShapeType::None:
        SetCollisionShape(ea::unique_ptr<Math::AbstractCollisionShape>());
        break;
    default:
        LOG_ERROR << "Invalid shape type for AOE " << static_cast<int>(shape) << std::endl;
        break;
    }
}

void AreaOfEffect::SetRange(Range range)
{
    // Size can also be set with GameObject::(_Lua)SetBoundingSize()
    if (range_ == range)
        return;

    range_ = range;

    // Update collision shape size
    auto* cs = GetCollisionShape();
    if (!cs)
        return;

    switch (cs->shapeType_)
    {
    case Math::ShapeType::BoundingBox:
    {
        const float rangeSize = RangeDistances[static_cast<int>(range_)] * 0.5f;
        const Math::Vector3 halfSize = { rangeSize, rangeSize, rangeSize };
        using BoxShape = Math::CollisionShape<Math::BoundingBox>;
        BoxShape* shape = static_cast<BoxShape*>(cs);
        shape->Object().min_ = -halfSize;
        shape->Object().max_ = halfSize;
        break;
    }
    case Math::ShapeType::Sphere:
    {
        using SphereShape = Math::CollisionShape<Math::Sphere>;
        SphereShape* shape = static_cast<SphereShape*>(cs);
        shape->Object().radius_ = RangeDistances[static_cast<int>(range)] * 0.5f;
        break;
    }
    default:
        LOG_WARNING << "Can not set range for shape type " << static_cast<int>(cs->shapeType_) << std::endl;
        break;
    }
}

void AreaOfEffect::SetSource(ea::shared_ptr<Actor> source)
{
    source_ = source;
}

ea::shared_ptr<Actor> AreaOfEffect::GetSource() const
{
    return source_.lock();
}

uint32_t AreaOfEffect::GetRemainingTime() const
{
    uint32_t elapsed = sa::time::time_elapsed(startTime_);
    if (elapsed > lifetime_)
        return 0u;
    return static_cast<uint32_t>(lifetime_ - elapsed);
}

uint32_t AreaOfEffect::GetGroupId() const
{
    if (auto s = source_.lock())
        return s->GetGroupId();
    return 0u;
}

bool AreaOfEffect::IsEnemy(const Actor* other) const
{
    if (auto s = source_.lock())
        return s->IsEnemy(other);
    return false;
}

bool AreaOfEffect::IsAlly(const Actor* other) const
{
    if (auto s = source_.lock())
        return s->IsAlly(other);
    return false;
}

bool AreaOfEffect::IsInRange(const Actor* other) const
{
    const float dist = transformation_.position_.Distance(other->GetPosition());
    return dist <= RangeDistances[static_cast<int>(range_)];
}

uint32_t AreaOfEffect::GetItemIndex() const
{
    return itemIndex_;
}

bool AreaOfEffect::Serialize(sa::PropWriteStream& stream)
{
    using namespace AB::GameProtocol;
    static constexpr uint32_t validFields = ObjectSpawnDataFieldName | ObjectSpawnDataFieldModelIndex;
    stream.Write<uint32_t>(validFields);

    if (!GameObject::Serialize(stream))
        return false;
    stream.Write<uint32_t>(GetItemIndex());   // Model/Item index
    return true;
}

void AreaOfEffect::WriteSpawnData(MessageStream& msg)
{
    auto& m = msg.GetMessage(64);
    GameObject::WriteSpawnData(msg);

    using namespace AB::GameProtocol;
    static constexpr uint32_t validFields = ObjectSpawnFieldPos | ObjectSpawnFieldRot | ObjectSpawnFieldScale |
        ObjectSpawnFieldUndestroyable | ObjectSpawnFieldSelectable | ObjectSpawnFieldState |
        ObjectSpawnFieldGroupId;

    m.Add<uint32_t>(validFields);

    m.Add<float>(transformation_.position_.x_);
    m.Add<float>(transformation_.position_.y_);
    m.Add<float>(transformation_.position_.z_);
    m.Add<float>(transformation_.GetYRotation());
    m.Add<float>(transformation_.scale_.x_);
    m.Add<float>(transformation_.scale_.y_);
    m.Add<float>(transformation_.scale_.z_);
    m.Add<uint8_t>(1);                                  // not destroyable
    m.Add<bool>(selectable_);
    m.Add<uint8_t>(static_cast<uint8_t>(stateComp_.GetState()));
    m.Add<uint32_t>(GetGroupId());                      // Group id
    sa::PropWriteStream data;
    size_t dataSize = 0;
    Serialize(data);
    const char* cData = data.GetStream(dataSize);
    m.Add(std::string(cData, dataSize));
}

}
