/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DamageComp.h"
#include "Actor.h"
#include "SkillBar.h"
#include "EffectsComp.h"
#include "MessageStream.h"
#include <libcommon/Random.h>
#include <libcommon/Subsystems.h>
#include <libcommon/NetworkMessage.h>
#include <sa/WeightedSelector.h>
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include <sa/time.h>

namespace Game {
namespace Components {

DamageComp::DamageComp(Actor& owner) :
    owner_(owner)
{ }

void DamageComp::ApplyDamage(Actor* source, uint32_t index, DamageType type, int value, float penetration, bool melee)
{
    if (owner_.IsDead())
        return;

    lastDamage_ = sa::time::tick();
    // Get a random pos where to hit
    const DamagePos pos = GetDamagePos();

    ea::shared_ptr<Skill> skill;
    if (source)
    {
        skill = source->skills_->GetSkillBySkillIndex(index);
        if (skill)
            source->effectsComp_->GetSkillDamageBonus(&owner_, skill.get(), type, value);
    }
    owner_.CallEvent<OnGettingDamageSignature>(OnGettingDamageEvent, source, skill.get(), type, value);
    // Get the armor effect at this pos with the given damage type and armor penetration
    const float am = owner_.GetArmorEffect(type, pos, penetration);
    const int realValue = static_cast<int>(static_cast<float>(value) * am);
    damages_.Enqueue({ { lastDamage_, type, pos, realValue, (source ? source->id_ : 0), index }, true });
    owner_.resourceComp_->SetHealth(SetValueType::Decrease, abs(realValue));
    if (source)
    {
        lastDamager_ = source->GetPtr<Actor>();
        if (melee)
        {
            lastMeleeDamager_ = source->GetPtr<Actor>();
            lastMeleeDamage_ = lastDamage_;
        }
    }
    owner_.CallEvent<OnGotDamageSignature>(OnGotDamageEvent, source, skill.get(), type, value);
}

int DamageComp::DrainLife(Actor* source, uint32_t index, int value)
{
    if (owner_.IsDead())
        return 0;

    const int currLife = owner_.resourceComp_->GetHealth();
    const int result = Math::Clamp(value, 0, currLife);
    lastDamage_ = sa::time::tick();
    damages_.Enqueue({ { lastDamage_, DamageType::LifeDrain, DamagePos::NoPos, result, (source ? source->id_ : 0), index }, true });
    owner_.resourceComp_->SetHealth(Components::SetValueType::Absolute, currLife - result);
    if (source)
        lastDamager_ = source->GetPtr<Actor>();
    return result;
}

void DamageComp::Touch()
{
    lastDamage_ = sa::time::tick();
}

DamagePos DamageComp::GetDamagePos()
{
    static sa::WeightedSelector<DamagePos> ws;
    if (!ws.IsInitialized())
    {
        for (size_t i = 0; i < Utils::CountOf(DamagePosChances); ++i)
            ws.Add(static_cast<DamagePos>(i), DamagePosChances[i]);
        ws.Update();
    }

    auto* rng = GetSubsystem<Crypto::Random>();
    const float rnd1 = rng->GetFloat();
    const float rnd2 = rng->GetFloat();
    return ws.Get(rnd1, rnd2);
}

uint32_t DamageComp::NoDamageTime() const
{
    return sa::time::time_elapsed(lastDamage_);
}

bool DamageComp::IsGettingMeleeDamage() const
{
    return sa::time::time_elapsed(lastMeleeDamage_) <= 1000;
}

bool DamageComp::IsLastDamager(const Actor& actor)
{
    if (auto d = lastDamager_.lock())
        if (actor.id_ == d->id_)
            return true;
    return false;
}

void DamageComp::Write(MessageStream& message)
{
    if (damages_.IsEmpty())
        return;
    for (auto& d : damages_)
    {
        if (!d.dirty)
            continue;

        AB::Packets::Server::ObjectDamaged packet = {
            owner_.id_,
            d.damage.actorId,
            static_cast<uint16_t>(d.damage.index),
            static_cast<uint8_t>(d.damage.type),
            static_cast<uint16_t>(abs(d.damage.value))
        };
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectDamaged, packet);
        d.dirty = false;
    }
}

bool DamageComp::GotDamageType(DamageType type) const
{
    if (damages_.IsEmpty())
        return false;
    for (const auto& d : damages_)
    {
        if (sa::time::time_elapsed(d.damage.tick) > LAST_DAMAGE_TIME)
            continue;
        if (d.damage.type == type)
            return true;
    }
    return false;
}

bool DamageComp::GotDamageCategory(DamageTypeCategory cat) const
{
    if (damages_.IsEmpty())
        return false;

    if (cat == DamageTypeCategory::Any)
    {
        const auto& last = damages_.Last();
        return sa::time::time_elapsed(last.damage.tick) <= LAST_DAMAGE_TIME;
    }

    for (const auto& d : damages_)
    {
        if (sa::time::time_elapsed(d.damage.tick) > LAST_DAMAGE_TIME)
            continue;
        if (IsDamageCategory(d.damage.type, cat))
            return true;
    }
    return false;
}

}
}
