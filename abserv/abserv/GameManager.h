/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <limits>
#include <eastl.hpp>
#include <libcommon/Service.h>
#include <sa/IdGenerator.h>
#include <AB/Entities/Game.h>
#include <sa/Iteration.h>

namespace Game {

class Player;
class Game;

class GameManager
{
public:
    enum class State
    {
        Running,
        Terminated
    };
private:
    State state_;
    std::mutex lock_;
    ea::map<uint32_t, ea::shared_ptr<Game>> games_;
    ea::map<std::string, ea::vector<Game*>> maps_;
    sa::IdGenerator<uint32_t> gameIds_;
    uint32_t GetNewGameId()
    {
        return gameIds_.Next();
    }
    ea::shared_ptr<Game> CreateGame(const std::string& mapUuid);
protected:
    friend class Game;
    void DeleteGameTask(uint32_t gameId);
public:
    GameManager() :
        state_(State::Terminated)
    { }

    void Start();
    void Stop();
    /// Creates a new game instance, e.g. when a group of players enters an instance
    ea::shared_ptr<Game> NewGame(const std::string& mapName)
    {
        return CreateGame(mapName);
    }
    bool InstanceExists(const std::string& uuid);
    ea::shared_ptr<Game> GetInstance(const std::string& instanceUuid);
    ea::shared_ptr<Game> GetOrCreateInstance(const std::string& mapUuid, const std::string& instanceUuid);
    /// Returns the game with the mapName. If no such game exists it creates one.
    ea::shared_ptr<Game> GetGame(const std::string& mapName, bool canCreate = false);
    ea::shared_ptr<Game> Get(uint32_t gameId);
    // Delete all games with no players
    void CleanGames();
    AB::Entities::GameType GetGameType(const std::string& mapUuid);

    GameManager::State GetState() const { return state_; }
    size_t GetGameCount() const { return games_.size(); }
    const ea::map<uint32_t, ea::shared_ptr<Game>>& GetGames() const { return games_; }
    template<typename Callback>
    void VisitGames(Callback&& callback)
    {
        for (auto& gamePair : games_)
        {
            if (callback(*gamePair.second) != Iteration::Continue)
                break;
        }
    }
};

}
