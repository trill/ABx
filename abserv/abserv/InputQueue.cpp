/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InputQueue.h"

namespace Game {

InputQueue::InputQueue() = default;
InputQueue::~InputQueue() = default;

bool InputQueue::Get(InputItem& item)
{
    // Dispatcher Thread
    if (queue_.empty())
        return false;
    item = queue_.front();
    queue_.pop();
    return true;
}

}
