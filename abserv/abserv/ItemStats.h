/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libshared/Damage.h>
#include <sa/PropStream.h>
#include <libcommon/Variant.h>
#include <libshared/Attributes.h>
#include <libshared/Items.h>

namespace Game {

class ItemStats
{
public:
    ItemStats();
    ~ItemStats() = default;

    DamageType GetDamageType() const;
    int32_t GetMinDamage() const;
    int32_t GetMaxDamage() const;
    uint32_t GetRequirement() const;
    Attribute GetAttribute() const;
    int GetArmor(DamageType damageType) const;
    uint32_t GetAttributeIncrease(Attribute index) const;
    int GetHealth() const;
    int GetEnergy() const;
    int GetHealthRegen() const;
    int GetEnergyRegen() const;
    int GetUsages() const;
    void DescreaseUsages();
    template <typename T>
    T GetValue(ItemStatIndex index, T def) const
    {
        return GetValue<T>(static_cast<size_t>(index), def);
    }
    template <typename T>
    T GetValue(size_t index, T def) const
    {
        const auto it = stats_.find(index);
        if (it != stats_.end())
            return static_cast<T>((*it).second);
        return def;
    }
    template<typename T>
    void SetValue(ItemStatIndex index, T value)
    {
        SetValue(static_cast<size_t>(index), value);
    }
    template<typename T>
    void SetValue(size_t index, T value)
    {
        stats_[index] = value;
    }

    bool Load(sa::PropReadStream& stream);
    void Save(sa::PropWriteStream& stream) const;
    bool LoadFromString(const std::string& value);
    std::string ToString() const;

    Utils::VariantMap stats_;
};

}
