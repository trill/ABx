/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageStream.h"
#include "Game.h"
#include "Player.h"
#include "Party.h"
#include <AB/Packets/ServerPackets.h>
#include "MessageFilter.h"
#include "EffectManager.h"
#include "Effect.h"
#include "EffectsComp.h"
#include <libcommon/Subsystems.h>
#include <libcommon/Dispatcher.h>
#include <utility>
#include <sa/shared_function.h>

namespace Game {

static ea::unique_ptr<Net::MessageFilter> messageFilter;

void MessageStream::InitMessageFilter()
{
    // This is static because it's heavy and does not need to store anything.
    ASSERT(!messageFilter);
    using namespace AB::Packets::Server;
    messageFilter = ea::make_unique<Net::MessageFilter>();
    // Subscribe to all messages we may filter out
    messageFilter->Subscribe<ObjectPositionUpdate>([](const Game& game, const Player& player, ObjectPositionUpdate& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::Interest, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectRotationUpdate>([](const Game& game, const Player& player, ObjectRotationUpdate& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::Interest, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectSkillFailure>([](const Game& game, const Player& player, ObjectSkillFailure& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectUseSkill>([](const Game& game, const Player& player, ObjectUseSkill& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectSkillSuccess>([](const Game& game, const Player& player, ObjectSkillSuccess& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectSkillInterrupted>([](const Game& game, const Player& player, ObjectSkillInterrupted& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectSkillsRecharged>([](const Game& game, const Player& player, ObjectSkillsRecharged& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectAttackFailure>([](const Game& game, const Player& player, ObjectAttackFailure& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectSetAttackSpeed>([](const Game& game, const Player& player, ObjectSetAttackSpeed& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::Compass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectTargetSelected>([](const Game& game, const Player& player, ObjectTargetSelected& packet) -> bool
    {
        if (packet.id == player.id_ || packet.targetId == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::Compass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectEffectAdded>([](const Game& game, const Player& player, ObjectEffectAdded& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;

        // Pass long lasting effects, in case the player comes into range
        const auto* object = game.GetObject<Actor>(packet.id);
        const auto* effect = object->effectsComp_->GetEffect(packet.effectIndex);
        if (!effect)
            return false;
        if (effect->GetRemainingTime() > 10000)
            return true;

        if (!player.IsInRange(Range::Interest, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectEffectRemoved>([](const Game& game, const Player& player, ObjectEffectRemoved& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;

        // Can't get the Effect object from the Actor because it was removed
        auto* effectMngr = GetSubsystem<EffectManager>();
        auto effect = effectMngr->Get(packet.effectIndex);
        if (!effect)
            return false;
        // Pass long lasting effects, in case the player comes into range
        if (effect->IsPersistent() || effect->GetTicks() > 10000)
            return true;

        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::Interest, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectDamaged>([](const Game& game, const Player& player, ObjectDamaged& packet) -> bool
    {
        if (packet.id == player.id_ || packet.sourceId == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectHealed>([](const Game& game, const Player& player, ObjectHealed& packet) -> bool
    {
        if (packet.id == player.id_ || packet.sourceId == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
    messageFilter->Subscribe<ObjectResourceChanged>([](const Game& game, const Player& player, ObjectResourceChanged& packet) -> bool
    {
        if (packet.id == player.id_)
            return true;
        const auto* object = game.GetObject<GameObject>(packet.id);
        if (!player.IsInRange(Range::TwoCompass, object))
            return false;
        return true;
    });
}

MessageStream::~MessageStream() = default;

Net::NetworkMessage& MessageStream::GetMessage(size_t size)
{
    ASSERT(size <= Net::NetworkMessage::MaxBodyLength);
    if (!currentMessage_)
        currentMessage_ = Net::NetworkMessage::GetNew();
    if (currentMessage_->GetSpace() < size)
    {
        Send();
        currentMessage_ = Net::NetworkMessage::GetNew();
    }
    return *currentMessage_;
}

void MessageStream::Reset()
{
    currentMessage_.reset();
}

GameMessageStream::GameMessageStream(Game& owner) :
    owner_(owner)
{
}

void GameMessageStream::Send()
{
    if (!currentMessage_ || currentMessage_->GetSize() == 0)
        return;

    // A function is copyable, but a unique_ptr is not. So moving a non-copyable object into a copyable object does not work.
    GetSubsystem<Asynch::Dispatcher>()->Add(Asynch::CreateTask(sa::make_shared_function([this, msgPtr = std::move(currentMessage_)]()
    {
        const auto& msg = *msgPtr;
        owner_.VisitPlayers([this, &msg](Player& player)
        {
            auto playerMsg = Net::NetworkMessage::GetNew();
            messageFilter->Execute(owner_, player, msg, *playerMsg);
            player.WriteToOutput(*playerMsg);
            return Iteration::Continue;
        });

        if (writeStream_ && writeStream_->IsOpen())
            writeStream_->Write(msg);
    })));
}

bool GameMessageStream::SetRecordingDir(const std::string& dir)
{
    writeStream_ = std::make_unique<IO::GameWriteStream>();
    return writeStream_->Open(dir, &owner_);
}

std::string GameMessageStream::GetRecordingFilename() const
{
    if (writeStream_ && writeStream_->IsOpen())
        return writeStream_->GetFilename();
    return "";
}

PlayerMessageStream::PlayerMessageStream(Player& owner) :
    owner_(owner)
{
}

PlayerMessageStream::~PlayerMessageStream()
{
    if (currentMessage_ && currentMessage_->GetSize() != 0)
        Send();
}

void PlayerMessageStream::Send()
{
    if (!currentMessage_ || currentMessage_->GetSize() == 0)
        return;
    owner_.WriteToOutput(*currentMessage_);
    currentMessage_.reset();
}

PartyMessageStream::PartyMessageStream(Party& owner) :
    owner_(owner)
{
}

PartyMessageStream::~PartyMessageStream()
{
    if (currentMessage_ && currentMessage_->GetSize() != 0)
        Send();
}

void PartyMessageStream::Send()
{
    if (!currentMessage_ || currentMessage_->GetSize() == 0)
        return;
    owner_.WriteToMembers(*currentMessage_);
    currentMessage_.reset();
}

}
