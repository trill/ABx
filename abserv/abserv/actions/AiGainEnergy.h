/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillAction.h"

namespace AI {
namespace Actions {

class GainEnergy final : public SkillAction
{
    NODE_CLASS(GainEnergy)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit GainEnergy(const ArgumentsType& arguments) :
        SkillAction(arguments)
    { }
};

}
}
