/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>

namespace AI {
namespace Actions {

class Die final : public Action
{
    NODE_CLASS(Die)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit Die(const ArgumentsType& arguments);
};

}
}
