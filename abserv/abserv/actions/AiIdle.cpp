/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiIdle.h"

namespace AI {
namespace Actions {

Node::Status Idle::ExecuteStart(Agent& agent, uint32_t timeElapsed)
{
    // TODO: Play some random animations
    return TimedNode::ExecuteStart(agent, timeElapsed);
}

}
}
