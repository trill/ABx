/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>

namespace AI {
namespace Actions {

class Wander final : public Action
{
    NODE_CLASS(Wander)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit Wander(const ArgumentsType& arguments);
};

}
}
