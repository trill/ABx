/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>
#include <eastl.hpp>

namespace Game {
class Actor;
class Skill;
}

namespace AI {
namespace Actions {

class SkillAction : public Action
{
protected:
    bool TestSkill(int index, Game::Actor& source, Game::Actor* target);
    bool TestSkill(Game::Skill& skill, Game::Actor& source, Game::Actor* target);
    int GetSkillIndex(const ea::vector<int>& candidates, Game::Actor& source, Game::Actor* target);
    explicit SkillAction(const ArgumentsType& arguments) :
        Action(arguments)
    {
        mustComplete_ = true;
    }
};

}
}
