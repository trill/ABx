/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiAttackSelection.h"
#include "../Npc.h"
#include "../Game.h"
#include "../AttackComp.h"
#include <CleanupNs.h>
#include <sa/Assert.h>

namespace AI {
namespace Actions {

Node::Status AttackSelection::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    ASSERT(npc.HasGame());

    const auto& selection = agent.filteredAgents_;
    if (selection.empty())
        return Status::Failed;

    for (auto id : selection)
    {
        auto* target = npc.GetGame()->GetObject<Game::Actor>(id);
        if (!target)
            continue;

        if (target->IsDead())
            return Status::Finished;
        if (npc.attackComp_->IsAttackingTarget(target))
            return Status::Running;

        if (npc.Attack(target, false))
        {
            if (npc.GetSpeed() < 1.0f)
                npc.SetSpeed(1.0f);
            return Status::Running;
        }
    }
    return Status::Failed;
}

AttackSelection::AttackSelection(const ArgumentsType& arguments) :
    Action(arguments)
{
}

}
}
