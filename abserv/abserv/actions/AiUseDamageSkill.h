/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillAction.h"
#include "../SkillDefs.h"

namespace AI {
namespace Actions {

class UseDamageSkill final : public SkillAction
{
    NODE_CLASS(UseDamageSkill)
private:
    // By default any target type
    Game::SkillEffectTarget targetType_{ Game::SkillTargetTarget };
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit UseDamageSkill(const ArgumentsType& arguments) :
        SkillAction(arguments)
    {
        GetArgument<Game::SkillEffectTarget>(arguments, 0, targetType_);
    }
};

}
}
