/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/TimedNode.h>

namespace AI {
namespace Actions {

class Idle final : public TimedNode
{
    NODE_CLASS(Idle)
public:
    explicit Idle(const ArgumentsType& arguments) :
        TimedNode(arguments)
    { }
    Node::Status ExecuteStart(Agent& agent, uint32_t timeElapsed) override;
};

}
}
