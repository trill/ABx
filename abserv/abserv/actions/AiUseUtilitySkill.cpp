/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiUseUtilitySkill.h"
#include "../Npc.h"
#include "../Game.h"
#include "../Skill.h"
#include "../SkillBar.h"

//#define DEBUG_AI

namespace AI {
namespace Actions {

Node::Status UseUtilitySkill::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (IsCurrentAction(agent))
    {
        if (auto* cs = npc.GetCurrentSkill())
        {
            if (cs->IsUsing())
                return Status::Running;
        }
        return Status::Finished;
    }

    if (npc.skills_->GetCurrentSkill())
        // Some other skill currently using
        return Status::Failed;

    // Utility skills are skills that are usually not used by the AI, because they don't have
    // an immediate effect.
    int skillIndex = -1;
    npc.skills_->VisitSkills([&](int index, Game::Skill& skill)
    {
        if (skill.NeedsTarget())
            return Iteration::Continue;
        if (TestSkill(skill, npc, nullptr))
        {
            skillIndex = index;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });
    if (skillIndex < 0)
        return Status::Failed;
    if (npc.UseSkill(skillIndex, false))
        return Status::Running;

    return Status::Failed;
}

}
}
