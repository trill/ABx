/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>

namespace AI {
namespace Actions {

class Flee final : public Action
{
    NODE_CLASS(Flee)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit Flee(const ArgumentsType& arguments) :
        Action(arguments)
    { }
};

}
}
