/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillAction.h"
#include "../SkillDefs.h"

namespace AI {
namespace Actions {

class UseUtilitySkill final : public SkillAction
{
    NODE_CLASS(UseUtilitySkill)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit UseUtilitySkill(const ArgumentsType& arguments) :
        SkillAction(arguments)
    { }
};

}
}
