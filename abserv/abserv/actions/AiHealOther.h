/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillAction.h"

namespace AI {
namespace Actions {

class HealOther final : public SkillAction
{
    NODE_CLASS(HealOther)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit HealOther(const ArgumentsType& arguments);
};

}
}
