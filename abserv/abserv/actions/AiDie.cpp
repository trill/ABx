/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiDie.h"
#include "../Npc.h"

namespace AI {
namespace Actions {

Node::Status Die::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (npc.IsDead())
        return Status::Failed;
    if (npc.Die(nullptr))
        return Status::Finished;
    return Status::Failed;
}

Die::Die(const ArgumentsType& arguments) :
    Action(arguments)
{
}

}
}
