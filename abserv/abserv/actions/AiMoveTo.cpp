/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiMoveTo.h"
#include "../Npc.h"
#include "../Game.h"

namespace AI {
namespace Actions {

Node::Status MoveTo::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    const auto& selection = agent.filteredAgents_;
    if (selection.empty())
        return Status::Failed;

#ifdef DEBUG_AI
    LOG_DEBUG << "Moving to " << selection[0] << std::endl;
#endif

    auto* target = npc.GetGame()->GetObject<Game::GameObject>(selection[0]);
    if (!target)
        return Status::Failed;
    if (IsCurrentAction(agent))
    {
        if (npc.IsInRange(Game::Range::Touch, target))
            return Status::Finished;
    }

    if (npc.FollowObjectById(selection[0], false))
        return Status::Running;
    return Status::Failed;
}

}
}
