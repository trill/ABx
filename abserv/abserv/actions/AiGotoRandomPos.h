/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>
#include <libmath/Vector3.h>

namespace AI {
namespace Actions {

class GotoRandomPos final : public Action
{
    NODE_CLASS(GotoRandomPos)
private:
    static Math::Vector3 GetAimPos(Agent& agent);
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit GotoRandomPos(const ArgumentsType& arguments);
};

}
}
