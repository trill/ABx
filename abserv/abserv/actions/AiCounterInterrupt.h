/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillAction.h"

namespace AI {
namespace Actions {

class CounterInterrupt final : public SkillAction
{
    NODE_CLASS(CounterInterrupt)
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit CounterInterrupt(const ArgumentsType& arguments);
};

}
}
