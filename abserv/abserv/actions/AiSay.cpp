/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiSay.h"
#include "../Npc.h"
#include "../AiAgent.h"

namespace AI {
namespace Actions {

Node::Status Say::DoAction(Agent& agent, uint32_t)
{
    [[maybe_unused]] Game::Npc& npc = GetNpc(agent);
    return Status::Failed;
}

}
}
