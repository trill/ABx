/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiCounterInterrupt.h"
#include "../AiAgent.h"
#include "../Npc.h"
#include "../Game.h"
#include "../Skill.h"
#include "../SkillBar.h"

//  #define DEBUG_AI

namespace AI {
namespace Actions {

CounterInterrupt::CounterInterrupt(const ArgumentsType& arguments) :
    SkillAction(arguments)
{ }

Node::Status CounterInterrupt::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (IsCurrentAction(agent))
    {
        if (auto* cs = npc.GetCurrentSkill())
        {
            if (cs->IsUsing())
                return Status::Running;
        }
        return Status::Finished;
    }

    if (npc.skills_->GetCurrentSkill())
        // Some other skill currently using
        return Status::Failed;

    ea::vector<int> skills;
    if (!npc.GetSkillCandidates(skills, Game::SkillEffectCounterInterrupt, Game::SkillTargetSelf))
    {
#ifdef DEBUG_AI
        LOG_DEBUG << npc.GetName() << " GetSkillCandidates() no skill found" << std::endl;
#endif
        return Status::Failed;
    }

    int skillIndex = GetSkillIndex(skills, npc, nullptr);
    if (skillIndex == -1)
    {
#ifdef DEBUG_AI
        LOG_DEBUG << npc.GetName() << " GetSkillIndex() No skill" << std::endl;
#endif
        return Status::Failed;
    }

    if (npc.IsDead())
        return Status::Failed;
    if (!TestSkill(skillIndex, npc, &npc))
        return Status::Failed;

    GetAgent(agent).selectedSkill_ = skillIndex;

    if (npc.UseSkill(skillIndex, false))
        return Status::Running;

#ifdef DEBUG_AI
    LOG_DEBUG << npc.GetName() << " failed to use skill " << skill->data_.name << std::endl;
#endif
    return Status::Failed;
}

}
}
