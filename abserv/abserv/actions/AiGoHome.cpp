/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiGoHome.h"
#include "../Npc.h"
#include <libshared/Mechanic.h>

//#define DEBUG_AI

namespace AI {
namespace Actions {

Node::Status GoHome::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (npc.IsImmobilized())
        return Status::Failed;
    const Math::Vector3& home = npc.GetHomePos();
#ifdef DEBUG_AI
//    LOG_DEBUG << "Home pos is " << home << std::endl;
#endif
    if (home.Equals(Math::Vector3_Zero))
        return Status::Failed;

    if (home.Equals(npc.GetPosition(), Game::AT_POSITION_THRESHOLD))
        return Status::Finished;

    if (IsCurrentAction(agent) && npc.stateComp_.GetState() == AB::GameProtocol::CreatureState::Moving)
        return Status::Running;

    if (npc.GotoHomePos())
    {
        if (npc.GetSpeed() < 1.0f)
            npc.SetSpeed(1.0f);
#ifdef DEBUG_AI
        LOG_DEBUG << npc.GetName() << " goes home from " << npc.GetPosition().ToString() <<
                     " to " << home.ToString() << std::endl;
#endif
        return Status::Running;
    }
#ifdef DEBUG_AI
//    LOG_DEBUG << "Failed to go to " << home << std::endl;
#endif
    return Status::Finished;
}

GoHome::GoHome(const ArgumentsType& arguments) :
    Action(arguments)
{ }

}
}
