/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SkillAction.h"
#include "../Actor.h"
#include "../Game.h"
#include "../AiAgent.h"
#include "../Npc.h"
#include "../Skill.h"
#include "../SkillBar.h"
#include <AB/ProtocolCodes.h>

namespace AI {
namespace Actions {

bool SkillAction::TestSkill(int index, Game::Actor& source, Game::Actor* target)
{
    auto skill = source.skills_->GetSkill(index);
    if (!skill)
        return false;
    return TestSkill(*skill, source, target);
}

bool SkillAction::TestSkill(Game::Skill& skill, Game::Actor& source, Game::Actor* target)
{
    if (!source.resourceComp_->HaveEnoughResources(&skill))
        return false;
    if (skill.NeedsTarget())
    {
        if (!target)
            return false;
        if (target->IsDead())
        {
            if (!skill.HasEffect(Game::SkillEffectResurrect))
                return false;
            if (!source.IsAlly(target))
                return false;
        }
    }
    auto res = skill.CanUse(&source, target);
#ifdef DEBUG_AI
    if (res != AB::GameProtocol::SkillError::None)
        LOG_DEBUG << skill->data_.name << "::CanUse() returned " << static_cast<int>(res) << std::endl;
#endif
    // Out of range is okay we will move to the target
    return res == AB::GameProtocol::SkillError::None || res == AB::GameProtocol::SkillError::OutOfRange;
}

int SkillAction::GetSkillIndex(const ea::vector<int>& candidates, Game::Actor& source, Game::Actor* target)
{
    if (candidates.empty())
        return -1;
    int skillIndex = -1;
    for (int index : candidates)
    {
        if (TestSkill(index, source, target))
        {
            skillIndex = index;
            break;
        }
    }
    return skillIndex;
}

}

}
