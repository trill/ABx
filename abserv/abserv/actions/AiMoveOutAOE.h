/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libai/Action.h>
#include <libmath/Vector3.h>

namespace Game {
class Npc;
class AreaOfEffect;
}

namespace AI {
namespace Actions {

class MoveOutAOE final : public Action
{
    NODE_CLASS(MoveOutAOE)
private:
    static bool TryMove(Game::Npc& npc, Game::AreaOfEffect& damager, Math::Vector3& destination);
protected:
    Status DoAction(Agent& agent, uint32_t timeElapsed) override;
public:
    explicit MoveOutAOE(const ArgumentsType& arguments) :
        Action(arguments)
    {
        mustComplete_ = true;
    }
};

}
}
