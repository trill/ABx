/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AiAgent.h"
#include "Npc.h"

namespace AI {

AiAgent::AiAgent(Game::Npc& npc) :
    Agent(npc.id_),
    npc_(npc)
{ }

AiAgent::~AiAgent() = default;

}
