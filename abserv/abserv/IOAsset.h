/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <sa/Compiler.h>

namespace IO {

class IOAsset
{
public:
    IOAsset() = default;
    virtual ~IOAsset() = default;
};

template<class T>
class IOAssetImpl : public IOAsset
{
public:
    virtual bool Import(T& asset, const std::string& name) = 0;
};

}
