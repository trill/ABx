/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Effect.h>

namespace Game {

class Effect;

enum EffectAttr : uint8_t
{
    EffectAttrId = 1,
    EffectAttrTicks,

    // For serialization
    EffectAttrEnd = 254
};

enum class EffectCategories : uint32_t
{
    None = 0,
    // From skills -------------------------------------------------------------
    Condition = (1 << AB::Entities::EffectCondition) - 1,
    Enchantment = (1 << AB::Entities::EffectEnchantment) - 1,
    Hex = (1 << AB::Entities::EffectHex) - 1,
    Shout = (1 << AB::Entities::EffectShout) - 1,
    Spirit = (1 << AB::Entities::EffectSpirit) - 1,
    Ward = (1 << AB::Entities::EffectWard) - 1,
    Well = (1 << AB::Entities::EffectWell) - 1,
    Skill = (1 << AB::Entities::EffectSkill) - 1,
    Preparation = (1 << AB::Entities::EffectPreparation) - 1,
    Stance = (1 << AB::Entities::EffectStance) - 1,
    Form = (1 << AB::Entities::EffectForm) - 1,
    Glyphe = (1 << AB::Entities::EffectGlyphe) - 1,
    PetAttack = (1 << AB::Entities::EffectPetAttack) - 1,
    WeaponSpell = (1 << AB::Entities::EffectWeaponSpell) - 1,
    Signet = (1 << AB::Entities::EffectSignet) - 1,
};

// Effects are fist-in-last-out
using EffectList = ea::vector<ea::shared_ptr<Effect>>;

}
