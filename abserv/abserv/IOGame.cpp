/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOGame.h"
#include <AB/Entities/GameList.h>
#include <libcommon/UuidUtils.h>
#include "Game.h"

namespace IO {

bool LoadGame(AB::Entities::Game& game)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    return client->Read(game);
}

bool LoadGameByName(Game::Game& game, const std::string& name)
{
    game.data_.name = name;
    return LoadGame(game.data_);
}

bool LoadGameByUuid(Game::Game& game, const std::string& uuid)
{
    game.data_.uuid = uuid;
    return LoadGame(game.data_);
}

std::string GetLandingGameUuid()
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::GameList gl;
    if (!client->Read(gl))
    {
        LOG_ERROR << "Error reading game list" << std::endl;
        return Utils::Uuid::EMPTY_UUID;
    }
    if (gl.gameUuids.empty())
    {
        LOG_ERROR << "Game list is empty" << std::endl;
        return Utils::Uuid::EMPTY_UUID;
    }

    for (const std::string& uuid : gl.gameUuids)
    {
        AB::Entities::Game g;
        g.uuid = uuid;
        if (client->Read(g) && g.landing)
            return g.uuid;
    }
    LOG_ERROR << "No landing game found" << std::endl;
    return Utils::Uuid::EMPTY_UUID;
}

AB::Entities::GameType GetGameType(const std::string& mapUuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Game g;
    g.uuid = mapUuid;
    if (!client->Read(g))
    {
        LOG_ERROR << "Error reading game with UUID " << mapUuid << std::endl;
        return AB::Entities::GameTypeUnknown;
    }
    return g.type;
}

std::vector<AB::Entities::Game> GetGameList()
{
    std::vector<AB::Entities::Game> result;

    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::GameList gl;
    if (!client->Read(gl))
    {
        LOG_ERROR << "Error reading game list" << std::endl;
        return result;
    }

    for (const std::string& uuid : gl.gameUuids)
    {
        AB::Entities::Game g;
        g.uuid = uuid;
        if (!client->Read(g))
            continue;
        result.push_back(g);
    }

    if (result.empty())
        LOG_WARNING << "No Games found!" << std::endl;
    return result;
}

std::string GetGameUuidFromName(const std::string& name)
{
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Game g;
    g.name = name;
    if (client->Read(g))
        return g.uuid;

    return Utils::Uuid::EMPTY_UUID;
}

}
