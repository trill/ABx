/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GroupComp.h"
#include "Actor.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include "Party.h"
#include "MessageStream.h"

namespace Game::Components {

GroupComp::GroupComp(Actor& owner) :
    owner_(owner),
    groupColor_(TeamColor::Default)
{
}

GroupComp::~GroupComp() = default;

void GroupComp::SetGroupColor(TeamColor value)
{
    if (value == groupColor_)
        return;
    groupColor_ = value;
    groupColorChanged_ = true;
}

void GroupComp::SetGroupMask(uint32_t value)
{
    if (groupMask_ == value)
        return;
    groupMask_ = value;
    groupMaskChanged_ = true;
}

void GroupComp::SetGroupId(uint32_t value)
{
    if (groupId_ == value)
        return;
    groupId_ = value;
    groupIdChanged_ = true;
}

void GroupComp::Update(uint32_t timeElapsed)
{
    if (auto* g = owner_.GetGroup())
    {
        // Call Group::Update() only once per tick
        if (g->IsLeader(owner_))
        {
            const bool oldDefeated = g->IsDefeated();
            const bool oldResigned = g->IsResigned();
            g->Update(timeElapsed);
            if (g->IsDefeated() && !oldDefeated)
                defeatedDirty_ = true;
            if (g->IsResigned() && !oldResigned)
                resignedDirty_ = true;
        }
    }
}

bool GroupComp::IsOwnerLeader() const
{
    if (auto* g = owner_.GetGroup())
        return g->IsLeader(owner_);
    return false;
}

void GroupComp::Write(MessageStream& message)
{
    if (groupMaskChanged_ || groupColorChanged_)
    {
        AB::Packets::Server::ObjectGroupChanged packet;
        packet.id = owner_.id_;
        packet.validFields = 0;
        if (groupMaskChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldMask;
            packet.mask = groupMask_;
            groupMaskChanged_ = false;
        }
        if (groupColorChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldColor;
            packet.color = static_cast<uint8_t>(groupColor_);
            groupColorChanged_ = false;
        }
        if (groupIdChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldId;
            packet.groupId = groupId_;
            groupIdChanged_ = false;
        }
        message.AddPacket(AB::GameProtocol::ServerPacketType::ObjectGroupChanged, packet);
    }

    if (auto* g = owner_.GetGroup())
    {
        // Write messages only once per tick
        if (!g->IsLeader(owner_))
            return;

        if (resignedDirty_)
        {
            AB::Packets::Server::PartyResigned packet = {
                g->GetId(),
                g->GetName()
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::PartyResigned, packet);
            resignedDirty_ = false;
        }

        if (defeatedDirty_)
        {
            AB::Packets::Server::PartyDefeated packet = {
                g->GetId(),
                g->GetName()
            };
            message.AddPacket(AB::GameProtocol::ServerPacketType::PartyDefeated, packet);
            defeatedDirty_ = false;
        }
    }
}

}
