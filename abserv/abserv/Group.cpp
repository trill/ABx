/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Group.h"
#include "Actor.h"
#include <sa/EAIterator.h>
#include <libcommon/Random.h>
#include <libcommon/Subsystems.h>

namespace Game {

sa::IdGenerator<uint32_t> Group::groupIds_;

void Group::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Group"].setClass(std::move(kaguya::UserdataMetatable<Group>()
        .addFunction("GetLeader", &Group::GetLeader)
        .addFunction("Add", &Group::_LuaAdd)
        .addFunction("Remove", &Group::_LuaRemove)
        .addFunction("IsAlly", &Group::IsAlly)
        .addFunction("IsEnemy", &Group::IsEnemy)
        .addFunction("IsAttacked", &Group::IsAttacked)
        .addFunction("GetMorale", &Group::GetMorale)
        .addFunction("IncreaseMorale", &Group::IncreaseMorale)
        .addFunction("DecreaseMorale", &Group::DecreaseMorale)
        .addFunction("Resurrect", &Group::Resurrect)
        .addFunction("KillAll", &Group::KillAll)
        .addFunction("GetMember", &Group::_LuaGetMember)
        .addFunction("GetMembers", &Group::_LuaGetMembers)
        .addFunction("GetMemberCount", &Group::_LuaGetMemberCount)
        .addFunction("GetRandomMember", &Group::GetRandomMember)
        .addFunction("GetRandomMemberInRange", &Group::GetRandomMemberInRange)
        .addFunction("Defeat", &Group::Defeat)
        .addFunction("IsDefeated", &Group::IsDefeated)

        .addProperty("Color", &Group::GetColor, &Group::SetColor)
        .addProperty("Id", &Group::GetId)
        .addProperty("Name", &Group::GetName, &Group::SetName)
    ));
    // clang-format on
}

Group::Group(uint32_t id) :
    id_(id)
{ }

void Group::SetColor(TeamColor value)
{
    if (value == color_)
        return;
    color_ = value;
    VisitMembers([this](Actor& current) -> Iteration
    {
        current.SetGroupColor(color_);
        return Iteration::Continue;
    });
}

bool Group::_LuaRemove(Actor* actor)
{
    if (!actor)
        return false;
    return Remove(actor->id_);
}

bool Group::_LuaAdd(Actor* actor)
{
    if (!actor)
        return false;
    return Add(actor->GetPtr<Actor>());
}

bool Group::IsEnemy(const Group* other) const
{
    auto* l1 = GetLeader();
    if (!l1)
        return false;
    auto* l2 = other->GetLeader();
    if (!l2)
        return false;
    return l1->IsEnemy(l2);
}

bool Group::IsAlly(const Group* other) const
{
    auto* l1 = GetLeader();
    if (!l1)
        return false;
    auto* l2 = other->GetLeader();
    if (!l2)
        return false;
    return l1->IsAlly(l2);
}

bool Group::IsAttacked() const
{
    bool result = false;
    VisitMembers([&result](const Actor& current) -> Iteration
    {
        if (current.IsAttacked())
        {
            result = true;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });

    return result;
}

bool Group::Remove(uint32_t id)
{
    auto it = ea::find_if(members_.begin(), members_.end(), [&](const ea::weak_ptr<Actor>& current)
    {
        if (auto c = current.lock())
            return c->id_ == id;
        return false;
    });
    if (it == members_.end())
        return false;
    members_.erase(it);
    return true;
}

bool Group::Add(ea::shared_ptr<Actor> actor)
{
    const auto it = ea::find_if(members_.begin(), members_.end(), [&](const ea::weak_ptr<Actor>& current)
    {
        if (auto c = current.lock())
            return c->id_ == actor->id_;
        return false;
    });
    if (it != members_.end())
        return false;
    actor->SetGroupId(id_);
    actor->SetGroupColor(color_);
    members_.push_back(actor);
    return true;
}

Actor* Group::GetLeader() const
{
    if (members_.empty())
        return nullptr;
    if (auto l = members_.front().lock())
        return l.get();
    return nullptr;
}

bool Group::IsLeader(const Actor& actor) const
{
    if (members_.empty())
        return false;
    auto m = members_.front().lock();
    if (!m)
        return false;
    return m->id_ == actor.id_;
}

void Group::Defeat()
{
    defeated_ = true;
}

void Group::Update(uint32_t)
{
    if (defeatedTick_ == 0)
    {
        if (defeated_)
        {
            defeatedTick_ = sa::time::tick();
            KillAll();
        }
    }
}

Actor* Group::GetRandomMember() const
{
    if (members_.empty())
        return nullptr;

    auto* rng = GetSubsystem<Crypto::Random>();
    const float rnd = rng->GetFloat();
    using iterator = ea::vector<ea::weak_ptr<Actor>>::const_iterator;
    auto it = sa::ea::SelectRandomly<iterator>(members_.begin(), members_.end(), rnd);
    if (it != members_.end())
    {
        if (auto p = (*it).lock())
            return p.get();
    }
    return nullptr;
}

Actor* Group::GetRandomMemberInRange(const Actor* actor, Range range) const
{
    if (members_.empty() || actor == nullptr)
        return nullptr;
    ea::vector<Actor*> actors;
    VisitMembers([&](Actor& current) {
        if (actor->IsInRange(range, &current))
            actors.push_back(&current);
        return Iteration::Continue;
    });
    if (actors.empty())
        return nullptr;

    auto* rng = GetSubsystem<Crypto::Random>();
    const float rnd = rng->GetFloat();
    using iterator = ea::vector<Actor*>::const_iterator;
    auto it = sa::ea::SelectRandomly<iterator>(actors.begin(), actors.end(), rnd);
    if (it != actors.end())
        return (*it);

    return nullptr;
}

Actor* Group::_LuaGetMember(int index)
{
    if (index >= static_cast<int>(members_.size()))
        return nullptr;
    if (auto m = members_.at(static_cast<size_t>(index)).lock())
        return m.get();
    return nullptr;
}

int Group::_LuaGetMemberCount() const
{
    return static_cast<int>(members_.size());
}

std::vector<Actor*> Group::_LuaGetMembers()
{
    std::vector<Actor*> result;
    result.reserve(members_.size());
    for (const auto& m : members_)
    {
        if (auto sm = m.lock())
            result.push_back(sm.get());
    }
    return result;
}

int Group::GetMorale() const
{
    int morale{ 0 };
    int count{ 0 };
    VisitMembers([&morale, &count](const Actor& current)
    {
        morale += current.GetMorale();
        ++count;
        return Iteration::Continue;
    });
    if (count == 0)
        return 0;
    return morale / count;
}

void Group::IncreaseMorale()
{
    VisitMembers([](Actor& current)
    {
        if (!current.IsDead())
            current.IncreaseMorale();
        return Iteration::Continue;
    });
}

void Group::DecreaseMorale()
{
    VisitMembers([](Actor& current)
    {
        if (!current.IsDead())
            current.DecreaseMorale();
        return Iteration::Continue;
    });
}

void Group::Resurrect(int precentHealth, int percentEnergy)
{
    VisitMembers([&](Actor& current)
    {
        if (!current.IsDead())
            current.Resurrect(precentHealth, percentEnergy);
        return Iteration::Continue;
    });
}

void Group::KillAll()
{
    VisitMembers([&](Actor& current)
    {
        current.Die(nullptr);
        return Iteration::Continue;
    });
}

}
