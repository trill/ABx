/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Script.h"
#include <sa/SequenceInputStream.h>
#include <libcommon/Logger.h>

namespace Game {

Script::Script() = default;

Script::~Script() = default;

bool Script::Execute(kaguya::State& luaState) const
{
    sa::SequenceInputStream is(buffer_.data(), buffer_.size());
    if (luaState["__SCRIPTFILE__"].type() != LUA_TSTRING)
        // Top level script
        luaState["__SCRIPTFILE__"] = fileName_;

    if (!luaState.dostream(is))
    {
        LOG_ERROR << lua_tostring(luaState.state(), -1) << std::endl;
        return false;
    }
    return true;
}

}
