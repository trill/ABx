/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "SkillDefs.h"
#include "LuaObject.h"
#include <AB/Entities/Skill.h>
#include <AB/Entities/Profession.h>
#include <AB/ProtocolCodes.h>
#include <eastl.hpp>
#include <sa/Bits.h>
#include <sa/Noncopyable.h>
#include <sa/time.h>
#include <libshared/Mechanic.h>
#include <libshared/Attributes.h>

namespace Game {

class Actor;
class SkillBar;
class Script;

class Skill
{
    NON_COPYABLE(Skill)
    friend class SkillBar;
private:
    LuaObject lua_;
    int64_t startUse_{ 0 };
    int64_t lastUse_{ 0 };
    // Timestamp when recharged
    int64_t rechargedAt_{ 0 };
    AB::Entities::ProfessionIndex profession_{ AB::Entities::ProfessionIndexNone };
    Range range_{ Range::Aggro };
    uint32_t skillEffect_{ SkillEffectNone };
    uint32_t effectTarget_{ SkillTargetNone };
    SkillTargetType targetType_{ SkillTargetTypeNone };
    AB::Entities::SkillType canInterrupt_{ AB::Entities::SkillTypeSkill };
    ea::weak_ptr<Actor> source_;
    ea::weak_ptr<Actor> target_;
    // The real cost may be influenced by skills, armor, effects etc.
    int32_t effectiveEnergy_{ 0 };
    int32_t effectiveAdrenaline_{ 0 };
    int32_t effectiveActivation_{ 0 };
    int32_t effectiveOvercast_{ 0 };
    int32_t effectiveHp_{ 0 };
    uint32_t effectiveRecharge_{ 0 };
    bool rechargedFired_{ true };
    AB::GameProtocol::SkillError lastError_{ AB::GameProtocol::SkillError::None };

    bool CanUseSkill(Actor& source, Actor* target);
    int _LuaGetType() const { return static_cast<int>(skillType_); }
    bool _LuaIsElite() const { return elite_; }
    std::string _LuaGetName() const { return name_; }
    int32_t _LuaGetEnergy() const { return energy_; }
    int32_t _LuaGetAdrenaline() const { return adrenaline_; }
    int32_t _LuaGetActivation() const { return activation_; }
    int32_t _LuaGetOvercast() const { return overcast_; }
    int32_t _LuaGetHp() const { return hp_; }
    int32_t _LuaGetRecharge() const { return recharge_; }
    bool _LuaIsMaintainable() const { return maintainable_; }
    AB::Entities::ProfessionIndex _LuaGetProfession() const { return profession_; }

    uint32_t CalculateRecharge(uint32_t recharge);
    uint32_t GetActivation(uint32_t activation);
    // Called after using the skill to get the effective value
    uint32_t GetHpSacrifies(uint32_t hp);
    // Only SkillBar may use this
    AB::GameProtocol::SkillError StartUse(ea::shared_ptr<Actor> source, ea::shared_ptr<Actor> target);
    AB::GameProtocol::SkillError Execute();
    bool InternalLoadScript(const Script& script);
    void FireOnRecharged();
    void EndUse();
public:
    static void RegisterLua(kaguya::State& state);

    explicit Skill(AB::Entities::Skill skill);
    ~Skill() = default;

    bool LoadScript(const std::string& fileName);
    void Update(uint32_t timeElapsed);

    /// Calls the `canUse` or `onStartUse` function to see if the skill can be used
    /// and/or makes sense to use it
    AB::GameProtocol::SkillError CanUse(Actor* source, Actor* target);
    void CancelUse();
    /// Disable a skill for some time
    void Disable(uint32_t ticks)
    {
        if (rechargedAt_ == 0)
            rechargedAt_ = sa::time::tick();
        rechargedAt_ += ticks;
    }
    bool Interrupt();
    AB::GameProtocol::SkillError GetLastError() const { return lastError_; }

    bool IsUsing() const { return (startUse_ != 0) && (sa::time::time_elapsed(startUse_) < static_cast<uint32_t>(effectiveActivation_)); }
    bool IsRecharged() const { return rechargedAt_ <= sa::time::tick(); }
    void SetRecharged(int64_t ticks);
    bool IsType(AB::Entities::SkillType type) const
    {
        // Every skill is a skill
        if (type == AB::Entities::SkillTypeSkill)
            return true;
        return sa::bits::is_set(skillType_, type);
    }
    bool CanUseOnAlly() const
    {
        return targetType_ == SkillTargetTypeNone || targetType_ == SkillTargetTypeAllyAndSelf || targetType_ == SkillTargetTypeAllyWithoutSelf;
    }
    bool CanUseOnFoe() const
    {
        return targetType_ == SkillTargetTypeNone || targetType_ == SkillTargetTypeFoe;
    }
    bool CanUseOnTarget(const Actor& source, const Actor* target) const;
    bool NeedsTarget() const
    {
        return targetType_ != SkillTargetTypeNone;
    }
    // Returns true if this skill can interrupt skills of type
    bool CanInterrupt(AB::Entities::SkillType type) const
    {
        if (!HasEffect(SkillEffectInterrupt))
            return false;
        if (canInterrupt_ == type)
            return true;
        return sa::bits::is_set(canInterrupt_, type);
    }
    /// Does a skill change the creature state.
    bool IsChangingState() const
    {
        return !IsType(AB::Entities::SkillTypeStance) &&
            !IsType(AB::Entities::SkillTypeFlashEnchantment) &&
            !IsType(AB::Entities::SkillTypeShout);
    }
    uint32_t GetIndex() const { return skillIndex_; }
    Attribute GetAttribute() const { return attribute_; }
    bool HasEffect(SkillEffect effect) const { return sa::bits::is_set(skillEffect_, effect); }
    bool HasTarget(SkillEffectTarget target) const { return sa::bits::is_set(effectTarget_, target); }
    float CalculateCost(const std::function<float(CostType)>& importanceCallback) const;
    bool IsInRange(const Actor* target) const;
    Range GetRange() const { return range_; }
    Actor* GetSource() const;
    Actor* GetTarget() const;
    int32_t GetEffectiveEnergy() const { return effectiveEnergy_; }
    int32_t GetEffectiveAdrenaline() const { return effectiveAdrenaline_; }
    int32_t GetEffectiveActivation() const { return effectiveActivation_; }
    int32_t GetEffectiveOvercast() const { return effectiveOvercast_; }
    int32_t GetEffectiveHp() const { return effectiveHp_; }
    int32_t GetEffectiveRecharge() const { return effectiveRecharge_; }

    int64_t GetLastUse() const { return	lastUse_; }

    void AddRecharge(int32_t ms);

    const std::string& GetName() const { return name_; }

    AB::Entities::SkillType skillType_{ AB::Entities::SkillTypeSkill };
    uint32_t skillIndex_{ 0 };
    std::string professionUuid_;
    Attribute attribute_{ Attribute::None };
    bool elite_{ false };
    std::string name_;
    int32_t energy_{ 0 };
    int32_t adrenaline_{ 0 };
    int32_t activation_{ 0 };
    uint32_t recharge_{ 0 };
    int32_t overcast_{ 0 };
    /// HP sacrifice
    int32_t hp_{ 0 };
    // There are Enchantments that are up forever but cost energy regeneration
    bool maintainable_{ false };
    bool easyInterruptable_{ false };
    // When we have this skill equipped, the companion is also spawned.
    bool activatesCompanion_{ false };

    std::function<void(Skill& skill)> onRecharged_;

    AB::Entities::Skill data_;
};

inline bool IsSkillError(AB::GameProtocol::SkillError err)
{
    return err != AB::GameProtocol::SkillError::None && err != AB::GameProtocol::SkillError::NotAppropriate;
}

}
