/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <libshared/Damage.h>
#include <libshared/Mechanic.h>
#include <AB/Entities/Character.h>
#include <AB/Entities/Account.h>
#include "ItemContainer.h"
#include "ItemsCache.h"
#include <sa/Noncopyable.h>

namespace Net {
class NetworkMessage;
}

namespace Game {

class Actor;
class Player;
class Skill;
class PlayerMessageStream;

namespace Components {

/// Equipment, like Armor, weapons, weapon mods etc.
class InventoryComp
{
    NON_COPYABLE(InventoryComp)
    NON_MOVEABLE(InventoryComp)
private:
    Actor& owner_;
    EquipmentMap equipment_;
    /// Character inventory
    ea::unique_ptr<ItemContainer> inventory_;
    /// Account chest
    ea::unique_ptr<ItemContainer> chest_;
public:
    static void WriteItemUpdate(const Item* const item, Net::NetworkMessage* message);
    // Move an Item from one player to another
    static void ExchangeItem(Item& item, uint32_t count,
        Player& removeFrom, Player& addTo,
        Net::NetworkMessage& removeMessage,
        Net::NetworkMessage& addMessage);

    explicit InventoryComp(Actor& owner);
    ~InventoryComp() = default;

    void Update(uint32_t timeElapsed);

    Item* SplitStack(Item* item, uint32_t count, AB::Entities::StoragePlace newItemPlace, ItemPos newItemPos);
    EquipPos SetEquipment(uint32_t itemId);
    Item* GetEquipment(EquipPos pos) const;
    uint32_t RemoveEquipment(EquipPos pos);
    /// Swap inventory - equipment
    EquipPos EquipInventoryItem(ItemPos pos);
    void MoveItemsToMail(const std::string& mailUuid, const std::vector<ItemPos>& items, PlayerMessageStream& stream);
    bool SetInventoryItem(uint32_t itemId, Net::NetworkMessage* message,
        ItemPos newPos = 0);
    bool SellItem(ItemPos pos, uint32_t count, uint32_t pricePer, Net::NetworkMessage* message);
    bool BuyItem(Item* item, uint32_t count, uint32_t pricePer, Net::NetworkMessage* message);
    /// Remove and Destroy (i.e. delete from DB) the item
    bool DestroyInventoryItem(ItemPos pos)
    {
        return inventory_->DestroyItem(pos);
    }
    /// Removes the item, does not delete it, e.g. when dropped. Returns the item for further anything.
    uint32_t RemoveInventoryItem(ItemPos pos)
    {
        return inventory_->RemoveItem(pos);
    }
    Item* GetInventoryItem(ItemPos pos)
    {
        return inventory_->GetItem(pos);
    }
    bool IsInventoryFull() const { return inventory_->IsFull(); }
    void SetInventorySize(size_t value)
    {
        inventory_->SetSize(value);
    }
    size_t GetInventorySize() const { return inventory_->GetSize(); }
    size_t GetInventoryCount() const
    {
        return inventory_->GetCount();
    }
    size_t GetFreeInventorySpace() const { return inventory_->GetFreeSpace(); }
    size_t GetMaxInventoryMoney() const { return inventory_->GetMaxMoney(); }
    bool CheckInventoryCapacity(uint32_t money, size_t itemCount) const { return inventory_->CheckCapacity(money, itemCount); }
    bool SetChestItem(uint32_t itemId, Net::NetworkMessage* message,
        ItemPos newPos = 0);
    /// Remove and Destroy (i.e. delete from DB) the item
    bool DestroyChestItem(ItemPos pos)
    {
        return chest_->DestroyItem(pos);
    }
    /// Removes the item, does not delete it, e.g. when dropped. Returns the item for further anything.
    /// Since it's a unique_ptr somebody should own it, if it's still needed.
    uint32_t RemoveChestItem(ItemPos pos)
    {
        return chest_->RemoveItem(pos);
    }
    Item* GetChestItem(ItemPos pos)
    {
        return chest_->GetItem(pos);
    }
    bool IsChestFull() const { return chest_->IsFull(); }
    void SetChestSize(size_t value)
    {
        chest_->SetSize(value);
    }
    size_t GetChestSize() const { return chest_->GetSize(); }
    size_t GetChestCount() const
    {
        return chest_->GetCount();
    }
    size_t GetFreeChestSpace() const { return chest_->GetFreeSpace(); }
    size_t GetMaxChestMoney() const { return chest_->GetMaxMoney(); }
    bool CheckChestCapacity(uint32_t money, size_t itemCount) const { return chest_->CheckCapacity(money, itemCount); }
    uint32_t AddChestMoney(uint32_t amount, Net::NetworkMessage* message);
    // Return how much money was removed
    uint32_t RemoveChestMoney(uint32_t amount, Net::NetworkMessage* message);
    uint32_t AddInventoryMoney(uint32_t amount, Net::NetworkMessage* message);
    uint32_t RemoveInventoryMoney(uint32_t amount, Net::NetworkMessage* message);
    uint32_t DepositMoney(uint32_t amount, Net::NetworkMessage* message);
    uint32_t WithdrawMoney(uint32_t amount, Net::NetworkMessage* message);
    uint32_t GetChestMoney() const;
    uint32_t GetInventoryMoney() const;
    bool HaveInventoryItem(uint32_t itemIndex, uint32_t count);
    bool TakeInventoryItem(uint32_t itemIndex, uint32_t count, Net::NetworkMessage* message);

    void SetUpgrade(Item& item, ItemUpgrade type, uint32_t upgradeId);
    void RemoveUpgrade(Item& item, ItemUpgrade type);

    /// Get lead hand weapon
    Item* GetWeapon() const;
    int GetArmor(DamageType damageType, DamagePos pos);
    float GetArmorPenetration() const;
    uint32_t GetAttributeRank(Attribute index);
    void GetResources(int& maxHealth, int& maxEnergy);
    void GetRegeneration(int& maxHealth, int& maxEnergy);
    void GetSkillCost(Skill* skill, int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    template<typename Func>
    void VisitEquipement(Func&& func);
    template<typename Func>
    void VisitInventory(Func&& func)
    {
        inventory_->VisitItems(std::forward<Func>(func));
    }
    template<typename Func>
    void VisitChest(Func&& func)
    {
        chest_->VisitItems(std::forward<Func>(func));
    }
};

template<typename Func>
inline void InventoryComp::VisitEquipement(Func&& func)
{
    auto* cache = GetSubsystem<ItemsCache>();
    for (auto& i : equipment_)
    {
        auto* item = cache->Get(i.second);
        if (item)
        {
            if (func(*item) != Iteration::Continue)
                break;
        }
    }
}

}
}
