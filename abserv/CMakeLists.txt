project (abserv CXX)

file(GLOB ABSERV_SOURCES
    abserv/*.cpp abserv/*.h
    abserv/actions/*.cpp abserv/actions/*.h
    abserv/conditions/*.cpp abserv/conditions/*.h
    abserv/filters/*.cpp abserv/filters/*.h
)

add_executable(
    abserv
    ${ABSERV_SOURCES}
)

target_precompile_headers(abserv PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/abserv/stdafx.h)

if (ABX_SCENE_VIEWER)
  target_compile_definitions(abserv PRIVATE SCENE_VIEWER)
endif(ABX_SCENE_VIEWER)

# Skip LTO for now, linking takes ages!
#set_property(TARGET abserv PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
set_property(TARGET abserv PROPERTY POSITION_INDEPENDENT_CODE ON)

target_link_libraries(abserv libcommon abcrypto libmath libio libaccount libai libipc libshared Detour DetourTileCache EASTL PugiXml)
if (ABX_SCENE_VIEWER)
    find_package(GLEW REQUIRED)
    target_include_directories(abserv PRIVATE ${GLEW_INCLUDE_DIRS})
    target_link_libraries(abserv freeglut_static ${GLEW_LIBRARIES})
    target_link_libraries(abserv GL)
endif(ABX_SCENE_VIEWER)
target_link_libraries(abserv chillout)

install(TARGETS abserv
    RUNTIME DESTINATION bin
    COMPONENT runtime
)
