/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BoundingBox.h"
#include "Mesh.h"
#include <eastl.hpp>
#include "MeshObject.h"

namespace Math {

class TriangleMesh : public Mesh, public MeshObject
{
private:
    ea::shared_ptr<Mesh> GenerateMesh() const final;
public:
    TriangleMesh() = default;
    explicit TriangleMesh(const Mesh& other);
    TriangleMesh(const TriangleMesh& other) = default;
    TriangleMesh(const TriangleMesh& other, const Matrix4& matrix) :
        Mesh(other, matrix),
        boundingBox_(other.boundingBox_)
    {}
    TriangleMesh(TriangleMesh&& other) noexcept = default;
    ~TriangleMesh() override = default;

    TriangleMesh& operator= (const TriangleMesh& other) = default;
    TriangleMesh& operator= (TriangleMesh&& other) noexcept = default;

    [[nodiscard]] BoundingBox GetBoundingBox() const
    {
        return boundingBox_;
    }
    [[nodiscard]] Intersection IsInside(const Vector3& point) const;
    [[nodiscard]] TriangleMesh Transformed(const Matrix4& transform) const;
    bool Collides(const Sphere& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const BoundingBox& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const HeightMap& b2, const Vector3& velocity, Vector3& move) const;

    BoundingBox boundingBox_;
};

}
