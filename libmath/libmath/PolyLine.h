/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include <eastl.hpp>
#include <sa/Assert.h>

namespace Math {

// A series of connected lines
class PolyLine
{
public:
    PolyLine() noexcept = default;
    PolyLine(const PolyLine& line) noexcept = default;
    PolyLine(PolyLine&& line) noexcept = default;
    ~PolyLine();
    PolyLine& operator = (const PolyLine& other) = default;
    PolyLine& operator = (PolyLine&& other) noexcept = default;

    [[nodiscard]] const float* VertexData() const
    {
        ASSERT(!points_.empty());
        return reinterpret_cast<const float*>(&points_.begin()->x_);
    }
    [[nodiscard]] size_t VertexDataSize() const { return points_.size() * sizeof(float) * 3; }

    ea::vector<Vector3> points_{};
};

} // namespace Math
