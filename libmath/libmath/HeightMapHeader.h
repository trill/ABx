/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Point.h"
#include "Vector3.h"
#include "MathDefs.h"

namespace Math {

struct HeightmapHeader
{
    // Patch size
    int32_t patchSize = 0;
    // Min height value
    float minHeight = std::numeric_limits<float>::max();
    // Max height value
    float maxHeight = std::numeric_limits<float>::lowest();
    // Number of patches
    IntVector2 numPatches;
    // Number of vertices
    IntVector2 numVertices;
    // Size of patch in world uints
    Vector2 patchWorldSize;
    // Origin in world units
    Vector2 patchWorldOrigin;
};

}
