/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include <algorithm>

namespace Math {

class HeightMap;
class ConvexHull;
class Mesh;
class BoundingBox;
class Matrix4;
class TriangleMesh;

class Sphere
{
public:
    constexpr Sphere() noexcept = default;
    /// Copy-construct from another sphere.
    constexpr Sphere(const Sphere& sphere) noexcept  = default;
    constexpr Sphere(Sphere&& sphere) noexcept  = default;
    /// Construct from center and radius.
    constexpr Sphere(const Vector3& center, float radius) noexcept :
        center_(center),
        radius_(radius)
    {}

    ~Sphere() = default;

    constexpr Sphere& operator =(const Sphere& vector) = default;
    constexpr Sphere& operator =(Sphere&& vector) = default;

#if defined(HAVE_DIRECTX_MATH)
    operator XMath::BoundingSphere() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::BoundingSphere({ center_.x_, center_.y_, center_.z_ }, radius_);
    }
#endif

    /// Define from another sphere.
    void Define(const Sphere& sphere) noexcept
    {
        Define(sphere.center_, sphere.radius_);
    }

    /// Define from center and radius.
    void Define(const Vector3& center, float radius) noexcept
    {
        center_ = center;
        radius_ = radius;
    }

    /// Define from an array of vertices.
    void Define(const Vector3* vertices, unsigned count);
    /// Define from a bounding box.
    void Define(const BoundingBox& box);

    /// Merge a point.
    void Merge(const Vector3& point);
    /// Merge an array of vertices.
    void Merge(const Vector3* vertices, unsigned count);
    /// Merge a bounding box.
    void Merge(const BoundingBox& box);

    /// Clear to undefined state.
    void Reset()
    {
        center_ = Vector3_Zero;
        radius_ = -Math::M_INFINITE;
    }

    /// Return true if this sphere is defined via a previous call to Define() or Merge().
    [[nodiscard]] bool IsDefined() const
    {
        return radius_ >= 0.0f;
    }

    [[nodiscard]] BoundingBox GetBoundingBox() const;
    [[nodiscard]] Mesh GetMesh() const;

    [[nodiscard]] Sphere Transformed(const Matrix4& transform) const;

    [[nodiscard]] bool Collides(const BoundingBox& b2) const;
    [[nodiscard]] bool Collides(const BoundingBox& b2, const Vector3& velocity, Vector3& move) const;
    [[nodiscard]] bool Collides(const Sphere& b2) const
    {
        return IsInsideFast(b2) != Intersection::Outside;
    }
    bool Collides(const Sphere& b2, const Vector3& velocity, Vector3&) const;
    bool Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const HeightMap& b2, const Vector3& velocity, Vector3& move) const;

    /// Test if a point is inside.
    [[nodiscard]] Intersection IsInside(const Vector3& point) const
    {
        const float distSquared = (point - center_).LengthSqr();
        if (distSquared < radius_ * radius_)
            return Intersection::Inside;
        return Intersection::Outside;
    }

    /// Test if another sphere is inside, outside or intersects.
    [[nodiscard]] Intersection IsInside(const Sphere& sphere) const
    {
        const float dist = (sphere.center_ - center_).Length();
        if (dist >= sphere.radius_ + radius_)
            return Intersection::Outside;
        if (dist + sphere.radius_ < radius_)
            return Intersection::Inside;
        return Intersection::Intersects;
    }
    [[nodiscard]] Intersection IsInside(const HeightMap& sphere) const;
    [[nodiscard]] Intersection IsInside(const ConvexHull& sphere) const;

    /// Test if another sphere is (partially) inside or outside.
    [[nodiscard]] Intersection IsInsideFast(const Sphere& sphere) const
    {
        const float distSquared = (sphere.center_ - center_).LengthSqr();
        const float combined = sphere.radius_ + radius_;

        if (distSquared >= combined * combined)
            return Intersection::Outside;
        return Intersection::Inside;
    }

    /// Test if a bounding box is inside, outside or intersects.
    [[nodiscard]] Intersection IsInside(const BoundingBox& box) const;
    /// Test if a bounding box is (partially) inside or outside.
    [[nodiscard]] Intersection IsInsideFast(const BoundingBox& box) const;

    /// Return distance of a point to the surface, or 0 if inside.
    [[nodiscard]] float Distance(const Vector3& point) const { return std::max<float>((point - center_).Length() - radius_, 0.0f); }
    friend std::ostream& operator << (std::ostream& os, const Sphere& value)
    {
        return os << value.center_ << " +/- " << value.radius_;
    }

    /// Sphere center.
    Vector3 center_;
    /// Sphere radius.
    float radius_{ -Math::M_INFINITE };
};

}
