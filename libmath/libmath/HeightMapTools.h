/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Mesh.h"
#include "Point.h"
#include <functional>
#include "HeightMapHeader.h"

namespace Math {

// Create height values from a 3D mesh
// \param[in] shape Source shape
// \param[in] targetWdth Desired width of height map. If 0 heightmaps width is the extends of the shape.
// \param[in] targetHeight Desired height of height map. If 0 heightmaps height is the extends of the shape.
// \param[out] width Width of height map
// \param[out] height Height of height map
// \param[out] minHeight Min height value
// \param[out] maxHeight Max height value
ea::vector<float> CreateHeightMapFromMesh(const Math::Mesh& shape,
    int targetWdth, int targetHeight,
    int& width, int& height,
    float& minHeight, float& maxHeight);

// Create height values from an Image
// \param[in] data Image data
// \param[in] width Image width
// \param[in] height Image height
// \param[in] components Number of color components
// \param[in] spacing Height map spacing
// \param[in] patchSize Patch size
// \param[out] header Height map header
ea::vector<float> CreateHeightMapFromImage(const unsigned char* data, int width, int height, int components,
    const Vector3& spacing, int patchSize,
    HeightmapHeader& header);

// Create a 3D mesh from a height map image
// \param[in] data Image data
// \param[in] width Image width
// \param[in] height Image height
// \param[in] components Number of color components
// \param[in] spacing Height map spacing
// \param[in] patchSize Patch size
// \param[in] onVertex Vertex callback
// \param[in] onTriangle Triangle callback
// \param[out] header Height map header
void CreateShapeFromHeightmapImage(const unsigned char* data, int width, int height, int components,
    const Vector3& spacing, int patchSize,
    const std::function<void(const Vector3& vertex)>& onVertex,
    const std::function<void(int i1, int i2, int i3)>& onTriangle,
    HeightmapHeader& header);

}
