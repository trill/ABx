/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "OctreeObject.h"
#include "Octree.h"

namespace Math {

OctreeObject::~OctreeObject() = default;

Octant* OctreeObject::GetOctant() const
{
    return octant_;
}

void OctreeObject::SetOctant(Octant* octant)
{
    octant_ = octant;
}

}
