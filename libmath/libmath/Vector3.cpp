/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Vector3.h"
#include "MathUtils.h"
#include "Vector4.h"
#include "MathUtils.h"
#include <sa/StringTempl.h>

namespace Math {

Vector3::Vector3(const Vector4& vector) noexcept :
    x_(vector.x_),
    y_(vector.y_),
    z_(vector.z_)
{ }

Vector3::Vector3(const std::string& str)
{
    std::vector<std::string> parts = sa::Split(str, " ");
    if (parts.size() == 3)
    {
        x_ = std::stof(parts.at(0));
        y_ = std::stof(parts.at(1));
        z_ = std::stof(parts.at(2));
    }
    else
    {
        x_ = 0.0f;
        y_ = 0.0f;
        z_ = 0.0f;
    }
}

void Vector3::SetLength(float length)
{
    *this = *this * length;
}

Vector3 Vector3::Orthogonal() const
{
    const float x = fabs(x_);
    const float y = fabs(y_);
    const float z = fabs(z_);
    const Vector3 other = (x < y) ?
        ((x < z) ? Vector3_UnitX : Vector3_UnitZ) :
        ((y < z) ? Vector3_UnitY : Vector3_UnitZ);
    return CrossProduct(other);
}

float Vector3::LengthSqr() const
{
    return x_*x_ + y_*y_ + z_*z_;
}

float Vector3::Length() const
{
    return sqrt(x_*x_ + y_*y_ + z_*z_);
}

float Vector3::Distance(const Vector3& v) const
{
    return (*this - v).Length();
}

float Vector3::DistanceXZ(const Vector3& v) const
{
    const float x = x_ - v.x_;
    const float z = z_ - v.z_;
    return sqrt(x*x + z*z);
}

Vector3 Vector3::Normal() const
{
    const float l = Length();
    if (!Math::Equals(l, 0.0f))
        return *this / l;
    return *this;
}

void Vector3::Normalize()
{
    float l = Length();
    if (!Math::Equals(l, 0.0f))
    {
        x_ /= l;
        y_ /= l;
        z_ /= l;
    }
}

void Vector3::Clamp(const Vector3& min, const Vector3& max)
{
    x_ = Math::Clamp(x_, min.x_, max.x_);
    y_ = Math::Clamp(y_, min.y_, max.y_);
    z_ = Math::Clamp(z_, min.z_, max.z_);
}

void Vector3::Clamp(float min, float max)
{
    x_ = Math::Clamp(x_, min, max);
    y_ = Math::Clamp(y_, min, max);
    z_ = Math::Clamp(z_, min, max);
}

}
