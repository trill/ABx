/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>

namespace Math {

class Mesh;

class MeshObject
{
public:
    constexpr MeshObject() = default;
    virtual ~MeshObject() = default;

    [[nodiscard]] const Mesh& GetMesh() const;
protected:
    virtual ea::shared_ptr<Mesh> GenerateMesh() const = 0;
    void ResetMesh();
private:
    // shared_ptr to keep the object copy-able
    mutable ea::shared_ptr<Mesh> mesh_{};
};

} // namespace Math
