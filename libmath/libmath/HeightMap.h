/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Point.h"
#include "Vector3.h"
#include "BoundingBox.h"
#include "Matrix4.h"
#include "MeshObject.h"
#include "HeightMapHeader.h"
#include <eastl.hpp>

namespace Math {

class Mesh;
class TriangleMesh;

class HeightMap : public MeshObject
{
private:
    ea::shared_ptr<Mesh> GenerateMesh() const final;
    /// Transformation matrix
    Matrix4 matrix_;
    Matrix4 inverseMatrix_{ Matrix4_Identity.Inverse() };
public:
    HeightMap();
    HeightMap(const HeightMap& other) = default;
    HeightMap(HeightMap&& other) noexcept = default;
    ~HeightMap() override;

    HeightMap& operator= (const HeightMap& other) = default;
    HeightMap& operator= (HeightMap&& other) noexcept = default;
    void ProcessData();

    [[nodiscard]] float GetRawHeight(int x, int z) const;
    [[nodiscard]] Vector3 GetRawNormal(int x, int z) const;
    /// Return height at world coordinates.
    [[nodiscard]] float GetHeight(const Vector3& world) const;
    [[nodiscard]] Vector3 GetNormal(const Vector3& world) const;

    [[nodiscard]] BoundingBox GetBoundingBox() const
    {
        return boundingBox_;
    }
    [[nodiscard]] const HeightMap& Transformed(const Matrix4&) const
    {
        return *this;
    }
    void SetMatrix(const Matrix4& matrix);

    bool Collides(const Sphere& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const BoundingBox& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const HeightMap&, const Vector3&, Vector3&) const;

    IntVector2 WorldToHeightmap(const Vector3& world);
    Vector3 HeightmapToWorld(const IntVector2& pixel);

    /// Vertex and height spacing.
    Vector3 spacing_;
    HeightmapHeader header_;

    ea::vector<float> heightData_;
    BoundingBox boundingBox_;
};

}

