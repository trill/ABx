/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include "MathDefs.h"
#include "Quaternion.h"
#include <array>
#include "MathConfig.h"

namespace Math {

class ConvexHull;
class HeightMap;
class Sphere;
class Mesh;
class Matrix4;
class Plane;
class Line;
class TriangleMesh;

class BoundingBox
{
private:
    enum Orientations : uint32_t
    {
        OrientationsNone = 0,
        OrientationsO1 = 1u << 1u,
        OrientationsO2 = 1u << 2u
    };
    [[nodiscard]] uint32_t GetOrientations(const BoundingBox& b2) const
    {
        uint32_t o = OrientationsNone;
        if (IsOriented())
            o |= OrientationsO1;
        if (b2.IsOriented())
            o |= OrientationsO2;
        return o;
    }
public:
    constexpr BoundingBox() noexcept = default;
    constexpr BoundingBox(const BoundingBox& other) noexcept = default;
    constexpr BoundingBox(BoundingBox&& other) noexcept = default;
    constexpr BoundingBox(const Vector3& min, const Vector3& max) noexcept :
        min_(min),
        max_(max)
    { }
    constexpr BoundingBox(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) noexcept :
        min_(minX, minY, minZ),
        max_(maxX, maxY, maxZ)
    { }
    /// Construct from minimum and maximum floats (all dimensions same.)
    constexpr BoundingBox(float min, float max) noexcept :
        min_(min, min, min),
        max_(max, max, max)
    { }
    BoundingBox(const Vector3* vertices, size_t count);
    ~BoundingBox() = default;

    BoundingBox& operator = (const BoundingBox& other) = default;
    BoundingBox& operator = (BoundingBox&& other) = default;

#if defined(HAVE_DIRECTX_MATH)
    operator XMath::BoundingBox() const //NOLINT(hicpp-explicit-conversions)
    {
        return { Center(), Extends() };
    }
    operator XMath::BoundingOrientedBox() const //NOLINT(hicpp-explicit-conversions)
    {
        return { Center(), Extends(), orientation_ };
    }
#endif
    constexpr bool operator ==(const BoundingBox& rhs) const { return Equals(rhs); }
    constexpr bool operator !=(const BoundingBox& rhs) const { return !Equals(rhs); }
    [[nodiscard]] constexpr bool Equals(const BoundingBox& rhs) const
    {
        return min_.Equals(rhs.min_) && max_.Equals(rhs.max_);
    }

    void Define(float min, float max);

    [[nodiscard]] constexpr Vector3 Center() const { return (max_ + min_) * 0.5; }
    [[nodiscard]] constexpr Vector3 Size() const { return max_ - min_; }
    [[nodiscard]] BoundingBox GetBoundingBox() const
    {
        return *this;
    }
    [[nodiscard]] std::array<Vector3, 8> GetCorners() const;
    [[nodiscard]] std::array<Vector3, 4> GetCorners2D() const;
    [[nodiscard]] Vector3 GetClosestCorner2D(const Vector3& pos) const;
    [[nodiscard]] std::vector<Plane> GetPlanes() const;
    [[nodiscard]] std::vector<Line> GetEdges() const;
    void Merge(float x, float y, float z);
    void Merge(const Vector3& vertex);
    void Merge(const Vector3* vertices, size_t count);
    void Merge(const BoundingBox& bb);
    void Reset();
    void AddSize(float value);

    [[nodiscard]] constexpr float Width() const
    {
        return max_.x_ - min_.x_;
    }
    [[nodiscard]] constexpr float Height() const
    {
        return max_.y_ - min_.y_;
    }
    [[nodiscard]] constexpr float Depth() const
    {
        return max_.z_ - min_.z_;
    }
    /// Distance from the center to each side, or half size
    [[nodiscard]] constexpr Vector3 Extends() const
    {
        return (max_ - min_) * 0.5f;
    }

    [[nodiscard]] constexpr bool IsDefined() const { return
        !std::isinf(min_.x_) &&
        !std::isinf(min_.y_) &&
        !std::isinf(min_.z_) &&
        !std::isinf(max_.x_) &&
        !std::isinf(max_.y_) &&
        !std::isinf(max_.z_);
    }
    [[nodiscard]] bool IsOriented() const { return !orientation_.IsIdentity(); }
    [[nodiscard]] std::string ToString() const
    {
        if (!IsDefined())
            return "Undefined";
        std::stringstream ss;
        ss << "min: " << min_.ToString() << " ";
        ss << "max: " << max_.ToString();
        if (IsOriented())
            ss << " o: " << orientation_.ToString();
        return ss.str();
    }

    /// AABB
    [[nodiscard]] BoundingBox Transformed(const Matrix4& transform) const;

    [[nodiscard]] Mesh GetMesh() const;

    /// https://www.gamedev.net/resources/_/technical/game-programming/swept-aabb-collision-detection-and-response-r3084
    /// Returns true if the boxes are colliding (velocities are not used)
    [[nodiscard]] bool Collides(const BoundingBox& b2) const;
    /// Returns true if the boxes are colliding (velocities are not used)
    /// move will return the movement the b1 must move to avoid the collision
    [[nodiscard]] bool Collides(const BoundingBox& b2, const Vector3&, Vector3& move) const;
    [[nodiscard]] bool Collides(const Sphere& b2) const;
    [[nodiscard]] bool Collides(const Sphere& b2, const Vector3& velocity, Vector3& move) const;
    [[nodiscard]] bool Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const;
    [[nodiscard]] bool Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const;
    [[nodiscard]] bool Collides(const HeightMap& b2, const Vector3& velocity, Vector3& move) const;

    /// Test if a point is inside.
    [[nodiscard]] Intersection IsInside(const Vector3& point) const
    {
        if (point.x_ < min_.x_ || point.x_ > max_.x_ || point.y_ < min_.y_ || point.y_ > max_.y_ ||
            point.z_ < min_.z_ || point.z_ > max_.z_)
            return Intersection::Outside;

        return Intersection::Inside;
    }

    /// Test if another bounding box is inside, outside or intersects.
    [[nodiscard]] Intersection IsInside(const BoundingBox& box) const
    {
        if (IsOriented() || box.IsOriented())
        {
#if defined(HAVE_DIRECTX_MATH)
            XMath::BoundingOrientedBox me = static_cast<XMath::BoundingOrientedBox>(*this);
            XMath::ContainmentType ct = me.Contains(static_cast<XMath::BoundingOrientedBox>(box));
            return ct == XMath::DISJOINT ? Intersection::Outside : (ct == XMath::INTERSECTS ? Intersection::Intersects : Intersection::Inside);
#else
#error DirectXMath needed
#endif
        }

        if (box.max_.x_ < min_.x_ || box.min_.x_ > max_.x_ || box.max_.y_ < min_.y_ || box.min_.y_ > max_.y_ ||
            box.max_.z_ < min_.z_ || box.min_.z_ > max_.z_)
            return Intersection::Outside;

        if (box.min_.x_ < min_.x_ || box.max_.x_ > max_.x_ || box.min_.y_ < min_.y_ || box.max_.y_ > max_.y_ ||
            box.min_.z_ < min_.z_ || box.max_.z_ > max_.z_)
            return Intersection::Intersects;

        return Intersection::Inside;
    }
    [[nodiscard]] Intersection IsInside(const HeightMap& shape) const;
    [[nodiscard]] Intersection IsInside(const ConvexHull& shape) const;
    [[nodiscard]] Intersection IsInside(const TriangleMesh& shape) const;
    [[nodiscard]] Intersection IsInside(const Sphere& shape) const;

    /// Test if another bounding box is (partially) inside or outside.
    [[nodiscard]] Intersection IsInsideFast(const BoundingBox& box) const
    {
        if (box.IsOriented())
            return IsInside(box);

        if (box.max_.x_ < min_.x_ || box.min_.x_ > max_.x_ || box.max_.y_ < min_.y_ || box.min_.y_ > max_.y_ ||
            box.max_.z_ < min_.z_ || box.min_.z_ > max_.z_)
            return Intersection::Outside;

        return Intersection::Inside;
    }

    friend std::ostream& operator << (std::ostream& os, const BoundingBox& value)
    {
        return os << value.ToString();
    }

    Vector3 min_{ Math::M_INFINITE, Math::M_INFINITE, Math::M_INFINITE };
    float dummyMin_{ 0.0f };
    Vector3 max_{ -Math::M_INFINITE, -Math::M_INFINITE, -Math::M_INFINITE };
    float dummyMax_{ 0.0f };
    Quaternion orientation_;
};

}
