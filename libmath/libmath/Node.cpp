/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Node.h"

namespace Math {

Node::Node(const Node* parent) :
    parent_(parent)
{ }

Node::~Node() = default;

Matrix4 Node::GetMatrix() const
{
    if (parent_)
        return parent_->GetMatrix() * transformation_.GetMatrix();
    return transformation_.GetMatrix();
}

Transformation Node::GetWorldTransformation() const
{
    return Transformation(GetMatrix());
}

} // namespace Math
