/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TriangleMesh.h"
#include "Gjk.h"
#include "ConvexHull.h"
#include "Hull.h"
#include "Vector3.h"
#include "Sphere.h"
#include "HeightMap.h"

namespace Math {

TriangleMesh::TriangleMesh(const Mesh& other) :
    Mesh(other)
{
    boundingBox_.Merge(other.vertexData_.data(), other.vertexData_.size());
}

ea::shared_ptr<Mesh> TriangleMesh::GenerateMesh() const
{
    return ea::make_shared<Mesh>(*this);
}

Intersection TriangleMesh::IsInside(const Vector3& point) const
{
    Mesh shape2(point);
    if (Gjk::StaticIntersects(*this, shape2))
        return Intersection::Inside;
    return Intersection::Outside;
}

TriangleMesh TriangleMesh::Transformed(const Matrix4& transform) const
{
    return { *this, transform };
}

bool TriangleMesh::Collides(const Sphere& b2, const Vector3&, Vector3&) const
{
    return Gjk::StaticIntersects(*this, b2.GetMesh());
}

bool TriangleMesh::Collides(const BoundingBox& b2, const Vector3&, Vector3&) const
{
    return b2.IsInside(*this) != Intersection::Outside;
}

bool TriangleMesh::Collides(const ConvexHull& b2, const Vector3&, Vector3&) const
{
    return Gjk::StaticIntersects(*this, b2.GetMesh());
}

bool TriangleMesh::Collides(const TriangleMesh& b2, const Vector3&, Vector3&) const
{
    return Gjk::StaticIntersects(*this, b2.GetMesh());
}

bool TriangleMesh::Collides(const HeightMap& b2, const Vector3&, Vector3& move) const
{
    const Vector3 pBottom = GetFarsetPointInDirection(Vector3_Down);
    const float y = b2.GetHeight(pBottom);
    if (pBottom.y_ < y)
    {
        move.y_ = pBottom.y_ - y;
        return true;
    }
    return false;
}

}
