/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"

namespace Math {

class Line
{
public:
    constexpr Line() noexcept = default;
    constexpr Line(const Line& line) noexcept = default;
    constexpr Line(Line&& line) noexcept = default;
    constexpr Line(const Vector3& s, const Vector3& e) noexcept :
        start_(s),
        end_(e)
    { }
    ~Line() = default;
    Line& operator = (const Line& other) = default;
    Line& operator = (Line&& other) noexcept = default;

    [[nodiscard]] const float* VertexData() const
    {
        return reinterpret_cast<const float*>(&start_.x_);
    }
    [[nodiscard]] size_t VertexDataSize() const { return 2 * sizeof(float) * 3; } //NOLINT(readability-convert-member-functions-to-static)

    friend std::ostream& operator << (std::ostream& os, const Line& value)
    {
        return os << "Start " << value.start_ << ", End " << value.end_;
    }

    Vector3 start_;
    Vector3 end_;
};

}
