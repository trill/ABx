/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "VectorMath.h"
#include "MathDefs.h"
#include "MathUtils.h"
#include "Matrix4.h"
#include <sa/Compiler.h>

namespace Math {

bool IsPointInTriangle(const Vector3& point, const Vector3& pa, const Vector3& pb, const Vector3& pc, float epsilon)
{
    const Vector3 v1 = (point - pa).Normal();
    const Vector3 v2 = (point - pb).Normal();
    const Vector3 v3 = (point - pc).Normal();

    // If the point is in the triangle the sum of the angles can not be
    // greater than 360 Deg, because the sum the of the angles of a triangle
    // is 360 Deg
    const float totalAngles =
        acos(v1.DotProduct(v2)) +
        acos(v2.DotProduct(v3)) +
        acos(v3.DotProduct(v1));

    return (fabs(totalAngles - M_TWOPI) <= epsilon);
}

Vector3 GetClosestPointOnLine(const Vector3& a, const Vector3& b, const Vector3& p)
{
    const Vector3 c = p - a;
    Vector3 V = b - a;
    float d = V.Length();

    V.Normalize();
    const float t = V.DotProduct(c);
    if (t < 0.0f)
        return a;
    if (t > d)
        return b;

    V *= t;

    return a + V;
}

Vector3 GetClosestPointOnTriangle(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& p)
{
    const Vector3 Rab = GetClosestPointOnLine(a, b, p);
    const Vector3 Rbc = GetClosestPointOnLine(b, c, p);
    const Vector3 Rca = GetClosestPointOnLine(c, a, p);

    const float dAB = (p - Rab).Length();
    const float dBC = (p - Rbc).Length();
    const float dCA = (p - Rca).Length();

    Vector3 result = Rab;

    float m = dAB;
    if (dBC < m)
    {
        m = dBC;
        result = Rbc;
    }
    if (dCA < m)
        result = Rca;

    return result;
}

PointClass GetPointClass(const Vector3& point, const Vector3& origin, const Vector3& normal)
{
    const Vector3 dir = origin - point;
    const float d = dir.DotProduct(normal);
    if (d < -0.001f)
        return PointClass::PlaneFront;
    if (d > 0.001f)
        return PointClass::PlaneBack;

    return PointClass::OnPlane;
}

Vector3 GetTriangleNormal(const Vector3& p1, const Vector3& p2, const Vector3& p3)
{
    return (p1 - p2).CrossProduct(p1 - p3).Normal();
}

void ReverseOrder(ea::array<Vector3, 3>& triangle)
{
    ea::swap(triangle[0], triangle[2]);
}

Vector3 GetPosFromDirectionDistance(const Vector3& position, const Quaternion& direction, float distance)
{
    const Matrix4 m = Matrix4::FromQuaternion(direction.Inverse());
    const Vector3 v = m * (Vector3_One * distance);
    return position + v;
}

Vector3 GetPosFromDirectionDistance(const Vector3& position, const Vector3& direction, float distance)
{
    // LookAt = position + vector
    return GetPosFromLookAtDistance(position, position + direction, distance);
}

Vector3 GetPosFromLookAtDistance(const Vector3& position, const Vector3& lookAt, float distance, const Vector3& up /* = Vector3_UnitY */)
{
    const Matrix4 m = Matrix4::FromLookAt(lookAt, position, up);
    const Vector3 v = m * (Vector3_One * distance);
    return position + v;
}

float IntersectsRayPlane(const Vector3& rOrigin, const Vector3& rVector, const Vector3& pOrogin, const Vector3& pNormal)
{
    const float d = -pNormal.DotProduct(pOrogin);
    const float numer = pNormal.DotProduct(rOrigin) + d;
    const float denom = pNormal.DotProduct(rVector);
    if (Equals(denom, 0.0f))
        return -1.0f;
    return -(numer / denom);
}

float IntersectsRaySphere(const Vector3& rayOrigin, const Vector3& rayVector, const Vector3& sphereOrigin, float sphereRadius)
{
    const Vector3 Q = sphereOrigin - rayOrigin;
    const float c = Q.Length();
    const float v = Q.DotProduct(rayVector);
    const float d = sphereRadius * sphereRadius + (c * c - v * v);
    if (d < 0.0f)
        return -1.0f;
    return (v - sqrt(d));
}

bool IsPointInSphere(const Vector3& point, const Vector3& sphereOrigin, float sphereRadius)
{
    const float distSquared = (point - sphereOrigin).LengthSqr();
    return distSquared < sphereRadius*sphereRadius;
}

float GetDistanceToTriangle(const ea::array<Vector3, 3>& tri, const Vector3& pos)
{
    const Vector3 normal = Math::GetTriangleNormal(tri[0], tri[1], tri[2]);
    const Vector3 v = pos - tri[0];
    const float dot = v.DotProduct(normal);
    const Vector3 s = v - (normal * dot);
    return s.Length();
}

}
