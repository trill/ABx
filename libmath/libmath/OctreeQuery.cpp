/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "OctreeQuery.h"

namespace Math {

bool CollisionMaskOctreeMatcher::Matches(const OctreeObject* object) const
{
    return object->GetCollisionLayer() & mask_;
}

OctreeQuery::~OctreeQuery() = default;

bool OctreeQuery::Matches(const OctreeObject* object) const
{
    if (!matcher_)
        return true;
    return matcher_->Matches(object);
}

Intersection PointOctreeQuery::TestOctant(const BoundingBox& box, bool inside)
{
    if (inside)
        return Intersection::Inside;
    return box.IsInside(point_);
}

void PointOctreeQuery::TestObjects(OctreeObject** start, OctreeObject** end, bool inside)
{
    while (start != end)
    {
        OctreeObject* object = *start++;

        if (object == ignore_ || !Matches(object))
            continue;
        if (inside || object->GetWorldBoundingBox().IsInside(point_) != Intersection::Outside)
            result_.push_back(object);
    }
}

Intersection SphereOctreeQuery::TestOctant(const BoundingBox& box, bool inside)
{
    if (inside)
        return Intersection::Inside;
    return sphere_.IsInside(box);
}

void SphereOctreeQuery::TestObjects(OctreeObject** start, OctreeObject** end, bool inside)
{
    while (start != end)
    {
        OctreeObject* object = *start++;

        if (object == ignore_ || !Matches(object))
            continue;
        if (inside || sphere_.IsInsideFast(object->GetWorldBoundingBox()) != Intersection::Outside)
            result_.push_back(object);
    }
}

Intersection BoxOctreeQuery::TestOctant(const BoundingBox& box, bool inside)
{
    if (inside)
        return Intersection::Inside;
    return box_.IsInside(box);
}

void BoxOctreeQuery::TestObjects(OctreeObject** start, OctreeObject** end, bool inside)
{
    while (start != end)
    {
        OctreeObject* object = *start++;
        if (object == ignore_ || !Matches(object))
            continue;
        if (inside || box_.IsInsideFast(object->GetWorldBoundingBox()) != Intersection::Outside)
            result_.push_back(object);
    }
}

}
