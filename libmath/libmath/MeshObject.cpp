/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MeshObject.h"
#include "Mesh.h"

namespace Math {

const Mesh& MeshObject::GetMesh() const
{
    if (!mesh_)
        mesh_ = ea::move(GenerateMesh());
    return *mesh_;
}

void MeshObject::ResetMesh()
{
    mesh_.reset();
}

} // namespace Math
