/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Skeleton.h"
#include "Bone.h"

namespace Math {

Skeleton::Skeleton() = default;
Skeleton::~Skeleton() = default;

Bone* Skeleton::GetBone(const std::string& name)
{
    const auto it = bones_.find(name);
    if (it != bones_.end())
        return it->second.get();
    return nullptr;
}

Bone* Skeleton::AddBone(std::string name, Bone* parent)
{
    ea::unique_ptr<Bone> bone = ea::make_unique<Bone>(this, parent);
    Bone* result = bone.get();
    bone->name_ = std::move(name);
    bones_.emplace(bone->name_, std::move(bone));
    return result;
}

} // namespace Math
