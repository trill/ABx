/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ConvexHull.h"
#include "Hull.h"
#include "Vector3.h"
#include "Gjk.h"
#include "Sphere.h"
#include "HeightMap.h"
#include "TriangleMesh.h"
#include <cstring>

namespace Math {

ConvexHull::ConvexHull(const ea::vector<Vector3>& vertices)
{
    BuildHull(vertices);
}

ea::shared_ptr<Mesh> ConvexHull::GenerateMesh() const
{
    return ea::make_shared<Mesh>(*this);
}

Intersection ConvexHull::IsInside(const Vector3& point) const
{
    Mesh shape2(point);
    if (Gjk::StaticIntersects(*this, shape2))
        return Intersection::Inside;
    return Intersection::Outside;
}

ConvexHull ConvexHull::Transformed(const Matrix4& transform) const
{
    return { *this, transform };
}

bool ConvexHull::Collides(const Sphere& b2, const Vector3&, Vector3&) const
{
    const Mesh s = b2.GetMesh();
    return Gjk::StaticIntersects(*this, s);
}

bool ConvexHull::Collides(const BoundingBox& b2, const Vector3&, Vector3&) const
{
    const Mesh s = b2.GetMesh();
    return Gjk::StaticIntersects(*this, s);
}

bool ConvexHull::Collides(const ConvexHull& b2, const Vector3&, Vector3&) const
{
    return Gjk::StaticIntersects(*this, b2);
}

bool ConvexHull::Collides(const TriangleMesh& b2, const Vector3&, Vector3&) const
{
    return Gjk::StaticIntersects(*this, b2);
}

bool ConvexHull::Collides(const HeightMap& b2, const Vector3&, Vector3& move) const
{
    const Vector3 pBottom = GetFarsetPointInDirection(Vector3_Down);
    const float y = b2.GetHeight(pBottom);
    if (pBottom.y_ < y)
    {
        move.y_ = pBottom.y_ - y;
        return true;
    }
    return false;
}

void ConvexHull::BuildHull(const ea::vector<Vector3>& vertices)
{
    if (!vertices.empty())
    {
        // Build the convex hull from the raw geometry
        StanHull::HullDesc desc;
        desc.SetHullFlag(StanHull::QF_TRIANGLES);
        desc.mVcount = static_cast<unsigned>(vertices.size());
        desc.mVertices = &vertices[0].x_;
        desc.mVertexStride = 3 * sizeof(float);
        desc.mSkinWidth = 0.0f;

        StanHull::HullLibrary lib;
        StanHull::HullResult result;
        lib.CreateConvexHull(desc, result);

        vertexCount_ = result.mNumOutputVertices;
        vertexData_.resize(vertexCount_);

        // Copy vertex data & index data
        std::memcpy(vertexData_.data(), result.mOutputVertices, vertexCount_ * (sizeof(float) * 3));

        indexCount_ = result.mNumIndices;
        indexData_.reserve(indexCount_);
        for (size_t i = 0; i < indexCount_; ++i)
        {
            indexData_.push_back(result.mIndices[i]);
        }

        lib.ReleaseResult(result);

        boundingBox_.Merge(&vertexData_[0], vertexCount_);
    }
    else
    {
        vertexData_.clear();
        vertexCount_ = 0;
        indexData_.clear();
        indexCount_ = 0;
    }
}

}
