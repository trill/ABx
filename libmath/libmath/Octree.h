/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "OctreeQuery.h"
#include "BoundingBox.h"
#include "Vector3.h"
#include "OctreeObject.h"
#include <limits>
#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Math {

inline constexpr int NUM_OCTANTS = 8;
inline constexpr unsigned ROOT_INDEX = std::numeric_limits<unsigned>::max();

class Octree;

class Octant
{
    NON_COPYABLE(Octant)
    NON_MOVEABLE(Octant)
public:
    Octant(const BoundingBox& box, unsigned level, Octant* parent, Octree* root, unsigned index = ROOT_INDEX);
    ~Octant();
    /// Return or create a child octant.
    Octant* GetOrCreateChild(unsigned index);
    /// Insert a drawable object by checking for fit recursively.
    void InsertObject(OctreeObject* object);
    /// Reset root pointer recursively. Called when the whole octree is being destroyed.
    void ResetRoot();
    /// Delete child octant.
    void DeleteChild(unsigned index);
    /// Check if a drawable object fits.
    [[nodiscard]] bool CheckObjectFit(const BoundingBox& box) const;
    /// Add a drawable object to this octant.
    void AddObject(OctreeObject* object);
    /// Remove a drawable object from this octant.
    void RemoveObject(OctreeObject* object, bool resetOctant = true);
    [[nodiscard]] Octree* GetRoot() const { return root_; }
    [[nodiscard]] const BoundingBox& GetCullingBox() const { return cullingBox_; }
protected:
    void Initialize(const BoundingBox& box);
    /// Return drawable objects by a query, called internally.
    void GetObjectsInternal(OctreeQuery& query, bool inside) const;
    /// Return drawable objects by a ray query, called internally.
    void GetObjectsInternal(RayOctreeQuery& query) const;
    /// Return drawable objects only for a threaded ray query, called internally.
    void GetObjectsOnlyInternal(RayOctreeQuery& query, ea::vector<OctreeObject*>& objects) const;
    BoundingBox worldBoundingBox_;
    /// Bounding box used for drawable object fitting.
    BoundingBox cullingBox_;
    Vector3 center_;
    Vector3 halfSize_;
    /// Subdivision level
    uint32_t level_;
    Octant* children_[NUM_OCTANTS];
    Octant* parent_;
    Octree* root_;
    /// Octant index relative to its siblings or ROOT_INDEX for root octant
    unsigned index_;
    ea::vector<OctreeObject*> objects_;
    /// Number of objects in this octant and child octants.
    unsigned numObjects_;
    /// Increase drawable object count recursively.
    void IncObjectCount()
    {
        ++numObjects_;
        if (parent_)
            parent_->IncObjectCount();
    }
    /// Decrease drawable object count recursively and remove octant if it becomes empty.
    void DecObjectCount()
    {
        Octant* parent = parent_;

        --numObjects_;
        if (!numObjects_)
        {
            if (parent)
                parent->DeleteChild(index_);
        }

        if (parent)
            parent->DecObjectCount();
    }
};

class Octree : public Octant
{
    NON_COPYABLE(Octree)
    NON_MOVEABLE(Octree)
public:
    Octree();
    ~Octree();
    void SetSize(const BoundingBox& box, unsigned numLevels);
    void Update();
    void AddObjectUpdate(OctreeObject* object);
    void RemoveObjectUpdate(OctreeObject* object);
    /// Return drawable objects by a query.
    void GetObjects(OctreeQuery& query) const;
    /// Return drawable objects by a ray query.
    void Raycast(RayOctreeQuery& query) const;
    /// Return the closest object by a ray query.
    void RaycastSingle(RayOctreeQuery& query) const;

    /// Subdivision level.
    unsigned numLevels_;
    ea::vector<OctreeObject*> objectUpdate_;
private:
    /// Ray query temporary list of objects.
    mutable ea::vector<OctreeObject*> rayQueryObjects_;
};

}
