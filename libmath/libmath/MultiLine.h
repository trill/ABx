/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Line.h"
#include <sa/Assert.h>

namespace Math {

// A series of independent (not connected) lines
class MultiLine
{
public:
    MultiLine() noexcept = default;
    MultiLine(const MultiLine& line) noexcept = default;
    MultiLine(MultiLine&& line) noexcept = default;
    ~MultiLine();
    MultiLine& operator = (const MultiLine& other) = default;
    MultiLine& operator = (MultiLine&& other) noexcept = default;

    [[nodiscard]] const float* VertexData() const
    {
        ASSERT(!lines_.empty());
        return reinterpret_cast<const float*>(&lines_.begin()->start_.x_);
    }
    [[nodiscard]] size_t VertexDataSize() const { return lines_.size() * 2 * sizeof(float) * 3; }

    ea::vector<Line> lines_{};
};

} // namespace Math
