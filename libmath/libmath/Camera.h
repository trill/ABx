/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Point.h"
#include "Transformation.h"
#include "Ray.h"
#include "Frustum.h"

namespace Math {

class Camera
{
public:
    Camera();

    void Resize(int w, int h);
    [[nodiscard]] float GetNearClip() const { return nearClip_; }
    [[nodiscard]] float GetFarClip() const { return farClip_; }
    [[nodiscard]] float GetFov() const { return fov_; }
    [[nodiscard]] float GetZoom() const { return zoom_; }
    void SetNearClip(float value);
    void SetFarClip(float value);
    // Set Field of view angle in Rad
    void SetFov(float value);
    void SetZoom(float value);
    [[nodiscard]] float GetAspectRatio() const { return aspectRatio_; }
    [[nodiscard]] const Matrix4& GetProjection() const;
    [[nodiscard]] Matrix4 GetView() const;
    [[nodiscard]] const IntVector2& GetSize() const { return size_; }
    [[nodiscard]] Quaternion GetDirection() const;
    // Frustum in woorld space
    [[nodiscard]] Frustum GetFrustum() const;

    void ApplyAngles();

    Transformation transformation_;
    float yaw_{ 0.0f };
    float pitch_{ 0.0f };
    // Flip Z axis
    bool flipZ_{ false };
    bool reverse_{ false };
private:
    Matrix4 GetWorldTransformation() const;
    mutable Matrix4 projection_;
    mutable bool projectionDirty_{ true };
    float nearClip_{ 0.5f };
    float farClip_{ 200000.0f };
    float fov_{ DegToRad<float>(60.0f) };
    float aspectRatio_{ 16.0f / 9.0f };
    float zoom_{ 1.0f };
    IntVector2 size_;
};

} // namespace Math
