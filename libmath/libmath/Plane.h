/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"

namespace Math {

class Plane
{
public:
    constexpr Plane() noexcept = default;
    constexpr Plane(const Plane& line) noexcept = default;
    constexpr Plane(Plane&& line) noexcept = default;
    constexpr Plane(const Vector3& normal, float distance) noexcept :
        normal_(normal),
        d_(distance)
    { }
    Plane(const Vector3& normal, const Vector3& point)
    {
        Define(normal, point);
    }
    Plane(const Vector3& p1, const Vector3& p2, const Vector3& p3)
    {
        Define(p1, p2, p3);
    }
    ~Plane() = default;
    Plane& operator = (const Plane& other) = default;
    Plane& operator = (Plane&& other) = default;

    /// Define from 3 vertices.
    void Define(const Vector3& v0, const Vector3& v1, const Vector3& v2);
    /// Define from a planeNormal vector and a point on the plane.
    void Define(const Vector3& normal, const Vector3& point);

    [[nodiscard]] float Distance(const Vector3& point) const { return normal_.DotProduct(point) + d_; }
    [[nodiscard]] bool IsFrontFacingTo(const Vector3& direction) const { return normal_.DotProduct(direction) <= 0.0f; }
    friend std::ostream& operator << (std::ostream& os, const Plane& value)
    {
        return os << "Normal " << value.normal_ << ", d " << value.d_;
    }

    Vector3 normal_;
    float d_{ 0.0f };
};

}
