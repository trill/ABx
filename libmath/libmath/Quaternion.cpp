/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Quaternion.h"
#include <sa/StringTempl.h>
#include "MathUtils.h"
#include "Matrix4.h"

namespace Math {

Quaternion::Quaternion(float x, float y, float z)
{
    x *= 0.5f;
    y *= 0.5f;
    z *= 0.5f;

    const float sinX = sinf(x);
    const float cosX = cosf(x);
    const float sinY = sinf(y);
    const float cosY = cosf(y);
    const float sinZ = sinf(z);
    const float cosZ = cosf(z);

    w_ = cosY * cosX * cosZ + sinY * sinX * sinZ;
    x_ = cosY * sinX * cosZ + sinY * cosX * sinZ;
    y_ = sinY * cosX * cosZ - cosY * sinX * sinZ;
    z_ = cosY * cosX * sinZ - sinY * sinX * cosZ;
}

Quaternion::Quaternion(const std::string& str)
{
    std::vector<std::string> parts = sa::Split(str, " ");

    if (parts.size() < 3)
    {
        w_ = 1.0f;
        x_ = 0.0f;
        y_ = 0.0f;
        z_ = 0.0f;
        return;
    }
    if (parts.size() < 4)
    {
        // Euler angles
        *this = Quaternion(std::stof(parts.at(1)), // x
            std::stof(parts.at(2)),                // y
            std::stof(parts.at(3)));               // z
        return;
    }

    w_ = std::stof(parts.at(0));
    x_ = std::stof(parts.at(1));
    y_ = std::stof(parts.at(2));
    z_ = std::stof(parts.at(3));
}

Quaternion Quaternion::FromLookAt(const Vector3& from, const Vector3& to)
{
    // https://gamedev.stackexchange.com/questions/15070/orienting-a-model-to-face-a-target
    const Vector3 forward = (to - from).Normal();
    const float dot = Vector3_UnitZ.DotProduct(forward);
    if (Math::Equals(dot, -1.0f, 0.000001f))
        // vector a and b point exactly in the opposite direction,
        // so it is a 180 degrees turn around the up-axis
        return { Math::M_PIF, Vector3_UnitY.x_, Vector3_UnitY.y_, Vector3_UnitY.z_ };
    if (Math::Equals(dot, 1.0f, 0.000001f))
        // vector a and b point exactly in the same direction
        // so we return the identity quaternion
        return Quaternion_Identity;
    const float rot = acos(dot);
    const Vector3 axis = Vector3_UnitZ.CrossProduct(forward).Normal();
    return Quaternion::FromAxisAngle(axis, rot);
}

Quaternion Quaternion::FromAxisAngle(const Vector3& axis, float angle)
{
    const Math::Vector3 normalAxis = axis.Normal();
    const float factor = sin(angle * 0.5f);
    const float x = normalAxis.x_ * factor;
    const float y = normalAxis.y_ * factor;
    const float z = normalAxis.z_ * factor;

    const float w = cos(angle / 2.0f);
    return { w, x, y, z };
}

Vector4 Quaternion::AxisAngle() const
{
    Quaternion q(w_, x_, y_, z_);
    if (abs(q.w_) > 1.0f)
        q.Normalize();

    const float den = sqrt(1.0f - q.w_ * q.w_);
    if (den > 0.0001f)
    {
        return {
            q.x_ / den,
            q.y_ / den,
            q.z_ / den,
            2.0f * acos(q.w_) // Angle
        };
    }
    return {
        Vector3_UnitX.x_,
        Vector3_UnitY.y_,
        Vector3_UnitZ.z_,
        2.0f * acos(q.w_) // Angle
    };
}

Vector3 Quaternion::EulerAngles() const
{
    // Derivation from http://www.geometrictools.com/Documentation/EulerAngles.pdf
    // Order of rotations: Z first, then X, then Y
    const float check = 2.0f * (-y_ * z_ + w_ * x_);

    if (check < -0.995f)
    {
        return { -float(M_PIHALF), 0.0f, -atan2f(2.0f * (x_ * z_ - w_ * y_), 1.0f - 2.0f * (y_ * y_ + z_ * z_)) };
    }
    if (check > 0.995f)
    {
        return { float(M_PIHALF), 0.0f, atan2f(2.0f * (x_ * z_ - w_ * y_), 1.0f - 2.0f * (y_ * y_ + z_ * z_)) };
    }
    return { asinf(check),
        atan2f(2.0f * (x_ * z_ + w_ * y_), 1.0f - 2.0f * (x_ * x_ + y_ * y_)),
        atan2f(2.0f * (x_ * y_ + w_ * z_), 1.0f - 2.0f * (x_ * x_ + z_ * z_)) };
}

float Quaternion::YawAngle() const
{
    return EulerAngles().y_;
}

float Quaternion::PitchAngle() const
{
    return EulerAngles().x_;
}

float Quaternion::RollAngle() const
{
    return EulerAngles().z_;
}

void Quaternion::SetYawAngle(float value)
{
    auto euler = EulerAngles();
    *this = { euler.x_, value, euler.z_ };
}
void Quaternion::SetPitchAngle(float value)
{
    auto euler = EulerAngles();
    *this = { value, euler.y_, euler.z_ };
}

void Quaternion::SetRollAngle(float value)
{
    auto euler = EulerAngles();
    *this = { euler.x_, euler.y_, value };
}

Quaternion Quaternion::Inverse() const
{
#if defined(HAVE_DIRECTX_MATH)
    return XMath::XMQuaternionInverse(*this);
#else
    float lenSquared = LengthSqr();
    if (lenSquared == 1.0f)
        return Conjugate();
    else if (lenSquared >= M_EPSILON)
        return Conjugate() * (1.0f / lenSquared);
    else
        return Quaternion_Identity;
#endif
}
Quaternion Quaternion::Conjugate() const
{
#if defined(HAVE_DIRECTX_MATH)
    return XMath::XMQuaternionConjugate(*this);
#else
    return Quaternion(w_, -x_, -y_, -z_);
#endif
}

Quaternion& Quaternion::operator*=(const Quaternion& rhs)
{
#if defined(HAVE_DIRECTX_MATH)
    *this = XMath::XMQuaternionMultiply(rhs, *this);
#else
    w_ = w_ * rhs.w_ - x_ * rhs.x_ - y_ * rhs.y_ - z_ * rhs.z_;
    x_ = w_ * rhs.x_ + x_ * rhs.w_ + y_ * rhs.z_ - z_ * rhs.y_;
    y_ = w_ * rhs.y_ + y_ * rhs.w_ + z_ * rhs.x_ - x_ * rhs.z_;
    z_ = w_ * rhs.z_ + z_ * rhs.w_ + x_ * rhs.y_ - y_ * rhs.x_;
#endif
    return *this;
}

Quaternion operator*(const Quaternion& lhs, const Quaternion& rhs)
{
#if defined(HAVE_DIRECTX_MATH)
    return XMath::XMQuaternionMultiply(rhs, lhs);
#else
    return { lhs.w_ * rhs.w_ - lhs.x_ * rhs.x_ - lhs.y_ * rhs.y_ - lhs.z_ * rhs.z_,
        lhs.w_ * rhs.x_ + lhs.x_ * rhs.w_ + lhs.y_ * rhs.z_ - lhs.z_ * rhs.y_,
        lhs.w_ * rhs.y_ + lhs.y_ * rhs.w_ + lhs.z_ * rhs.x_ - lhs.x_ * rhs.z_,
        lhs.w_ * rhs.z_ + lhs.z_ * rhs.w_ + lhs.x_ * rhs.y_ - lhs.y_ * rhs.x_ };
#endif
}

void Quaternion::Normalize()
{
    const float length = Length();
    x_ /= length;
    y_ /= length;
    z_ /= length;
    w_ /= length;
}

Quaternion Quaternion::Normal() const
{
    return *this / Length();
}

float Quaternion::Length() const
{
    return sqrt(w_ * w_ + x_ * x_ + y_ * y_ + z_ * z_);
}

Quaternion Quaternion::Slerp(const Quaternion& rhs, float t) const
{
#if defined(HAVE_DIRECTX_MATH)
    return XMath::XMQuaternionSlerp(*this, rhs, t);
#else
    // Favor accuracy for native code builds
    float cosAngle = DotProduct(rhs);
    float sign = 1.0f;
    // Enable shortest path rotation
    if (cosAngle < 0.0f)
    {
        cosAngle = -cosAngle;
        sign = -1.0f;
    }

    const float angle = acosf(cosAngle);
    const float sinAngle = sinf(angle);
    float t1, t2;

    if (sinAngle > 0.001f)
    {
        float invSinAngle = 1.0f / sinAngle;
        t1 = sinf((1.0f - t) * angle) * invSinAngle;
        t2 = sinf(t * angle) * invSinAngle;
    }
    else
    {
        t1 = 1.0f - t;
        t2 = t;
    }

    return *this * t1 + (rhs * sign) * t2;
#endif
}

Quaternion Quaternion::Nlerp(const Quaternion& rhs, float t, bool shortestPath) const
{
    Quaternion result;
    float fCos = DotProduct(rhs);
    if (fCos < 0.0f && shortestPath)
        result = (*this) + (((-rhs) - (*this)) * t);
    else
        result = (*this) + ((rhs - (*this)) * t);
    result.Normalize();
    return result;
}

Matrix4 Quaternion::GetMatrix() const
{
    return { { 1.0f - 2.0f * y_ * y_ - 2.0f * z_ * z_,
                 2.0f * x_ * y_ - 2.0f * w_ * z_,
                 2.0f * x_ * z_ + 2.0f * w_ * y_,
                 0.0f },
        { 2.0f * x_ * y_ + 2.0f * w_ * z_,
            1.0f - 2.0f * x_ * x_ - 2.0f * z_ * z_,
            2.0f * y_ * z_ - 2.0f * w_ * x_,
            0.0f },
        { 2.0f * x_ * z_ - 2.0f * w_ * y_,
            2.0f * y_ * z_ + 2.0f * w_ * x_,
            1.0f - 2.0f * x_ * x_ - 2.0f * y_ * y_,
            0.0f },
        Vector4_UnitW };
}

float Quaternion::LengthSqr() const
{
    return (w_ * w_ + x_ * x_ + y_ * y_ + z_ * z_);
}

bool Quaternion::IsIdentity() const
{
#if defined(HAVE_DIRECTX_MATH)
    return XMath::XMQuaternionIsIdentity(*this);
#else
    return Equals(Quaternion_Identity);
#endif
}

}
