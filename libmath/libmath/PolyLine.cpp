/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PolyLine.h"

namespace Math {

PolyLine::~PolyLine() = default;

} // namespace Math
