/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BoundingBox.h"
#include "Vector3.h"
#include "Sphere.h"
#include "Ray.h"
#include <eastl.hpp>
#include <sa/Noncopyable.h>
#include <sa/Compiler.h>
#include "OctreeObject.h"

namespace Math {

// This can be used to filter out objects. Filtering objects can increase performance,
// because it skips expensive collision/intersection checking for these objects.
class OctreeMatcher
{
public:
    virtual bool Matches(const OctreeObject* object) const = 0;
};

template<typename Callback>
class CallbackOctreeMatcher final : public OctreeMatcher
{
private:
    Callback callback_;
public:
    explicit CallbackOctreeMatcher(Callback&& callback) :
        callback_(std::move(callback))
    { }
    bool Matches(const OctreeObject* object) const override
    {
        return callback_(object);
    }
};

// Match collsion mask and collision layer. At least one layer must be in our mask.
class CollisionMaskOctreeMatcher final : public OctreeMatcher
{
private:
    uint32_t mask_;
public:
    explicit CollisionMaskOctreeMatcher(uint32_t mask) :
        mask_(mask)
    { }
    bool Matches(const OctreeObject* object) const override;
};

class OctreeQuery
{
    NON_COPYABLE(OctreeQuery)
    NON_MOVEABLE(OctreeQuery)
private:
    const OctreeMatcher* matcher_;
protected:
    const OctreeObject* ignore_;
public:
    explicit OctreeQuery(ea::vector<OctreeObject*>& result, const OctreeObject* ignore, const OctreeMatcher* matcher) :
        matcher_(matcher),
        ignore_(ignore),
        result_(result)
    { }
    virtual ~OctreeQuery();

    /// Intersection test for an octant.
    virtual Intersection TestOctant(const BoundingBox& box, bool inside) = 0;
    /// Intersection test for objects.
    virtual void TestObjects(OctreeObject** start, OctreeObject** end, bool inside) = 0;
    bool Matches(const OctreeObject* object) const;

    ea::vector<OctreeObject*>& result_;
};

class PointOctreeQuery final : public OctreeQuery
{
public:
    PointOctreeQuery(ea::vector<OctreeObject*>& result,
        const Vector3 point, const OctreeObject* ignore = nullptr, const OctreeMatcher* matcher = nullptr) :
        OctreeQuery(result, ignore, matcher),
        point_(point)
    { }

    /// Intersection test for an octant.
    Intersection TestOctant(const BoundingBox& box, bool inside) override;
    /// Intersection test for objects.
    void TestObjects(OctreeObject** start, OctreeObject** end, bool inside) override;

    Vector3 point_;
};

class SphereOctreeQuery final : public OctreeQuery
{
public:
    /// Construct with sphere and query parameters.
    SphereOctreeQuery(ea::vector<OctreeObject*>& result,
        const Sphere& sphere, const OctreeObject* ignore = nullptr, const OctreeMatcher* matcher = nullptr) :
        OctreeQuery(result, ignore, matcher),
        sphere_(sphere)
    {}

    /// Intersection test for an octant.
    Intersection TestOctant(const BoundingBox& box, bool inside) override;
    /// Intersection test for objects.
    void TestObjects(OctreeObject** start, OctreeObject** end, bool inside) override;

    /// Sphere.
    Sphere sphere_;
};

class BoxOctreeQuery final : public OctreeQuery
{
public:
    /// Construct with bounding box and query parameters.
    BoxOctreeQuery(ea::vector<OctreeObject*>& result,
        const BoundingBox& box, const OctreeObject* ignore = nullptr, const OctreeMatcher* matcher = nullptr) :
        OctreeQuery(result, ignore, matcher),
        box_(box)
    {}

    /// Intersection test for an octant.
    Intersection TestOctant(const BoundingBox& box, bool inside) override;
    /// Intersection test for objects.
    void TestObjects(OctreeObject** start, OctreeObject** end, bool inside) override;

    /// Bounding box.
    BoundingBox box_;
};

struct RayQueryResult
{
    /// Construct with defaults.
    RayQueryResult() noexcept = default;
    RayQueryResult(const RayQueryResult& other) noexcept = default;
    RayQueryResult(RayQueryResult&& other) noexcept = default;

    RayQueryResult& operator= (const RayQueryResult& other) noexcept = default;
    RayQueryResult& operator= (RayQueryResult&& other) noexcept = default;

    /// Test for inequality, added to prevent GCC from complaining.
    bool operator !=(const RayQueryResult& rhs) const
    {
        return position_ != rhs.position_ ||
            normal_ != rhs.normal_ ||
            !Equals(distance_, rhs.distance_) ||
            object_ != rhs.object_;
    }

    /// Hit position in world space.
    Vector3 position_;
    /// Hit planeNormal in world space. Negation of ray direction if per-triangle data not available.
    Vector3 normal_;
    /// Distance from ray origin.
    float distance_{ 0.0f };
    OctreeObject* object_{ nullptr };
};

class RayOctreeQuery
{
    NON_COPYABLE(RayOctreeQuery)
    NON_MOVEABLE(RayOctreeQuery)
public:
    /// Construct with ray and query parameters.
    RayOctreeQuery(ea::vector<RayQueryResult>& result, const Ray& ray,
        float maxDistance = Math::M_INFINITE,
        const OctreeObject* ignore = nullptr,
        const OctreeMatcher* matcher = nullptr) :
        result_(result),
        ray_(ray),
        maxDistance_(maxDistance),
        ignore_(ignore),
        matcher_(matcher)
    { }

    bool Matches(const OctreeObject* object) const
    {
        if (!matcher_)
            return true;
        return matcher_->Matches(object);
    }
    /// Result vector reference.
    ea::vector<RayQueryResult>& result_;
    /// Ray.
    Ray ray_;
    /// Maximum ray distance.
    float maxDistance_;
    const OctreeObject* ignore_;
    const OctreeMatcher* matcher_;
};

}
