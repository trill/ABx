/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include "Vector4.h"
#include <string>

namespace Math {

class Matrix4;

class Quaternion
{
public:
    constexpr Quaternion() noexcept = default;
    constexpr Quaternion(float w, float x, float y, float z) noexcept :
        w_(w),
        x_(x),
        y_(y),
        z_(z)
    {}
    constexpr explicit Quaternion(const std::array<float, 4>& values) noexcept :
        w_(values[0]),
        x_(values[1]),
        y_(values[2]),
        z_(values[3])
    {}
    explicit Quaternion(const std::array<float, 3>& values) noexcept :
        // 0 = Pitch, 1 = Yaw, 2 = Roll
        Quaternion(values[0], values[1], values[2])
    {}
    Quaternion(float x, float y, float z);
#if defined(HAVE_DIRECTX_MATH)
    Quaternion(const XMath::XMVECTOR& q) :  //NOLINT(hicpp-explicit-conversions)
        w_(XMath::XMVectorGetW(q)),
        x_(XMath::XMVectorGetX(q)),
        y_(XMath::XMVectorGetY(q)),
        z_(XMath::XMVectorGetZ(q))
    { }
#endif
    explicit Quaternion(const Vector3& eulerAngles) :
        // x = Pitch, y = Yaw, z = Roll
        Quaternion(eulerAngles.x_, eulerAngles.y_, eulerAngles.z_)
    {}
    /// Parse from string
    explicit Quaternion(const std::string& str);

    /// Create a Quaternion from Axis and Angle. Angle is Rad
    static Quaternion FromAxisAngle(const Vector3& axis, float angle);
    static Quaternion FromLookAt(const Vector3& from, const Vector3& to);

    constexpr operator std::array<float, 4>() const //NOLINT(hicpp-explicit-conversions)
    {
        return { x_, y_, z_, w_ };
    }
#if defined(HAVE_DIRECTX_MATH)
    /// Cast to XMVECTOR
    operator XMath::XMVECTOR() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::XMVectorSet(x_, y_, z_, w_);
    }
    constexpr operator XMath::XMFLOAT4() const //NOLINT(hicpp-explicit-conversions)
    {
        return { x_, y_, z_, w_ };
    }
#endif

    [[nodiscard]] bool IsIdentity() const;

       // Negate
    Quaternion operator -() const
    {
#if defined(_XM_SSE3_INTRINSICS_)
        // https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html#
        return { _mm_xor_ps(_mm_loadu_ps(&w_), _mm_castsi128_ps(_mm_set1_epi32((int)0x80000000UL))) };
#else
        return { -w_, -x_, -y_, -z_ };
#endif
    }

    /// Test for equality
    constexpr bool operator ==(const Quaternion& rhs) const { return Equals(rhs); }
    /// Test for inequality
    constexpr bool operator !=(const Quaternion& rhs) const { return !Equals(rhs); }
    constexpr Quaternion& operator+=(const Quaternion& v)
    {
        x_ += v.x_;
        y_ += v.y_;
        z_ += v.z_;
        w_ += v.w_;
        return *this;
    }
    constexpr Quaternion& operator-=(const Quaternion& v)
    {
        x_ -= v.x_;
        y_ -= v.y_;
        z_ -= v.z_;
        w_ -= v.w_;
        return *this;
    }

    constexpr Quaternion operator+(const Quaternion& v) const
    {
        return { w_ + v.w_, x_ + v.x_, y_ + v.y_, z_ + v.z_ };
    }
    constexpr Quaternion operator-(const Quaternion& v) const
    {
        return { w_ - v.w_, x_ - v.x_, y_ - v.y_, z_ - v.z_ };
    }
    friend constexpr Quaternion operator*(const Quaternion& v, float n)
    {
        return { v.w_ * n, v.x_ * n, v.y_ * n, v.z_ * n };
    }
    friend constexpr Quaternion operator*(float n, const Quaternion& v)
    {
        return { v.w_ * n, v.x_ * n, v.y_ * n, v.z_ * n };
    }
    friend constexpr Quaternion operator/(const Quaternion& v, float n)
    {
        return { v.w_ / n, v.x_ / n, v.y_ / n, v.z_ / n };
    }
    friend constexpr Quaternion operator/(float n, const Quaternion& v)
    {
        return { v.w_ / n, v.x_ / n, v.y_ / n, v.z_ / n };
    }
    /// Multiply Quaternions
    /// https://www.3dgep.com/understanding-quaternions/#Quaternion_Products
    friend Quaternion operator*(const Quaternion& lhs, const Quaternion& rhs);
    Quaternion& operator*=(const Quaternion& rhs);

    Vector3 operator *(const Vector3& rhs) const
    {
        const Vector3 qVec(x_, y_, z_);
        const Vector3 cross1(qVec.CrossProduct(rhs));
        const Vector3 cross2(qVec.CrossProduct(cross1));

        return rhs + 2.0f * (cross1 * w_ + cross2);
    }
    /// Test for equality with another vector with epsilon.
    [[nodiscard]] constexpr bool Equals(const Quaternion& rhs) const
    {
        return Math::Equals(x_, rhs.x_) && Math::Equals(y_, rhs.y_) && Math::Equals(z_, rhs.z_) && Math::Equals(w_, rhs.w_);
    }

    [[nodiscard]] Quaternion Inverse() const;
    [[nodiscard]] Quaternion Conjugate() const;

    /// Return Axis and Angle
    /// x, y, z -> Axis
    /// w -> Angle
    [[nodiscard]] Vector4 AxisAngle() const;

    /// Convert to Euler angles
    /// x = roll
    /// y = pitch
    /// z = yaw
    [[nodiscard]] Vector3 EulerAngles() const;
    [[nodiscard]] float YawAngle() const;
    [[nodiscard]] float PitchAngle() const;
    [[nodiscard]] float RollAngle() const;
    void SetYawAngle(float value);
    void SetPitchAngle(float value);
    void SetRollAngle(float value);
    [[nodiscard]] Quaternion Normal() const;
    [[nodiscard]] Quaternion Slerp(const Quaternion& rhs, float t) const;
    [[nodiscard]] Quaternion Nlerp(const Quaternion& rhs, float t, bool shortestPath) const;
    void Normalize();
    [[nodiscard]] float LengthSqr() const;
    [[nodiscard]] float Length() const;
    [[nodiscard]] float DotProduct(const Quaternion& rhs) const
    {
        return w_ * rhs.w_ + x_ * rhs.x_ + y_ * rhs.y_ + z_ * rhs.z_;
    }
    /// Return rotation matrix
    [[nodiscard]] Matrix4 GetMatrix() const;

    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << w_ << " " << x_ << " " << y_ << " " << z_;
        return ss.str();
    }
    friend std::ostream& operator << (std::ostream& os, const Quaternion& value)
    {
        return os << value.ToString();
    }

    float w_{ 1.0f };
    float x_{ 0.0f };
    float y_{ 0.0f };
    float z_{ 0.0f };
};

static constexpr Quaternion Quaternion_Identity;

}
