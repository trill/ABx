/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MathDefs.h"
#include <vector>
#include <sstream>
#include <limits>
#include <cmath>
#include <type_traits>

namespace Math {

template <typename T>
constexpr T Clamp(T value, T min, T max)
{
    if (value < min)
        return min;
    if (value > max)
        return max;
    return value;
}

/// Check whether two values are equal within accuracy.
template <typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs &&
            lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

template <typename T>
constexpr bool Equals(T lhs, T rhs, T epsilon)
{
    return lhs + epsilon >= rhs && lhs - epsilon <= rhs;
}

template <typename T>
constexpr bool IsInfinite(T value)
{
    static_assert(std::numeric_limits<T>::has_infinity, "T has no infinity");
    return Equals(value, std::numeric_limits<T>::infinity());
}

template <typename T>
constexpr bool IsNegInfinite(T value)
{
    static_assert(std::numeric_limits<T>::has_infinity, "T has no infinity");
    static_assert(std::numeric_limits<T>::is_iec559, "T is not iec559");
    return Equals(value, -std::numeric_limits<T>::infinity());
}

template <typename T, typename U>
constexpr T Lerp(T lhs, T rhs, U i)
{
    return lhs * (static_cast<U>(1.0) - i) + rhs * i;
}

/// Return fractional part of passed value in range [0, 1].
template <typename T>
inline T Fract(T value)
{
    return value - floor(value);
}

template <typename T>
constexpr T DegToRad(T deg)
{
    return deg * (static_cast<T>(M_PIF / 180.0));
}

template <typename T>
constexpr T RadToDeg(T rad)
{
    return (rad / static_cast<T>(M_PIF)) * (static_cast<T>(180.0));
}

/// Make angle between 0 and 2 * Pi.
inline void NormalizeAngle(float& angle)
{
    angle = fmodf(angle, M_TWOPI);
    angle = fmodf(angle + M_TWOPI, M_TWOPI);
}

inline float NormalizedAngle(float angle)
{
    NormalizeAngle(angle);
    return angle;
}

}
