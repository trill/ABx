/**
 * Copyright 2017-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <sstream>
#include "Point.h"

namespace Math {

template<typename T>
class Rect
{
public:
    constexpr Rect() noexcept = default;
    constexpr Rect(const Rect&) noexcept = default;
    constexpr Rect(Rect&&) noexcept = default;
    constexpr Rect(T left, T top, T right, T bottom) noexcept :
        left_(left),
        top_(top),
        right_(right),
        bottom_(bottom)
    { }
    constexpr Rect(const Point<T>& topLeft, const Point<T>& bottomRight) noexcept :
        left_(topLeft.x_),
        top_(topLeft.y_),
        right_(bottomRight.x_),
        bottom_(bottomRight.y_)
    { }

    ~Rect() = default;

    constexpr Rect& operator =(const Rect&) = default;
    constexpr Rect& operator =(Rect&&) noexcept = default;

    constexpr bool operator ==(const Rect<T>& rect) const
    {
        return Equals(rect);
    }
    constexpr bool operator !=(const Rect<T>& rect) const
    {
        return !Equals(rect);
    }

    constexpr bool Equals(const Rect<T>& rect)
    {
        return Equals(left_, rect.left_) && Equals(top_, rect.top_) &&
            Equals(right_, rect.right_) && Equals(bottom_, rect.bottom_);
    }

    constexpr T Width() const
    {
        return right_ - left_;
    }
    constexpr T Height() const
    {
        return bottom_ - top_;
    }
    constexpr Point<T> TopLeft() const { return Point<T>(top_, left_); }
    constexpr Point<T> BottomRight() const { return Point<T>(bottom_, right_); }
    constexpr bool IsInside(T x, T y) const
    {
        return (left_ < x) && (right_ > x) &&
            (top_ < y) && (bottom_ > y);
    }
    /// Subtracts units from the left and top and adds units to the right and bottom.
    constexpr void Inflate(T x, T y)
    {
        left_ -= x;
        top_ -= y;
        right_ += x;
        bottom_ += y;
    }
    constexpr void Inflate(T v)
    {
        left_ -= v;
        top_ -= v;
        right_ += v;
        bottom_ += v;
    }
    constexpr void Offset(T x, T y)
    {
        left_ += x;
        right_ += x;
        top_ += y;
        bottom_ += y;
    }
    constexpr void Offset(T v)
    {
        left_ += v;
        right_ += v;
        top_ += v;
        bottom_ += v;
    }
    void Normalize()
    {
        if (left_ > right_)
            std::swap(left_, right_);
        if (top_ > bottom_)
            std::swap(top_, bottom_);
    }
    Rect<T> Normalized() const
    {
        Rect<T> result = *this;
        result.Normalize();
        return result;
    }

    [[nodiscard]] const T* Data() const { return &left_; }
    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << left_ << " " << top_ << " " << right_ << " " << bottom_;
        return ss.str();
    }
    friend std::ostream& operator << (std::ostream& os, const Rect<T>& value)
    {
        return os << value.ToString();
    }

    T left_{ };
    T top_{ };
    T right_{ };
    T bottom_{ };
};

using FloatRect = Rect<float>;
using IntRect = Rect<int>;

}
