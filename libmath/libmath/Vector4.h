/**
 * Copyright 2017-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"

namespace Math {

class Vector4
{
public:
    constexpr Vector4() noexcept = default;

    constexpr Vector4(const Vector4& vector) noexcept = default;
    constexpr Vector4(Vector4&& vector) noexcept = default;

    constexpr Vector4(float x, float y, float z, float w) noexcept :
        x_(x),
        y_(y),
        z_(z),
        w_(w)
    { }

    explicit constexpr Vector4(const Vector3& vector, float w = 1.0f) noexcept :
        x_(vector.x_),
        y_(vector.y_),
        z_(vector.z_),
        w_(w)
    { }

    explicit constexpr Vector4(const std::array<float, 4>& values) noexcept :
        x_(values[0]),
        y_(values[1]),
        z_(values[2]),
        w_(values[3])
    { }

#if defined(HAVE_DIRECTX_MATH)
    Vector4(const XMath::XMVECTOR& vector) noexcept : //NOLINT(hicpp-explicit-conversions)
        x_(XMath::XMVectorGetX(vector)),
        y_(XMath::XMVectorGetY(vector)),
        z_(XMath::XMVectorGetZ(vector)),
        w_(XMath::XMVectorGetW(vector))
    { }
#endif

    /// Parse from string
    explicit Vector4(const std::string& str);

    ~Vector4() = default;

    constexpr Vector4& operator =(const Vector4& vector) = default;
    constexpr Vector4& operator =(Vector4&&) noexcept = default;

    constexpr operator std::array<float, 4>() const //NOLINT(hicpp-explicit-conversions)
    {
        return{ x_, y_, z_, w_ };
    }
#if defined(HAVE_DIRECTX_MATH)
    /// Cast to XMVECTOR
    operator XMath::XMVECTOR() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::XMVectorSet(x_, y_, z_, w_);
    }
    constexpr operator XMath::XMFLOAT4() const //NOLINT(hicpp-explicit-conversions)
    {
        return{ x_, y_, z_, w_ };
    }
#endif

    constexpr bool operator ==(const Vector4& vector) const { return Equals(vector); }
    constexpr bool operator !=(const Vector4& vector) const { return !Equals(vector); }

    constexpr Vector4& operator+=(const Vector4& v)
    {
        x_ += v.x_;
        y_ += v.y_;
        z_ += v.z_;
        w_ += v.w_;
        return *this;
    }
    constexpr Vector4& operator-=(const Vector4& v)
    {
        x_ -= v.x_;
        y_ -= v.y_;
        z_ -= v.z_;
        w_ -= v.w_;
        return *this;
    }
    constexpr Vector4 operator+(const Vector4& v) const
    {
        return { x_ + v.x_, y_ + v.y_, z_ + v.z_, w_ + v.w_ };
    }
    constexpr Vector4 operator-(const Vector4& v) const
    {
        return { x_ - v.x_, y_ - v.y_, z_ - v.z_, w_ - v.w_ };
    }
    friend constexpr Vector4 operator*(const Vector4& v, float n)
    {
        return { v.x_ * n, v.y_ * n, v.z_ * n, v.w_ * n };
    }
    friend constexpr Vector4 operator*(float n, const Vector4& v)
    {
        return { v.x_ * n, v.y_ * n, v.z_ * n, v.w_ * n };
    }
    friend constexpr Vector4 operator/(const Vector4& v, float n)
    {
        return { v.x_ / n, v.y_ / n, v.z_ / n, v.w_ / n };
    }
    friend constexpr Vector4 operator/(float n, const Vector4& v)
    {
        return { v.x_ / n, v.y_ / n, v.z_ / n, v.w_ / n };
    }

    [[nodiscard]] constexpr bool Equals(const Vector4& rhs) const
    {
        return Math::Equals(x_, rhs.x_) && Math::Equals(y_, rhs.y_) && Math::Equals(z_, rhs.z_) && Math::Equals(w_, rhs.w_);
    }
    [[nodiscard]] float LengthSqr() const;
    [[nodiscard]] float Length() const;
    [[nodiscard]] float Distance(const Vector4& v) const;
    [[nodiscard]] Vector4 Normal() const;
    void Normalize();

    [[nodiscard]] const float* Data() const { return &x_; }
    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << x_ << " " << y_ << " " << z_ << " " << w_;
        return ss.str();
    }
    friend std::ostream& operator << (std::ostream& os, const Vector4& value)
    {
        return os << value.ToString();
    }

    float x_{ 0.0f };
    float y_{ 0.0f };
    float z_{ 0.0f };
    float w_{ 0.0f };
};

static constexpr Vector4 Vector4_Zero;
static constexpr Vector4 Vector4_One(1.0f, 1.0f, 1.0f, 1.0f);
static constexpr Vector4 Vector4_UnitX(1.0f, 0.0f, 0.0f, 0.0f);
static constexpr Vector4 Vector4_UnitY(0.0f, 1.0f, 0.0f, 0.0f);
static constexpr Vector4 Vector4_UnitZ(0.0f, 0.0f, 1.0f, 0.0f);
static constexpr Vector4 Vector4_UnitW(0.0f, 0.0f, 0.0f, 1.0f);

}
