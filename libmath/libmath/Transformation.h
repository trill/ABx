/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MathConfig.h"
#include "Vector3.h"
#include "Quaternion.h"
#include "Matrix4.h"

namespace Math {

class Transformation
{
public:
    constexpr Transformation() = default;
    constexpr Transformation(const Transformation& other) noexcept = default;
    constexpr Transformation(Transformation&& other) noexcept = default;
    Transformation(const Vector3& pos, float rot, const Vector3& scale) :
        position_(pos),
        orientation_(Quaternion::FromAxisAngle(Vector3_UnitY, rot)),
        scale_(scale)
    {}
    constexpr Transformation(const Vector3& pos, const Quaternion& rot, const Vector3& scale) :
        position_(pos),
        orientation_(rot),
        scale_(scale)
    {}
    // Create from transformation matrix
    explicit Transformation(const Matrix4& matrix);

    ~Transformation() = default;

    constexpr Transformation& operator=(const Transformation& other) = default;
    constexpr Transformation& operator=(Transformation&& other) = default;

    // Return transformed transformation
    Transformation operator*(const Matrix4& rhs) const;

    [[nodiscard]] float GetYRotation() const;
    void SetYRotation(float rad);
    void LookAt(const Vector3& lookAt, const Vector3& up = Vector3_UnitY);
    void Transform(const Matrix4& matrix);

    /// Get transformation matrix
    [[nodiscard]] Matrix4 GetMatrix() const;
    /// Use rot instead of oriention_
    [[nodiscard]] Matrix4 GetMatrix(const Quaternion& rot) const;
    void Move(float speed, const Vector3& amount);
    void MoveXYZ(float speed, const Vector3& amount);
    void Turn(float yAngle);
    friend std::ostream& operator << (std::ostream& os, const Transformation& value)
    {
        return os << " position " << value.position_ << ", scale " << value.scale_ << ", orientation " << value.orientation_;
    }

    Vector3 position_{ Vector3_Zero };
    Quaternion orientation_{ Quaternion_Identity };
    Vector3 scale_{ Vector3_One };
};

}
