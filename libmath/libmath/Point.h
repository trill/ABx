/**
 * Copyright 2017-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sstream>
#include "MathUtils.h"
#include <type_traits>

namespace Math {

template<typename T>
class Point
{
public:
    constexpr Point() noexcept = default;
    constexpr Point(const Point& line) noexcept = default;
    constexpr Point(Point&& line) noexcept = default;
    constexpr Point(T x, T y) noexcept :
        x_(x),
        y_(y)
    { }
    ~Point() = default;
    Point& operator = (const Point& other) = default;
    Point& operator = (Point&& other) noexcept = default;

    // Negation
    constexpr Point<T> operator -() const
    {
        return Point<T>(-x_, -y_);
    }

    constexpr bool operator ==(const Point<T>& point) const
    {
        return Equals(point);
    }
    constexpr bool operator !=(const Point<T>& point) const
    {
        return !Equals(point);
    }

    constexpr bool Equals(const Point<T>& point)
    {
        return Equals(x_, point.x_) && Equals(y_, point.y_);
    }

    template<typename U>
    constexpr Point<T>& operator+=(const Point<U>& v) requires std::is_convertible<T, U>::value
    {
        x_ += v.x_;
        y_ += v.y_;
        return *this;
    }
    template<typename U>
    constexpr Point<T>& operator-=(const Point<U>& v) requires std::is_convertible<T, U>::value
    {
        x_ -= v.x_;
        y_ -= v.y_;
        return *this;
    }
    template<typename U>
    constexpr Point<T>& operator*=(const Point<U>& v) requires std::is_convertible<T, U>::value
    {
        x_ *= v.x_;
        y_ *= v.y_;
        return *this;
    }
    template<typename U>
    constexpr Point<T>& operator/=(const Point<U>& v) requires std::is_convertible<T, U>::value
    {
        x_ /= v.x_;
        y_ /= v.y_;
        return *this;
    }
    template<typename U>
    constexpr Point<T>& operator*=(U v) requires std::is_convertible<T, U>::value
    {
        x_ *= v;
        y_ *= v;
        return *this;
    }
    template<typename U>
    constexpr Point<T>& operator/=(U v) requires std::is_convertible<T, U>::value
    {
        x_ /= v;
        y_ /= v;
        return *this;
    }

    template<typename U>
    constexpr Point<T> operator+(const Point<U>& v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ + v.x_, y_ + v.y_);
    }
    template<typename U>
    constexpr Point<T> operator-(const Point<U>& v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ - v.x_, y_ - v.y_);
    }
    template<typename U>
    constexpr Point<T> operator+(U v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ + v, y_ + v);
    }
    template<typename U>
    constexpr Point<T> operator-(U v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ - v, y_ - v);
    }
    template<typename U>
    constexpr Point<T> operator*(const Point<U>& v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ * v.x_, y_ * v.y_);
    }
    template<typename U>
    constexpr Point<T> operator/(const Point<U>& v) const requires std::is_convertible<T, U>::value
    {
        return Point<T>(x_ / v.x_, y_ / v.y_);
    }

    constexpr void Offset(T x, T y)
    {
        x_ += x;
        y_ += y;
    }
    constexpr void Offset(T v)
    {
        x_ += v;
        y_ += v;
    }

    [[nodiscard]] const T* Data() const { return &x_; }

    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << x_ << " " << y_;
        return ss.str();
    }

    friend std::ostream& operator << (std::ostream& os, const Point<T>& value)
    {
        return os << value.ToString();
    }

    T x_{ };
    T y_{ };
};

using Vector2 = Point<float>;
using IntVector2 = Point<int>;

}
