/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include "Matrix4.h"
#include <eastl.hpp>
#include <sa/Assert.h>

namespace Math {

/// A 3D Mesh
class Mesh
{
public:
    Mesh() noexcept = default;
    Mesh(const Mesh& other) noexcept = default;
    Mesh(const Mesh& other, const Matrix4& matrix) :
        vertexData_(other.vertexData_),
        indexData_(other.indexData_),
        vertexCount_(other.vertexCount_),
        indexCount_(other.indexCount_),
        matrix_(matrix)
    { }
    Mesh(Mesh&& other) noexcept = default;
    explicit Mesh(const Vector3& vector) :
        vertexCount_(1),
        matrix_(Matrix4_Identity)
    {
        vertexData_.push_back(vector);
    }
    explicit Mesh(ea::vector<Vector3> vertices) :
        vertexData_(std::move(vertices)),
        vertexCount_(vertexData_.size()),
        matrix_(Matrix4_Identity)
    { }
    Mesh(ea::vector<Vector3> vertices, ea::vector<uint32_t> indices) :
        vertexData_(std::move(vertices)),
        indexData_(std::move(indices)),
        vertexCount_(vertexData_.size()),
        indexCount_(indexData_.size()),
        matrix_(Matrix4_Identity)
    {}
    ~Mesh() = default;

    Mesh& operator= (const Mesh& other) noexcept = default;
    Mesh& operator= (Mesh&& other) noexcept = default;

    [[nodiscard]] bool IsDefined() const
    {
        return vertexCount_ != 0;
    }
    void Reset();

    void AddTriangle(uint32_t i1, uint32_t i2, uint32_t i3);
    /// Naive check whether we have triangles or not.
    [[nodiscard]] bool IsTriangles() const
    {
        if (indexCount_)
            return ((indexCount_ % 3) == 0);
        return (vertexCount_ % 3) == 0;
    }
    [[nodiscard]] size_t GetTriangleCount() const
    {
        if (IsTriangles())
            return GetCount() / 3;
        return 0;
    }
    [[nodiscard]] ea::array<Vector3, 3> GetTriangle(size_t i) const
    {
        return {
            GetVertex(i * 3),
            GetVertex(i * 3 + 1),
            GetVertex(i * 3 + 2)
        };
    }
    [[nodiscard]] Vector3 GetTriangleNormal(size_t i) const;
    [[nodiscard]] Vector3 GetClosestPointOnTriangle(size_t i, const Vector3& pos) const;
    [[nodiscard]] float GetDistanceToTriangle(size_t i, const Vector3& pos) const;
    [[nodiscard]] size_t GetClosestTriangleIndex(const Vector3& pos) const;

    void AddQuad(uint32_t i1, uint32_t i2, uint32_t i3, uint32_t i4);
    [[nodiscard]] bool IsQuads() const
    {
        if (indexCount_)
            return ((indexCount_ % 4) == 0);
        return (vertexCount_ % 4) == 0;
    }
    [[nodiscard]] size_t GetQuadsCount() const
    {
        if (IsQuads())
            return GetCount() / 4;
        return 0;
    }
    [[nodiscard]] ea::array<Vector3, 4> GetQuad(size_t i) const
    {
        return {
            GetVertex(i * 4),
            GetVertex(i * 4 + 1),
            GetVertex(i * 4 + 2),
            GetVertex(i * 4 + 3)
        };
    }

    [[nodiscard]] Vector3 GetVertex(size_t index) const
    {
        if (indexCount_)
        {
            ASSERT(index < indexData_.size());
            ASSERT(indexData_[index] < vertexData_.size());
            return matrix_ * vertexData_[indexData_[index]];
        }
        return matrix_ * vertexData_[index];
    }
    // Vertext count
    [[nodiscard]] size_t GetCount() const
    {
        if (indexCount_)
            return indexCount_;
        return vertexCount_;
    }
    [[nodiscard]] const float* VertexData() const
    {
        return reinterpret_cast<const float*>(&vertexData_[0]);
    }
    // Size of vertex data in bytes
    [[nodiscard]] size_t VertexDataSize() const { return vertexCount_ * sizeof(float) * 3; }
    [[nodiscard]] const uint32_t* IndexData() const
    {
        return reinterpret_cast<const uint32_t*>(&indexData_[0]);
    }
    [[nodiscard]] size_t IndexDataSize() const { return indexCount_ * sizeof(uint32_t); }
    /// World Coordinates, that's why we need a transformation matrix
    /// Get center position. Note: Shape must be convex.
    [[nodiscard]] Vector3 Center() const;
    [[nodiscard]] Vector3 GetFarsetPointInDirection(const Vector3& direction) const;
    [[nodiscard]] float GetMinHeight() const;
    [[nodiscard]] float GetMaxHeight() const;

    /// Transformation matrix
    ea::vector<Vector3> vertexData_;
    ea::vector<uint32_t> indexData_;
    size_t vertexCount_{ 0 };
    size_t indexCount_{ 0 };
    Matrix4 matrix_;
};

}
