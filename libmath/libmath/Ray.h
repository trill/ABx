/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"

namespace Math {

class BoundingBox;
class Sphere;
class Matrix4;
class Plane;

/// Infinite straight line in three-dimensional space.
class Ray
{
public:
    constexpr Ray() noexcept = default;
    constexpr Ray(const Ray& other) noexcept = default;
    constexpr Ray(Ray&& other) noexcept = default;
    /// Construct from origin and direction.
    /// Hint: To get a Ray from one point to another use (target - origin) as direction
    Ray(const Vector3& origin, const Vector3& direction)
    {
        Define(origin, direction);
    }
    ~Ray() = default;

    /// Assign from another ray.
    Ray& operator =(const Ray& rhs) = default;
    Ray& operator = (Ray&& other) = default;

    /// Check for equality with another ray.
    constexpr bool operator ==(const Ray& rhs) const { return origin_.Equals(rhs.origin_) && direction_.Equals(rhs.direction_); }

    /// Check for inequality with another ray.
    constexpr bool operator !=(const Ray& rhs) const { return !origin_.Equals(rhs.origin_) || !direction_.Equals(rhs.direction_); }

    /// Define from origin and direction. The direction will be normalized.
    void Define(const Vector3& origin, const Vector3& direction)
    {
        origin_ = origin;
        direction_ = direction.Normal();
    }

    /// Project a point on the ray.
    [[nodiscard]] constexpr Vector3 Project(const Vector3& point) const
    {
        const Vector3 offset = point - origin_;
        return origin_ + offset.DotProduct(direction_) * direction_;
    }

    /// Return distance of a point from the ray.
    [[nodiscard]] float Distance(const Vector3& point) const
    {
        const Vector3 projected = Project(point);
        return (point - projected).Length();
    }

    /// Return closest point to another ray.
    [[nodiscard]] Vector3 ClosestPoint(const Ray& ray) const;
    /// Return hit distance to a bounding box, or infinity if no hit.
    [[nodiscard]] float HitDistance(const BoundingBox& box) const;
    /// Return hit distance to a sphere, or infinity if no hit.
    [[nodiscard]] float HitDistance(const Sphere& sphere) const;
    [[nodiscard]] float HitDistance(const Plane& plane) const;

    /// Return transformed by a matrix. This may result in a non-normalized direction.
    [[nodiscard]] Ray Transformed(const Matrix4& transform) const;
    [[nodiscard]] bool IsDefined() const { return !origin_.Equals(Vector3_Zero) && !direction_.Equals(Vector3_Zero); }
    friend std::ostream& operator << (std::ostream& os, const Ray& value)
    {
        return os << value.origin_ << " -> " << value.direction_;
    }

    /// Ray origin.
    Vector3 origin_;
    /// Ray direction.
    Vector3 direction_;
};

}
