/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MultiLine.h"

namespace Math {

MultiLine::~MultiLine() = default;

} // namespace Math
