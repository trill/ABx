/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <sa/Noncopyable.h>

namespace Math {

class Bone;

class Skeleton
{
    NON_COPYABLE(Skeleton)
    NON_MOVEABLE(Skeleton)
public:
    Skeleton();
    ~Skeleton();

    Bone* GetBone(const std::string& name);
    Bone* AddBone(std::string name, Bone* parent);
private:
    ea::map<std::string, ea::unique_ptr<Bone>> bones_;
};

} // namespace Math
