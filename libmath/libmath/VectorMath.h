/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include "Quaternion.h"
#include <sa/Iteration.h>
#include <eastl.hpp>

namespace Math {

enum class PointClass
{
    PlaneFront,
    PlaneBack,
    OnPlane
};

// Check if a point is inside a triangle
bool IsPointInTriangle(const Vector3& point, const Vector3& pa, const Vector3& pb, const Vector3& pc, float epsilon = 0.005f);
// Get closest point on line segment
Vector3 GetClosestPointOnLine(const Vector3& a, const Vector3& b, const Vector3& p);
// Find a point inside the triangle that is closest to pos
// \return A new point that is close to pos and is inside the triangle
Vector3 GetClosestPointOnTriangle(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& p);
PointClass GetPointClass(const Vector3& point, const Vector3& origin, const Vector3& normal);
/// Get face planeNormal vector.
Vector3 GetTriangleNormal(const Vector3& p1, const Vector3& p2, const Vector3& p3);
/// Make CCW -> CW and vice versa
void ReverseOrder(ea::array<Vector3, 3>& triangle);
Vector3 GetPosFromDirectionDistance(const Vector3& position, const Quaternion& direction, float distance);
Vector3 GetPosFromDirectionDistance(const Vector3& position, const Vector3& direction, float distance);
Vector3 GetPosFromLookAtDistance(const Vector3& position, const Vector3& lookAt, float distance, const Vector3& up = Vector3_UnitY);
float IntersectsRayPlane(const Vector3& rOrigin, const Vector3& rVector, const Vector3& pOrogin, const Vector3& pNormal);
float IntersectsRaySphere(const Vector3& rayOrigin, const Vector3& rayVector, const Vector3& sphereOrigin, float sphereRadius);
bool IsPointInSphere(const Vector3& point, const Vector3& sphereOrigin, float sphereRadius);
float GetDistanceToTriangle(const ea::array<Vector3, 3>& tri, const Vector3& pos);

template<typename Callback>
inline void GetTriangleIndices(int width, int height, Callback&& callback)
{
    // Create index data in clockwise order
    for (int y = 0; y < height - 1; ++y)
    {
        for (int x = 0; x < width - 1; ++x)
        {
            /*
                x+1,y
        x,y +----+----+
            | 1 /|(3)/|
            |  / |  / |
            | /  | /  |
            |/ 2 |/(4)|
      x,y+1 +----+----+
              x+1,y+1
            */
            {
                // First triangle
                int i1 = (y + 1) * width + x;
                int i2 = y * width + x;
                int i3 = (y * width) + x + 1;
                if (callback(i3, i2, i1) == Iteration::Break)
                    return;
            }

            {
                // Second triangle
                int i1 = y * width + x + 1;
                int i2 = (y + 1) * width + (x + 1);
                int i3 = (y + 1) * width + x;
                if (callback(i3, i2, i1) == Iteration::Break)
                    return;
            }
        }
    }
}

}
