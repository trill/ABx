/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Color.h"
#include <sa/StringTempl.h>

namespace Math {

Color::Color(const std::string& str)
{
    std::vector<std::string> parts = sa::Split(str, " ");
    if (parts.size() == 3)
    {
        r_ = std::stof(parts.at(0));
        g_ = std::stof(parts.at(1));
        b_ = std::stof(parts.at(2));
        a_ = 1.0f;
    }
    else if (parts.size() == 4)
    {
        r_ = std::stof(parts.at(0));
        g_ = std::stof(parts.at(1));
        b_ = std::stof(parts.at(2));
        a_ = std::stof(parts.at(3));
    }
    else
    {
        r_ = 0.0f;
        g_ = 0.0f;
        b_ = 0.0f;
        a_ = 1.0f;
    }
}

Color::Color(const sa::Color& color)
{
    std::tie(r_, g_, b_, a_) = color.ToRgba();
}

}
