/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Mesh.h"
#include <sa/Noncopyable.h>

namespace Math {

/// https://github.com/xuzebin/gjk
class Gjk
{
    NON_COPYABLE(Gjk)
    NON_MOVEABLE(Gjk)
private:
    Vector3 v_;
    Vector3 b_, c_, d_;
    /// Simplex size
    unsigned n_{ 0 };
    static constexpr int MAX_ITERATIONS = 30;
    static Vector3 TripleProduct(const Vector3& ab, const Vector3& c)
    {
        return ab.CrossProduct(c).CrossProduct(ab);
    }
    static Vector3 Support(const Mesh& mesh1, const Mesh& mesh2, const Vector3& v)
    {
        const Vector3 p1 = mesh1.GetFarsetPointInDirection(v);
        const Vector3 p2 = mesh2.GetFarsetPointInDirection(-v);  //negate v
        return p1 - p2;
    }
    bool CheckOneFaceAC(const Vector3& abc, const Vector3& ac, const Vector3& ao);
    bool CheckOneFaceAB(const Vector3& abc, const Vector3& ab, const Vector3& ao);
    bool CheckTwoFaces(Vector3& abc, Vector3& acd, Vector3& ac, Vector3& ab, Vector3& ad, const Vector3& ao);
    bool Update(const Vector3& a);
public:
    Gjk() = default;
    ~Gjk() = default;

    static inline bool StaticIntersects(const Mesh& mesh1, const Mesh& mesh2)
    {
        Gjk gjk;
        return gjk.Intersects(mesh1, mesh2);
    }

    bool Intersects(const Mesh& mesh1, const Mesh& mesh2);
};

}
