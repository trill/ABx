/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BoundingBox.h"
#include "Mesh.h"
#include <eastl.hpp>
#include "MeshObject.h"

namespace Math {

class HeightMap;
class TriangleMesh;

class ConvexHull : public Mesh, public MeshObject
{
private:
    void BuildHull(const ea::vector<Vector3>& vertices);
    ea::shared_ptr<Mesh> GenerateMesh() const final;
public:
    ConvexHull() = default;
    explicit ConvexHull(const ea::vector<Vector3>& vertices);
    ConvexHull(const ConvexHull& other) = default;
    ConvexHull(const ConvexHull& other, const Matrix4& matrix) :
        Mesh(other, matrix),
        boundingBox_(other.boundingBox_)
    {}
    ConvexHull(ConvexHull&& other) noexcept = default;
    ~ConvexHull() override = default;

    ConvexHull& operator= (const ConvexHull& other) = default;
    ConvexHull& operator= (ConvexHull&& other) noexcept = default;

    [[nodiscard]] BoundingBox GetBoundingBox() const
    {
        return boundingBox_;
    }
    [[nodiscard]] Intersection IsInside(const Vector3& point) const;
    [[nodiscard]] ConvexHull Transformed(const Matrix4& transformation) const;
    bool Collides(const Sphere& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const BoundingBox& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const;
    bool Collides(const HeightMap& b2, const Vector3& velocity, Vector3& move) const;

    BoundingBox boundingBox_;
};

}
