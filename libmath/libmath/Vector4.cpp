/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Vector4.h"
#include "MathUtils.h"
#include <sa/StringTempl.h>

namespace Math {

Vector4::Vector4(const std::string& str)
{
    const std::vector<std::string> parts = sa::Split(str, " ");
    if (parts.size() > 2)
    {
        x_ = std::stof(parts.at(0));
        y_ = std::stof(parts.at(1));
        z_ = std::stof(parts.at(2));
        if (parts.size() > 3)
            w_ = std::stof(parts.at(3));
        else
            w_ = 1.0f;
    }
    else
    {
        x_ = 0.0f;
        y_ = 0.0f;
        z_ = 0.0f;
        w_ = 0.0f;
    }
}

float Vector4::LengthSqr() const
{
    return x_*x_ + y_*y_ + z_*z_ + w_*w_;
}

float Vector4::Length() const
{
    return sqrt(x_*x_ + y_*y_ + z_*z_ + w_*w_);
}

float Vector4::Distance(const Vector4& v) const
{
    return (*this - v).Length();
}

void Vector4::Normalize()
{
    float length = Length();
    x_ /= length;
    y_ /= length;
    z_ /= length;
    w_ /= length;
}

Vector4 Vector4::Normal() const
{
    return *this / Length();
}

}
