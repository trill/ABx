/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Transformation.h"
#include <sa/Noncopyable.h>

namespace Math {

class Node
{
    NON_COPYABLE(Node)
    NON_MOVEABLE(Node)
public:
    explicit Node(const Node* parent);
    ~Node();

    [[nodiscard]] Matrix4 GetMatrix() const;
    [[nodiscard]] Transformation GetWorldTransformation() const;

    Transformation transformation_;
private:
    const Node* const parent_;
};

} // namespace Math
