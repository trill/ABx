/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Camera.h"

namespace Math {

Camera::Camera() = default;

void Camera::Resize(int w, int h)
{
    aspectRatio_ = (float)w / (h ? (float)h : 1.0f);
    size_ = { w, h };
    projectionDirty_ = true;
}

void Camera::SetNearClip(float value)
{
    if (Equals(nearClip_, value))
        return;
    nearClip_ = value;
    projectionDirty_ = true;
}

void Camera::SetFarClip(float value)
{
    if (Equals(farClip_, value))
        return;
    farClip_ = value;
    projectionDirty_ = true;
}

void Camera::SetFov(float value)
{
    if (Equals(fov_, value))
        return;
    fov_ = NormalizedAngle(value);
    projectionDirty_ = true;
}

void Camera::SetZoom(float value)
{
    if (Equals(zoom_, value))
        return;
    zoom_ = value;
    projectionDirty_ = true;
}

void Camera::ApplyAngles()
{
    if (!Math::Equals(yaw_, 0.0f))
    {
        transformation_.orientation_.SetYawAngle(transformation_.orientation_.YawAngle() + yaw_);
        yaw_ = 0.0f;
    }
    if (!Math::Equals(pitch_, 0.0f))
    {
        transformation_.orientation_.SetPitchAngle(transformation_.orientation_.PitchAngle() + pitch_);
        pitch_ = 0.0f;
    }
}

Frustum Camera::GetFrustum() const
{
    Frustum result;
    result.Define(fov_, aspectRatio_, zoom_, nearClip_, farClip_, GetWorldTransformation());
    return result;
}

Matrix4 Camera::GetWorldTransformation() const
{
    // Note: view matrix is unaffected by scale
    return { transformation_.position_, GetDirection(), Vector3_One };
}

Quaternion Camera::GetDirection() const
{
    const float yaw = reverse_ ? yaw_ + M_PIF : yaw_;
    const Quaternion rot = Quaternion::FromAxisAngle(Vector3_UnitY, transformation_.orientation_.YawAngle() + yaw);
    return rot * Quaternion::FromAxisAngle(Vector3_UnitX, transformation_.orientation_.PitchAngle() + pitch_);
}

Matrix4 Camera::GetView() const
{
    Matrix4 worldTransform = GetWorldTransformation();
    return flipZ_ ? (Matrix4_FlipZ * worldTransform).Inverse() : worldTransform.Inverse();
}

const Matrix4& Camera::GetProjection() const
{
    if (projectionDirty_)
    {
        projection_ = Matrix4::FromPerspective(fov_, aspectRatio_, nearClip_, farClip_, zoom_);
        projectionDirty_ = false;
    }
    return projection_;
}

} // namespace Math
