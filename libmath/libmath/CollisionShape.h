/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector3.h"
#include "Matrix4.h"
#include "MathDefs.h"
#include "Mesh.h"
#include <eastl.hpp>
#include <limits>
#include <sa/Assert.h>
#include <sa/Compiler.h>
#include "BoundingBox.h"
#include "HeightMap.h"
#include "ConvexHull.h"
#include "TriangleMesh.h"
#include <sa/Noncopyable.h>

namespace Math {

enum class ShapeType
{
    BoundingBox,
    Sphere,
    ConvexHull,
    TriangleMesh,
    HeightMap,

    None = 99
};

struct CollisionManifold
{
    // Input
    Vector3 position;
    Vector3 velocity;
    Vector3 radius;

    // Output
    Vector3 nearestIntersectionPoint;
    Vector3 nearestPolygonIntersectionPoint;
    float nearestDistance{ M_INFINITE };
    bool stuck{ false };
    bool foundCollision{ false };
};

class AbstractCollisionShape
{
    NON_COPYABLE(AbstractCollisionShape)
    NON_MOVEABLE(AbstractCollisionShape)
public:
    explicit AbstractCollisionShape(ShapeType type) :
        shapeType_(type)
    {}
    virtual ~AbstractCollisionShape();
    /// Create a transformed version. Requires that the wrapped class has a copy constructor.
    [[nodiscard]] ea::unique_ptr<Math::AbstractCollisionShape> GetTranformedShapePtr(const Matrix4& matrix) const;

    /// AABB
    [[nodiscard]] virtual BoundingBox GetWorldBoundingBox(const Matrix4& transform) const = 0;
    [[nodiscard]] virtual BoundingBox GetBoundingBox() const = 0;

    // Check for collision
    // \param[in] other The other object
    // \param[in] transformation The transformation of the other object
    // \param[in] velocity Our velocity
    // \param[out] move May return some position to avoid collision
    bool Collides(const AbstractCollisionShape& other, const Matrix4& transformation, const Vector3& velocity, Vector3& move) const;
    virtual bool Collides(const Matrix4& transformation, const BoundingBox& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const Matrix4& transformation, const Sphere& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const Matrix4& transformation, const HeightMap& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const Matrix4& transformation, const ConvexHull& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const Matrix4& transformation, const TriangleMesh& other, const Vector3& velocity, Vector3& move) const = 0;

    virtual bool Collides(const BoundingBox& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const Sphere& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const HeightMap& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const ConvexHull& other, const Vector3& velocity, Vector3& move) const = 0;
    virtual bool Collides(const TriangleMesh& other, const Vector3& velocity, Vector3& move) const = 0;

    [[nodiscard]] virtual Mesh GetMesh() const = 0;
    bool GetManifold(CollisionManifold&, const Matrix4& transformation) const;

    ShapeType shapeType_;
};

template <typename T>
class CollisionShape final : public AbstractCollisionShape
{
private:
    ea::shared_ptr<T> object_;
public:
    /// Ctor. Create new shape
    template<typename... _CArgs>
    explicit CollisionShape(ShapeType type, _CArgs&&... _Args) :
        AbstractCollisionShape(type),
        object_(ea::make_shared<T>(std::forward<_CArgs>(_Args)...))
    { }
    /// Ctor. Assign existing shape
    explicit CollisionShape(ShapeType type, ea::shared_ptr<T> ptr) :
        AbstractCollisionShape(type),
        object_(std::move(ptr))
    { }
    /// Create a transformed version. Requires that T has a copy constructor.
    explicit CollisionShape(const CollisionShape<T>& other, const Matrix4& transformation) :
        AbstractCollisionShape(other.shapeType_),
        object_(ea::make_shared<T>(other.Object().Transformed(transformation)))
    { }

    [[nodiscard]] BoundingBox GetWorldBoundingBox(const Matrix4& transform) const override
    {
        ASSERT(object_);
        return object_->GetBoundingBox().Transformed(transform);
    }

    [[nodiscard]] BoundingBox GetBoundingBox() const override
    {
        ASSERT(object_);
        return object_->GetBoundingBox();
    }

    // Object must be in local space
    /// @param transformation Our transformation matrix
    /// @param other The other object transformed to world coordinates
    /// @param velocity Our velocity
    /// @param move
    bool Collides(const Matrix4& transformation, const BoundingBox& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Transformed(transformation).Collides(other, velocity, move);
    }
    bool Collides(const Matrix4& transformation, const Sphere& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Transformed(transformation).Collides(other, velocity, move);
    }
    bool Collides(const Matrix4& transformation, const HeightMap& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Transformed(transformation).Collides(other, velocity, move);
    }
    bool Collides(const Matrix4& transformation, const ConvexHull& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Transformed(transformation).Collides(other, velocity, move);
    }
    bool Collides(const Matrix4& transformation, const TriangleMesh& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Transformed(transformation).Collides(other, velocity, move);
    }

    // Object must be transformed to world coordinates
    bool Collides(const BoundingBox& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Collides(other, velocity, move);
    }
    bool Collides(const Sphere& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Collides(other, velocity, move);
    }
    bool Collides(const HeightMap& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Collides(other, velocity, move);
    }
    bool Collides(const ConvexHull& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Collides(other, velocity, move);
    }
    bool Collides(const TriangleMesh& other, const Vector3& velocity, Vector3& move) const override
    {
        ASSERT(object_);
        return object_->Collides(other, velocity, move);
    }

    const T& Object() const
    {
        ASSERT(object_);
        return *object_;
    }
    T& Object()
    {
        ASSERT(object_);
        return *object_;
    }

    [[nodiscard]] Mesh GetMesh() const override
    {
        ASSERT(object_);
        return object_->GetMesh();
    }
};

}
