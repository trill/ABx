/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Vector4.h"
#include "Vector3.h"
#include "Quaternion.h"
#include "MathConfig.h"
#include <array>
#include <sa/Assert.h>

namespace Math {

class Matrix4
{
public:
    enum Index : size_t
    {
        Index00,      // Row 0
        Index10,
        Index20,
        Index30,
        Index01,      // Row 1
        Index11,
        Index21,
        Index31,
        Index02,      // Row 2
        Index12,
        Index22,
        Index32,
        Index03,      // Row 3
        Index13,
        Index23,
        Index33,
    };

    constexpr Matrix4() noexcept :
        // Identity
        m_{
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        }
    { }
    constexpr Matrix4(
        float v00, float v01, float v02, float v03,                             // Row 0
        float v10, float v11, float v12, float v13,                             // Row 1
        float v20, float v21, float v22, float v23,                             // Row 2
        float v30, float v31, float v32, float v33                              // Row 3
    ) noexcept :
        m_{
            v00, v10, v20, v30,
            v01, v11, v21, v31,
            v02, v12, v22, v32,
            v03, v13, v23, v33
        }
    { }
    constexpr Matrix4(const Vector4& row0, const Vector4& row1, const Vector4& row2, const Vector4& row3) noexcept :
        Matrix4(
            row0.x_, row0.y_, row0.z_, row0.w_,
            row1.x_, row1.y_, row1.z_, row1.w_,
            row2.x_, row2.y_, row2.z_, row2.w_,
            row3.x_, row3.y_, row3.z_, row3.w_)
    { }
#if defined(HAVE_DIRECTX_MATH)
    Matrix4(const XMath::XMMATRIX& matrix) noexcept; //NOLINT(hicpp-explicit-conversions)
#endif
    Matrix4(const Vector3& translation, const Quaternion& rotation, const Vector3& scale) noexcept
    {
        SetRotation(rotation.Conjugate().GetMatrix().Scale(scale));
        SetTranslation(translation);
    }
#if defined(HAVE_DIRECTX_MATH)
    operator XMath::XMMATRIX() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::XMMatrixSet(
            m_[0], m_[4], m_[8], m_[12],
            m_[1], m_[5], m_[9], m_[13],
            m_[2], m_[6], m_[10], m_[14],
            m_[3], m_[7], m_[11], m_[15]
        );
    }
#endif

    Vector3 operator *(const Vector3& rhs) const;
    Vector4 operator *(const Vector4& rhs) const;
    Matrix4 operator *(const Matrix4& rhs) const;
    Matrix4 operator *(float rhs) const
    {
        return {
            m_[Index00] * rhs,
            m_[Index01] * rhs,
            m_[Index02] * rhs,
            m_[Index03] * rhs,
            m_[Index10] * rhs,
            m_[Index11] * rhs,
            m_[Index12] * rhs,
            m_[Index13] * rhs,
            m_[Index20] * rhs,
            m_[Index21] * rhs,
            m_[Index22] * rhs,
            m_[Index23] * rhs,
            m_[Index30] * rhs,
            m_[Index31] * rhs,
            m_[Index32] * rhs,
            m_[Index33] * rhs
        };
    }

    Matrix4 operator +(const Matrix4& rhs) const
    {
        return {
            m_[Index00] + rhs.m_[Index00],
            m_[Index01] + rhs.m_[Index01],
            m_[Index02] + rhs.m_[Index02],
            m_[Index03] + rhs.m_[Index03],
            m_[Index10] + rhs.m_[Index10],
            m_[Index11] + rhs.m_[Index11],
            m_[Index12] + rhs.m_[Index12],
            m_[Index13] + rhs.m_[Index13],
            m_[Index20] + rhs.m_[Index20],
            m_[Index21] + rhs.m_[Index21],
            m_[Index22] + rhs.m_[Index22],
            m_[Index23] + rhs.m_[Index23],
            m_[Index30] + rhs.m_[Index30],
            m_[Index31] + rhs.m_[Index31],
            m_[Index32] + rhs.m_[Index32],
            m_[Index33] + rhs.m_[Index33]
        };
    }
    Matrix4 operator -(const Matrix4& rhs) const
    {
        return {
            m_[Index00] - rhs.m_[Index00],
            m_[Index01] - rhs.m_[Index01],
            m_[Index02] - rhs.m_[Index02],
            m_[Index03] - rhs.m_[Index03],
            m_[Index10] - rhs.m_[Index10],
            m_[Index11] - rhs.m_[Index11],
            m_[Index12] - rhs.m_[Index12],
            m_[Index13] - rhs.m_[Index13],
            m_[Index20] - rhs.m_[Index20],
            m_[Index21] - rhs.m_[Index21],
            m_[Index22] - rhs.m_[Index22],
            m_[Index23] - rhs.m_[Index23],
            m_[Index30] - rhs.m_[Index30],
            m_[Index31] - rhs.m_[Index31],
            m_[Index32] - rhs.m_[Index32],
            m_[Index33] - rhs.m_[Index33]
        };
    }

    void SetTranslation(const Vector3& v);
    void SetScale(const Vector3& v);
    void SetRotation(const Quaternion& v);
    void SetRotation(const Matrix4& v);

    // Translate this Matrix by v and return it
    Matrix4& Translate(const Vector3& v);
    // Scale this Matrix by v and return it
    Matrix4& Scale(const Vector3& v);

    Matrix4& RotateX(float ang);
    Matrix4& RotateY(float ang);
    Matrix4& RotateZ(float ang);
    Matrix4& Rotate(const Vector3& axis, float ang);
    Matrix4& Rotate(const Vector4& axisAngle);

    /// Get rotation part
    [[nodiscard]] Quaternion Rotation() const;
    /// Get translation part
    [[nodiscard]] Vector3 Translation() const;
    /// Get scaling part
    [[nodiscard]] Vector3 Scaling() const;
    void Decompose(Vector3* translation, Quaternion* rotation, Vector3* scale) const;
    [[nodiscard]] bool IsIdentity() const;

    [[nodiscard]] Matrix4 Transpose() const;
    [[nodiscard]] float Determinant() const;
    [[nodiscard]] Matrix4 Inverse() const;
    [[nodiscard]] const float* Data() const { return &m_[0]; }

    static Matrix4 FromFrustum(float left, float right, float bottom, float top, float nearZ, float farZ);
    static Matrix4 FromPerspective(float fovY, float aspect, float nearZ, float farZ, float zoom = 1.0f);
    static Matrix4 FromOrtho(float left, float right, float bottom, float top, float nearZ, float farZ);
    static Matrix4 FromOrtho(float width, float height, float nearZ, float farZ);
    static Matrix4 FromLookAt(const Vector3& eye, const Vector3& center, const Vector3& up);
    static Matrix4 FromQuaternion(const Quaternion& q);
    static Matrix4 FromAxisAngle(const Vector3& axis, float angle);
    static Matrix4 FromScale(const Vector3& scale);
    static Matrix4 FromTranslation(const Vector3& v);

    static Vector3 UnProject(const Vector3& vec, const Matrix4& view, const Matrix4& proj, const std::array<float, 4>& viewport);
    static Vector3 Project(const Vector3& vec, const Matrix4& view, const Matrix4& proj, const std::array<float, 4>& viewport);

    /// Column-major
    float m_[16];
};

static constexpr Matrix4 Matrix4_Zero = {
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
};
static constexpr Matrix4 Matrix4_Identity;
// Flip X axis
static constexpr Matrix4 Matrix4_FlipX = {
    -1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f
};
// Flip Y axis
static constexpr Matrix4 Matrix4_FlipY = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, -1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f
};
// Flip Z axis
static constexpr Matrix4 Matrix4_FlipZ = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, -1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f
};

}
