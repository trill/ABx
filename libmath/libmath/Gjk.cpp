/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Gjk.h"

namespace Math {

bool Gjk::CheckOneFaceAC(const Vector3& abc, const Vector3& ac, const Vector3& ao)
{
    if (abc.CrossProduct(ac).DotProduct(ao) > 0.0f)
    {
        //origin is in the region of edge ac
        b_ = -ao;
        v_ = TripleProduct(ac, ao);
        n_ = 2;
        return false;
    }
    return true;
}

bool Gjk::CheckOneFaceAB(const Vector3& abc, const Vector3& ab, const Vector3& ao)
{
    if (ab.CrossProduct(abc).DotProduct(ao) > 0.0f)
    {
        c_ = b_;
        b_ = -ao;
        v_ = TripleProduct(ab, ao);
        n_ = 2;
        return false;
    }
    return true;
}

bool Gjk::CheckTwoFaces(Vector3& abc, Vector3& acd, Vector3& ac, Vector3& ab, Vector3& ad, const Vector3& ao)
{
    if (abc.CrossProduct(ac).DotProduct(ao) > 0.0f)
    {
        b_ = c_;
        c_ = d_;
        ab = ac;
        ac = ad;

        abc = acd;
        return false;
    }
    return true;
}

bool Gjk::Update(const Vector3& a)
{
    if (n_ == 2)
    {
        // Handling triangle
        const Vector3 ao = -a;
        const Vector3 ab = b_ - a;
        const Vector3 ac = c_ - a;

        // Normal of triangle ABC
        const Vector3 abc = ab.CrossProduct(ac);

        // Plane test on edge ab
        const Vector3 abp = ab.CrossProduct(abc);       // direction vector pointing inside triangle abc from ab
        if (abp.DotProduct(ao) > 0.0f)
        {
            // origin lies outside the triangle abc, near the edge ab
            c_ = b_;
            b_ = a;
            v_ = TripleProduct(ab, ao);
            return false;
        }

        // plane test on edge ac

        // direction vector pointing inside triangle abc from ac
        // note that different than abp, the result of acp is abc cross ac, while abp is ab cross abc.
        // The order does matter. Based on the right-handed rule, we want the vector pointing inside the triangle.
        const Vector3 acp = abc.CrossProduct(ac);
        if (acp.DotProduct(ao) > 0.0f)
        {
            // origin lies outside the triangle abc, near the edge ac
            b_ = a;
            v_ = TripleProduct(ac, ao);
            return false;
        }

        // Now the origin is within the triangle abc, either above or below it.
        if (abc.DotProduct(ao) > 0.0f)
        {
            // origin is above the triangle
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = -abc;
        }
        else
        {
            // origin is below the triangle
            d_ = b_;
            b_ = a;
            v_ = -abc;
        }

        n_ = 3;
        return false;
    }

    if (n_ == 3)
    {
        const Vector3 ao = -a;
        Vector3 ab = b_ - a;
        Vector3 ac = c_ - a;
        Vector3 ad = d_ - a;

        Vector3 abc = ab.CrossProduct(ac);
        Vector3 acd = ac.CrossProduct(ad);
        const Vector3 adb = ad.CrossProduct(ab);

        Vector3 tmp;

        static constexpr unsigned OVER_ABC = 0x1;
        static constexpr unsigned OVER_ACD = 0x2;
        static constexpr unsigned OVER_ADB = 0x4;

        unsigned planeTest =
            (abc.DotProduct(ao) > 0.0f ? OVER_ABC : 0) |
            (acd.DotProduct(ao) > 0.0f ? OVER_ACD : 0) |
            (adb.DotProduct(ao) > 0.0f ? OVER_ADB : 0);

        switch (planeTest)
        {
        case 0:
            // inside the tetrahedron
            return true;
        case OVER_ABC:
            if (!CheckOneFaceAC(abc, ac, ao))
                //in the region of AC
                return false;
            if (!CheckOneFaceAB(abc, ab, ao))
                //in the region of AB
                return false;
            //otherwise, in the region of ABC
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        case OVER_ACD:
            //rotate acd to abc, and perform the same procedure
            b_ = c_;
            c_ = d_;

            ab = ac;
            ac = acd;

            abc = acd;

            if (!CheckOneFaceAC(abc, ac, ao))
                //in the region of AC (actually is ad)
                return false;
            if (!CheckOneFaceAB(abc, ab, ao))
                //in the region of AB (actually is ac)
                return false;

            //otherwise, in the region of "ABC" (which is actually acd)
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        case OVER_ADB:
            //rotate adb to abc, and perform the same procedure
            c_ = b_;
            b_ = d_;

            ac = ab;
            ab = ad;

            abc = adb;
            if (!CheckOneFaceAC(abc, ac, ao))
                return false;
            if (!CheckOneFaceAB(abc, ab, ao))
                return false;
            //otherwise, in the region of "ABC" (which is actually acd)
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        case OVER_ABC | OVER_ACD:
            if (!CheckTwoFaces(abc, acd, ac, ab, ad, ao))
            {
                if (!CheckOneFaceAC(abc, ac, ao))
                    return false;
                if (!CheckOneFaceAB(abc, ab, ao))
                    return false;
                //otherwise, in the region of "ABC" (which is actually acd)
                d_ = c_;
                c_ = b_;
                b_ = a;
                v_ = abc;
                n_ = 3;
                return false;
            }
            if (!CheckOneFaceAB(abc, ab, ao))
                return false;
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        case OVER_ACD | OVER_ADB:
            //rotate ACD, ADB into ABC, ACD
            tmp = b_;
            b_ = c_;
            c_ = d_;
            d_ = tmp;

            tmp = a;
            ab = ac;
            ac = ad;
            ad = tmp;

            abc = acd;
            acd = adb;

            if (!CheckTwoFaces(abc, acd, ac, ab, ad, ao))
            {
                if (!CheckOneFaceAC(abc, ac, ao))
                    return false;
                if (!CheckOneFaceAB(abc, ab, ao))
                    return false;
                //otherwise, in the region of "ABC" (which is actually acd)
                d_ = c_;
                c_ = b_;
                b_ = a;
                v_ = abc;
                n_ = 3;
                return false;
            }
            if (!CheckOneFaceAB(abc, ab, ao))
                return false;
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        case OVER_ADB | OVER_ABC:
            //rotate ADB, ABC into ABC, ACD
            tmp = c_;
            c_ = b_;
            b_ = d_;
            d_ = tmp;

            tmp = ac;
            ac = ab;
            ab = ad;
            ad = tmp;

            acd = abc;
            abc = adb;

            if (!CheckTwoFaces(abc, acd, ac, ab, ad, ao))
            {
                if (!CheckOneFaceAC(abc, ac, ao))
                    return false;
                if (!CheckOneFaceAB(abc, ab, ao))
                    return false;
                //otherwise, in the region of "ABC" (which is actually acd)
                d_ = c_;
                c_ = b_;
                b_ = a;
                v_ = abc;
                n_ = 3;
                return false;
            }
            if (!CheckOneFaceAB(abc, ab, ao))
                return false;
            d_ = c_;
            c_ = b_;
            b_ = a;
            v_ = abc;
            n_ = 3;
            return false;
        default:
            return true;
        }
    }

    return true;
}

bool Gjk::Intersects(const Mesh& mesh1, const Mesh& mesh2)
{
    v_ = Vector3_UnitX;
    n_ = 0;

    c_ = Support(mesh1, mesh2, v_);

    if (c_.DotProduct(v_) < 0.0f)
        return false;
    v_ = -c_;

    b_ = Support(mesh1, mesh2, v_);
    if (b_.DotProduct(v_) < 0.0f)
        return false;

    v_ = TripleProduct(c_ - b_, -b_);
    n_ = 2;

    for (unsigned i = 0; i < MAX_ITERATIONS; ++i)
    {
        const Vector3 a = Support(mesh1, mesh2, v_);
        if (a.DotProduct(v_) < 0.0f)
            // No intersection
            return false;

        if (Update(a))
            return true;
    }
    return true;
}

}
