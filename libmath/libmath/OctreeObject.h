/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BoundingBox.h"
#include <eastl.hpp>
#include <sa/Compiler.h>

namespace Math {

class Octant;
class RayOctreeQuery;
struct RayQueryResult;
class Octant;

class OctreeObject
{
private:
    float sortValue_{ 0.0f };
    Octant* octant_{ nullptr };
    /// Occluder flag. An object that can hide another object from view.
    bool occluder_{ false };
    /// Occludee flag. An object that can be hidden from view (because it is occluded by another object) but that cannot, itself, hide another object from view.
    bool occludee_{ true };
    uint32_t collisionLayer_{ 1 };
    uint32_t collisionMask_{ 0xFFFFFFFF };     // Collides with all by default
public:
    virtual ~OctreeObject();
    void SetSortValue(float value) { sortValue_ = value; }
    [[nodiscard]] float GetSortValue() const { return sortValue_; }
    [[nodiscard]] Octant* GetOctant() const;
    void SetOctant(Octant* octant);
    [[nodiscard]] bool IsOccludee() const { return occludee_; }
    void SetOccludee(bool value) { occludee_ = value; }
    [[nodiscard]] bool IsOccluder() const { return occluder_; }
    void SetOccluder(bool value) { occluder_ = value; }
    void SetCollisionLayer(uint32_t value) { collisionLayer_ = value; }
    [[nodiscard]] uint32_t GetCollisionLayer() const { return collisionLayer_; }
    void SetCollisionMask(uint32_t mask) { collisionMask_ = mask; }
    [[nodiscard]] uint32_t GetCollisionMask() const { return collisionMask_; }
    [[nodiscard]] bool CollisionMaskMatches(uint32_t layer) const { return (layer & collisionMask_); }

    [[nodiscard]] virtual BoundingBox GetWorldBoundingBox() const = 0;
    virtual void ProcessRayQuery(const RayOctreeQuery& query, ea::vector<RayQueryResult>& results) = 0;
};

inline bool CompareObjects(const OctreeObject* lhs, const OctreeObject* rhs)
{
    return lhs->GetSortValue() < rhs->GetSortValue();
}

}
