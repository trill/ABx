/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HeightMapTools.h"
#include "VectorMath.h"
#include <fstream>

namespace Math {

ea::vector<float> CreateHeightMapFromMesh(const Math::Mesh& shape,
    int targetWdth, int targetHeight,
    int& width, int& height,
    float& minHeight, float& maxHeight)
{
    const Math::Vector3 minX = shape.GetFarsetPointInDirection(-Vector3_UnitX);
    const Math::Vector3 maxX = shape.GetFarsetPointInDirection(Vector3_UnitX);
    const Math::Vector3 minZ = shape.GetFarsetPointInDirection(-Vector3_UnitZ);
    const Math::Vector3 maxZ = shape.GetFarsetPointInDirection(Vector3_UnitZ);

    const Math::Vector3 minHeightVertex = shape.GetFarsetPointInDirection(-Vector3_UnitY);
    const Math::Vector3 maxHeightVertex = shape.GetFarsetPointInDirection(Vector3_UnitY);
    minHeight = minHeightVertex.y_;
    maxHeight = maxHeightVertex.y_;

    if (targetWdth != 0)
        width = targetWdth;
    else
        width = (int)std::ceil(maxX.x_ - minX.x_);
    if (targetHeight != 0)
        height = targetHeight;
    else
        height = (int)std::ceil(maxZ.z_ - minZ.z_);

    ea::vector<float> heights;
    heights.resize((size_t)width * (size_t)height);
    ea::fill(heights.begin(), heights.end(), -M_INFINITE);

    for (const auto& v : shape.vertexData_)
    {
        const int x = (targetWdth == 0) ? static_cast<int>(v.x_ - minX.x_) : (static_cast<int>(v.x_) + targetWdth / 2);
        const int y = (targetHeight == 0) ? (height - static_cast<int>(v.z_ - minZ.z_)) : (static_cast<int>(v.z_) + targetHeight / 2);

        if (x < 0 || x >= width)
            continue;
        if (y < 0 || y >= height)
            continue;

        const size_t index = (size_t)y * (size_t)width + (size_t)x;
        if (heights[index] < v.y_)
            heights[index] = v.y_;
    }

    // 2nd pass to fill triangles
    for (size_t t = 0; t < shape.GetTriangleCount(); ++t)
    {
        const auto triangle = shape.GetTriangle(t);

        const int minTriangleX = (int)floor(std::min(std::min(triangle[0].x_, triangle[1].x_), triangle[2].x_));
        const int minTriangleZ = (int)floor(std::min(std::min(triangle[0].z_, triangle[1].z_), triangle[2].z_));
        const int maxTriangleX = (int)ceil(std::max(std::max(triangle[0].x_, triangle[1].x_), triangle[2].x_));
        const int maxTriangleZ = (int)ceil(std::max(std::max(triangle[0].z_, triangle[1].z_), triangle[2].z_));

        for (int y = minTriangleZ; y <= maxTriangleZ; ++y)
        {
            for (int x = minTriangleX; x < maxTriangleX; ++x)
            {
                // Get interpolated Y
                const Math::Vector3 pointY = Math::GetClosestPointOnTriangle(triangle[0], triangle[1], triangle[2], { (float)x, triangle[0].y_, (float)y });
                const Math::Vector3 point = { (float)x, pointY.y_, (float)y };
                if (Math::IsPointInTriangle(point, triangle[0], triangle[1], triangle[2]))
                {
                    const int posX = (targetWdth == 0) ? static_cast<int>(x - minX.x_) : (static_cast<int>(x) + targetWdth / 2);
                    const int posY = (targetHeight == 0) ? (height - static_cast<int>(y - minZ.z_)) : (static_cast<int>(y) + targetHeight / 2);

                    if (posX < 0 || posX >= width)
                        continue;
                    if (posY < 0 || posY >= height)
                        continue;

                    const size_t index = (size_t)posY * (size_t)width + (size_t)posX;

                    if (heights[index] < point.y_)
                        heights[index] = point.y_;
                }
            }
        }
    }

    return heights;
}

ea::vector<float> CreateHeightMapFromImage(const unsigned char* data, int width, int height, int components,
    const Vector3& spacing, int patchSize,
    HeightmapHeader& header)
{
    header.patchSize = patchSize;
    header.patchWorldSize = { spacing.x_ * (float)patchSize, spacing.z_ * (float)patchSize };
    header.numPatches = { (width - 1) / patchSize, (height - 1) / patchSize };
    header.numVertices = { header.numPatches.x_ * patchSize + 1, header.numPatches.y_ * patchSize + 1 };
    header.patchWorldOrigin = { -0.5f * (float)header.numPatches.x_ * header.patchWorldSize.x_,
        -0.5f * (float)header.numPatches.y_ * header.patchWorldSize.y_ };

    int imgRow = width * components;
    auto getHeight = [&](int x, int z) -> float
    {
        if (!data)
            return 0.0f;

        // From bottom to top
        int offset = imgRow * (header.numVertices.y_ - 1 - z) + components * x;

        if (components == 1)
            return (float)data[offset];

        // If more than 1 component, use the green channel for more accuracy
        return (float)data[offset] +
            (float)data[offset + 1] / 256.0f;
    };

    header.minHeight = std::numeric_limits<float>::max();
    header.maxHeight = std::numeric_limits<float>::lowest();

    ea::vector<float> heightData;
    heightData.resize((size_t)header.numVertices.x_ * (size_t)header.numVertices.y_);
    for (int y = 0; y < header.numVertices.y_; ++y)
    {
        for (int x = 0; x < header.numVertices.x_; ++x)
        {
            float fy = getHeight(x, y) * spacing.y_;
            heightData[(size_t)y * (size_t)header.numVertices.x_ + (size_t)x] = fy;
            if (header.minHeight > fy)
                header.minHeight = fy;
            if (header.maxHeight < fy)
                header.maxHeight = fy;
        }
    }
    return heightData;
}

void CreateShapeFromHeightmapImage(const unsigned char* data, int width, int height, int components,
    const Vector3& spacing, int patchSize,
    const std::function<void(const Vector3& vertex)>& onVertex,
    const std::function<void(int i1, int i2, int i3)>& onTriangle,
    HeightmapHeader& header)
{
    header.patchSize = patchSize;
    // Image size - 1 sould be a multiple of patchSize, otherwise it cuts off some pixels.
    header.patchWorldSize = { spacing.x_ * (float)patchSize, spacing.z_ * (float)patchSize };
    header.numPatches = { (width - 1) / patchSize, (height - 1) / patchSize };
    header.numVertices = { header.numPatches.x_ * patchSize + 1, header.numPatches.y_ * patchSize + 1 };
    header.patchWorldOrigin = { -0.5f * (float)header.numPatches.x_ * header.patchWorldSize.x_,
        -0.5f * (float)header.numPatches.y_ * header.patchWorldSize.y_ };

    int imgRow = width * components;
    auto getHeight = [&](int x, int z) -> float
    {
        if (!data)
            return 0.0f;

        // From bottom to top
        int offset = imgRow * (header.numVertices.y_ - 1 - z) + components * x;

        if (components == 1)
            return (float)data[offset];

        // If more than 1 component, use the green channel for more accuracy
        return (float)data[offset] +
            (float)data[offset + 1] / 256.0f;
    };

    header.minHeight = std::numeric_limits<float>::max();
    header.maxHeight = std::numeric_limits<float>::lowest();
    const float offsetX = ((float)header.numVertices.x_ * 0.5f);
    const float offsetY = ((float)header.numVertices.y_ * 0.5f);
    for (int y = 0; y < header.numVertices.y_; ++y)
    {
        for (int x = 0; x < header.numVertices.x_; ++x)
        {
            float fy = getHeight(x, y) * spacing.y_;
            float fx = ((float)x - offsetX) * spacing.x_;
            float fz = ((float)y - offsetY) * spacing.z_;
            if (header.minHeight > fy)
                header.minHeight = fy;
            if (header.maxHeight < fy)
                header.maxHeight = fy;

            if (onVertex)
                onVertex({ fx, fy, fz });
        }
    }

    // Create index data
    for (int y = 0; y < header.numVertices.y_ - 1; ++y)
    {
        for (int x = 0; x < header.numVertices.x_ - 1; ++x)
        {
            /*
                x+1,y
        x,y +----+----+
            | 1 /|(3)/|
            |  / |  / |
            | /  | /  |
            |/ 2 |/(4)|
      x,y+1 +----+----+
              x+1,y+1
            */
            {
                // First triangle
                int i1 = (y + 1) * header.numVertices.x_ + x;
                int i2 = y * header.numVertices.x_ + x;
                int i3 = (y * header.numVertices.x_) + x + 1;
                if (onTriangle)
                    onTriangle(i3, i2, i1);
            }

            {
                // Second triangle
                int i1 = y * header.numVertices.x_ + x + 1;
                int i2 = (y + 1) * header.numVertices.x_ + (x + 1);
                int i3 = (y + 1) * header.numVertices.x_ + x;
                if (onTriangle)
                    onTriangle(i3, i2, i1);
            }
        }
    }
}

}
