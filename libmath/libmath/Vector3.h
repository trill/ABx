/**
 * Copyright 2017-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MathDefs.h"
#include "MathUtils.h"
#include "MathConfig.h"
#include <sstream>
#include <cmath>
#include <array>

namespace Math {

class Vector4;

class Vector3
{
public:
    constexpr Vector3() noexcept = default;

    constexpr Vector3(const Vector3& vector) noexcept  = default;
    constexpr Vector3(Vector3&& vector) noexcept  = default;
    /// Create from Vector4, drop w_
    explicit Vector3(const Vector4& vector) noexcept;

    constexpr Vector3(float x, float y, float z) noexcept :
        x_(x),
        y_(y),
        z_(z)
    { }
    constexpr Vector3(const std::array<float, 3>& values) noexcept : //NOLINT(hicpp-explicit-conversions)
        x_(values[0]),
        y_(values[1]),
        z_(values[2])
    { }

#if defined(HAVE_DIRECTX_MATH)
    Vector3(const XMath::XMVECTOR& vector) noexcept : //NOLINT(hicpp-explicit-conversions)
        x_(XMath::XMVectorGetX(vector)),
        y_(XMath::XMVectorGetY(vector)),
        z_(XMath::XMVectorGetZ(vector))
    { }
    constexpr Vector3(const XMath::XMFLOAT3& vector) noexcept : //NOLINT(hicpp-explicit-conversions)
        x_(vector.x),
        y_(vector.y),
        z_(vector.z)
    { }
#endif
    ~Vector3() = default;

    /// Parse from string
    explicit Vector3(const std::string& str);

    constexpr Vector3& operator =(const Vector3& vector) = default;
    constexpr Vector3& operator =(Vector3&& vector) noexcept = default;

    constexpr operator std::array<float, 3>() const //NOLINT(hicpp-explicit-conversions)
    {
        return { x_, y_, z_ };
    }
#if defined(HAVE_DIRECTX_MATH)
    /// Cast to XMVECTOR
    operator XMath::XMVECTOR() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::XMVectorSet(x_, y_, z_, 0.0f);
    }
    constexpr operator XMath::XMFLOAT3() const //NOLINT(hicpp-explicit-conversions)
    {
        return{ x_, y_, z_ };
    }
#endif

    constexpr bool operator ==(const Vector3& vector) const { return Equals(vector); }
    constexpr bool operator !=(const Vector3& vector) const { return !Equals(vector); }

    /// Return negation.
    constexpr Vector3 operator -() const { return { -x_, -y_, -z_ }; }

    constexpr Vector3& operator+=(const Vector3& v)
    {
        x_ += v.x_;
        y_ += v.y_;
        z_ += v.z_;
        return *this;
    }
    constexpr Vector3& operator-=(const Vector3& v)
    {
        x_ -= v.x_;
        y_ -= v.y_;
        z_ -= v.z_;
        return *this;
    }
    constexpr Vector3& operator*=(const Vector3& v)
    {
        x_ *= v.x_;
        y_ *= v.y_;
        z_ *= v.z_;
        return *this;
    }
    constexpr Vector3& operator/=(const Vector3& v)
    {
        x_ /= v.x_;
        y_ /= v.y_;
        z_ /= v.z_;
        return *this;
    }
    constexpr Vector3& operator*=(float v)
    {
        x_ *= v;
        y_ *= v;
        z_ *= v;
        return *this;
    }
    constexpr Vector3& operator/=(float v)
    {
        x_ /= v;
        y_ /= v;
        z_ /= v;
        return *this;
    }

    constexpr Vector3 operator+(const Vector3& v) const
    {
        return { x_ + v.x_, y_ + v.y_, z_ + v.z_ };
    }
    constexpr Vector3 operator-(const Vector3& v) const
    {
        return { x_ - v.x_, y_ - v.y_, z_ - v.z_ };
    }
    constexpr Vector3 operator+(float v) const
    {
        return { x_ + v, y_ + v, z_ + v };
    }
    constexpr Vector3 operator-(float v) const
    {
        return { x_ - v, y_ - v, z_ - v };
    }
    constexpr Vector3 operator*(const Vector3& v) const
    {
        return { x_ * v.x_, y_ * v.y_, z_ * v.z_ };
    }
    constexpr Vector3 operator/(const Vector3& v) const
    {
        return { x_ / v.x_, y_ / v.y_, z_ / v.z_ };
    }

    friend constexpr Vector3 operator*(const Vector3& v, float n)
    {
        return { v.x_ * n, v.y_ * n, v.z_ * n };
    }
    friend constexpr Vector3 operator*(float n, const Vector3& v)
    {
        return { v.x_ * n, v.y_ * n, v.z_ * n };
    }

    friend constexpr Vector3 operator/(const Vector3& v, float n)
    {
        return { v.x_ / n, v.y_ / n, v.z_ / n };
    }
    friend constexpr Vector3 operator/(float n, const Vector3& v)
    {
        return { v.x_ / n, v.y_ / n, v.z_ / n };
    }

    [[nodiscard]] Vector3 CrossProduct(const Vector3& v) const
    {
#if defined(HAVE_DIRECTX_MATH)
        return XMath::XMVector3Cross(*this, v);
#else
        return Vector3(
            y_ * v.z_ - z_ * v.y_,
            z_ * v.x_ - x_ * v.z_,
            x_ * v.y_ - y_ * v.x_
        );
#endif
    }

    [[nodiscard]] constexpr float DotProduct(const Vector3& v) const
    {
        return x_ * v.x_ + y_ * v.y_ + z_ * v.z_;
    }

    [[nodiscard]] float Angle(const Vector3& v) const
    {
        return acosf(DotProduct(v) / (Length() * v.Length()));
    }
    /// Returns the Y angle between this point and v in Rad
    [[nodiscard]] float AngleY(const Vector3& v) const
    {
        return atan2f(x_ - v.x_, z_ - v.z_);
    }
    [[nodiscard]] Vector3 Orthogonal() const;

    [[nodiscard]] float LengthSqr() const;
    [[nodiscard]] float Length() const;
    [[nodiscard]] float Distance(const Vector3& v) const;
    // Distance without Y component, i.e. assuming the same height
    [[nodiscard]] float DistanceXZ(const Vector3& v) const;
    [[nodiscard]] Vector3 Normal() const;
    void Normalize();
    /// Absolute
    [[nodiscard]] Vector3 Abs() const
    {
        return { fabsf(x_), fabsf(y_), fabsf(z_) };
    }
    /// Linear interpolation
    [[nodiscard]] Vector3 Lerp(const Vector3& to, float i) const
    {
        return *this * (1.0f - i) + to * i;
    }
    /// Test for equality with another vector with epsilon.
    [[nodiscard]] constexpr bool Equals(const Vector3& rhs) const;
    [[nodiscard]] constexpr bool Equals(const Vector3& rhs, float epsilon) const;
    void Clamp(const Vector3& min, const Vector3& max);
    void Clamp(float min, float max);

    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << x_ << " " << y_ << " " << z_;
        return ss.str();
    }
    void SetLength(float length);

    [[nodiscard]] const float* Data() const { return &x_; }
    friend std::ostream& operator << (std::ostream& os, const Vector3& value)
    {
        return os << value.ToString();
    }

    float x_{ 0.0f };
    float y_{ 0.0f };
    float z_{ 0.0f };
};

static constexpr Vector3 Vector3_Zero;
static constexpr Vector3 Vector3_Infinite(M_INFINITE, M_INFINITE, M_INFINITE);
static constexpr Vector3 Vector3_One(1.0f, 1.0f, 1.0f);
static constexpr Vector3 Vector3_UnitX(1.0f, 0.0f, 0.0f);           // Right
static constexpr Vector3 Vector3_UnitY(0.0f, 1.0f, 0.0f);           // Up
static constexpr Vector3 Vector3_UnitZ(0.0f, 0.0f, 1.0f);           // Forward

static constexpr Vector3 Vector3_Forward(0.0f, 0.0f, 1.0f);
static constexpr Vector3 Vector3_Back(0.0f, 0.0f, -1.0f);
static constexpr Vector3 Vector3_Up(0.0f, 1.0f, 0.0f);
static constexpr Vector3 Vector3_Down(0.0f, -1.0f, 0.0f);
static constexpr Vector3 Vector3_Left(-1.0f, 0.0f, 0.0f);
static constexpr Vector3 Vector3_Right(1.0f, 0.0f, 0.0f);

constexpr bool Vector3::Equals(const Vector3& rhs) const
{
    return Math::Equals(x_, rhs.x_) && Math::Equals(y_, rhs.y_) && Math::Equals(z_, rhs.z_);
}

constexpr bool Vector3::Equals(const Vector3& rhs, float epsilon) const
{
    return Math::Equals(x_, rhs.x_, epsilon) &&
        Math::Equals(y_, rhs.y_, epsilon) &&
        Math::Equals(z_, rhs.z_, epsilon);
}

#if defined(HAVE_DIRECTX_MATH)
inline void XMStoreVector3(Vector3* pDestination, const XMath::FXMVECTOR& V)
{
#if defined(_XM_NO_INTRINSICS_)
    pDestination->x_ = V.vector4_f32[0];
    pDestination->y_ = V.vector4_f32[1];
    pDestination->z_ = V.vector4_f32[2];
#elif defined(_XM_ARM_NEON_INTRINSICS_)
    float32x2_t VL = vget_low_f32(V);
    vst1_f32(reinterpret_cast<float*>(pDestination), VL);
    vst1q_lane_f32(reinterpret_cast<float*>(pDestination) + 2, V, 2);
#elif defined(_XM_SSE_INTRINSICS_)
    XMath::XMVECTOR T1 = XM_PERMUTE_PS(V, _MM_SHUFFLE(1, 1, 1, 1));
    XMath::XMVECTOR T2 = XM_PERMUTE_PS(V, _MM_SHUFFLE(2, 2, 2, 2));
    _mm_store_ss(&pDestination->x_, V);
    _mm_store_ss(&pDestination->y_, T1);
    _mm_store_ss(&pDestination->z_, T2);
#endif
}
#endif

}
