/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Mesh.h"
#include "VectorMath.h"

namespace Math {

void Mesh::Reset()
{
    vertexData_.clear();
    vertexCount_ = 0;
    indexData_.clear();
    indexCount_ = 0;
}

Vector3 Mesh::GetTriangleNormal(size_t i) const
{
    const auto tri = GetTriangle(i);
    return Math::GetTriangleNormal(tri[0], tri[1], tri[2]);
}

void Mesh::AddTriangle(uint32_t i1, uint32_t i2, uint32_t i3)
{
    indexData_.push_back(i1);
    indexData_.push_back(i2);
    indexData_.push_back(i3);
    indexCount_ += 3;
}

void Mesh::AddQuad(uint32_t i1, uint32_t i2, uint32_t i3, uint32_t i4)
{
    indexData_.push_back(i1);
    indexData_.push_back(i2);
    indexData_.push_back(i3);
    indexData_.push_back(i4);
    indexCount_ += 4;
}

Vector3 Mesh::Center() const
{
    Vector3 result;
    for (size_t i = 0; i < GetCount(); ++i)
        result += GetVertex(i);
    result /= static_cast<float>(GetCount());
    return result;
}

Vector3 Mesh::GetClosestPointOnTriangle(size_t i, const Vector3& pos) const
{
    if (GetTriangleCount() == 0)
        return {};
    const auto tri = GetTriangle(i);
    return Math::GetClosestPointOnTriangle(tri[0], tri[1], tri[2], pos);
}

float Mesh::GetDistanceToTriangle(size_t i, const Vector3& pos) const
{
    if (GetTriangleCount() == 0)
        return M_INFINITE;
    return Math::GetDistanceToTriangle(GetTriangle(i), pos);
}

size_t Mesh::GetClosestTriangleIndex(const Vector3& pos) const
{
    float minDist = std::numeric_limits<float>::max();
    size_t best = 0;
    const size_t count = GetTriangleCount();
    for (size_t i = 0; i < count; ++i)
    {
        float dist = GetDistanceToTriangle(i, pos);
        if (dist < minDist)
        {
            minDist = dist;
            best = i;
        }
    }
    return best;
}

Vector3 Mesh::GetFarsetPointInDirection(const Vector3& direction) const
{
    size_t best = 0;
    float farest = GetVertex(0).DotProduct(direction);

    const size_t count = GetCount();
    for (size_t i = 1; i < count; ++i)
    {
        float d = GetVertex(i).DotProduct(direction);
        if (farest < d)
        {
            best = i;
            farest = d;
        }
    }
    return GetVertex(best);
}

float Mesh::GetMinHeight() const
{
    float result = std::numeric_limits<float>::max();
    for (const auto& v : vertexData_)
    {
        if (v.y_ < result)
            result = v.y_;
    }
    return result;
}

float Mesh::GetMaxHeight() const
{
    float result = std::numeric_limits<float>::min();
    for (const auto& v : vertexData_)
    {
        if (v.y_ > result)
            result = v.y_;
    }
    return result;
}

}
