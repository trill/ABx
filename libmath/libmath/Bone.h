/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Matrix4.h"
#include "Node.h"
#include <string>
#include <eastl.hpp>

namespace Math {

class Skeleton;

class Bone final : public Node
{
    NON_COPYABLE(Bone)
    NON_MOVEABLE(Bone)
public:
    struct VertexWeight
    {
        uint32_t index;
        float weight;
    };

    Bone(Skeleton* skeleton, Bone* parent);
    ~Bone();

    std::string name_;
    ea::vector<VertexWeight> weights_;
private:
    Skeleton* skeleton_;
};

} // namespace Math
