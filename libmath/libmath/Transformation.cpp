/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Transformation.h"

namespace Math {

Transformation::Transformation(const Matrix4& matrix)
{
    matrix.Decompose(&position_, &orientation_, &scale_);
}

Transformation Transformation::operator*(const Matrix4& rhs) const
{
    return Transformation(rhs * GetMatrix());
}

Matrix4 Transformation::GetMatrix() const
{
    return GetMatrix(orientation_);
}

Matrix4 Transformation::GetMatrix(const Quaternion& rot) const
{
#if defined(HAVE_DIRECTX_MATH)
    static constexpr XMath::XMVECTOR vZero = { 0 };
    static constexpr XMath::XMVECTOR qId = { 0.0f, 0.0f, 0.0f, 1.0f };
    const XMath::XMVECTOR scale = XMath::XMVectorSet(scale_.x_, scale_.y_, scale_.z_, 0.0f);
    const XMath::XMVECTOR rotation = XMath::XMVectorSet(rot.x_, rot.y_, rot.z_, rot.w_);
    const XMath::XMVECTOR position = XMath::XMVectorSet(position_.x_, position_.y_, position_.z_, 0.0f);
    return XMath::XMMatrixTransformation(vZero, qId, scale, vZero, rotation, position);
#else
    return Matrix4(position_, rot, scale_);
#endif
}

void Transformation::Move(float speed, const Vector3& amount)
{
    // new position = position + direction * speed (where speed = amount * speed)

    // It's as easy as:
    // 1. Create a matrix from the rotation,
    // 2. multiply this matrix with the moving vector and
    // 3. add the resulting vector to the current position
#if defined(HAVE_DIRECTX_MATH)
    const XMath::XMMATRIX m = XMath::XMMatrixRotationAxis(Math::Vector3_UnitY, -GetYRotation());
    const Vector3 a = amount * speed;
    const XMath::XMVECTOR v = XMath::XMVector3Transform(a, m);
    position_.x_ += XMath::XMVectorGetX(v);
    position_.y_ += XMath::XMVectorGetY(v);
    position_.z_ += XMath::XMVectorGetZ(v);
#else
    const Matrix4 m = Math::Matrix4::FromAxisAngle(Math::Vector3::UnitY, -GetYRotation());
    const Vector3 a = amount * speed;
    const Vector3 v = m * a;
    position_ += v;
#endif
}

void Transformation::MoveXYZ(float speed, const Vector3& amount)
{
    // new position = position + direction * speed (where speed = amount * speed)

    // It's as easy as:
    // 1. Create a matrix from the rotation,
    // 2. multiply this matrix with the moving vector and
    // 3. add the resulting vector to the current position
#if defined(HAVE_DIRECTX_MATH)
    const XMath::XMMATRIX m = orientation_.GetMatrix().Inverse();
    const Vector3 a = amount * speed;
    const XMath::XMVECTOR v = XMath::XMVector3Transform(a, m);
    position_.x_ += XMath::XMVectorGetX(v);
    position_.y_ += XMath::XMVectorGetY(v);
    position_.z_ += XMath::XMVectorGetZ(v);
#else
    const Matrix4 m = orientation_.GetMatrix().Inverse();
    const Vector3 a = amount * speed;
    const Vector3 v = m * a;
    position_ += v;
#endif
}

void Transformation::Turn(float yAngle)
{
    SetYRotation(GetYRotation() + yAngle);
}

float Transformation::GetYRotation() const
{
    const Math::Vector4 aa = orientation_.AxisAngle();
    return Math::NormalizedAngle(aa.y_ * aa.w_);
}

void Transformation::SetYRotation(float rad)
{
    orientation_ = Quaternion::FromAxisAngle(Math::Vector3_UnitY, NormalizedAngle(rad));
}

void Transformation::LookAt(const Vector3& lookAt, const Vector3& up /* = Math::Vector3_UnitY */)
{
    const Matrix4 mat = Matrix4::FromLookAt(lookAt, position_, up);
    orientation_ = mat.Rotation();
}

void Transformation::Transform(const Matrix4& matrix)
{
    *this = Transformation(matrix * GetMatrix());
}

}
