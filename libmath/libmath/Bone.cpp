/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Bone.h"
#include "Skeleton.h"

namespace Math {

Bone::Bone(Skeleton* skeleton, Bone* parent) :
    Node(parent),
    skeleton_(skeleton)
{ }

Bone::~Bone() = default;

} // namespace Math
