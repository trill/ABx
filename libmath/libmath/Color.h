/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MathConfig.h"
#include "MathUtils.h"
#include <string>
#include <sa/color.h>
#include "Vector3.h"
#include "Vector4.h"

namespace Math {

// RGBA color. Values between 0.0..1.0
class Color
{
public:
    static constexpr Color From32(uint32_t value)
    {
        const float a = (float)((value >> 24u) & 0xffu) / 255.0f;
        const float b = (float)((value >> 16u) & 0xffu) / 255.0f;
        const float g = (float)((value >> 8u) & 0xffu) / 255.0f;
        const float r = (float)((value >> 0u) & 0xffu) / 255.0f;
        return { r, g, b, a };
    }
    static constexpr Color From24(uint32_t value)
    {
        const float b = (float)((value >> 16u) & 0xffu) / 255.0f;
        const float g = (float)((value >> 8u) & 0xffu) / 255.0f;
        const float r = (float)((value >> 0u) & 0xffu) / 255.0f;
        return { r, g, b };
    }

    constexpr Color() noexcept = default;
    constexpr Color(float r, float g, float b) : r_(r), g_(g), b_(b) {}
    constexpr Color(float r, float g, float b, float a) : r_(r), g_(g), b_(b), a_(a) {}
    constexpr Color(const Color&) noexcept = default;
    constexpr Color(Color&&) noexcept = default;
    explicit Color(const std::string& str);
    // From sa::Color to use the filter functions
    explicit Color(const sa::Color& color);
    explicit constexpr Color(const Vector3& color) :
        r_(color.x_),
        g_(color.y_),
        b_(color.z_)
    { }
    explicit constexpr Color(const Vector4& color) :
        r_(color.x_),
        g_(color.y_),
        b_(color.z_),
        a_(color.w_)
    { }
#if defined(HAVE_DIRECTX_MATH)
    Color(const XMath::XMVECTOR& vector) noexcept : //NOLINT(hicpp-explicit-conversions)
        r_(XMath::XMVectorGetX(vector)),
        g_(XMath::XMVectorGetY(vector)),
        b_(XMath::XMVectorGetZ(vector)),
        a_(XMath::XMVectorGetW(vector))
    { }
#endif
    ~Color() = default;

    constexpr Color& operator=(const Color&) = default;
    constexpr Color& operator=(Color&&) = default;

    // To sa::Color to use the filter functions
    explicit constexpr operator sa::Color() const
    {
        return sa::Color::FromRgb(r_, g_, b_, a_);
    }
    explicit constexpr operator Vector3() const
    {
        return { r_, g_, b_ };
    }
    explicit constexpr operator Vector4() const
    {
        return { r_, g_, b_, a_ };
    }
#if defined(HAVE_DIRECTX_MATH)
    /// Cast to XMVECTOR
    operator XMath::XMVECTOR() const //NOLINT(hicpp-explicit-conversions)
    {
        return XMath::XMVectorSet(r_, g_, b_, a_);
    }
    constexpr operator XMath::XMFLOAT4() const //NOLINT(hicpp-explicit-conversions)
    {
        return{ r_, g_, b_, a_ };
    }
#endif

    constexpr bool operator==(const Color& rhs) const
    {
        return Equals(r_, rhs.r_) && Equals(g_, rhs.g_) && Equals(b_, rhs.b_) && Equals(a_, rhs.a_);
    }
    constexpr bool operator!=(const Color& rhs) const
    {
        return !Equals(r_, rhs.r_) || !Equals(g_, rhs.g_) || !Equals(b_, rhs.b_) || !Equals(a_, rhs.a_);
    }
    constexpr Color operator+(const Color& rhs) const
    {
        return {
            Clamp(r_ + rhs.r_, 0.0f, 1.0f),
            Clamp(g_ + rhs.g_, 0.0f, 1.0f),
            Clamp(b_ + rhs.b_, 0.0f, 1.0f),
            Clamp(a_ + rhs.a_, 0.0f, 1.0f),
        };
    }
    constexpr Color& operator+=(const Color& rhs)
    {
        r_ = Clamp(r_ + rhs.r_, 0.0f, 1.0f);
        g_ = Clamp(g_ + rhs.g_, 0.0f, 1.0f);
        b_ = Clamp(b_ + rhs.b_, 0.0f, 1.0f);
        a_ = Clamp(a_ + rhs.a_, 0.0f, 1.0f);

        return *this;
    }
    constexpr Color operator-(const Color& rhs) const
    {
        return {
            Clamp(r_ - rhs.r_, 0.0f, 1.0f),
            Clamp(g_ - rhs.g_, 0.0f, 1.0f),
            Clamp(b_ - rhs.b_, 0.0f, 1.0f),
            Clamp(a_ - rhs.a_, 0.0f, 1.0f),
        };
    }
    constexpr Color& operator-=(const Color& rhs)
    {
        r_ = Clamp(r_ - rhs.r_, 0.0f, 1.0f);
        g_ = Clamp(g_ - rhs.g_, 0.0f, 1.0f);
        b_ = Clamp(b_ - rhs.b_, 0.0f, 1.0f);
        a_ = Clamp(a_ - rhs.a_, 0.0f, 1.0f);

        return *this;
    }
    constexpr Color operator*(const Color& rhs) const
    {
        return {
            Clamp(r_ * rhs.r_, 0.0f, 1.0f),
            Clamp(g_ * rhs.g_, 0.0f, 1.0f),
            Clamp(b_ * rhs.b_, 0.0f, 1.0f),
            Clamp(a_ * rhs.a_, 0.0f, 1.0f)
        };
    }
    constexpr Color operator*(float rhs) const
    {
        return {
            Clamp(r_ * rhs, 0.0f, 1.0f),
            Clamp(g_ * rhs, 0.0f, 1.0f),
            Clamp(b_ * rhs, 0.0f, 1.0f),
            a_
        };
    }
    constexpr Color& operator*=(const Color& rhs)
    {
        r_ = Clamp(r_ * rhs.r_, 0.0f, 1.0f);
        g_ = Clamp(g_ * rhs.g_, 0.0f, 1.0f);
        b_ = Clamp(b_ * rhs.b_, 0.0f, 1.0f);
        a_ = Clamp(a_ * rhs.a_, 0.0f, 1.0f);

        return *this;
    }
    constexpr Color& operator*=(float rhs)
    {
        r_ = Clamp(r_ * rhs, 0.0f, 1.0f);
        g_ = Clamp(g_ * rhs, 0.0f, 1.0f);
        b_ = Clamp(b_ * rhs, 0.0f, 1.0f);

        return *this;
    }
    constexpr Color operator/(const Color& rhs) const
    {
        return {
            Clamp(r_ / rhs.r_, 0.0f, 1.0f),
            Clamp(g_ / rhs.g_, 0.0f, 1.0f),
            Clamp(b_ / rhs.b_, 0.0f, 1.0f),
            Clamp(a_ / rhs.a_, 0.0f, 1.0f)
        };
    }
    constexpr Color operator/(float rhs) const
    {
        return {
            Clamp(r_ / rhs, 0.0f, 1.0f),
            Clamp(g_ / rhs, 0.0f, 1.0f),
            Clamp(b_ / rhs, 0.0f, 1.0f),
            a_
        };
    }
    constexpr Color& operator/=(const Color& rhs)
    {
        r_ = Clamp(r_ / rhs.r_, 0.0f, 1.0f);
        g_ = Clamp(g_ / rhs.g_, 0.0f, 1.0f);
        b_ = Clamp(b_ / rhs.b_, 0.0f, 1.0f);
        a_ = Clamp(a_ / rhs.a_, 0.0f, 1.0f);

        return *this;
    }
    constexpr Color& operator/=(float rhs)
    {
        r_ = Clamp(r_ / rhs, 0.0f, 1.0f);
        g_ = Clamp(g_ / rhs, 0.0f, 1.0f);
        b_ = Clamp(b_ / rhs, 0.0f, 1.0f);

        return *this;
    }

    // RGBA
    [[nodiscard]] constexpr uint32_t To32() const
    {
        return ((uint32_t)(a_ * 255.0f) << 24u) | ((uint32_t)(b_ * 255.0f) << 16u) |
            ((uint32_t)(g_ * 255.0f) << 8u) | (uint32_t)(r_ * 255.0f);
    }
    // RGB
    [[nodiscard]] constexpr uint32_t To24() const
    {
        return ((uint32_t)(b_ * 255.0f) << 16u) | ((uint32_t)(g_ * 255.0f) << 8u) | (uint32_t)(r_ * 255.0f);
    }

    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << r_ << " " << g_ << " " << b_ << " " << a_;
        return ss.str();
    }

    [[nodiscard]] const float* Data() const { return &r_; }

    friend std::ostream& operator << (std::ostream& os, const Color& value)
    {
        return os << value.ToString();
    }

    float r_{ 0.0f };
    float g_{ 0.0f };
    float b_{ 0.0f };
    float a_{ 1.0f };
};

static constexpr Color Color_Black = { 0.0f, 0.0f, 0.0f };
static constexpr Color Color_White = { 1.0f, 1.0f, 1.0f };
static constexpr Color Color_Red = { 1.0f, 0.0f, 0.0f };
static constexpr Color Color_Green = { 0.0f, 1.0f, 0.0f };
static constexpr Color Color_Blue = { 0.0f, 0.0f, 1.0f };

} // namespace Math
