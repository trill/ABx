/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HeightMap.h"
#include "MathUtils.h"
#include "Sphere.h"
#include "BoundingBox.h"
#include "ConvexHull.h"
#include "Mesh.h"
#include <sa/Assert.h>
#include "TriangleMesh.h"

namespace Math {

HeightMap::HeightMap() = default;

HeightMap::~HeightMap() = default;

void HeightMap::ProcessData()
{
    const Vector3 localAabbMin(0.0f, header_.minHeight, 0.0f);
    const Vector3 localAabbMax((float)header_.numVertices.x_, header_.maxHeight, (float)header_.numVertices.y_);
    const Vector3 halfExtends = (localAabbMax - localAabbMin) * 0.5f;

    boundingBox_.min_ = -halfExtends;
    boundingBox_.max_ = halfExtends;
}

float HeightMap::GetRawHeight(int x, int z) const
{
    if (heightData_.empty())
        return 0.0f;
    const int _x = Clamp(x, 0, header_.numVertices.x_ - 1);
    const int _z = Clamp(z, 0, header_.numVertices.y_ - 1);
    const size_t offset = static_cast<size_t>(_z * header_.numVertices.x_ + _x);
    return heightData_[offset];
}

Vector3 HeightMap::GetRawNormal(int x, int z) const
{
    const float baseHeight = GetRawHeight(x, z);
    const float nSlope = GetRawHeight(x, z - 1) - baseHeight;
    const float neSlope = GetRawHeight(x + 1, z - 1) - baseHeight;
    const float eSlope = GetRawHeight(x + 1, z) - baseHeight;
    const float seSlope = GetRawHeight(x + 1, z + 1) - baseHeight;
    const float sSlope = GetRawHeight(x, z + 1) - baseHeight;
    const float swSlope = GetRawHeight(x - 1, z + 1) - baseHeight;
    const float wSlope = GetRawHeight(x - 1, z) - baseHeight;
    const float nwSlope = GetRawHeight(x - 1, z - 1) - baseHeight;
    const float up = 0.5f * (spacing_.x_ + spacing_.z_);

    return (Vector3(0.0f, up, nSlope) +
        Vector3(-neSlope, up, neSlope) +
        Vector3(-eSlope, up, 0.0f) +
        Vector3(-seSlope, up, -seSlope) +
        Vector3(0.0f, up, -sSlope) +
        Vector3(swSlope, up, -swSlope) +
        Vector3(wSlope, up, 0.0f) +
        Vector3(nwSlope, up, nwSlope)).Normal();
}

float HeightMap::GetHeight(const Vector3& world) const
{
    // Get local
    const Vector3 position = inverseMatrix_ * world;
    const float xPos = (position.x_ - header_.patchWorldOrigin.x_) / spacing_.x_;
    const float zPos = (position.z_ - header_.patchWorldOrigin.y_) / spacing_.z_;
    float xFrac = Fract(xPos);
    float zFrac = Fract(zPos);
    const int uxPos = static_cast<int>(xPos);
    const int uzPos = static_cast<int>(zPos);
    float h1, h2, h3;

    if (xFrac + zFrac >= 1.0f)
    {
        h1 = GetRawHeight(uxPos + 1, uzPos + 1);
        h2 = GetRawHeight(uxPos, uzPos + 1);
        h3 = GetRawHeight(uxPos + 1, uzPos);
        xFrac = 1.0f - xFrac;
        zFrac = 1.0f - zFrac;
    }
    else
    {
        h1 = GetRawHeight(uxPos, uzPos);
        h2 = GetRawHeight(uxPos + 1, uzPos);
        h3 = GetRawHeight(uxPos, uzPos + 1);
    }

    // 2nd Layer may have -inf for undefined heights
    if (IsNegInfinite(h1) || IsNegInfinite(h2) || IsNegInfinite(h3))
        return -M_INFINITE;

    const float h = h1 * (1.0f - xFrac - zFrac) + h2 * xFrac + h3 * zFrac;

    return matrix_.Scaling().y_ * h + matrix_.Translation().y_;
}

Vector3 HeightMap::GetNormal(const Vector3& world) const
{
    const Vector3 position = inverseMatrix_ * world;
    const float xPos = (position.x_ / spacing_.x_) + ((float)header_.numVertices.x_ / 2.0f);
    const float zPos = (position.z_ / spacing_.z_) + ((float)header_.numVertices.y_ / 2.0f);
    float xFrac = Fract(xPos);
    float zFrac = Fract(zPos);
    Vector3 n1, n2, n3;

    if (xFrac + zFrac >= 1.0f)
    {
        n1 = GetRawNormal((int)xPos + 1, (int)zPos + 1);
        n2 = GetRawNormal((int)xPos, (int)zPos + 1);
        n3 = GetRawNormal((int)xPos + 1, (int)zPos);
        xFrac = 1.0f - xFrac;
        zFrac = 1.0f - zFrac;
    }
    else
    {
        n1 = GetRawNormal((int)xPos, (int)zPos);
        n2 = GetRawNormal((int)xPos + 1, (int)zPos);
        n3 = GetRawNormal((int)xPos, (int)zPos + 1);
    }

    const Vector3 n = (n1 * (1.0f - xFrac - zFrac) + n2 * xFrac + n3 * zFrac).Normal();
    return matrix_.Rotation() * n;
}

void HeightMap::SetMatrix(const Matrix4& matrix)
{
    matrix_ = matrix;
    inverseMatrix_ = matrix_.Inverse();
}

bool HeightMap::Collides(const Sphere& b2, const Vector3& velocity, Vector3& move) const
{
    return b2.Collides(*this, velocity, move);
}

bool HeightMap::Collides(const BoundingBox& b2, const Vector3& velocity, Vector3& move) const
{
    return b2.Collides(*this, velocity, move);
}

bool HeightMap::Collides(const ConvexHull& b2, const Vector3& velocity, Vector3& move) const
{
    return b2.Collides(*this, velocity, move);
}

bool HeightMap::Collides(const TriangleMesh& b2, const Vector3& velocity, Vector3& move) const
{
    return b2.Collides(*this, velocity, move);
}

bool HeightMap::Collides(const HeightMap&, const Vector3&, Vector3&) const //NOLINT
{
    // Can not collide Heightmap with Heightmap
    ASSERT_FALSE();
}

ea::shared_ptr<Mesh> HeightMap::GenerateMesh() const
{
    auto result = ea::make_shared<Math::Mesh>();

    result->vertexData_.reserve((size_t)header_.numVertices.x_ * (size_t)header_.numVertices.y_);
    const float offsetX = ((float)header_.numVertices.x_ * 0.5f);
    const float offsetY = ((float)header_.numVertices.y_ * 0.5f);
    for (int z = 0; z < header_.numVertices.y_; ++z)
    {
        for (int x = 0; x < header_.numVertices.x_; ++x)
        {
            const float fy = GetRawHeight(x, z);
            const float fx = static_cast<float>(x) - offsetX;
            const float fz = static_cast<float>(z) - offsetY;
            result->vertexData_.emplace_back(fx, fy, fz);
        }
    }
    result->vertexCount_ = (size_t)header_.numVertices.x_ * (size_t)header_.numVertices.y_;

    // Create index data
    for (int z = 0; z < header_.numVertices.y_ - 1; ++z)
    {
        for (int x = 0; x < header_.numVertices.x_ - 1; ++x)
        {
            /*
            Normal edge:
            +----+----+
            |\ 1 |\   |
            | \  | \  |
            |  \ |  \ |
            | 2 \|   \|
            +----+----+
            */

            // First triangle
            int i1 = z * header_.numVertices.x_ + x;
            int i2 = (z * header_.numVertices.x_) + x + 1;
            int i3 = (z + 1) * header_.numVertices.x_ + (x + 1);
            result->AddTriangle(i1, i2, i3);
            // Second triangle
            int i4 = (z + 1) * header_.numVertices.x_ + x;
            result->AddTriangle(i3, i4, i1);
        }
    }

    return result;
}

IntVector2 HeightMap::WorldToHeightmap(const Vector3& world)
{
    const Vector3 pos = inverseMatrix_ * world;
    int xPos = static_cast<int>(pos.x_ / spacing_.x_ + 0.5f);
    int zPos = static_cast<int>(pos.z_ / spacing_.z_ + 0.5f);
    xPos = Clamp(xPos, 0, header_.numVertices.x_ - 1);
    zPos = Clamp(zPos, 0, header_.numVertices.y_ - 1);
    return { xPos, zPos };
}

Vector3 HeightMap::HeightmapToWorld(const IntVector2& pixel)
{
    const IntVector2 pos(pixel.x_, header_.numVertices.y_ - 1 - pixel.y_);
    const float xPos = static_cast<float>((float)pos.x_ * spacing_.x_);
    const float zPos = static_cast<float>((float)pos.y_ * spacing_.z_);
    const Vector3 lPos(xPos, 0.0f, zPos);
    Vector3 wPos = matrix_ * lPos;
    wPos.y_ = GetHeight(wPos);

    return wPos;
}

}
