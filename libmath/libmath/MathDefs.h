/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <limits>

namespace Math {

inline constexpr float M_EPSILON = 0.000001f;              // Small value
inline constexpr float M_PIF = 3.14159265358979323846f;    // PI
inline constexpr float M_TWOPI = M_PIF * 2.0f;             // 2*PI
inline constexpr float M_PIHALF = M_PIF / 2.0f;            // PI/2
inline constexpr float M_PIFOURTH = M_PIF / 4.0f;          // PI/4
inline constexpr float M_INFINITE = std::numeric_limits<float>::infinity();

/// Intersection test result.
enum class Intersection
{
    Outside,
    Intersects,
    Inside
};

// These types are used for Lua
using StdVector3 = std::array<float, 3>;
using StdVector4 = std::array<float, 4>;

}
