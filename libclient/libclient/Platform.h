/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace System {

// Get Host platform/distribution, e.g. Windows, manjaro etc.
std::string GetPlatform();

}
