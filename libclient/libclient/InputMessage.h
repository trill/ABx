/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <stdint.h>
#include <sa/Assert.h>

namespace Client {

class Protocol;

/// Message read from network
class InputMessage
{
private:
    static constexpr size_t MaxHeaderSize = 8;
    static constexpr size_t MaxBufferSize = 4096;
    uint8_t buffer_[MaxBufferSize];
    size_t size_{ 0 };
    size_t pos_{ MaxHeaderSize };
    size_t headerPos_{ MaxHeaderSize };

    void CheckRead(size_t size);
    bool CanRead(size_t size) const;
protected:
    friend class Protocol;

    void Reset()
    {
        size_ = 0;
        pos_ = MaxHeaderSize;
        headerPos_ = MaxHeaderSize;
    }
    void SetHeaderSize(size_t size)
    {
        ASSERT(MaxHeaderSize >= size);
        headerPos_ = MaxHeaderSize - size;
        pos_ = headerPos_;
    }
    void FillBuffer(uint8_t *buffer, size_t size);
    void SetMessageSize(uint16_t size) { size_ = size; }
    std::string GetString();
    std::string GetStringEncrypted();
public:
    InputMessage();

    size_t ReadSize() { return Get<uint16_t>(); }
    bool ReadChecksum();

    size_t GetUnreadSize() const { return size_ - (pos_ - headerPos_); }
    size_t GetMessageSize() const { return size_; }

    uint8_t* GetReadBuffer() { return buffer_ + pos_; }
    uint8_t* GetHeaderBuffer() { return buffer_ + headerPos_; }
    uint8_t* GetDataBuffer() { return buffer_ + MaxHeaderSize; }
    size_t GetHeaderSize() const { return (MaxHeaderSize - headerPos_); }
    bool Eof() const { return (pos_ - headerPos_) >= size_; }

    template <typename T>
    T Get()
    {
        CheckRead(sizeof(T));
        size_t p = pos_;
        pos_ += sizeof(T);
        return *reinterpret_cast<T*>(buffer_ + p);
    }
    template <typename T>
    T GetDecrypted()
    {
        // Only strings
        return Get<T>();
    }
};

template<>
inline std::string InputMessage::Get<std::string>()
{
    return GetString();
}
template<>
inline std::string InputMessage::GetDecrypted<std::string>()
{
    return GetStringEncrypted();
}
template<>
inline bool InputMessage::Get<bool>()
{
    return Get<uint8_t>() != 0;
}

}
