/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <mutex>
#include <limits>
#include <string>
#include <abcrypto.hpp>
#include <sa/Assert.h>

namespace Client {
namespace Utils {

class Random
{
private:
    std::mutex lock_;
public:
    Random() = default;
    ~Random() {}

    void Initialize();
    bool GetBool();
    /// 0..1
    float GetFloat();

    template <typename T>
    T Get()
    {
        T r;
        std::lock_guard<std::mutex> lock(lock_);
        arc4random_buf(&r, sizeof(T));
        return static_cast<T>(r);
    }
    void GetBuff(void* buff, size_t len)
    {
        std::lock_guard<std::mutex> lock(lock_);
        arc4random_buf(buff, len);
    }
    std::string GetString(size_t length)
    {
        std::string result;
        result.resize(length);
        for (size_t i = 0; i < length; ++i)
            result[i] = Get<char>(33, 126);
        return result;
    }
    /// Get value from 0..max
    template <typename T>
    T Get(T max)
    {
        return static_cast<T>(static_cast<float>(Get<T>()) /
            static_cast<float>(std::numeric_limits<T>::max()) *
            static_cast<float>(max + 1));
    }
    /// Get value from min..max
    template <typename T>
    T Get(T min, T max)
    {
        ASSERT(max > min);
        return Get<T>(max - min) + min;
    }

    static Random Instance;
};

}
}
