/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Client {

enum class ConnectionError
{
    None = 0,
    ResolveError,
    WriteError,
    ConnectError,
    ReceiveError,
    ConnectTimeout,
    ReadTimeout,
    WriteTimeout,
    DisconnectNoPong,
    WaitError,
};

}
