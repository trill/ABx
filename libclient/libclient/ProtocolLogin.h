/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Protocol.h"
#include <AB/Entities/Account.h>
#include <AB/Entities/Character.h>
#include <AB/Entities/Game.h>
#include <AB/Entities/Service.h>
#include <AB/ProtocolCodes.h>
#include <abcrypto.hpp>
#include "Structs.h"
#include <AB/Packets/LoginPackets.h>

namespace Client {

class ProtocolLogin final : public Protocol
{
public:
    // static protocol information
    static constexpr bool ServerSendsFirst = false;
    static constexpr uint8_t ProtocolIdentifier = AB::ProtocolLoginId;
    using LoggedInCallback = std::function<void(const std::string& accountUuid, const std::string& authToken, AB::Entities::AccountType accType)>;
    using CharlistCallback = std::function<void(const AB::Entities::CharList& chars)>;
    using GamelistCallback = std::function<void(const std::vector<AB::Entities::Game>& outposts)>;
    using ServerlistCallback = std::function<void(const std::vector<AB::Entities::Service>& services)>;
    using CreateAccountCallback = std::function<void()>;
    using CreatePlayerCallback = std::function<void(const std::string& uuid, const std::string& mapUuid)>;
    using AccountKeyAddedCallback = std::function<void()>;
    using CharacterDeletedCallback = std::function<void(const std::string& uuid)>;
private:
    LoggedInCallback loggedInCallback_;
    CharlistCallback charlistCallback_;
    GamelistCallback gamelistCallback_;
    ServerlistCallback serverlistCallback_;
    CreateAccountCallback createAccCallback_;
    CreatePlayerCallback createPlayerCallback_;
    AccountKeyAddedCallback accountKeyAddedCallback_;
    CharacterDeletedCallback characterDeletedCallback_;
    bool firstRecv_{ false };
    void ParseMessage(InputMessage& message);

    void HandleCharList(const AB::Packets::Server::Login::CharacterList& packet);
    void HandleOutpostList(const AB::Packets::Server::Login::OutpostList& packet);
    void HandleServerList(const AB::Packets::Server::Login::ServerList& packet);
    void HandleLoginError(const AB::Packets::Server::Login::Error& packet);
    void HandleCharacterDeleted(const AB::Packets::Server::Login::CharacterDeleted& packet);
    void HandleCreatePlayerSuccess(const AB::Packets::Server::Login::CreateCharacterSuccess& packet);
    void OnReceive(InputMessage& message) override;
public:
    ProtocolLogin(Crypto::DHKeys& keys, asio::io_service& ioService);
    ~ProtocolLogin() override;

    void Login(std::string& host, uint16_t port,
        const std::string& account, const std::string& password,
        const LoggedInCallback& onLoggedIn,
        const CharlistCallback& callback);
    void CreateAccount(std::string& host, uint16_t port,
        const std::string& account, const std::string& password,
        const std::string& email, const std::string& accKey,
        const CreateAccountCallback& callback);
    void CreatePlayer(std::string& host, uint16_t port,
        const std::string& accountUuid, const std::string& token,
        const std::string& charName, const std::string& profUuid,
        uint32_t modelIndex,
        AB::Entities::CharacterSex sex, bool isPvp,
        const CreatePlayerCallback& callback);
    void DeleteCharacter(std::string& host, uint16_t port,
        const std::string& accountUuid, const std::string& token,
        const std::string& uuid,
        const CharacterDeletedCallback& callback);
    void AddAccountKey(std::string& host, uint16_t port,
        const std::string& accountUuid, const std::string& token,
        const std::string& newAccountKey,
        const AccountKeyAddedCallback& callback);
    void GetOutposts(std::string& host, uint16_t port,
        const std::string& accountUuid, const std::string& token,
        const GamelistCallback& callback);
    void GetServers(std::string& host, uint16_t port,
        const std::string& accountUuid, const std::string& token,
        const ServerlistCallback& callback);

    std::string gameHost_;
    uint16_t gamePort_{ 0 };
    std::string fileHost_;
    uint16_t filePort_{ 0 };
};

}
