/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Platform.h"
#include <AB/CommonConfig.h>
#include <fstream>

namespace System {

std::string GetPlatform()
{
#if defined(AB_WINDOWS)
    return "Windows";
#elif defined(AB_UNIX)
    std::ifstream in("/etc/os-release");
    std::string line;
    while (std::getline(in, line))
    {
        if (line.find("ID=") != std::string::npos)
        {
            return line.substr(3);
        }
    }
    return "Linux";
#else
#error Unsupported platform
#endif
}

}
