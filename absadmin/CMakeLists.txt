project (absadmin CXX)

file(GLOB ABSADMIN_SOURCES      absadmin/*.cpp absadmin/*.h)

add_executable(
    absadmin
    ${ABSADMIN_SOURCES}
)

target_precompile_headers(absadmin PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/absadmin/stdafx.h)
#set_property(TARGET absadmin PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
set_property(TARGET absadmin PROPERTY POSITION_INDEPENDENT_CODE ON)

target_link_libraries(absadmin libcommon libaccount less PugiXml json)
target_link_libraries(absadmin chillout ssl crypto z)

install(TARGETS absadmin
    RUNTIME DESTINATION bin
    COMPONENT runtime
)
