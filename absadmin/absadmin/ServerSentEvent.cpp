/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ServerSentEvent.h"
#include <sstream>

namespace Resources {

ServerSentEvent::ServerSentEvent(std::shared_ptr<HttpsServer::Request> request) :
    Resource(request)
{
    header_.emplace("Content-Type", "text/event-stream");
    header_.emplace("Cache-Control", "no-cache");
}

void ServerSentEvent::Send(const std::string& content, std::shared_ptr<HttpsServer::Response> response)
{
    std::stringstream ss;
    std::stringstream ssout;
    if (retry_ != 0)
        ssout << "retry: " << retry_ << "\n";
    if (!event_.empty())
        ssout << "event: " << event_ << "\n";
    ss << content;
    std::string line;
    while (std::getline(ss, line))
    {
        ssout << "data: " << line << "\n";
    }
    ssout << "\n";
    Resource::Send(ssout.str(), response);
}

}
