/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TemplateResource.h"

namespace Resources {

class CreateAccountResource final : public TemplateResource
{
public:
    explicit CreateAccountResource(std::shared_ptr<HttpsServer::Request> request);
};

}
