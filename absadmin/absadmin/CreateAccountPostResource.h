/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include <libaccount/Account.h>

namespace Resources {

class CreateAccountPostResource final : public Resource
{
private:
    Account::CreateAccountResult DoCreate(const std::string& name, const std::string& pass, const std::string& email, const std::string& accKey);

public:
    explicit CreateAccountPostResource(std::shared_ptr<HttpsServer::Request> request) :
        Resource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    {
    }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
