/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ClearCacheResource.h"
#include "ContentTypes.h"
#include <AB/Entities/Service.h>
#include <uuid.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <json.h>

namespace Resources {

bool ClearCacheResource::ClearCache(const std::string& uuid)
{
    if (uuid.empty() || uuids::uuid(uuid).nil())
        return false;


    auto dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::Service s;
    s.uuid = uuid;
    if (!dataClient->Read(s))
        return false;

    if (s.type == AB::Entities::ServiceTypeGameServer)
    {
        auto msgClient = GetSubsystem<Net::MessageClient>();
        if (!msgClient)
            return false;
        Net::MessageMsg msg;
        msg.type_ = Net::MessageType::ClearCache;
        msg.SetBodyString(uuid);
        return msgClient->Write(msg);
    }

    if (s.type == AB::Entities::ServiceTypeDataServer)
    {
        return dataClient->Clear();
    }

    return false;
}

void ClearCacheResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    auto uuidIt = GetFormField("uuid");
    if (!uuidIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing UUID field";
    }
    else
    {
        if (!ClearCache(uuidIt.value()))
        {
            obj["status"] = "Failed";
            obj["message"] = "Failed";
        }
        else
        {
            obj["status"] = "OK";
        }
    }

    Send(obj.Dump(), response);
}

}
