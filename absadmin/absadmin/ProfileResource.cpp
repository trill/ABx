/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProfileResource.h"
#include <sa/StringHash.h>
#include <AB/Entities/Account.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/Xml.h>

namespace Resources {

ProfileResource::ProfileResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/profile.lpp";
}

bool ProfileResource::GetContext(LuaContext& objects)
{
    if (!TemplateResource::GetContext(objects))
        return false;

    const std::string& uuid = session_->values_[sa::StringHash("account_uuid")].GetString();
    AB::Entities::Account account;
    account.uuid = uuid;
    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(account))
        return false;

    kaguya::State& state = objects.GetState();
    std::map<std::string, std::string> xs;
    xs["uuid"] = account.uuid;
    xs["name"] = Utils::XML::Escape(account.name);
    xs["email"] = Utils::XML::Escape(account.email);

    state["account"] = xs;
    return true;
}

std::string ProfileResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Profile";
}

void ProfileResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
