/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TemplateResource.h"

namespace Resources {

class AccountsResource final : public TemplateResource
{
protected:
    std::string GetTitle() const override;
public:
    explicit AccountsResource(std::shared_ptr<HttpsServer::Request> request);
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
