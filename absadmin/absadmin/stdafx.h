/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#if defined(_MSC_VER)
// Decorated name length exceeded
#pragma warning(disable: 4503 4307)
#endif

#include "targetver.h"
#include <sa/Compiler.h>

#include <stdio.h>
#include <stdint.h>

#include <memory>
#include <limits>
#include <unordered_map>

#include <AB/CommonConfig.h>
#include <libcommon/DebugConfig.h>

#if !defined(USE_STANDALONE_ASIO)
#define USE_STANDALONE_ASIO
#endif

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4702 4127)
#   include <lua.hpp>
#   include <kaguya/kaguya.hpp>
PRAGMA_WARNING_POP

#define PROFILING
