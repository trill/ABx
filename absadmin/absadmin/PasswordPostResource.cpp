/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PasswordPostResource.h"
#include "ContentTypes.h"
#include <abcrypto.hpp>
#include <sa/StringHash.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/Logger.h>
#include <json.h>

namespace Resources {

bool PasswordPostResource::ChangePassword(std::string& error)
{
    auto oldPwIt = GetFormField("old_password");
    auto newPwIt = GetFormField("new_password");
    auto new2PwIt = GetFormField("new_password2");
    if (!oldPwIt.has_value() || !newPwIt.has_value() || !new2PwIt.has_value())
        return false;

    std::string uuid = session_->values_[sa::StringHash("account_uuid")].GetString();
    AB::Entities::Account account;
    account.uuid = uuid;
    auto dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(account))
    {
        error = "Invalid Account";
        return false;
    }

    if (bcrypt_checkpass(oldPwIt.value().c_str(), account.password.c_str()) != 0)
    {
        error = "Password check failed";
        return false;
    }
    if ((*newPwIt) != (*new2PwIt))
    {
        error = "Password repeat check failed";
        return false;
    }

    // Create the account
    char pwhash[61];
    if (bcrypt_newhash(newPwIt.value().c_str(), 10, pwhash, 61) != 0)
    {
        LOG_ERROR << "bcrypt_newhash() failed" << std::endl;
        error = "Don't ask!";
        return false;
    }
    std::string passwordHash(pwhash, 61);
    account.password = passwordHash;
    if (!dataClient->Update(account))
    {
        error = "Update failed";
        return false;
    }

    return true;
}

void PasswordPostResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    std::string error;
    if (ChangePassword(error))
        obj["status"] = "OK";
    else
    {
        obj["status"] = "Failed";
        obj["message"] = error;
    }

    Send(obj.Dump(), response);
}

}
