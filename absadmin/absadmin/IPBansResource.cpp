/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IPBansResource.h"
#include "Application.h"
#include "Version.h"
#include <AB/Entities/GameInstance.h>
#include <AB/Entities/Game.h>

namespace Resources {

std::string IPBansResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - IP Bans";
}

IPBansResource::IPBansResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/ipbans.lpp";
    styles_.insert(styles_.begin(), "vendors/css/footable.bootstrap.min.css");
    footerScripts_.push_back("vendors/js/footable.min.js");
}

void IPBansResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
