/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SpawnResource.h"
#include "ContentTypes.h"
#include <sa/ScopeGuard.h>
#include <uuid.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <json.h>

namespace Resources {

bool SpawnResource::Spawn(const std::string& uuid)
{
    if (uuid.empty() || uuids::uuid(uuid).nil())
        return false;

    auto msgClient = GetSubsystem<Net::MessageClient>();
    if (!msgClient)
        return false;
    Net::MessageMsg msg;
    msg.type_ = Net::MessageType::Spawn;
    msg.SetBodyString(uuid);
    return msgClient->Write(msg);
}

void SpawnResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    if (!uuidIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing UUID field";
        return;
    }
    if (!Spawn(uuidIt.value()))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed";
        return;
    }
    obj["status"] = "OK";
}

}
