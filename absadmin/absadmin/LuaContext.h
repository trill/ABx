/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <kaguya/kaguya.hpp>
#include <sstream>
#define LPP_WRITE_FUNC "echo"
#include <sa/lpp.h>

namespace Resources {
class TemplateResource;
}

class LuaContext
{
private:
    kaguya::State luaState_;
    std::stringstream stream_;
    Resources::TemplateResource& resource_;
public:
    explicit LuaContext(Resources::TemplateResource& resource);
    kaguya::State& GetState() { return luaState_; };
    const kaguya::State& GetState() const { return luaState_; }
    const std::stringstream& GetStream() const { return stream_; }
    std::stringstream& GetStream() { return stream_; }
    bool Execute(const std::string& source);
};
