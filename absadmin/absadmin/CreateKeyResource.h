/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"

namespace Resources {

class CreateKeyResource final : public Resource
{
private:
    bool CreateKey(const std::string& uuid, const std::string& keyType,
        const std::string& count, const std::string& keyStatus,
        const std::string& email, const std::string& descr);
public:
    explicit CreateKeyResource(std::shared_ptr<HttpsServer::Request> request) :
        Resource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    { }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
