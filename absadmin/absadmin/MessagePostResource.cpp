/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessagePostResource.h"
#include "ContentTypes.h"
#include <sa/ScopeGuard.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <json.h>

namespace Resources {

bool MessagePostResource::SendMessage(const std::string& body)
{
    if (body.empty())
        return false;

    auto* msgClient = GetSubsystem<Net::MessageClient>();
    if (!msgClient)
        return false;
    Net::MessageMsg msg;
    msg.type_ = Net::MessageType::AdminMessage;
    msg.SetBodyString(body);
    return msgClient->Write(msg);
}

void MessagePostResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto messageIt = GetFormField("message");
    if (!messageIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing Message field";
        return;
    }
    if (!SendMessage(messageIt.value()))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed";
        return;
    }
    obj["status"] = "OK";
}

}
