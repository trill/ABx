/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"

namespace Resources {

class MailsJsonResource final : public Resource
{
public:
    explicit MailsJsonResource(std::shared_ptr<HttpsServer::Request> request) :
        Resource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    { }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
