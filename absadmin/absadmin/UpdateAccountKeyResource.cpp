/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UpdateAccountKeyResource.h"
#include "ContentTypes.h"
#include <AB/Entities/AccountKey.h>
#include <sa/ScopeGuard.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

bool UpdateAccountKeyResource::Update(const std::string& uuid,
    const std::string& filed,
    const std::string& value)
{
    auto dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::AccountKey ak;
    ak.uuid = uuid;
    if (!dataClient->Read(ak))
        return false;

    if (filed == "type")
    {
        int iType = std::atoi(value.c_str());
        if (iType < 0 || iType > AB::Entities::KeyTypeCharSlot)
            return false;
        ak.type = static_cast<AB::Entities::AccountKeyType>(iType);
    }
    else if (filed == "count")
    {
        int iCount = std::atoi(value.c_str());
        if (iCount < 0)
            return false;
        ak.total = static_cast<uint16_t>(iCount);
    }
    else if (filed == "status")
    {
        int iStatus = std::atoi(value.c_str());
        if (iStatus < 0 || iStatus > AB::Entities::KeyStatusBanned)
            return false;
        ak.status = static_cast<AB::Entities::AccountKeyStatus>(iStatus);
    }
    else
        return false;

    return dataClient->Update(ak);
}

void UpdateAccountKeyResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    auto fieldIt = GetFormField("field");
    auto valueIt = GetFormField("value");
    if (!uuidIt.has_value() || !fieldIt.has_value() || !valueIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing field(s)";
        return;
    }
    if (!Update(uuidIt.value(), fieldIt.value(), valueIt.value()))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed";
        return;
    }
    obj["status"] = "OK";
}

}
