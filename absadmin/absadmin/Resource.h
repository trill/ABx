/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Cookie.h"
#include "Sessions.h"
#include <AB/Entities/Account.h>
#include "Servers.h"
#include <sa/Noncopyable.h>
#include <optional>

namespace Resources {

class Resource
{
    NON_COPYABLE(Resource)
    NON_MOVEABLE(Resource)
protected:
    static std::string GetETagValue(const std::string& content);
    std::shared_ptr<HttpsServer::Request> request_;
    SimpleWeb::CaseInsensitiveMultimap header_;
    std::unique_ptr<HTTP::Cookies> requestCookies_;
    std::unique_ptr<HTTP::Cookies> responseCookies_;
    std::shared_ptr<HTTP::Session> session_;
    SimpleWeb::CaseInsensitiveMultimap formValues_;
    SimpleWeb::CaseInsensitiveMultimap queryValues_;
    void Redirect(std::shared_ptr<HttpsServer::Response> response, const std::string& url);
    bool IsAllowed(AB::Entities::AccountType minType);
    virtual void Send(const std::string& content, std::shared_ptr<HttpsServer::Response> response);
    std::string GetRequestHeader(const std::string& key);
public:
    explicit Resource(std::shared_ptr<HttpsServer::Request> request);
    virtual ~Resource() = default;

    virtual void Render(std::shared_ptr<HttpsServer::Response> response) = 0;
    SimpleWeb::CaseInsensitiveMultimap& GetHeaders() { return header_; }
    const SimpleWeb::CaseInsensitiveMultimap& GetHeaders() const { return header_; }
    std::optional<std::string> GetFormField(const std::string& key) const;
    std::optional<std::string> GetQueryValue(const std::string& key) const;
    std::string GetRequestPath() const;
};

}
