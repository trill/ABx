/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GamesResource.h"
#include "Application.h"
#include "Version.h"
#include <AB/Entities/GameInstance.h>
#include <AB/Entities/Game.h>

namespace Resources {

std::string GamesResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Games";
}

GamesResource::GamesResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/games.lpp";
    styles_.insert(styles_.begin(), "vendors/css/footable.bootstrap.min.css");
    footerScripts_.push_back("vendors/js/footable.min.js");
}

void GamesResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
