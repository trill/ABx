/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProfilePostResource.h"
#include "ContentTypes.h"
#include <sa/StringHash.h>
#include <sa/ScopeGuard.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void ProfilePostResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto emailIt = GetFormField("email");
    if (!emailIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing Email field";
        return;
    }
    std::string uuid = session_->values_[sa::StringHash("account_uuid")].GetString();
    AB::Entities::Account account;
    account.uuid = uuid;
    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(account))
    {
        obj["status"] = "Failed";
        obj["message"] = "Invalid Account";
        return;
    }
    account.email = emailIt.value();
    if (dataClient->Update(account))
    {
        obj["status"] = "OK";
        return;
    }
    obj["status"] = "Failed";
    obj["message"] = "Update failed";
}

}
