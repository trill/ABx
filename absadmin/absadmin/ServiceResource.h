/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TemplateResource.h"

namespace Resources {

class ServiceResource final : public TemplateResource
{
private:
    std::string id_;
protected:
    bool GetContext(LuaContext& objects) override;
    std::string GetTitle() const override;
public:
    explicit ServiceResource(std::shared_ptr<HttpsServer::Request> request);
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
