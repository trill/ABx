/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LoginResource.h"
#include "ContentTypes.h"
#include <AB/Entities/Account.h>
#include <abcrypto.hpp>
#include <libcommon/BanManager.h>
#include <sa/StringHash.h>
#include <sa/ScopeGuard.h>
#include <uuid.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

LoginResource::AuthResult LoginResource::Auth(const std::string& user, const std::string& pass)
{
    uint32_t ip = request_->remote_endpoint->address().to_v4().to_uint();

    AB::Entities::Account account;
    account.name = user;
    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(account))
        return AuthResult::WrongUsernamePassword;
    if (account.status != AB::Entities::AccountStatusActivated)
        return AuthResult::NotActivated;
    if (account.type < AB::Entities::AccountType::Normal)
        return AuthResult::InternalError;
    auto* banMan = GetSubsystem<Auth::BanManager>();
    if (Auth::BanManager::IsAccountBanned(uuids::uuid(account.uuid)))
    {
        banMan->AddLoginAttempt(ip, false);
        return AuthResult::Banned;
    }
    if (bcrypt_checkpass(pass.c_str(), account.password.c_str()) != 0)
    {
        banMan->AddLoginAttempt(ip, false);
        return AuthResult::WrongUsernamePassword;
    }
    banMan->AddLoginAttempt(ip, true);

    session_->values_[sa::StringHash("logged_in")] = true;
    session_->values_[sa::StringHash("username")] = user;
    session_->values_[sa::StringHash("account_uuid")] = account.uuid;
    session_->values_[sa::StringHash("account_type")] = static_cast<int>(account.type);

    return AuthResult::Success;
}

void LoginResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto userIt = GetFormField("username");
    auto passIt = GetFormField("password");
    if (!userIt.has_value() || !passIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Username and/or password missing.";
        return;
    }
    auto result = Auth(userIt.value(), passIt.value());
    switch (result)
    {
    case AuthResult::Success:
        obj["status"] = "OK";
        obj["message"] = "OK";
        break;
    case AuthResult::Banned:
        obj["status"] = "Failed";
        obj["message"] = "Your account is banned.";
        break;
    case AuthResult::InternalError:
        obj["status"] = "Failed";
        obj["message"] = "Internal error";
        break;
    case AuthResult::NotActivated:
        obj["status"] = "Failed";
        obj["message"] = "This account is not activated.";
        break;
    case AuthResult::WrongUsernamePassword:
        obj["status"] = "Failed";
        obj["message"] = "Wrong Username and/or password.";
        break;
    }
}

}
