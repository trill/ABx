/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DownloadResource.h"
#include <AB/Entities/Account.h>

namespace Resources {

DownloadResource::DownloadResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/download.lpp";
}

std::string DownloadResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Download";
}

void DownloadResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
