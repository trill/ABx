/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "BanEnableResource.h"
#include "ContentTypes.h"
#include <AB/Entities/Ban.h>
#include <sa/ScopeGuard.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void BanEnableResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    if (!uuidIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing UUID field";
        return;
    }

    std::string uuid = uuidIt.value();
    AB::Entities::Ban ban;
    ban.uuid = uuid;
    auto dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(ban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Invalid UUID";
        return;
    }

    ban.active = true;
    if (dataClient->Update(ban))
        obj["status"] = "OK";
    else
    {
        obj["status"] = "Failed";
        obj["message"] = "Update failed";
    }
}

}
