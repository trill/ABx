/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "CreateKeyResource.h"
#include "Application.h"
#include "ContentTypes.h"
#include <AB/Entities/AccountKey.h>
#include <AB/Entities/AccountKeyList.h>
#include <sa/ScopeGuard.h>
#include <uuid.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <json.h>

namespace Resources {

bool CreateKeyResource::CreateKey(const std::string& uuid,
    const std::string& keyType, const std::string& count,
    const std::string& keyStatus, const std::string& email,
    const std::string & descr)
{
    auto dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::AccountKey ak;
    if (!uuids::uuid(uuid).nil())
        ak.uuid = uuid;
    else
        ak.uuid = Utils::Uuid::New();
    if (dataClient->Read(ak))
        // Already exists
        return false;

    int iType = std::atoi(keyType.c_str());
    if (iType < 0 || iType > AB::Entities::KeyTypeCharSlot)
        return false;
    ak.type = static_cast<AB::Entities::AccountKeyType>(iType);
    int iCount = std::atoi(count.c_str());
    if (iCount < 0)
        return false;
    ak.total = static_cast<uint16_t>(iCount);
    int iStatus = std::atoi(keyStatus.c_str());
    if (iStatus < 0 || iStatus > AB::Entities::KeyStatusBanned)
        return false;
    ak.status = static_cast<AB::Entities::AccountKeyStatus>(iStatus);
    ak.email = email;
    ak.description = descr;

    bool succ = dataClient->Create(ak);
    if (succ)
    {
        AB::Entities::AccountKeyList akl;
        dataClient->Invalidate(akl);
    }
    return succ;
}

void CreateKeyResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    auto keyTypeIt = GetFormField("key_type");
    auto countIt = GetFormField("count");
    auto keyStatusIt = GetFormField("key_status");
    auto emailIt = GetFormField("email");
    auto descrIt = GetFormField("description");
    if (!uuidIt.has_value() || !keyTypeIt.has_value() || !countIt.has_value() ||
        !keyStatusIt.has_value() || !emailIt.has_value() || !descrIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing field(s)";
        return;
    }
    if (!CreateKey(uuidIt.value(), keyTypeIt.value(), countIt.value(),
        keyStatusIt.value(), emailIt.value(), descrIt.value()))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed";
        return;
    }
    obj["status"] = "OK";
}

}
