/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IpBanDeleteResource.h"
#include "ContentTypes.h"
#include <AB/Entities/Ban.h>
#include <sa/ScopeGuard.h>
#include <AB/Entities/IpBan.h>
#include <AB/Entities/IpBanList.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void IpBanDeleteResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    if (!uuidIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing UUID field";
        return;
    }

    std::string uuid = uuidIt.value();
    AB::Entities::IpBan ipban;
    ipban.uuid = uuid;
    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(ipban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Invalid UUID";
        return;
    }

    AB::Entities::Ban ban;
    ban.uuid = ipban.banUuid;
    if (!dataClient->Read(ban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed";
        return;
    }

    if (!dataClient->Delete(ban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Delete failed";
    }
    dataClient->Invalidate(ban);
    if (!dataClient->Delete(ipban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Delete failed";
    }
    dataClient->Invalidate(ipban);

    obj["status"] = "OK";

    AB::Entities::IpBanList banlist;
    dataClient->Invalidate(banlist);
}

}
