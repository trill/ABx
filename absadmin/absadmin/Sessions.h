/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <unordered_map>
#include <memory>
#include <libcommon/Variant.h>

namespace HTTP {

class Session
{
public:
    static uint32_t sessionLifetime_;
    static void RegisterLua(kaguya::State& state);
    Session();
    void Touch();
    bool GetBool(const std::string& key);
    std::string GetString(const std::string& key);
    float GetNumber(const std::string& key);
    void SetBool(const std::string& key, bool value);
    void SetString(const std::string& key, const std::string& value);
    void SetNumber(const std::string& key, float value);
    time_t expires_{ 0 };
    Utils::VariantMap values_;
};

class Sessions
{
private:
    std::unordered_map<std::string, std::shared_ptr<Session>> sessions_;
public:
    Sessions() = default;
    ~Sessions() = default;

    void Add(const std::string& id, std::shared_ptr<Session> session);
    std::shared_ptr<Session> Get(const std::string& id);
};

}
