/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FileResource.h"
#include "Application.h"
#include "ContentTypes.h"
#include <libcommon/FileUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/StringUtils.h>
#include <libcommon/SimpleConfigManager.h>
#include <libcommon/Logger.h>
#include <fstream>
#include <sa/path.h>

namespace Resources {

void FileResource::SendFileRange(std::shared_ptr<HttpsServer::Response> response,
    const std::string& path, const sa::http::Range& range,
    bool multipart, const std::string& boundary)
{
    auto ifs = std::make_shared<std::ifstream>();
    ifs->open(path, std::ifstream::in | std::ios::binary | std::ios::ate);
    ASSERT(ifs);

    auto fileSize = (long)ifs->tellg();
    size_t start = range.start;
    size_t end = (range.end != 0) ? range.end : (size_t)fileSize;
    ASSERT(end > start);
    size_t length = end - start;

    ifs->seekg(start, std::ios::beg);

    if (multipart)
    {
        response->write("--" + boundary);
        response->write("Content-Type: application/octet-stream\n");
        response->write("Content-Range: " + std::to_string(range.start) + "-" +
            std::to_string(range.end) + "/" + std::to_string(fileSize) + "\n");
        response->write("\n");
    }

    struct FileServer
    {
        static void ReadAndSend(const std::shared_ptr<HttpsServer::Response>& response,
            const std::shared_ptr<std::ifstream>& ifs, size_t remaining)
        {
            size_t chunkSize = std::min<size_t>(131072u, remaining);
            std::vector<char> buffer;
            buffer.resize(chunkSize);
            std::streamsize read_length;
            if ((read_length = ifs->read(&buffer[0], static_cast<std::streamsize>(buffer.size())).gcount()) > 0)
            {
                response->write(&buffer[0], read_length);
                if (read_length == static_cast<std::streamsize>(buffer.size()))
                {
                    response->send([response, ifs, remaining, read_length](const SimpleWeb::error_code& ec)
                    {
                        if (!ec)
                        {
                            if ((long)remaining > read_length)
                                ReadAndSend(response, ifs, remaining - read_length);
                        }
                        else
                            LOG_ERROR << "Connection interrupted " << ec.default_error_condition().value() << " " <<
                            ec.default_error_condition().message() << std::endl;
                    });
                }
            }
        }
    };
    FileServer::ReadAndSend(response, ifs, length);
}

void FileResource::SetWholeFile(std::shared_ptr<HttpsServer::Response> response, const std::string& path)
{
    std::ifstream ifs(path, std::ifstream::in | std::ios::binary | std::ios::ate);
    if (!ifs.good())
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found " + request_->path);
        return;
    }
    size_t size = (long)ifs.tellg();
    std::string content;
    content.resize(size);
    ifs.seekg(0);
    ifs.read((char*)&content[0], size);
    Send(content, std::forward<std::shared_ptr<HttpsServer::Response>>(response));
}

void FileResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    const std::string& root = Application::Instance->GetRoot();

    try
    {
        auto web_root_path = fs::canonical(root);
        auto path = fs::canonical((sa::path(root) / sa::path(request_->path)).string());
        // Check if path is within web_root_path
        if (std::distance(web_root_path.begin(), web_root_path.end()) > std::distance(path.begin(), path.end()) ||
            !std::equal(web_root_path.begin(), web_root_path.end(), path.begin()))
        {
            LOG_ERROR << request_->remote_endpoint_address() << ":" << request_->remote_endpoint_port() << ": "
                << "Trying to access file outside root " << path.string() << std::endl;
            throw std::invalid_argument("Path must be within root path");
        }
        if (fs::is_directory(path))
        {
            LOG_ERROR << request_->remote_endpoint_address() << ":" << request_->remote_endpoint_port() << ": "
                << "Trying to access a directory " << path.string() << std::endl;
            throw std::invalid_argument("Not a file");
        }
        if (Utils::IsHiddenFile(path.string()))
        {
            LOG_ERROR << request_->remote_endpoint_address() << ":" << request_->remote_endpoint_port() << ": "
                << "Trying to access a hidden file " << path.string() << std::endl;
            throw std::invalid_argument("Hidden file");
        }


        std::ifstream ifs(path.string(), std::ifstream::in | std::ios::binary | std::ios::ate);
        if (!ifs)
            throw std::invalid_argument("Could not read file");

        const auto fileSize = ifs.tellg();

        const auto rangeHeaderIt = request_->header.find("Range");
        sa::http::Ranges ranges;
        if (rangeHeaderIt == request_->header.end())
        {
            ranges.push_back({ 0, 0, 0 });
        }
        else
        {
            if (!sa::http::ParseRanges(fileSize, rangeHeaderIt->second, ranges))
            {
                response->write(SimpleWeb::StatusCode::client_error_range_not_satisfiable,
                    "Range Not Satisfiable");
                return;
            }
        }

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests
        const bool multipart = ranges.size() > 1;
        const bool isRange = !sa::http::IsFullRange(fileSize, ranges[0]);
        // Multipart not supported
        if (multipart)
        {
            LOG_WARNING << "TODO: Multipart not supported" << std::endl;
            response->write(SimpleWeb::StatusCode::client_error_range_not_satisfiable,
                "Range Not Satisfiable");
            return;
        }

        auto* contT = GetSubsystem<ContentTypes>();
        const std::string contentType = contT->Get(Utils::GetFileExt(request_->path));
        if (!isRange && ContentTypes::ShouldCompress(contentType))
        {
            auto* cfg = GetSubsystem<IO::SimpleConfigManager>();
            auto smallFileSize = cfg->GetGlobalInt("small_file_size", 204800);
            if ((long)fileSize < smallFileSize)
            {
                // If it's a small text file send it as a whole and compress it, add ETag etc.
                header_.emplace("Content-Type", contentType);
                header_.emplace("Cache-Control", "max-age=2592000, public");             //30days (60sec * 60min * 24hours * 30days)
                SetWholeFile(response, path.string());
                return;
            }
        }

        const std::string boundary = "3d6b6a416f9b5";

        responseCookies_->Write(header_);
        if (isRange)
        {
            // Single part of a file
            header_.emplace("Content-Type", contentType);
            header_.emplace("Cache-Control", "max-age=2592000, public");             //30days (60sec * 60min * 24hours * 30days)
            header_.emplace("Content-Length", std::to_string(ranges[0].length));
            header_.emplace("Content-Range", std::to_string(ranges[0].start) + "-" +
                std::to_string(ranges[0].end) + "/" + std::to_string(fileSize));
            response->write(SimpleWeb::StatusCode::success_partial_content, header_);
        }
        else
        {
            // Whole file
            header_.emplace("Content-Type", contentType);
            header_.emplace("Cache-Control", "max-age=2592000, public");             //30days (60sec * 60min * 24hours * 30days)
            header_.emplace("Content-Length", std::to_string(fileSize));
            response->write(SimpleWeb::StatusCode::success_ok, header_);
        }

        SendFileRange(response, path.string(), ranges[0], multipart, boundary);
    }
    catch (const std::exception& ex)
    {
        LOG_ERROR << ex.what() << std::endl;
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found " + request_->path);
    }
}

}
