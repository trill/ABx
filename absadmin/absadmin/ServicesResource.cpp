/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ServicesResource.h"
#include "Application.h"
#include "Version.h"
#include <AB/Entities/ServiceList.h>
#include <AB/Entities/Service.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/Xml.h>

namespace Resources {

bool ServicesResource::GetContext(LuaContext& objects)
{
    if (!TemplateResource::GetContext(objects))
        return false;

    auto dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::ServiceList sl;
    if (!dataClient->Read(sl))
        return false;

    kaguya::State& state = objects.GetState();
    state["services"] = kaguya::NewTable();
    int i = 0;
    for (const std::string& uuid : sl.uuids)
    {
        AB::Entities::Service s;
        s.uuid = uuid;
        if (!dataClient->Read(s))
            continue;

        state["services"][++i] = kaguya::TableData{
            { "uuid", s.uuid },
            { "name", Utils::XML::Escape(s.name) },
            { "file", Utils::XML::Escape(s.file) },
            { "host", Utils::XML::Escape(s.host) },
            { "load", std::to_string(s.load) },
            { "location", Utils::XML::Escape(s.location) },
            { "path", Utils::XML::Escape(s.path) },
            { "port", s.port },
            { "run_time", s.runTime },
            { "start_time", s.startTime },
            { "stop_time", s.stopTime },
            { "status", s.status },
            { "online", (bool)(s.status == AB::Entities::ServiceStatusOnline) },
            { "type", s.type }
        };
    }

    return true;
}

std::string ServicesResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Services";
}

ServicesResource::ServicesResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/services.lpp";

    footerScripts_.push_back("vendors/js/gauge.min.js");
}

void ServicesResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
