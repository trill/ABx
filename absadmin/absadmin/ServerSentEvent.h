/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"

namespace Resources {

class ServerSentEvent : public Resource
{
protected:
    unsigned retry_{ 0 };
    std::string event_;
    void Send(const std::string& content, std::shared_ptr<HttpsServer::Response> response) override;
public:
    explicit ServerSentEvent(std::shared_ptr<HttpsServer::Request> request);
};

}
