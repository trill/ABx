/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ServicesJsonResource.h"
#include <AB/Entities/ServiceList.h>
#include <AB/Entities/Service.h>
#include "ContentTypes.h"
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void ServicesJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::ServiceList sl;
    if (!dataClient->Read(sl))
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }

    sa::json::JSON obj = sa::json::Object();
    obj["uuids"] = sa::json::Array();
    obj["services"] = sa::json::Array();
    for (const std::string& uuid : sl.uuids)
    {
        AB::Entities::Service s;
        s.uuid = uuid;
        if (!dataClient->Read(s))
            continue;
        auto serv = sa::json::Object();
        serv["uuid"] = s.uuid;
        serv["name"] = s.name;
        serv["file"] = s.file;
        serv["host"] = s.host;
        serv["load"] = s.load;
        serv["location"] = s.location;
        serv["path"] = s.path;
        serv["port"] = s.port;
        serv["run_time"] = static_cast<long>(s.runTime);
        serv["status"] = static_cast<int>(s.status);
        serv["online"] = s.status == AB::Entities::ServiceStatusOnline;
        serv["type"] = static_cast<int>(s.type);

        obj["uuids"].Append(s.uuid);
        obj["services"].Append(serv);
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
