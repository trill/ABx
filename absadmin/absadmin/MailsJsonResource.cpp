/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MailsJsonResource.h"
#include "ContentTypes.h"
#include <uuid.h>
#include <sa/time.h>
#include <AB/Entities/MailList.h>
#include <AB/Entities/Mail.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void MailsJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    sa::json::JSON obj = sa::json::Array();

    std::string uuid = session_->values_[sa::StringHash("account_uuid")].GetString();
    auto* dataClient = GetSubsystem<IO::DataClient>();

    AB::Entities::MailList list;
    list.uuid = uuid;
    if (!dataClient->Read(list))
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }
    for (const auto& header : list.mails)
    {
        AB::Entities::Mail mail;
        mail.uuid = header.uuid;
        if (!dataClient->Read(mail))
            continue;

        auto mailsJson = sa::json::Object();
        mailsJson["uuid"] = header.uuid;
        mailsJson["from"] = header.fromName;
        mailsJson["date_time"] = header.created;
        mailsJson["subject"] = header.subject;
        mailsJson["read"] = header.isRead ? 1 : 0;
        mailsJson["attachments"] = header.numAttachments;
        mailsJson["body"] = mail.message;

        obj.Append(mailsJson);
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
