/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ContentTypes.h"

bool ContentTypes::IsText(const std::string& contentType)
{
    return contentType.starts_with("text/") ||
        contentType.find("javascript") != std::string::npos ||
        contentType.find("json") != std::string::npos ||
        contentType.find("xml") != std::string::npos ||
        contentType.find("svg") != std::string::npos;
}

bool ContentTypes::ShouldCompress(const std::string& contentType)
{
    return IsText(contentType) || contentType.starts_with("font/");
}

const std::string& ContentTypes::Get(const std::string& ext) const
{
    static const std::string def = "application/octet-stream";
    const auto it = map_.find(ext);
    if (it == map_.end())
        return def;
    return (*it).second;
}
