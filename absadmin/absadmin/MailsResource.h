/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TemplateResource.h"

namespace Resources {

class MailsResource final : public TemplateResource
{
protected:
    std::string GetTitle() const override;
public:
    explicit MailsResource(std::shared_ptr<HttpsServer::Request> request);
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
