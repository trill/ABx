/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NewsCreateResource.h"
#include "ContentTypes.h"
#include <sa/ScopeGuard.h>
#include <libcommon/UuidUtils.h>
#include <sa/StringTempl.h>
#include <sa/time.h>
#include <AB/Entities/News.h>
#include <AB/Entities/NewsList.h>
#include <limits>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void NewsCreateResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto content = GetFormField("content");

    if (!content.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing content field";
        return;
    }

    AB::Entities::News news;
    news.uuid = Utils::Uuid::New();
    news.created = sa::time::tick();
    news.body = content.value();

    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Create(news))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed creating news";
        return;
    }

    AB::Entities::LatestNewsList latestlist;
    dataClient->Invalidate(latestlist);
    AB::Entities::AllNewsList alllist;
    dataClient->Invalidate(alllist);

    obj["status"] = "OK";
}

}
