/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TemplateResource.h"

namespace Resources {

class IndexResource final : public TemplateResource
{
protected:
    bool GetContext(LuaContext& objects) override;
public:
    explicit IndexResource(std::shared_ptr<HttpsServer::Request> request);
};

}
