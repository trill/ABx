/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Cookie.h"
#include <sa/StringTempl.h>
#include <sa/time.h>

namespace HTTP {

void Cookie::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Cookie"].setClass(std::move(kaguya::UserdataMetatable<Cookie>()
        .addProperty("Content", &Cookie::GetContent, &Cookie::SetContent)
        .addProperty("Domain", &Cookie::GetDomain, &Cookie::SetDomain)
        .addProperty("Path", &Cookie::GetPath, &Cookie::SetPath)
        .addProperty("Expires", &Cookie::GetExpires, &Cookie::SetExpires)
        .addProperty("HttpOnly", &Cookie::IsHttpOnly, &Cookie::SetHttpOnly)
        .addProperty("SameSite", &Cookie::GetSameSite, &Cookie::SetSameSite)
        .addProperty("Secure", &Cookie::IsSecure, &Cookie::SetSecure)
    ));
    // clang-format on
}

Cookie::Cookie()
{
    time(&expires_);
    std::tm tm = sa::time::localtime(expires_);
    tm.tm_year++;
    expires_ = mktime(&tm);
}

Cookie::Cookie(std::string content) :
    content_(std::move(content))
{
    time(&expires_);
    std::tm tm = sa::time::localtime(expires_);
    tm.tm_year++;
    expires_ = mktime(&tm);
}

void Cookies::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Cookies"].setClass(std::move(kaguya::UserdataMetatable<Cookies>()
        .addFunction("Get", &Cookies::Get)
        .addFunction("Add", &Cookies::Add)
        .addFunction("Delete", &Cookies::Delete)
    ));
    // clang-format on
}

Cookies::Cookies(const HttpsServer::Request& request)
{
    // Cookie: CONSENT=WP.272d88; 1P_JAR=2018-10-26-06; NID=144=UE-02yrDztg9wZze3sLgWzEs4YR713X4Onjzj0uEzpIUWlymHKJ9ZcJsrbbWjZMci0c8wQAFVH6pTT-hZQvcbsr77wKG5mGfVkS2OH1bXfFdIlEzBMzOZrESgpNSJnHZdoyZdiGd3tTd6MpvtDJo4X34hgMD1ZPUsKJvVQr_pLI
    const SimpleWeb::CaseInsensitiveMultimap& header = request.header;
    const auto it = header.find("Cookie");
    if (it == header.end())
        return;
    const auto& cHeader = (*it).second;
    const std::vector<std::string> parts = sa::Split(cHeader, ";");
    for (const auto& part : parts)
    {
        std::string trimPart = sa::Trim(part);
        std::vector<std::string> c = sa::Split(trimPart, "=");
        if (c.size() == 2)
        {
            cookies_.emplace(c[0], std::make_unique<Cookie>(c[1]));
        }
    }
}

void Cookies::Write(SimpleWeb::CaseInsensitiveMultimap& header)
{
    std::vector<std::string> cookies;

    VisitCookies([&cookies](const std::string& name, const Cookie& cookie)
    {
        std::string c = name + "=";
        c += cookie.GetContent() + "; ";
        // Sat, 26-Oct-2019 08:24:53 GMT
        std::tm p = sa::time::gmtime(cookie.GetExpires());
        c += "expires=" + sa::time::put_time(&p, "%a, %e-%b-%G %T GMT") + "; ";
        c += "path=" + cookie.GetPath() + "; ";
        if (!cookie.GetDomain().empty())
            c += "domain=." + cookie.GetDomain() + "; ";
        switch (cookie.GetSameSite())
        {
        case Cookie::SameSite::Lax:
            c += "SameSite=Lax; ";
            break;
        case Cookie::SameSite::Strict:
            c += "SameSite=Strict; ";
            break;
        case Cookie::SameSite::None:
            c += "SameSite=None; ";
            break;
        }
        if (cookie.IsSecure())
            c += "Secure; ";
        if (cookie.IsHttpOnly())
            c += "HttpOnly; ";
        c = c.substr(0, c.size() - 2);
        cookies.push_back(c);
        return Iteration::Continue;
    });

    if (!cookies.empty())
    {
        std::string content = sa::CombineString<char>(cookies, "");
        header.emplace("Set-Cookie", content);
    }
}

Cookie* Cookies::Add(const std::string& name)
{
    std::unique_ptr<Cookie> cookiePtr = std::make_unique<Cookie>();
    Cookie* result = cookiePtr.get();
    cookies_.emplace(name, std::move(cookiePtr));
    return result;
}

Cookie* Cookies::Get(const std::string& name)
{
    const auto it = cookies_.find(name);
    if (it == cookies_.end())
        return nullptr;
    return (*it).second.get();
}

void Cookies::Delete(const std::string& name)
{
    auto it = cookies_.find(name);
    if (it != cookies_.end())
        cookies_.erase(it);
}

}
