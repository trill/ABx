/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Resource.h"
#include <uuid.h>
#include "Application.h"
#include <libcommon/SimpleConfigManager.h>
#include <sa/StringHash.h>
#define SA_ZLIB_SUPPORT
#include <sa/compress.h>
extern "C" {
#include <sha256.h>
}
#include <sa/hash.h>
#include <libcommon/Subsystems.h>
#include <libcommon/Logger.h>

namespace Resources {

std::string Resource::GetETagValue(const std::string& content)
{
    sa::Hash<unsigned char, 20> hash;
    sha256_ctx ctx;
    sha256_init(&ctx);
    for (unsigned char c : content)
        sha256_update(&ctx, (const unsigned char*)&c, 1);
    sha256_final(&ctx, hash.Data());
    return hash.ToString();
}

Resource::Resource(std::shared_ptr<HttpsServer::Request> request) :
    request_(std::move(request)),
    header_(Application::GetDefaultHeader())
{
    std::stringstream ss;
    ss << request_->content.rdbuf();
    formValues_ = SimpleWeb::QueryString::parse(ss.str());
    request_->content.seekg(0, std::istream::beg);
    queryValues_ = request_->parse_query_string();

    responseCookies_ = std::make_unique<HTTP::Cookies>();
    requestCookies_ = std::make_unique<HTTP::Cookies>(*request_);
    HTTP::Cookie* sessCookie = requestCookies_->Get("SESSION_ID");
    const std::string sessId = (sessCookie == nullptr) ? Utils::Uuid::New() : sessCookie->GetContent();

    auto* sessions = GetSubsystem<HTTP::Sessions>();
    session_ = sessions->Get(sessId);
    if (!session_)
    {
        // Create a session with this ID
        session_ = std::make_shared<HTTP::Session>();
        sessions->Add(sessId, session_);
    }
    // Update expiry date/time
    session_->Touch();
    auto* config = GetSubsystem<IO::SimpleConfigManager>();

    HTTP::Cookie* respSessCookie = responseCookies_->Add("SESSION_ID");
    respSessCookie->SetContent(sessId);
    respSessCookie->SetExpires(session_->expires_);
    respSessCookie->SetDomain(Application::Instance->GetHost());
    respSessCookie->SetHttpOnly(true);
    respSessCookie->SetSameSite(static_cast<HTTP::Cookie::SameSite>(config->GetGlobalInt("cookie_samesite", 0)));
    respSessCookie->SetSecure(config->GetGlobalBool("cookie_secure", false));
}

void Resource::Send(const std::string& content, std::shared_ptr<HttpsServer::Response> response)
{
    const std::string etag = GetETagValue(content);
    header_.emplace("ETag", etag);
    responseCookies_->Write(header_);
    const std::string requestETag = GetRequestHeader("If-None-Match");

    if (requestETag.length() == 40)
    {
        const sa::Hash<char, 20> responseHash(etag);
        const sa::Hash<char, 20> requestHash(requestETag);
        if (requestHash && responseHash == requestHash)
        {
            response->write(SimpleWeb::StatusCode::redirection_not_modified, "Not Modified", header_);
            return;
        }
    }

    const std::string accept = GetRequestHeader("Accept-Encoding");
    if (accept.find("gzip") != std::string::npos)
    {
        sa::zlib_compress compress;
        std::string compressed;
        compressed.resize(content.length() + 1024);
        size_t compressedSize = compressed.length();
        if (compress(content.data(), content.length(), compressed.data(), compressedSize))
        {
            header_.emplace("Content-Encoding", "gzip");
            compressed.resize(compressedSize);
            header_.emplace("Content-Length", std::to_string(compressedSize));
            response->write(compressed, header_);
            return;
        }
        LOG_ERROR << "Compression Error" << std::endl;
    }
    header_.emplace("Content-Length", std::to_string(content.length()));
    response->write(content, header_);
}

std::optional<std::string> Resource::GetFormField(const std::string& key) const
{
    const auto it = formValues_.find(key);
    if (it == formValues_.end())
        return {};
    return it->second;
}

std::optional<std::string> Resource::GetQueryValue(const std::string& key) const
{
    const auto it = queryValues_.find(key);
    if (it == queryValues_.end())
        return {};
    return it->second;
}

std::string Resource::GetRequestPath() const
{
    return request_->path;
}

void Resource::Redirect(std::shared_ptr<HttpsServer::Response> response, const std::string& url)
{
    auto header = Application::GetDefaultHeader();
    header.emplace("Location", url);
    responseCookies_->Write(header);
    response->write(SimpleWeb::StatusCode::redirection_found, "Found", header);
}

bool Resource::IsAllowed(AB::Entities::AccountType minType)
{
    using namespace sa::literals;
    bool loggedIn = session_->values_["logged_in"_Hash].GetBool();
    if (!loggedIn)
    {
        return minType == AB::Entities::AccountType::Unknown;
    }
    auto accIt = session_->values_.find("account_type"_Hash);
    AB::Entities::AccountType accType = AB::Entities::AccountType::Unknown;
    if (accIt != session_->values_.end())
        accType = static_cast<AB::Entities::AccountType>((*accIt).second.GetInt());

    return accType >= minType;
}

std::string Resource::GetRequestHeader(const std::string& key)
{
    const auto it = request_->header.find(key);
    if (it == request_->header.end())
        return "";
    return (*it).second;
}

}
