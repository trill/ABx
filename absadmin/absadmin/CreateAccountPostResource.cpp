/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "CreateAccountPostResource.h"
#include <AB/CommonConfig.h>
#include <AB/Entities/AccountKey.h>
#include <AB/Entities/AccountKeyAccounts.h>
#include <AB/Entities/AccountList.h>
#include <libcommon/DataClient.h>
#include <libcommon/Logger.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <sa/ScopeGuard.h>
#include <uuid.h>
#include <abcrypto.hpp>
#include <json.h>
#include "ContentTypes.h"

namespace Resources {

Account::CreateAccountResult CreateAccountPostResource::DoCreate(const std::string& name,
    const std::string& pass,
    const std::string& email,
    const std::string& accKey)
{
    auto res = Account::CreateAccount(name, pass, email, accKey);
    if (res != Account::CreateAccountResult::OK)
        return res;

    // Login
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Account acc;
    acc.name = name;
    if (client->Read(acc))
    {
        session_->values_[sa::StringHash("logged_in")] = true;
        session_->values_[sa::StringHash("username")] = name;
        session_->values_[sa::StringHash("account_uuid")] = acc.uuid;
        session_->values_[sa::StringHash("account_type")] = static_cast<int>(acc.type);
    }
    else
        LOG_ERROR << "Error reading account " << name << std::endl;

    return res;
}

void CreateAccountPostResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]() { Send(obj.Dump(), response); });

    auto userIt = GetFormField("username");
    auto passIt = GetFormField("password");
    auto pass2It = GetFormField("password_repeat");
    if (userIt.value_or("").empty() || passIt.value_or("").empty() || pass2It.value_or("").empty())
    {
        obj["status"] = "Failed";
        obj["message"] = "Username and/or password missing.";
        return;
    }
    auto emailIt = GetFormField("email");
#if defined(EMAIL_MANDATORY)
    if (emailIt.value_or("").empty())
    {
        obj["status"] = "Failed";
        obj["message"] = "Email can not be empty.";
        return;
    }
#endif
    if (passIt.value() != pass2It.value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Password and repeated password do not match.";
        return;
    }
    auto accKeyIt = GetFormField("account_key");
    if (accKeyIt.value_or("").empty())
    {
        obj["status"] = "Failed";
        obj["message"] = "Account key can not be empty.";
        return;
    }
    Account::CreateAccountResult res = DoCreate(
        userIt.value(), passIt.value(), emailIt.value_or(""), accKeyIt.value());
    switch (res)
    {
    case Account::CreateAccountResult::OK:
        obj["status"] = "OK";
        obj["message"] = "OK";
        break;
    case Account::CreateAccountResult::AlreadyAdded:
        obj["status"] = "Failed";
        obj["message"] = "Account key already used.";
        break;
    case Account::CreateAccountResult::EmailError:
        obj["status"] = "Failed";
        obj["message"] = "Email can not be empty.";
        break;
    case Account::CreateAccountResult::InternalError:
        obj["status"] = "Failed";
        obj["message"] = "Internal error.";
        break;
    case Account::CreateAccountResult::InvalidAccount:
        obj["status"] = "Failed";
        obj["message"] = "Invalid account.";
        break;
    case Account::CreateAccountResult::InvalidAccountKey:
        obj["status"] = "Failed";
        obj["message"] = "Invalid account key.";
        break;
    case Account::CreateAccountResult::NameExists:
        obj["status"] = "Failed";
        obj["message"] = "Username already exists.";
        break;
    case Account::CreateAccountResult::PasswordError:
        obj["status"] = "Failed";
        obj["message"] = "Password can not be empty.";
        break;

    default:
        ASSERT_FALSE();
    }
}

}
