/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include <sa/http_range.h>

namespace Resources {

class FileResource : public Resource
{
private:
    void SendFileRange(std::shared_ptr<HttpsServer::Response> response,
        const std::string& path, const sa::http::Range& range,
        bool multipart, const std::string& boundary);
    void SetWholeFile(std::shared_ptr<HttpsServer::Response> response,
        const std::string& path);
public:
    explicit FileResource(std::shared_ptr<HttpsServer::Request> request) :
        Resource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    { }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
