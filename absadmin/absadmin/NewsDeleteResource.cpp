/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NewsDeleteResource.h"
#include "ContentTypes.h"
#include <sa/ScopeGuard.h>
#include <libcommon/UuidUtils.h>
#include <sa/StringTempl.h>
#include <AB/Entities/News.h>
#include <AB/Entities/NewsList.h>
#include <limits>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void NewsDeleteResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuid = GetFormField("uuid");

    if (!uuid.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing uuid field";
        return;
    }

    AB::Entities::News news;
    news.uuid = uuid.value();

    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Delete(news))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed deleting news";
        return;
    }

    AB::Entities::LatestNewsList latestlist;
    dataClient->Invalidate(latestlist);
    AB::Entities::AllNewsList alllist;
    dataClient->Invalidate(alllist);

    obj["status"] = "OK";
}

}
