/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IndexResource.h"
#include "Application.h"
#include <sa/StringHash.h>
#include "Version.h"
#include <AB/Entities/Account.h>
#include <AB/Entities/AccountList.h>
#include <AB/Entities/CharacterList.h>
#include <AB/Entities/Service.h>
#include <AB/Entities/ServiceList.h>
#include <AB/Entities/GameInstanceCount.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>

namespace Resources {

bool IndexResource::GetContext(LuaContext& objects)
{
    if (!TemplateResource::GetContext(objects))
        return false;
    bool loggedIn = session_->values_[sa::StringHash("logged_in")].GetBool();
    if (!loggedIn)
        return true;

    auto dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::AccountList al;
    if (!dataClient->Read(al))
        return false;

    kaguya::State& state = objects.GetState();
    state["total_accounts"] = al.uuids.size();

    AB::Entities::CharacterList cl;
    if (!dataClient->Read(cl))
        return false;
    state["total_chars"] = cl.uuids.size();

    int64_t uptime = 0;
    AB::Entities::ServiceList services;
    if (dataClient->Read(services))
    {
        for (const auto& uuid : services.uuids)
        {
            AB::Entities::Service service;
            service.uuid = uuid;
            if (dataClient->Read(service))
            {
                if (service.type == AB::Entities::ServiceTypeGameServer)
                    uptime = std::max(uptime, service.runTime);
            }
        }
    }
    state["uptime"] = uptime;

    AB::Entities::GameInstanceCount count;
    if (dataClient->Read(count))
        state["instance_count"] = count.count;
    else
        state["instance_count"] = 0;


    return true;
}

IndexResource::IndexResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    bool loggedIn = session_->values_[sa::StringHash("logged_in")].GetBool();
    if (loggedIn)
        template_ = "../templates/dashboard.lpp";
    else
        template_ = "../templates/login.lpp";
}

}
