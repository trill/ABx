/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GamesJsonResource.h"
#include "ContentTypes.h"
#include <AB/Entities/GameInstance.h>
#include <AB/Entities/Game.h>
#include <AB/Entities/GameInstanceList.h>
#include <AB/Entities/Service.h>
#include <uuid.h>
#include <sa/time.h>
#include <uuid.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/Xml.h>
#include <json.h>

namespace Resources {

void GamesJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::GameInstanceList al;
    if (!dataClient->Read(al))
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }

    sa::json::JSON obj = sa::json::Array();
    // https://raw.githubusercontent.com/fooplugins/FooTable/V3/docs/content/rows.json
    // https://datatables.net/examples/ajax/objects.html

    for (const auto& uuid : al.uuids)
    {
        AB::Entities::GameInstance inst;
        inst.uuid = uuid;
        if (!dataClient->Read(inst))
            continue;

        AB::Entities::Game game;
        game.uuid = inst.gameUuid;
        if (uuids::uuid(game.uuid).nil() || !dataClient->Read(game))
        {
            game.name = "None";
        }
        AB::Entities::Service serv;
        serv.uuid = inst.serverUuid;
        if (uuids::uuid(serv.uuid).nil() || !dataClient->Read(serv))
        {
            serv.name = "None";
        }

        auto instanceJson = sa::json::Object();
        instanceJson["uuid"] = inst.uuid;
        instanceJson["type"] = static_cast<int>(game.type);
        instanceJson["start"] = inst.startTime;
        if (inst.stopTime == 0)
            instanceJson["time"] = (sa::time::tick() - inst.startTime);
        else
            instanceJson["time"] = (inst.stopTime - inst.startTime);
        instanceJson["players"] = inst.players;
        instanceJson["name"] = Utils::XML::Escape(game.name);
        instanceJson["number"] = inst.number;
        instanceJson["current_server"] = inst.serverUuid;
        instanceJson["current_server_name"] = Utils::XML::Escape(serv.name);
        instanceJson["current_server_link"] = "<a href=\"service?id=" + inst.serverUuid + "\">" + Utils::XML::Escape(serv.name) + "</a>";
        instanceJson["current_instance"] = inst.uuid;
        instanceJson["current_instance_link"] = "<a href=\"game?id=" + inst.uuid + "\">" + Utils::XML::Escape(game.name) + "</a>";

        obj.Append(instanceJson);
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
