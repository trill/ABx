/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FriendsResource.h"
#include <AB/Entities/Account.h>

namespace Resources {

bool FriendsResource::GetContext(LuaContext& objects)
{
    // TODO: Implement this
    (void)objects;
    return false;
}

std::string FriendsResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Friends";
}

FriendsResource::FriendsResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
}

void FriendsResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}
}
