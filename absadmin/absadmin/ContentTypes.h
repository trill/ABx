/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <unordered_map>

class ContentTypes
{
public:
    static bool IsText(const std::string& contentType);
    static bool ShouldCompress(const std::string& contentType);
    ContentTypes() = default;
    ~ContentTypes() = default;

    std::unordered_map<std::string, std::string> map_;

    const std::string& Get(const std::string& ext) const;
};
