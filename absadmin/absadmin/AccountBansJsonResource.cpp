/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AccountBansJsonResource.h"
#include <AB/Entities/AccountBanList.h>
#include <AB/Entities/AccountList.h>
#include <AB/Entities/Account.h>
#include <AB/Entities/Ban.h>
#include <AB/Entities/Character.h>
#include <AB/Entities/Service.h>
#include <AB/Entities/GameInstance.h>
#include <AB/Entities/Game.h>
#include "ContentTypes.h"
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/UuidUtils.h>
#include <json.h>

namespace Resources {

void AccountBansJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        Redirect(response, "/");
        return;
    }

    auto uuid = GetQueryValue("id");
    if (!uuid.has_value())
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }

    auto* dataClient = GetSubsystem<IO::DataClient>();
    sa::json::JSON obj = sa::json::Array();

    AB::Entities::AccountBanList bans;
    bans.uuid = uuid.value();
    if (!dataClient->Read(bans))
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }

    for (const auto& ban_uuid : bans.uuids)
    {
        AB::Entities::Ban ban;
        ban.uuid = ban_uuid;
        if (!dataClient->Read(ban))
            continue;

        auto banJson = sa::json::Object();
        AB::Entities::Account adminAccount;
        adminAccount.uuid = ban.adminUuid;
        if (!Utils::Uuid::IsEmpty(ban.adminUuid))
            dataClient->Read(adminAccount);

        banJson["uuid"] = ban.uuid;
        banJson["active"] = ban.active;
        banJson["added"] = ban.added;
        banJson["expires"] = ban.expires == std::numeric_limits<int64_t>::max() ? 0 : ban.expires;
        banJson["comment"] = ban.comment;
        banJson["hits"] = ban.hits;
        banJson["admin_uuid"] = ban.adminUuid;
        banJson["admin_name"] = adminAccount.name.empty() ? "Unknown" : adminAccount.name;
        obj.Append(banJson);
    }
    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
