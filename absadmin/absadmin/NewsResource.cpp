/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NewsResource.h"
#include "Application.h"
#include "Version.h"
#include <AB/Entities/GameInstance.h>
#include <AB/Entities/Game.h>

namespace Resources {

NewsResource::NewsResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/news.lpp";
    styles_.insert(styles_.begin(), "vendors/css/footable.bootstrap.min.css");
    footerScripts_.push_back("vendors/js/footable.min.js");
}

std::string NewsResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - News";
}

void NewsResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
