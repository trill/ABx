/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Sessions.h"
#include <sa/StringHash.h>
#include <sa/time.h>

namespace HTTP {

uint32_t Session::sessionLifetime_ = (1000 * 60 * 10);

void Session::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Session"].setClass(std::move(kaguya::UserdataMetatable<Session>()
        .addFunction("GetBool", &Session::GetBool)
        .addFunction("GetString", &Session::GetString)
        .addFunction("GetNumber", &Session::GetNumber)
        .addFunction("SetBool", &Session::SetBool)
        .addFunction("SetString", &Session::SetString)
        .addFunction("SetNumber", &Session::SetNumber)
    ));
    // clang-format on
}

Session::Session()
{
    Touch();
}

void Session::Touch()
{
    time(&expires_);
    std::tm tm = sa::time::localtime(expires_);
    tm.tm_sec += sessionLifetime_ / 1000;
    expires_ = mktime(&tm);
}

bool Session::GetBool(const std::string& key)
{
    const auto it = values_.find(sa::StringHash(key));
    if (it == values_.end())
        return false;
    return it->second.GetBool();
}

std::string Session::GetString(const std::string& key)
{
    const auto it = values_.find(sa::StringHash(key));
    if (it == values_.end())
        return "";
    return it->second.GetString();
}

float Session::GetNumber(const std::string& key)
{
    const auto it = values_.find(sa::StringHash(key));
    if (it == values_.end())
        return false;
    switch (it->second.GetType())
    {
    case Utils::VariantType::Int:
        return (float)it->second.GetInt();
    case Utils::VariantType::Int64:
        return (float)it->second.GetInt64();
    case Utils::VariantType::Float:
        return it->second.GetFloat();
    default:
        return 0.0f;
    }
}

void Session::SetBool(const std::string& key, bool value)
{
    values_.emplace(sa::StringHash(key), value);
}

void Session::SetString(const std::string& key, const std::string& value)
{
    values_.emplace(sa::StringHash(key), value);
}

void Session::SetNumber(const std::string& key, float value)
{
    values_.emplace(sa::StringHash(key), value);
}

void Sessions::Add(const std::string& id, std::shared_ptr<Session> session)
{
    sessions_[id] = std::move(session);
}

std::shared_ptr<Session> Sessions::Get(const std::string& id)
{
    auto it = sessions_.find(id);
    if (it == sessions_.end())
        return std::shared_ptr<Session>();
    time_t now = {};
    time(&now);
    if ((*it).second->expires_ < now)
    {
        sessions_.erase(it);
        return std::shared_ptr<Session>();
    }
    return (*it).second;
}

}
