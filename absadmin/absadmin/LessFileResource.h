/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "FileResource.h"
#include <mutex>

namespace Resources {

class LessFileResource final : public FileResource
{
private:
    std::mutex mutex_;
public:
    explicit LessFileResource(std::shared_ptr<HttpsServer::Request> request) :
        FileResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    { }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
