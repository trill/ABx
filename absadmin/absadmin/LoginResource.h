/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"

namespace Resources {

class LoginResource final : public Resource
{
private:
    enum class AuthResult
    {
        Success = 0,
        Banned,
        WrongUsernamePassword,
        NotActivated,
        InternalError,
    };
    AuthResult Auth(const std::string& user, const std::string& pass);
public:
    explicit LoginResource(std::shared_ptr<HttpsServer::Request> request) :
        Resource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
    { }
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
};

}
