/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageResource.h"
#include <AB/Entities/Account.h>

namespace Resources {

MessageResource::MessageResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/message.lpp";
}

std::string MessageResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Message";
}

void MessageResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
