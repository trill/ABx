/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ServiceResource.h"
#include "Application.h"
#include <AB/Entities/ServiceList.h>
#include <AB/Entities/Service.h>
#include <uuid.h>
#include <sa/time.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/Xml.h>

namespace Resources {

bool ServiceResource::GetContext(LuaContext& objects)
{
    if (!TemplateResource::GetContext(objects))
        return false;

    if (id_.empty() || uuids::uuid(id_).nil())
        return false;

    auto* dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::Service s;
    s.uuid = id_;
    if (!dataClient->Read(s))
        return false;

    kaguya::State& state = objects.GetState();

    state["uuid"] = s.uuid;
    state["machine"] = Utils::XML::Escape(s.machine);
    state["name"] = Utils::XML::Escape(s.name);
    state["location"] = Utils::XML::Escape(s.location);
    state["host"] = Utils::XML::Escape(s.host);
    state["port"] = std::to_string(s.port);
    state["ip"] = s.ip.empty() ? "0.0.0.0" : s.ip;
    state["file"] = Utils::XML::Escape(s.file);
    state["path"] = Utils::XML::Escape(s.path);
    state["arguments"] = Utils::XML::Escape(s.arguments);
    state["spawnable"] = (s.type == AB::Entities::ServiceTypeFileServer ||
        s.type == AB::Entities::ServiceTypeGameServer) && (s.status == AB::Entities::ServiceStatusOnline);
    state["has_cache"] = (s.type == AB::Entities::ServiceTypeGameServer || s.type == AB::Entities::ServiceTypeDataServer) &&
        (s.status == AB::Entities::ServiceStatusOnline);
    state["online"] = s.status == AB::Entities::ServiceStatusOnline;
    // Temporary are always running
    state["termable"] = s.temporary;
    state["version"] = std::to_string(AB_VERSION_GET_MAJOR(s.version)) + "." + std::to_string(AB_VERSION_GET_MINOR(s.version));
    const sa::time::time_span ts = sa::time::get_time_span(static_cast<uint32_t>(s.runTime));
    std::stringstream ss;
    if (ts.months > 0)
        ss << ts.months << " month(s) ";
    if (ts.days > 0)
        ss << ts.days << " day(s) ";
    ss << ts.hours << "h " << ts.minutes << "m " << ts.seconds << "s";

    state["uptime"] = Utils::XML::Escape(ss.str());

    return true;
}

std::string ServiceResource::GetTitle() const
{
    return TemplateResource::GetTitle() + " - Service";
}

ServiceResource::ServiceResource(std::shared_ptr<HttpsServer::Request> request) :
    TemplateResource(std::forward<std::shared_ptr<HttpsServer::Request>>(request))
{
    template_ = "../templates/service.lpp";

    SimpleWeb::CaseInsensitiveMultimap form = request_->parse_query_string();
    auto it = form.find("id");
    if (it != form.end())
        id_ = (*it).second;
}

void ServiceResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        Redirect(response, "/");
        return;
    }

    TemplateResource::Render(response);
}

}
