/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LuaContext.h"
#include <libcommon/Xml.h>
#include "Sessions.h"
#include "Cookie.h"
#include "TemplateResource.h"
#include <libcommon/Logger.h>
#include <sa/time.h>

static void LuaErrorHandler(int errCode, const char* message)
{
    LOG_ERROR << "Lua Error (" << errCode << "): " << message << std::endl;
}

LuaContext::LuaContext(Resources::TemplateResource& resource) :
    resource_(resource)
{
    luaState_.setErrorHandler(LuaErrorHandler);
    HTTP::Cookie::RegisterLua(luaState_);
    HTTP::Cookies::RegisterLua(luaState_);
    HTTP::Session::RegisterLua(luaState_);
    luaState_["session"] = resource.GetSession();
    luaState_["requestCookies"] = resource.GetRequestCookies();
    luaState_["responseCookies"] = resource.GetResponseCookies();
    // The function which writes to the output stream
    luaState_["echo"] = kaguya::function([this](const std::string& value)
    {
        stream_ << value;
    });
    luaState_["escape"] = kaguya::function([](const std::string& value) -> std::string
    {
        return Utils::XML::Escape(value);
    });
    luaState_["tick"] = kaguya::function([]() -> int64_t
    {
        return sa::time::tick();
    });
    luaState_["getHeader"] = kaguya::function([this](const std::string& key) -> std::string
    {
        const auto& headers = resource_.GetHeaders();
        const auto it = headers.find(key);
        if (it == headers.end())
            return "";
        return (*it).second;
    });
    luaState_["setHeader"] = kaguya::function([this](const std::string& key, const std::string& value)
    {
        auto& headers = resource_.GetHeaders();
        headers.emplace(key, value);
    });
    luaState_["getContentType"] = kaguya::function([this]() -> std::string
    {
        const auto& headers = resource_.GetHeaders();
        const auto it = headers.find("Content-Type");
        if (it == headers.end())
            return "";
        return (*it).second;
    });
    luaState_["getPath"] = kaguya::function([this]() -> std::string
    {
        return resource_.GetRequestPath();
    });
    luaState_["setContentType"] = kaguya::function([this](const std::string& value)
    {
        auto& headers = resource_.GetHeaders();
        headers.emplace("Content-Type", value);
    });
    luaState_["getFormField"] = kaguya::function([this](const std::string& key) -> std::string
    {
        const auto value = resource_.GetFormField(key);
        return value.value_or("");
    });
    luaState_["getQueryValue"] = kaguya::function([this](const std::string& key) -> std::string
    {
        const auto value = resource_.GetQueryValue(key);
        return value.value_or("");
    });
}

bool LuaContext::Execute(const std::string& source)
{
    stream_.clear();
    bool result = sa::lpp::Run(luaState_.state(), source);
    if (!result)
    {
        std::istringstream ss(source);
        int line = 0;
        std::string ln;
        while (std::getline(ss, ln))
        {
            std::cout << ++line << ": " << ln << std::endl;
        }
    }
    return result;
}
