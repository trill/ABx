/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IPBansJsonResource.h"
#include "ContentTypes.h"
#include <uuid.h>
#include <sa/time.h>
#include <AB/Entities/IpBanList.h>
#include <AB/Entities/IpBan.h>
#include <AB/Entities/Ban.h>
#include <AB/Entities/Account.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/UuidUtils.h>
#include <json.h>

namespace Resources {

void IPBansJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* dataClient = GetSubsystem<IO::DataClient>();
    AB::Entities::IpBanList list;
    if (!dataClient->Read(list))
    {
        response->write(SimpleWeb::StatusCode::client_error_not_found,
            "Not found");
        return;
    }

    sa::json::JSON obj = sa::json::Array();

    for (const auto& uuid : list.uuids)
    {
        AB::Entities::IpBan ipban;
        ipban.uuid = uuid;
        if (!dataClient->Read(ipban))
            continue;

        AB::Entities::Ban ban;
        ban.uuid = ipban.banUuid;
        if (!dataClient->Read(ban))
            continue;

        AB::Entities::Account adminAccount;
        adminAccount.uuid = ban.adminUuid;
        if (!Utils::Uuid::IsEmpty(ban.adminUuid))
            dataClient->Read(adminAccount);

        auto banJson = sa::json::Object();
        banJson["uuid"] = ipban.banUuid;
        banJson["ipban_uuid"] = ipban.uuid;
        banJson["ip"] = Utils::ConvertIPToString(ipban.ip);
        banJson["mask"] = Utils::ConvertIPToString(ipban.mask);
        banJson["active"] = ban.active;
        banJson["added"] = ban.added;
        banJson["expires"] = ban.expires == std::numeric_limits<int64_t>::max() ? 0 : ban.expires;
        banJson["comment"] = ban.comment;
        banJson["hits"] = ban.hits;
        banJson["admin_uuid"] = ban.adminUuid;
        banJson["admin_name"] = adminAccount.name.empty() ? "Unknown" : adminAccount.name;

        obj.Append(banJson);
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
