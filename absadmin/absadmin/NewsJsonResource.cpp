/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NewsJsonResource.h"
#include "ContentTypes.h"
#include <uuid.h>
#include <sa/time.h>
#include <AB/Entities/News.h>
#include <AB/Entities/NewsList.h>
#include <AB/Entities/Account.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void NewsJsonResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::Normal))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    sa::json::JSON obj = sa::json::Array();

    auto* dataClient = GetSubsystem<IO::DataClient>();
    auto all = GetQueryValue("all");

    if (all.has_value())
    {
        if (!IsAllowed(AB::Entities::AccountType::Gamemaster))
        {
            response->write(SimpleWeb::StatusCode::client_error_unauthorized,
                "Unauthorized");
            return;
        }

        AB::Entities::AllNewsList list;
        if (!dataClient->Read(list))
        {
            response->write(SimpleWeb::StatusCode::client_error_not_found,
                "Not found");
            return;
        }
        for (const auto& uuid : list.uuids)
        {
            AB::Entities::News news;
            news.uuid = uuid;
            if (!dataClient->Read(news))
                continue;

            auto newsJson = sa::json::Object();
            newsJson["uuid"] = news.uuid;
            newsJson["created"] = news.created;
            newsJson["body"] = news.body;

            obj.Append(newsJson);
        }
    }
    else
    {
        AB::Entities::LatestNewsList list;
        if (!dataClient->Read(list))
        {
            response->write(SimpleWeb::StatusCode::client_error_not_found,
                "Not found");
            return;
        }
        for (const auto& uuid : list.uuids)
        {
            AB::Entities::News news;
            news.uuid = uuid;
            if (!dataClient->Read(news))
                continue;

            auto newsJson = sa::json::Object();
            newsJson["uuid"] = news.uuid;
            newsJson["created"] = news.created;
            newsJson["body"] = news.body;

            obj.Append(newsJson);
        }
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));
    Send(obj.Dump(), response);
}

}
