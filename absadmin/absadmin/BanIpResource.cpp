/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "BanIpResource.h"
#include "ContentTypes.h"
#include <AB/Entities/Ban.h>
#include <sa/ScopeGuard.h>
#include <libcommon/UuidUtils.h>
#include <sa/StringTempl.h>
#include <sa/StringHash.h>
#include <sa/time.h>
#include <AB/Entities/IpBan.h>
#include <AB/Entities/IpBanList.h>
#include <limits>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void BanIpResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();

    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto ipField = GetFormField("ip");
    auto maskField = GetFormField("mask");
    auto expiresField = GetFormField("expires");

    if (!ipField.has_value() || !maskField.has_value() || !expiresField.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing form field";
        return;
    }
    auto expires = sa::ToNumber<int>(expiresField.value());
    if (!expires.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Wrong expires value";
        return;
    }
    uint32_t ip = Utils::ConvertStringToIP(ipField.value());
    if (ip == 0)
    {
        obj["status"] = "Failed";
        obj["message"] = "IP can not be zero";
        return;
    }
    uint32_t mask = Utils::ConvertStringToIP(maskField.value());
    if (mask == 0)
    {
        obj["status"] = "Failed";
        obj["message"] = "Mask can not be zero";
        return;
    }

    AB::Entities::Ban ban;
    ban.uuid = Utils::Uuid::New();
    using namespace sa::time::literals;
    switch (expires.value())
    {
    case 1:
        ban.expires = sa::time::tick() + 1_W;
        break;
    case 2:
        ban.expires = sa::time::tick() + 1_M;
        break;
    case 3:
        ban.expires = sa::time::tick() + 3_M;
        break;
    case 4:
        ban.expires = sa::time::tick() + 1_Y;
        break;
    case 9:
        ban.expires = std::numeric_limits<int64_t>::max();
        break;
    default:
        ban.expires = sa::time::tick() + expires.value();
        break;
    }
    ban.added = sa::time::tick();
    ban.reason = AB::Entities::BanReasonOther;
    ban.active = true;

    auto adminIt = session_->values_.find(sa::StringHash("account_uuid"));
    if (adminIt != session_->values_.end())
        ban.adminUuid = adminIt->second.GetString();

    auto commentField = GetFormField("comment");
    if (commentField.has_value())
    {
        ban.comment = commentField.value();
    }

    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Create(ban))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed creating ban";
        return;
    }

    AB::Entities::IpBan ipBan;
    ipBan.uuid = Utils::Uuid::New();
    ipBan.ip = ip;
    ipBan.mask = mask;
    ipBan.banUuid = ban.uuid;
    if (!dataClient->Create(ipBan))
    {
        obj["status"] = "Failed";
        obj["message"] = "Failed creating IP ban";
        return;
    }

    AB::Entities::IpBanList banList;
    dataClient->Invalidate(banList);

    obj["status"] = "OK";
}

}
