/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AccountPostResource.h"
#include "ContentTypes.h"
#include <sa/ScopeGuard.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <json.h>

namespace Resources {

void AccountPostResource::Render(std::shared_ptr<HttpsServer::Response> response)
{
    if (!IsAllowed(AB::Entities::AccountType::God))
    {
        response->write(SimpleWeb::StatusCode::client_error_unauthorized,
            "Unauthorized");
        return;
    }

    auto* contT = GetSubsystem<ContentTypes>();
    header_.emplace("Content-Type", contT->Get(".json"));

    sa::json::JSON obj = sa::json::Object();
    sa::ScopeGuard send([&]()
    {
        Send(obj.Dump(), response);
    });

    auto uuidIt = GetFormField("uuid");
    if (!uuidIt.has_value())
    {
        obj["status"] = "Failed";
        obj["message"] = "Missing UUID field";
        return;
    }
    std::string uuid = uuidIt.value();
    AB::Entities::Account account;
    account.uuid = uuid;
    auto* dataClient = GetSubsystem<IO::DataClient>();
    if (!dataClient->Read(account))
    {
        obj["status"] = "Failed";
        obj["message"] = "Invalid Account";
        return;
    }
    auto typeIt = GetFormField("account_type");
    if (typeIt.has_value())
    {
        int t = std::atoi(typeIt.value().c_str());
        account.type = static_cast<AB::Entities::AccountType>(t);
    }
    auto statusIt = GetFormField("account_status");
    if (statusIt.has_value())
    {
        int s = std::atoi(statusIt.value().c_str());
        account.status = static_cast<AB::Entities::AccountStatus>(s);
    }

    if (dataClient->Update(account))
    {
        obj["status"] = "OK";
        return;
    }
    obj["status"] = "Failed";
    obj["message"] = "Update failed";
}

}
