/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include <kaguya/kaguya.hpp>
#include "LuaContext.h"

namespace Resources {

class TemplateResource : public Resource
{
private:
    void LoadTemplates(std::string& result);
    std::string GetTemplateFile(const std::string& templ);
protected:
    std::vector<std::string> headerScripts_;
    std::vector<std::string> footerScripts_;
    std::vector<std::string> styles_;
    std::string template_;
    virtual bool GetContext(LuaContext& objects);
    virtual std::string GetTitle() const;
public:
    explicit TemplateResource(std::shared_ptr<HttpsServer::Request> request);
    void Render(std::shared_ptr<HttpsServer::Response> response) override;
    HTTP::Session* GetSession() const;
    HTTP::Cookies* GetRequestCookies() const;
    HTTP::Cookies* GetResponseCookies() const;
};

}
