/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_SQLITE

#include "Database.h"
#include <sqlite3.h>
#include <map>

namespace DB {

class DatabaseSqlite final : public Database
{
protected:
    sqlite3* handle_{ nullptr };
    std::recursive_mutex lock_;
    bool InternalQuery(const std::string& query) override;
    std::shared_ptr<DBResult> InternalSelectQuery(const std::string& query) override;
public:
    DatabaseSqlite(const std::string& file);
    ~DatabaseSqlite() override;

    bool BeginTransaction() override;
    bool Rollback() override;
    bool Commit() override;

    bool GetParam(DBParam param) override;
    uint64_t GetLastInsertId() override;
    std::string EscapeString(const std::string& s) override;
    std::string EscapeBlob(const char* s, size_t length) override;
    void FreeResult(DBResult* res) override;
    bool CheckConnection() override { return true; }
};

class SqliteResult final : public DBResult
{
    friend class DatabaseSqlite;
protected:
    explicit SqliteResult(sqlite3_stmt* res);

    using ListNames = std::map<std::string, uint32_t>;
    ListNames listNames_;
    sqlite3_stmt* handle_;
    bool rowAvailable_;
public:
    ~SqliteResult() override;
    int32_t GetInt(const std::string& col) override;
    uint32_t GetUInt(const std::string& col) override;
    int64_t GetLong(const std::string& col) override;
    uint64_t GetULong(const std::string& col) override;
    time_t GetTime(const std::string& col) override;
    std::string GetString(const std::string& col) override;
    std::string GetStream(const std::string& col) override;
    bool IsNull(const std::string& col) override;

    bool Empty() const override { return !rowAvailable_; }
    std::shared_ptr<DBResult> Next() override;
};

}

#endif
