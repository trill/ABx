/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_MYSQL

#include "Database.h"
#ifdef _WIN32 // hack for broken mysql.h not including the correct winsock header for SOCKET definition, fixed in 5.7
#include <winsock2.h>
#endif
#include <mysql/mysql.h>

namespace DB {

class DatabaseMysql final : public Database
{
protected:
    MYSQL handle_;
    bool InternalQuery(const std::string& query) override;
    std::shared_ptr<DBResult> InternalSelectQuery(const std::string& query) override;
public:
    DatabaseMysql();
    ~DatabaseMysql() override;

    bool BeginTransaction() override;
    bool Rollback() override;
    bool Commit() override;

    bool GetParam(DBParam param) override;
    uint64_t GetLastInsertId() override;
    std::string EscapeString(const std::string& s) override;
    std::string EscapeBlob(const char* s, size_t length) override;
    void FreeResult(DBResult* res) override;
};

class MysqlResult final : public DBResult
{
    friend class DatabaseMysql;
protected:
    explicit MysqlResult(MYSQL_RES* res);

    typedef std::map<const std::string, uint32_t> ListNames;
    ListNames listNames_;
    MYSQL_RES* handle_;
    MYSQL_ROW row_;
public:
    ~MysqlResult() override;
    int32_t GetInt(const std::string& col) override;
    uint32_t GetUInt(const std::string& col) override;
    int64_t GetLong(const std::string& col) override;
    uint64_t GetULong(const std::string& col) override;
    time_t GetTime(const std::string& col) override;
    std::string GetString(const std::string& col) override;
    std::string GetStream(const std::string& col) override;
    bool IsNull(const std::string& col) override;

    bool Empty() const override { return row_ == NULL; }
    std::shared_ptr<DBResult> Next() override;
};

}

#endif
