/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifdef HAVE_SQLITE

#include "DatabaseSqlite.h"
#include <libcommon/Logger.h>

namespace DB {

DatabaseSqlite::DatabaseSqlite(const std::string& file)
{
    connected_ = false;

    std::fstream fin(file.c_str(), std::ios::in | std::ios::binary);
    if (fin.fail()) {
        LOG_ERROR << "Failed to initialize SQLite connection. File " << file <<
            " does not exist." << std::endl;
        return;
    }
    fin.close();

    if (sqlite3_open(file.c_str(), &handle_) != SQLITE_OK)
    {
        LOG_ERROR << "Failed to initialize SQLite connection." << std::endl;
        sqlite3_close(handle_);
    }
    else
        connected_ = true;
}

DatabaseSqlite::~DatabaseSqlite()
{
    sqlite3_close(handle_);
}

bool DatabaseSqlite::GetParam(DBParam param)
{
    switch (param)
    {
    case DBPARAM_MULTIINSERT:
        return false;
    default:
        return false;
    }
}

bool DatabaseSqlite::BeginTransaction()
{
    return ExecuteQuery("BEGIN");
}

bool DatabaseSqlite::Rollback()
{
    return ExecuteQuery("ROLLBACK");
}

bool DatabaseSqlite::Commit()
{
    return ExecuteQuery("COMMIT");
}

bool DatabaseSqlite::InternalQuery(const std::string &query)
{
    std::lock_guard<std::recursive_mutex> lockClass(lock_);

    if (!connected_)
        return false;

#ifdef DEBUG_SQL
    LOG_DEBUG << "SQLITE QUERY: " << query << std::endl;
#endif

    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(handle_, query.c_str(), (int)query.length(), &stmt, NULL) != SQLITE_OK)
    {
        sqlite3_finalize(stmt);
        LOG_ERROR << "sqlite3_prepare_v2(): SQLITE ERROR: " << sqlite3_errmsg(handle_) << " (" << query << ")" << std::endl;
        return false;
    }

    int ret = sqlite3_step(stmt);
    if (ret != SQLITE_OK && ret != SQLITE_DONE && ret != SQLITE_ROW)
    {
        sqlite3_finalize(stmt);
        LOG_ERROR << "sqlite3_step(): SQLITE ERROR: " << sqlite3_errmsg(handle_) << " (" << query << ")" << std::endl;
        return false;
    }

    sqlite3_finalize(stmt);

    return true;
}

std::shared_ptr<DBResult> DatabaseSqlite::InternalSelectQuery(const std::string &query)
{
    std::lock_guard<std::recursive_mutex> lockClass(lock_);

    if (!connected_)
        return std::shared_ptr<DBResult>();

#ifdef __DEBUG_SQL__
    LOG_DEBUG << "SQLITE QUERY: " << query << std::endl;
#endif

    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(handle_, query.c_str(), (int)query.length(), &stmt, NULL) != SQLITE_OK)
    {
        sqlite3_finalize(stmt);
        LOG_ERROR << "sqlite3_prepare_v2(): SQLITE ERROR: " << sqlite3_errmsg(handle_) << " (" << query << ")" << std::endl;
        return std::shared_ptr<DBResult>();
    }

    std::shared_ptr<DBResult> results(new SqliteResult(stmt),
        std::bind(&Database::FreeResult, this, std::placeholders::_1));
    return VerifyResult(results);
}

uint64_t DatabaseSqlite::GetLastInsertId()
{
    return (uint64_t)sqlite3_last_insert_rowid(handle_);
}

std::string DatabaseSqlite::EscapeString(const std::string &s)
{
    if (!s.size())
        return std::string("''");

    char* output = new char[s.length() * 2 + 3];
    sqlite3_snprintf((int)s.length() * 2 + 3, output, "%Q", s.c_str());
    std::string r(output);
    delete[] output;
    return r;
}

std::string DatabaseSqlite::EscapeBlob(const char* s, size_t length)
{
    std::stringstream r;
    r << "'" << base64::encode((const unsigned char*)s, length) << "'";
    return r.str();
}

void DatabaseSqlite::FreeResult(DBResult* res)
{
    delete (SqliteResult*)res;
}

SqliteResult::SqliteResult(sqlite3_stmt* res) :
    handle_(res),
    rowAvailable_(false)
{
    listNames_.clear();
    int32_t fields = sqlite3_column_count(handle_);
    for (int32_t i = 0; i < fields; i++)
    {
        listNames_[sqlite3_column_name(handle_, i)] = i;
    }
}

SqliteResult::~SqliteResult()
{
    sqlite3_finalize(handle_);
}

int32_t SqliteResult::GetInt(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return sqlite3_column_int(handle_, it->second);

    LOG_ERROR << "Error GetInt(" << col << ")." << std::endl;
    return 0;
}

uint32_t SqliteResult::GetUInt(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return static_cast<uint32_t>(sqlite3_column_int(handle_, it->second));

    LOG_ERROR << "Error GetUInt(" << col << ")." << std::endl;
    return 0;
}

int64_t SqliteResult::GetLong(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return sqlite3_column_int64(handle_, it->second);

    LOG_ERROR << "Error GetLong(" << col << ")." << std::endl;
    return 0;
}

uint64_t SqliteResult::GetULong(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return static_cast<uint64_t>(sqlite3_column_int64(handle_, it->second));

    LOG_ERROR << "Error GetULong(" << col << ")." << std::endl;
    return 0;
}

time_t SqliteResult::GetTime(const std::string& col)
{
    return static_cast<time_t>(GetLong(col));
}

std::string SqliteResult::GetString(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return std::string((const char*)sqlite3_column_text(handle_, it->second));

    LOG_ERROR << "Error GetString(" << col << ")." << std::endl;
    return "";
}

std::string SqliteResult::GetStream(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
    {
        const char* value = (const char*)sqlite3_column_blob(handle_, it->second);
        int size = sqlite3_column_bytes(handle_, it->second);
        return base64::decode(value, size);
    }

    LOG_ERROR << "Error GetStream(" << col << ")." << std::endl;
    return "";
}

bool SqliteResult::IsNull(const std::string& col)
{
    ListNames::iterator it = listNames_.find(col);
    if (it != listNames_.end())
        return sqlite3_column_type(handle_, it->second) == SQLITE_NULL;

    LOG_ERROR << "Error IsNull(" << col << ")." << std::endl;
    return true;
}

std::shared_ptr<DBResult> SqliteResult::Next()
{
    // checks if after moving to next step we have a row result
    bool rowAvail = (sqlite3_step(handle_) == SQLITE_ROW);
    return rowAvail ? shared_from_this() : std::shared_ptr<DBResult>();
}

}

#endif
