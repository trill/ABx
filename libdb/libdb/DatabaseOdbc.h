/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_ODBC

#include "Database.h"
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define _WINSOCKAPI_
#include <windows.h>
#else
#include <sqltypes.h>
#endif

#include <sql.h>
#include <sqlext.h>

namespace DB {

/// ODBC Database driver. Should only be used with MS SQL Server.
class DatabaseOdbc final : public Database
{
private:
    /// Number of active transactions.
    unsigned transactions_;
protected:
    SQLHDBC handle_;
    SQLHENV env_;
    bool BeginTransaction() override;
    bool Rollback() override;
    bool Commit() override;
    bool InternalQuery(const std::string& query) override;
    std::shared_ptr<DBResult> InternalSelectQuery(const std::string& query) override;
    DatabaseOdbc();
    ~DatabaseOdbc() override;

    bool GetParam(DBParam param) override;
    uint64_t GetLastInsertId() override;
    std::string EscapeString(const std::string& s) override;
    std::string EscapeBlob(const char* s, size_t length) override;
    void FreeResult(DBResult* res) override;
};

class OdbcResult : public DBResult
{
    friend class DatabaseOdbc;
protected:
    OdbcResult(SQLHSTMT stmt);

    typedef std::map<const std::string, uint32_t> ListNames;
    ListNames listNames_;
    bool rowAvailable_;

    SQLHSTMT handle_;
public:
    ~OdbcResult() override;
    int32_t GetInt(const std::string& col) override;
    uint32_t GetUInt(const std::string& col) override;
    int64_t GetLong(const std::string& col) override;
    uint64_t GetULong(const std::string& col) override;
    time_t GetTime(const std::string& col) override;
    std::string GetString(const std::string& col) override;
    std::string GetStream(const std::string& col) override;
    bool IsNull(const std::string& col) override;

    bool Empty() const override { return !rowAvailable_; }
    std::shared_ptr<DBResult> Next() override;
};

}

#endif
