/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Database.h"
#ifdef HAVE_MYSQL
#include "DatabaseMysql.h"
#endif // HAVE_MYSQL
#ifdef HAVE_PGSQL
#include "DatabasePgsql.h"
#endif // HAVE_PGSQL
#ifdef HAVE_ODBC
#include "DatabaseOdbc.h"
#endif // HAVE_ODBC
#ifdef HAVE_SQLITE
#include "DatabaseSqlite.h"
#endif
#include <libcommon/Logger.h>

namespace DB {

std::string Database::driver_ = "";
std::string Database::dbHost_ = "";
std::string Database::dbName_ = "";
std::string Database::dbUser_ = "";
std::string Database::dbPass_ = "";
uint16_t Database::dbPort_ = 0;

Database* Database::CreateInstance(const std::string& driver,
    const std::string& host, uint16_t port,
    const std::string& user, const std::string& pass,
    const std::string& name)
{
    Database::driver_ = driver;
    Database::dbHost_ = host;
    Database::dbName_ = name;
    Database::dbUser_ = user;
    Database::dbPass_ = pass;
    Database::dbPort_ = port;
#ifdef HAVE_MYSQL
    if (driver == "mysql")
        return new DatabaseMysql();
#endif
#ifdef HAVE_PGSQL
    if (driver == "pgsql")
        return new DatabasePgsql();
#endif
#ifdef HAVE_ODBC
    if (driver == "odbc")
        return new DatabaseOdbc();
#endif
#ifdef HAVE_SQLITE
    if (driver == "sqlite")
        return new DatabaseSqlite(Database::dbName_);
#endif
    LOG_ERROR << "Unknown/unsupported database driver " << driver << std::endl;
    return nullptr;
}

bool Database::ExecuteQuery(const std::string& query)
{
    return InternalQuery(query);
}

std::shared_ptr<DBResult> Database::StoreQuery(const std::string& query)
{
    return InternalSelectQuery(query);
}

std::shared_ptr<DBResult> Database::VerifyResult(std::shared_ptr<DBResult> result)
{
    if (!result->Next())
        return {};
    return result;
}

void Database::FreeResult(DBResult*)
{
    LOG_ERROR << "No database driver loaded, yet a DBResult was freed.";
}

DBResult::~DBResult() = default;

}
