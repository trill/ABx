/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_PGSQL

#include "Database.h"
#include <libpq-fe.h>

namespace DB {

class DatabasePgsql final : public Database
{
protected:
    PGconn* handle_;
    std::string dns_;
    bool Connect(int numTries = 1);
    bool InternalQuery(const std::string& query) override;
    std::shared_ptr<DBResult> InternalSelectQuery(const std::string& query) override;
public:
    DatabasePgsql();
    ~DatabasePgsql() override;

    bool BeginTransaction() override;
    bool Rollback() override;
    bool Commit() override;

    bool GetParam(DBParam param) override;
    uint64_t GetLastInsertId() override;
    std::string EscapeString(const std::string& s) override;
    std::string EscapeBlob(const char* s, size_t length) override;
    void FreeResult(DBResult* res) override;
    bool CheckConnection() override;
};

class PgsqlResult final : public DBResult
{
    friend class DatabasePgsql;
protected:
    explicit PgsqlResult(PGresult* res);

    int32_t rows_, cursor_;
    PGresult* handle_;
public:
    ~PgsqlResult() override;
    int32_t GetInt(const std::string& col) override;
    uint32_t GetUInt(const std::string& col) override;
    int64_t GetLong(const std::string& col) override;
    uint64_t GetULong(const std::string& col) override;
    time_t GetTime(const std::string& col) override;
    std::string GetString(const std::string& col) override;
    std::string GetStream(const std::string& col) override;
    bool IsNull(const std::string& col) override;

    bool Empty() const override { return cursor_ >= rows_; }
    std::shared_ptr<DBResult> Next() override;
};

}

#endif
