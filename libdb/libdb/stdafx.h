/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#include "targetver.h"

#include <stdio.h>

#include <iostream>
#include <map>
#include <string>
#include <memory>
#include <fstream>
#include <functional>
#include <thread>

#include <AB/CommonConfig.h>
#include <base64.h>
#include <libcommon/DebugConfig.h>

#if !defined(HAVE_MYSQL) && !defined(HAVE_PGSQL) && !defined(HAVE_ODBC) && !defined(HAVE_SQLITE)
#error "Define at least one database driver"
#endif
