/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/DataClient.h>
#include <libcommon/MessageClient.h>
#include <libcommon/ServerApp.h>
#include <libcommon/Service.h>

constexpr int64_t QUEUE_UPDATE_INTERVAL_MS = 1000;

class Application final : public ServerApp
{
private:
    asio::io_service ioService_;
    int64_t lastUpdate_{ 0 };
    bool LoadMain();
    void PrintServerInfo();
    void ShowLogo();
    void UpdateQueue();
    void MainLoop();
    void HandleMessage(const Net::MessageMsg& msg);
    void HandleQueueAdd(const Net::MessageMsg& msg);
    void HandleQueueRemove(const Net::MessageMsg& msg);
protected:
    void ShowVersion() override;
public:
    Application();
    ~Application() override;

    bool Initialize(const std::vector<std::string>& args) override;
    void Run() override;
    void Stop() override;
};

