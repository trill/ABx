/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include <mutex>
#include "Queue.h"

class MatchQueues
{
private:
    ea::map<std::string, ea::unique_ptr<Queue>> queues_;
    /// Player -> Map to find the queue when the players wants to be removed from it
    ea::unordered_map<std::string, std::string> players_;
    std::mutex lock_;
public:
    void Add(const std::string& mapUuid, const std::string& playerUuid);
    void Remove(const std::string& playerUuid);
    Queue* GetQueue(const std::string& mapUuid);
    void Update(uint32_t timeElapsed);
};
