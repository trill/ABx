/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "GameObject.h"
#include <libmath/MathDefs.h>
#include <kaguya/kaguya.hpp>
#include <libclient/Client.h>

class Player final : public GameObject
{
private:
    Client::Client& client_;
    kaguya::State luaState_;
    bool firstUpdate_{ true };
    void LoadScript(const std::string& script);
    void SelectObject(GameObject* object);
    void Goto(const Math::StdVector3& pos);
    void Move(unsigned direction);
    void Turn(unsigned direction);
    void SetDirection(float deg);
    void Say(unsigned channel, const std::string& message);
    void Command(unsigned type, const std::string& data);
    void Cancel();
    void Interact();
    void ChangeMap(const std::string& mapUuid);
    void ClickObject(GameObject* object);
    void LoadSkills(const std::string& templ);
    void UseSkill(int pos);

    void OnStateChanged(unsigned state) override;
    void OnDamaged(GameObject* source) override;
    void OnHealed(GameObject* source) override;
    void OnRecourceChanged(uint8_t type, uint16_t value) override;
    void ObjectSpawn(GameObject* object) override;
    void ObjectDespawn(GameObject* object) override;

public:
    static void RegisterLua(kaguya::State& state);
    Player(Type type, uint32_t id, Client::Client& client, const std::string& script);
    void Update(uint32_t timeElapsed) override;
};
