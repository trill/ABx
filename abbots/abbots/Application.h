/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/ServerApp.h>
#include <vector>
#include <memory>

class BotClient;

struct Account
{
    std::string name;
    std::string pass;
    std::string character;
    std::string script;
};

class Application final : public ServerApp
{
private:
    std::shared_ptr<asio::io_service> ioService_;
    std::string loginHost_;
    uint16_t loginPort_{ 0 };
    std::unique_ptr<BotClient> client_;
    std::vector<Account> accounts_;
    int64_t lastUpdate_{ 0 };
    bool LoadMain();
    void ShowVersion() override;
    void ShowLogo();
    void Update();
    void MainLoop();
    void CreateBots();
    void StartBot();
    void Shutdown();
protected:
    bool ParseCommandLine() override;
public:
    Application();
    ~Application() override;

    bool Initialize(const std::vector<std::string>& args) override;
    void Run() override;
    void Stop() override;
};
