/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/Transformation.h>
#include <kaguya/kaguya.hpp>
#include <sa/PropStream.h>

class Game;

class GameObject
{
private:
    Game* game_{ nullptr };
public:
    enum class Type
    {
        ItemDrop,
        AOE,
        Projectile,
        Npc,
        Player,
        Self,
    };
    static void RegisterLua(kaguya::State& state);

    GameObject(Type type, uint32_t id);

    virtual void Update(uint32_t timeElapsed);
    Game* GetGame() const;
    unsigned GetState() const { return state_; }
    void SetGame(Game* game);
    void SetData(sa::PropReadStream& data);

    virtual void OnStateChanged(unsigned);
    virtual void OnDamaged(GameObject*) { }
    virtual void OnHealed(GameObject*) { }
    virtual void OnRecourceChanged(uint8_t, uint16_t) { }
    virtual void ObjectSpawn(GameObject*) { }
    virtual void ObjectDespawn(GameObject*) { }

    Type type_;
    uint32_t id_{ 0 };
    Math::Transformation transformation_;
    uint32_t level_{ 0 };
    bool pvpCharacter_{ false };
    uint32_t itemIndex_{ 0 };
    std::string name_;
    unsigned state_{ 0 };
};
