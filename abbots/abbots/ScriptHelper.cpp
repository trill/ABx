/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ScriptHelper.h"
#include "Game.h"
#include "GameObject.h"
#include "Player.h"
#include <libcommon/Subsystems.h>
#include <libcommon/SimpleConfigManager.h>
#include <libcommon/Logger.h>
#include <libcommon/StringUtils.h>
#include <libcommon/FileUtils.h>
#include <sa/StringTempl.h>
#include <sa/time.h>
#include <libcommon/Random.h>

static void LuaErrorHandler(int errCode, const char* message)
{
    LOG_ERROR << "Lua Error (" << errCode << "): " << message << std::endl;
}

void InitLuaState(kaguya::State& state)
{
    state.setErrorHandler(LuaErrorHandler);
    state["Tick"] = kaguya::function([]
    {
        return sa::time::tick();
    });
    state["Random"] = kaguya::overload(
        [] { return GetSubsystem<Crypto::Random>()->GetFloat(); },
        [](float max) { return GetSubsystem<Crypto::Random>()->Get<float>(0.0f, max); },
        [](float min, float max) { return GetSubsystem<Crypto::Random>()->Get<float>(min, max); }
    );
    state["include"] = kaguya::function([&state](const std::string& file)
    {
        auto script = GetDataFile(file);
        if (Utils::FileExists(script))
        {
            // Make something like an include guard
            std::string ident(file);
            sa::MakeIdent(ident);
            ident = "__included_" + ident + "__";
            if (IsBool(state, ident))
                return;
            if (state.dofile(script.c_str()))
                state[ident] = true;
        }
    });
    Game::RegisterLua(state);
    GameObject::RegisterLua(state);
    Player::RegisterLua(state);
}

std::string GetDataDir()
{
    auto* cfg = GetSubsystem<IO::SimpleConfigManager>();
    return cfg->GetGlobalString("data_dir", "");
}

std::string GetDataFile(const std::string& name)
{
    return Utils::ConcatPath(GetDataDir(), name);
}
