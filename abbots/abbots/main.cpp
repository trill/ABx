/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Application.h"
#include <libcommon/ServiceConfig.h>
#include <csignal>
#include <chillout.h>
#include <sa/path.h>
#include <libcommon/FileUtils.h>

#if defined(_MSC_VER) && defined(_DEBUG)
#   define CRTDBG_MAP_ALLOC
#   include <stdlib.h>
#   include <crtdbg.h>
#endif

namespace {
std::function<void(int)> shutdown_handler;
void signal_handler(int signal)
{
    shutdown_handler(signal);
}
} // namespace

#ifdef AB_WINDOWS
static std::mutex gTermLock;
static std::condition_variable termSignal;
#endif

int main(int argc, char** argv)
{
#if defined(_MSC_VER) && defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    sa::path dumpsPath = sa::path(Utils::GetExeName()).directory() / sa::path("crash");
    ASSERT(Utils::EnsureDirectory(dumpsPath.string()));
    auto &chillout = Debug::Chillout::getInstance();
    chillout.init("abbots", dumpsPath.string());
    chillout.setBacktraceCallback([](const char * const stackEntry)
    {
        fprintf(stderr, "  %s\n", stackEntry);
    });

    chillout.setCrashCallback([&chillout](const char * const message)
    {
        LOG_ERROR << message << std::endl;
        chillout.backtrace();
#ifdef AB_WINDOWS
        chillout.createCrashDump();
#endif
    });

    std::signal(SIGINT, signal_handler);              // Ctrl+C
    std::signal(SIGTERM, signal_handler);
#ifdef AB_WINDOWS
    std::signal(SIGBREAK, signal_handler);            // X clicked
#endif

    {
        Application app;
        if (!app.InitializeA(argc, argv))
            return EXIT_FAILURE;

        shutdown_handler = [&app](int /*signal*/)
        {
#ifdef AB_WINDOWS
            std::unique_lock<std::mutex> lockUnique(gTermLock);
#endif
            app.Stop();
#ifdef AB_WINDOWS
            termSignal.wait(lockUnique);
#endif
        };

        app.Run();
    }

#ifdef AB_WINDOWS
    termSignal.notify_all();
#endif

    return EXIT_SUCCESS;
}
