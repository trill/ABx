/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <memory>
#include <AB/Entities/Game.h>
#include <CleanupNs.h>

class GameObject;

class Game
{
private:
    AB::Entities::GameType type_;
    std::map<uint32_t, std::unique_ptr<GameObject>> objects_;
public:
    static void RegisterLua(kaguya::State& state);
    Game(AB::Entities::GameType type);

    void Update(uint32_t timeElapsed);
    void AddObject(std::unique_ptr<GameObject>&& object);
    void RemoveObject(uint32_t id);
    GameObject* GetObject(uint32_t id);
    GameObject* GetObjectByName(const std::string& name);
};

