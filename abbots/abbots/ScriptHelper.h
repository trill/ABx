/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <kaguya/kaguya.hpp>
#include <string>

void InitLuaState(kaguya::State& state);
std::string GetDataDir();
std::string GetDataFile(const std::string& name);

inline bool IsFunction(kaguya::State& state, const std::string& name)
{
    return state[name].type() == LUA_TFUNCTION;
}

inline bool IsVariable(kaguya::State& state, const std::string& name)
{
    auto t = state[name].type();
    return t == LUA_TBOOLEAN || t == LUA_TNUMBER || t == LUA_TSTRING;
}

inline bool IsString(kaguya::State& state, const std::string& name)
{
    return state[name].type() == LUA_TSTRING;
}

inline bool IsBool(kaguya::State& state, const std::string& name)
{
    return state[name].type() == LUA_TBOOLEAN;
}

inline bool IsNumber(kaguya::State& state, const std::string& name)
{
    return state[name].type() == LUA_TNUMBER;
}

inline bool IsNil(kaguya::State& state, const std::string& name)
{
    return state[name].type() == LUA_TNIL;
}
