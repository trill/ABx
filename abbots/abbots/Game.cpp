/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Game.h"
#include "GameObject.h"
#include <sa/Assert.h>
#include <algorithm>

void Game::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Game"].setClass(std::move(kaguya::UserdataMetatable<Game>()
        .addFunction("GetObject", &Game::GetObject)
        .addFunction("GetObjectByName", &Game::GetObjectByName)
    ));
    // clang-format on
}

Game::Game(AB::Entities::GameType type) :
    type_(type)
{
}

void Game::Update(uint32_t timeElapsed)
{
    for (const auto& object : objects_)
    {
        object.second->Update(timeElapsed);
    }
}

void Game::AddObject(std::unique_ptr<GameObject>&& object)
{
    ASSERT(object);
    uint32_t id = object->id_;
    object->SetGame(this);
    auto* obj = object.get();

    objects_.emplace(id, std::move(object));
    for (const auto& o : objects_)
        o.second->ObjectSpawn(obj);
}

void Game::RemoveObject(uint32_t id)
{
    auto* object = GetObject(id);
    if (!object)
        return;
    for (const auto& o : objects_)
        o.second->ObjectDespawn(object);

    object->SetGame(nullptr);
    auto it = objects_.find(id);
    if (it != objects_.end())
        objects_.erase(it);
}

GameObject* Game::GetObject(uint32_t id)
{
    const auto it = objects_.find(id);
    if (it != objects_.end())
        return (*it).second.get();
    return nullptr;
}

GameObject* Game::GetObjectByName(const std::string& name)
{
    const auto it = std::find_if(objects_.begin(), objects_.end(), [&name](const auto& current) {
        return current.second->name_.compare(name) == 0;
    });
    if (it != objects_.end())
        return (*it).second.get();
    return nullptr;
}
