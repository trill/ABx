/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "utf8.h"
#include <sstream>
#include "unicode.h"

namespace utf8 {

static uint32_t Mask8(uint8_t value)
{
    return value & 0xFFu;
}

static bool IsValid(uint32_t value)
{
    return (value & 0xC0u) == 0x80;
}

static uint32_t NextByte(std::string_view::const_iterator& begin, const std::string_view::const_iterator& end)
{
    if (begin != end)
        return Mask8(*(++begin));
    return 0;
}

static void Insert(std::vector<uint32_t>& out, uint32_t replace, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        out.push_back(replace);
}

std::vector<uint32_t> Decode(std::string_view input, uint32_t replace)
{
    std::vector<uint32_t> result;
    result.reserve(input.length());
    for (std::string_view::const_iterator it = input.begin(); it != input.end(); ++it)
    {
        uint32_t code1 = Mask8(*it);
        if (code1 < 0x80)
            result.push_back(code1);
        else if (code1 < 0xC2)
            result.push_back(replace);
        else if (code1 < 0xE0)
        {
            uint32_t code2 = NextByte(it, input.end());
            if (!IsValid(code2))
            {
                Insert(result, replace, 2);
                continue;
            }

            result.push_back((code1 << 6u) + code2 - 0x3080);
        }
        else if (code1 < 0xF0)
        {
            uint32_t code2 = NextByte(it, input.end());
            if (!IsValid(code2)
                || (code1 == 0xE0 && code2 < 0xA0))
            {
                Insert(result, replace, 3);
                continue;
            }

            uint32_t code3 = NextByte(it, input.end());
            if (!IsValid(code3))
            {
                Insert(result, replace, 3);
                continue;
            }
            result.push_back((code1 << 12u) + (code2 << 6u) + code3 - 0xE2080);
        }
        else if (code1 < 0xF5)
        {
            uint32_t code2 = NextByte(it, input.end());
            if (!IsValid(code2)
                || (code1 == 0xF0 && code2 < 0x90)
                || (code1 == 0xF4 && code2 >= 0x90))
            {
                Insert(result, replace, 2);
                continue;
            }

            uint32_t code3 = NextByte(it, input.end());
            if (!IsValid(code3))
            {
                Insert(result, replace, 3);
                continue;
            }

            uint32_t code4 = NextByte(it, input.end());
            if (!IsValid(code4))
            {
                Insert(result, replace, 4);
                continue;
            }

            result.push_back((code1 << 18u)
                + (code2 << 12u)
                + (code1 << 6u)
                + code1 - 0x3C82080);
        }
        else
            Insert(result, replace, 1);
    }
    return result;
}

std::string Encode(const std::vector<uint32_t>& codepoints)
{
    std::stringstream ss;
    for (const auto& cp : codepoints)
    {
        if (cp < 0x80)
            ss << (char)cp;
        else if (cp <= 0x7FF)
        {
            ss << (char)((cp >> 6u) + 0xC0);
            ss << (char)((cp & 0x3Fu) + 0x80);
        }
        else if (cp <= 0xFFFF)
        {
            ss << (char)((cp >> 12u) + 0xE0);
            ss << (char)(((cp >> 6u) & 0x3Fu) + 0x80);
            ss << (char)((cp & 0x3Fu) + 0x80);
        }
        else if (cp <= 0x10FFFF)
        {
            ss << (char)((cp >> 18u) + 0xF0);
            ss << (char)(((cp >> 12u) & 0x3Fu) + 0x80);
            ss << (char)(((cp >> 6u) & 0x3Fu) + 0x80);
            ss << (char)((cp & 0x3Fu) + 0x80);
        }
        // Else invalid
    }
    return ss.str();
}

std::string ToLower(const std::string& str)
{
    auto cps = Decode(str);
    for (auto& c : cps)
        c = ToLowerCodepoint(c);
    return Encode(cps);
}

std::string ToUpper(const std::string& str)
{
    auto cps = Decode(str);
    for (auto& c : cps)
        c = ToUpperCodepoint(c);
    return Encode(cps);
}

size_t Length(const std::string& str)
{
    return Decode(str).size();
}

int Compare(const std::string& s1, const std::string& s2)
{
    auto cp1 = Decode(s1);
    auto cp2 = Decode(s2);
    size_t lim = std::min(cp1.size(), cp2.size());
    size_t k = 0;
    while (k < lim)
    {
        uint32_t c1 = cp1[k];
        uint32_t c2 = cp2[k];
        if (c1 != c2)
            return c1 - c2;
        ++k;
    }
    return cp1.size() - cp2.size();
}

}
