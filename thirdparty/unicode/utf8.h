/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <string_view>
#include <vector>
#include <cstdint>

namespace utf8 {

std::vector<uint32_t> Decode(std::string_view input, uint32_t replace = 0xFFFD);
std::string Encode(const std::vector<uint32_t>& codepoints);
std::string ToLower(const std::string& str);
std::string ToUpper(const std::string& str);
size_t Length(const std::string& str);
int Compare(const std::string& s1, const std::string& s2);

}
