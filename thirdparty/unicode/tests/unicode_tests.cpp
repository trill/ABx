/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <unicode.h>
#include <utf8.h>

TEST_CASE("utf8_to_upper german", "[std][unicode]")
{
    const std::string in = "ÄÖ?öüß";
    auto cps = utf8::Decode(in);
    for (auto& c : cps)
        c = utf8::ToUpperCodepoint(c);
    auto upper = utf8::Encode(cps);
    REQUIRE(upper == "ÄÖ?ÖÜß");
    auto upper2 = utf8::ToUpper(in);
    REQUIRE(upper == upper2);
}

TEST_CASE("utf8_to_lower german", "[std][unicode]")
{
    const std::string in = "ÄÖ?öüß";

    auto cps = utf8::Decode(in);
    for (auto& c : cps)
        c = utf8::ToLowerCodepoint(c);
    auto lower = utf8::Encode(cps);
    REQUIRE(lower == "äö?öüß");
    auto lower2 = utf8::ToLower(in);
    REQUIRE(lower == lower2);
}

TEST_CASE("utf8_to_upper greek", "[std][unicode]")
{
    auto cps = utf8::Decode("άέήβ");
    for (auto& c : cps)
        c = utf8::ToUpperCodepoint(c);
    auto lower = utf8::Encode(cps);
    REQUIRE(lower == "ΆΈΉΒ");
}

TEST_CASE("utf8_to_lower greek", "[std][unicode]")
{
    auto cps = utf8::Decode("ΆΈΉΒ");
    for (auto& c : cps)
        c = utf8::ToLowerCodepoint(c);
    auto lower = utf8::Encode(cps);
    REQUIRE(lower == "άέήβ");
}

TEST_CASE("lookup last", "[std][unicode]")
{
    auto l = utf8::ToLowerCodepoint(125217u);
    REQUIRE(l == 125251u);
    auto u = utf8::ToUpperCodepoint(125251u);
    REQUIRE(u == 125217u);
}

TEST_CASE("lookup first", "[std][unicode]")
{
    auto l = utf8::ToLowerCodepoint(65u);
    REQUIRE(l == 97u);
    auto u = utf8::ToUpperCodepoint(97u);
    REQUIRE(u == 65u);
}

TEST_CASE("lookup last invalid", "[std][unicode]")
{
    auto l = utf8::ToLowerCodepoint(125218u);
    REQUIRE(l == 125218u);
    auto u = utf8::ToUpperCodepoint(125252u);
    REQUIRE(u == 125252u);
}

TEST_CASE("lookup first invalid", "[std][unicode]")
{
    auto l = utf8::ToLowerCodepoint(64u);
    REQUIRE(l == 64u);
    auto u = utf8::ToUpperCodepoint(96u);
    REQUIRE(u == 96u);
}

TEST_CASE("UTF-8 Decode", "[unicode]")
{
    auto cp = utf8::Decode("ABC");
    REQUIRE(cp.size() == 3);
    REQUIRE(cp[0] == 'A');
    REQUIRE(cp[1] == 'B');
    REQUIRE(cp[2] == 'C');
}

TEST_CASE("UTF-8 Encode", "[unicode]")
{
    std::vector<uint32_t> cp;
    cp.push_back('A');
    cp.push_back('B');
    cp.push_back('C');
    auto s = utf8::Encode(cp);
    REQUIRE(s == "ABC");
}
