/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "unicode_data.h"
#include <string>

namespace utf8 {

namespace details {

template<std::size_t N>
constexpr uint32_t Lookup(const std::array<std::tuple<uint32_t, uint32_t>, N>& arr, uint32_t cp)
{
    // Still O(log n) like a std::map
    std::size_t first = 0;
    std::size_t last = N - 1;
    while (first <= last)
    {
        std::size_t current = (first + last) >> 1u;
        int compare = std::get<0>(arr[current]) - cp;
        if (compare == 0)
            return std::get<1>(arr[current]);
        if (compare < 0 && current < N)
            first = current + 1;
        else if (compare > 0 && last > current)
            last = current - 1;
        else
            return cp;
    }
    // Not found
    return cp;
}

}

constexpr uint32_t ToUpperCodepoint(uint32_t cp)
{
    return details::Lookup(details::UPPER, cp);
}

constexpr uint32_t ToLowerCodepoint(uint32_t cp)
{
    return details::Lookup(details::LOWER, cp);
}

}
