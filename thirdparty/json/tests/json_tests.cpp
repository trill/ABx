/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <json.h>

TEST_CASE("JSON construct initialize_list", "[json]")
{
    sa::json::JSON docSyncSave = { { "includeText", false } };
    REQUIRE(docSyncSave.Size() == 1);
    REQUIRE(docSyncSave.HasKey("includeText"));
    REQUIRE((bool)docSyncSave["includeText"] == false);
}

TEST_CASE("JSON construct", "[json]")
{
    sa::json::JSON docSyncSave = sa::json::Object();
    docSyncSave["includeText"] = false;
    REQUIRE(docSyncSave.Size() == 1);
    REQUIRE(docSyncSave.HasKey("includeText"));
    REQUIRE((bool)docSyncSave["includeText"] == false);
}

TEST_CASE("JSON parse", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "id" : 1,
  "params" : {
    "processId" : 266263,
    "workspaceFolders" : []
  }
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(result.Size() == 2);
}

TEST_CASE("JSON parse Unicode", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "key16" : "\u03B2",
  "key32" : "\U0001FA00"
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(result.Size() == 2);
    REQUIRE(result.HasKey("key16"));
    REQUIRE(result.HasKey("key32"));
    REQUIRE((std::string)result["key16"] == "\u03B2");
    REQUIRE((std::string)result["key32"] == "🨀");
}
