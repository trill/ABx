/**
 * Copyright (c) 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cmath>
#include <exception>
#include <functional>
#include <optional>
#include <initializer_list>
#include <limits>
#include <map>
#include <memory>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>
#include <utf8.h>

#if !defined(SA_JSON_FLOATING_POINT_TYPE)
#define SA_JSON_FLOATING_POINT_TYPE double
#endif
#if !defined(SA_JSON_INTEGER_TYPE)
#define SA_JSON_INTEGER_TYPE long long
#endif

namespace sa::json {

namespace {

inline std::string EscapeString(const std::string& str)
{
    std::stringstream ss;
    for (unsigned i = 0; i < str.length(); ++i)
    {
        switch (str[i])
        {
        case '\"':
            ss << "\\\"";
            break;
        case '\\':
            ss << "\\\\";
            break;
        case '\b':
            ss << "\\b";
            break;
        case '\f':
            ss << "\\f";
            break;
        case '\n':
            ss << "\\n";
            break;
        case '\r':
            ss << "\\r";
            break;
        case '\t':
            ss << "\\t";
            break;
        default:
            ss << str[i];
            break;
        }
    }
    return ss.str();
}

template<typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

template<typename T>
inline std::optional<T> ToNumber(const std::string& number)
{
    T result;
    std::istringstream iss(number);
    iss.imbue(std::locale::classic());
    iss >> result;
    if (iss.fail())
        return {};
    return result;
}

}

class JSON;

using ObjectType = std::map<std::string, JSON>;
using ArrayType = std::vector<JSON>;
using Float = SA_JSON_FLOATING_POINT_TYPE;
using Integer = SA_JSON_INTEGER_TYPE;

template<typename T>
concept IsInteger = std::is_integral_v<T> && !std::is_same_v<T, bool>;
template<typename T>
concept IsUnsigned = IsInteger<T> && std::is_unsigned_v<T>;

class JSON
{
private:
    using ObjectPtr = std::unique_ptr<ObjectType>;
    using ArrayPtr = std::unique_ptr<ArrayType>;
    using DataType = std::variant<std::nullptr_t, bool, Integer, Float, std::string, ObjectPtr, ArrayPtr>;
    DataType data_{ nullptr };

public:
    enum class Type
    {
        Null,
        Bool,
        Int,
        Float,
        String,
        Object,
        Array
    };
    static JSON Make(Type type)
    {
        switch (type)
        {
        case Type::Null:
            return nullptr;
        case Type::Bool:
            return false;
        case Type::Int:
            return static_cast<Integer>(0);
        case Type::Float:
            return static_cast<Float>(0.0);
        case Type::String:
            return std::string("");
        case Type::Object:
            return std::make_unique<ObjectType>();
        case Type::Array:
            return std::make_unique<ArrayType>();
        }
        return {};
    }
    constexpr JSON() noexcept = default;
    constexpr JSON(JSON&& value) noexcept
    {
        // Somehow GCC doesn't like the default move ctor.
        std::swap(data_, value.data_);
    }
    JSON(const JSON& other)
    {
        switch (other.GetType())
        {
        case Type::Null:
            break;
        case Type::Bool:
            data_ = std::get<bool>(other.data_);
            break;
        case Type::Int:
            data_ = std::get<Integer>(other.data_);
            break;
        case Type::Float:
            data_ = std::get<Float>(other.data_);
            break;
        case Type::String:
            data_ = std::get<std::string>(other.data_);
            break;
        case Type::Object:
        {
            auto obj = std::make_unique<ObjectType>();
            const auto& otherObj = *std::get<ObjectPtr>(other.data_);
            for (const auto& ti : otherObj)
                obj->emplace(ti.first, ti.second);
            data_ = std::move(obj);
            break;
        }
        case Type::Array:
        {
            auto obj = std::make_unique<ArrayType>();
            const auto& otherArr = *std::get<ArrayPtr>(other.data_);
            obj->reserve(otherArr.size());
            for (const auto& ti : otherArr)
                obj->push_back(ti);
            data_ = std::move(obj);
            break;
        }
        }
    }

    template<typename T>
    JSON(T) // NOLINT(hicpp-explicit-conversions)
    requires std::is_same_v<T, std::nullptr_t>
    {
    }
    template<typename T>
    JSON(T value) // NOLINT(hicpp-explicit-conversions)
    requires std::is_same_v<T, bool> : data_(value)
    {
    }
    template<typename T>
    JSON(T value) // NOLINT(hicpp-explicit-conversions)
    requires IsInteger<T> : data_(value)
    {
    }
    template<typename T>
    JSON(T value) // NOLINT(hicpp-explicit-conversions)
    requires std::is_floating_point_v<T> : data_(value)
    {
    }
    template<typename T>
    JSON(T value) // NOLINT(hicpp-explicit-conversions)
    requires std::is_same_v<T, std::string> : data_(std::move(value))
    {
    }
    template<typename T>
    JSON(T&& value) // NOLINT(hicpp-explicit-conversions)
    requires std::is_same_v<T, ObjectPtr> : data_(std::move(value))
    {
    }
    template<typename T>
    JSON(T&& value) // NOLINT(hicpp-explicit-conversions)
    requires std::is_same_v<T, ArrayPtr> : data_(std::move(value))
    {
    }

    JSON(std::initializer_list<std::tuple<std::string, JSON>> init)
    {
        data_ = std::make_unique<ObjectType>();
        std::for_each(init.begin(),
            init.end(),
            [this](const auto& current)
            { std::get<ObjectPtr>(data_)->emplace(std::move(std::get<0>(current)), std::move(std::get<1>(current))); });
    }
    JSON(std::initializer_list<JSON> init)
    {
        data_ = std::make_unique<ArrayType>();
        std::for_each(
            init.begin(), init.end(), [this](const auto& current)
            { std::get<ArrayPtr>(data_)->push_back(std::move(current)); });
    }

    ~JSON() = default;

    JSON& operator=(JSON&&) = default;
    JSON& operator=(const JSON& other)
    {
        *this = JSON(other);
        return *this;
    }

    JSON& operator[](const std::string& key)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    JSON& operator[](const char* key)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    template<typename T>
    JSON& operator[](T index)
        requires IsUnsigned<T>
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        if (std::get<ArrayPtr>(data_)->size() <= static_cast<size_t>(index))
            std::get<ArrayPtr>(data_)->resize(static_cast<size_t>(index) + 1);
        return std::get<ArrayPtr>(data_)->operator[](static_cast<size_t>(index));
    }
    const JSON& operator[](const std::string& key) const
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    const JSON& operator[](const char* key) const
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    template<typename T>
    const JSON& operator[](T index) const
        requires IsUnsigned<T>
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return std::get<ArrayPtr>(data_)->operator[](static_cast<size_t>(index));
    }

    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, bool>
    {
        data_ = value;
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires IsInteger<T>
    {
        data_ = (Integer)value;
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_floating_point_v<T>
    {
        data_ = (Float)value;
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_convertible_v<T, std::string>
    {
        data_ = std::string(value);
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, ObjectPtr>
    {
        data_ = std::move(value);
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, ArrayPtr>
    {
        data_ = std::move(value);
        return *this;
    }

    [[nodiscard]] Type GetType() const
    {
        if (std::holds_alternative<bool>(data_))
            return Type::Bool;
        if (std::holds_alternative<Integer>(data_))
            return Type::Int;
        if (std::holds_alternative<Float>(data_))
            return Type::Float;
        if (std::holds_alternative<std::string>(data_))
            return Type::String;
        if (std::holds_alternative<ObjectPtr>(data_))
            return Type::Object;
        if (std::holds_alternative<ArrayPtr>(data_))
            return Type::Array;
        return Type::Null;
    }

    [[nodiscard]] bool IsNaN() const
    {
        if (GetType() == Type::Float)
            return std::isnan(std::get<Float>(data_));
        return false;
    }
    [[nodiscard]] bool IsInf() const
    {
        if (GetType() == Type::Float)
            return Equals(std::get<Float>(data_), std::numeric_limits<Float>::infinity());
        return false;
    }
    [[nodiscard]] bool IsNegInf() const
    {
        if (GetType() == Type::Float)
        {
            if constexpr (std::numeric_limits<Float>::is_iec559)
                return Equals(std::get<Float>(data_), -std::numeric_limits<Float>::infinity());
            else
                return false;
        }
        return false;
    }

    template<typename T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, bool>
    {
        if (GetType() != Type::Bool)
            throw std::runtime_error("Not a bool");
        return std::get<bool>(data_);
    }
    template<typename T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
        requires IsInteger<T>
    {
        if (GetType() != Type::Int)
            throw std::runtime_error("Not an int");
        return std::get<Integer>(data_);
    }
    template<typename T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
        requires std::is_floating_point_v<T>
    {
        if (GetType() != Type::Float)
            throw std::runtime_error("Not a float");
        return std::get<Float>(data_);
    }
    operator const ObjectType&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return *std::get<ObjectPtr>(data_);
    }
    operator const ArrayType&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return *std::get<ArrayPtr>(data_);
    }
    operator const std::string&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::String)
            throw std::runtime_error("Not a string " + std::to_string((int)GetType()));
        return std::get<std::string>(data_);
    }
    operator ObjectType&() // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return *std::get<ObjectPtr>(data_);
    }
    operator ArrayType&() // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return *std::get<ArrayPtr>(data_);
    }

    [[nodiscard]] size_t Size() const
    {
        switch (GetType())
        {
        case Type::Array:
            return std::get<ArrayPtr>(data_)->size();
        case Type::Object:
            return std::get<ObjectPtr>(data_)->size();
        default:
            return 0;
        }
    }

    [[nodiscard]] bool Empty() const
    {
        switch (GetType())
        {
        case Type::Null:
            return true;
        case Type::Array:
            return std::get<ArrayPtr>(data_)->empty();
        case Type::Object:
            return std::get<ObjectPtr>(data_)->empty();
        default:
            return true;
        }
    }

    [[nodiscard]] bool HasKey(const std::string& key) const
    {
        if (GetType() == Type::Object)
            return std::get<ObjectPtr>(data_)->find(key) != std::get<ObjectPtr>(data_)->end();
        return false;
    }

    template<typename T>
    void Append(T value)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        std::get<ArrayPtr>(data_)->push_back(value);
    }
    [[nodiscard]] std::string Dump(bool minify = false) const
    {
        return InternalDump(minify, 1, 0);
    }

    friend std::ostream& operator<<(std::ostream&, const JSON&);
private:
    [[nodiscard]] std::string InternalDump(bool minify = false, int depth = 1, int indent = 0) const
    {
        switch (GetType())
        {
        case Type::Null:
            return "null";
        case Type::Bool:
            return std::get<bool>(data_) ? "true" : "false";
        case JSON::Type::Int:
            return std::to_string(std::get<Integer>(data_));
        case JSON::Type::Float:
        {
            if (IsInf())
                return "\"Infinity\"";
            if (IsNegInf())
                return "\"-Infinity\"";
            if (IsNaN())
                return "\"NaN\"";

            std::stringstream ss;
            ss.imbue(std::locale::classic());
            ss << std::get<Float>(data_);
            std::string result = ss.str();
            if (result.find('.') == std::string::npos)
                return result + ".0";
            return result;
        }
        case Type::String:
            return std::string("\"") + EscapeString(std::get<std::string>(data_)) + std::string("\"");
        case Type::Object:
        {
            std::stringstream ss;
            ss << "{";
            if (!minify)
                ss << "\n";
            bool skip = true;
            for (const auto& i : *std::get<ObjectPtr>(data_))
            {
                if (!skip)
                {
                    ss << ",";
                    if (!minify)
                        ss << "\n";
                }
                if (!minify)
                    ss << std::string(indent + 2, ' ');
                ss << "\"" << i.first << "\"";
                if (!minify)
                    ss << " ";
                ss << ":";
                if (!minify)
                    ss << " ";
                ss << i.second.InternalDump(minify, depth + 1, indent + 2);
                skip = false;
            }
            if (!minify)
                ss << "\n" << std::string(indent, ' ');
            ss << "}";
            return ss.str();
        }
        case Type::Array:
        {
            std::stringstream ss;
            ss << "[";
            bool skip = true;
            for (const auto& i : *std::get<ArrayPtr>(data_))
            {
                if (!skip)
                {
                    ss << ",";
                    if (!minify)
                        ss << " ";
                }
                ss << i.InternalDump(minify, depth + 1, indent + 2);
                skip = false;
            }
            ss << "]";
            return ss.str();
        }
        }

        return {};
    }

};

using ErrorFunc = std::function<void(size_t, const std::string&)>;

namespace {

JSON ParseNext(std::string_view, size_t&, const ErrorFunc&);

void SkipWhite(std::string_view str, size_t& offset)
{
    while (isspace(str[offset]))
        ++offset;
}

JSON ParseObject(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    JSON result = JSON::Make(JSON::Type::Object);

    SkipWhite(str, ++offset);

    if (str[offset] == '}')
    {
        ++offset;
        return result;
    }

    while (true)
    {
        JSON key = ParseNext(str, offset, error);

        SkipWhite(str, offset);
        if (str[offset] != ':')
        {
            if (error)
                error(offset, "Error: : expected");
            break;
        }

        SkipWhite(str, ++offset);
        JSON value = ParseNext(str, offset, error);
        result[(std::string)key] = std::move(value);

        SkipWhite(str, offset);
        if (str[offset] == ',')
        {
            ++offset;
            continue;
        }
        if (str[offset] == '}')
        {
            ++offset;
            break;
        }
        if (error)
            error(offset, "Error: , or } expected");
    }

    return result;
}

JSON ParseArray(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    JSON result = JSON::Make(JSON::Type::Array);
    size_t index = 0;
    SkipWhite(str, ++offset);
    if (str[offset] == ']')
    {
        ++offset;
        return result;
    }

    while (true)
    {
        auto value = ParseNext(str, offset, error);
        result[index++] = std::move(value);
        SkipWhite(str, offset);
        if (str[offset] == ',')
        {
            ++offset;
            continue;
        }
        if (str[offset] == ']')
        {
            ++offset;
            break;
        }
        if (error)
            error(offset, "Error: , or ] expected");
    }
    return result;
}

JSON ParseString(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    std::stringstream result;

    auto isHexDigit = [](char c) -> bool
    {
        return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
    };
    auto isOctDigit = [](char c) -> bool
    {
        return c >= '0' && c <= '7';
    };
    auto peek = [&](size_t off) -> char
    {
        if (offset + off >= str.length())
            return '\0';
        return str[offset + off];
    };

    bool escaped = false;
    for (char c = str[++offset]; (c != '\"' || escaped); c = str[++offset])
    {
        if (escaped)
        {
            switch (c)
            {
            case '\'':
                result << '\'';
                break;
            case '\"':
                result << '\"';
                break;
            case '\?':
                result << '\?';
                break;
            case '\\':
                result << '\\';
                break;
            case 'a':
                // Just skip
                break;
            case 'b':
                result << '\b';
                break;
            case 'v':
                result << '\v';
                break;
            case 'f':
                result << '\f';
                break;
            case 'n':
                result << '\n';
                break;
            case 'r':
                result << '\r';
                break;
            case 't':
                result << '\t';
                break;
            case 'x':
            {
                if (isHexDigit(peek(1)))
                {
                    char hex[2] = {};
                    hex[0] = str[++offset];
                    if (isHexDigit(peek(1)))
                        hex[1] = str[++offset];
                    result << (char)std::stoi(hex, nullptr, 16);
                }
                else
                {
                    if (error)
                        error(offset, "Error: Malformed escape sequence");
                }

                break;
            }
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            {
                char hex[3] = {};
                hex[0] = c;
                if (isOctDigit(peek(1)))
                {
                    hex[1] = str[++offset];
                    if (isHexDigit(peek(1)))
                        hex[2] = str[++offset];
                }
                result << (char)std::stoi(hex, nullptr, 8);
                break;
            }
            case 'u':
            {
                if (isHexDigit(peek(1)) && isHexDigit(peek(2)) && isHexDigit(peek(3)) && isHexDigit(peek(4)))
                {
                    char hex[5] = {};
                    for (int i = 0; i < 4; ++i)
                    {
                        hex[i] = str[++offset];
                    }
                    uint32_t value = std::stoi(hex, nullptr, 16);
                    result << utf8::Encode({ value });
                }
                else
                    if (error)
                        error(offset, "Malformed escape sequence, expecting two bytes");
                break;
            }
            case 'U':
            {
                // Same as \u but 4 bytes expected
                if (isHexDigit(peek(1)) && isHexDigit(peek(2)) && isHexDigit(peek(3)) && isHexDigit(peek(4))
                    && isHexDigit(peek(5)) && isHexDigit(peek(6)) && isHexDigit(peek(7)) && isHexDigit(peek(8)))
                {
                    char hex[9] = {};
                    for (int i = 0; i < 8; ++i)
                    {
                        hex[i] = str[++offset];
                    }
                    uint32_t value = std::stol(hex, nullptr, 16);
                    result << utf8::Encode({ value });
                }
                else
                    if (error)
                        error(offset, "Malformed escape sequence, expecting four bytes");
                break;
            }
            default:
                if (error)
                    error(offset, "Error: Malformed escape sequence");
                break;
            }
            escaped = false;
        }
        else if (c == '\\')
            escaped = true;
        else
            result << c;
    }
    ++offset;
    std::string value = result.str();
    if (value == "Infinity")
        return std::numeric_limits<Float>::infinity();
    if (value == "-Infinity")
        return -std::numeric_limits<Float>::infinity();
    if (value == "NaN")
        return std::numeric_limits<Float>::quiet_NaN();
    return value;
}

JSON ParseBool(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    if (str.substr(offset, 4) == "true")
    {
        offset += 4;
        return true;
    }
    if (str.substr(offset, 5) == "false")
    {
        offset += 5;
        return false;
    }
    if (error)
        error(offset, "Error: true or false expected");
    return nullptr;
}

JSON ParseNull(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    if (str.substr(offset, 4) != "null")
    {
        if (error)
            error(offset, "Error: null expected");
        return {};
    }
    offset += 4;
    return nullptr;
}

JSON ParseNumber(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    size_t start = offset;
    char current = str[offset];

    // Not JSON spec compliant to support Hex, Oct, Bin literals...
    enum class NumberType
    {
        Decimal,
        Hex,
        Octal,
        Bin
    } numberType = NumberType::Decimal;

    auto isDigit = [&numberType](char c) -> bool
    {
        switch (numberType)
        {
        case NumberType::Decimal:
            return isdigit(c);
        case NumberType::Hex:
            return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
        case NumberType::Octal:
            return c >= '0' && c <= '7';
        case NumberType::Bin:
            return c == '0' || c == '1';
        default:
            return false;
        }
    };

    if (current == '0')
    {
        current = str[++offset];
        if (current == 'x' || current == 'X')
        {
            current = str[++offset];
            numberType = NumberType::Hex;
        }
        else if (current == 'o' || current == 'O')
        {
            current = str[++offset];
            numberType = NumberType::Octal;
        }
        else if (current == 'b' || current == 'B')
        {
            current = str[++offset];
            numberType = NumberType::Bin;
        }
    }

    while (isDigit(current))
    {
        current = str[++offset];
    }

    if (current == '.')
    {
        if (numberType != NumberType::Decimal)
        {
            if (error)
                error(offset, "Error: Malformed number");
            return static_cast<Integer>(0);
        }
        current = str[++offset];
        while (isdigit(current))
        {
            current = str[++offset];
        }
    }

    if ((current == 'e' || current == 'E'))
    {
        if (numberType != NumberType::Decimal)
        {
            if (error)
                error(offset, "Error: Malformed number");
            return static_cast<Integer>(0);
        }
        current = str[++offset];

        if (current == '+' || current == '-')
        {
            current = str[++offset];
        }

        if (!isdigit(current))
        {
            if (error)
                error(offset, "Error: Expected exponent in floating point constant");
            return static_cast<Integer>(0);
        }

        while (isdigit(current))
        {
            current = str[++offset];
        }
    }

    auto value = std::string_view(&str[start], offset - start);
    std::string strValue = std::string(value);

    if (numberType == NumberType::Hex)
    {
        strValue.erase(0, 2);
        return static_cast<Integer>(std::stoi(strValue, nullptr, 16));
    }
    if (numberType == NumberType::Bin)
    {
        strValue.erase(0, 2);
        return static_cast<Integer>(std::stoi(strValue, nullptr, 2));
    }
    if (numberType == NumberType::Octal)
    {
        strValue.erase(0, 2);
        return static_cast<Integer>(std::stoi(strValue, nullptr, 8));
    }

    const auto v = ToNumber<Float>(strValue);
    if (!v.has_value())
    {
        if (error)
            error(offset, "Error: Malformed number \"" + strValue + "\"");
        return static_cast<Integer>(0);
    }
    if ((strValue.find('.') == std::string::npos) && (std::fmod(v.value(), 1.0) == 0.0))
        // When it doesn't have a fractional part make it an integer
        return static_cast<Integer>(v.value());
    return static_cast<Float>(v.value());
}

JSON ParseNext(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    SkipWhite(str, offset);
    char value = str[offset];
    switch (value)
    {
    case '[':
        return ParseArray(str, offset, error);
    case '{':
        return ParseObject(str, offset, error);
    case '\"':
        return ParseString(str, offset, error);
    case 't':
    case 'f':
        return ParseBool(str, offset, error);
    case 'n':
        return ParseNull(str, offset, error);
    default:
        if ((value >= '0' && value <= '9') || value == '-')
            return ParseNumber(str, offset, error);
    }
    if (error)
        error(offset, "Error: Unexpected character " + std::string(1, value));
    return nullptr;
}

}

inline JSON Parse(std::string_view str, ErrorFunc error = {}) // NOLINT(performance-unnecessary-value-param)
{
    size_t offset = 0;
    return ParseNext(str, offset, error);
}

inline std::ostream& operator<<(std::ostream& os, const JSON& json)
{
    os << json.Dump();
    return os;
}

inline JSON Array()
{
    return JSON::Make(JSON::Type::Array);
}

inline JSON Object()
{
    return JSON::Make(JSON::Type::Object);
}

}
