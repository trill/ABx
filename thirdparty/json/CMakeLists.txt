project (json CXX)

file(GLOB SOURCES *.h)

add_library(json INTERFACE ${SOURCES})
target_include_directories(json INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(json INTERFACE unicode)
if(BUILD_TESTING)
    add_subdirectory(tests)
endif()
