/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

namespace AB {

enum Dialogs
{
    DialogUnknown = 0,
    DialogAccountChest,
    DialogMerchantItems,
    DialogCraftsmanItems,
};

}
