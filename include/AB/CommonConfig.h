/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Compiler.h>

#if defined(SA_PLATFORM_WIN)
#   define AB_WINDOWS
#elif defined(SA_PLATFORM_LINUX) || defined(SA_PLATFORM_UNIX)
#   define AB_UNIX
#endif

#if !defined(AB_WINDOWS) && !defined(AB_UNIX)
#error Unsupported platform
#endif

// Configurations shared by the server and client
#define AB_VERSION_CREATE(major, minor) (((major) << 8) | (minor))
#define AB_VERSION_GET_MAJOR(version) (((version) >> 8) & 0xFF)
#define AB_VERSION_GET_MINOR(version) ((version) & 0xFF)

#define CURRENT_YEAR 2022
#define AB_AUTHOR "Stefan Ascher"

#define  UUID_LENGTH 36
// If Email is mandatory when creating an account uncomment bellow
//#define EMAIL_MANDATORY
#define CHARACTER_NAME_NIM   6
#define CHARACTER_NAME_MAX  20
#define ACCOUNT_NAME_MIN     6
#define ACCOUNT_NAME_MAX    32
#define PASSWORD_LENGTH_MIN  6
#define PASSWORD_LENGTH_MAX 61
#define EMAIL_LENGTH_MAX    60

// If defined disable nagle's algorithm, this make the game play smoother
// acceptor_.set_option(asio::ip::tcp::no_delay(true));
#define TCP_OPTION_NODELAY
#ifdef AB_WINDOWS
#define TCP_REUSE_ADDRESS false
#else
// On Linux it says sometimes the address is still in use, although the program was closed
#define TCP_REUSE_ADDRESS true
#endif

inline constexpr auto RESTRICTED_NAME_CHARS = R"(1234567890<>^!"$%&/()[]{}=?\`´,.-;:_+*~#'|)";

// As long as we dont have a trusted cert for the server, do not verify SSL
#define HTTP_CLIENT_VERIFY_SSL false

namespace Auth {
// Auth token expires in 1 hr of inactivity
inline constexpr long long AUTH_TOKEN_EXPIRES_IN = 1000 * 60 * 60;
// When a user deletes a character, the characters name is reserved for 1 week
inline constexpr int NAME_RESERVATION_EXPIRES_MS = 1000 * 60 * 60 * 24 * 7;
}
