/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace AB {
namespace Packets {

namespace Internal {

template <typename _Msg>
class Reader
{
private:
    _Msg& msg_;
public:
    Reader(_Msg& msg) :
        msg_(msg)
    { }
    template<typename T>
    void value(T& value)
    {
        value = msg_.template Get<T>();
    }
    template<typename T>
    void value_enc(T& value)
    {
        value = msg_.template GetDecrypted<T>();
    }
};

template <typename _Msg>
class Writer
{
private:
    _Msg& msg_;
public:
    Writer(_Msg& msg) :
        msg_(msg)
    { }
    template<typename T>
    void value(T& value)
    {
        msg_.template Add<T>(value);
    }
    template<typename T>
    void value_enc(T& value)
    {
        msg_.template AddEncryted<T>(value);
    }
};

// Calculates the required size for this packet
class SizeCalculator
{
private:
    size_t size_{ 0 };
public:
    template<typename T>
    void value(T&)
    {
        size_ += sizeof(T);
    }
    template<typename T>
    void value_enc(T&)
    {
        size_ += sizeof(T);
    }
    size_t GetSize() const { return size_; }
};

template<>
inline void SizeCalculator::value<std::string>(std::string& value)
{
    size_ += sizeof(uint16_t);  // Length
    size_ += value.length();
}

template<>
inline void SizeCalculator::value_enc<std::string>(std::string& value)
{
    size_ += sizeof(uint16_t);  // Length
    size_t len = value.length();
    // Padding bytes
    if ((len % 8) != 0)
    {
        uint16_t n = 8 - (len % 8);
        len += n;
    }
    size_ += len;
}

template <>
inline void SizeCalculator::value<bool>(bool&)
{
    // Using 1 byte to store bool
    ++size_;
}

}

// Read a packet from the message
template<typename T, typename _Msg>
[[nodiscard]] T Get(_Msg& msg)
{
    T result{};
    Internal::Reader<_Msg> reader(msg);
    result.Serialize(reader);
    return result;
}

// Add a packet to the message
template<typename T, typename _Msg>
void Add(T& value, _Msg& msg)
{
    // Can not be const T& value because we are using the same function for reading
    // and writing which is not const
    Internal::Writer<_Msg> writer(msg);
    value.Serialize(writer);
}

// Calculate required size
template<typename T>
[[nodiscard]] size_t GetSize(T& value)
{
    Internal::SizeCalculator calc;
    value.Serialize(calc);
    return calc.GetSize();
}

}
}
