/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace AI {

struct GetGames
{
    template<typename _Ar>
    void Serialize(_Ar&)
    {
    }
};

struct SelectGame
{
    uint32_t gameId;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value(gameId);
    }
};

struct GetTrees
{
    template<typename _Ar>
    void Serialize(_Ar&)
    {
    }
};

}
