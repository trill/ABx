/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

static constexpr int MAX_PARTY_MEMBERS = 12;

struct Party : Entity
{
    MAKE_ENTITY(Party)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(members);
        s.value(npcs);
    }

    // Player UUIDs
    std::vector<std::string> members;
    // NPC scripts
    std::vector<std::string> npcs;
};

}
}
