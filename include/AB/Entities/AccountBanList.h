/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct AccountBanList : Entity
{
    MAKE_ENTITY(AccountBanList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(uuids);
    }

    std::vector<std::string> uuids;
};

}
}
