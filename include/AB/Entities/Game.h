/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum GameType : uint8_t
{
    GameTypeUnknown = 0,
    GameTypeOutpost = 1,
    GameTypeTown,
    GameTypeGuildHall,
    // Bellow this games are exclusive and always created new
    GameTypePvPCombat,
    GameTypeExploreable,
    GameTypeMission,
};

inline bool IsOutpost(GameType type)
{
    return type >= GameTypeOutpost && type <= GameTypeGuildHall;
}

inline bool IsBattle(GameType type)
{
    return type >= GameTypePvPCombat;
}

enum GameModeFlags : uint32_t
{
    GameModeFlagNone = 0,
};

enum GameMode : uint32_t
{
    GameModeUnknown = 0,
};

struct Game : Entity
{
    MAKE_ENTITY(Game)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(name);
        s.value(directory);
        s.value(script);
        s.value(queueMapUuid);
        s.value(type);
        s.value(mode);
        s.value(landing);
        s.value(partySize);
        s.value(partyCount);
        s.value(randomParty);
        s.value(mapCoordX);
        s.value(mapCoordY);
        s.value(defaultLevel);
    }

    /// The name of the game
    std::string name;
    /// Directory with game data
    std::string directory;
    /// Script file
    std::string script;
    std::string queueMapUuid;
    GameType type = GameTypeUnknown;
    GameMode mode = GameModeUnknown;
    bool landing = false;
    uint8_t partySize = 0;
    uint8_t partyCount = 0;
    bool randomParty = false;
    int32_t mapCoordX = 0;
    int32_t mapCoordY = 0;
    int8_t defaultLevel = 1;
};

}
}
