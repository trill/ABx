/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/Version.h>

namespace AB {
namespace Entities {

struct VersionList : Entity
{
    MAKE_ENTITY(VersionList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(versions);
    }

    std::vector<Version> versions;
};

}
}
