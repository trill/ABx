/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

// Item that belongs to the merchant. UUID is the Item UUID.
struct MerchantItem : Entity
{
    MAKE_ENTITY(MerchantItem)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(concreteUuid);
    }

    std::string concreteUuid;
};

}
}
