/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <vector>

namespace AB {
namespace Entities {

struct MailHeader
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(uuid);
        s.value(fromName);
        s.value(subject);
        s.value(created);
        s.value(isRead);
        s.value(numAttachments);
    }
    std::string uuid;
    std::string fromName;
    std::string subject;
    timestamp_t created;
    bool isRead;
    size_t numAttachments;
};

/// List of mails. UUID is the receiver account UUID.
struct MailList : Entity
{
    MAKE_ENTITY(MailList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(mails);
    }

    std::vector<MailHeader> mails;
};

}
}
