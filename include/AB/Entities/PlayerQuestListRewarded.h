/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

/// A list of quests a player has completed and collected the reward. Keeping track
/// of this is necessary for e.g. Quest chains.
/// UUID is the player UUID
struct PlayerQuestListRewarded : Entity
{
    MAKE_ENTITY(PlayerQuestListRewarded)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(questUuids);
    }

    std::vector<std::string> questUuids;
};

}
}
