/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct Guild : Entity
{
    MAKE_ENTITY(Guild)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(name);
        s.value(tag);
        s.value(creatorAccountUuid);
        s.value(creatorName);
        s.value(creatorPlayerUuid);
        s.value(creation);
        s.value(guildHall);
        s.value(guildHallInstanceUuid);
        s.value(guildHallServerUuid);
    }

    std::string name;
    std::string tag;
    std::string creatorAccountUuid = EMPTY_GUID;
    std::string creatorName;
    std::string creatorPlayerUuid = EMPTY_GUID;
    timestamp_t creation;
    std::string guildHall = EMPTY_GUID;
    std::string guildHallInstanceUuid = EMPTY_GUID;
    std::string guildHallServerUuid = EMPTY_GUID;
};

}
}
