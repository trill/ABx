/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct IpBan : Entity
{
    MAKE_ENTITY(IpBan)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(banUuid);
        s.value(ip);
        s.value(mask);
    }

    std::string banUuid;
    uint32_t ip = 0;
    uint32_t mask = 0xFFFFFFFF;
};

}
}
