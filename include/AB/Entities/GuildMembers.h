/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

// 0 = Unknown, 1 = Guest, 2 = Invited, 3 = Member, 4 = Officer, 5 = Leader
enum GuildRole : uint8_t
{
    GuildRoleUnknown = 0,
    GuildRoleGuest,
    GuildRoleInvited,
    GuildRoleMember,
    GuildRoleOfficer,
    GuildRoleLeader
};

struct GuildMember
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(accountUuid);
        s.value(inviteName);
        s.value(role);
        s.value(invited);
        s.value(joined);
        s.value(expires);
    }
    std::string accountUuid = EMPTY_GUID;
    std::string inviteName;
    GuildRole role = GuildRoleUnknown;
    /// Time invited
    timestamp_t invited = 0;
    /// Time joined
    timestamp_t joined = 0;
    /// Time membership expires or 0 does not expire
    timestamp_t expires = 0;
};

/// List of Guild members. UUID is the Guild UUID. To add members to a Guild, add
/// to this list and save it.
struct GuildMembers : Entity
{
    MAKE_ENTITY(GuildMembers)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(members);
    }

    std::vector<GuildMember> members;
};

}
}
