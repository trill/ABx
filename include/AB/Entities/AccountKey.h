/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

inline constexpr uint16_t CHEST_SLOT_INCREASE = 10;

enum AccountKeyStatus : uint8_t
{
    KeyStatusUnknown = 0,
    KeyStatusNotActivated = 1,
    KeryStatusReadyForUse = 2,
    KeyStatusBanned = 3
};
enum AccountKeyType : uint8_t
{
    KeyTypeUnknown = 0,
    KeyTypeAccount = 1,
    KeyTypeCharSlot = 2,
    KeyTypeChestSlots = 3,     // Increase count of player inventory by 10
};

/// Account key entity. The UUID is the key.
struct AccountKey : Entity
{
    MAKE_ENTITY(AccountKey)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(type);
        s.value(total);
        s.value(used);
        s.value(status);
        s.value(email);
        s.value(description);
    }

    AccountKeyType type = KeyTypeUnknown;
    uint16_t total = 1;
    uint16_t used = 0;
    AccountKeyStatus status = KeyStatusUnknown;
    std::string email;
    std::string description;
};

}
}
