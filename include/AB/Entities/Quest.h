/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <vector>

namespace AB {
namespace Entities {

struct Quest : Entity
{
    MAKE_ENTITY(Quest)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(index);
        s.value(repeatable);
        s.value(rewardXp);
        s.value(rewardMoney);
        s.value(name);
        s.value(script);
        s.value(description);
        s.value(dependsOn);
        s.value(rewardItems);
    }

    uint32_t index{ INVALID_INDEX };
    bool repeatable{ false };
    int32_t rewardXp{ 0 };
    int32_t rewardMoney{ 0 };
    std::string name;
    std::string script;
    std::string description;
    std::string dependsOn{ EMPTY_GUID };
    std::vector<std::string> rewardItems;
};

}
}
