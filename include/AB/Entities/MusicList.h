/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct MusicList : Entity
{
    MAKE_ENTITY(MusicList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(musicUuids);
    }

    std::vector<std::string> musicUuids;
};

}
}
