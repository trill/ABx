/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum class StoragePlace : uint8_t
{
    None = 0,
    Scene = 1,      // On ground
    Inventory,      // In players inventory
    Chest,          // Account chest
    Equipped,       // A player has equipped this item
    Merchant,       // Merchant has this item and may be bought by a player
    UpgradeSlot,    // This item is part of another item
    Mail,           // Attached to a Mail
};

enum ConcreteItemFlag : uint32_t
{
    ConcreteItemFlagCustomized = 1,
};

using ItemPos = uint16_t;

struct ConcreteItem : Entity
{
    MAKE_ENTITY(ConcreteItem)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(playerUuid);
        s.value(accountUuid);
        s.value(mailUuid);
        s.value(upgrade1Uuid);
        s.value(upgrade2Uuid);
        s.value(upgrade3Uuid);
        s.value(itemUuid);
        s.value(itemStats);

        s.value(storagePlace);
        s.value(storagePos);
        s.value(count);
        s.value(creation);
        s.value(deleted);
        s.value(value);
        s.value(instanceUuid);
        s.value(mapUuid);
        s.value(flags);
        s.value(sold);
    }

    std::string playerUuid{ EMPTY_GUID };
    std::string accountUuid{ EMPTY_GUID };
    std::string mailUuid{ EMPTY_GUID };
    StoragePlace storagePlace{ StoragePlace::None };
    ItemPos storagePos{ 0 };
    std::string upgrade1Uuid{ EMPTY_GUID };
    std::string upgrade2Uuid{ EMPTY_GUID };
    std::string upgrade3Uuid{ EMPTY_GUID };
    std::string itemUuid{ EMPTY_GUID };
    std::string itemStats;
    uint32_t count{ 0 };
    timestamp_t creation{ 0 };
    timestamp_t deleted{ 0 };
    uint16_t value{ 0 };
    std::string instanceUuid{ EMPTY_GUID };
    std::string mapUuid{ EMPTY_GUID };
    uint32_t flags{ 0 };
    timestamp_t sold{ 0 };
};

inline bool IsItemCustomized(uint32_t flags)
{
    return (flags & ConcreteItemFlagCustomized) == ConcreteItemFlagCustomized;
}

}
}
