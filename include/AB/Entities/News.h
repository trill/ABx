/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct News : Entity
{
    MAKE_ENTITY(News)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(created);
        s.value(body);
    }

    timestamp_t created = 0;
    std::string body;
};

}
}
