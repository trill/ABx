/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/ConcreteItem.h>

namespace AB {
namespace Entities {

template <StoragePlace _Place>
struct _AccountItemList : Entity
{
    MAKE_ENTITY(_AccountItemList<_Place>)
    static constexpr StoragePlace Place = _Place;
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(storagePlace);
        s.value(itemUuids);
    }

    StoragePlace storagePlace = _Place;
    std::vector<std::string> itemUuids;
};

using AccountItemList = _AccountItemList<StoragePlace::None>;
using ChestItems = _AccountItemList<StoragePlace::Chest>;

}
}
