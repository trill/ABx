/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/FriendList.h>

namespace AB {
namespace Entities {

struct FriendAcc
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(accountUuid);
        s.value(relation);
    }
    std::string accountUuid;
    FriendRelation relation{ FriendRelationUnknown };
};

/// All these have me in their friend list
struct FriendedMe : Entity
{
    MAKE_ENTITY(FriendedMe)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(friends);
    }

    std::vector<FriendAcc> friends;
};

}
}
