/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

static constexpr auto ATTRIBUTE_NONE_UUID = "4e8d25fe-50f7-11e8-a7ca-02100700d6f0";

struct Attribute : Entity
{
    MAKE_ENTITY(Attribute)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(index);
        s.value(professionUuid);
        s.value(name);
        s.value(isPrimary);
    }

    uint32_t index = INVALID_INDEX;
    std::string professionUuid = EMPTY_GUID;
    std::string name;
    bool isPrimary = false;
};

}
}
