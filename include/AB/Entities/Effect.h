/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum EffectCategory : uint8_t
{
    EffectNone = 0,
    // From skills -------------------------------------------------------------
    EffectCondition = 1,
    EffectEnchantment = 2,
    EffectHex = 3,
    EffectShout = 4,
    EffectSpirit = 5,
    EffectWard = 6,
    EffectWell = 7,
    EffectSkill = 8,
    // Single, e.g. only one stance at a time, other stances remove previous stances
    EffectPreparation = 9,
    EffectStance = 10,
    EffectForm = 11,
    EffectGlyphe = 12,
    EffectPetAttack = 13,
    EffectWeaponSpell = 14,
    EffectSignet = 15,
    // /Single
    // Other -------------------------------------------------------------------
    EffectGeneral = 20,
    EffectEnvironment = 254
};

inline constexpr uint32_t EFFECT_INDEX_MORALE = 1001;

struct Effect : Entity
{
    MAKE_ENTITY(Effect)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(index);
        s.value(script);
    }

    uint32_t index = 0;
    std::string script;
};

}
}
