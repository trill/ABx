/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/ConcreteItem.h>

namespace AB {
namespace Entities {

static constexpr auto KEY_PLAYER_ITEMLIST = "player_item_list";

template<StoragePlace _Place>
struct _PlayerItemList : Entity
{
    MAKE_ENTITY(_PlayerItemList<_Place>);
    static constexpr StoragePlace Place = _Place;
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(storagePlace);
        s.value(itemUuids);
    }

    StoragePlace storagePlace = _Place;
    std::vector<std::string> itemUuids;
};

using PlayerItemList = _PlayerItemList<StoragePlace::None>;
using EquippedItems = _PlayerItemList<StoragePlace::Equipped>;
using InventoryItems = _PlayerItemList<StoragePlace::Inventory>;

}
}
