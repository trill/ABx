/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

// Check price of item. Only cached for a very short time.
struct ItemPrice : Entity
{
    MAKE_ENTITY(ItemPrice)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(countAvail);
        s.value(priceSell);
        s.value(priceBuy);
        s.value(priceBase);
    }

    uint32_t countAvail{ 0 };
    // How much the merchant charges
    uint32_t priceSell{ 0 };
    // How much the merchant pays
    uint32_t priceBuy{ 0 };
    uint32_t priceBase{ 0 };
};

}
}
