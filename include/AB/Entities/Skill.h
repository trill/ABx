/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

/// Skill type Tree
/// https://wiki.guildwars.com/wiki/Skill_type
// clang-format off
enum SkillType : uint64_t
{
    SkillTypeSkill = 0ll,
    SkillTypeAttack = 1ll,
        SkillTypeRangedAttack        = SkillTypeAttack | 1ll << 16,
            SkillTypeBowAttack           = SkillTypeRangedAttack | 1ll << 32,
            SkillTypeSpearAttack         = SkillTypeRangedAttack | 2ll << 32,
        SkillTypeMeleeAttack         = SkillTypeAttack | 2ll << 16,
            SkillTypeAxeAttack           = SkillTypeMeleeAttack | 1ll << 32,
            SkillTypeDaggerAttack        = SkillTypeMeleeAttack | 2ll << 32,
                SkillTypeLeadAttack          = SkillTypeDaggerAttack | 1ll << 48,
                SkillTypeOffHandAttack       = SkillTypeDaggerAttack | 2ll << 48,
                SkillTypeDualAttack          = SkillTypeDaggerAttack | 4ll << 48,
            SkillTypeHammerAttack        = SkillTypeMeleeAttack | 4ll << 16,
            SkillTypeScyteAttack         = SkillTypeMeleeAttack | 8ll << 16,
            SkillTypeSwordAttack         = SkillTypeMeleeAttack | 16ll << 16,
        SkillTypePetAttack           = SkillTypeAttack | 4ll << 16,
    SkillTypeShout = 2ll,
    SkillTypeChant = 4ll,
    SkillTypeEcho = 8ll,
    SkillTypeForm = 16ll,
    SkillTypeGlypthe = 32ll,
    SkillTypePreparation = 64ll,
    SkillTypeRitual = 128ll,
        SkillTypeBindingRitual       = SkillTypeRitual | 1ll << 16,
        SkillTypeNatureRitual        = SkillTypeRitual | 2ll << 16,
    SkillTypeSignet = 256ll,
    SkillTypeSpell = 512ll,
        SkillTypeEnchantment         = SkillTypeSpell | 1ll << 16,
            SkillTypeFlashEnchantment    = SkillTypeEnchantment | 1ll << 32,
        SkillTypeHex                 = SkillTypeSpell | 2ll << 16,
        SkillTypeItemSpell           = SkillTypeSpell | 4ll << 16,
        SkillTypeWardSpell           = SkillTypeSpell | 8ll << 16,
        SkillTypeWeaponSpell         = SkillTypeSpell | 16ll << 16,
        SkillTypeWellSpell           = SkillTypeSpell | 32ll << 16,
    SkillTypeStance = 1024ll,
    SkillTypeTrap = 2048ll,
    SkillTypeAll = 0xFFFFFFFFFFFFFFFF
};
// clang-format on

enum SkillAccess : uint32_t
{
    SkillAccessNone = 0,
    SkillAccessPlayer = 1,
    SkillAccessGM = 1 << 1,
    SkillAccessMonster = 1 << 2
};

struct Skill : Entity
{
    MAKE_ENTITY(Skill)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(index);
        s.value(script);
        s.value(access);
    }

    uint32_t index{ 0 };
    std::string script;
    uint32_t access{ SkillAccessNone };
};

inline bool HasSkillAccess(const Skill& skill, SkillAccess access)
{
    return (skill.access & access) == access;
}

}
}
