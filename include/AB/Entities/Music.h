/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum MusicStyle : uint32_t
{
    MusicStyleUnknown = 0,
    MusicStyleEastern = 1 << 1,
    MusicSttyleWestern = 1 << 2,
    MusicStyleSlow = 1 << 3,
    MusicStyleDriving = 1 << 4,
    MusicStyleEpic = 1 << 5,
    MusicStyleAction = 1 << 6,
    MusicStyleMystic = 1 << 7,
    MusicStyleIntense = 1 << 8,
    MusicStyleAggressive = 1 << 9,
    MusicStyleRelaxed = 1 << 10,
    MusicStyleHumorous = 1 << 11
};

struct Music : Entity
{
    MAKE_ENTITY(Music)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(mapUuid);
        s.value(localFile);
        s.value(remoteFile);
        s.value(sorting);
        s.value(style);
    }

    std::string mapUuid;
    std::string localFile;
    std::string remoteFile;
    uint8_t sorting = 0;
    MusicStyle style = MusicStyleUnknown;
};

}
}
