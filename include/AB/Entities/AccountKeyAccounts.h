/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

/// Account key entity. The UUID is the key.
struct AccountKeyAccounts : Entity
{
    MAKE_ENTITY(AccountKeyAccounts)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(accountUuid);
    }

    std::string accountUuid = EMPTY_GUID;
};

}
}
