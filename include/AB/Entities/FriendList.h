/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum FriendRelation : uint8_t
{
    FriendRelationUnknown = 0,
    FriendRelationFriend = 1,
    FriendRelationIgnore = 2
};

struct Friend
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(friendUuid);
        s.value(friendName);
        s.value(relation);
        s.value(creation);
    }

    /// Friend account UUID
    std::string friendUuid;
    /// Character or nick name of the friend
    std::string friendName;
    FriendRelation relation{ FriendRelationUnknown };
    timestamp_t creation;
};

/// Friendlist. UUID is Account UUID
struct FriendList : Entity
{
    MAKE_ENTITY(FriendList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(friends);
    }

    std::vector<Friend> friends;
};

}
}
