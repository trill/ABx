/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct ProfessionList : Entity
{
    MAKE_ENTITY(ProfessionList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(profUuids);
    }

    std::vector<std::string> profUuids;
};

}
}
