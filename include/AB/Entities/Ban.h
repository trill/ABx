/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum BanReason : uint8_t
{
    BanReasonUnknown = 0,
    BanReasonOther,
    BanReasonScamming,
    BanReasonBotting,
    BanReasonGoldSelling,
    BanReasonNameReport,
};

struct Ban : Entity
{
    MAKE_ENTITY(Ban)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(expires);
        s.value(added);
        s.value(reason);
        s.value(active);
        s.value(adminUuid);
        s.value(comment);
    }

    timestamp_t expires = 0;
    timestamp_t added = 0;
    BanReason reason = BanReasonUnknown;
    bool active = false;
    std::string adminUuid = EMPTY_GUID;
    std::string comment;
    uint32_t hits = 0;
};

}
}
