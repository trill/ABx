/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct ItemChance
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(itemUuid);
        s.value(chance);
        s.value(canDrop);
    }
    std::string itemUuid;
    // Chance in promille (0..1000)
    uint32_t chance;
    bool canDrop;
};

struct ItemChanceList : Entity
{
    MAKE_ENTITY(ItemChanceList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(items);
    }

    std::vector<ItemChance> items;
};

}
}
