/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/Game.h>

namespace AB {
namespace Entities {

struct GameInstance : Entity
{
    MAKE_ENTITY(GameInstance)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(gameUuid);
        s.value(serverUuid);
        s.value(name);
        s.value(recording);
        s.value(startTime);
        s.value(stopTime);
        s.value(number);
        s.value(running);
        s.value(players);
    }

    std::string gameUuid = EMPTY_GUID;
    /// Server running this instance
    std::string serverUuid = EMPTY_GUID;
    std::string name;
    /// Recording filename
    std::string recording;
    timestamp_t startTime{ 0 };
    timestamp_t stopTime{ 0 };
    /// If there are more instances of the same game on the same server this is the number, e.g. District.
    uint16_t number = 0;
    bool running{ false };
    // Number of players in this instance
    uint16_t players{ 0 };
};

}
}
