/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct Version : Entity
{
    MAKE_ENTITY(Version)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(name);
        s.value(value);
        s.value(isInternal);
    }

    std::string name;
    uint32_t value = 0;
    bool isInternal = false;
};

}
}
