/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <vector>

namespace AB {
namespace Entities {

static constexpr int MAX_MAIL_COUNT = 100;
static constexpr int MAX_MAIL_SUBJECT = 60;
static constexpr int MAX_MAIL_MESSAGE = 255;

struct Mail : Entity
{
    MAKE_ENTITY(Mail)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(fromAccountUuid);
        s.value(toAccountUuid);
        s.value(fromName);
        s.value(toName);
        s.value(subject);
        s.value(message);
        s.value(created);
        s.value(isRead);
        s.value(attachments);
    }

    std::string fromAccountUuid = EMPTY_GUID;
    std::string toAccountUuid = EMPTY_GUID;
    std::string fromName;
    std::string toName;
    std::string subject;
    std::string message;
    timestamp_t created = 0;
    bool isRead = false;
    std::vector<std::string> attachments;
};

}
}
