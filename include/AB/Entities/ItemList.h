/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct ItemList : Entity
{
    MAKE_ENTITY(ItemList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(itemUuids);
    }

    std::vector<std::string> itemUuids;
};

}
}
