/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/Item.h>

namespace AB {
namespace Entities {

static constexpr auto KEY_TYPED_ITEMLIST = "typed_item_list";

struct TypedListItem
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(uuid);
        s.value(belongsTo);
        s.value(chance);
    }

    std::string uuid;
    ItemType belongsTo = ItemType::Unknown;
    float chance = 0.0f;
};

/// Item by type on a certain map with drop chances. UUID is the map uuid.
struct TypedItemList : Entity
{
    static constexpr const char* KEY()
    {
        return KEY_TYPED_ITEMLIST;
    }
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(type);
        s.value(items);
    }

    ItemType type = ItemType::Unknown;
    std::vector<TypedListItem> items;
};

/// All insignias
struct TypedItemsInsignia : TypedItemList
{
    static constexpr const char* KEY()
    {
        return "insignia_item_list";
    }
    TypedItemsInsignia() :
        TypedItemList()
    {
        type = ItemType::ModifierInsignia;
    }
};

/// All runes
struct TypedItemsRunes : TypedItemList
{
    static constexpr const char* KEY()
    {
        return "runes_item_list";
    }
    TypedItemsRunes() :
        TypedItemList()
    {
        type = ItemType::ModifierRune;
    }
};

struct TypedItemsWeaponPrefix : TypedItemList
{
    static constexpr const char* KEY()
    {
        return "weapon_prefix_item_list";
    }
    TypedItemsWeaponPrefix() :
        TypedItemList()
    {
        type = ItemType::ModifierWeaponPrefix;
    }
};

struct TypedItemsWeaponSuffix : TypedItemList
{
    static constexpr const char* KEY()
    {
        return "weapon_suffix_item_list";
    }
    TypedItemsWeaponSuffix() :
        TypedItemList()
    {
        type = ItemType::ModifierWeaponSuffix;
    }
};

struct TypedItemsWeaponInscription : TypedItemList
{
    static constexpr const char* KEY()
    {
        return "weapon_inscrition_item_list";
    }
    TypedItemsWeaponInscription() :
        TypedItemList()
    {
        type = ItemType::ModifierWeaponInscription;
    }
};

}
}
