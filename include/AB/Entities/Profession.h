/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <vector>

namespace AB {
namespace Entities {

enum ProfessionIndex : uint32_t
{
    ProfessionIndexNone = 0,
    ProfessionIndexWarrior = 1,
    ProfessionIndexRanger = 2,
    ProfessionIndexMonk = 3,
    ProfessionIndexNecromancer = 4,
    ProfessionIndexMesmer = 5,
    ProfessionIndexElementarist = 6
};

/// Usual position of the profession. Used by the matchmaking server to make balanced teams
enum class ProfessionPosition : uint8_t
{
    None = 0,
    Frontline,                   // Synonym for Warrior
    Midline,                     // DD
    Backline,                    // Synonym for supporter, healer, prot etc.
};

struct AttriInfo
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(uuid);
        s.value(index);
        s.value(primary);
    }
    std::string uuid;
    uint32_t index{ 0 };
    bool primary{ false };
};

inline constexpr auto PROFESSION_NONE_UUID = "79b75ff4-92f0-11e8-a7ca-02100700d6f0";

struct Profession : Entity
{
    MAKE_ENTITY(Profession)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(index);
        s.value(name);
        s.value(abbr);
        s.value(modelIndexFemale);
        s.value(modelIndexMale);
        s.value(position);
        s.value(attributeCount);
        s.value(attributes);
    }

    uint32_t index = 0;
    std::string name;
    std::string abbr;
    uint32_t modelIndexFemale = 0;
    uint32_t modelIndexMale = 0;
    ProfessionPosition position = ProfessionPosition::None;
    uint32_t attributeCount = 0;
    std::vector<AttriInfo> attributes;
};

}
}
