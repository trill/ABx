/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include <array>
#include <string>
#include <cstring>
#include <type_traits>
#include <sa/Assert.h>

// Serialize any struct/class without inheritance, polymorphism, RTTI or macros into a byte buffer.

/*
// Example:

struct Foo
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(aValues);
        s.value(strValues);
    }
    std::array<int, 5> aValues;
    std::vector<std::string> strValues;
};

struct Bar
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(iValue);
        s.value(strValue);
    }
    int iValue;
    std::string strValue;
};

struct Baz : public Foo
{
    template<typename S>
    void Serialize(S& s)
    {
        Foo::Serialize(s);
        s.value(bar);
        s.value(largeInt);
    }
    // Struct with its own Serialize() function
    Bar bar;
    int64_t largeInt;
};

Serializer::DataBuff buffer;

// Write it to buffer
Baz baz;
size_t bytesWritten = Serializer::Set(baz, buffer);

// Read it from buffer
Baz baz2;
bool success = Serializer::Get(baz2, data);

// Or
Baz baz3 = Serializer::Get(data);

// Calculate size needed to store baz
size_t sizeNeeded = Serializer::GetSize(baz);
*/

namespace AB::Entities::Serializer {

using DataBuff = std::vector<uint8_t>;
// Type to store sizes, shouldn't be a generic type like size_t
using ContainerSizeType = uint16_t;

namespace Internal {

template <typename T>
struct IsVector
{
    static const bool value = false;
};
template <typename T, typename Alloc>
struct IsVector<std::vector<T, Alloc>>
{
    static const bool value = true;
};

template <typename T>
struct IsArray
{
    static const bool value = false;
};
template <class T, size_t Size>
struct IsArray<std::array<T, Size>>
{
    static const bool value = true;
};

template <typename T, typename S>
struct HasSerialize
{
private:
    typedef char YesType[1];
    typedef char NoType[2];

    template <typename C> static YesType& test(decltype(&C::template Serialize<S>));
    template <typename C> static NoType& test(...);
public:
    static const bool value = sizeof(test<T>(0)) == sizeof(YesType);
};

class BufferWrapper
{
private:
    DataBuff& buff_;
    mutable size_t pos_{ 0 };
    bool CanRead(size_t size) const
    {
        return pos_ + size <= buff_.size();
    }
    void AddString(const std::string& value)
    {
        ContainerSizeType len = static_cast<ContainerSizeType>(value.length());
        Add<ContainerSizeType>(len);
        // Buffer must have correct size
        ASSERT(buff_.size() >= pos_ + len);
        // Allows also \0
#ifdef _MSC_VER
        memcpy_s(buff_.data() + pos_, len, value.data(), len);
#else
        memcpy(buff_.data() + pos_, value.data(), len);
#endif
        pos_ += static_cast<size_t>(len);
    }
    std::string GetString() const
    {
        size_t len = Get<ContainerSizeType>();
        if (!CanRead(len))
            return {};

        char* v = reinterpret_cast<char*>(buff_.data()) + pos_;
        pos_ += len;
        return std::string(v, len);
    }
public:
    explicit BufferWrapper(DataBuff& buff) :
        buff_(buff)
    { }
    template <typename T>
    T Get() const
    {
        // Everything else must be trivially copyable
        static_assert(std::is_trivially_copyable_v<T>);
        if (!CanRead(sizeof(T)))
            return {};
        T v{};
#ifdef _MSC_VER
        memcpy_s(&v, sizeof(T), buff_.data() + pos_, sizeof(T));
#else
        memcpy(&v, buff_.data() + pos_, sizeof(T));
#endif
        pos_ += sizeof(T);
        return v;
    }
    template <typename T>
    void Add(const T& value)
    {
        // Everything else must be trivially copyable
        static_assert(std::is_trivially_copyable_v<T>);
        // Buffer must have correct size
        ASSERT(buff_.size() >= pos_ + sizeof(T));
#ifdef _MSC_VER
        memcpy_s(buff_.data() + pos_, sizeof(T), &value, sizeof(T));
#else
        memcpy(buff_.data() + pos_, &value, sizeof(T));
#endif
        pos_ += sizeof(T);
    }
};

template <>
inline void BufferWrapper::Add<std::string>(const std::string& value)
{
    AddString(value);
}
template <>
inline void BufferWrapper::Add<bool>(const bool& value)
{
    Add<uint8_t>(value ? 1 : 0);
}
template <>
inline std::string BufferWrapper::Get<std::string>() const
{
    return GetString();
}
template <>
inline bool BufferWrapper::Get<bool>() const
{
    return Get<uint8_t>() != 0;
}

class Reader
{
private:
    const BufferWrapper& buff_;
public:
    explicit Reader(const BufferWrapper& buff) :
        buff_(buff)
    { }
    template<typename T>
    void value(T& v) requires IsVector<T>::value
    {
        size_t size = (size_t)buff_.template Get<ContainerSizeType>();
        v.resize(size);
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires IsArray<T>::value
    {
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires HasSerialize<T, Reader>::value
    {
        v.Serialize(*this);
    }
    template<typename T>
    void value(T& v)
    {
        v = buff_.template Get<T>();
    }
};

class Writer
{
private:
    BufferWrapper& buff_;
public:
    explicit Writer(BufferWrapper& buff) :
        buff_(buff)
    { }
    template<typename T>
    void value(T& v) requires IsVector<T>::value
    {
        buff_.template Add<ContainerSizeType>(static_cast<ContainerSizeType>(v.size()));
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires IsArray<T>::value
    {
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires HasSerialize<T, Writer>::value
    {
        v.Serialize(*this);
    }
    template<typename T>
    void value(T& v)
    {
        buff_.template Add<T>(v);
    }
};

// Calculates the required size for this packet
class SizeCalculator
{
private:
    size_t size_{ 0 };
public:
    template<typename T>
    void value(T& v) requires IsVector<T>::value
    {
        size_ += sizeof(ContainerSizeType);
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires IsArray<T>::value
    {
        for (auto& item : v)
            value(item);
    }
    template<typename T>
    void value(T& v) requires HasSerialize<T, SizeCalculator>::value
    {
        v.Serialize(*this);
    }
    template<typename T>
    void value(T&)
    {
        size_ += sizeof(T);
    }
    size_t GetSize() const { return size_; }
};

template<>
inline void SizeCalculator::value<std::string>(std::string& value)
{
    size_ += sizeof(ContainerSizeType);  // Length
    size_ += value.length();
}

template <>
inline void SizeCalculator::value<bool>(bool&)
{
    ++size_;
}

}  // namespace Internal

// Calculate required size
template<typename T>
[[nodiscard]] inline size_t GetSize(const T& value)
{
    static_assert(Internal::HasSerialize<T, Internal::SizeCalculator>::value);
    Internal::SizeCalculator calc;
    const_cast<T&>(value).Serialize(calc);
    return calc.GetSize();
}

// Read a packet from the message
template<typename T>
[[nodiscard]] inline T Get(DataBuff& buff)
{
    static_assert(Internal::HasSerialize<T, Internal::Reader>::value);
    T result{};
    const Internal::BufferWrapper wrapper(buff);
    Internal::Reader reader(wrapper);
    result.Serialize(reader);
    return result;
}

// Read a packet from the message
template<typename T>
inline bool Get(T& value, DataBuff& buff)
{
    static_assert(Internal::HasSerialize<T, Internal::Reader>::value);
    if (buff.empty())
        return false;
    const Internal::BufferWrapper wrapper(buff);
    Internal::Reader reader(wrapper);
    value.Serialize(reader);
    return true;
}

// Add a packet to the message
template<typename T>
inline size_t Set(const T& value, DataBuff& buff)
{
    static_assert(Internal::HasSerialize<T, Internal::Writer>::value);
    // Can not be const T& value because we are using the same function for reading
    // and writing which is not const
    size_t size = GetSize(value);
    buff.resize(size);
    Internal::BufferWrapper wrapper(buff);
    Internal::Writer writer(wrapper);
    const_cast<T&>(value).Serialize(writer);
    return size;
}

}
