/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

template<size_t _Limit>
struct NewsList : Entity
{
    MAKE_ENTITY(NewsList<_Limit>)
    static constexpr size_t Limit = _Limit;
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(uuids);
    }

    std::vector<std::string> uuids;
};

using LatestNewsList = NewsList<5>;
using AllNewsList = NewsList<0>;

}
}
