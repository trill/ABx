/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <string>
#include <sa/TypeName.h>
#include <limits>

#define MAKE_ENTITY(e)                      \
    static constexpr std::string_view KEY() \
    {                                       \
        return sa::TypeName<e>::Get();      \
    }

namespace AB {
namespace Entities {

typedef long long timestamp_t;
static constexpr uint32_t INVALID_INDEX = std::numeric_limits<uint32_t>::max();
static constexpr auto EMPTY_GUID = "00000000-0000-0000-0000-000000000000";

/// Base class for entities.
struct Entity
{
    template<typename S>
    void Serialize(S& s)
    {
        // UUID must be the first serialized
        s.value(uuid);
    }

    std::string uuid = EMPTY_GUID;
};

}
}
