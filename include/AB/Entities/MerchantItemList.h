/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <AB/Entities/ConcreteItem.h>
#include <AB/Entities/Item.h>

namespace AB {
namespace Entities {

// A list of items the Merchant has to sell
struct MerchantItemList : Entity
{
    MAKE_ENTITY(MerchantItemList)
    struct Item
    {
        template<typename S>
        void Serialize(S& s)
        {
            s.value(index);
            s.value(type);
            s.value(name);
            s.value(concreteUuid);
            s.value(itemUuid);
        }
        uint32_t index;
        ItemType type;
        std::string name;
        std::string concreteUuid;
        std::string itemUuid;
    };
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(items);
    }

    std::vector<Item> items;
};

}
}
