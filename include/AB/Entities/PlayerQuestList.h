/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

/// A list of quests a player has running, completed or not but still not collected reward.
/// UUID is the player UUID
struct PlayerQuestList : Entity
{
    MAKE_ENTITY(PlayerQuestList)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(questUuids);
    }

    std::vector<std::string> questUuids;
};

}
}
