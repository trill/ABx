/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

/// Account ban entity.
struct AccountBan : Entity
{
    MAKE_ENTITY(AccountBan)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(banUuid);
        s.value(accountUuid);
    }

    std::string banUuid = EMPTY_GUID;
    std::string accountUuid = EMPTY_GUID;
};

}
}
