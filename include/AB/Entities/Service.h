/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum ServiceType : uint8_t
{
    ServiceTypeUnknown = 0,
    ServiceTypeDataServer,
    ServiceTypeMessageServer,
    ServiceTypeFileServer,
    ServiceTypeLoginServer,
    ServiceTypeGameServer,
    ServiceTypeMatchServer,

    ServiceTypeAdminServer,

    // Must be last
    ServiceTypeLoadBalancer = 255
};

enum ServiceStatus : uint8_t
{
    ServiceStatusOffline = 0,
    ServiceStatusOnline
};

inline constexpr unsigned HEARTBEAT_INTERVAL = 1000;

struct Service : Entity
{
    MAKE_ENTITY(Service)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(name);
        s.value(version);
        s.value(type);
        s.value(location);
        s.value(host);
        s.value(port);
        s.value(ip);
        s.value(status);
        s.value(startTime);
        s.value(stopTime);
        s.value(runTime);
        s.value(machine);
        s.value(file);
        s.value(path);
        s.value(arguments);
        s.value(temporary);
        s.value(load);
        s.value(heartbeat);
    }

    std::string name;
    uint32_t version{ 0 };
    ServiceType type = ServiceTypeUnknown;
    std::string location;
    std::string host;
    uint16_t port = 0;
    std::string ip;
    ServiceStatus status = ServiceStatusOffline;
    timestamp_t startTime = 0;
    timestamp_t stopTime = 0;
    /// Runtime in seconds
    int64_t runTime = 0;
    /// Machine the server is running on
    std::string machine;
    std::string file;
    std::string path;
    std::string arguments;

    /// Temporary service, not written to DB
    bool temporary = false;
    /// Service load, something between 0..100. Not written to DB. The service
    /// is responsible to update this value.
    uint8_t load = 0;
    /// Last heart beat time
    timestamp_t heartbeat{ 0 };
};

}
}
