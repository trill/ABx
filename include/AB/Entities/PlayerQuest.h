/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

struct PlayerQuest : Entity
{
    MAKE_ENTITY(PlayerQuest)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(playerUuid);
        s.value(questUuid);
        s.value(completed);
        s.value(rewarded);
        s.value(deleted);
        s.value(pickupTime);
        s.value(completeTime);
        s.value(rewardTime);
        s.value(progress);
    }

    std::string playerUuid{ EMPTY_GUID };
    std::string questUuid{ EMPTY_GUID };
    bool completed{ false };   //!< Player completed the quest
    bool rewarded{ false };    //!< Player collected reward for this quest -> no longer shown in quest log
    bool deleted{ false };
    timestamp_t pickupTime{ 0 };
    timestamp_t completeTime{ 0 };
    timestamp_t rewardTime{ 0 };
    std::string progress;
};

}
}
