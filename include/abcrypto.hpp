#pragma once

extern "C"
{
#include <arc4random.h>
#include <bcrypt.h>
#include <blowfish.h>
#include <dh.h>
#include <xxtea.h>
#include <aes.h>
#include <md5.h>
#include <sha1.h>
#include <sha256.h>
}
