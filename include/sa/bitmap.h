/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/color.h>
#include <sa/Assert.h>
#include <sa/Noncopyable.h>
#include <algorithm>
#include <array>
#include <sa/math.h>

namespace sa {

// Wrapper around pixel data.
// Note: This class does not manage ownership of the data.
class Bitmap
{
    NON_COPYABLE(Bitmap)
public:
    Bitmap() = default;
    Bitmap(int width, int height, int components, unsigned char* data = nullptr) :
        width_(width),
        height_(height),
        components_(components),
        data_(data)
    { }
    // Allocate data for a bitmap. The caller is responsible to free() it.
    unsigned char* Create(int width, int height, int components)
    {
        width_ = width;
        height_ = height;
        components_ = components;
        data_ = (unsigned char*)malloc((size_t)width_ * (size_t)height_ * (size_t)components_);
        return data_;
    }
    void Destroy()
    {
        if (!data_)
            return;
        free(data_);
        data_ = nullptr;
    }
    Color GetPixel(int x, int y) const
    {
        ASSERT(data_);
        if (x < 0 || y < 0 || x >= width_ || y >= height_)
            return {};
        const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
        uint8_t r = 0, g = 0, b = 0, a = 255;
        if (components_ == 1)
            // B/W
            r = g = b = data_[(index * components_)];
        else if (components_ == 3)
        {
            // RGB
            r = data_[(index * components_)];
            g = data_[(index * components_ + 1)];
            b = data_[(index * components_ + 2)];
        }
        else if (components_ == 4)
        {
            // RGBA
            r = data_[(index * components_)];
            g = data_[(index * components_ + 1)];
            b = data_[(index * components_ + 2)];
            a = data_[(index * components_ + 3)];
        }
        else
            ASSERT_FALSE();
        return Color{ r, g, b, a };
    }
    void SetPixel(int x, int y, const Color& color)
    {
        ASSERT(data_);
        if (x < 0 || y < 0 || x >= width_ || y >= height_)
            return;
        const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
        if (components_ == 1)
            // B/W
            data_[(index * components_)] = color.r_;
        else if (components_ == 3)
        {
            // RGB
            data_[(index * components_)] = color.r_;
            data_[(index * components_ + 1)] = color.g_;
            data_[(index * components_ + 2)] = color.b_;
        }
        else if (components_ == 4)
            // RGBA
            *reinterpret_cast<uint32_t*>(&data_[(index * components_)]) = color.To32();
        else
            ASSERT_FALSE();
    }

    void Tint(float factor)
    {
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                SetPixel(x, y, GetPixel(x, y).Filtered<filter::Tint>({ factor }));
    }
    void Tint(float factor, Bitmap& result) const
    {
        ASSERT(width_ == result.width_);
        ASSERT(height_ == result.height_);
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                result.SetPixel(x, y, GetPixel(x, y).Filtered<filter::Tint>({ factor }));
    }
    void Shade(float factor)
    {
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                SetPixel(x, y, GetPixel(x, y).Filtered<filter::Shade>({ factor }));
    }
    void Shade(float factor, Bitmap& result)
    {
        ASSERT(width_ == result.width_);
        ASSERT(height_ == result.height_);
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                result.SetPixel(x, y, GetPixel(x, y).Filtered<filter::Shade>({ factor }));
    }
    void Lerp(const Color& rhs, float factor)
    {
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                SetPixel(x, y, GetPixel(x, y).Filtered<filter::Lerp>({ factor }, rhs));
    }
    void Lerp(const Bitmap& rhs, float factor)
    {
        ASSERT(width_ == rhs.width_);
        ASSERT(height_ == rhs.height_);
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                SetPixel(x, y, GetPixel(x, y).Filtered<filter::Lerp>({ factor }, rhs.GetPixel(x, y)));
    }
    void Lerp(const Bitmap& rhs, float factor, Bitmap& result)
    {
        ASSERT(width_ == rhs.width_);
        ASSERT(height_ == rhs.height_);
        ASSERT(width_ == result.width_);
        ASSERT(height_ == result.height_);
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                result.SetPixel(x, y, GetPixel(x, y).Filtered<filter::Lerp>({ factor }, rhs.GetPixel(x, y)));
    }
    void Lerp(const Color& rhs, float factor, Bitmap& result)
    {
        ASSERT(width_ == result.width_);
        ASSERT(height_ == result.height_);
        for (int y = 0; y < height_; ++y)
            for (int x = 0; x < width_; ++x)
                result.SetPixel(x, y, GetPixel(x, y).Filtered<filter::Lerp>({ factor }, rhs));
    }
    void AlphaBlend(int x, int y, const Color& c, bool keep_alpha = true)
    {
        if (x < 0 || y < 0 || x >= width_ || y >= height_)
            return;
        SetPixel(x, y, GetPixel(x, y).Filtered<filter::AlphaBlend>({ keep_alpha }, c));
    }
    void AlphaBlendLerp(int x, int y, const Color& c, const Color& lerp_color, float lerp_factor, bool keep_alpha = true)
    {
        if (x < 0 || y < 0 || x >= width_ || y >= height_)
            return;
        const Color cl = c.Filtered<filter::Lerp>({ lerp_factor }, lerp_color);
        SetPixel(x, y, GetPixel(x, y).Filtered<filter::AlphaBlend>({ keep_alpha }, cl));
    }
    // Alpha blends b over this at x,y
    // \param x X position
    // \param y Y position
    // \param b The bitmap to draw
    // \param keep_alpha If false it changes the alpha value b's alpha value.
    // \note If keep_alpha is true and you draw a bitmap into transparent bitmap the resulting bitmap is also transparent,
    // because it keeps the original alpha value. In this case you may want to set it to false.
    void AlphaBlend(int x, int y, const Bitmap& b, bool keep_alpha = true)
    {
        for (int _y = 0; _y < b.Height(); ++_y)
        {
            if (_y + y >= height_)
                break;
            for (int _x = 0; _x < b.Width(); ++_x)
            {
                if (_x + x >= width_)
                    break;
                AlphaBlend(_x + x, _y + y, b.GetPixel(_x, _y), keep_alpha);
            }
        }
    }
    void AlphaBlendLerp(int x, int y, const Bitmap& b, const Color& lerp_color, float lerp_factor, bool keep_alpha = true)
    {
        for (int _y = 0; _y < b.Height(); ++_y)
        {
            if (_y + y >= height_)
                break;
            for (int _x = 0; _x < b.Width(); ++_x)
            {
                if (_x + x >= width_)
                    break;

                AlphaBlendLerp(_x + x, _y + y, b.GetPixel(_x, _y), lerp_color, lerp_factor, keep_alpha);
            }
        }
    }
    void DrawPoint(int x, int y, int size, const Color& color)
    {
        if (size < 2)
            return AlphaBlend(x, y, color, false);
        for (int _y = -(size / 2); _y < size / 2; ++_y)
        {
            for (int _x = -(size / 2); _x < size / 2; ++_x)
                AlphaBlend(x + _x, y + _y, color, false);
        }
    }
    void DrawLine(int x1, int y1, int x2, int y2, int size, const Color& color)
    {
        const bool steep = (abs(y2 - y1) > abs(x2 - x1));
        if (steep)
        {
            std::swap(x1, y1);
            std::swap(x2, y2);
        }

        if (x1 > x2)
        {
            std::swap(x1, x2);
            std::swap(y1, y2);
        }

        const int dx = x2 - x1;
        const int dy = abs(y2 - y1);

        int error = dx / 2;
        const int ystep = (y1 < y2) ? 1 : -1;
        int y = y1;
        const int maxX = x2;
        for (int x = x1; x <= maxX; x++)
        {
            if (steep)
                DrawPoint(y, x, size, color);
            else
                DrawPoint(x, y, size, color);

            error -= dy;
            if (error < 0)
            {
                y += ystep;
                error += dx;
            }
        }
    }
    void DrawRectangle(int x1, int y1, int x2, int y2, const Color& color)
    {
        for (int y = y1; y <= y2; ++y)
        {
            for (int x = x1; x <= x2; ++x)
                DrawPoint(x, y, 1, color);
        }
    }
    void DrawCircle(int x, int y, float radius, const Color& color, int thickness)
    {
        for (float i = 0; i < 360.0f; i += 0.1f)
        {
            const int _x = (int)(radius * cosf(i * (float)M_PI / 180.0f));
            const int _y = (int)(radius * sinf(i * (float)M_PI / 180.0f));

            DrawPoint(x + _x, y + _y, thickness, color);
        }
    }
    void Clear(const Color& c)
    {
        if (components_ == 4)
        {
            const uint32_t uc = c.To32();
            for (int y = 0; y < height_; ++y)
            {
                for (int x = 0; x < width_; ++x)
                {
                    const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
                    *reinterpret_cast<uint32_t*>(&data_[(index * components_)]) = uc;
                }
            }
        }
        else
        {
            for (int y = 0; y < height_; ++y)
            {
                for (int x = 0; x < width_; ++x)
                    SetPixel(x, y, c);
            }
        }
    }
    void Clear()
    {
        // Black transparent
        static constexpr Color black{ 0, 0, 0, 0 };
        Clear(black);
    }
    unsigned char* ScanLine(int line)
    {
        const size_t index = (size_t)line * (size_t)width_;
        return &data_[(index * components_)];
    }
    int Width() const { return width_; }
    int Height() const { return height_; }
    int Components() const { return components_; }
    unsigned char* Data() const { return data_; }
    bool IsEmpty() const { return !data_; }
    void SetBitmap(int width, int height, int components, unsigned char* data)
    {
        ASSERT(width > 0);
        ASSERT(height > 0);
        ASSERT(components > 0);
        ASSERT(data);
        width_ = width;
        height_ = height;
        components_ = components;
        data_ = data;
    }
#ifdef STBIR_INCLUDE_STB_IMAGE_RESIZE_H
    // Resize this into result. Result must be data_ allocated to hold with * height * components
    // Must include stb_image_resize.h before this
    bool resize(bitmap& result)
    {
        return stbir_resize_uint8(data_, width_, height_, width_ * components_,
            result.data_, result.width_, result.height_, result.width_ * result.components_);
    }
#endif
#ifdef STBI_INCLUDE_STB_IMAGE_H
    // After using this you must free() the returned pointer because stbi_load()
    // allocates it but this class does not own the data.
    // Must include stb_image.h before this
    unsigned char* load(const std::string& filename)
    {
        data_ = stbi_load(filename.c_str(), &width_, &height_, &components_, 0);
        return data_;
    }
#endif
#ifdef INCLUDE_STB_IMAGE_WRITE_H
    // Must include stb_image_write.h before this
    bool save(const std::string& filename)
    {
        return stbi_write_png(filename.c_str(), width_, height_, components_, data_, width_ * components_);
    }
#endif
private:
    int width_{ 0 };
    int height_{ 0 };
    int components_{ 0 };
    unsigned char* data_{ nullptr };
};

}
