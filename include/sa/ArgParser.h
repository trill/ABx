/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace sa {
namespace ArgParser {

static constexpr char NAME_VALUE_DELIM = '=';

enum class OptionType
{
    None,
    String,
    Integer,
    Float
};

struct Option
{
    std::string name;
    std::vector<std::string> switches;
    std::string help;
    bool mandatory;
    bool has_argument;
    OptionType type{ OptionType::None };
};

struct ValueBase { };

template <typename T>
struct Value : public ValueBase
{
    T Value;
};

using Values = std::map<std::string, std::unique_ptr<ValueBase>>;

template <typename T>
inline std::optional<T> GetValue(const Values& vals, const std::string& name)
{
    const auto it = vals.find(name);
    if (it == vals.end())
        return {};
    const auto* v = static_cast<Value<T>*>((*it).second.get());
    return v->Value;
}

template <typename T>
inline T GetValue(const Values& vals, const std::string& name, T def)
{
    const auto it = vals.find(name);
    if (it == vals.end())
        return def;
    const auto* v = static_cast<Value<T>*>((*it).second.get());
    return v->Value;
}

// Command line interface
using Cli = std::vector<Option>;

inline void RemoveOption(const std::string& name, Cli& _cli)
{
    auto it = std::find_if(_cli.begin(), _cli.end(), [&name](const Option& current) {
        return current.name == name;
    });
    if (it != _cli.end())
        _cli.erase(it);
}

// Parsing result
struct Result
{
    bool success{ false };
    std::vector<std::string> items;
    operator bool() noexcept { return success; }
};

inline Result Parse(const std::vector<std::string>& args, const Cli& _cli, Values& vals)
{
    Result res;
    res.success = true;

    auto is_int = [](const std::string& s) -> bool
    {
        if (s.empty())
            return false;
        std::string::const_iterator it = s.begin();
        while (it != s.end() && std::isdigit(*it)) ++it;
        return it == s.end();
    };
    auto is_float = [](const std::string& s) -> bool
    {
        if (s.empty())
            return false;
        std::istringstream iss(s);
        float f;
        iss >> std::noskipws >> f;
        return iss.eof() && !iss.fail();
    };

    auto find_option = [&](const std::string& name) -> std::optional<Option>
    {
        for (const auto& o : _cli)
        {
            for (const auto& s : o.switches)
            {
                if (s.compare(name) == 0)
                    return o;
            }
        }
        return { };
    };

    auto split_name_value = [](const std::string& s) -> std::pair<std::string, std::string>
    {
        auto pos = s.find(NAME_VALUE_DELIM);
        if (pos != std::string::npos)
            return { s.substr(0, pos), s.substr(pos + 1) };
        return { s, "" };
    };

    int unnamed = 0;
    for (auto it = args.begin(); it != args.end(); ++it)
    {
        // Split -name=value
        auto name_value = split_name_value(*it);
        auto o = find_option(name_value.first);

        if (!o.has_value())
        {
            // This option is not defined in the CLI, add is as unnamed.
            std::unique_ptr<Value<std::string>> val = std::make_unique<Value<std::string>>();
            val->Value = (*it);
            vals.emplace(std::to_string(unnamed), std::move(val));
            ++unnamed;
            continue;
        }

        if (o->has_argument)
        {
            switch (o->type)
            {
            case OptionType::None:
                // Argument with type none?
                res.success = false;
                break;
            case OptionType::String:
            {
                if (name_value.second.empty())
                {
                    ++it;
                    if (it == args.end())
                    {
                        res.success = false;
                        res.items.push_back("Missing argument for '" + o->name + "'");
                    }
                }
                if (res.success)
                {
                    std::unique_ptr<Value<std::string>> val = std::make_unique<Value<std::string>>();
                    if (name_value.second.empty())
                        val->Value = (*it);
                    else
                        val->Value = name_value.second;
                    vals.emplace(o->name, std::move(val));
                }
                break;
            }
            case OptionType::Integer:
            {
                if (name_value.second.empty())
                {
                    ++it;
                    if (it == args.end())
                    {
                        res.success = false;
                        res.items.push_back("Missing argument for '" + o->name + "'");
                    }
                    if (res.success && !is_int(*it))
                    {
                        res.success = false;
                        res.items.push_back("Expected integer type but got '" + (*it) +  + "' for '" + o->name + "'");
                    }
                }
                else
                {
                    if (!is_int(name_value.second))
                    {
                        res.success = false;
                        res.items.push_back("Expected integer type but got '" + name_value.second +  + "' for '" + o->name + "'");
                    }
                }
                if (res.success)
                {
                    std::unique_ptr<Value<int>> val = std::make_unique<Value<int>>();
                    if (name_value.second.empty())
                        val->Value = std::atoi((*it).c_str());
                    else
                        val->Value = std::atoi(name_value.second.c_str());
                    vals.emplace(o->name, std::move(val));
                }
                break;
            }
            case OptionType::Float:
            {
                if (name_value.second.empty())
                {
                    ++it;
                    if (it == args.end())
                    {
                        res.success = false;
                        res.items.push_back("Missing argument for '" + o->name + "'");
                    }
                    if (res.success && !is_float(*it))
                    {
                        res.success = false;
                        res.items.push_back("Expected float type but got '" + (*it) + "' for '" + o->name + "'");
                    }
                }
                else
                {
                    if (!is_float(name_value.second))
                    {
                        res.success = false;
                        res.items.push_back("Expected float type but got '" + name_value.second + "' for '" + o->name + "'");
                    }
                }
                if (res.success)
                {
                    std::unique_ptr<Value<float>> val = std::make_unique<Value<float>>();
                    if (name_value.second.empty())
                        val->Value = static_cast<float>(std::atof((*it).c_str()));
                    else
                        val->Value = static_cast<float>(std::atof(name_value.second.c_str()));
                    vals.emplace(o->name, std::move(val));
                }
                break;
            }
            }
        }
        else
        {
            // No argument -> set this flag to true
            std::unique_ptr<Value<bool>> val = std::make_unique<Value<bool>>();
            val->Value = true;
            vals.emplace(o->name, std::move(val));
        }
        if (!res.success)
            break;
    }

    if (res.success)
    {
        // Check if all mandatory options are given
        int i = 0;
        for (const auto& o : _cli)
        {
            if (!o.mandatory)
                continue;

            if (o.switches.size() != 0)
            {
                if (vals.find(o.name) == vals.end())
                {
                    res.items.push_back("Required argument '" + o.name + "' is missing");
                    res.success = false;
                }
            }
            else
            {
                // Mandatory unnamed argument
                if (vals.find(std::to_string(i)) == vals.end())
                {
                    res.items.push_back("Required positional argument '" + o.name + "' is missing");
                    res.success = false;
                }
                ++i;
            }
        }
    }
    return res;
}

// Parse the arguments according the command line interface and put the
// result in vals
inline Result Parse(int argc, char** argv, const Cli& _cli, Values& vals)
{
    std::vector<std::string> args;
    for (int i = 1; i < argc; ++i)
        args.push_back(argv[i]);

    return Parse(args, _cli, vals);
}

struct Help
{
    std::vector<std::string> lines;
};

enum class HelpFormat
{
    Plain,
    Markdown,
};

// Generate help for the given command line interface
inline Help GetHelp(const std::string& prog, const Cli& arg,
    const std::string description = "",
    const std::string positional = "",
    HelpFormat format = HelpFormat::Plain)
{
    auto get_type_string = [](const Option& o) -> std::string
    {
        switch (o.type)
        {
        case OptionType::None:
            return o.name;
        case OptionType::String:
            return "string";
        case OptionType::Integer:
            return "integer";
        case OptionType::Float:
            return "number";
        default:
            return o.name;
        }
    };

    auto get_heading = [&](const std::string& heading) -> std::string
    {
        switch (format)
        {
        case HelpFormat::Markdown:
            return "## " + heading + "\n";
        default:
            return heading;
        }
    };

    auto get_code = [&](const std::string& value) -> std::string
    {
        switch (format)
        {
        case HelpFormat::Markdown:
            return "`" + value + "`";
        default:
            return value;
        }
    };

    auto get_synopsis = [&]() -> std::string
    {
        std::stringstream synopsis;
        synopsis << prog << " ";
        for (const auto& o : arg)
        {
            std::stringstream sw;
            if (!o.mandatory)
                sw << "[";
            if (o.switches.size() != 0)
            {
                sw << o.switches.front();
                if (o.has_argument)
                    sw << " ";
            }
            if (o.has_argument)
                sw << "<" << o.name << ">";
            if (!o.mandatory)
                sw << "]";
            sw << " ";
            synopsis << sw.str();
        }
        if (!positional.empty())
            synopsis << positional;

        switch (format)
        {
        case HelpFormat::Markdown:
            return "~~~sh\n$ " + synopsis.str() + "\n~~~";
        default:
            return "    " + synopsis.str();
        }
    };

    auto get_switches_line = [&](const std::string& switches)
    {
        switch (format)
        {
        case HelpFormat::Markdown:
            return switches;
        default:
            return "    " + switches;
        }
    };

    Help result;

    if (!description.empty())
    {
        result.lines.push_back(description);
        result.lines.push_back("");
    }
    if (!prog.empty())
    {
        result.lines.push_back(get_heading("SYNOPSIS"));
        result.lines.push_back(get_synopsis());
        result.lines.push_back("");
    }
    result.lines.push_back(get_heading("OPTIONS"));
    std::vector<std::pair<std::string, std::string>> shp;
    size_t maxLength = 0;
    size_t maxHelp = 0;
    for (const auto& o : arg)
    {
        std::string switches;
        if (!o.mandatory)
            switches += "[";
        if (o.switches.size() != 0)
        {
            for (const auto& a : o.switches)
            {
                if (switches.size() > 1)
                    switches += ", ";
                switches += get_code(a);
            }
        }
        else
        {
            switches += o.name;
        }
        if (o.has_argument)
            switches += " <" + get_type_string(o) + ">";
        if (!o.mandatory)
            switches += "]";
        std::string line = get_switches_line(switches);
        shp.push_back({ line, o.help });
        if (maxLength < line.size())
            maxLength = line.size();
        if (maxHelp < o.help.length())
            maxHelp = o.help.length();
    }

    if (format == HelpFormat::Plain)
    {
        bool wrap = maxLength + maxHelp > 72;
        for (const auto& l : shp)
        {
            std::string f = l.first;

            if (wrap)
            {
                // Wrap long lines
                result.lines.push_back(l.first);
                result.lines.push_back("        " + l.second);
            }
            else
            {
                f.insert(f.size(), maxLength - f.size(), ' ');
                result.lines.push_back(f + "   " + l.second);
            }
        }
    }
    else if (format == HelpFormat::Markdown)
    {
        for (const auto& l : shp)
        {
            std::string f = l.first;
            result.lines.push_back("* " + l.first + ": " + l.second);
        }
    }

    return result;
}

template<class _Stream>
inline _Stream& operator << (_Stream& os, const Help& help)
{
    for (const auto& line : help.lines)
    {
        os << line;
        os << '\n';
    }
    return os;
}

template<class _Stream>
inline _Stream& operator << (_Stream& os, const Result& res)
{
    if (!res.success)
    {
        for (const auto& line : res.items)
        {
            os << line;
            os << '\n';
        }
    }
    return os;
}

}
}
