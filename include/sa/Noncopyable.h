/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#define NON_COPYABLE(t)                \
private:                               \
    t(const t&) = delete;              \
    t& operator=(const t&) = delete;

#define NON_MOVEABLE(t)               \
private:                              \
    t(t&&) = delete;                  \
    t& operator=(t&&) = delete;
