/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <unordered_map>
#include <functional>
#include <sa/Assert.h>

namespace sa {

template <typename IndexType, typename ReturnType, typename... _CArgs>
class CallableTable
{
public:
    using FunctionType = std::function<ReturnType(_CArgs...)>;
private:
    std::unordered_map<IndexType, FunctionType> callables_;
public:
    void Add(IndexType index, FunctionType&& f)
    {
        callables_[index] = std::move(f);
    }
    ReturnType Call(IndexType index, _CArgs&& ... _Args)
    {
        ASSERT(Exists(index));
        auto& c = callables_[index];
        return c(std::forward<_CArgs>(_Args)...);
    }
    bool Exists(IndexType index) const
    {
        return callables_.find(index) != callables_.end();
    }
    FunctionType& operator [](IndexType index)
    {
        ASSERT(Exists(index));
        return callables_[index];
    }
};

}
