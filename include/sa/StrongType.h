/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <utility>

namespace sa {

// Example:
// using int_type1 = sa::StrongType<int, struct IntTag1>;
// using int_type2 = sa::StrongType<int, struct IntTag2>;

template <typename T, typename Tag>
struct StrongType
{
    constexpr StrongType() noexcept = default;
    constexpr StrongType(const T& value) noexcept :
        value_(value)
    { }
    StrongType(T&& value) noexcept :
        value_(std::move(value))
    { }
    operator T& () { return value_; }
    operator const T& () const { return value_; }
private:
    T value_;
};

}
