/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <cctype>
#include <sa/Compiler.h>

namespace sa {

class path
{
private:
    std::string path_;
public:
    path() = default;
    path(const std::string& path) :
        path_(path)
    { }
    path(const char* path) :
        path_(path)
    { }
    // No trailing slash
    [[nodiscard]] path directory() const
    {
        size_t pos = path_.find_last_of("\\/");
        return path(path_.substr(0, pos));
    }
    void set_directory(const std::string& dir)
    {
        path_ = (path(dir) / path(filename())).string();
    }
    [[nodiscard]] std::string filename() const
    {
        size_t pos = path_.find_last_of("\\/");
        if (pos != std::string::npos)
            return path_.substr(pos + 1);
        return path_;
    }
    void set_filename(const std::string& fn)
    {
        path_ = (directory() / path(fn)).string();
    }
    [[nodiscard]] std::string ext() const
    {
        size_t pos = path_.find_last_of('.');
        if (pos != std::string::npos)
            return path_.substr(pos);
        return "";
    }
    void set_ext(const std::string& ext)
    {
        size_t pos = path_.find_last_of('.');
        if (pos != std::string::npos)
            path_ = path_.substr(0, pos) + ext;
        else
            path_ += ext;
    }
    path operator + (const path& rhs) const
    {
        return path(path_ + rhs.path_);
    }
    path operator + (const std::string& rhs) const
    {
        return path(path_ + rhs);
    }
    path operator / (const path& rhs) const
    {
        path result(path_);
        if (result.path_.back() != '/' && result.path_.back() != '\\' && rhs.path_.front() != '/' && rhs.path_.front() != '\\')
            result.path_ += "/";
        result.path_ += rhs.path_;
        return result;
    }
    bool operator != (const path& rhs) const
    {
        return !(*this == rhs);
    }
    bool operator == (const path& rhs) const
    {
#ifdef SA_PLATFORM_WIN
        return path_.size() == rhs.path_.size()
            && std::equal(path_.cbegin(), path_.cend(), rhs.path_.cbegin(),
                [](std::string::value_type l1, std::string::value_type r1)
        {
            return toupper(l1) == toupper(r1);
        });
#else
        return path_ == rhs.path_;
#endif
    }
    [[nodiscard]] const std::string& string() const { return path_; }
};

}
