/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <utility>
#include <algorithm>
#include <random>

namespace sa {

template <typename ForwardIterator, typename OutputIterator, typename UnaryPredicate>
inline void SelectIterators(ForwardIterator first, ForwardIterator last,
    OutputIterator out, UnaryPredicate pred)
{
    while (first != last)
    {
        if (pred(*first))
            *out++ = first;
        ++first;
    }
}

template<typename Iter, typename RandomGenerator>
Iter SelectRandomly(Iter start, Iter end, RandomGenerator& g)
{
    std::uniform_int_distribution<> dis(0, static_cast<int>(std::distance(start, end)) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter>
Iter SelectRandomly(Iter start, Iter end)
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return SelectRandomly(start, end, gen);
}

template<typename Iter>
Iter SelectRandomly(Iter start, Iter end, float rnd)
{
    int adv = static_cast<int>(round(static_cast<float>(std::distance(start, end) - 1) * rnd));
    std::advance(start, adv);
    return start;
}

template<typename T>
void DeleteDuplicates(T& contanier)
{
    std::sort(contanier.begin(), contanier.end());
    contanier.erase(std::unique(contanier.begin(), contanier.end()), contanier.end());
}

}
