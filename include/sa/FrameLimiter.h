/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <utility>
#include <cstdint>
#include <chrono>

namespace sa {

// RAII Frame limiter
template<typename Callback>
class [[nodiscard]] FrameLimiter
{
public:
    FrameLimiter(int32_t maxFPS, Callback callback) :
        maxFPS_(maxFPS),
        callback_(std::move(callback)),
        start_(GetTick())
    { }
    ~FrameLimiter()
    {
        if (callback_)
        {
            const int64_t stop = GetTick();
            int64_t frameTime = 0;
            if (stop > start_)
                frameTime = (stop - start_);
            const int64_t targetMax = 1000000LL / maxFPS_;
            const int64_t sleep = ((targetMax - frameTime) / 1000LL);
            if (sleep > 0)
                callback_(sleep);
        }
    }
private:
    static int64_t GetTick()
    {
        using namespace std::chrono;
        const milliseconds ms = duration_cast<milliseconds>(
            system_clock::now().time_since_epoch());
        return ms.count();
    }
    int32_t maxFPS_;
    Callback callback_;
    int64_t start_;
};

}
