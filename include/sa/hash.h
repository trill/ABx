/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string_view>
#include <charconv>
#include <iomanip>
#include <stdint.h>
#include <cstring>
#include <string>
#include <sstream>

namespace sa {

template<typename T, size_t Count>
class Hash
{
    static_assert(Count != 0);
private:
    T data_[Count]{};
public:
    Hash() = default;
    // Construct from Hex string
    explicit Hash(const std::string_view value)
    {
        constexpr size_t charsPerNum = sizeof(T) * 2;
        if (value.size() == charsPerNum * Count)
        {
            for (size_t i = 0; i < Count; ++i)
            {
                std::from_chars(
                    value.data() + (i * charsPerNum),
                    value.data() + (i * charsPerNum) + charsPerNum,
                    data_[i],
                    16
                );
            }
        }
    }
    Hash(const Hash& rhs)
    {
        std::copy(&rhs.data_[0], &rhs.data_[0] + Count * sizeof(T), &data_[0]);
    }
    Hash& operator=(const Hash& rhs)
    {
        if (&rhs == this)
            return *this;
        std::copy(&rhs.data_[0], &rhs.data_[0] + Count * sizeof(T), &data_[0]);
        return *this;
    }
    bool operator ==(const Hash& rhs) const
    {
        return memcmp(&data_[0], &rhs.data_[0], Count * sizeof(T)) == 0;
    }
    bool operator !=(const Hash& rhs) const
    {
        return !(*this == rhs);
    }
    friend std::ostream& operator << (std::ostream& os, const Hash& value)
    {
        const uint8_t* buff = reinterpret_cast<const uint8_t*>(value.Data());
        constexpr size_t c = sizeof(value.data_);
        for (size_t i = 0; i < c; ++i)
        {
            os << std::setfill('0') << std::setw(2) << std::hex << (0xff & (unsigned int)buff[i]);
        }
        return os;
    }
    // To Hex string
    [[nodiscard]] std::string ToString() const
    {
        std::stringstream ss;
        ss << *this;
        return ss.str();
    }
    [[nodiscard]] bool IsEmpty() const
    {
        for (size_t i = 0; i < Count; ++i)
        {
            if (data_[i] != 0)
                return false;
        }
        return true;
    }
    [[nodiscard]] explicit operator bool() const
    {
        return !IsEmpty();
    }

    const T* Data() const { return &data_[0]; }
    T* Data() { return &data_[0]; }
    [[nodiscard]] constexpr size_t Size() const { return Count * sizeof(T); }
};

}
