/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <utility>

namespace sa {

template <typename Callback>
class [[nodiscard]] ScopeGuard
{
private:
    Callback callback_;
public:
    explicit ScopeGuard(Callback&& callback) :
        callback_(std::move(callback))
    { }
    ~ScopeGuard()
    {
        callback_();
    }
};

template <typename Callback> ScopeGuard(Callback&&) -> ScopeGuard<Callback>;

}
