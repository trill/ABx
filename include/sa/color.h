/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <tuple>
#include <limits>
#include <string>
#include <string_view>
#include <sstream>
#include <charconv>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <sa/math.h>

// HSL, HSV <-> RGB
// https://github.com/ratkins/RGBConverter/blob/master/RGBConverter.cpp
// https://en.wikipedia.org/wiki/HSL_and_HSV
namespace sa {

namespace details {

constexpr float hue2rgb(float p, float q, float t)
{
    if (t < 0.0f) t += 1.0f;
    if (t > 1.0f) t -= 1.0f;
    if (t < 1.0f / 6.0f)
        return p + (q - p) * 6.0f * t;
    if (t < 1.0f / 2.0f)
        return q;
    if (t < 2.0f / 3.0f)
        return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
    return p;
}

}

class Color
{
public:
    // r, g, b, a in range 0..1
    static constexpr Color FromRgb(float r, float g, float b, float a = 1.0f)
    {
        return { static_cast<uint8_t>(std::clamp(r, 0.0f, 1.0f) * 255.0f),
            static_cast<uint8_t>(std::clamp(g, 0.0f, 1.0f) * 255.0f),
            static_cast<uint8_t>(std::clamp(b, 0.0f, 1.0f) * 255.0f),
            static_cast<uint8_t>(std::clamp(a, 0.0f, 1.0f) * 255.0f) };
    }
    static constexpr Color From32(uint32_t value)
    {
        const int a = (value >> 24u) & 0xffu;
        const int b = (value >> 16u) & 0xffu;
        const int g = (value >> 8u) & 0xffu;
        const int r = (value >> 0u) & 0xffu;
        return { (uint8_t)r, (uint8_t)g, (uint8_t)b, (uint8_t)a };
    }
    static constexpr Color From24(uint32_t value)
    {
        const int a = 255;
        const int b = (value >> 16u) & 0xffu;
        const int g = (value >> 8u) & 0xffu;
        const int r = (value >> 0u) & 0xffu;
        return { (uint8_t)r, (uint8_t)g, (uint8_t)b, (uint8_t)a };
    }
    // h, s, l must be in 0..1
    static constexpr Color FromHsl(float h, float s, float l)
    {
        float r, g, b;
        if (math::Equals(s, 0.0f))
            r = g = b = 1;
        else
        {
            const float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
            const float p = 2.0f * l - q;
            r = details::hue2rgb(p, q, h + 1.0f / 3.0f);
            g = details::hue2rgb(p, q, h);
            b = details::hue2rgb(p, q, h - 1.0f / 3.0f);
        }

        return Color::FromRgb(r, g, b);
    }
    // h, s, v must be in 0..1
    static constexpr Color FromHsv(float h, float s, float v)
    {
        float r, g, b;
        int i = static_cast<int>(h * 6.0f);
        const float f = h * 6.0f - i;
        const float p = v * (1.0f - s);
        const float q = v * (1.0f - f * s);
        const float t = v * (1.0f - (1.0f - f) * s);


        switch (i % 6)
        {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
        }
        return Color::FromRgb(r, g, b);
    }
    // Parse Hex string
    static Color FromString(std::string_view value)
    {
        size_t start = value.front() == '#' ? 1 : 0;
        if ((value.length() - start) % 2 != 0)
            return {};

        const size_t count = (value.length() - start) / 2;
        uint8_t r = 0, g = 0, b = 0, a = 255;
        std::from_chars(
            value.data() + (start * 2),
            value.data() + (start * 2) + 2,
            r,
            16
        );
        if (count > 1)
        {
            ++start;
            std::from_chars(
                value.data() + (start * 2),
                value.data() + (start * 2) + 2,
                g,
                16
            );
        }
        if (count > 2)
        {
            ++start;
            std::from_chars(
                value.data() + (start * 2),
                value.data() + (start * 2) + 2,
                b,
                16
            );
        }
        if (count > 3)
        {
            ++start;
            std::from_chars(
                value.data() + (start * 2),
                value.data() + (start * 2) + 2,
                a,
                16
            );
        }
        return { r, g, b, a };
    }
    constexpr bool operator==(const Color& rhs) const { return r_ == rhs.r_ && g_ == rhs.g_ && b_ == rhs.b_ && a_ == rhs.a_; }
    constexpr bool operator!=(const Color& rhs) const { return r_ != rhs.r_ || g_ != rhs.g_ || b_ != rhs.b_ || a_ != rhs.a_; }
    constexpr Color operator+(const Color& rhs) const
    {
        const int r = (r_ + rhs.r_);
        const int g = (g_ + rhs.g_);
        const int b = (b_ + rhs.b_);
        const int a = (a_ + rhs.a_);

        return { (uint8_t)(r < 256 ? (uint8_t)r : 255),
            (uint8_t)(g < 256 ? (uint8_t)g : 255),
            (uint8_t)(b < 256 ? (uint8_t)b : 255),
            (uint8_t)(a < 256 ? (uint8_t)a : 255) };
    }
    constexpr Color& operator+=(const Color& rhs)
    {
        const int r = (r_ + rhs.r_);
        const int g = (g_ + rhs.g_);
        const int b = (b_ + rhs.b_);
        const int a = (a_ + rhs.a_);
        r_ = (uint8_t)(r < 256 ? (uint8_t)r : 255);
        g_ = (uint8_t)(g < 256 ? (uint8_t)g : 255);
        b_ = (uint8_t)(b < 256 ? (uint8_t)b : 255);
        a_ = (uint8_t)(a < 256 ? (uint8_t)a : 255);

        return *this;
    }
    constexpr Color operator-(const Color& rhs) const
    {
        const int r = (r_ - rhs.r_);
        const int g = (g_ - rhs.g_);
        const int b = (b_ - rhs.b_);
        const int a = (a_ - rhs.a_);

        return { (uint8_t)(r >= 0 ? (uint8_t)r : 0),
            (uint8_t)(g >= 0 ? (uint8_t)g : 0),
            (uint8_t)(b >= 0 ? (uint8_t)b : 0),
            (uint8_t)(a >= 0 ? (uint8_t)a : 0) };
    }
    constexpr Color& operator-=(const Color& rhs)
    {
        const int r = (r_ - rhs.r_);
        const int g = (g_ - rhs.g_);
        const int b = (b_ - rhs.b_);
        const int a = (a_ - rhs.a_);
        r_ = (uint8_t)(r >= 0 ? (uint8_t)r : 0);
        g_ = (uint8_t)(g >= 0 ? (uint8_t)g : 0);
        b_ = (uint8_t)(b >= 0 ? (uint8_t)b : 0);
        a_ = (uint8_t)(a >= 0 ? (uint8_t)a : 0);

        return *this;
    }
    constexpr Color operator*(const Color& rhs) const
    {
        const int r = std::clamp((int)(r_ * rhs.r_), 0, 255);
        const int g = std::clamp((int)(g_ * rhs.g_), 0, 255);
        const int b = std::clamp((int)(b_ * rhs.b_), 0, 255);
        const int a = std::clamp((int)(a_ * rhs.a_), 0, 255);

        return { (uint8_t)r,
            (uint8_t)g,
            (uint8_t)b,
            (uint8_t)a };
    }
    constexpr Color& operator*=(const Color& rhs)
    {
        r_ = std::clamp((int)(r_ * rhs.r_), 0, 255);
        g_ = std::clamp((int)(g_ * rhs.g_), 0, 255);
        b_ = std::clamp((int)(b_ * rhs.b_), 0, 255);
        a_ = std::clamp((int)(a_ * rhs.a_), 0, 255);

        return *this;
    }
    constexpr Color operator*(float rhs) const
    {
        const int r = std::clamp((int)(r_ * rhs), 0, 255);
        const int g = std::clamp((int)(g_ * rhs), 0, 255);
        const int b = std::clamp((int)(b_ * rhs), 0, 255);
        const int a = std::clamp((int)(a_ * rhs), 0, 255);

        return { (uint8_t)r,
            (uint8_t)g,
            (uint8_t)b,
            (uint8_t)a };
    }
    constexpr Color operator/(const Color& rhs) const
    {
        const int r = std::clamp((int)(r_ / rhs.r_), 0, 255);
        const int g = std::clamp((int)(g_ / rhs.g_), 0, 255);
        const int b = std::clamp((int)(b_ / rhs.b_), 0, 255);
        const int a = std::clamp((int)(a_ / rhs.a_), 0, 255);

        return { (uint8_t)r,
            (uint8_t)g,
            (uint8_t)b,
            (uint8_t)a };
    }
    constexpr Color operator/(float rhs) const
    {
        const int r = std::clamp((int)(r_ / rhs), 0, 255);
        const int g = std::clamp((int)(g_ / rhs), 0, 255);
        const int b = std::clamp((int)(b_ / rhs), 0, 255);
        const int a = std::clamp((int)(a_ / rhs), 0, 255);

        return { (uint8_t)r,
            (uint8_t)g,
            (uint8_t)b,
            (uint8_t)a };
    }
    explicit constexpr operator uint32_t() const
    {
        return To32();
    }
    // RGBA
    constexpr uint32_t To32() const
    {
        return (a_ << 24u) | (b_ << 16u) | (g_ << 8u) | r_;
    }
    // RGB without A
    constexpr uint32_t To24() const
    {
        return (b_ << 16u) | (g_ << 8u) | r_;
    }

    constexpr std::tuple<float, float, float> ToRgb() const
    {
        return { (float)r_ / 255.0f, (float)g_ / 255.0f, (float)b_ / 255.0f };
    }
    constexpr std::tuple<float, float, float, float> ToRgba() const
    {
        return { (float)r_ / 255.0f, (float)g_ / 255.0f, (float)b_ / 255.0f, (float)a_ / 255.0f };
    }

    // Returns h, s, l in range 0..1
    constexpr std::tuple<float, float, float> ToHsl() const
    {
        const float r = (float)r_ / 255.0f;
        const float g = (float)g_ / 255.0f;
        const float b = (float)b_ / 255.0f;

        const float min = std::min(r, std::min(g, b));
        const float max = std::max(r, std::max(g, b));
        float h = 0.0f, s = 0.0f, l = (max + min) * 0.5f;
        if (math::Equals(max, min))
            h = s = 0;
        else
        {
            const float d = max - min;
            s = l > 0.5f ? d / (2.0f - max - min) : d / (max - min);
            if (math::Equals(max, r))
                h = (g - b) / d + (g < b ? 6.0f : 0.0f);
            else if (math::Equals(max, g))
                h = (b - r) / d + 2.0f;
            else if (math::Equals(max, b))
                h = (r - g) / d + 4.0f;

            h /= 6;
        }
        return { h, s, l };
    }
    // Returns h, s, v in range 0..1
    constexpr std::tuple<float, float, float> ToHsv() const
    {
        const float r = (float)r_ / 255.0f;
        const float g = (float)g_ / 255.0f;
        const float b = (float)b_ / 255.0f;

        const float min = std::min(r, std::min(g, b));
        const float max = std::max(r, std::max(g, b));
        float h = 0.0f, s = 0.0f, v = max;
        const float d = max - min;
        s = math::Equals(max, 0.0f) ? 0.0f : d / max;
        if (math::Equals(max, min))
            h = 0;
        else
        {
            if (math::Equals(max, r))
                h = (g - b) / d + (g < b ? 6.0f : 0.0f);
            else if (math::Equals(max, g))
                h = (b - r) / d + 2.0f;
            else if (math::Equals(max, b))
                h = (r - g) / d + 4.0f;

            h /= 6;
        }
        return { h, s, v };
    }

    constexpr float Hue() const
    {
        const auto [h, s, l] = ToHsl();
        return h;
    }
    constexpr void SetHue(float value)
    {
        const uint8_t a = a_;
        auto [h, s, l] = ToHsl();
        h = value;
        *this = FromHsl(h, s, l);
        this->a_ = a;
    }
    constexpr float Saturation() const
    {
        const auto [h, s, l] = ToHsl();
        return s;
    }
    constexpr void SetSaturation(float value)
    {
        const uint8_t a = a_;
        auto [h, s, l] = ToHsl();
        s = value;
        *this = FromHsl(h, s, l);
        this->a_ = a;
    }
    constexpr float Lightness() const
    {
        const auto [h, s, l] = ToHsl();
        return s;
    }
    constexpr void SetLightness(float value)
    {
        const uint8_t a = a_;
        auto [h, s, l] = ToHsl();
        l = value;
        *this = FromHsl(h, s, l);
        this->a_ = a;
    }
    constexpr float Value() const
    {
        const auto [h, s, v] = ToHsv();
        return v;
    }
    constexpr void SetValue(float value)
    {
        const uint8_t a = a_;
        auto [h, s, v] = ToHsv();
        v = value;
        *this = FromHsv(h, s, v);
        this->a_ = a;
    }

    // Apply a filter.
    // \param f The filter class
    // \param Arguments Additional arguments passed to the filters () operator. The first argument is always this.
    template<typename F, typename... ArgTypes>
    void Filter(const F& f, ArgTypes&& ... Arguments)
    {
        *this = f(*this, std::forward<ArgTypes>(Arguments)...);
    }
    // Apply a filter.
    // \param f The filter class
    // \param Arguments Additional arguments passed to the filters () operator. The first argument is always this.
    // \return The filtered color
    template<typename F, typename... ArgTypes>
    Color Filtered(const F& f, ArgTypes&& ... Arguments) const
    {
        return f(*this, std::forward<ArgTypes>(Arguments)...);
    }

    // To Hex string
    std::string ToString() const
    {
        std::stringstream ss;
        ss << std::hex;
        ss << std::setw(2) << std::setfill('0') << (int)r_ <<
            std::setw(2) << std::setfill('0') << (int)g_ <<
            std::setw(2) << std::setfill('0') << (int)b_ <<
            std::setw(2) << std::setfill('0') << (int)a_;
        return ss.str();
    }

    uint8_t r_{ 0 };
    uint8_t g_{ 0 };
    uint8_t b_{ 0 };
    uint8_t a_{ 255 };
};

namespace filter {

struct Lerp
{
    Color operator ()(const Color& lhs, const Color& rhs) const
    {
        const float invt = 1.0f - t;
        return {
            (uint8_t)((float)lhs.r_ * invt + (float)rhs.r_ * t),
            (uint8_t)((float)lhs.g_ * invt + (float)rhs.g_ * t),
            (uint8_t)((float)lhs.b_ * invt + (float)rhs.b_ * t),
            lerp_alpha ? (uint8_t)((float)lhs.a_ * invt + (float)rhs.a_ * t) : lhs.a_
        };
    }
    float t;
    bool lerp_alpha = false;
};

struct Tint
{
    constexpr Tint(float f, float mi = 0.0f, float ma = 1.0f) :
        factor(f),
        min(mi),
        max(ma)
    {
        val = (((factor - min) * 100.0f) / (max - min)) * 0.01f;
    }
    Color operator ()(const Color& lhs) const
    {
        const int r = lhs.r_ + static_cast<int>((float)(255 - lhs.r_) * factor);
        const int g = lhs.g_ + static_cast<int>((float)(255 - lhs.g_) * factor);
        const int b = lhs.b_ + static_cast<int>((float)(255 - lhs.b_) * factor);
        return { (uint8_t)std::clamp(r, 0, 255),
            (uint8_t)std::clamp(g, 0, 255),
            (uint8_t)std::clamp(b, 0, 255),
            lhs.a_ };
    }
private:
    float factor;
    float min = 0.0f;
    float max = 1.0f;
    float val;
};

struct Shade
{
    constexpr Shade(float f, float mi = 0.0f, float ma = 1.0f) :
        factor(f),
        min(mi),
        max(ma)
    {
        val = (((factor - min) * 100.0f) / (max - min)) * 0.01f;
    }
    Color operator ()(const Color& lhs) const
    {
        const int r = static_cast<int>((float)lhs.r_ * (1.0f - factor));
        const int g = static_cast<int>((float)lhs.g_ * (1.0f - factor));
        const int b = static_cast<int>((float)lhs.b_ * (1.0f - factor));
        return { (uint8_t)std::clamp(r, 0, 255),
            (uint8_t)std::clamp(g, 0, 255),
            (uint8_t)std::clamp(b, 0, 255),
            lhs.a_ };
    }
private:
    float factor;
    float min = 0.0f;
    float max = 1.0f;
    float val;
};

struct Invert
{
    Color operator ()(const Color& lhs) const
    {
        return { (uint8_t)(255 - lhs.r_),
            (uint8_t)(255 - lhs.g_),
            (uint8_t)(255 - lhs.b_),
            (uint8_t)(invert_alpha ? 255 - lhs.a_ : lhs.a_) };
    }
    bool invert_alpha = false;
};

struct GreyScale
{
    Color operator ()(const Color& lhs) const
    {
        const float fr = (float)lhs.r_;
        const float fg = (float)lhs.g_;
        const float fb = (float)lhs.b_;
        const float v = sqrt((fr * fr + fg * fg + fb * fb) / 3.0f);
        return { (uint8_t)v,
            (uint8_t)v,
            (uint8_t)v,
            lhs.a_ };
    }
};

struct Scale
{
    constexpr Scale(float v, float mi = 0.0f, float ma = 1.0f, bool inv = false) :
        value(v),
        min(mi),
        max(ma),
        inverted(inv)
    {
        const float half = (max - min) / 2.0f;
        val = ((((value - min) * 100.0f) / (max - min)) - half) * 0.01f;
    }
    Color operator ()(const Color& lhs) const
    {
        if (!inverted)
        {
            if (val < 0.0f)
                return lhs.Filtered<Shade>({ -val, min, max });
            if (val > 0.0f)
                return lhs.Filtered<Tint>({ val, min, max });
        }
        if (val < 0.0f)
            return lhs.Filtered<Tint>({ -val, min, max });
        if (val > 0.0f)
            return lhs.Filtered<Shade>({ val, min, max });
        return lhs;
    }
private:
    float value;
    float min;
    float max;
    bool inverted = false;
    float val;
};

struct AlphaBlend
{
    Color operator ()(const Color& lhs, const Color& rhs) const
    {
        const float alpha = (float)rhs.a_ / 255.0f;
        const uint8_t r = (uint8_t)(((float)rhs.r_ * alpha) + ((1.0f - alpha) * (float)lhs.r_));
        const uint8_t g = (uint8_t)(((float)rhs.g_ * alpha) + ((1.0f - alpha) * (float)lhs.g_));
        const uint8_t b = (uint8_t)(((float)rhs.b_ * alpha) + ((1.0f - alpha) * (float)lhs.b_));
        return { r, g, b, keep_alpha ? lhs.a_ : rhs.a_ };
    }
    bool keep_alpha = false;
};

}

}
