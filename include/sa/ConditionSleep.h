/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <thread>
#include <chrono>

namespace sa {

/// This thread sleeps until pred returns true. Returns true when pred returned true, and false when timed out.
template<typename Predicate>
bool ConditionSleep(Predicate&& pred, unsigned maxWait = 0)
{
    using namespace std::chrono_literals;
    unsigned waited = 0;
    while (!pred())
    {
        std::this_thread::sleep_for(100ms);
        waited += 100;
        if (maxWait != 0 && waited >= maxWait)
            return false;
    }
    return true;
}

}
