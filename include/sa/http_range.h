/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stddef.h>
#include <string_view>
#include <vector>
#include <utility>
#include <sstream>
#include <optional>
#include <algorithm>
#include <sa/StringTempl.h>
#include <sa/Compiler.h>
#if defined(SA_MSVC)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#elif defined(SA_PLATFORM_LINUX)
#include <sys/types.h>
#endif

// Parse HTTP Range header
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range

namespace sa {
namespace http {

struct Range
{
    size_t start{ 0 };
    size_t end{ 0 };
    ssize_t length{ 0 };
    bool operator > (const Range& rhs) const { return start > rhs.start; }
};

using Ranges = std::vector<Range>;

namespace details {

inline bool ParseRange(size_t size, std::string_view value, Range& result)
{
    auto dash_sign = value.find('-');
    if (dash_sign == std::string_view::npos)
        return false;
    std::string_view from = value.substr(0, dash_sign);
    std::string_view to = value.substr(dash_sign + 1);
    if (from.length() == 0 && to.length() == 0)
        return false;
    auto from_value = sa::ToNumber<size_t>(from);
    auto to_value = sa::ToNumber<size_t>(to);
    if (!from_value.has_value() && !to_value.has_value())
        return false;

    if (from_value.has_value())
        result.start = from_value.value();
    if (to_value.has_value())
        result.end = to_value.value();

    if (!from_value.has_value())
    {
        result.start = size - to_value.value();
        result.end = size;
    }
    else if (!to_value.has_value())
        result.end = size;
    result.length = result.end - result.start;
    return result.length > 0;
}

inline bool RangesOverlap(const Range& a, const Range& b)
{
    return a.end > b.start;
}

inline void CombineRanges(Ranges& input)
{
    // Combine overlapping ranges into one bigger range
    Ranges result(input);
    std::sort(result.begin(), result.end(), [](const Range& a, const Range& b) -> bool
    {
        return b > a;
    });

    for (ssize_t i = result.size() - 1; i > 0; --i)
    {
        auto& b = result[i];
        auto& a = result[i - 1];
        if (RangesOverlap(a, b))
        {
            a.start = std::min(a.start, b.start);
            a.end = std::max(a.end, b.end);
            a.length = a.end - a.start;
            result.erase(result.begin() + i);
        }
    }
    input = result;
}

}

inline bool IsFullRange(size_t size, const Range& range)
{
    if (range.end == 0)
        return true;
    if (range.start == 0 && (size_t)range.length >= size)
        return true;
    return false;
}

inline size_t ContentLength(const Ranges& ranges)
{
    size_t result = 0;
    for (const auto& r : ranges)
        result += r.length;
    return result;
}

// Parse Range header, return a sorted list of ranges that do not overlap.
// size is the file size in bytes.
inline bool ParseRanges(size_t size, std::string_view header, Ranges& result)
{
    auto equal_sign = header.find('=');
    if (equal_sign == std::string_view::npos)
        return false;
    std::string_view units = header.substr(0, equal_sign);
    if (units != "bytes")
        // Only support bytes unit
        return false;
    std::string_view header_value = header.substr(equal_sign + 1);

    std::vector<std::string> values = sa::Split(std::string(header_value), ",", false, false);
    for (auto& value : values)
    {
        value = sa::Trim<char>(value);
        Range range_value;
        if (details::ParseRange(size, value, range_value))
            result.push_back(std::move(range_value));
    }
    if (!result.empty())
        details::CombineRanges(result);

    // If it does not contain any valid ranges, it is clearly not a valid ranges header.
    return !result.empty();
}

inline std::string ToString(const Ranges& ranges)
{
    std::stringstream ss;
    ss << "bytes=";
    size_t i = 0;
    for (const auto& range : ranges)
    {
        ++i;
        ss << range.start << "-";
        if (range.end != 0)
            ss << range.end;
        if (i < ranges.size())
            ss << ", ";
    }
    return ss.str();
}

inline std::string ToString(const Range& range)
{
    std::stringstream ss;
    ss << "bytes=";
    ss << range.start << "-";
    if (range.end != 0)
        ss << range.end;
    return ss.str();
}

}
}
