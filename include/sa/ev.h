/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <string_view>
#include <functional>
#include <variant>
#include <optional>
#include <string>
#include <vector>
#include <cmath>
#include <memory>
#include <sstream>
#include <charconv>
#include <random>
#include <algorithm>
#include <limits>
#include <array>

namespace sa::ev {

namespace builtins {
inline constexpr double E = 2.71828182845904523536;
inline constexpr double PI = 3.14159265358979323846;
}
namespace details {
template <typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

}
struct Error
{
    enum class Type
    {
        SyntaxError,
        RuntimeError
    };
    Type type;
    size_t pos;
    std::string message;
    friend std::ostream& operator << (std::ostream& os, const Error& value)
    {
        if (value.type == Error::Type::SyntaxError)
            os << "Syntax Error";
        else
            os << "Runtime Error";
        os << "(" << value.pos << "): " << value.message;
        return os;
    }
};

template<typename T>
class Context
{
private:
    using Function0 = std::function<T()>;
    using Function1 = std::function<T(T)>;
    using Function2 = std::function<T(T, T)>;
    using Function3 = std::function<T(T, T, T)>;
    using Function4 = std::function<T(T, T, T, T)>;
    using FunctionTypes = std::variant<Function0, Function1, Function2, Function3, Function4>;
public:
    Context()
    {
        constants["e"] = static_cast<T>(builtins::E);
        if constexpr (std::numeric_limits<T>::has_infinity)
        {
            constants["inf"] = std::numeric_limits<T>::infinity();
        }
        constants["pi"] = static_cast<T>(builtins::PI);

        functions["abs"] = [](T v) -> T { return static_cast<T>(std::abs(static_cast<double>(v))); };
        functions["acos"] = [](T v) -> T { return static_cast<T>(std::acos(static_cast<double>(v))); };
        functions["asin"] = [](T v) -> T { return static_cast<T>(std::asin(static_cast<double>(v))); };
        functions["atan"] = [](T v) -> T { return static_cast<T>(std::atan(static_cast<double>(v))); };
        functions["atan2"] = [](T a, T b) -> T { return static_cast<T>(std::atan2(static_cast<double>(a), static_cast<double>(b))); };
        functions["ceil"] = [](T v) -> T
        {
            if constexpr (std::is_integral<T>::value)
                return v;
            else
                return static_cast<T>(std::ceil(static_cast<double>(v)));
        };
        functions["clamp"] = [](T v, T min, T max) -> T { return std::clamp<T>(v, min, max); };
        functions["cos"] = [](T v) -> T { return static_cast<T>(std::cos(static_cast<double>(v))); };
        functions["cosh"] = [](T v) -> T { return static_cast<T>(std::cosh(static_cast<double>(v))); };
        functions["exp"] = [](T v) -> T { return static_cast<T>(std::exp(static_cast<double>(v))); };
        functions["fac"] = [](T v) -> T
        {
            if (v < static_cast<T>(0))
                return std::numeric_limits<T>::quiet_NaN();
            if (static_cast<unsigned int>(v) > std::numeric_limits<unsigned int>::max())
                return std::numeric_limits<T>::infinity();
            const auto ua = static_cast<size_t>(v);
            unsigned long int result{ 1 }, i{ 1 };
            for (i = 1; i <= ua; i++) {
                if (i > std::numeric_limits<unsigned long>::max() / result)
                    return std::numeric_limits<T>::infinity();
                result *= i;
            }
            return static_cast<T>(result);
        };
        functions["floor"] = [](T v) -> T
        {
            if constexpr (std::is_integral<T>::value)
                return v;
            else
                return static_cast<T>(std::floor(static_cast<double>(v)));
        };
        functions["ln"] = [](T v) -> T { return static_cast<T>(std::log(static_cast<double>(v))); };
        functions["log10"] = [](T v) -> T { return static_cast<T>(std::log10(v)); };
        functions["max"] = [](T a, T b) -> T { return std::max<T>(a, b); };
        functions["min"] = [](T a, T b) -> T { return std::min<T>(a, b); };
        functions["pow"] = [](T a, T b) -> T { return static_cast<T>(std::pow(a, b)); };
        functions["rnd"] = []() -> T
        {
            std::random_device rd;
            std::mt19937 gen(rd());
            if constexpr (std::is_integral<T>::value)
            {
                // For integral types return 0..100
                std::uniform_int_distribution<> distr(0, 100);
                return static_cast<T>(distr(gen));
            }
            else
            {
                // For floats 0..1
                std::uniform_real_distribution<> distr(0, 1);
                return static_cast<T>(distr(gen));
            }
        };
        functions["sign"] = [](T v) -> T
        {
            if (v < 0)
                return -1;
            if (details::Equals(v, static_cast<T>(0)))
                return 0;
            return 1;
        };
        functions["sin"] = [](T v) -> T { return static_cast<T>(std::sin(static_cast<double>(v))); };
        functions["sinh"] = [](T v) -> T { return static_cast<T>(std::sinh(static_cast<double>(v))); };
        functions["sqrt"] = [](T v) -> T { return static_cast<T>(std::sqrt(static_cast<double>(v))); };
        functions["tan"] = [](T v) -> T { return static_cast<T>(std::tan(static_cast<double>(v))); };
        functions["tanh"] = [](T v) -> T { return static_cast<T>(std::tanh(static_cast<double>(v))); };
        functions["trunc"] = [](T v) -> T
        {
            if constexpr (std::is_integral<T>::value)
                return v;
            else
                return static_cast<T>(std::trunc(static_cast<double>(v)));
        };
        if constexpr (std::is_floating_point<T>::value)
        {
            functions["fract"] = [](T v) -> T
            {
                return static_cast<T>(static_cast<double>(v) - std::floor(static_cast<double>(v)));
            };
            functions["lerp"] = [](T lhs, T rhs, T i) -> T
            {
                return lhs * ((static_cast<T>(1.0)) - i) + rhs * i;
            };
            // Deg to Rad
            functions["rad"] = [](T v) -> T
            {
                return v * (static_cast<T>(builtins::PI / 180.0));
            };
            // Rad to Deg
            functions["deg"] = [](T v) -> T
            {
                return (v / static_cast<T>(builtins::PI)) * (static_cast<T>(180.0));
            };
        }
    }
    bool HasConstant(const std::string& name) const
    {
        return constants.contains(name);
    }
    std::optional<T> GetConstant(const std::string& name) const
    {
        const auto it = constants.find(name);
        if (it == constants.end())
            return {};
        return it->second;
    }
    bool HasVariable(const std::string& name) const
    {
        return variables.contains(name);
    }
    std::optional<const T*> GetVariable(const std::string& name) const
    {
        const auto it = variables.find(name);
        if (it == variables.end())
            return {};
        return it->second;
    }
    template<typename... Args>
    std::optional<T> CallFunction(const std::string& name, Args... args) const
    {
        const auto it = functions.find(name);
        if (it == functions.end())
            return {};
        const auto& var = it->second;
        constexpr size_t nArguments = sizeof...(Args);
        if (var.index() == nArguments)
        {
            const auto& func = std::get<nArguments>(var);
            return func(std::forward<Args>(args)...);
        }
        errors.emplace_back(Error::Type::RuntimeError, 0, "Wrong number of arguments for function \"" + name + "\", expected " +
            std::to_string(var.index()) + " got " + std::to_string(nArguments));
        return {};
    }
    bool HasFunction(const std::string& name) const
    {
        return functions.contains(name);
    }
    std::unordered_map<std::string, T> constants;
    std::unordered_map<std::string, const T*> variables;
    std::unordered_map<std::string, FunctionTypes> functions;
    mutable std::vector<Error> errors;
};

namespace bi {
enum class Op
{
    NoOp,
    Store,
    Shl,
    Shr,
    And,
    Xor,
    Or,
    Add,
    Sub,
    Mul,
    Div,
#ifdef POWER_OPERATOR
    Pow,
#endif
    Mod,
    Negate,
    Not,
    LoadVar,
    Call,
};

template<typename T>
class Insn
{
public:
    Op op;
    // Some value
    T arg1 = {};
    // Argument count
    size_t arg2 = {};
    // Function, variable, constant name
    std::string arg3;
    friend std::ostream& operator << (std::ostream& os, const Insn& value)
    {
        switch (value.op)
        {
        case Op::NoOp:
            os << "NoOp\targ1: " << value.arg1 << "arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Store:
            os << "Store\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Shl:
            os << "Shl\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Shr:
            os << "Shr\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::And:
            os << "And\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Xor:
            os << "Xor\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Or:
            os << "Or\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Add:
            os << "Add\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Sub:
            os << "Sub\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Mul:
            os << "Mul\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Div:
            os << "Div\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
#ifdef POWER_OPERATOR
        case Op::Pow:
            os << "Pow\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
#endif
        case Op::Mod:
            os << "Mod\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Negate:
            os << "Negate\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Not:
            os << "Not\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::LoadVar:
            os << "LoadVar\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        case Op::Call:
            os << "Call\targ1: " << value.arg1 << " arg2 " << value.arg2 << " arg3 " << value.arg3;
            break;
        default:
            break;
        }
        return os;
    }
};

template<typename T>
using Code = std::vector<Insn<T>>;

inline constexpr size_t DEF_STACK_SIZE = 8;

// Simple stack based VM. Interestingly it's slower than the interpreted version :/.
// I thought getting rid of all the virtual function calls would improve the performance.
template<typename T, size_t StackSize = DEF_STACK_SIZE>
class VM
{
public:
    explicit VM(const Context<T>& ctx) :
        ctx_(ctx)
    { }
    T Execute(const Code<T>& code)
    {
        sp_ = StackSize - 1;
        for (const auto& insn : code)
        {
            switch (insn.op)
            {
            case Op::Store:
            {
                Push(insn.arg1);
                break;
            }
            case Op::Shl:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                {
                    Push(lhs << rhs);
                }
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Left shift operator only for integral types");
                }
                break;
            }
            case Op::Shr:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                {
                    Push(lhs >> rhs);
                }
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Right shift operator only for integral types");
                }
                break;
            }
            case Op::And:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                {
                    Push(lhs & rhs);
                }
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "And operator only for integral types");
                }
                break;
            }
            case Op::Xor:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                {
                    Push(lhs ^ rhs);
                }
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Xor operator only for integral types");
                }
                break;
            }
            case Op::Or:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                {
                    Push(lhs | rhs);
                }
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Or operator only for integral types");
                }
                break;
            }
            case Op::Add:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                Push(lhs + rhs);
                break;
            }
            case Op::Sub:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                Push(lhs - rhs);
                break;
            }
            case Op::Mul:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                Push(lhs * rhs);
                break;
            }
            case Op::Div:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                Push(lhs / rhs);
                break;
            }
#ifdef POWER_OPERATOR
            case Op::Pow:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                Push(static_cast<T>(std::pow(lhs, rhs)));
                break;
            }
#endif
            case Op::Mod:
            {
                const T rhs = Pop();
                const T lhs = Pop();
                if constexpr (std::is_integral<T>::value)
                    Push(lhs % rhs);
                else
                    Push(static_cast<T>(fmod(lhs, rhs)));
                break;
            }
            case Op::Negate:
            {
                Push(-Pop());
                break;
            }
            case Op::Not:
            {
                const T expr = Pop();
                if constexpr (std::is_integral<T>::value)
                    Push(~expr);
                else
                {
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Not operator only for integral types");
                }
                break;
            }
            case Op::LoadVar:
            {
                const auto varValue = ctx_.GetVariable(insn.arg3);
                if (varValue.has_value())
                {
                    if (varValue.value() != nullptr)
                        Push(*varValue.value());
                    else
                    {
                        Push({});
                        ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Variable \"" + insn.arg3 + "\" is null");
                    }
                    break;
                }
                const auto constValue = ctx_.GetConstant(insn.arg3);
                if (constValue.has_value())
                {
                    Push(constValue.value());
                    break;
                }
                Push({});
                ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Constant or Variable \"" + insn.arg3 + "\" not found");
                break;
            }
            case Op::Call:
            {
                switch (insn.arg2)
                {
                case 0:
                {
                    const auto res = ctx_.CallFunction(insn.arg3);
                    if (res.has_value())
                        Push(*res);
                    else
                        Push({});
                    break;
                }
                case 1:
                {
                    const auto res = ctx_.CallFunction(insn.arg3, Pop());
                    if (res.has_value())
                        Push(*res);
                    else
                        Push({});
                    break;
                }
                case 2:
                {
                    const auto res = ctx_.CallFunction(insn.arg3, Pop(), Pop());
                    if (res.has_value())
                        Push(*res);
                    else
                        Push({});
                    break;
                }
                case 3:
                {
                    const auto res = ctx_.CallFunction(insn.arg3, Pop(), Pop(), Pop());
                    if (res.has_value())
                        Push(*res);
                    else
                        Push({});
                    break;
                }
                case 4:
                {
                    const auto res = ctx_.CallFunction(insn.arg3, Pop(), Pop(), Pop(), Pop());
                    if (res.has_value())
                        Push(*res);
                    else
                        Push({});
                    break;
                }
                default:
                    Push({});
                    ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Too many arguments");
                }
                break;
            }
            default:
                break;
            }
        }
        return Pop();
    }
private:
    void Push(T value)
    {
        if (sp_ > 0)
            stack_[sp_--] = value;
        else
            ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Stack overflow");
    }
    T Pop()
    {
        if (sp_ < StackSize - 1)
            return stack_[++sp_];
        ctx_.errors.emplace_back(Error::Type::RuntimeError, 0, "Stack underflow");
        return {};
    }
    const Context<T>& ctx_;
    size_t sp_{ StackSize - 1 };
    std::array<T, StackSize> stack_{};
};

}  // namespace bi

template<typename T>
class Expr
{
public:
    virtual ~Expr() = default;
    using type = T;
    static constexpr T def = {};
    virtual T operator()(const Context<T>&) const = 0;
    virtual void Generate(bi::Code<T>& code) const = 0;
    virtual void Dump(std::ostream&, int) const { }
};

template<typename T>
using ExprPtr = std::unique_ptr<Expr<T>>;

template<typename T>
class Value final : public Expr<T>
{
public:
    explicit Value(T value) :
        value(value)
    { }
    T operator()(const Context<T>&) const override { return value; }
    void Generate(bi::Code<T>& code) const override
    {
        code.emplace_back(bi::Op::Store, value);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Value " << value << std::endl;
    }
    T value;
};

template<typename T>
class Variable final : public Expr<T>
{
public:
    explicit Variable(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        // Prefer variable
        const auto v = ctx.GetVariable(name);
        if (v.has_value())
        {
            if (v.value() != nullptr)
                return *v.value();
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Variable \"" + name + "\" is null");
            return this->def;
        }
        // Maybe a constant
        const auto c = ctx.GetConstant(name);
        if (c.has_value())
            return c.value();
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Variable or Constant \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        code.push_back({ .op = bi::Op::LoadVar, .arg1 = {}, .arg2 = 0, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Variable " << name << std::endl;
    }
    std::string name;
};

template<typename T>
class Function0 final : public Expr<T>
{
public:
    explicit Function0(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if (ctx.HasFunction(name))
            return ctx.CallFunction(name).value_or(this->def);
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Function \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        code.push_back({ .op = bi::Op::Call, .arg1 = {}, .arg2 = 0, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function0 " << name << std::endl;
    }
    std::string name;
};
template<typename T>
class Function1 final : public Expr<T>
{
public:
    explicit Function1(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if (ctx.HasFunction(name))
        {
            const auto r1 = (*this->arg1)(ctx);
            const auto result = ctx.CallFunction(name, r1);
            return result.value_or(this->def);
        }
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Function \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->arg1->Generate(code);
        code.push_back({ .op = bi::Op::Call, .arg1 = {}, .arg2 = 1, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function1 " << name << std::endl;
        this->arg1->Dump(os, indent + 2);
    }
    std::string name;
    ExprPtr<T> arg1;
};
template<typename T>
class Function2 final : public Expr<T>
{
public:
    explicit Function2(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if (ctx.HasFunction(name))
        {
            const auto r1 = (*this->arg1)(ctx);
            const auto r2 = (*this->arg2)(ctx);
            const auto result = ctx.CallFunction(name, r1, r2);
            return result.value_or(this->def);
        }
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Function \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->arg2->Generate(code);
        this->arg1->Generate(code);
        code.push_back({ .op = bi::Op::Call, .arg1 = {}, .arg2 = 2, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function2 " << name << std::endl;
        this->arg1->Dump(os, indent + 2);
        this->arg2->Dump(os, indent + 2);
    }
    std::string name;
    ExprPtr<T> arg1;
    ExprPtr<T> arg2;
};
template<typename T>
class Function3 final : public Expr<T>
{
public:
    explicit Function3(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if (ctx.HasFunction(name))
        {
            const auto r1 = (*this->arg1)(ctx);
            const auto r2 = (*this->arg2)(ctx);
            const auto r3 = (*this->arg3)(ctx);
            const auto result = ctx.CallFunction(name, r1, r2, r3);
            return result.value_or(this->def);
        }
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Function \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->arg3->Generate(code);
        this->arg2->Generate(code);
        this->arg1->Generate(code);
        code.push_back({ .op = bi::Op::Call, .arg1 = {}, .arg2 = 3, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function3 " << name << std::endl;
        this->arg1->Dump(os, indent + 2);
        this->arg2->Dump(os, indent + 2);
        this->arg3->Dump(os, indent + 2);
    }
    std::string name;
    ExprPtr<T> arg1;
    ExprPtr<T> arg2;
    ExprPtr<T> arg3;
};
template<typename T>
class Function4 final : public Expr<T>
{
public:
    explicit Function4(std::string name) :
        name(std::move(name))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if (ctx.HasFunction(name))
        {
            const auto r1 = (*this->arg1)(ctx);
            const auto r2 = (*this->arg2)(ctx);
            const auto r3 = (*this->arg3)(ctx);
            const auto r4 = (*this->arg4)(ctx);
            const auto result = ctx.CallFunction(name, r1, r2, r3, r4);
            return result.value_or(this->def);
        }
        ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Function \"" + name + "\" not found");
        return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->arg4->Generate(code);
        this->arg3->Generate(code);
        this->arg2->Generate(code);
        this->arg1->Generate(code);
        code.push_back({ .op = bi::Op::Call, .arg1 = {}, .arg2 = 4, .arg3 = name });
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function4 " << name << std::endl;
        this->arg1->Dump(os, indent + 2);
        this->arg2->Dump(os, indent + 2);
        this->arg3->Dump(os, indent + 2);
        this->arg4->Dump(os, indent + 2);
    }
    std::string name;
    ExprPtr<T> arg1;
    ExprPtr<T> arg2;
    ExprPtr<T> arg3;
    ExprPtr<T> arg4;
};

template<typename T>
class UnaryExpr : public Expr<T>
{
public:
    UnaryExpr(ExprPtr<T> expr) :
        expr(std::move(expr))
    { }
    ExprPtr<T> expr;
};

template<typename T>
class Negate final : public UnaryExpr<T>
{
public:
    Negate(ExprPtr<T> expr) :
        UnaryExpr<T>(std::forward<ExprPtr<T>>(expr))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return -(*this->expr)(ctx);
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->expr->Generate(code);
        code.emplace_back(bi::Op::Negate);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Negate" << std::endl;
        this->expr->Dump(os, indent + 2);
    }
};
template<typename T>
class Not final : public UnaryExpr<T>
{
public:
    Not(ExprPtr<T> expr) :
        UnaryExpr<T>(std::forward<ExprPtr<T>>(expr))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return ~(*this->expr)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Bitwise not operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->expr->Generate(code);
        code.emplace_back(bi::Op::Not);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Not" << std::endl;
        this->expr->Dump(os, indent + 2);
    }
};

template<typename T>
class BinaryExpr : public Expr<T>
{
public:
    BinaryExpr(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        lhs(std::move(lhs)),
        rhs(std::move(rhs))
    { }
    ExprPtr<T> lhs;
    ExprPtr<T> rhs;
};

template<typename T>
class Shl final : public BinaryExpr<T>
{
public:
    Shl(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return (*this->lhs)(ctx) << (*this->rhs)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Left shift operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Shl);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Shl" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Shr final : public BinaryExpr<T>
{
public:
    Shr(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return (*this->lhs)(ctx) >> (*this->rhs)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Right shift operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Shr);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Shr" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class And final : public BinaryExpr<T>
{
public:
    And(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return (*this->lhs)(ctx) & (*this->rhs)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "And operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::And);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "And" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Or final : public BinaryExpr<T>
{
public:
    Or(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return (*this->lhs)(ctx) | (*this->rhs)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Or operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Or);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Or" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Xor final : public BinaryExpr<T>
{
public:
    Xor(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
        {
            return (*this->lhs)(ctx) ^ (*this->rhs)(ctx);
        }
        else
        {
            ctx.errors.emplace_back(Error::Type::RuntimeError, 0, "Xor operator only for integral types");
            return this->def;
        }
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Xor);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Xor" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Add final : public BinaryExpr<T>
{
public:
    Add(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return (*this->lhs)(ctx) + (*this->rhs)(ctx);
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Add);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Add" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Sub final : public BinaryExpr<T>
{
public:
    Sub(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return (*this->lhs)(ctx) - (*this->rhs)(ctx);
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Sub);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Sub" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Div final : public BinaryExpr<T>
{
public:
    Div(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return (*this->lhs)(ctx) / (*this->rhs)(ctx);
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Div);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Div" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
template<typename T>
class Mul final : public BinaryExpr<T>
{
public:
    Mul(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return (*this->lhs)(ctx) * (*this->rhs)(ctx);
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Mul);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Mul" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
#ifdef POWER_OPERATOR
template<typename T>
class Pow final : public BinaryExpr<T>
{
public:
    Pow(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        return static_cast<T>(std::pow((*this->lhs)(ctx), (*this->rhs)(ctx)));
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Pow);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Pow" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};
#endif
template<typename T>
class Mod final : public BinaryExpr<T>
{
public:
    Mod(ExprPtr<T> lhs, ExprPtr<T> rhs) :
        BinaryExpr<T>(std::forward<ExprPtr<T>>(lhs), std::forward<ExprPtr<T>>(rhs))
    { }
    T operator()(const Context<T>& ctx) const override
    {
        if constexpr (std::is_integral<T>::value)
            return (*this->lhs)(ctx) % (*this->rhs)(ctx);
        else if constexpr (std::is_floating_point<T>::value)
            return fmod((*this->lhs)(ctx), (*this->rhs)(ctx));
        else
            return this->def;
    }
    void Generate(bi::Code<T>& code) const override
    {
        this->lhs->Generate(code);
        this->rhs->Generate(code);
        code.emplace_back(bi::Op::Mod);
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Mod" << std::endl;
        this->lhs->Dump(os, indent + 2);
        this->rhs->Dump(os, indent + 2);
    }
};

template<typename T>
class Parser
{
public:
    explicit Parser(const Context<T>& ctx) :
        ctx_(ctx)
    { }
    ExprPtr<T> operator()(std::string_view source)
    {
        pos_ = 0;
        source_ = source;
        auto expr = ParseBitwise();
        if (!ctx_.errors.empty())
            return {};

        return expr;
    }
private:
    ExprPtr<T> ParseBitwise()
    {
        auto lhs = ParseShift();
        while (true)
        {
            SkipWhite();
            const char c = Peek();
            if (c == '&')
            {
                GetChar();
                lhs = std::make_unique<And<T>>(std::move(lhs), ParseShift());
            }
#ifdef POWER_OPERATOR
            else if (c == '!')
#else
            else if (c == '^')
#endif
            {
                GetChar();
                lhs = std::make_unique<Xor<T>>(std::move(lhs), ParseShift());
            }
            else if (c == '|')
            {
                GetChar();
                lhs = std::make_unique<Or<T>>(std::move(lhs), ParseShift());
            }
            else
                break;
        }
        return lhs;
    }
    ExprPtr<T> ParseShift()
    {
        auto lhs = ParseSum();
        while (true)
        {
            SkipWhite();
            const char c = Peek();
            if (c == '>' && Peek(1) == '>')
            {
                GetChar();
                GetChar();
                lhs = std::make_unique<Shr<T>>(std::move(lhs), ParseSum());
            }
            else if (c == '<' && Peek(1) == '<')
            {
                GetChar();
                GetChar();
                lhs = std::make_unique<Shl<T>>(std::move(lhs), ParseSum());
            }
            else
                break;
        }
        return lhs;
    }
    ExprPtr<T> ParseSum()
    {
        auto lhs = ParseProduct();
        while (true)
        {
            SkipWhite();
            const char c = Peek();
            if (c == '+')
            {
                GetChar();
                lhs = std::make_unique<Add<T>>(std::move(lhs), ParseProduct());
            }
            else if (c == '-')
            {
                GetChar();
                lhs = std::make_unique<Sub<T>>(std::move(lhs), ParseProduct());
            }
            else
                break;
        }
        return lhs;
    }
    ExprPtr<T> ParseProduct()
    {
#ifdef POWER_OPERATOR
        auto lhs = ParsePow();
#else
        auto lhs = ParseUnary();
#endif
        while (true)
        {
            SkipWhite();
            const char c = Peek();
            if (c == '*')
            {
                GetChar();
#ifdef POWER_OPERATOR
                lhs = std::make_unique<Mul<T>>(std::move(lhs), ParsePow());
#else
                lhs = std::make_unique<Mul<T>>(std::move(lhs), ParseUnary());
#endif
            }
            else if (c == '/')
            {
                GetChar();
#ifdef POWER_OPERATOR
                lhs = std::make_unique<Div<T>>(std::move(lhs), ParsePow());
#else
                lhs = std::make_unique<Div<T>>(std::move(lhs), ParseUnary());
#endif
            }
            else if (c == '%')
            {
                GetChar();
#ifdef POWER_OPERATOR
                lhs = std::make_unique<Mod<T>>(std::move(lhs), ParsePow());
#else
                lhs = std::make_unique<Mod<T>>(std::move(lhs), ParseUnary());
#endif
            }
            else
                break;
        }
        return lhs;
    }
#ifdef POWER_OPERATOR
    ExprPtr<T> ParsePow()
    {
        auto lhs = ParseUnary();
        while (true)
        {
            SkipWhite();
            if (Peek() == '^')
            {
                GetChar();
                lhs = std::make_unique<Pow<T>>(std::move(lhs), ParsePow());
            }
            else
                break;
        }
        return lhs;
    }
#endif
    ExprPtr<T> ParseUnary()
    {
        enum class Operator
        {
            NoOp,
            Negate,
            Not
        };
        SkipWhite();
        char next = Peek();
        Operator op = Operator::NoOp;
        switch (next)
        {
        case '-':
            op = Operator::Negate;
            GetChar();
            SkipWhite();
            break;
        case '~':
            op = Operator::Not;
            GetChar();
            SkipWhite();
            break;
        case '+':
            GetChar();
            SkipWhite();
            break;
        default:
            break;
        }
        switch (op)
        {
        case Operator::NoOp:
            return ParseTerm();
        case Operator::Negate:
            return std::make_unique<Negate<T>>(ParseTerm());
        case Operator::Not:
            return std::make_unique<Not<T>>(ParseTerm());
        default:
            return {};
        }
    }
    ExprPtr<T> ParseTerm()
    {
        SkipWhite();

        char next = Peek();

        if (next == '(')
        {
            GetChar();

            auto result = ParseBitwise();

            SkipWhite();
            Consume(')');

            return result;
        }
        if (IsNumber(next))
        {
            return std::make_unique<Value<T>>(ParseValue());
        }
        if (isalpha(next))
        {
            const std::string symbol = ParseSymbol();
            SkipWhite();
            if (Peek() == '(')
            {
                // Function call
                SkipWhite();
                std::vector<ExprPtr<T>> args;
                Consume('(');
                SkipWhite();
                if (Peek() != ')')
                {
                    auto arg = ParseBitwise();
                    while (arg)
                    {
                        // Parse argument list
                        args.push_back(std::move(arg));
                        SkipWhite();
                        if (Peek() != ',')
                            break;
                        Consume(',');
                        arg = ParseBitwise();
                    }
                }
                SkipWhite();
                Consume(')');
                switch (args.size())
                {
                case 0:
                    return std::make_unique<Function0<T>>(std::move(symbol));
                case 1:
                {
                    auto result = std::make_unique<Function1<T>>(std::move(symbol));
                    result->arg1 = std::move(args[0]);
                    return result;
                }
                case 2:
                {
                    auto result = std::make_unique<Function2<T>>(std::move(symbol));
                    result->arg1 = std::move(args[0]);
                    result->arg2 = std::move(args[1]);
                    return result;
                }
                case 3:
                {
                    auto result = std::make_unique<Function3<T>>(std::move(symbol));
                    result->arg1 = std::move(args[0]);
                    result->arg2 = std::move(args[1]);
                    result->arg3 = std::move(args[2]);
                    return result;
                }
                case 4:
                {
                    auto result = std::make_unique<Function4<T>>(std::move(symbol));
                    result->arg1 = std::move(args[0]);
                    result->arg2 = std::move(args[1]);
                    result->arg3 = std::move(args[2]);
                    result->arg4 = std::move(args[3]);
                    return result;
                }
                default:
                    ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Too many arguments");
                    return {};
                }
            }
            // Try to expand the constant, if it's not a variable
            if (!ctx_.HasVariable(symbol))
            {
                const auto c = ctx_.GetConstant(symbol);
                if (c.has_value())
                    return std::make_unique<Value<T>>(c.value());
            }
            // May still be a constant, e.g. when it's added to the context after parsing, but before evaluating
            return std::make_unique<Variable<T>>(std::move(symbol));
        }
        ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Expected a number, symbol or parenthesized expression");
        return {};
    }

    T ParseValue()
    {
        enum class NumberType
        {
            Decimal,
            Hex,
            Octal,
            Bin
        } numberType = NumberType::Decimal;
        std::string s;
        SkipWhite();

        auto isDigit = [&numberType](char c) -> bool
        {
            switch (numberType)
            {
            case NumberType::Decimal:
                return isdigit(c);
            case NumberType::Hex:
                return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
            case NumberType::Octal:
                return c >= '0' && c <= '7';
            case NumberType::Bin:
                return c == '0' || c == '1';
            default:
                return false;
            }
        };

        if (Peek() == '0')
        {
            s.push_back(GetChar());
            if (Peek() == 'x' || Peek() == 'X')
            {
                s.push_back(GetChar());
                numberType = NumberType::Hex;
            }
            else if (Peek() == 'o' || Peek() == 'O')
            {
                s.push_back(GetChar());
                numberType = NumberType::Octal;
            }
            else if (Peek() == 'b' || Peek() == 'B')
            {
                s.push_back(GetChar());
                numberType = NumberType::Bin;
            }
        }

        while (isDigit(Peek()))
        {
            s.push_back(GetChar());
        }

        if (Peek() == '.')
        {
            if (numberType != NumberType::Decimal)
            {
                ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Malformed number");
                return {};
            }
            s.push_back(GetChar());
            while (isdigit(Peek()))
            {
                s.push_back(GetChar());
            }
        }

        if ((Peek() == 'e' || Peek() == 'E'))
        {
            if (numberType != NumberType::Decimal)
            {
                ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Malformed number");
                return {};
            }
            s.push_back(GetChar());

            if (Peek() == '+' || Peek() == '-')
            {
                s.push_back(GetChar());
            }

            if (!isdigit(Peek()))
            {
                ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Expected exponent in floating point constant");
                return {};
            }

            while (isdigit(Peek()))
            {
                s.push_back(GetChar());
            }
        }
        if (isalnum(Peek()))
        {
            ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Malformed number");
            return {};
        }

        try
        {
            if (numberType == NumberType::Bin)
            {
                s.erase(0, 2);
                return static_cast<T>(stoi(s, 0, 2));
            }
            if (numberType == NumberType::Octal)
            {
                s.erase(0, 2);
                return static_cast<T>(stoi(s, 0, 8));
            }
            return static_cast<T>(stod(s));
        }
        catch (...)
        {
            ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Invalid number");
            return {};
        }
    }
    std::string ParseSymbol()
    {
        std::string result;
        SkipWhite();
        if (!isalpha(Peek()))
        {
            ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, "Alpha expected");
            return {};
        }
        while (isalpha(Peek()) || Peek() == '_' || isdigit(Peek()))
            result.push_back(GetChar());
        return result;
    }

    void SkipWhite()
    {
        while (isspace(Peek()))
            GetChar();
    }
    void Consume(char c)
    {
        if (Peek() != c)
        {
            std::stringstream ss;
            ss << "Expecting " << c;
            ctx_.errors.emplace_back(Error::Type::SyntaxError, pos_, ss.str());
            return;
        }
        GetChar();
    }
    char GetChar()
    {
        char result = Peek();
        if (result != '\0')
            ++pos_;
        return result;
    }
    constexpr char Peek(size_t off = 0) const
    {
        if (pos_ + off >= source_.length())
            return '\0';
        return source_[pos_ + off];
    }
    constexpr bool IsEof() const { return source_.length() <= pos_; }
    constexpr bool IsNumber(char c) const { return isdigit(c) || c == '-' || c == '+' || c == '.'; }

    const Context<T>& ctx_;
    std::string_view source_;
    size_t pos_{ 0 };
};

// Convenience functions

template<typename T>
inline T Evaluate(std::string_view source, const Context<T>& ctx)
{
    Parser<T> parser(ctx);
    auto expr = parser(source);
    if (!expr)
        return {};
    return (*expr)(ctx);
}
template<typename T>
inline T Evaluate(std::string_view source)
{
    Context<T> ctx;
    return Evaluate<T>(std::forward<std::string_view>(source), ctx);
}

}
