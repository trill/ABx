/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <limits>
#include <type_traits>

namespace sa {
namespace math {

template <typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs &&
            lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

template<typename T, typename U>
constexpr T Lerp(T a, T b, U t)
{
    return a * (static_cast<T>(1.0) - t) + b * t;
}

}
}
