/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <limits>
#include <type_traits>

namespace sa {

template<typename T>
constexpr T ClampedAdd(T value, T add, T* result)
{
    if constexpr (std::is_signed<T>::value)
    {
        if (add > 0 && value > std::numeric_limits<T>::max() - add)
        {
            if (result)
                *result = std::numeric_limits<T>::max();
            return true;
        }
        if (add < 0 && value < std::numeric_limits<T>::min() - add)
        {
            if (result)
                *result = std::numeric_limits<T>::min();
            return true;
        }
        if (result)
            *result = value + add;
        return false;
    }
    else
    {
        if ((std::numeric_limits<T>::max() - add) < value)
        {
            if (result)
                *result = std::numeric_limits<T>::max();
            return true;
        }
        if (result)
            *result = value + add;
        return false;
    }
}

/// Check if value would exceed max if add was added
template <typename T>
constexpr bool OverflowsAdd(T value, T add, T max)
{
    if constexpr (std::is_signed<T>::value)
    {
        if (add > 0 && value > max - add)
            return true;
        if (add < 0 && value < max - add)
            return true;
        return false;
    }
    else
    {
        return (max - add) < value;
    }
}

template <typename T>
constexpr bool OverflowsAdd(T value, T add)
{
    return ClampedAdd(value, add, (T*)nullptr);
}

template<typename T>
constexpr T ClampedAdd(T value, T add)
{
    T result = {};
    ClampedAdd(value, add, &result);
    return result;
}

}
