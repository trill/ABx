/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

// Provide assert() even when NDEBUG is defined, i.e. release builds. In Debug build nothing changes.
// SA_ASSERT must be defined.

#include <cassert>
#include <stdlib.h>
#include <iostream>
#include <sa/Compiler.h>

#ifdef SA_ASSERT

#ifdef NDEBUG

namespace sa {
namespace details {

[[noreturn]] SA_ALWAYS_INLINE void assertion_failed(const char* msg, const char* file, unsigned line, const char* func)
{
    std::cerr << "Assertion failed: " << msg << " in " << file << ":" << line << " " << func << std::endl;
    abort();
}

}
}

#define ASSERT(expr) (static_cast<bool>(expr) ? (void)0 : sa::details::assertion_failed(#expr, __FILE__, __LINE__, SA_FUNCTION_SIG))
#else
#define ASSERT assert
#endif  // NDEBUG
#else
#define ASSERT(expr)
#endif  // SA_ASSERT

// NOTE: ASSERT_FALSE() never returns, no matter what is defined, i.e. also not in release builds.
#if !defined(NDEBUG) || defined(SA_ASSERT)
// There is an abort() to make sure it really does not return, even when the assert() macro doesn't do much.
#define ASSERT_FALSE()           \
    do                           \
    {                            \
        ASSERT(false);           \
        abort();                 \
    } while (0)
#else
#define ASSERT_FALSE() abort()
#endif
