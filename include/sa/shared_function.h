#pragma once

namespace sa {

template<class F>
auto make_shared_function(F&& f)
{
    return [pf = std::make_shared<std::decay_t<F>>(std::forward<F>(f))](auto&&...args) -> decltype(auto)
    {
        return (*pf)(decltype(args)(args)...);
    };
}

}
