/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdlib.h>
#include <string_view>

namespace sa {

/// Compile time string hash

// FNV-1a constants
#if defined(_M_X64) || defined(__amd64__) || defined(__x86_64) || defined(__x86_64__)
static_assert(sizeof(size_t) == 8);
static const size_t OFFSET = 14695981039346656037ULL;
static const size_t PRIME = 1099511628211ULL;
#else
static_assert(sizeof(size_t) == 4);
static const size_t OFFSET = 2166136261U;
static const size_t PRIME = 16777619U;
#endif

constexpr size_t StringHash(std::string_view str)
{
    size_t hash = OFFSET;
    for (auto c : str)
    {
        hash ^= static_cast<size_t>(c);
        hash *= PRIME;
    }
    return hash;
}

namespace literals {
constexpr size_t operator"" _Hash(const char* str, size_t) { return StringHash(str); }
}

}
