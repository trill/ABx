/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace sa {

template <typename T>
class [[nodiscard]] Transaction
{
private:
    T copy_;
    T& value_;
    bool committed_{ false };
public:
    Transaction(T& value) :
        copy_(value),
        value_(value)
    {}
    ~Transaction()
    {
        if (!committed_)
            value_ = copy_;
    }
    void Commit() { committed_ = true; }
};

template <typename T> Transaction(T&) -> Transaction<T>;

}
