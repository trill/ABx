/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

// WIP, do not use!

#include <AB/CommonConfig.h>
#include <libcommon/FileUtils.h>
#include <libcommon/SimpleConfigManager.h>
#include <sa/StringTempl.h>
#include <sa/path.h>
#include <sa/Process.h>
#include <stdint.h>
#include <fstream>
#include <iostream>
#include <libcommon/Process.hpp>
#include <string>

static sa::path GetProgramDir()
{
    return sa::path(Utils::GetExeName()).directory();
}

static sa::path GetConfigDir()
{
    return GetProgramDir() / sa::path("config");
}

static sa::path GetClientBinDir()
{
    return sa::path(Utils::GetExeName()).directory() / sa::path("../abclient/bin");
}

static bool SetupDatabase()
{
    std::string dbPrivate = (GetConfigDir() / sa::path("db_private.lua")).string();
    std::string dbHost;
    uint16_t dbPort = 5432;
    std::string dbUser;
    std::string dbPass;
    IO::SimpleConfigManager cfg;

    if (cfg.Load(dbPrivate))
    {
        dbHost = cfg.GetGlobalString("db_host", "");
        dbPort = cfg.GetGlobalInt("db_port", 5432);
        dbUser = cfg.GetGlobalString("db_user", "");
        dbPass = cfg.GetGlobalString("db_pass", "");
    }

    std::cout << " == Database setup ===============================" << std::endl;
    std::string strHost;
    std::cout << "Databse host [" << dbHost << "]: ";
    std::getline(std::cin, strHost);
    if (!strHost.empty())
        dbHost = strHost;

enterPort:
    std::string portString;

    std::cout << "Databse port [" << dbPort << "]: ";
    std::getline(std::cin, portString);
    if (!portString.empty())
    {
        auto port = sa::ToNumber<uint16_t>(portString);
        if (!port.has_value())
        {
            std::cerr << portString << " is not a number" << std::endl;
            goto enterPort;
        }
        dbPort = port.value();
    }

    std::string strUser;
    std::cout << "Databse username [" << dbUser << "]: ";
    std::getline(std::cin, strUser);
    if (!strUser.empty())
        dbUser = strUser;

    std::string strPass;
    std::cout << "Databse password [" << dbPass << "]: ";
    std::getline(std::cin, strPass);
    if (!strPass.empty())
        dbPass = strPass;

    if (dbHost.empty())
    {
        std::cerr << "Host name can not be empty" << std::endl;
        return false;
    }
    if (dbPort == 0)
    {
        std::cerr << "Port can not be 0" << std::endl;
        return false;
    }

    if (cfg.GetGlobalString("db_host", "") == dbHost && cfg.GetGlobalInt("db_port", 0) == dbPort
        && cfg.GetGlobalString("db_user", "") == dbUser && cfg.GetGlobalString("db_pass", "") == dbPass)
        return true;

    std::ofstream out(dbPrivate);
    out << "db_host = \"" << dbHost << "\"" << std::endl;
    out << "db_port = " << dbPort << std::endl;
    out << "db_user = \"" << dbUser << "\"" << std::endl;
    out << "db_pass = \"" << dbPass << "\"" << std::endl;

    return true;
}

static bool CreateDatabase()
{
    std::cout << " == Generating Database ===============================" << std::endl;
    System::Process proc("dbtool update", GetProgramDir().string());
    return proc.get_exit_status() == 0;
}

static bool DownloadAssets()
{
    std::cout << " == Downloading Assets ===============================" << std::endl;
    {
        System::Process proc("wget -o sound_data.7z "
                             "https://ejcdcg.ch.files.1drv.com/"
                             "y4mu7fZBluLmacyuTa2aETF6L2vD6XcoZZrYctZGZ0j2VIGYxYDNKT4Js70cGk3GSQLQLo4sBvKZ287JXFW8r1m_"
                             "B4x1boa2Brv2cD26OsKs6qmnbXfcQqcPkWTeLx1URRxQFl_PFDCSHbL4lOAJpPw2dk2Olg_gupHgu6CuK2jab_"
                             "LFg_YwrW7KvTgUOvRS8LqYp1B5v4v6oNvYI_FtyetCg",
            GetClientBinDir().string());
        if (proc.get_exit_status() != 0)
            return false;
    }
    {
        System::Process proc("wget -o client_data.zip "
                             "https://ejcdcg.ch.files.1drv.com/"
                             "y4mxFtYhlrCkT6RgiB8VDXXGZxtzZeMJYs07cIg30Bb3Wea23MPBawUvXMT5ddpxNJWgjsKt9bryQd88jsaOdmRZG"
                             "G114A-lGsvmeXSLKtjR6uYma6JWqowstvfOBvwypSEdecpG-3bWbH9Cit-CIDXJ-zxUBxUX5ZFzmxZq_"
                             "z6vtLjP3yMevTjwYSl-PZK8iS2rhJIO7tfSdy5XGmEyLOsZw",
            GetClientBinDir().string());
        if (proc.get_exit_status() != 0)
            return false;
    }
    return true;
}

static bool GenerateAssets()
{
    std::cout << " == Generating Assets ===============================" << std::endl;
    System::Process proc("genassets.sh", GetProgramDir().string());
    return proc.get_exit_status() == 0;
}

static bool GenerateDHKeys()
{
    std::cout << " == Generating DH Keys ===============================" << std::endl;
    System::Process proc("keygen", GetProgramDir().string());
    return proc.get_exit_status() == 0;
}

static bool GenerateSSLKeys()
{
    std::cout << " == Generating SSL Keys ===============================" << std::endl;
    System::Process proc(
        "openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout server.key -out server.crt",
        GetProgramDir().string());
    return proc.get_exit_status() == 0;
}

static void CreateMainAccount()
{
    std::cout << " == Creating main account ===============================" << std::endl;
}

static void RunServices()
{
    std::string runScript = (GetProgramDir() / "run").string();
    sa::Process::Run(runScript);
}

int main()
{
    if (!SetupDatabase())
        return 1;

    if (!CreateDatabase())
        return 1;

    DownloadAssets();
    GenerateAssets();
    GenerateDHKeys();
    GenerateSSLKeys();
    CreateMainAccount();
    RunServices();
    return 0;
}
