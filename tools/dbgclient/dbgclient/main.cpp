/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <iostream>
#include <sa/ArgParser.h>
#include <sa/Compiler.h>
#include <AB/CommonConfig.h>
PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4592)
    PRAGMA_WARNING_DISABLE_CLANG("-Wpadded")
    PRAGMA_WARNING_DISABLE_GCC("-Wpadded")
#   include <asio.hpp>
PRAGMA_WARNING_POP
#include <libcommon/Subsystems.h>
#include <libcommon/Scheduler.h>
#include <libcommon/Dispatcher.h>
#include <thread>
#include <atomic>
#include "DebugClient.h"
#include "Window.h"
#include <thread>
#include <csignal>

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("dbgclient", _cli, "Debug Client");
}

int main(int argc, char** argv)
{
    sa::ArgParser::Cli _cli{ {
        { "help", { "-h", "-help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None },
        { "host", { "-host" }, "Server host", true, true, sa::ArgParser::OptionType::String },
        { "port", { "-p", "-port" }, "Server port", true, true, sa::ArgParser::OptionType::Integer }
    } };

    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return EXIT_SUCCESS;
    }
    if (!cmdres)
    {
        std::cerr << cmdres << std::endl;
        std::cerr << "Type `dbgclient -h` for help." << std::endl;
        return EXIT_FAILURE;
    }

    Subsystems::Instance.CreateSubsystem<Asynch::Dispatcher>();
    Subsystems::Instance.CreateSubsystem<Asynch::Scheduler>();

    GetSubsystem<Asynch::Dispatcher>()->Start();
    GetSubsystem<Asynch::Scheduler>()->Start();

    asio::io_service io;

    Window wnd;
    DebugClient client(io, wnd);

    std::string host = sa::ArgParser::GetValue<std::string>(parsedArgs, "host", "");
    uint16_t port = sa::ArgParser::GetValue<uint16_t>(parsedArgs, "port", 0);
    if (!client.Connect(host, port))
    {
        std::cerr << "Unable to connect to " << host << ":" << port << std::endl;
        return EXIT_FAILURE;
    }

    std::thread thread([&io]() { io.run(); });
    wnd.Loop();
    io.stop();
    thread.join();

    GetSubsystem<Asynch::Dispatcher>()->Stop();
    GetSubsystem<Asynch::Scheduler>()->Stop();

    return EXIT_SUCCESS;
}
