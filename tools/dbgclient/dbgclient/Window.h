/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Compiler.h>
#include <string>
#include <functional>
#include <AB/CommonConfig.h>
#if defined(AB_UNIX)
#include <ncurses.h>
#elif defined(AB_WINDOWS)
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4005)
#include <curses.h>
PRAGMA_WARNING_POP
#endif
#include <panel.h>

// http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/

struct Point
{
    int x;
    int y;
};

#define DEFAULT_BORDER_COLOR 1
#define ACTIVE_BORDER_COLOR 2
#define DEFAULT_LISTITEM_COLOR 3
#define SELECTED_LISTITEM_COLOR 4

class Window
{
public:
    enum Windows : size_t
    {
        WindowGames,
        WindowActors,
        WindowBehavior,
        __WindowsCount
    };
private:
    bool running_{ false };
    WINDOW* wins_[__WindowsCount];
    PANEL* panels_[__WindowsCount];
    PANEL* topPanel_{ nullptr };
    std::string statusText_;
    void ActivatePanel(PANEL* newTop);
    void CreateWindows();
    void DestroyWindows();
public:
    Window();
    ~Window();
    void Loop();
    void Print(const Point& pos, const std::string& text);
    void Goto(const Point& pos);
    void PrintStatusLine(const std::string& txt);
    void Clear();
    void BeginWindowUpdate(Windows window);
    void EndWindowUpdate(Windows window);
    void PrintGame(const std::string& txt, int index, bool selected);
    void PrintObject(const std::string& txt, int index, bool selected);
    void PrintObjectDetails(const std::string& txt, int line,
        bool selected = false, bool bold = false);
    void ShowCursor(bool visible);
    Point GetPosition() const;
    Point GetSize() const;
    bool IsRunning() const { return running_; }
    Windows GetActiveWindow() const;
    void SetStatusText(const std::string& value);

    std::function<void(Windows window, int c)> onKey_;
    std::function<void()> onResized_;
};

