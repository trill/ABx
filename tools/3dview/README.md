# 3dview

Simple 3D mesh viewer.

![Screenshot](3dview.png?raw=true)

![Screenshot](3dview2.png?raw=true)

## Usage

~~~
3dview [options] <input file>
~~~

### Options

* -h, --help, -?: Show help
* -v, --version: Show version
* -info, --show-info: Show info
* -s, --solid: Fill triangles
* -G, --dont-draw-grid: Don't draw Grid
* -b, --draw-boundingbox: Draw Bounding box
* -n, --draw-face-normals: Draw Face normals
* -l, --normal-length <number>: Length of Face normals
* -f, --max-fps <integer>: Limit FPS (0 for no limit, default 60)
* -fov, --field-of-view <number>: Field of view angle in degree (default 60)
* -nc, --near-clip <number>: Near clip
* -fc, --far-clip <number>: Far clip
* -z, --zoom <number>: Zoom
* -i, --importer <importer>: Use specific importer

### Importers

* default: Let 3dview decide (implied)
* modl: ABx .mdl files
* hm: ABx .hm files
* obstacles: ABx .obstacles files
* umd: Urho3D's .mdl files
* obj: Built in .obj files
* bitmap: Bitmap files files
* json: JSON heightmap loader
* assimp: Assimp importer (multiple file formats)

### Shortcuts

* f: Toggle wireframe/solid
* g: Toggle draw grid
* b: Toggle draw bounding box
* n: Toggle draw face normals
* l: Increase normal vector length
* L: Decrease normal vector length
* w: Move camera forward
* s: Move camera backward
* a: Move camera left
* d: Move camera right
* e: Move camera up
* x: Move camera down
* Shift: Move faster
* RMB: Mouse look
* Mouse wheel: Zoom
* y: Reverse camera
* v: Increase field of view
* V: Decrease field of view
* z: Flip Z-Axis
* Esc: Quit

## Supported file formats

* ABx own obstacles format (*.obstacles)
* ABx own Model format (*.mdl)
* ABx own height map format (*.hm)
* Simplified Wavefront OBJ (*.obj)
* PNG bitmaps are converted to height maps (*.png)
* Urho3D Model files with *one* vertex buffer (*.mdl)
* When compiled with [Assimp](http://assimp.org/), everything Assimp supports

## Dependencies

* OpenGL
* GLEW
* freeglut
* PugiXML
* libmath
* libio
* libcommon
* stb
* chillout
* Assimp (optional)
