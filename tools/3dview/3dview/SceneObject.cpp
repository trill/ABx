/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SceneObject.h"
#include <libmath/VectorMath.h>

SceneObject::SceneObject(std::shared_ptr<Math::Mesh> mesh) :
    bb_(&mesh->vertexData_[0], mesh->vertexCount_),
    mesh_(std::move(mesh))
{
}

SceneObject::~SceneObject() = default;

void SceneObject::UpdateBB()
{
    ASSERT(mesh_);
    bb_ = Math::BoundingBox(&mesh_->vertexData_[0], mesh_->vertexCount_);
}

const Math::MultiLine& SceneObject::GetNormals(float length) const
{
    ASSERT(mesh_);
    if (!Math::Equals(normalLength_, length))
    {
        normalLength_ = length;
        normals_.reset();
    }

    if (!normals_)
    {
        normals_ = std::make_unique<Math::MultiLine>();
        if (vertexNormals_.empty())
        {
            // Calculate face normals
            for (size_t i = 0; i < mesh_->GetTriangleCount(); ++i)
            {
                const auto tri = mesh_->GetTriangle(i);
                const auto center = (tri[0] + tri[1] + tri[2]) / 3.0f;
                const auto normal = mesh_->GetTriangleNormal(i);
                normals_->lines_.emplace_back(center, center + normal * normalLength_);
            }
        }
        else
        {
            ASSERT(vertexNormals_.size() == mesh_->vertexCount_);
            // If we have vertex normals
            for (size_t i = 0; i < vertexNormals_.size(); ++i)
            {
                const auto& pos = mesh_->vertexData_[i];
                const auto& vn = vertexNormals_[i];
                normals_->lines_.emplace_back(pos, pos + vn * normalLength_);
            }
        }
    }
    return *normals_;
}

size_t SceneObject::GetVertexCount() const
{
    ASSERT(mesh_);
    return mesh_->vertexCount_;
}

size_t SceneObject::GetTriangleCount() const
{
    ASSERT(mesh_);
    return mesh_->GetTriangleCount();
}

const Math::Mesh& SceneObject::GetMesh() const
{
    ASSERT(mesh_);
    return *mesh_;
}

Math::Mesh& SceneObject::GetMesh()
{
    ASSERT(mesh_);
    return *mesh_;
}
