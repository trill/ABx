/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "BitmapLoader.h"

#include <libmath/Mesh.h>
#include <libio/Mesh.h>
#include <libmath/HeightMap.h>
#include <libmath/HeightMapTools.h>
#include <stb_image.h>

BitmapLoader::BitmapLoader(std::string filename) :
    Loader(std::forward<std::string>(filename))
{ }

std::vector<std::shared_ptr<SceneObject>> BitmapLoader::Load()
{
    int width = 0; int height = 0; int components = 0;
    unsigned char* data = stbi_load(filename_.c_str(), &width, &height, &components, 0);
    if (!data)
    {
        std::cerr << "Bitmap " << filename_ << " has no data" << std::endl;
        return {};
    }

    // Default values
    Math::Vector3 spacing = { 1.0f, 0.2f, 1.0f };
    int patchSize = 32;

    Math::HeightmapHeader header;

    auto mesh = std::make_shared<Math::Mesh>();
    Math::CreateShapeFromHeightmapImage(data, width, height, components, spacing, patchSize,
        [&mesh](const Math::Vector3& vertex)
        {
            mesh->vertexData_.push_back(vertex);
            ++mesh->vertexCount_;
        },
        [&mesh](int i1, int i2, int i3)
        {
            mesh->AddTriangle(i1, i2, i3);
        },
        header);
    free(data);
    std::cout << "patchSize: " << header.patchSize << std::endl;
    std::cout << "patchWorldSize: " << header.patchWorldSize << std::endl;
    std::cout << "numPatches: " << header.numPatches << std::endl;
    std::cout << "numVertices: " << header.numVertices << std::endl;
    std::cout << "patchWorldOrigin: " << header.patchWorldOrigin << std::endl;
    std::cout << "min/max height: " << header.minHeight << "/" << header.maxHeight << std::endl;
    auto sceneObject = std::make_shared<SceneObject>(std::move(mesh));
    return { sceneObject };
}

void BitmapLoader::GetExtensions(std::vector<std::string>& result)
{
    result.emplace_back("*.png");
    result.emplace_back("*.bmp");
    result.emplace_back("*.jpg");
}
