/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Loader.h"
#include "AssimpLoader.h"
#include "ModlLoader.h"
#include "UmdLoader.h"
#include "ObjLoader.h"
#include "ObstaclesLoader.h"
#include "BitmapLoader.h"
#include "HmLoader.h"
#include "UrhoSceneLoader.h"
#include "JsonLoader.h"
#include <fstream>
#include <iomanip>
#include <istream>
#include <sstream>
#include <libcommon/StringUtils.h>

std::shared_ptr<Loader> Loader::GetLoader(const std::string& filename, std::optional<std::string> importer)
{
    if (importer.has_value())
    {
        std::string imp = importer.value();
        if (imp == "modl")
            return std::make_shared<ModlLoader>(filename);
        if (imp == "umd")
            return std::make_shared<UmdLoader>(filename);
        if (imp == "uscene")
            return std::make_shared<UrhoSceneLoader>(filename);
        if (imp == "hm")
            return std::make_shared<HmLoader>(filename);
        if (imp == "obj")
            return std::make_shared<ObjLoader>(filename);
        if (imp == "obstacles")
            return std::make_shared<ObstaclesLoader>(filename);
        if (imp == "bitmap")
            return std::make_shared<BitmapLoader>(filename);
        if (imp == "json")
            return std::make_shared<JsonLoader>(filename);
        if (imp == "assimp")
            return std::make_shared<AssimpLoader>(filename);
        if (imp != "default")
            return {};
    }

    std::fstream input(filename, std::ios::binary | std::fstream::in);
    if (!input.is_open())
        return {};

    char sig[4];
    input.read(sig, 4);
    if (sig[0] == 'M' && sig[1] == 'O' && sig[2] == 'D' && sig[3] == 'L')
        return std::make_shared<ModlLoader>(filename);
    if (sig[0] == 'U' && sig[1] == 'M' && sig[2] == 'D' && (sig[3] == 'L' || sig[3] == '2'))
        return std::make_shared<UmdLoader>(filename);
    if (sig[0] == 'H' && sig[1] == 'M' && sig[2] == '\0' && sig[3] == '\0')
        return std::make_shared<HmLoader>(filename);
    if (sig[0] == 'O' && sig[1] == 'B' && sig[2] == 'S' && sig[3] == '\0')
        return std::make_shared<ObstaclesLoader>(filename);

    const std::string ext = Utils::GetFileExt(filename);
    if (Utils::StringEquals(ext, ".obj"))
        return std::make_shared<ObjLoader>(filename);
    if (Utils::StringEquals(ext, ".png") || Utils::StringEquals(ext, ".bmp") || Utils::StringEquals(ext, ".jpg"))
        return std::make_shared<BitmapLoader>(filename);
    if (Utils::StringEquals(ext, ".xml") && UrhoSceneLoader::Identify(filename))
        return std::make_shared<UrhoSceneLoader>(filename);
    if (Utils::StringEquals(ext, ".json") && JsonLoader::Identify(filename))
        return std::make_shared<JsonLoader>(filename);

#if defined(HAVE_ASSIMP)
    return std::make_shared<AssimpLoader>(filename);
#else
    return {};
#endif
}

void Loader::GetExtensions(std::vector<std::string>& result)
{
    ModlLoader::GetExtensions(result);
    UmdLoader::GetExtensions(result);
    HmLoader::GetExtensions(result);
    ObjLoader::GetExtensions(result);
    ObstaclesLoader::GetExtensions(result);
    BitmapLoader::GetExtensions(result);
    JsonLoader::GetExtensions(result);
#if defined(HAVE_ASSIMP)
    AssimpLoader::GetExtensions(result);
#endif
}
