/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <string>
#include <libmath/Mesh.h>
#include <vector>
#include <optional>
#include "SceneObject.h"
#include <sa/Noncopyable.h>

class Loader
{
    NON_COPYABLE(Loader)
    NON_MOVEABLE(Loader)
public:
    static std::shared_ptr<Loader> GetLoader(const std::string& filename, std::optional<std::string> importer = {});
    static void GetExtensions(std::vector<std::string>& result);
    virtual ~Loader() = default;

    virtual std::vector<std::shared_ptr<SceneObject>> Load() = 0;
protected:
    explicit Loader(std::string filename) :
        filename_(std::move(filename))
    { }
    std::string filename_;
};
