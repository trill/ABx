/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HmLoader.h"

#include <libmath/Mesh.h>
#include <libio/Heightmap.h>
#include <libmath/HeightMap.h>
#include <libmath/MathUtils.h>

HmLoader::HmLoader(std::string filename) :
    Loader(std::forward<std::string>(filename))
{ }

std::vector<std::shared_ptr<SceneObject>> HmLoader::Load()
{
    auto hm = std::make_unique<Math::HeightMap>();

    hm->heightData_ = IO::LoadHeightmap(filename_, hm->header_);
    if (hm->heightData_.empty())
    {
        std::cerr << "Heightmap " << filename_ << " does not contain data" << std::endl;
        return {};
    }
    hm->ProcessData();
    std::cout << "patchSize: " << hm->header_.patchSize << std::endl;
    std::cout << "patchWorldSize: " << hm->header_.patchWorldSize << std::endl;
    std::cout << "numPatches: " << hm->header_.numPatches << std::endl;
    std::cout << "numVertices: " << hm->header_.numVertices << std::endl;
    std::cout << "patchWorldOrigin: " << hm->header_.patchWorldOrigin << std::endl;
    std::cout << "min/max height: " << hm->header_.minHeight << "/" << hm->header_.maxHeight << std::endl;

    auto sceneObject =  std::make_shared<SceneObject>(std::make_shared<Math::Mesh>(hm->GetMesh()));
    // When the heightmap is from obstacles, it may contain vertices with y == -inf.
    if (Math::IsNegInfinite(sceneObject->bb_.min_.y_))
    {
        sceneObject->bb_.min_.y_ =  hm->header_.minHeight;
        std::cout << "WARNING: Heightmap contains -inf heights, it is probably generated from obstacles. Representation may not be correct." << std::endl;
    }

    return { sceneObject };
}

void HmLoader::GetExtensions(std::vector<std::string>& result)
{
    result.emplace_back("*.hm");
}
