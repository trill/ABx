/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

extern "C" {
#include <GL/glew.h>
}
#include <libmath/Camera.h>
#include <libmath/Mesh.h>
#include <libmath/MultiLine.h>
#include <libmath/BoundingBox.h>
#include <libmath/Color.h>
#include <sa/Noncopyable.h>
#include <memory>
#include <vector>
#include "SceneObject.h"

class Scene
{
    NON_COPYABLE(Scene)
    NON_MOVEABLE(Scene)
public:
    Scene();
    ~Scene();

    void Init();
    void AddObject(std::shared_ptr<SceneObject> object);
    const Math::BoundingBox& GetSceneBB() const { return sceneBB_; }
    void Render();
    void SetNormalLength(float value);
    float GetNormalLength() const { return normalLength_; }
    size_t GetVertexCount() const;
    size_t GetTriangleCount() const;
    size_t GetObjectCount() const { return objects_.size(); }

    Math::Camera camera_;

    bool wireFrame_{ true };
    bool drawGrid_{ true };
    bool drawBB_{ false };
    bool drawNormals_{ false };
private:
    void DrawLine(const Math::Line& line, const Math::Matrix4& matrix, const Math::Color& color);
    void DrawAxis();
    void DrawGrid();
    void DrawMeshes();
    void DrawObject(const SceneObject& object);
    void DrawBB();

    GLuint VAO_{ 0 };
    GLuint shaderProgram_{ 0 };
    float normalLength_{ 0.05f };
    std::vector<std::shared_ptr<SceneObject>> objects_;
    std::unique_ptr<Math::MultiLine> grid_;
    Math::BoundingBox sceneBB_;
};

