/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Loader.h"

class UrhoSceneLoader : public Loader
{
public:
    static void GetExtensions(std::vector<std::string>& result);
    static bool Identify(const std::string& filename);
    explicit UrhoSceneLoader(std::string filename);
    std::vector<std::shared_ptr<SceneObject>> Load() override;
    std::vector<std::string> searchPaths_;
private:
    [[nodiscard]] std::string FindFile(const std::string& name) const;
    std::string filePath_;
};

