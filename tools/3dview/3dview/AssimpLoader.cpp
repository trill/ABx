/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(HAVE_ASSIMP)

#include "AssimpLoader.h"
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/postprocess.h>     // Post processing flags
#include <assimp/scene.h>
#include <sa/StringTempl.h>
#include <memory>

AssimpLoader::AssimpLoader(std::string filename) :
    Loader(std::forward<std::string>(filename))
{ }

std::vector<std::shared_ptr<SceneObject>> AssimpLoader::Load()
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filename_, aiProcessPreset_TargetRealtime_Quality);
    if (!scene)
        return {};

    std::vector<std::shared_ptr<SceneObject>> result;

    unsigned int numMeshes = scene->mNumMeshes;
    for (unsigned int nm = 0; nm < numMeshes; ++nm)
    {
        std::shared_ptr<SceneObject> obj = std::make_shared<SceneObject>(std::make_shared<Math::Mesh>());
        auto& mesh = obj->GetMesh();

        const aiMesh* ai_mesh = scene->mMeshes[nm];
        // Add vertices
        for (unsigned int i = 0; i < ai_mesh->mNumVertices; ++i)
        {
            if (ai_mesh->HasPositions())
            {
                const aiVector3D& pos = ai_mesh->mVertices[i];
                mesh.vertexData_.emplace_back(pos.x, pos.y, pos.z);
                ++mesh.vertexCount_;
            }
            if (ai_mesh->HasNormals())
            {
                const aiVector3D& norm = ai_mesh->mNormals[i];
                obj->vertexNormals_.emplace_back(norm.x, norm.y, norm.z);
            }
        }

        // Add indices
        for (unsigned int i = 0; i < ai_mesh->mNumFaces; ++i)
        {
            const aiFace& ai_face = ai_mesh->mFaces[i];
            // We triangulate the faces so it must be always 3 vertices per face.
            if (ai_face.mNumIndices == 3)
                mesh.AddTriangle(ai_face.mIndices[0], ai_face.mIndices[1], ai_face.mIndices[2]);
            else
                // or we just don't draw it.
                std::cerr << "Invalid/unsupported number of indices, expected 3 but got " << ai_face.mNumIndices << std::endl;
        }
        obj->UpdateBB();
        result.push_back(std::move(obj));
    }

    return result;
}

void AssimpLoader::GetExtensions(std::vector<std::string>& result)
{
    Assimp::Importer importer;
    aiString exts;
    importer.GetExtensionList(exts);

    std::string stdExts(exts.C_Str());

    auto e = sa::Split(stdExts, ";", false, false);
    result.insert(result.end(), e.begin(), e.end());
}

#endif
