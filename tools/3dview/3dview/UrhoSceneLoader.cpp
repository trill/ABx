/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UrhoSceneLoader.h"
#include <libcommon/FileUtils.h>
#include <libcommon/StringUtils.h>
#include <libio/UrhoSceneLoader.h>
#include <libmath/ConvexHull.h>
#include <libmath/Mesh.h>
#include <libmath/Sphere.h>
#include <libmath/TriangleMesh.h>
#include <sa/StringTempl.h>
#include <fstream>
#include <istream>
#include <sstream>

void UrhoSceneLoader::GetExtensions(std::vector<std::string>& result)
{
    result.emplace_back("*.xml");
}

bool UrhoSceneLoader::Identify(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in.is_open())
        return false;
    std::string line;
    size_t lineCount = 0;
    while (std::getline(in, line))
    {
        if (sa::Contains(line, "<scene"))
            return true;
        ++lineCount;
        if (lineCount > 3)
            break;
    }
    return false;
}

UrhoSceneLoader::UrhoSceneLoader(std::string filename) : Loader(std::forward<std::string>(filename))
{
    // Include trailing slash
    const size_t slashPos = filename_.find_last_of("\\/");
    filePath_ = (std::string::npos == slashPos) ? "./" : filename_.substr(0, slashPos + 1);
    searchPaths_.push_back(Utils::ConcatPath(filePath_, "../"));
    searchPaths_.push_back(Utils::ConcatPath(filePath_, "../../Data"));
}

std::vector<std::shared_ptr<SceneObject>> UrhoSceneLoader::Load()
{
    IO::Urho::SceneLoader loader;
    std::vector<std::shared_ptr<SceneObject>> result;
    loader.onObject_ = [this, &result](const IO::Urho::SceneNode& urhoObject)
    {
        std::string modelFile = FindFile(urhoObject.modelFile_);
        if (!modelFile.empty())
        {
            auto loader = Loader::GetLoader(modelFile);
            if (!loader)
                return;

            auto objects = loader->Load();
            for (const auto& object : objects)
            {
                object->transformation_ = urhoObject.GetWorldTransformation();
                result.push_back(object);
            }
            return;
        }

        std::string terrain = FindFile(urhoObject.heightmap_.heightmapFile);
        if (!terrain.empty())
        {
            auto loader = Loader::GetLoader(terrain);
            if (!loader)
                return;

            auto objects = loader->Load();
            for (const auto& object : objects)
            {
                object->transformation_ = urhoObject.GetWorldTransformation();
                result.push_back(object);
            }
            return;
        }

        // If there is no model add the collision shape
        switch (urhoObject.collision_.shapeType)
        {
        case Math::ShapeType::BoundingBox:
        {
            auto object = std::make_shared<SceneObject>(
                std::make_shared<Math::Mesh>(urhoObject.boundingBox_.GetMesh()));
            object->transformation_ = urhoObject.GetWorldTransformation();
            result.push_back(std::move(object));
            break;
        }
        case Math::ShapeType::Sphere:
        {
            const Math::Sphere sphere = Math::Sphere(
                urhoObject.GetWorldTransformation().position_, urhoObject.spereRadius_);
            auto object = std::make_shared<SceneObject>(std::make_shared<Math::Mesh>(sphere.GetMesh()));
            result.push_back(std::move(object));
            break;
        }
        case Math::ShapeType::TriangleMesh:
        {
            std::string model = FindFile(urhoObject.collision_.model);
            if (!model.empty())
            {
                auto loader = Loader::GetLoader(model);
                if (loader)
                {
                    auto objects = loader->Load();
                    if (!objects.empty())
                    {
                        const auto& first = objects.front();
                        auto object = std::make_shared<SceneObject>(
                            std::make_shared<Math::TriangleMesh>(first->GetMesh()));
                        object->transformation_ = urhoObject.GetWorldTransformation();
                        result.push_back(std::move(object));
                    }
                }
            }
            break;
        }
        case Math::ShapeType::ConvexHull:
        {
            std::string model = FindFile(urhoObject.collision_.model);
            if (!model.empty())
            {
                auto loader = Loader::GetLoader(model);
                if (loader)
                {
                    auto objects = loader->Load();
                    if (!objects.empty())
                    {
                        const auto& first = objects.front();
                        auto object = std::make_shared<SceneObject>(
                            std::make_shared<Math::ConvexHull>(first->GetMesh().vertexData_));
                        object->transformation_ = urhoObject.GetWorldTransformation();
                        result.push_back(std::move(object));
                    }
                }
            }
            break;
        }
        default:
            break;
        }
    };

    if (!loader.Load(filename_))
    {
        std::cerr << loader.GetLastError() << std::endl;
        return {};
    }

    return result;
}

std::string UrhoSceneLoader::FindFile(const std::string& name) const
{
    if (name.empty())
        return "";

    if (Utils::FileExists(name))
        return name;

    for (const auto& p : searchPaths_)
    {
        std::string f = Utils::ConcatPath(p, name);
        if (Utils::FileExists(f))
            return f;
    }
    return "";
}
