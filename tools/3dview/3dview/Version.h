/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/CommonConfig.h>

#define THREEDVIEW_VERSION_MAJOR 0
#define THREEDVIEW_VERSION_MINOR 1
#define THREEDVIEW_YEAR CURRENT_YEAR
#define THREEDVIEW_PRODUCT_NAME "ABx 3D Mesh Viewer"

#define AB_THREEDVIEW_VERSION AB_VERSION_CREATE(THREEDVIEW_VERSION_MAJOR, THREEDVIEW_VERSION_MINOR)
