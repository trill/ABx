/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Loader.h"

class ObstaclesLoader : public Loader
{
public:
    static void GetExtensions(std::vector<std::string>& result);
    explicit ObstaclesLoader(std::string filename);
    std::vector<std::shared_ptr<SceneObject>> Load() override;
};
