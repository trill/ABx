/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#if defined(HAVE_ASSIMP)

#include "Loader.h"

class AssimpLoader : public Loader
{
public:
    static void GetExtensions(std::vector<std::string>& result);
    explicit AssimpLoader(std::string filename);
    std::vector<std::shared_ptr<SceneObject>> Load() override;
};

#endif
