/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <sa/Compiler.h>
extern "C" {
#include <GL/glew.h>
}
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4505)
#include <GL/freeglut.h>
PRAGMA_WARNING_POP
#ifdef AB_UNIX
#include <unistd.h>
#endif
#include <AB/CommonConfig.h>
#include <chillout.h>
#include <libcommon/FileUtils.h>
#include <libcommon/ConfigFile.h>
#include <libmath/Camera.h>
#include <libmath/Line.h>
#include <libmath/Mesh.h>
#include <libmath/BoundingBox.h>
#include <libmath/VectorMath.h>
#include <libmath/MultiLine.h>
#include <sa/ArgParser.h>
#include <sa/FrameLimiter.h>
#include <sa/StringTempl.h>
#include <sa/time.h>
#include <sa/Container.h>
#include <sa/path.h>
#include <chrono>
#include <filesystem>
#include <memory>
#include <sstream>
#include <thread>
#include "Loader.h"
#include "Version.h"
#include "Scene.h"
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#undef STB_IMAGE_IMPLEMENTATION
PRAGMA_WARNING_POP

// Most simple vertex shader, just transform the vertices
static constexpr const char* vertexShaderString = R"glsl(
#version 330 core
uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
in vec3 position;
void main()
{
  gl_Position = proj * view * model * vec4(position, 1.0);
}
)glsl";
// Most simple fragment shader, just draw the given color
static constexpr const char* fragmentShaderString = R"glsl(
#version 330 core
uniform vec3 my_color;
out vec4 color;
void main()
{
  color = vec4(my_color, 1.0);
}
)glsl";

static Scene scene;
static Math::IntVector2 mouseDownPos;
static bool mouseRightDown = false;
static bool mouseLeftDown = false;
static int keyboardMods = 0;

static std::string filename;
static int32_t maxFPS = 60;
static std::array<bool, 256> downKeys;

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "version", { "-v", "--version" }, "Show version", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "info", { "-info", "--show-info" }, "Show info", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "solid", { "-s", "--solid" }, "Fill triangles", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "nogrid", { "-G", "--dont-draw-grid" }, "Don't draw Grid", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "bb", { "-b", "--draw-boundingbox" }, "Draw Bounding box", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "normals", { "-n", "--draw-face-normals" }, "Draw Face normals", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "normal_length", { "-l", "--normal-length" }, "Length of Face normals", false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "maxfps", { "-f", "--max-fps" }, "Limit FPS (0 for no limit, default 60)", false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "fov", { "-fov", "--field-of-view" }, "Field of view angle in degree (default 60)", false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "nearclip", { "-nc", "--near-clip" }, "Near clip", false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "farclip", { "-fc", "--far-clip" }, "Far clip", false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "zoom", { "-z", "--zoom" }, "Zoom", false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "importer", { "-i", "--importer" }, "Use specific importer, see IMPORTER for possible values", false, true, sa::ArgParser::OptionType::String });
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("3dview", _cli, "", "<input file>");
    std::cout << std::endl;
    std::cout << "IMPORTER" << std::endl;
    std::cout << "    default\tLet 3dview decide (implied)" << std::endl;
    std::cout << "    modl\tABx .mdl files" << std::endl;
    std::cout << "    hm\t\tABx .hm files" << std::endl;
    std::cout << "    obstacles\tABx .obstacles files" << std::endl;
    std::cout << "    umd\t\tUrho3D's .mdl files" << std::endl;
    std::cout << "    uscene\tUrho3D .xml scene files" << std::endl;
    std::cout << "    obj\t\tBuilt in .obj files" << std::endl;
    std::cout << "    bitmap\tBitmap files files, .png and .bmp" << std::endl;
    std::cout << "    json\tJSON heightmap loader" << std::endl;
#if defined(HAVE_ASSIMP)
    std::cout << "    assimp\tAssimp importer (multiple file formats)" << std::endl;
#endif
    std::cout << std::endl;
    std::cout << "SHORTCUTS" << std::endl;
    std::cout << "    f\t\tToggle wireframe/solid" << std::endl;
    std::cout << "    g\t\tToggle draw grid" << std::endl;
    std::cout << "    b\t\tToggle draw bounding box" << std::endl;
    std::cout << "    n\t\tToggle draw face normals" << std::endl;
    std::cout << "    l\t\tIncrease normal vector length" << std::endl;
    std::cout << "    L\t\tDecrease normal vector length" << std::endl;
    std::cout << "    w\t\tMove camera forward" << std::endl;
    std::cout << "    s\t\tMove camera backward" << std::endl;
    std::cout << "    a\t\tMove camera left" << std::endl;
    std::cout << "    d\t\tMove camera right" << std::endl;
    std::cout << "    e\t\tMove camera up" << std::endl;
    std::cout << "    x\t\tMove camera down" << std::endl;
    std::cout << "    Shift\tMove faster" << std::endl;
    std::cout << "    RMB\t\tMouse look" << std::endl;
    std::cout << "    Mouse wheel\tZoom" << std::endl;
    std::cout << "    y\t\tReverse camera" << std::endl;
    std::cout << "    v\t\tIncrease field of view" << std::endl;
    std::cout << "    V\t\tDecrease field of view" << std::endl;
    std::cout << "    z\t\tFlip Z-Axis" << std::endl;
    std::cout << "    Esc\t\tQuit" << std::endl;
    std::cout << std::endl;
    std::cout << "SUPPORTED FILES" << std::endl;
    std::vector<std::string> exts;
    Loader::GetExtensions(exts);
    sa::DeleteDuplicates(exts);
    std::cout << "    " << sa::CombineString(exts, std::string(";")) << std::endl;
}

static void ShowInfo()
{
    std::cout << "3dview - ABx 3D Mesh Viewer" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static void ShowVersion()
{
    std::cout << "3dview " << THREEDVIEW_VERSION_MAJOR << "." << THREEDVIEW_VERSION_MINOR << std::endl;
}

static void RenderScene()
{
    sa::FrameLimiter limiter(maxFPS, [](int64_t millis) {
        std::this_thread::sleep_for(std::chrono::milliseconds(millis));
    });

    if (downKeys['w'] || downKeys['W'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Forward);
    if (downKeys['s'] || downKeys['S'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Back);
    if (downKeys['a'] || downKeys['A'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Left);
    if (downKeys['d'] || downKeys['D'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Right);
    if (downKeys['x'] || downKeys['X'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Down);
    if (downKeys['e'] || downKeys['E'])
        scene.camera_.transformation_.MoveXYZ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 0.4f : 0.1f, Math::Vector3_Up);

    scene.camera_.reverse_ = downKeys['y'];
    scene.Render();

    glutSwapBuffers();
    glutPostRedisplay();
}

static void ChangeSize(GLsizei w, GLsizei h)
{
    glViewport(0, 0, w, h);
    scene.camera_.Resize(w, h);
}

static void Mouse(int button, int state, int x, int y)
{
    keyboardMods = glutGetModifiers();
    if (button == GLUT_RIGHT_BUTTON)
    {
        mouseRightDown = state == GLUT_DOWN;
        mouseDownPos = { x, y };
    }
    else if (button == GLUT_LEFT_BUTTON)
    {
        mouseLeftDown = state == GLUT_DOWN;
        mouseDownPos = { x, y };
    }
    else if (button == GLUT_MIDDLE_BUTTON)
    {
        if (state == GLUT_DOWN)
        {
            // Reset
            scene.camera_.yaw_ = 0.0f;
            scene.camera_.pitch_ = 0.0f;
            scene.camera_.SetZoom(1.0f);
            downKeys.fill(false);
        }
    }
    else if (button == 3 || button == 4)
    {
        // Mouse Wheel -> Zoom in/out
        if (button == 3)
            scene.camera_.SetZoom(scene.camera_.GetZoom() + 0.1f);
        else
            scene.camera_.SetZoom(scene.camera_.GetZoom() - 0.1f);
    }
}

static void MouseMove(int x, int y)
{
    if (mouseRightDown)
    {
        // Mouse look
        scene.camera_.yaw_ -= Math::DegToRad((float)(mouseDownPos.x_ - x)) * 0.05f;
        scene.camera_.pitch_ -= Math::DegToRad((float)(mouseDownPos.y_ - y)) * 0.05f;
    }
    if (mouseLeftDown)
    {
        scene.camera_.ApplyAngles();
        float moveX = ((float)x - (float)mouseDownPos.x_) / ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 250.0f : 500.0f);
        float moveY = ((float)y - (float)mouseDownPos.y_) / ((keyboardMods & GLUT_ACTIVE_SHIFT) ? 250.0f : 500.0f);

        scene.camera_.transformation_.MoveXYZ(moveX, Math::Vector3_Left);
        scene.camera_.transformation_.MoveXYZ(moveY, Math::Vector3_Up);
    }
    mouseDownPos = { x, y };
}

static void Keyboard(unsigned char key, int, int)
{
    downKeys[key] = true;
    keyboardMods = glutGetModifiers();

    switch (key)
    {
    case 'f':
        scene.wireFrame_ = !scene.wireFrame_;
        break;
    case 'g':
        scene.drawGrid_ = !scene.drawGrid_;
        break;
    case 'b':
        scene.drawBB_ = !scene.drawBB_;
        break;
    case 'n':
        scene.drawNormals_ = !scene.drawNormals_;
        break;
    case 'l':
        scene.SetNormalLength(scene.GetNormalLength() + 0.01f);
        break;
    case 'L':
        scene.SetNormalLength(scene.GetNormalLength() - 0.01f);
        break;
    case 'v':
        scene.camera_.SetFov(scene.camera_.GetFov() + Math::DegToRad(10.0f));
        std::cout << "FOV " << Math::RadToDeg(scene.camera_.GetFov()) << " Deg" << std::endl;
        break;
    case 'V':
        scene.camera_.SetFov(scene.camera_.GetFov() - Math::DegToRad(10.0f));
        std::cout << "FOV " << Math::RadToDeg(scene.camera_.GetFov()) << " Deg" << std::endl;
        break;
    case 'w':
    case 'W':
    case 's':
    case 'S':
    case 'a':
    case 'A':
    case 'd':
    case 'D':
    case 'e':
    case 'E':
    case 'x':
    case 'X':
        scene.camera_.ApplyAngles();
        break;
    case 'z':
        scene.camera_.flipZ_ = !scene.camera_.flipZ_;
        break;
    case 27: // Escape
        glutLeaveMainLoop();
        break;
    default:
        break;
    }
}

static void KeyboardUp(unsigned char key, int, int)
{
    downKeys[key] = false;
}

static void Init(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitContextVersion(3, 2);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    glutInitWindowSize(1280, 720);
    scene.camera_.Resize(1280, 720);
    scene.camera_.flipZ_ = true;

    std::stringstream ss;
    if (!filename.empty())
        ss << filename << " - ";
    ss << "ABx 3D Mesh Viewer";
    glutCreateWindow(ss.str().c_str());

    scene.Init();

    glutDisplayFunc(RenderScene);
    glutReshapeFunc(ChangeSize);
    glutMouseFunc(Mouse);
    glutKeyboardFunc(Keyboard);
    glutKeyboardUpFunc(KeyboardUp);
    glutMotionFunc(MouseMove);
}

static bool LoadFile(const std::string& filename, std::optional<std::string> importer)
{
    if (!Utils::FileExists(filename))
    {
        std::cerr << "File " << filename << " not found" << std::endl;
        return false;
    }

    auto loader = Loader::GetLoader(filename, std::move(importer));
    if (!loader)
    {
        std::cerr << "Unknown file type " << filename << std::endl;
        return false;
    }
    auto objects = loader->Load();
    for (const auto& object : objects)
    {
        if (object->GetMesh().indexCount_ == 0)
        {
            std::cerr << "Mesh has no faces" << std::endl;
            continue;
        }
        if (!object->GetMesh().IsTriangles())
        {
            std::cerr << "Mesh faces must be triangles" << std::endl;
            continue;
        }
        scene.AddObject(object);
    }
    return !objects.empty();
}

static std::string FindConfigFile()
{
#ifdef AB_UNIX
    static constexpr const char* CONFIG_FILE = "~/.config/3dview.cfg";
    std::string file = Utils::ExpandPath(CONFIG_FILE);
    if (Utils::FileExists(file))
        return file;
#endif
    sa::path appFile = sa::path(Utils::GetExeName()).directory() / sa::path("3dview.cfg");
    if (Utils::FileExists(appFile.string()))
        return appFile.string();

    return "";
}

static void ReadConfig(const sa::ArgParser::Values& parsedArgs)
{
    IO::ConfigFile cfg;
    std::string configFile = FindConfigFile();
    if (cfg.Load(configFile))
        std::cout << "Found config file " << configFile << std::endl;

    scene.wireFrame_ = !sa::ArgParser::GetValue<bool>(parsedArgs, "solid").value_or(cfg.Get<bool>("solid", false));
    scene.drawGrid_ = !sa::ArgParser::GetValue<bool>(parsedArgs, "nogrid").value_or(cfg.Get<bool>("nogrid", false));
    scene.drawBB_ = sa::ArgParser::GetValue<bool>(parsedArgs, "bb").value_or(cfg.Get("bb", false));
    scene.drawNormals_ = sa::ArgParser::GetValue<bool>(parsedArgs, "normals").value_or(cfg.Get<bool>("normals", false));
    scene.SetNormalLength((sa::ArgParser::GetValue<float>(parsedArgs, "normal_length").value_or(cfg.Get<float>("normal_length", 0.02f))));
    scene.camera_.SetFov((sa::ArgParser::GetValue<float>(parsedArgs, "fov").value_or(cfg.Get<float>("fov", scene.camera_.GetFov()))));
    scene.camera_.SetNearClip((sa::ArgParser::GetValue<float>(parsedArgs, "nearclip").value_or(cfg.Get<float>("nearclip", scene.camera_.GetNearClip()))));
    scene.camera_.SetFarClip((sa::ArgParser::GetValue<float>(parsedArgs, "farclip").value_or(cfg.Get<float>("farclip", scene.camera_.GetFarClip()))));
    scene.camera_.SetZoom((sa::ArgParser::GetValue<float>(parsedArgs, "zoom").value_or(cfg.Get<float>("zoom", scene.camera_.GetZoom()))));
    maxFPS = sa::ArgParser::GetValue<int32_t>(parsedArgs, "maxfps").value_or(cfg.Get<float>("maxfps", 60));
}

int main(int argc, char** argv)
{
    namespace fs = std::filesystem;
    auto& chillout = Debug::Chillout::getInstance();
    chillout.init("3dview", fs::temp_directory_path().string());
    chillout.setBacktraceCallback([](const char* const stackEntry) { fprintf(stderr, "  %s\n", stackEntry); });

    chillout.setCrashCallback(
        [&chillout](const char* const message)
        {
            std::cerr << message << std::endl;
            chillout.backtrace();
#ifdef AB_WINDOWS
            chillout.createCrashDump();
#endif
        });

    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    if (sa::ArgParser::GetValue<bool>(parsedArgs, "help").value_or(false))
    {
        ShowInfo();
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cerr << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }
    if (sa::ArgParser::GetValue<bool>(parsedArgs, "version").value_or(false))
    {
        ShowVersion();
        return 0;
    }

    if (sa::ArgParser::GetValue<bool>(parsedArgs, "info").value_or(false))
        ShowInfo();
    auto fval = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    if (!fval.has_value())
    {
        std::cerr << "No input file provided" << std::endl << std::endl;
        ShowHelp(_cli);
        return 1;
    }
    filename = fval.value();
    sa::time::timer timer;
    if (!LoadFile(filename, sa::ArgParser::GetValue<std::string>(parsedArgs, "importer")))
        return 1;

    std::cout << "Loaded " << filename << " in " << timer.elapsed_millis() << "ms: " << scene.GetObjectCount() << " object(s), " <<
        scene.GetVertexCount() << " vertices, " << scene.GetTriangleCount() <<
        " triangles, BB " << scene.GetSceneBB() << std::endl;

    ReadConfig(parsedArgs);

    Init(argc, argv);

    glutMainLoop();

    return 0;
}
