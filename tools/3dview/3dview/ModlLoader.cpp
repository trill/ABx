/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ModlLoader.h"
#include <libmath/Mesh.h>
#include <libio/Mesh.h>

ModlLoader::ModlLoader(std::string filename) :
    Loader(std::forward<std::string>(filename))
{ }

std::vector<std::shared_ptr<SceneObject>> ModlLoader::Load()
{
    auto result = std::make_shared<Math::Mesh>();

    Math::BoundingBox bb;
    if (IO::LoadMesh(filename_, *result, bb))
    {
        auto sceneObject = std::make_shared<SceneObject>(std::move(result));
        return { sceneObject };
    }
    return {};
}

void ModlLoader::GetExtensions(std::vector<std::string>& result)
{
    result.emplace_back("*.mdl");
}
