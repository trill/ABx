/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Loader.h"

class JsonLoader : public Loader
{
public:
    static void GetExtensions(std::vector<std::string>& result);
    static bool Identify(const std::string& filename);
    explicit JsonLoader(std::string filename);
    std::vector<std::shared_ptr<SceneObject>> Load() override;
};
