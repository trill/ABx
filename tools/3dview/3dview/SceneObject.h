/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/BoundingBox.h>
#include <libmath/Mesh.h>
#include <libmath/MultiLine.h>
#include <libmath/Transformation.h>
#include <sa/Noncopyable.h>
#include <memory>

class SceneObject
{
    NON_COPYABLE(SceneObject)
    NON_MOVEABLE(SceneObject)
public:
    explicit SceneObject(std::shared_ptr<Math::Mesh> mesh);
    ~SceneObject();

    const Math::MultiLine& GetNormals(float length) const;
    size_t GetVertexCount() const;
    size_t GetTriangleCount() const;
    const Math::Mesh& GetMesh() const;
    Math::Mesh& GetMesh();
    Math::Matrix4 GetMatrix() const { return transformation_.GetMatrix(); }
    void UpdateBB();
    Math::Transformation transformation_;
    std::vector<Math::Vector3> vertexNormals_;
    Math::BoundingBox bb_;

private:
    std::shared_ptr<Math::Mesh> mesh_;
    mutable std::unique_ptr<Math::MultiLine> normals_;
    mutable float normalLength_{ 0.0f };
};
