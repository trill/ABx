/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UmdLoader.h"

#include <libmath/Mesh.h>
#include <libio/UrhoModel.h>

UmdLoader::UmdLoader(std::string filename) :
    Loader(std::forward<std::string>(filename))
{ }

std::vector<std::shared_ptr<SceneObject>> UmdLoader::Load()
{
    auto result = std::make_shared<Math::Mesh>();

    if (IO::Urho::LoadModel(filename_, *result))
    {
        auto sceneObject = std::make_shared<SceneObject>(std::move(result));
        return { sceneObject };
    }
    return {};
}

void UmdLoader::GetExtensions(std::vector<std::string>& result)
{
    result.emplace_back("*.mdl");
}
