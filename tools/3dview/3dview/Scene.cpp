/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

// TODO: Draw MultiLines with glMultiDrawArrays()

#include "Scene.h"
#include <libmath/MathUtils.h>
#include <libmath/VectorMath.h>

// Most simple vertex shader, just transform the vertices
static constexpr const char* vertexShaderString = R"glsl(
#version 330 core
uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
layout(location = 0) in vec3 position;
void main()
{
  gl_Position = proj * view * model * vec4(position, 1.0);
}
)glsl";
// Most simple fragment shader, just draw the given color
static constexpr const char* fragmentShaderString = R"glsl(
#version 330 core
uniform vec4 my_color;
out vec4 color;
void main()
{
  color = my_color;
}
)glsl";

static constexpr Math::Color GREY = { 0.3f, 0.3f, 0.3f };
static constexpr Math::Color YELLOW = { 1.0f, 0.937f, 0.0f };
static constexpr Math::Color LIGHT_BLUE = { 0.4f, 0.6f, 1.0f };

Scene::Scene()
{
    camera_.transformation_.position_ = { 0.0f, 1.5f, 0.0f };
}

Scene::~Scene()
{
    if (VAO_)
        glDeleteVertexArrays(1, &VAO_);
    if (shaderProgram_)
        glDeleteProgram(shaderProgram_);
}

void Scene::Init()
{
    glEnable(GL_BLEND);
    // Line antialiasing
    glEnable(GL_LINE_SMOOTH);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glewExperimental = GL_TRUE;
    glewInit();

    const auto compile_shader = [](const GLint type, const char* str) -> GLuint
    {
        GLuint id = glCreateShader(type);
        glShaderSource(id, 1, &str, NULL);
        glCompileShader(id);
        return id;
    };
    GLuint vid = compile_shader(GL_VERTEX_SHADER, vertexShaderString);
    GLuint fid = compile_shader(GL_FRAGMENT_SHADER, fragmentShaderString);
    shaderProgram_ = glCreateProgram();
    glAttachShader(shaderProgram_, vid);
    glAttachShader(shaderProgram_, fid);
    glLinkProgram(shaderProgram_);
    GLint status = 0;
    glGetProgramiv(shaderProgram_, GL_LINK_STATUS, &status);
    glDeleteShader(vid);
    glDeleteShader(fid);

    glGenVertexArrays(1, &VAO_);
}

void Scene::AddObject(std::shared_ptr<SceneObject> object)
{
    if (!object)
        return;

    sceneBB_.Merge(object->bb_);

    objects_.push_back(std::move(object));
}

size_t Scene::GetVertexCount() const
{
    size_t result = 0;
    for (const auto& mesh : objects_)
        result += mesh->GetVertexCount();
    return result;
}

size_t Scene::GetTriangleCount() const
{
    size_t result = 0;
    for (const auto& mesh : objects_)
        result += mesh->GetTriangleCount();
    return result;
}

void Scene::SetNormalLength(float value)
{
    if (Math::Equals(normalLength_, value))
        return;
    if (value > 0.0f)
    {
        normalLength_ = value;
    }
}

void Scene::Render()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderProgram_);
    GLint proj_loc = glGetUniformLocation(shaderProgram_, "proj");
    // Let it transpose the matrix
    glUniformMatrix4fv(proj_loc, 1, GL_TRUE, camera_.GetProjection().Data());
    GLint view_loc = glGetUniformLocation(shaderProgram_, "view");
    glUniformMatrix4fv(view_loc, 1, GL_TRUE, camera_.GetView().Data());

    if (drawGrid_)
    {
        DrawGrid();
        DrawAxis();
    }
    DrawMeshes();
}

void Scene::DrawLine(const Math::Line& line, const Math::Matrix4& matrix, const Math::Color& color)
{
    GLuint VBO = 0;
    GLuint EBO = 0;
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, line.VertexDataSize(), line.VertexData(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    GLint model_loc = glGetUniformLocation(shaderProgram_, "model");
    // Let it transpose the matrix
    glUniformMatrix4fv(model_loc, 1, GL_TRUE, matrix.Data());
    GLint color_location = glGetUniformLocation(shaderProgram_, "my_color");
    glUniform4fv(color_location, 1, color.Data());

    glBindVertexArray(VAO_);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);

    glDeleteBuffers(1, &VBO);
}

void Scene::DrawGrid()
{
    if (!sceneBB_.IsDefined())
        return;

    if (!grid_)
    {
        grid_ = std::make_unique<Math::MultiLine>();
        const int minx = std::min(-1, (int)std::floor(sceneBB_.min_.x_));
        const int minz = std::min(-1, (int)std::floor(sceneBB_.min_.z_));
        const int maxx = std::max(1, (int)std::ceil(sceneBB_.max_.x_));
        const int maxz = std::max(1, (int)std::ceil(sceneBB_.max_.z_));
        for (int z = minz; z <= maxz; ++z)
        {
            grid_->lines_.emplace_back(Math::Vector3{ (float)minx, 0.0f, (float)z }, Math::Vector3{ (float)maxx, 0.0f, (float)z });
        }
        for (int x = minx; x <= maxx; ++x)
        {
            grid_->lines_.emplace_back(Math::Vector3{ (float)x, 0.0f, (float)minz }, Math::Vector3{ (float)x, 0.0f, (float)maxz });
        }
    }
    for (const auto& line : grid_->lines_)
    {
        if (Math::Equals(line.start_.x_, 0.0f) || Math::Equals(line.start_.z_, 0.0f))
            glLineWidth(2.0f);
        else
            glLineWidth(1.0f);
        DrawLine(line, Math::Matrix4_Identity, GREY);
    }
    glLineWidth(1.0f);
}

void Scene::DrawAxis()
{
    if (!sceneBB_.IsDefined())
        return;

    const int minx = std::min(-1, (int)std::floor(sceneBB_.min_.x_));
    const int minz = std::min(-1, (int)std::floor(sceneBB_.min_.z_));
    const int maxx = std::max(1, (int)std::ceil(sceneBB_.max_.x_));
    const int maxz = std::max(1, (int)std::ceil(sceneBB_.max_.z_));
    const int maxy = std::max(1, (int)std::ceil(sceneBB_.max_.y_));
    const Math::Line xLine = { { (float)minx, 0.0f, (float)minz }, { (float)maxx, 0.0f, (float)minz } };
    DrawLine(xLine, Math::Matrix4_Identity, Math::Color_Red);

    const Math::Line yLine = { { (float)minx, 0.0f, (float)minz }, { (float)minx, (float)maxy, (float)minz } };
    DrawLine(yLine, Math::Matrix4_Identity, Math::Color_Green);

    const Math::Line zLine = { { (float)minx, 0.0f, (float)minz }, { (float)minx, 0.0f, (float)maxz } };
    DrawLine(zLine, Math::Matrix4_Identity, Math::Color_Blue);
}

void Scene::DrawBB()
{
    if (!sceneBB_.IsDefined())
        return;

    glLineWidth(2.0f);
    const auto edges = sceneBB_.GetEdges();
    for (const auto& edge : edges)
        DrawLine(edge, Math::Matrix4_Identity, YELLOW);
    glLineWidth(1.0f);
}

void Scene::DrawObject(const SceneObject& object)
{
    const auto matrix = object.GetMatrix();
    GLuint VBO = 0;
    GLuint EBO = 0;
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, object.GetMesh().VertexDataSize(), object.GetMesh().VertexData(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, object.GetMesh().IndexDataSize(), object.GetMesh().IndexData(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    GLint model_loc = glGetUniformLocation(shaderProgram_, "model");
    // Let it transpose the matrix
    glUniformMatrix4fv(model_loc, 1, GL_TRUE, matrix.Data());
    GLint color_location = glGetUniformLocation(shaderProgram_, "my_color");
    glUniform4fv(color_location, 1, Math::Color_White.Data());

       // Draw mesh as wireframe or filled
    glPolygonMode(GL_FRONT_AND_BACK, wireFrame_ ? GL_LINE : GL_FILL);
    glBindVertexArray(VAO_);
    glDrawElements(GL_TRIANGLES, (int)object.GetMesh().indexCount_, GL_UNSIGNED_INT, nullptr);

    glBindVertexArray(0);

    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    if (drawNormals_)
    {
        const auto& normals = object.GetNormals(normalLength_);
        for (const auto& line : normals.lines_)
        {
            DrawLine(line, matrix, LIGHT_BLUE);
        }
    }
    if (drawBB_)
    {
        const Math::BoundingBox& meshBB = object.bb_;
        const auto edges = meshBB.GetEdges();
        for (const auto& edge : edges)
            DrawLine(edge, matrix, YELLOW);
    }
}

void Scene::DrawMeshes()
{
    for (const auto& object : objects_)
    {
        DrawObject(*object);
    }
    if (drawBB_)
        DrawBB();
}
