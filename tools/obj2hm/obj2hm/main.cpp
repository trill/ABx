/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <libio/Heightmap.h>
#include <sa/Compiler.h>
#include <iostream>
#include <libmath/VectorMath.h>
#include <sa/ArgParser.h>
#include <libmath/HeightMapTools.h>
#include <libcommon/StringUtils.h>
#include <fstream>
#include <iomanip>
#include <libio/Mesh.h>

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image.h>
#include <stb_image_write.h>
#undef STB_IMAGE_IMPLEMENTATION
#undef STB_IMAGE_WRITE_IMPLEMENTATION
PRAGMA_WARNING_POP

static bool flip = false;

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "comps", { "-c", "--components" }, "Number of color components, default 1",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetwidth", { "-W", "--target-width" }, "Target image width",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetheight", { "-H", "--target-height" }, "Target image height",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "minheight", { "-minh", "--min-height" }, "Min height value for PNG images (default min height of source)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "maxheight", { "-maxh", "--max-height" }, "Max height value for PNG images (default max height of source)",
        false, true, sa::ArgParser::OptionType::Float });

    cli.push_back({ "nvx", { "-nvx", "--number-vertices-x" }, "Number of vertices (.hm only)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "nvy", { "-nvy", "--number-vertices-y" }, "Number of vertices (.hm only)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "ps", { "-ps", "--patch-size" }, "Patch size (.hm only)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "pwsx", { "-pwsx", "--patch-world-size-x" }, "Patch world size (.hm only)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "pwsy", { "-pwsy", "--patch-world-size-y" }, "Patch world size (.hm only)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "npx", { "-npx", "--number-patches-x" }, "Number of patches (.hm only)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "npy", { "-npy", "--number-patches-y" }, "Number of patches (.hm only)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "pwox", { "-pwox", "--patch-world-origin-x" }, "Patch world origin (.hm only)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "pwoy", { "-pwoy", "--patch-world-origin-y" }, "Patch world origin (.hm only)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "flip", { "-f", "--flip" }, "Vertically flip image (.png output only)",
        false, false, sa::ArgParser::OptionType::None });

    cli.push_back({ "output", { "-o", "--output" }, "Output file, either .png, .hm, .json or .txt",
        true, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "input", { }, "Input .obj, .obstacles or .hm file",
        true, true, sa::ArgParser::OptionType::String });
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("obj2hm", _cli, "Construct height map from 3D mesh");
}

static void ShowInfo()
{
    std::cout << "obj2hm - Construct height map image from 3D mesh" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static void CreateImage(const ea::vector<float>& heights, const std::string& filename, int comps, int sizeX, int sizeY,
    float minHeight, float maxHeight)
{
    unsigned char* data = (unsigned char*)malloc((size_t)sizeX * (size_t)sizeY * (size_t)comps);
    memset(data, 0, (size_t)sizeX * (size_t)sizeY * (size_t)comps);
    IO::CreateHeightmapImage(heights, data, comps, { sizeX, sizeY }, minHeight, maxHeight, flip);

    stbi_write_png(filename.c_str(), sizeX, sizeY, comps, data, sizeX * comps);

    std::cout << "Created PNG heightmap " << filename << " width " << sizeX << " height " << sizeY << std::endl;
    free(data);
}

static void CreateImage(const Math::Mesh& shape, const std::string& filename, int comps, int sizeX, int sizeY,
    float minHeight, float maxHeight)
{
    int width = 0;
    int height = 0;
    float minh = std::numeric_limits<float>::max();
    float maxh = std::numeric_limits<float>::lowest();
    const ea::vector<float> heights = Math::CreateHeightMapFromMesh(shape, sizeX, sizeY, width, height, minh, maxh);
    minHeight = std::min(minh, minHeight);
    maxHeight = std::max(maxh, maxHeight);
    CreateImage(heights, filename, comps, width, height, minHeight, maxHeight);
}

static void CreateHeightmap(const ea::vector<float>& heights, const std::string& filename, int sizeX, int sizeY,
    const Math::HeightmapHeader header)
{
    std::cout << "Num vertices: " << header.numVertices << ", patch size: " << header.patchSize <<
        ", patch world size: " << header.patchWorldSize << ", num patches: " << header.numPatches <<
        ", patch world origin: " << header.patchWorldOrigin << ", min/max height: " << header.minHeight << "/" << header.maxHeight <<
        ", n height values: " << heights.size() << std::endl;
    const std::string ext = Utils::GetFileExt(filename);
    if (Utils::SameFilename(ext, ".txt"))
    {
        if (!IO::SaveHeightmapText(filename, heights, { sizeX, sizeY }, header))
            std::cerr << "Could not create TXT heightmap " << filename << std::endl;
        else
            std::cout << "Created TXT heightmap " << filename << std::endl;
        return;
    }
    if (Utils::SameFilename(ext, ".json"))
    {
        if (!IO::SaveHeightmapJSON(filename, heights, { sizeX, sizeY }, header))
            std::cerr << "Could not create JSON heightmap " << filename << std::endl;
        else
            std::cout << "Created JSON heightmap " << filename << std::endl;
        return;
    }

    if (!IO::SaveHeightmap(filename, heights, header))
        std::cerr << "Could not create HM heightmap " << filename << std::endl;
    else
        std::cout << "Created HM heightmap " << filename << std::endl;
}

static void CreateHeightmap(const Math::Mesh& shape, const std::string& filename, int sizeX, int sizeY,
    Math::HeightmapHeader& header)
{
    int width = 0; int height = 0;
    const ea::vector<float> heights = Math::CreateHeightMapFromMesh(shape, sizeX, sizeY, width, height,
        header.minHeight, header.maxHeight);
    CreateHeightmap(heights, filename, sizeX, sizeY, header);
}

static bool LoadHeightmap(const std::string& filename, ea::vector<float>& heights,
    Math::HeightmapHeader& header)
{
    heights = IO::LoadHeightmap(filename, header);
    return !heights.empty();
}

int main(int argc, char** argv)
{
    ShowInfo();
    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }

    flip = sa::ArgParser::GetValue<bool>(parsedArgs, "flip", false);

    auto actval = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    if (!actval.has_value())
    {
        std::cerr << "No input file provided" << std::endl;
        return 1;
    }
    int comps = sa::ArgParser::GetValue<int>(parsedArgs, "comps", 1);
    if (comps < 1 || comps > 3)
    {
        std::cerr << "Components must between 1 and 3" << std::endl;
        return 1;
    }

    std::string inputFile = actval.value();
    Math::Mesh shape;
    ea::vector<float> heights;
    const std::string ext = Utils::GetFileExt(inputFile);

    Math::HeightmapHeader header;

    if (Utils::StringEquals(ext, ".obj"))
    {
        if (!IO::LoadMeshFromOBJ(inputFile, shape))
        {
            std::cerr << "Error loading " << inputFile << std::endl;
            return 1;
        }
    }
    else if (Utils::StringEquals(ext, ".obstacles"))
    {
        if (!IO::LoadObstacles(inputFile, shape))
        {
            std::cerr << "Error loading " << inputFile << std::endl;
            return 1;
        }
    }
    else if (Utils::StringEquals(ext, ".hm"))
    {
        if (!LoadHeightmap(inputFile, heights, header))
        {
            std::cerr << "Error loading " << inputFile << std::endl;
            return 1;
        }
    }
    else
    {
        std::cerr << "Unknown file type " << inputFile << std::endl;
        return 1;
    }

    if (shape.vertexCount_ != 0)
    {
        std::cout << "Shape " << shape.vertexCount_ << " vertices " << shape.indexCount_ << " indices " <<
            shape.GetTriangleCount() << " tris" << std::endl;
        if (!shape.IsTriangles())
        {
            std::cerr << "Shape does not consist of triangles" << std::endl;
            return 1;
        }
    }

    const std::string output = sa::ArgParser::GetValue<std::string>(parsedArgs, "output", inputFile + ".png");
    int sizeX = sa::ArgParser::GetValue<int>(parsedArgs, "targetwidth", 0);
    int sizeY = sa::ArgParser::GetValue<int>(parsedArgs, "targetheight", 0);
    const std::string outext = Utils::GetFileExt(output);
    if (Utils::StringEquals(outext, ".png"))
    {
        float minH = sa::ArgParser::GetValue<float>(parsedArgs, "minheight", std::numeric_limits<float>::max());
        float maxH = sa::ArgParser::GetValue<float>(parsedArgs, "maxheight", std::numeric_limits<float>::lowest());
        header.minHeight = std::min(minH, header.minHeight);
        header.maxHeight = std::max(maxH, header.maxHeight);
        if (!heights.empty())
        {
            CreateImage(heights, output, comps, sizeX, sizeY, header.minHeight, header.maxHeight);
            return 0;
        }
        if (shape.vertexCount_ != 0)
        {
            CreateImage(shape, output, comps, sizeX, sizeY, header.minHeight, header.maxHeight);
            return 0;
        }
    }

    header.numVertices.x_ = sa::ArgParser::GetValue<int>(parsedArgs, "nvx", 0);
    header.numVertices.y_ = sa::ArgParser::GetValue<int>(parsedArgs, "nvy", 0);
    if (header.numVertices.x_ == 0 || header.numVertices.y_ == 0)
    {
        std::cerr << "Missing argument number of vertices" << std::endl;
        return 1;
    }

    header.patchSize = sa::ArgParser::GetValue<int>(parsedArgs, "ps", 0);
    if (header.patchSize == 0)
    {
        std::cerr << "Missing argument patch size" << std::endl;
        return 1;
    }

    header.patchWorldSize.x_ = sa::ArgParser::GetValue<float>(parsedArgs, "pwsx", 0.0f);
    header.patchWorldSize.y_ = sa::ArgParser::GetValue<float>(parsedArgs, "pwsy", 0.0f);
    if (Math::Equals(header.patchWorldSize.x_, 0.0f) || Math::Equals(header.patchWorldSize.y_, 0.0f))
    {
        std::cerr << "Missing argument patch world size" << std::endl;
        return 1;
    }

    header.numPatches.x_ = sa::ArgParser::GetValue<int>(parsedArgs, "npx", 0);
    header.numPatches.y_ = sa::ArgParser::GetValue<int>(parsedArgs, "npy", 0);
    if (header.numPatches.x_ == 0 || header.numPatches.y_ == 0)
    {
        std::cerr << "Missing argument number of patches" << std::endl;
        return 1;
    }

    header.patchWorldOrigin.x_ = sa::ArgParser::GetValue<float>(parsedArgs, "pwox", Math::M_INFINITE);
    header.patchWorldOrigin.y_ = sa::ArgParser::GetValue<float>(parsedArgs, "pwoy", Math::M_INFINITE);
    if (Math::IsInfinite(header.patchWorldOrigin.x_) || Math::IsInfinite(header.patchWorldOrigin.y_))
    {
        std::cerr << "Missing argument patch world origin" << std::endl;
        return 1;
    }

    if (shape.vertexCount_ != 0)
    {
        CreateHeightmap(shape, output, sizeX, sizeY, header);
        return 0;
    }
    if (heights.empty())
    {
        std::cerr << "Missing input" << std::endl;
        return 1;
    }

    CreateHeightmap(heights, output, sizeX, sizeY, header);

    return 0;
}
