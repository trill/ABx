/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <sa/ArgParser.h>
#include <iostream>
#include <sa/Compiler.h>
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#undef STB_IMAGE_IMPLEMENTATION
PRAGMA_WARNING_POP
#include <libmath/HeightMapTools.h>
#include <libio/Mesh.h>

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help",
        false, false, sa::ArgParser::OptionType::None });

    cli.push_back({ "spacingx", { "-X", "--spacing-x" }, "Spacing X (default 1)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "spacingy", { "-Y", "--spacing-y" }, "Spacing Y (default 0.2)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "spacingz", { "-Z", "--spacing-z" }, "Spacing Z (default 1)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "patchsize", { "-P", "--patch-size" }, "Patch size (default 32)",
        false, true, sa::ArgParser::OptionType::Integer });

    cli.push_back({ "output", { "-o", "--output" }, "Output OBJ file",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "input", { }, "Input PNG file",
        true, true, sa::ArgParser::OptionType::String });
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("hm2obj", _cli, "Construct 3D mesh from heightmap");
}

static void ShowInfo()
{
    std::cout << "hm2obj - Construct 3D mesh from heightmap" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

int main(int argc, char** argv)
{
    ShowInfo();
    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }

    auto inputval = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    if (!inputval.has_value())
    {
        std::cerr << "No input file provided" << std::endl;
        return 1;
    }
    std::string inputFile = inputval.value();
    std::string outputFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "output", inputFile + ".obj");

    int width = 0; int height = 0; int components = 0;
    unsigned char* data = stbi_load(inputFile.c_str(), &width, &height, &components, 0);
    if (!data)
    {
        std::cout << "Error loading file " << inputFile << std::endl;
        return 1;
    }

    Math::Vector3 spacing;
    spacing.x_ = sa::ArgParser::GetValue<float>(parsedArgs, "spacingx", 1.0f);
    spacing.y_ = sa::ArgParser::GetValue<float>(parsedArgs, "spacingy", 0.2f);
    spacing.z_ = sa::ArgParser::GetValue<float>(parsedArgs, "spacingz", 1.0f);
    int patchSize = sa::ArgParser::GetValue<int>(parsedArgs, "patchsize", 32);

    Math::HeightmapHeader header;

    Math::Mesh shape;
    Math::CreateShapeFromHeightmapImage(data, width, height, components, spacing, patchSize,
        [&shape](const Math::Vector3& vertex)
    {
        shape.vertexData_.push_back(vertex);
        ++shape.vertexCount_;
    },
        [&shape](int i1, int i2, int i3)
    {
        shape.AddTriangle(i1, i2, i3);
    },
        header);
    free(data);
    std::cout << "patchWorldSize: " << header.patchWorldSize << std::endl;
    std::cout << "numPatches: " << header.numPatches << std::endl;
    std::cout << "numVertices: " << header.numVertices << std::endl;
    std::cout << "patchWorldOrigin: " << header.patchWorldOrigin << std::endl;
    std::cout << "min/max height: " << header.minHeight << "/" << header.maxHeight << std::endl;

    if (!IO::SaveMeshToOBJ(outputFile, shape, "heightmap"))
    {
        std::cerr << "Failed to save to " << outputFile << std::endl;
        return 1;
    }
    std::cout << "Created " << outputFile << std::endl;

    return 0;
}
