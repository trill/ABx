project (hm2obj CXX)

file(GLOB SOURCES
    hm2obj/*.cpp
    hm2obj/*.h
)

add_executable(
    hm2obj
    ${SOURCES}
)

target_include_directories(hm2obj PRIVATE ${CMAKE_SOURCE_DIR}/include/stb)
target_link_libraries(hm2obj libcommon libmath libio)

install(TARGETS hm2obj
    RUNTIME DESTINATION bin
    COMPONENT runtime
)
