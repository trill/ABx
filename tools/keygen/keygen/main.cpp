/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <string>
#include <vector>
#include <iostream>
#include <sa/ArgParser.h>
#include <libcommon/StringUtils.h>
#include <libcommon/SimpleConfigManager.h>
#include <libcommon/FileUtils.h>
#include <libcommon/Random.h>
#include <AB/DHKeys.hpp>
#include <AB/CommonConfig.h>
#include <libcommon/Utils.h>
#include <libcommon/Logo.h>

static bool GenerateKeys(const std::string& outFile)
{
    Crypto::Random rnd;
    rnd.Initialize();
    Crypto::DHKeys keys;
    keys.GenerateKeys();
    return keys.SaveKeys(outFile);
}

static void ShowLogo()
{
    std::cout << "This is AB Key Generator" << std::endl;
#ifdef _DEBUG
    std::cout << " DEBUG";
#endif
    std::cout << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;

    std::cout << AB_CONSOLE_LOGO << std::endl;

    std::cout << std::endl;
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("keygen", _cli, "This program generates a Diffie Hellman key pair for the server.");
}

int main(int argc, char** argv)
{
    const std::string exeFile = Utils::GetExeName();
    const std::string path = Utils::ExtractFileDir(exeFile);

    sa::ArgParser::Cli _cli{ {
        { "help", { "-h", "--help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None },
        { "file", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String },
        { "force", { "-f", "--force" }, "Overwrite existing file without asking", false, false, sa::ArgParser::OptionType::None }
    } };
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.value_or(false))
    {
        ShowHelp(_cli);
        return EXIT_SUCCESS;
    }
    ShowLogo();
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        std::cout << "Type `keygen -h` for help." << std::endl;
        return EXIT_FAILURE;
    }

    std::string cfgFile = Utils::ConcatPath(path, "abserv.lua");
    IO::SimpleConfigManager cfg;
    if (!cfg.Load(cfgFile))
    {
        std::cerr << "Failed to load config file " << cfgFile << std::endl;
        return EXIT_FAILURE;
    }
    std::string keyFile = cfg.GetGlobalString("server_keys", "");
    if (keyFile.empty())
        keyFile = Utils::ConcatPath(path, "abserver.dh");
    keyFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "file", keyFile);

    if (Utils::FileExists(keyFile))
    {
        if (!sa::ArgParser::GetValue<bool>(parsedArgs, "force", false))
        {
            std::cout << "Overwrite existing file " << keyFile << " (y/n)? ";
            std::string answer;
            if (!std::getline(std::cin, answer))
                return EXIT_FAILURE;
            if (answer == "y")
            {
                std::cout << "Aborted" << std::endl;
                return EXIT_FAILURE;
            }
        }
    }

    if (!GenerateKeys(keyFile))
    {
        std::cerr << "Error generating keys: " << keyFile << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "Generated new keys: " << keyFile << std::endl;
    return EXIT_SUCCESS;
}

