project (keygen CXX)

file(GLOB KEYGEN_SOURCES      keygen/*.cpp keygen/*.h)

add_executable(
    keygen
    ${KEYGEN_SOURCES}
)

target_link_libraries(keygen libcommon)

install(TARGETS keygen
    RUNTIME DESTINATION bin
    COMPONENT runtime
)
