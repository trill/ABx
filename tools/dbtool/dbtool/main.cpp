/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <sa/Compiler.h>
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4702 4127)
#   include <lua.hpp>
#   include <kaguya/kaguya.hpp>
PRAGMA_WARNING_POP
#include "SqlReader.h"
#include <AB/CommonConfig.h>
#include <libdb/Database.h>
#include <libcommon/FileUtils.h>
#include <libcommon/Logger.h>
#include <libcommon/Logo.h>
#include <libcommon/LuaEffect.h>
#include <libcommon/LuaSkill.h>
#include <libcommon/SimpleConfigManager.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Utils.h>
#include <libcommon/UuidUtils.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sa/ArgParser.h>
#include <sa/table.h>
#include <streambuf>
#include <sa/TemplateParser.h>
#include <sa/Process.h>
#include <sa/Assert.h>
#include <AB/Entities/Account.h>

namespace fs = std::filesystem;

static bool sReadOnly = false;
static bool sVerbose = false;
static bool sSaveSQL = false;
std::string gDataDir;

static void ShowLogo()
{
    std::cout << "This is AB Database tool";
#ifdef _DEBUG
    std::cout << " DEBUG";
#endif
    std::cout << std::endl << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;

    std::cout << AB_CONSOLE_LOGO << std::endl;

    std::cout << std::endl;
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("dbtool", _cli, "Database administration tool");
    std::cout << std::endl;
    std::cout << "ACTIONS" << std::endl;
    sa::tab::table table;
    table << "    update" << sa::tab::endc << "Update the database" << sa::tab::endr;
    table << "    versions" << sa::tab::endc << "Show database and table versions" << sa::tab::endr;
    table << "    acckeys" << sa::tab::endc << "Show account keys" << sa::tab::endr;
    table << "    genacckey" << sa::tab::endc << "Generate a new account key" << sa::tab::endr;
    table << "    importskill" << sa::tab::endc << "Import a Lua skill file, expects a file with the -f option" << sa::tab::endr;
    table << "    importeffect" << sa::tab::endc << "Import a Lua effect file, expects a file with the -f option" << sa::tab::endr;
    table << "    makegod" << sa::tab::endc << "Make an account god, expects a username with -user option" << sa::tab::endr;
    std::cout << table;
    std::cout << std::endl;
    std::cout << "EXAMPLES" << std::endl;
    std::cout << "    dbtool update" << std::endl;
    std::cout << "    dbtool update -d \"dir/with/sql/files\"" << std::endl;
}

static void InitCli(sa::ArgParser::Cli& cli)
{
    std::stringstream dbDrivers;
#ifdef HAVE_SQLITE
    dbDrivers << " sqlite";
#endif
#ifdef HAVE_MYSQL
    dbDrivers << " mysql";
#endif
#ifdef HAVE_PGSQL
    dbDrivers << " pgsql";
#endif
#ifdef HAVE_ODBC
    dbDrivers << " odbc";
#endif

    cli.push_back({ "action", { }, "What to do, possible value(s) see bellow",
        true, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "readonly", { "-r", "--read-only" }, "Do not write to Database",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "verbose", { "-v", "--verbose" }, "Write out stuff",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "dbdriver", { "-dbdriver", "--database-driver" }, "Database driver, possible value(s):" + dbDrivers.str(),
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "dbhost", { "-dbhost", "--database-host" }, "Host name of database server",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "dbport", { "-dbport", "--database-port" }, "Port to connect to",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "dbname", { "-dbname", "--database-name" }, "Name of database",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "dbuser", { "-dbuser", "--database-user" }, "User name for database",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "dbpass", { "-dbpass", "--database-password" }, "Password for database",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "schemadir", { "-d", "--schema-dir" }, "Directory with .sql files to import for updating",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "user", { "-user", "--user-name" }, "User name",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "file", { "-f", "--file" }, "File name",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "savesql", { "-save", "--save-sql" }, "Save SQL when importing Skill or Effect",
        false, true, sa::ArgParser::OptionType::String });
}

static void GetFiles(const std::string& dir, std::map<unsigned, std::string>& result)
{
    for (const auto& entry : fs::directory_iterator(dir))
    {
        const auto path = entry.path().string();
        const std::string filename = Utils::ExtractFileName(path);
        unsigned int version;
#ifdef _MSC_VER
        if (sscanf_s(filename.c_str(), "schema.%u.sql", &version) != 1)
            continue;
#else
        if (sscanf(filename.c_str(), "schema.%u.sql", &version) != 1)
            continue;
#endif
        result[version] = path;
    }
}

static bool ImportFile(DB::Database& db, const std::string& file)
{
    std::cout << "Importing " << file << std::endl;

    SqlReader reader;
    if (!reader.Read(file))
    {
        std::cerr << "Error reading file " << file << std::endl;
        return false;
    }
    if (reader.IsEmpty())
    {
        std::cout << "File " << file << " is empty" << std::endl;
        // I guess empty files are okay.
        return true;
    }

    DB::DBTransaction transaction(&db);
    if (!transaction.Begin())
        return false;

    bool failed = false;
    reader.VisitStatements([&](const auto& statement) -> Iteration
    {
        if (sVerbose)
            std::cout << statement << std::endl;
        if (!sReadOnly)
        {
            if (!db.ExecuteQuery(statement))
            {
                failed = true;
                return Iteration::Break;
            }
        }
        return Iteration::Continue;
    });

    if (failed)
        return false;

    return transaction.Commit();
}

static int GetDBVersion(DB::Database& db)
{
    std::ostringstream query;
#ifdef HAVE_PGSQL
    if (DB::Database::driver_ == "pgsql")
        query << "SET search_path TO schema, public; ";
#endif
    query << "SELECT value FROM versions WHERE ";
    query << "name = " << db.EscapeString("schema");

    std::shared_ptr<DB::DBResult> result = db.StoreQuery(query.str());
    if (!result)
    {
        // No such record means not even the first file (file 0) was imported
        return -1;
    }

    return static_cast<int>(result->GetUInt("value"));
}

static bool UpdateDatabase(DB::Database& db, const std::string& dir)
{
    std::cout << "Importing directory " << dir << std::endl;

    size_t importedCount = 0;
    std::map<unsigned, std::string> files;
    GetFiles(dir, files);
    // Fortunately the items in a map are sorted by key
    for (const auto& f : files)
    {
        // Files must update the version, so with each import this value should change
        int version = GetDBVersion(db);
        if (sVerbose)
            std::cout << "Database version is: " << version << std::endl;
        if (static_cast<int>(f.first) > version)
        {
            ++importedCount;
            if (!ImportFile(db, f.second))
                return false;
        }
    }

    std::cout << "Imported " << importedCount << " files" << std::endl;
    std::cout << "Database version is now: " << GetDBVersion(db) << std::endl;

    return true;
}

static void ShowVersions(DB::Database& db)
{
    std::ostringstream query;
    query << "SELECT * FROM versions ORDER BY name";
    sa::tab::table table;
    table.table_sep_ = '=';
    table << sa::tab::head << "Name" << sa::tab::endc << "Value" << sa::tab::endr;
    for (std::shared_ptr<DB::DBResult> result = db.StoreQuery(query.str()); result; result = result->Next())
    {
        table << result->GetString("name") << sa::tab::endc <<
                 sa::tab::ralign << result->GetUInt("value") << sa::tab::endr;
    }
    std::cout << table;
}

static void ShowAccountKeys(DB::Database& db)
{
    std::ostringstream query;
    query << "SELECT * FROM account_keys";
    sa::tab::table table;
    table.table_sep_ = '=';
    table.col_sep_ = " | ";
    table << sa::tab::head << "UUID" << sa::tab::endc << sa::tab::ralign << "Used" <<
             sa::tab::endc << sa::tab::ralign << "Total" << sa::tab::endc << "Description" << sa::tab::endr;
    for (std::shared_ptr<DB::DBResult> result = db.StoreQuery(query.str()); result; result = result->Next())
    {
        table << result->GetString("uuid") << sa::tab::endc <<
                 sa::tab::ralign << result->GetUInt("used") << sa::tab::endc <<
                 sa::tab::ralign << result->GetUInt("total") <<sa::tab::endc <<
                 result->GetString("description") << sa::tab::endr;
    }
    std::cout << table;
}

static std::string GenAccKey(DB::Database& db)
{
    std::string uuid = Utils::Uuid::New();

    static constexpr const char* SQL = "INSERT INTO account_keys ("
        "uuid, used, total, description, status, key_type, email"
        ") VALUES ( "
        "${uuid}, ${used}, ${total}, ${description}, ${status}, ${key_type}, ${email}"
        ")";
    const std::string query = sa::templ::Parser::Evaluate(SQL, [&](const sa::templ::Token& token)->std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "uuid")
                return db.EscapeString(uuid);
            if (token.value == "used")
                return std::to_string(0);
            if (token.value == "total")
                return std::to_string(1);
            if (token.value == "description")
                return db.EscapeString("Generated by dbtool");
            if (token.value == "status")
                return std::to_string(2);
            if (token.value == "key_type")
                return std::to_string(1);
            if (token.value == "email")
                return db.EscapeString("invalid@email");

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    DB::DBTransaction transaction(&db);
    if (!transaction.Begin())
        return "Error creating transaction";

    if (!sReadOnly)
    {
        if (!db.ExecuteQuery(query))
            return "Error";
    }

    if (!transaction.Commit())
        return "Error";

    return uuid;
}

static bool ImportEffect(DB::Database& db, const std::string& effectFile)
{
    if (!Utils::FileExists(effectFile))
    {
        std::cerr << "File " << effectFile << " not found" << std::endl;
        return false;
    }
    IO::LuaEffect lEffect;
    if (!lEffect.Execute(effectFile))
        return false;
    if (lEffect.GetIndex() == 0)
    {
        std::cerr << "Missing `index` field in " << effectFile << std::endl;
        return false;
    }
    if (sVerbose)
        std::cout << "Importing: " << effectFile << std::endl;

    static constexpr const char* SQL_SELECT = "SELECT COUNT(*) AS count FROM game_effects WHERE idx = ${idx}";
    const std::string selectQuery = sa::templ::Parser::Evaluate(SQL_SELECT, [&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "idx")
                return std::to_string(lEffect.GetIndex());

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    static constexpr const char* UPDATE_SQL = "UPDATE public.game_effects SET "
        "script = ${script} "
        "WHERE idx = ${idx}";
    static constexpr const char* INSERT_SQL = "INSERT INTO public.game_effects ("
            "uuid, "
            "idx, "
            "script"
        ") VALUES ("
            "${uuid}, "
            "${idx}, "
            "${script}"
        ")";

    sa::templ::Parser parser;
    std::shared_ptr<DB::DBResult> selectResult = db.StoreQuery(selectQuery);

    const auto tokens = (selectResult && selectResult->GetUInt("count") != 0) ?
        parser.Parse(UPDATE_SQL) :
        parser.Parse(INSERT_SQL);

    const std::string query = tokens.ToString([&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "uuid")
                return db.EscapeString(lEffect.GetUuid());
            if (token.value == "idx")
                return std::to_string(lEffect.GetIndex());
            if (token.value == "script")
            {
                auto p = fs::absolute(effectFile);
                auto relative = fs::relative(p, gDataDir);
                std::string relString = relative.string();
                sa::ReplaceSubstring<char>(relString, "\\", "/");
                if (!relString.starts_with("/"))
                    relString = "/" + relString;
                return db.EscapeString(relString);
            }

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    if (sVerbose)
        std::cout << query << std::endl;
    if (sSaveSQL)
    {
        auto p = fs::absolute(effectFile);
        std::string sqlFile = Utils::ChangeFileExt(p.string(), ".sql");
        std::ofstream out(sqlFile);
        out << query;
        if (!query.ends_with(";"))
            out << ";";
        out << std::endl;
    }

    if (!sReadOnly)
    {
        if (!db.ExecuteQuery(query))
            return false;
    }
    return true;
}

static bool ImportSkill(DB::Database& db, const std::string& skillFile)
{
    if (!Utils::FileExists(skillFile))
    {
        std::cerr << "File " << skillFile << " not found" << std::endl;
        return false;
    }
    IO::LuaSkill lSkill;
    if (!lSkill.Execute(skillFile))
        return false;
    if (lSkill.GetIndex() == 0)
    {
        std::cerr << "Missing `index` field in " << skillFile << std::endl;
        return false;
    }

    if (sVerbose)
        std::cout << "Importing: " << skillFile << std::endl;

    static constexpr const char* SQL_SELECT = "SELECT COUNT(*) AS count FROM game_skills WHERE idx = ${idx}";
    const std::string selectQuery = sa::templ::Parser::Evaluate(SQL_SELECT, [&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "idx")
                return std::to_string(lSkill.GetIndex());

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    static constexpr const char* UPDATE_SQL = "UPDATE public.game_skills SET "
        "access = ${access}, "
        "script = ${script} "
        "WHERE idx = ${idx}";
    static constexpr const char* INSERT_SQL = "INSERT INTO public.game_skills ("
            "uuid, "
            "idx, "
            "script, "
            "access"
        ") VALUES ("
            "${uuid}, "
            "${idx}, "
            "${script}, "
            "${access}"
        ")";

    sa::templ::Parser parser;
    std::shared_ptr<DB::DBResult> selectResult = db.StoreQuery(selectQuery);

    const auto tokens = (selectResult && selectResult->GetUInt("count") != 0) ?
        parser.Parse(UPDATE_SQL) :
        parser.Parse(INSERT_SQL);

    const std::string query = tokens.ToString([&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "uuid")
                return db.EscapeString(lSkill.GetUuid());
            if (token.value == "idx")
                return std::to_string(lSkill.GetIndex());
            if (token.value == "script")
            {
                auto p = fs::absolute(skillFile);
                auto relative = fs::relative(p, gDataDir);
                std::string relString = relative.string();
                sa::ReplaceSubstring<char>(relString, "\\", "/");
                if (!relString.starts_with("/"))
                    relString = "/" + relString;
                return db.EscapeString(relString);
            }
            if (token.value == "access")
                return std::to_string(lSkill.GetAccess());

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    if (sVerbose)
        std::cout << query << std::endl;
    if (sSaveSQL)
    {
        auto p = fs::absolute(skillFile);
        std::string sqlFile = Utils::ChangeFileExt(p.string(), ".sql");
        std::ofstream out(sqlFile);
        out << query;
        if (!query.ends_with(";"))
            out << ";";
        out << std::endl;
    }

    if (!sReadOnly)
    {
        if (!db.ExecuteQuery(query))
            return false;
    }
    return true;
}

static bool MakeGod(DB::Database& db, const std::string& username)
{
    static constexpr const char* SQL_SELECT = "SELECT COUNT(*) AS count FROM accounts WHERE name = ${name}";
    const std::string selectQuery = sa::templ::Parser::Evaluate(SQL_SELECT, [&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "name")
                return db.EscapeString(username);

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    std::shared_ptr<DB::DBResult> selectResult = db.StoreQuery(selectQuery);
    if (!selectResult || selectResult->GetUInt("count") == 0)
    {
        std::cerr << "No account with name " << username << std::endl;
        return false;
    }

    static constexpr const char* SQL = "UPDATE accounts SET type = ${type} WHERE name = ${name}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, [&](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "name")
                return db.EscapeString(username);
            if (token.value == "type")
                return std::to_string(static_cast<int>(AB::Entities::AccountType::God));

            std::cout << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });
    DB::DBTransaction transaction(&db);
    if (!transaction.Begin())
        return false;

    if (!sReadOnly)
    {
        if (!db.ExecuteQuery(query))
            return false;
    }

    return transaction.Commit();
}

enum class Action
{
    Update,
    Versions,
    AccountKeys,
    GenAccKey,
    ImportSkill,
    ImportEffect,
    MakeGod,
};

int main(int argc, char** argv)
{
    const std::string exeFile = Utils::GetExeName();
    const std::string path = Utils::ExtractFileDir(exeFile);

    std::string sCfgFile = Utils::ConcatPath(path, "abserv.lua");
    IO::SimpleConfigManager sCfg;
    if (!sCfg.Load(sCfgFile))
    {
        std::cerr << "Failed to load config file " << sCfgFile << std::endl;
        return EXIT_FAILURE;
    }
    gDataDir = sCfg.GetGlobalString("data_dir", "");
    if (sVerbose)
        std::cout << "Data directory: " << gDataDir << std::endl;

    sa::ArgParser::Cli _cli{ {
        { "help", { "-h", "--help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None }
    } };
    InitCli(_cli);

    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return EXIT_SUCCESS;
    }
    ShowLogo();
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return EXIT_FAILURE;
    }

    // Action is first unnamed argument
    auto actval = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    if (!actval.has_value())
    {
        std::cerr << "No action provided" << std::endl;
        return EXIT_FAILURE;
    }

    const std::string& sActval = actval.value();
    Action action;
    if (sActval.compare("update") == 0)
        action = Action::Update;
    else if (sActval.compare("versions") == 0)
        action = Action::Versions;
    else if (sActval.compare("acckeys") == 0)
        action = Action::AccountKeys;
    else if (sActval.compare("genacckey") == 0)
        action = Action::GenAccKey;
    else if (sActval.compare("importskill") == 0)
        action = Action::ImportSkill;
    else if (sActval.compare("importeffect") == 0)
        action = Action::ImportEffect;
    else if (sActval.compare("makegod") == 0)
        action = Action::MakeGod;
    else
    {
        std::cerr << "Unknown action `" << sActval << "`" << std::endl;
        return EXIT_FAILURE;
    }

    sReadOnly = sa::ArgParser::GetValue<bool>(parsedArgs, "readonly", false);
    sVerbose = sa::ArgParser::GetValue<bool>(parsedArgs, "verbose", false);
    sSaveSQL = sa::ArgParser::GetValue<bool>(parsedArgs, "savesql", false);

    std::string dbCfgFile = Utils::ConcatPath(path, "config/db.lua");
    IO::SimpleConfigManager dbCfg;
    if (!dbCfg.Load(dbCfgFile))
    {
        std::cerr << "Failed to load config file " << dbCfgFile << std::endl;
        return EXIT_FAILURE;
    }

    DB::Database::driver_ = sa::ArgParser::GetValue<std::string>(parsedArgs, "dbdriver", DB::Database::driver_);
    DB::Database::dbHost_ = sa::ArgParser::GetValue<std::string>(parsedArgs, "dbhost", DB::Database::dbHost_);
    DB::Database::dbName_ = sa::ArgParser::GetValue<std::string>(parsedArgs, "dbname", DB::Database::dbName_);
    DB::Database::dbUser_ = sa::ArgParser::GetValue<std::string>(parsedArgs, "dbuser", DB::Database::dbUser_);
    DB::Database::dbPass_ = sa::ArgParser::GetValue<std::string>(parsedArgs, "dbpass", DB::Database::dbPass_);
    DB::Database::dbPort_ = sa::ArgParser::GetValue<uint16_t>(parsedArgs, "dbport", DB::Database::dbPort_);

    if (DB::Database::driver_.empty())
        DB::Database::driver_ = dbCfg.GetGlobalString("db_driver", "");
    if (DB::Database::dbHost_.empty())
        DB::Database::dbHost_ = dbCfg.GetGlobalString("db_host", "");
    if (DB::Database::dbName_.empty())
        DB::Database::dbName_ = dbCfg.GetGlobalString("db_name", "");
    if (DB::Database::dbUser_.empty())
        DB::Database::dbUser_ = dbCfg.GetGlobalString("db_user", "");
    if (DB::Database::dbPass_.empty())
        DB::Database::dbPass_ = dbCfg.GetGlobalString("db_pass", "");
    if (DB::Database::dbPort_ == 0)
        DB::Database::dbPort_ = static_cast<uint16_t>(dbCfg.GetGlobalInt("db_port", 0ll));

    std::unique_ptr<DB::Database> db = std::unique_ptr<DB::Database>(DB::Database::CreateInstance(
        DB::Database::driver_,
        DB::Database::dbHost_, DB::Database::dbPort_,
        DB::Database::dbUser_, DB::Database::dbPass_,
        DB::Database::dbName_
    ));
    if (!db || !db->IsConnected())
    {
        std::cerr << "Database connection failed" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Connected to " << DB::Database::driver_ <<
        " " << DB::Database::dbUser_ << "@" << DB::Database::dbHost_ << ":" << DB::Database::dbPort_ << std::endl;

    if (sReadOnly)
        std::cout << "READ-ONLY mode, nothing is written to the database" << std::endl;

    switch (action)
    {
    case Action::Update:
    {
        std::string schemasDir = sa::ArgParser::GetValue<std::string>(parsedArgs, "schemadir", "");
        if (schemasDir.empty())
            schemasDir = dbCfg.GetGlobalString("db_schema_dir", "");

        if (schemasDir.empty())
        {
            std::cerr << "Invalid Schema directory" << std::endl;
            return EXIT_FAILURE;
        }
        if (!UpdateDatabase(*db, schemasDir))
            return EXIT_FAILURE;
        break;
    }
    case Action::Versions:
        ShowVersions(*db);
        break;
    case Action::AccountKeys:
        ShowAccountKeys(*db);
        break;
    case Action::GenAccKey:
    {
        std::string key = GenAccKey(*db);
        std::cout << key << std::endl;
        break;
    }
    case Action::ImportSkill:
    {
        std::string file = sa::ArgParser::GetValue<std::string>(parsedArgs, "file", "");
        if (file.empty())
        {
            std::cerr << "Missing -f option" << std::endl;
            return EXIT_FAILURE;
        }
        if (ImportSkill(*db, file))
        {
            std::cout << "Imported skill " << file << std::endl;
        }
        break;
    }
    case Action::ImportEffect:
    {
        std::string file = sa::ArgParser::GetValue<std::string>(parsedArgs, "file", "");
        if (file.empty())
        {
            std::cerr << "Missing -f option" << std::endl;
            return EXIT_FAILURE;
        }
        if (ImportEffect(*db, file))
        {
            std::cout << "Imported effect " << file << std::endl;
        }
        break;
    }
    case Action::MakeGod:
    {
        std::string user = sa::ArgParser::GetValue<std::string>(parsedArgs, "user", "");
        if (user.empty())
        {
            std::cerr << "Missing -user option" << std::endl;
            return EXIT_FAILURE;
        }
        if (MakeGod(*db, user))
        {
            std::cout << user << " is now God" << std::endl;
        }
        break;
    }
    default:
        ASSERT_FALSE();
    }

    return EXIT_SUCCESS;
}
