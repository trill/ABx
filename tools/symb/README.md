# symb

Tool to symbolicate backtraces. Basically runs `addr2line`.
Unfortunately with ASLR you can't just pass the address of the backtrace
to `addr2line`.
