/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <cstdio>
#include <cstdlib>
#include <string>
#include <sa/ArgParser.h>
#include <iostream>
#include <fstream>
#include <sa/StringTempl.h>
#include <cxxabi.h>
#include <AB/CommonConfig.h>

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "nodemangle", { "-D", "--no-demangle" }, "Don't demangle symbols",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "short", { "-s", "--short" }, "Only symbols, file and line",
        false, false, sa::ArgParser::OptionType::None });
}

static void ShowInfo()
{
    std::cout << "symb - Symbolicate backtraces" << std::endl;
    std::cout << "Copyright (C) 2021-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("symb", _cli, "Symbolicate backtraces", "<backtrace file>");
}

static std::string Demangle(const std::string& symbol)
{
    size_t buff_size = 512;
    auto* buff = reinterpret_cast<char*>(std::malloc(buff_size));
    int stat = 0;

    std::string s = symbol;
    auto p = s.find(" at ");
    if (p != std::string::npos)
        s = s.substr(0, p);
    buff = abi::__cxa_demangle(s.c_str(), buff, &buff_size, &stat);
    std::string result = (!!buff) ? buff : "(unknown) ";
    std::free(buff);
    if (p != std::string::npos)
        result += symbol.substr(p, std::string::npos);
    else
        result += symbol;
    return result;
}

static std::string Addr2Line(const std::string& prog, const void* addr)
{
    std::stringstream result;
    char buff[1024] = {};
    char addr2line_cmd[512] = {};

    sprintf(addr2line_cmd, "addr2line -f -p -e %.256s %p", prog.c_str(), addr);

    FILE* fp = popen(addr2line_cmd, "r");
    if (fp == NULL)
    {
        std::cerr << "Unable to run command " << addr2line_cmd << std::endl;
        return "";
    }

    while (fgets(buff, sizeof(buff), fp) != NULL)
    {
        result << buff;
    }

    pclose(fp);
    return sa::Trim(result.str(), std::string(" \t\r\n"));
}

static bool ParseLine(const std::string& input, std::string& module, uintptr_t& addr)
{
    auto p = input.find('(');
    if (p == std::string::npos)
        return false;
    module = input.substr(0, p);
    auto p2 = input.find(')');
    if (p2 == std::string::npos)
        return false;
    std::string a = input.substr(p + 1, p2 - p - 1);

    auto p3 = a.find('+');
    if (p3 != std::string::npos)
        a.erase(0, p3 + 1);

    auto opta = sa::ToNumber<uintptr_t>(a);
    if (!opta.has_value())
        return false;
    addr = opta.value();
    return true;
}

int main(int argc, char** argv)
{
    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);

    bool help = sa::ArgParser::GetValue<bool>(parsedArgs, "help").value_or(false);
    if (help)
    {
        ShowInfo();
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cerr << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }

    auto fval = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    if (!fval.has_value())
    {
        std::cerr << "No input file provided" << std::endl;
        return 1;
    }

    bool noDemangle = sa::ArgParser::GetValue<bool>(parsedArgs, "nodemangle", false);
    bool shrt = sa::ArgParser::GetValue<bool>(parsedArgs, "short", false);

    std::string line;
    std::ifstream infile;
    infile.open(fval.value());

    if (!infile.is_open())
    {
        std::cerr << "Could not open file " << fval.value() << std::endl;
        return 1;
    }
    while (getline(infile, line))
    {
        std::string module;
        uintptr_t addr = 0;
        if (ParseLine(line, module, addr))
        {
            std::string addrline = Addr2Line(module, (const void*)addr);
            if (addrline.empty())
                continue;

            if (!shrt)
                std::cout << line << ": ";
            std::cout << (noDemangle ? addrline : Demangle(addrline)) << std::endl;
        }
    }

    return 0;
}
