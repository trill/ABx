/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Compiler.h>

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
#include <stb_image.h>
PRAGMA_WARNING_POP
#include <libmath/Vector3.h>
#include <libmath/Point.h>
#include <eastl.hpp>
#include <libmath/HeightMapHeader.h>

/// Creates Heightmap and Mesh from Image
class CreateHeightMapAction
{
private:
    std::string file_;
    std::string outputDirectory_;
    ea::vector<float> heightData_;

    int width_{ 0 };
    int height_{ 0 };
    int components_{ 0 };
    stbi_uc* data_{ nullptr };
    void SaveHeightMap();
    std::string GetOutputFile() const;
public:
    CreateHeightMapAction(const std::string& file, const std::string& outDir) :
        file_(file),
        outputDirectory_(outDir)
    {
        header_.patchSize = 32;
    }
    ~CreateHeightMapAction()
    {
        if (data_)
            free(data_);
    }
    bool Execute(bool clean = false);

    Math::Vector3 spacing_;
    Math::HeightmapHeader header_;
    std::string ext_{ ".hm" };
    int GetWidth() const { return width_; }
    int GetHeight() const { return height_; }
};
