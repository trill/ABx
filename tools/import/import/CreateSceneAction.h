/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <pugixml.hpp>
#include <libmath/Vector3.h>
#include <vector>
#include <memory>
#include <libmath/Mesh.h>
#include <libmath/Point.h>
#include <libmath/HeightMapHeader.h>
#include <libcommon/ConfigFile.h>

class CreateSceneAction
{
private:
    std::string file_;
    std::string outputDirectory_;
    std::string heightfieldFile_;
    std::string obstaclesHeightmap_;
    Math::Vector3 heightmapSpacing_{ 1.0f, 0.25f, 1.0f };
    Math::HeightmapHeader header_;
    std::string navmeshFile_{ "navmesh.bin" };
    std::vector<std::string> searchpaths_;
    std::vector<std::unique_ptr<Math::Mesh>> obstackles_;
    int heightmapWidth_{ 0 };
    int heightmapHeight_{ 0 };
    const IO::ConfigFile& cfg_;
    bool LoadScene();
    bool LoadSceneNode(const pugi::xml_node& node, const Math::Matrix4& parentMatrix);
    bool CopySceneFile();
    bool SaveObstacles();
    bool SaveObstaclesObj();
    bool SaveObstaclesHm();
    bool CreateHightmap();
    bool CreateNavMesh();
    bool CreateIndexFile();
    bool CreateTerrainFile();
    // Create level 2 heightmap for the client
    bool CreateClientHeightmap();
    // Create clients minimap. Basically a PNG with the terrain and obstacles.
    bool CreateClientMinimap();
    bool SaveModel(const Math::Mesh& shape, const std::string& srcFile, const std::string& filename);
    std::string FindFile(const std::string& name);
    bool ShouldCreateFile(const std::string& src, const std::string& dst) const;
public:
    CreateSceneAction(const std::string& file, const std::string& outDir, const IO::ConfigFile& cfg) :
        file_(file),
        outputDirectory_(outDir),
        cfg_(cfg)
    {
        header_.patchSize = 32;
        header_.minHeight = -Math::M_INFINITE;
        header_.maxHeight = Math::M_INFINITE;
    }
    ~CreateSceneAction() = default;
    void Execute();

    std::string dataDir_;
    bool createObjs_{ false };
    bool clean_{ false };
};
