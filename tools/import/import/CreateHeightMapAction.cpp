/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "CreateHeightMapAction.h"
#include <libcommon/FileUtils.h>
#include <libcommon/StringUtils.h>
#include <libio/Heightmap.h>
#include <libmath/HeightMapTools.h>
#include <libmath/MathUtils.h>
#include <libmath/VectorMath.h>
#include <sa/StringTempl.h>
#include <limits>
#include "stdafx.h"

void CreateHeightMapAction::SaveHeightMap()
{
    const std::string fileName = GetOutputFile();

    std::cout << "Num vertices: " << header_.numVertices << ", patch size: " << header_.patchSize
              << ", patch world size: " << header_.patchWorldSize << ", num patches: " << header_.numPatches
              << ", patch world origin: " << header_.patchWorldOrigin
              << ", min/max height: " << header_.minHeight << "/" << header_.maxHeight
              << ", n height values: " << heightData_.size() << std::endl;

    if (!IO::SaveHeightmap(fileName, heightData_, header_))
    {
        std::cerr << "Failed to create " << fileName << std::endl;
        return;
    }

    std::cout << "Created " << fileName << std::endl;
}

std::string CreateHeightMapAction::GetOutputFile() const
{
    if (!outputDirectory_.empty())
        return Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(file_) + ext_);
    return file_ + ext_;
}

bool CreateHeightMapAction::Execute(bool clean)
{
    data_ = stbi_load(file_.c_str(), &width_, &height_, &components_, 0);

    if (!data_)
    {
        std::cout << "Error loading file " << file_ << std::endl;
        return false;
    }

    if ((width_ - 1) % header_.patchSize != 0 || (height_ - 1) % header_.patchSize != 0)
    {
        std::cout << "WARNING: Image size - 1 (" << width_ << "x" << height_ << ") should be a multiple of patch size ("
                  << header_.patchSize << ")" << std::endl;
    }

    heightData_ = Math::CreateHeightMapFromImage(
        (const unsigned char*)data_, width_, height_, components_, spacing_, header_.patchSize, header_);

    free(data_);
    data_ = nullptr;

    // We need the properties so we can just skip creation of the file if it already exists
    if (clean || Utils::IsSourceNewerThanDest(file_, GetOutputFile()))
        SaveHeightMap();
    else
        std::cout << GetOutputFile() << " is up to date" << std::endl;

    return true;
}
