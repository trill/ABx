/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include <string>

class Application
{
private:
    enum class Action
    {
        Unknown,
        CreateHeightMap,
        CreateScene
    };
    Action action_{ Action::Unknown };
    bool ParseCommandLine();
    std::vector<std::string> files_;
    std::string outputDirectory_;
    bool createObjs_{ false };
    bool clean_{ false };
    void ShowHelp();
public:
    Application() = default;
    ~Application() = default;

    bool Initialize(int argc, char** argv);
    void Run();

    std::vector<std::string> arguments_;
};

