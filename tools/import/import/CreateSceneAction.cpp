/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "stdafx.h"
#include "CreateSceneAction.h"
#include <libcommon/FileUtils.h>
#include <libcommon/StringUtils.h>
#include <sa/StringTempl.h>
#include <AB/CommonConfig.h>
#include <libcommon/Process.hpp>
#include <codecvt>
#include <sa/Process.h>
#include <sa/StringHash.h>
#include <sa/Assert.h>
#include <libmath/BoundingBox.h>
#include <libmath/Sphere.h>
#include <libmath/Transformation.h>
#include "CreateHeightMapAction.h"
#include <libio/UrhoModel.h>
#include <libio/Mesh.h>
#include <fstream>
#include <iomanip>

void CreateSceneAction::Execute()
{
    if (!Utils::EnsureDirectory(outputDirectory_))
    {
        std::cerr << "Error creating directory " << outputDirectory_ << std::endl;
        return;
    }
    searchpaths_.push_back(Utils::ExtractFileDir(file_) + "/");
    // AbData
    searchpaths_.push_back(Utils::ExtractFileDir(file_) + "/../");
    searchpaths_.push_back(Utils::ExtractFileDir(file_) + "/../../Data/");
    std::cout << "==============================================================================" << std::endl;
    if (!LoadScene())
    {
        std::cerr << "Error loading scene " << file_ << std::endl;
        return;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CopySceneFile())
    {
        std::cerr << "Error copying Scene file " << file_ << " to " << outputDirectory_ << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!SaveObstacles())
    {
        std::cerr << "Error saving obstacles" << std::endl;
    }
    if (createObjs_)
    {
        std::cout << "==============================================================================" << std::endl;
        if (!SaveObstaclesObj())
        {
            std::cerr << "Error saving obstacles" << std::endl;
        }
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CreateHightmap())
    {
        std::cerr << "Error creating height map" << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CreateNavMesh())
    {
        std::cerr << "Error creating navigation mesh" << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!SaveObstaclesHm())
    {
        std::cerr << "Error creating obstacles layer" << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CreateIndexFile())
    {
        std::cerr << "Error creating index.xml" << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CreateClientHeightmap())
    {
        std::cerr << "Error creating client heightmap" << std::endl;
    }
    std::cout << "==============================================================================" << std::endl;
    if (!CreateClientMinimap())
    {
        std::cerr << "Error creating client minimap" << std::endl;
    }
}

bool CreateSceneAction::CreateClientMinimap()
{
    // TODO: Improve colors of minimap
    std::string outDir = Utils::ExtractFileDir(file_) + "/../Textures/Minimaps";
    if (!Utils::EnsureDirectory(outDir))
    {
        std::cerr << "Unable to create directory " << outDir << std::endl;
        return false;
    }
    std::string filename = Utils::ExtractFileName(file_) + ".png";
    std::string obstaclesFile;
    if (!obstackles_.empty())
        obstaclesFile = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_) + ".obstacles");

    std::string outFile = Utils::ConcatPath(outDir, filename);
    if (!ShouldCreateFile(file_, outFile))
    {
        if (obstaclesFile.empty() || !ShouldCreateFile(file_, obstaclesFile))
        {
            std::cout << outFile << " is up to date" << std::endl;
            return true;
        }
    }

    std::cout << "Creating client minimap " << outFile << std::endl;


    const std::string layer1Color = cfg_.Get<std::string>("l1-color", "7f7f51");
    bool layer1Invert = cfg_.Get<bool>("l1-invert", false);
    std::stringstream ss;
    ss << Utils::EscapeArguments(Utils::ConcatPath(sa::Process::GetSelfPath(), "cmm"));
    ss << " -W " << heightmapWidth_;
    ss << " -H " << heightmapHeight_;
    ss << " -X " << heightmapSpacing_.x_;
    ss << " -Y " << heightmapSpacing_.y_;
    ss << " -Z " << heightmapSpacing_.z_;
    ss << " -P " << header_.patchSize;
    if (layer1Invert)
        ss << " -I1";
    ss << " -L1 " << layer1Color << ":" << Utils::EscapeArguments(heightfieldFile_);
    if (!obstaclesFile.empty())
    {
        const std::string layer2Color = cfg_.Get<std::string>("l2-color", "7f7f7f");
        bool layer2Invert = cfg_.Get<bool>("l2-invert", false);
        if (layer2Invert)
            ss << " -I2";
        ss << " -L2 " << layer2Color << ":" << Utils::EscapeArguments(obstaclesFile);
    }
    ss << " -o " << Utils::EscapeArguments(outFile);

    const std::string cmdLine = ss.str();
    std::cout << "Running commandline: " << cmdLine << std::endl;
    System::Process process(cmdLine);
    int exitCode = process.get_exit_status();
    return exitCode == 0;
}

bool CreateSceneAction::CreateClientHeightmap()
{
    if (obstackles_.empty())
        return true;

    std::string inputFile = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_) + ".obstacles");
    std::string outFile = file_ + ".json";
    if (!ShouldCreateFile(inputFile, outFile))
    {
        std::cout << outFile << " is up to date" << std::endl;
        return true;
    }

    std::cout << "Creating client heightmap " << outFile << std::endl;
    std::stringstream ss;

    ss << Utils::EscapeArguments(Utils::ConcatPath(sa::Process::GetSelfPath(), "obj2hm"));
    ss << " -c 1";
    ss << " -W " << heightmapWidth_;
    ss << " -H " << heightmapHeight_;
    ss << " -nvx " << header_.numVertices.x_ << " -nvy " << header_.numVertices.y_;
    ss << " -ps " << header_.patchSize;
    ss << " -pwsx " << header_.patchWorldSize.x_ << " -pwsy " << header_.patchWorldSize.y_;
    ss << " -npx " << header_.numPatches.x_ << " -npy " << header_.numPatches.y_;
    ss << " -pwox " << header_.patchWorldOrigin.x_ << " -pwoy " << header_.patchWorldOrigin.y_;
    if (!Math::IsNegInfinite(header_.minHeight) && !Math::IsInfinite(header_.maxHeight))
    {
        ss << " -minh " << header_.minHeight << " -maxh " << header_.maxHeight;
    }

    ss << " -o " << Utils::EscapeArguments(outFile) << " ";
    ss << Utils::EscapeArguments(inputFile) << " ";

    const std::string cmdLine = ss.str();
    std::cout << "Running commandline: " << cmdLine << std::endl;
    System::Process process(cmdLine);
    int exitCode = process.get_exit_status();
    return exitCode == 0;
}

bool CreateSceneAction::CopySceneFile()
{
    if (outputDirectory_.empty())
    {
        std::cerr << "Output directory is empty" << std::endl;
        return false;
    }
    std::cout << "Copy scene file " << file_ << " to " << outputDirectory_ << std::endl;
    std::string fileName = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(file_));
    if (!ShouldCreateFile(file_, fileName))
    {
        std::cout << fileName << " is up to date" << std::endl;
        return true;
    }
    return Utils::FileCopy(file_, fileName);
}

bool CreateSceneAction::CreateHightmap()
{
    if (outputDirectory_.empty())
    {
        std::cerr << "Output directory is empty" << std::endl;
        return false;
    }
    std::string fileName = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_));
    if (ShouldCreateFile(heightfieldFile_, fileName))
    {
        std::cout << "Copying heightmap " << heightfieldFile_ << std::endl;
        if (!Utils::FileCopy(heightfieldFile_, fileName))
            return false;
    }
    else
        std::cout << fileName << " is up to date" << std::endl;

    CreateHeightMapAction action(heightfieldFile_, outputDirectory_);
    action.spacing_ = heightmapSpacing_;
    action.header_ = header_;
    action.Execute(clean_);
    heightmapWidth_ = action.GetWidth();
    heightmapHeight_ = action.GetHeight();
    header_ = action.header_;
    return true;
}

bool CreateSceneAction::SaveObstacles()
{
    std::string fileName = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_) + ".obstacles");
    if (!ShouldCreateFile(file_, fileName))
    {
        std::cout << fileName << " is up to date" << std::endl;
        return true;
    }

    std::cout << "Saving " << obstackles_.size() << " obstacles to " << fileName << std::endl;
    IO::ObstaclesSaver saver;
    if (!saver.Open(fileName))
    {
        std::cerr << "Could not open file " << fileName << std::endl;
        return false;
    }
    for (const auto& o : obstackles_)
    {
        saver.Add(*o);
    }
    saver.Close();

    return true;
}

bool CreateSceneAction::SaveObstaclesHm()
{
    if (heightmapWidth_ == 0 || heightmapHeight_ == 0)
        return false;
    if (obstackles_.empty())
        return true;

    std::string inputFile = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_) + ".obstacles");
    std::string outputFile = inputFile + ".hm";
    obstaclesHeightmap_ = outputFile;
    if (!ShouldCreateFile(inputFile, outputFile))
    {
        std::cout << outputFile << " is up to date" << std::endl;
        return true;
    }

    std::cout << "Creating Heightmap layer 2 " << outputFile << std::endl;

    std::stringstream ss;

    ss << Utils::EscapeArguments(Utils::ConcatPath(sa::Process::GetSelfPath(), "obj2hm"));
    ss << " -c 1";
    ss << " -W " << heightmapWidth_;
    ss << " -H " << heightmapHeight_;
    ss << " -nvx " << header_.numVertices.x_ << " -nvy " << header_.numVertices.y_;
    ss << " -ps " << header_.patchSize;
    ss << " -pwsx " << header_.patchWorldSize.x_ << " -pwsy " << header_.patchWorldSize.y_;
    ss << " -npx " << header_.numPatches.x_ << " -npy " << header_.numPatches.y_;
    ss << " -pwox " << header_.patchWorldOrigin.x_ << " -pwoy " << header_.patchWorldOrigin.y_;
    if (!Math::IsNegInfinite(header_.minHeight) && !Math::IsInfinite(header_.maxHeight))
    {
        ss << " -minh " << header_.minHeight << " -maxh " << header_.maxHeight;
    }

    ss << " -o " << Utils::EscapeArguments(outputFile) << " ";
    ss << Utils::EscapeArguments(inputFile) << " ";

    const std::string cmdLine = ss.str();
    std::cout << "Running commandline: " << cmdLine << std::endl;
    System::Process process(cmdLine);
    int exitCode = process.get_exit_status();
    return exitCode == 0;
}

bool CreateSceneAction::SaveObstaclesObj()
{
    std::string filename = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_) + ".obstacles.obj");
    if (!ShouldCreateFile(file_, filename))
    {
        std::cout << filename << " is up to date" << std::endl;
        return true;
    }

    std::fstream f(filename, std::fstream::out);
    if (!f.good())
        return false;
    std::cout << "Saving obstacle OBJ to " << filename << std::endl;

    f << std::fixed << std::setprecision(6);
    f << "o " << "obstacles" << std::endl;

    ea::vector<size_t> offsets;
    offsets.push_back(0);
    size_t offset = 0;
    for (const auto& o : obstackles_)
    {
        for (const auto& v : o->vertexData_)
        {
            f << "v " << v.x_ << " " << v.y_ << " " << v.z_ << std::endl;
            ++offset;
        }
        offsets.push_back(offset);
    }
    for (size_t o = 0; o < obstackles_.size(); ++o)
    {
        f << "g obstalce_" << o + 1 << std::endl;
        const auto& obstacle = obstackles_[o];
        size_t offset = offsets[o];
        for (size_t i = 0; i < obstacle->indexCount_; i += 3)
        {
            size_t index1 = obstacle->indexData_[i];
            size_t index2 = obstacle->indexData_[i + 1];
            size_t index3 = obstacle->indexData_[i + 2];
            f << "f " << offset + index1 + 1 <<
                " " << offset + index2 + 1 <<
                " " << offset + index3 + 1 << std::endl;
        }
    }

    f.close();
    return true;

}

bool CreateSceneAction::CreateNavMesh()
{
    std::string fileName = Utils::ConcatPath(outputDirectory_, sa::ExtractFileName<char>(heightfieldFile_));

    navmeshFile_ = fileName + ".navmesh";
    if (!ShouldCreateFile(heightfieldFile_, navmeshFile_))
    {
        std::cout << navmeshFile_ << " is up to date" << std::endl;
        return true;
    }

    std::cout << "Creating navigation mesh from " << heightfieldFile_ << std::endl;

    // Run genavmesh
    std::stringstream ss;

    ss << Utils::EscapeArguments(Utils::ConcatPath(sa::Process::GetSelfPath(), "genavmesh"));
    ss << " ";
    if (createObjs_)
        ss << "-createobj ";
    ss << "-hmsx:" << heightmapSpacing_.x_ << " ";
    ss << "-hmsy:" << heightmapSpacing_.y_ << " ";
    ss << "-hmsz:" << heightmapSpacing_.z_ << " ";
    ss << "-hmps:" << header_.patchSize << " ";
    ss << Utils::EscapeArguments(fileName);

    const std::string cmdLine = ss.str();
    std::cout << "Running commandline: " << cmdLine << std::endl;
    System::Process process(cmdLine);
    int exitCode = process.get_exit_status();
    return exitCode == 0;
}

bool CreateSceneAction::CreateTerrainFile()
{
    pugi::xml_document doc;
    auto declarationNode = doc.append_child(pugi::node_declaration);
    declarationNode.append_attribute("version").set_value("1.0");
    declarationNode.append_attribute("encoding").set_value("UTF-8");
    declarationNode.append_attribute("standalone").set_value("yes");
    auto root = doc.append_child("terrain");
    {
        auto nd = root.append_child("file");
        nd.append_attribute("type").set_value("Heightmap.0");
        nd.append_attribute("src").set_value(Utils::ExtractFileName(heightfieldFile_ + ".hm").c_str());
    }
    if (!obstaclesHeightmap_.empty())
    {
        auto nd = root.append_child("file");
        nd.append_attribute("type").set_value("Heightmap.1");
        nd.append_attribute("src").set_value(Utils::ExtractFileName(obstaclesHeightmap_).c_str());
    }

    std::string fileName = Utils::ConcatPath(outputDirectory_, "terrain.xml");
    std::cout << "Creating terrain.xml" << std::endl;

    std::fstream f(fileName, std::fstream::out);
    if (!f.is_open())
        return false;
    doc.save(f);
    return true;
}

bool CreateSceneAction::CreateIndexFile()
{
    CreateTerrainFile();
    pugi::xml_document doc;
    auto declarationNode = doc.append_child(pugi::node_declaration);
    declarationNode.append_attribute("version").set_value("1.0");
    declarationNode.append_attribute("encoding").set_value("UTF-8");
    declarationNode.append_attribute("standalone").set_value("yes");
    auto root = doc.append_child("index");
    {
        auto nd = root.append_child("file");
        nd.append_attribute("type").set_value("Scene");
        nd.append_attribute("src").set_value(Utils::ExtractFileName(file_).c_str());
    }
    {
        auto nd = root.append_child("file");
        nd.append_attribute("type").set_value("NavMesh");
        nd.append_attribute("src").set_value(Utils::ExtractFileName(navmeshFile_).c_str());
    }
    {
        auto nd = root.append_child("file");
        nd.append_attribute("type").set_value("Terrain");
        nd.append_attribute("src").set_value("terrain.xml");
    }

    std::string fileName = Utils::ConcatPath(outputDirectory_, "index.xml");
    std::cout << "Creating index.xml" << std::endl;

    std::fstream f(fileName, std::fstream::out);
    if (!f.is_open())
        return false;
    doc.save(f);
    return true;
}

std::string CreateSceneAction::FindFile(const std::string& name)
{
    if (name.empty())
        return "";

    if (Utils::FileExists(name))
        return name;

    for (const auto& p : searchpaths_)
    {
        std::string f = Utils::ConcatPath(p, name);
        if (Utils::FileExists(f))
            return f;
    }
    return "";
}

bool CreateSceneAction::ShouldCreateFile(const std::string& src, const std::string& dst) const
{
    if (clean_)
        return true;
    return Utils::IsSourceNewerThanDest(src, dst);
}

static std::string GetComponentAttribute(const pugi::xml_node& node, const std::string& name)
{
    for (const auto& attr : node.children("attribute"))
    {
        const pugi::xml_attribute nameAttr = attr.attribute("name");
        const pugi::xml_attribute valueAttr = attr.attribute("value");
        if (name.compare(nameAttr.as_string()) == 0)
        {
            return valueAttr.as_string();
        }
    }
    return "";
}

bool CreateSceneAction::SaveModel(const Math::Mesh& shape, const std::string& srcFile, const std::string& filename)
{
    if (dataDir_.empty())
        return false;

    // When the bounding colume is triangle mesh or convex hull also the server needs the model.
    // Let's save the model to a simplified file format with the same name in the servers data directory.
    std::string outfile = Utils::ConcatPath(dataDir_, filename);
    std::string outdir = Utils::ExtractFileDir(outfile);
    if (!Utils::EnsureDirectory(outdir))
        return false;
    if (!ShouldCreateFile(srcFile, outfile))
    {
        std::cout << outfile << " is up to date" << std::endl;
        return true;
    }

    std::cout << "Saving model file as " << outfile << std::endl;
    return IO::SaveMesh(outfile, shape);
}


bool CreateSceneAction::LoadSceneNode(const pugi::xml_node& node, const Math::Matrix4& parentMatrix)
{
    using namespace sa::literals;

    Math::Transformation transform;

    Math::Vector3 size = Math::Vector3_One;
    Math::Vector3 offset = Math::Vector3_Zero;
    Math::Quaternion offsetRot = Math::Quaternion_Identity;

    for (const auto& attr : node.children("attribute"))
    {
        const pugi::xml_attribute nameAttr = attr.attribute("name");
        const pugi::xml_attribute valueAttr = attr.attribute("value");
        const size_t nameHash = sa::StringHash(nameAttr.as_string());
        switch (nameHash)
        {
        case "Position"_Hash:
            transform.position_ = Math::Vector3(valueAttr.as_string());
            break;
        case "Rotation"_Hash:
            transform.orientation_ = Math::Quaternion(valueAttr.as_string()).Normal();
            break;
        case "Scale"_Hash:
            transform.scale_ = Math::Vector3(valueAttr.as_string());
            break;
        }
    }

    for (const auto& comp : node.children("component"))
    {
        const pugi::xml_attribute type_attr = comp.attribute("type");
        const size_t type_hash = sa::StringHash(type_attr.as_string());
        switch (type_hash)
        {
        case "CollisionShape"_Hash:
        {
            size_t collShape = "Box"_Hash;
            for (const auto& attr : comp.children())
            {
                const pugi::xml_attribute& nameAttr = attr.attribute("name");
                const size_t nameHash = sa::StringHash(nameAttr.as_string());
                const pugi::xml_attribute& valueAttr = attr.attribute("value");
                const size_t valueHash = sa::StringHash(valueAttr.as_string());
                switch (nameHash)
                {
                case "Size"_Hash:
                    size = Math::Vector3(valueAttr.as_string());
                    break;
                case "Offset Position"_Hash:
                    offset = Math::Vector3(valueAttr.as_string());
                    break;
                case "Offset Rotation"_Hash:
                    offsetRot = Math::Quaternion(valueAttr.as_string()).Normal();
                    break;
                case "Shape Type"_Hash:
                {
                    collShape = valueHash;
                    break;
                }
                }
            }

            if (collShape == "Box"_Hash && size != Math::Vector3_Zero)
            {
                // The object has the scaling.
                const Math::Vector3 halfSize = (size * 0.5f);
                Math::BoundingBox bb(offset - halfSize, offset + halfSize);
                // Add Node and Offset rotation -> absolute orientation
                transform.orientation_ = transform.orientation_ * offsetRot;
                const Math::Matrix4 matrix = static_cast<Math::Matrix4>(transform.GetMatrix()) * parentMatrix;
                Math::Mesh shape = bb.GetMesh();
                for (auto& v : shape.vertexData_)
                    v = matrix * v;
                obstackles_.push_back(std::make_unique<Math::Mesh>(std::move(shape)));
            }
            else if ((collShape == "Sphere"_Hash || collShape == "Cylinder"_Hash) &&
                size != Math::Vector3_Zero)
            {
                // The object has the scaling.
                float radius = (size.x_ * 0.5f);
                Math::Sphere sphere(offset, radius);
                transform.orientation_ = transform.orientation_ * offsetRot;
                const Math::Matrix4 matrix = static_cast<Math::Matrix4>(transform.GetMatrix()) * parentMatrix;
                Math::Mesh shape = sphere.GetMesh();
                for (auto& v : shape.vertexData_)
                    v = matrix * v;
                obstackles_.push_back(std::make_unique<Math::Mesh>(std::move(shape)));
            }
            else if (collShape == "TriangleMesh"_Hash || collShape == "ConvexHull"_Hash)
            {
                std::string model = GetComponentAttribute(comp, "Model");
                if (!model.empty())
                {
                    // Model;Models/ResurrectionShrine1.mdl
                    auto parts = sa::Split(model, ";", false, false);
                    if (parts.size() != 2)
                    {
                        std::cerr << "Wrong format of attribute value " << model << std::endl;
                        return false;
                    }
                    std::string modelFile = FindFile(parts[1]);
                    if (!modelFile.empty())
                    {
                        Math::Mesh modelShape;
                        if (IO::Urho::LoadModel(modelFile, modelShape))
                        {
                            SaveModel(modelShape, modelFile, parts[1]);
                            const Math::Matrix4 matrix = static_cast<Math::Matrix4>(transform.GetMatrix()) * parentMatrix;
                            for (auto& v : modelShape.vertexData_)
                                v = matrix * v;
                            obstackles_.push_back(std::make_unique<Math::Mesh>(std::move(modelShape)));
                        }
                        else
                            std::cerr << "Error loading model " << modelFile << std::endl;
                    }
                    else
                        std::cerr << "File " << parts[1] << " not found" << std::endl;
                }
                else
                    std::cerr << "Missing Model attribute" << std::endl;
            }
            break;
        }
        case "Terrain"_Hash:
        {
            for (const auto& attr : comp.children())
            {
                const pugi::xml_attribute& nameAttr = attr.attribute("name");
                const size_t nameHash = sa::StringHash(nameAttr.as_string());
                const pugi::xml_attribute& valueAttr = attr.attribute("value");
                switch (nameHash)
                {
                case "Vertex Spacing"_Hash:
                    heightmapSpacing_ = Math::Vector3(valueAttr.as_string());
                    break;
                case "Patch Size"_Hash:
                    header_.patchSize = valueAttr.as_int();
                    break;
                case "Height Map"_Hash:
                {
                    // value="Image;Textures/Rhodes_Heightfield.png"
                    auto parts = sa::Split(valueAttr.as_string(), ";", false, false);
                    if (parts.size() != 2)
                    {
                        std::cerr << "Wrong format of attribute value " << valueAttr.as_string() << std::endl;
                        return false;
                    }
                    heightfieldFile_ = FindFile(parts[1]);
                    if (heightfieldFile_.empty())
                    {
                        std::cerr << "File " << parts[1] << " not found" << std::endl;
                        return false;
                    }
                    break;
                }
                }
            }
        }
        }
    }

    const Math::Matrix4 matrix = static_cast<Math::Matrix4>(transform.GetMatrix()) * parentMatrix;
    for (const auto& node : node.children("node"))
    {
        if (!LoadSceneNode(node, matrix))
            return false;
    }

    return true;
}

bool CreateSceneAction::LoadScene()
{
    header_.patchSize = 32;
    std::cout << "Loading scene " << file_ << std::endl;
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_file(file_.c_str());
    if (result.status != pugi::status_ok)
    {
        std::cerr << "Error loading file " << file_ << ": " << result.description() << std::endl;
        return false;
    }
    pugi::xml_node sceneNode = doc.child("scene");
    if (!sceneNode)
    {
        std::cerr << "WARNING: There is no scene node in file " << file_ << std::endl;
        sceneNode = doc.child("node");
        if (!sceneNode)
        {
            std::cerr << "File " << file_ << " does not have a scene node" << std::endl;
            return false;
        }
    }
    for (pugi::xml_node_iterator it = sceneNode.begin(); it != sceneNode.end(); ++it)
    {
        if (strcmp((*it).name(), "node") == 0)
        {
            if (!LoadSceneNode(*it, Math::Matrix4_Identity))
            {
                std::cerr << "Error loading scene node" << std::endl;
                // Can't continue
                return false;
            }
        }
    }
    return true;
}
