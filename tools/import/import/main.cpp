/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "stdafx.h"
#include "Application.h"

#include <sa/Compiler.h>

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#undef STB_IMAGE_IMPLEMENTATION
PRAGMA_WARNING_POP

int main(int argc, char** argv)
{
    Application app;
    if (!app.Initialize(argc, argv))
        return EXIT_FAILURE;

    app.Run();
    return EXIT_SUCCESS;
}
