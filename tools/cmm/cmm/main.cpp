/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <libcommon/StringUtils.h>
#include <libio/Heightmap.h>
#include <libio/Mesh.h>
#include <libmath/HeightMapTools.h>
#include <libmath/MathDefs.h>
#include <sa/ArgParser.h>
#include <sa/Compiler.h>
#include <sa/StringTempl.h>
#include <sa/color.h>
#include <fstream>
#include <iostream>

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image.h>
#include <stb_image_write.h>
#undef STB_IMAGE_IMPLEMENTATION
#undef STB_IMAGE_WRITE_IMPLEMENTATION
PRAGMA_WARNING_POP

Math::Vector3 scaling = { 1.0f, 0.25f, 1.0f };
int patchSize = 32;

struct Layer
{
    ea::vector<float> heights;
    sa::Color color;
    bool invert = false;
};

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "layer1",
        { "-L1", "--layer-1" },
        "Layer 1, format <color>:<file> e.g. -L1 FF0000:myfile.png",
        true,
        true,
        sa::ArgParser::OptionType::String });
    cli.push_back({ "layer2", { "-L2", "--layer-2" }, "Layer 2", false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "layer3", { "-L3", "--layer-3" }, "Layer 3", false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "layer4", { "-L4", "--layer-4" }, "Layer 4", false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "invert1",
        { "-I1", "--invert-1" },
        "Invert layer 1, i.e. higher values -> darker colors",
        false,
        false,
        sa::ArgParser::OptionType::None });
    cli.push_back(
        { "invert2", { "-I2", "--invert-2" }, "Invert layer 2", false, false, sa::ArgParser::OptionType::None });
    cli.push_back(
        { "invert3", { "-I3", "--invert-3" }, "Invert layer 3", false, false, sa::ArgParser::OptionType::None });
    cli.push_back(
        { "invert4", { "-I4", "--invert-4" }, "Invert layer 4", false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "scalex",
        { "-X", "--scale-x" },
        "X scaling (default 1.0)",
        false,
        true,
        sa::ArgParser::OptionType::Float });
    cli.push_back({ "scaley",
        { "-Y", "--scale-y" },
        "Y scaling (default 0.25)",
        false,
        true,
        sa::ArgParser::OptionType::Float });
    cli.push_back({ "scalez",
        { "-Z", "--scale-z" },
        "Z scaling (default 1.0)",
        false,
        true,
        sa::ArgParser::OptionType::Float });
    cli.push_back({ "patchsize",
        { "-P", "--patch-size" },
        "Patch size (default 32)",
        false,
        true,
        sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetwidth",
        { "-W", "--target-width" },
        "Target image width",
        false,
        true,
        sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetheight",
        { "-H", "--target-height" },
        "Target image height",
        false,
        true,
        sa::ArgParser::OptionType::Integer });
    cli.push_back(
        { "output", { "-o", "--output" }, "Output PNG file", true, true, sa::ArgParser::OptionType::String });
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("cmm", _cli, "Create client mini map");
}

static void ShowInfo()
{
    std::cout << "cmm - Create client mini map" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static bool GetHeights(const std::string& filename,
    int targetWidth,
    int targetHeight,
    ea::vector<float>& heights,
    int& width,
    int& height,
    float& minHeight,
    float& maxHeight)
{
    std::string ext = Utils::GetFileExt(filename);

    if (Utils::StringEquals(ext, ".png"))
    {
        int comps = 0;
        unsigned char* data = stbi_load(filename.c_str(), &width, &height, &comps, 0);
        if (!data)
            return false;

        Math::HeightmapHeader header;
        heights = Math::CreateHeightMapFromImage(
            (const unsigned char*)data, width, height, comps, scaling, patchSize, header);
        minHeight = header.minHeight;
        maxHeight = header.maxHeight;
        free(data);
        return true;
    }

    if (Utils::StringEquals(ext, ".obj"))
    {
        Math::Mesh mesh;
        if (!IO::LoadMeshFromOBJ(filename, mesh))
            return false;

        heights = Math::CreateHeightMapFromMesh(mesh, targetWidth, targetHeight, width, height, minHeight, maxHeight);
        return true;
    }
    if (Utils::StringEquals(ext, ".hm"))
    {
        Math::HeightmapHeader header;
        width = targetWidth;
        height = targetHeight;
        heights = IO::LoadHeightmap(filename, header);
        minHeight = header.minHeight;
        maxHeight = header.maxHeight;
#ifdef _DEBUG
        std::cout << "Num vertices: " << numVertices << ", patch size: " << patchSize
                  << ", patch world size: " << patchWorldSize << ", num patches: " << numPatches
                  << ", patch world origin: " << patchWorldOrigin << ", min/max height: " << minHeight << "/"
                  << maxHeight << ", n height values: " << heights.size() << std::endl;
#endif
        return true;
    }
    if (Utils::StringEquals(ext, ".obstacles"))
    {
        Math::Mesh mesh;
        if (!IO::LoadObstacles(filename, mesh))
            return false;

        heights = Math::CreateHeightMapFromMesh(mesh, targetWidth, targetHeight, width, height, minHeight, maxHeight);
        return true;
    }

    std::cerr << "Unknown file type " << filename << std::endl;
    return false;
}

static bool CreateImage(const std::string& filename,
    const Layer& layer1,
    const Layer& layer2,
    const Layer& layer3,
    const Layer& layer4,
    int width,
    int height,
    float minHeight,
    float maxHeight)
{
    // These images always have 4 components: RGBA
    const int comps = 4;

    unsigned char* data = (unsigned char*)malloc((size_t)width * (size_t)height * (size_t)comps);
    if (!data)
        return false;

    memset(data, 0, (size_t)width * (size_t)height * (size_t)comps);

    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            const size_t index = ((size_t)y * ((size_t)width) + (size_t)x);
            if (index >= layer1.heights.size())
                continue;

            const float value1 = layer1.heights[index];
            if (Math::IsNegInfinite(value1))
                continue;
            sa::Color finalColor = layer1.color.Filtered<sa::filter::Scale>(
                { value1, minHeight, maxHeight, layer1.invert });

            if (index < layer2.heights.size())
            {
                const float value2 = layer2.heights[index];
                if (!Math::IsNegInfinite(value2))
                    finalColor.Filter<sa::filter::AlphaBlend>(
                        {}, layer2.color.Filtered<sa::filter::Scale>({ value2, minHeight, maxHeight, layer2.invert }));
            }

            if (index < layer3.heights.size())
            {
                const float value3 = layer3.heights[index];
                if (!Math::IsNegInfinite(value3))
                    finalColor.Filter<sa::filter::AlphaBlend>(
                        {}, layer3.color.Filtered<sa::filter::Scale>({ value3, minHeight, maxHeight, layer3.invert }));
            }

            if (index < layer4.heights.size())
            {
                const float value4 = layer4.heights[index];
                if (!Math::IsNegInfinite(value4))
                    finalColor.Filter<sa::filter::AlphaBlend>(
                        {}, layer4.color.Filtered<sa::filter::Scale>({ value4, minHeight, maxHeight, layer4.invert }));
            }

            const size_t dataIndex = ((size_t)height - y - 1) * (size_t)width + (size_t)x;
            *reinterpret_cast<uint32_t*>(&data[(dataIndex * comps)]) = finalColor.To32();
        }
    }

    stbi_write_png(filename.c_str(), width, height, comps, data, width * comps);

    std::cout << "Created " << filename << " width " << width << ", height " << height << ", min/max height "
              << minHeight << "/" << maxHeight << std::endl;

    free(data);
    return true;
}

int main(int argc, char** argv)
{
    ShowInfo();
    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }

    scaling.x_ = sa::ArgParser::GetValue<float>(parsedArgs, "scalex", scaling.x_);
    scaling.y_ = sa::ArgParser::GetValue<float>(parsedArgs, "scaley", scaling.y_);
    scaling.z_ = sa::ArgParser::GetValue<float>(parsedArgs, "scalez", scaling.z_);
    patchSize = sa::ArgParser::GetValue<int>(parsedArgs, "patchsize", patchSize);
    int targetWidth = sa::ArgParser::GetValue<int>(parsedArgs, "targetwidth", 0);
    int targetHeight = sa::ArgParser::GetValue<int>(parsedArgs, "targetheight", 0);

    std::string layer1Option = sa::ArgParser::GetValue<std::string>(parsedArgs, "layer1", "");
    if (layer1Option.empty())
    {
        return 1;
    }
    std::string layer2Option = sa::ArgParser::GetValue<std::string>(parsedArgs, "layer2", "");
    std::string layer3Option = sa::ArgParser::GetValue<std::string>(parsedArgs, "layer3", "");
    std::string layer4Option = sa::ArgParser::GetValue<std::string>(parsedArgs, "layer4", "");

    int width = 0;
    int height = 0;
    float minHeight = std::numeric_limits<float>::max();
    float maxHeight = std::numeric_limits<float>::min();

    Layer layer1;
    Layer layer2;
    Layer layer3;
    Layer layer4;

    layer1.invert = sa::ArgParser::GetValue<bool>(parsedArgs, "invert1", false);
    layer2.invert = sa::ArgParser::GetValue<bool>(parsedArgs, "invert2", false);
    layer3.invert = sa::ArgParser::GetValue<bool>(parsedArgs, "invert3", false);
    layer4.invert = sa::ArgParser::GetValue<bool>(parsedArgs, "invert4", false);

    std::vector<std::string> layer1Parts = sa::Split(layer1Option, ":", false, false);
    if (layer1Parts.size() != 2)
    {
        return 1;
    }
    int w, h;
    float minH, maxH;
    layer1.color = sa::Color::FromString(layer1Parts[0]);
    if (!GetHeights(layer1Parts[1], targetWidth, targetHeight, layer1.heights, w, h, minH, maxH))
    {
        std::cerr << "Unable to load file " << layer1Parts[1] << std::endl;
        return 1;
    }
    width = std::max(width, w);
    height = std::max(height, h);
    minHeight = std::min(minHeight, minH);
    maxHeight = std::max(maxHeight, maxH);

    if (!layer2Option.empty())
    {
        std::vector<std::string> layer2Parts = sa::Split(layer2Option, ":", false, false);
        if (layer2Parts.size() != 2)
        {
            return 1;
        }
        layer2.color = sa::Color::FromString(layer2Parts[0]);
        if (!GetHeights(layer2Parts[1], targetWidth, targetHeight, layer2.heights, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << layer2Parts[1] << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (layer1.heights.size() != layer2.heights.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }
    if (!layer3Option.empty())
    {
        std::vector<std::string> layer3Parts = sa::Split(layer3Option, ":", false, false);
        if (layer3Parts.size() != 2)
        {
            return 1;
        }
        layer3.color = sa::Color::FromString(layer3Parts[0]);
        if (!GetHeights(layer3Parts[1], targetWidth, targetHeight, layer3.heights, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << layer3Parts[1] << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (layer1.heights.size() != layer3.heights.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }
    if (!layer4Option.empty())
    {
        std::vector<std::string> layer4Parts = sa::Split(layer4Option, ":", false, false);
        if (layer4Parts.size() != 2)
        {
            return 1;
        }
        layer4.color = sa::Color::FromString(layer4Parts[0]);
        if (!GetHeights(layer4Parts[1], targetWidth, targetHeight, layer4.heights, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << layer4Parts[1] << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (layer1.heights.size() != layer4.heights.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }

    std::string outfile = sa::ArgParser::GetValue<std::string>(parsedArgs, "output", "");

    if (!CreateImage(outfile, layer1, layer2, layer3, layer4, width, height, minHeight, maxHeight))
    {
        std::cerr << "Error creating image" << std::endl;
        return 1;
    }

    return 0;
}
