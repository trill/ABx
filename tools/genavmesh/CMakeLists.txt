project (genavmesh CXX)

file(GLOB GENAVMESH_SOURCES      genavmesh/*.cpp genavmesh/*.h)

add_executable(
    genavmesh
    ${GENAVMESH_SOURCES}
)

target_compile_options(genavmesh PRIVATE -Wno-reorder -Wimplicit-fallthrough=0)
target_precompile_headers(genavmesh PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/genavmesh/stdafx.h)

target_include_directories(genavmesh PRIVATE ${CMAKE_SOURCE_DIR}/include/stb)
target_link_libraries(genavmesh libmath libcommon libio EASTL DebugUtils Recast Detour DetourTileCache)

install(TARGETS genavmesh
    RUNTIME DESTINATION bin
    COMPONENT runtime
)
