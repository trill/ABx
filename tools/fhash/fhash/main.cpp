/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <libsync/Hash.h>
#include <libsync/Partition.h>
#include <libsync/SyncDefs.h>
#include <sa/ArgParser.h>
#include <sa/time.h>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("fhash", _cli, "Calculate file block hashes", "[<directory>]");
    std::cout << std::endl;
    std::cout << "If no directory is given, it uses the current directory" << std::endl;
}

static void ShowInfo()
{
    std::cout << "fhash - Calculate file block hashes" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static void ProcessEntry(const fs::directory_entry& p)
{
    if (p.is_directory())
        return;
    if (p.path().extension().string() == std::string(Sync::META_FILE_EXT)
        || p.path().extension().string() == std::string(Sync::HASH_FILE_EXT))
        return;

    const std::string filename = p.path().string();
    if (filename.empty())
        return;
    if ((filename != "..") && (filename != ".")
        && ((filename[0] == '.') || (filename.find("/.") != std::string::npos)
            || (filename.find("\\.") != std::string::npos)))
        return;

    std::cout << "Processing file " << filename << std::endl;
    const auto hashes = Sync::PartitionFile(filename, {});
    if (hashes.empty())
        // File with 0 bytes
        return;
    Sync::SaveBoundaryList(filename, hashes);
    Sync::CreateFileHash(filename, filename + std::string(Sync::HASH_FILE_EXT));
}

template<typename T>
static void ProcessDirectory(const T& iterator)
{
    for (const auto& p : iterator)
        ProcessEntry(p);
}

int main(int argc, char** argv)
{
    ShowInfo();
    sa::ArgParser::Cli _cli{
        { { "help", { "-h", "--help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None },
            { "recursive",
                { "-R", "--recursive" },
                "Processs also subdirectores",
                false,
                false,
                sa::ArgParser::OptionType::None } }
    };

    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return EXIT_SUCCESS;
    }

    if (!cmdres)
        return EXIT_FAILURE;

    auto recursive = sa::ArgParser::GetValue<bool>(parsedArgs, "recursive", false);

    auto dirarg = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    fs::path dir = dirarg.has_value() ? fs::path(dirarg.value()) : fs::current_path();

    std::cout << "Directory " << dir.string() << std::endl;
    sa::time::timer timer;

    if (recursive)
        ProcessDirectory(fs::recursive_directory_iterator(dir));
    else
        ProcessDirectory(fs::directory_iterator(dir));

    std::cout << "Took " << timer.elapsed_seconds() << " seconds" << std::endl;
    return EXIT_SUCCESS;
}
