/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <sa/ArgParser.h>
#include <iostream>
#include <sa/Compiler.h>
#include <libmath/HeightMapTools.h>
#include <libcommon/StringUtils.h>
#include <libio/Heightmap.h>
#include <fstream>
#include <libmath/MathDefs.h>
#include <libio/Mesh.h>

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244 4456)
PRAGMA_WARNING_DISABLE_GCC("-Wimplicit-fallthrough=0")
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image.h>
#include <stb_image_write.h>
#undef STB_IMAGE_IMPLEMENTATION
#undef STB_IMAGE_WRITE_IMPLEMENTATION
PRAGMA_WARNING_POP

static Math::Vector3 scaling = { 1.0f, 0.25f, 1.0f };
static int patchSize = 32;

static void InitCli(sa::ArgParser::Cli& cli)
{
    cli.push_back({ "help", { "-h", "--help", "-?" }, "Show help",
        false, false, sa::ArgParser::OptionType::None });
    cli.push_back({ "red", { "-R", "--red" }, "Red channel",
        true, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "green", { "-G", "--green" }, "Creen channel",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "blue", { "-B", "--blue" }, "Blue channel",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "alpha", { "-A", "--alpha" }, "Alpha channel",
        false, true, sa::ArgParser::OptionType::String });
    cli.push_back({ "scalex", { "-X", "--scale-x" }, "X scaling (default 1.0)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "scaley", { "-Y", "--scale-y" }, "Y scaling (default 0.25)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "scalez", { "-Z", "--scale-z" }, "Z scaling (default 1.0)",
        false, true, sa::ArgParser::OptionType::Float });
    cli.push_back({ "patchsize", { "-P", "--patch-size" }, "Patch size (default 32)",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetwidth", { "-W", "--target-width" }, "Target image width",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "targetheight", { "-H", "--target-height" }, "Target image height",
        false, true, sa::ArgParser::OptionType::Integer });
    cli.push_back({ "output", { "-o", "--output" }, "Output PNG file",
        true, true, sa::ArgParser::OptionType::String });
}

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("hmerge", _cli, "Merge heightmaps");
}

static void ShowInfo()
{
    std::cout << "hmerge - Merge heightmaps" << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;
}

static bool GetHeights(const std::string& filename, int targetWidth, int targetHeight,
    ea::vector<float>& heights, int& width, int& height, float& minHeight, float& maxHeight)
{
    std::string ext = Utils::GetFileExt(filename);

    if (Utils::StringEquals(ext, ".png"))
    {
        int comps = 0;
        unsigned char* data = stbi_load(filename.c_str(), &width, &height, &comps, 0);
        if (!data)
            return false;

        Math::HeightmapHeader header;
        heights = Math::CreateHeightMapFromImage((const unsigned char*)data, width, height, comps, scaling, patchSize,
            header);
        free(data);
        return true;
    }

    if (Utils::StringEquals(ext, ".obj"))
    {
        Math::Mesh mesh;
        if (!IO::LoadMeshFromOBJ(filename, mesh))
            return false;

        heights = Math::CreateHeightMapFromMesh(mesh, targetWidth, targetHeight, width, height, minHeight, maxHeight);
        return true;
    }
    if (Utils::StringEquals(ext, ".hm"))
    {
        Math::HeightmapHeader header;
        width = targetWidth;
        height = targetHeight;
        heights = IO::LoadHeightmap(filename, header);
        patchSize = header.patchSize;
#ifdef _DEBUG
        std::cout << "Num vertices: " << header.numVertices << ", patch size: " << header.patchSize <<
            ", patch world size: " << header.patchWorldSize << ", num patches: " << header.numPatches <<
            ", patch world origin: " << header.patchWorldOrigin << ", min/max height: " << header.minHeight << "/" << header.maxHeight <<
            ", n height values: " << heights.size() << std::endl;
#endif
        return true;
    }
    if (Utils::StringEquals(ext, ".obstacles"))
    {
        Math::Mesh mesh;
        if (!IO::LoadObstacles(filename, mesh))
            return false;

        heights = Math::CreateHeightMapFromMesh(mesh, targetWidth, targetHeight, width, height, minHeight, maxHeight);
        return true;
    }

    std::cerr << "Unknown file type " << filename << std::endl;
    return false;
}

static bool CreateImage(const std::string& filename,
    const ea::vector<float>& red,
    const ea::vector<float>& green,
    const ea::vector<float>& blue,
    const ea::vector<float>& alpha,
    int width, int height,
    float minHeight, float maxHeight
)
{
    // We either have 1, 3 or 4 components
    int comps = 1;
    if (!alpha.empty())
        comps = 4;
    else if (!blue.empty() || !green.empty())
        comps = 3;

    const float zD = maxHeight - minHeight;

    unsigned char* data = (unsigned char*)malloc((size_t)width * (size_t)height * (size_t)comps);
    if (!data)
        return false;

    memset(data, 0, (size_t)width * (size_t)height * (size_t)comps);

    auto setValue = [&](const ea::vector<float>& heights, size_t x, size_t y, size_t offset)
    {
        const size_t index = ((size_t)y * ((size_t)width) + (size_t)x);
        if (heights.empty() || index >= heights.size())
            return;

        const float value = heights[index];
        if (Math::Equals(value, -Math::M_INFINITE))
            return;

        const unsigned char heightValue = static_cast<unsigned char>(((value - minHeight) / zD) * 255.0f);
        const size_t _index = ((size_t)height - y - 1) * (size_t)width + (size_t)x;
        data[(_index * comps) + offset] = heightValue;
    };

    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            setValue(red, x, y, 0);
            if (comps > 1)
                setValue(green, x, y, 1);
            if (comps > 2)
                setValue(blue, x, y, 2);
            if (comps > 3)
                setValue(alpha, x, y, 3);
        }
    }

    stbi_write_png(filename.c_str(), width, height, comps, data, width * comps);

    std::cout << "Created " << filename << " width " << width << ", height " << height <<
        ", min/max height " << minHeight << "/" << maxHeight << std::endl;

    free(data);
    return true;
}

int main(int argc, char** argv)
{
    ShowInfo();
    sa::ArgParser::Cli _cli;
    InitCli(_cli);
    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return 0;
    }
    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return 1;
    }

    scaling.x_ = sa::ArgParser::GetValue<float>(parsedArgs, "scalex", scaling.x_);
    scaling.y_ = sa::ArgParser::GetValue<float>(parsedArgs, "scaley", scaling.y_);
    scaling.z_ = sa::ArgParser::GetValue<float>(parsedArgs, "scalez", scaling.z_);
    patchSize = sa::ArgParser::GetValue<int>(parsedArgs, "patchsize", patchSize);
    int targetWidth = sa::ArgParser::GetValue<int>(parsedArgs, "targetwidth", 0);
    int targetHeight = sa::ArgParser::GetValue<int>(parsedArgs, "targetheight", 0);

    std::string redFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "red", "");
    if (redFile.empty())
    {
        return 1;
    }
    std::cout << "Red layer " << redFile << std::endl;

    std::string greenFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "green", "");
    std::string blueFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "blue", "");
    std::string alphaFile = sa::ArgParser::GetValue<std::string>(parsedArgs, "alpha", "");

    int width = 0;
    int height = 0;
    float minHeight = std::numeric_limits<float>::max();
    float maxHeight = std::numeric_limits<float>::min();

    ea::vector<float> red;
    ea::vector<float> green;
    ea::vector<float> blue;
    ea::vector<float> alpha;

    int w, h;
    float minH, maxH;

    if (!GetHeights(redFile, targetWidth, targetHeight, red, w, h, minH, maxH))
    {
        std::cerr << "Unable to load file " << redFile << std::endl;
        return 1;
    }
    width = std::max(width, w);
    height = std::max(height, h);
    minHeight = std::min(minHeight, minH);
    maxHeight = std::max(maxHeight, maxH);

    if (!greenFile.empty())
    {
        std::cout << "Green layer " << greenFile << std::endl;
        if (!GetHeights(greenFile, targetWidth, targetHeight, green, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << greenFile << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (red.size() != green.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }
    if (!blueFile.empty())
    {
        std::cout << "Blue layer " << blueFile << std::endl;
        if (!GetHeights(blueFile, targetWidth, targetHeight, blue, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << blueFile << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (red.size() != blue.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }
    if (!alphaFile.empty())
    {
        std::cout << "Alpha layer " << alphaFile << std::endl;
        if (!GetHeights(alphaFile, targetWidth, targetHeight, alpha, w, h, minH, maxH))
        {
            std::cerr << "Unable to load file " << alphaFile << std::endl;
            return 1;
        }
        width = std::max(width, w);
        height = std::max(height, h);
        minHeight = std::min(minHeight, minH);
        maxHeight = std::max(maxHeight, maxH);
        if (red.size() != alpha.size())
        {
            std::cout << "WARNING: Layers have different sizes" << std::endl;
        }
    }

    std::string outfile = sa::ArgParser::GetValue<std::string>(parsedArgs, "output", "");
    if (!CreateImage(outfile, red, green, blue, alpha, width, height, minHeight, maxHeight))
    {
        std::cerr << "Error creating image" << std::endl;
        return 1;
    }
    return 0;
}
