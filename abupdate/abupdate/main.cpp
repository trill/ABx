/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <AB/CommonConfig.h>
#include <AB/DHKeys.hpp>
#include <libsync/Hash.h>
#include <libsync/Synchronizer.h>
#include <libsync/Updater.h>
#include <asio.hpp>
#include <filesystem>
#include <iostream>
#include <libclient/Client.h>
#include <libclient/Platform.h>
#include <libclient/ProtocolLogin.h>
#include <memory>
#include <sa/ArgParser.h>
#include <sa/Process.h>
#include <sa/StringTempl.h>
#include <sa/time.h>
#ifdef AB_UNIX
#include <unistd.h>
#endif
#include <chillout.h>

namespace fs = std::filesystem;

static std::vector<std::string> patterns = { "*.pak" };

static std::string host;
static uint16_t port;
static std::string account;
static std::string token;
static std::string username;
static std::string password;

static void ShowHelp(const sa::ArgParser::Cli& _cli)
{
    std::cout << sa::ArgParser::GetHelp("abupdate", _cli, "Client updater");
    std::cout << std::endl;
    std::cout << "You must either pass --account-key plus --auth-token or --username and --password arguments." << std::endl;
}

static std::string GetAuthHeader()
{
    if (!account.empty() && !token.empty())
        return account + token;

    if (username.empty() && password.empty())
    {
        std::cerr << "Username and/or password missing" << std::endl;
        return "";
    }

    Crypto::DHKeys keys;
    keys.GenerateKeys();
    asio::io_service ioService;

    std::string accUuid;
    std::string atoken;
    std::shared_ptr<Client::ProtocolLogin> login = std::make_shared<Client::ProtocolLogin>(keys, ioService);
    login->SetErrorCallback([](Client::ConnectionError error, const std::error_code&) {
        std::cerr << "Network error: " << Client::Client::GetNetworkErrorMessage(error) << std::endl;
    });
    login->SetProtocolErrorCallback([](AB::ErrorCodes error) {
        std::cerr << "Protocol error " << Client::Client::GetProtocolErrorMessage(error) << std::endl;
    });
    login->Login(host, port, username, password,
        [&accUuid, &atoken](const std::string& accountUuid, const std::string& authToken, AB::Entities::AccountType)
    {
        accUuid = accountUuid;
        atoken = authToken;
    },
        [](const AB::Entities::CharList&)
    {
        // Nothing to do here
    });
    ioService.run();
    if (accUuid.empty() || atoken.empty())
        return "";

    port = login->filePort_;
    if (!login->fileHost_.empty())
        host = login->fileHost_;

    return accUuid + atoken;
}

int main(int argc, char** argv)
{
    auto &chillout = Debug::Chillout::getInstance();
    chillout.init("abupdate", fs::temp_directory_path().string());
    chillout.setBacktraceCallback([](const char * const stackEntry)
    {
        fprintf(stderr, "  %s\n", stackEntry);
    });

    chillout.setCrashCallback([&chillout](const char * const message)
    {
        std::cerr << message << std::endl;
        chillout.backtrace();
#ifdef AB_WINDOWS
        chillout.createCrashDump();
#endif
    });

    std::cout << "This is AB Client updater";
#ifdef _DEBUG
    std::cout << " DEBUG";
#endif
    std::cout << std::endl;
    std::cout << "Running on " << System::GetPlatform() << std::endl << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;

    sa::ArgParser::Cli _cli{ {
        { "help",    { "-h", "--help", "-?" },    "Show help", false, false, sa::ArgParser::OptionType::None },
        { "host",    { "-H", "--server-host" },   "Server host", true, true, sa::ArgParser::OptionType::String },
        { "port",    { "-P", "--server-port" },   "Server port", true, true, sa::ArgParser::OptionType::Integer },
        { "account", { "-a", "--account-key" },   "Account", false, true, sa::ArgParser::OptionType::String },
        { "token",   { "-t", "--auth-token" },    "Auth token to login", false, true, sa::ArgParser::OptionType::String },
        { "user",    { "-u", "--username" },      "Username to login", false, true, sa::ArgParser::OptionType::String },
        { "pass",    { "-p", "--password" },      "Password to login", false, true, sa::ArgParser::OptionType::String },
        { "pattern", { "-m", "--match-pattern" }, "Filename pattern (default *.pak)", false, true, sa::ArgParser::OptionType::String },
        { "clean",   { "-c", "--clean" },         "Clean download everything", false, false, sa::ArgParser::OptionType::None },
        { "run",     { "-r", "--run" },           "Run program after update", false, true, sa::ArgParser::OptionType::String },
        { "directory", { },                       "Directory to update (default current directory)", false, true, sa::ArgParser::OptionType::String },
    } };

    sa::ArgParser::Values parsedArgs;
    sa::ArgParser::Result cmdres = sa::ArgParser::Parse(argc, argv, _cli, parsedArgs);
    auto val = sa::ArgParser::GetValue<bool>(parsedArgs, "help");
    if (val.has_value() && val.value())
    {
        ShowHelp(_cli);
        return EXIT_SUCCESS;
    }

    if (!cmdres)
    {
        std::cout << cmdres << std::endl;
        ShowHelp(_cli);
        return EXIT_FAILURE;
    }

    // File or login server
    host = sa::ArgParser::GetValue<std::string>(parsedArgs, "host", "");
    if (host.empty())
    {
        std::cerr << "No host" << std::endl;
        return EXIT_FAILURE;
    }
    port = sa::ArgParser::GetValue<uint16_t>(parsedArgs, "port", 0u);
    if (port == 0)
    {
        std::cerr << "No port" << std::endl;
        return EXIT_FAILURE;
    }

    auto pattern = sa::ArgParser::GetValue<std::string>(parsedArgs, "pattern");
    if (pattern.has_value())
    {
        patterns = sa::Split(pattern.value(), ";", false, false);
    }

    account = sa::ArgParser::GetValue<std::string>(parsedArgs, "account", "");
    token = sa::ArgParser::GetValue<std::string>(parsedArgs, "token", "");
    username = sa::ArgParser::GetValue<std::string>(parsedArgs, "user", "");
    password = sa::ArgParser::GetValue<std::string>(parsedArgs, "pass", "");

    if ((account.empty() || token.empty()) && (username.empty() || password.empty()))
    {
        std::cout << "Enter username: ";
        std::getline(std::cin, username);
#ifdef AB_UNIX
        password = getpass("Enter password: ");
#else
        std::cout << "Enter password: ";
        std::getline(std::cin, password);
#endif
    }
    const std::string authHeader = GetAuthHeader();
    if (authHeader.empty())
    {
        std::cerr << "Unable to login" << std::endl;
        return EXIT_FAILURE;
    }

    bool clean = sa::ArgParser::GetValue<bool>(parsedArgs, "clean", false);
    auto dirarg = sa::ArgParser::GetValue<std::string>(parsedArgs, "0");
    fs::path dir = dirarg.has_value() ? fs::path(dirarg.value()) : fs::current_path();
    std::string indexFile = sa::StringToLower(System::GetPlatform()) + "/_files_";
    Sync::Updater updater(host, port, authHeader, dir.string(), indexFile);
    updater.clean_ = clean;
    updater.onError = [](Sync::Updater::ErrorType type, const char* message)
    {
        if (type == Sync::Updater::ErrorType::Remote)
            std::cerr << "HTTP Error ";
        else if (type == Sync::Updater::ErrorType::Local)
            std::cerr << "File Error ";
        std::cerr << message << std::endl;
    };
    updater.onProcessFile_ = [](size_t, size_t, const std::string& filename) -> bool
    {
        bool match = false;
        for (const auto& pattern : patterns)
        {
            if (sa::PatternMatch<char>(filename, pattern))
            {
                match = true;
                break;
            }
        }
        if (match)
        {
            std::cout << "Processing file " << filename << std::endl;
        }
        return match;
    };
    updater.onFailure_ = [](const std::string& filename)
    {
        std::cerr << "Error synchronizing " << filename << std::endl;
    };
    updater.onDoneFile_ = [](const std::string&, bool different, size_t downloaded, size_t copied, int savings)
    {
        if (different)
        {
            std::cout << "  Copied " << copied << " bytes" << std::endl;
            std::cout << "  Downloaded " << downloaded << " bytes" << std::endl;
            std::cout << "  Download savings " << savings << "%" << std::endl;
        }
        else
        {
            std::cout << "  File is up to date" << std::endl;
        }
    };
    updater.onProgress_ = [](size_t, size_t, size_t value, size_t max)
    {
        std::cout << '\r';
        std::cout << "[" << value << "/" << max << "]";
        if (value == max)
            std::cout << " done" << std::endl;
        std::cout << std::flush;
    };

    std::cout << "Updating " << dir.string() << "..." << std::endl;
    sa::time::timer timer;
    bool result = updater.Execute();
    std::cout << "Took " << timer.elapsed_seconds() << " seconds" << std::endl;
    if (result)
    {
        auto run = sa::ArgParser::GetValue<std::string>(parsedArgs, "run");
        if (run.has_value())
        {
            std::stringstream ss;
            ss << "\"" << run.value() << "\"";
            sa::Process::Run(ss.str());
        }
    }

    return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
