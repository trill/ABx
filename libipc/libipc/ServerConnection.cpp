/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ServerConnection.h"
#include "IpcServer.h"
#include <iostream>

namespace IPC {

sa::IdGenerator<uint32_t> ServerConnection::sIdGen;

ServerConnection::ServerConnection(asio::io_service& ioService, Server& server) :
    socket_(ioService),
    strand_(ioService),
    server_(server),
    id_(sIdGen.Next())
{ }

void ServerConnection::Start()
{
    asio::async_read(socket_,
        asio::buffer(readBuffer_.Data(), MessageBuffer::HeaderLength),
        std::bind(
            &ServerConnection::HandleRead, shared_from_this(), std::placeholders::_1
        ));
}

void ServerConnection::Close()
{
    asio::error_code err2;
    socket_.close(err2);
    server_.RemoveConnection(shared_from_this());
}

void ServerConnection::HandleRead(const asio::error_code& error)
{
    if (error || !readBuffer_.DecodeHeader())
    {
        Close();
        return;
    }

    asio::read(socket_, asio::buffer(readBuffer_.Body(), readBuffer_.BodyLength()));

    // Client sent a message
    server_.HandleMessage(*this, readBuffer_);

    readBuffer_.Empty();
    asio::async_read(socket_,
        asio::buffer(readBuffer_.Data(), MessageBuffer::HeaderLength),
        std::bind(
            &ServerConnection::HandleRead, shared_from_this(), std::placeholders::_1
        ));
}

void ServerConnection::HandleWrite(const asio::error_code& error, size_t)
{
    writeBuffers_.pop_front();
    if (error)
        return;
    if (!writeBuffers_.empty())
        Write();
}

void ServerConnection::WriteImpl(const MessageBuffer& msg)
{
    writeBuffers_.push_back(msg);
    if (writeBuffers_.size() > 1)
        // Write in progress
        return;
    Write();
}

void ServerConnection::Write()
{
    const MessageBuffer& msg = writeBuffers_[0];
    asio::async_write(
        socket_,
        asio::buffer(msg.Data(), msg.Length()),
        strand_.wrap(
            std::bind(
                &ServerConnection::HandleWrite,
                shared_from_this(),
                std::placeholders::_1,
                std::placeholders::_2
            )
        )
    );
}

void ServerConnection::Send(const MessageBuffer& msg)
{
    strand_.post(std::bind(&ServerConnection::WriteImpl, this, msg));
}

uint32_t ServerConnection::GetIP() const
{
    asio::error_code err;
    const asio::ip::tcp::endpoint endpoint = socket_.remote_endpoint(err);
    if (!err)
        return endpoint.address().to_v4().to_uint();
    return 0;
}

uint16_t ServerConnection::GetPort() const
{
    asio::error_code err;
    const asio::ip::tcp::endpoint endpoint = socket_.remote_endpoint(err);
    if (!err)
        return endpoint.port();
    return 0;
}

}
