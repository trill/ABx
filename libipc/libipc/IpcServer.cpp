/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IpcServer.h"
#include "ServerConnection.h"
#include "MessageBuffer.h"
#include <algorithm>

namespace IPC {

Server::Server(asio::io_service& ioService, const asio::ip::tcp::endpoint& endpoint) :
    ioService_(ioService),
    acceptor_(ioService, endpoint)
{
    StartAccept();
}

void Server::RemoveConnection(std::shared_ptr<ServerConnection> conn)
{
    std::scoped_lock lock(lock_);
    if (onClientDisconnect)
        onClientDisconnect(*conn);

    clients_.erase(conn);
}

void Server::AddConnection(std::shared_ptr<ServerConnection> conn)
{
    std::scoped_lock lock(lock_);
    clients_.emplace(conn);

    if (onClientConnect)
        onClientConnect(*conn);
}

ServerConnection* Server::GetConnection(uint32_t id)
{
    auto it = std::find_if(clients_.begin(), clients_.end(), [&id](const std::shared_ptr<ServerConnection>& current)
    {
        return id == current->GetId();
    });
    if (it == clients_.end())
        return nullptr;

    return it->get();
}

void Server::DisconnectClient(ServerConnection& client)
{
    client.Close();
}

void Server::DisconnectClient(uint32_t id)
{
    auto* client = GetConnection(id);
    if (!client)
        return;
    DisconnectClient(*client);
}

void Server::InternalSend(const MessageBuffer& msg)
{
    for (const auto& c : clients_)
        c->Send(msg);
}

void Server::InternalSendTo(ServerConnection& client, const MessageBuffer& msg)
{
    client.Send(msg);
}

void Server::HandleMessage(ServerConnection& client, MessageBuffer& msg)
{
    if (handlers_.Exists(msg.type_))
        handlers_.Call(msg.type_, client, msg);
}

void Server::StartAccept()
{
    std::shared_ptr<ServerConnection> connection = std::make_shared<ServerConnection>(ioService_, *this);
    acceptor_.async_accept(connection->GetSocket(),
        std::bind(&Server::HandleAccept, this, connection, std::placeholders::_1));
}

void Server::HandleAccept(std::shared_ptr<ServerConnection> connection, const asio::error_code& error)
{
    if (!error)
    {
        bool accept = true;
        if (onAccept)
        {
            if (!onAccept(*connection))
                accept = false;
        }
        if (accept)
        {
            AddConnection(connection);
            connection->Start();
        }
    }
    StartAccept();
}

}
