/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#include <sa/Compiler.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#endif

#if !defined(ASIO_STANDALONE)
#define ASIO_STANDALONE
#endif

PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4592)
    PRAGMA_WARNING_DISABLE_CLANG("-Wpadded")
    PRAGMA_WARNING_DISABLE_GCC("-Wpadded")
#   include <asio.hpp>
PRAGMA_WARNING_POP

#include "Message.h"
#include "MessageBuffer.h"
#include <cstring>
#include <deque>
#include <functional>
#include <memory>
#include <mutex>
#include <sa/CallableTable.h>
#include <sa/IdGenerator.h>
#include <sa/StringHash.h>
#include <sa/TypeName.h>
#include <set>
#include <stdint.h>
#include <string>
#include <vector>
