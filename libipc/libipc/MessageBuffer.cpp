/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageBuffer.h"

namespace IPC {

bool MessageBuffer::DecodeHeader()
{
    bodyLength_ = static_cast<size_t>(data_[0] | data_[1] << 8);
    if (bodyLength_ > MaxBodyLength)
        return false;
    type_ = *reinterpret_cast<uint64_t*>(data_ + 2);
    return true;
}

void MessageBuffer::EncodeHeader()
{
    data_[0] = static_cast<uint8_t>(bodyLength_);
    data_[1] = static_cast<uint8_t>(bodyLength_ >> 8);

#ifdef _MSC_VER
    memcpy_s(data_ + 2, sizeof(uint64_t), &type_, sizeof(uint64_t));
#else
    memcpy(data_ + 2, &type_, sizeof(size_t));
#endif
}

void MessageBuffer::AddString(const std::string& value)
{
    uint16_t len = static_cast<uint16_t>(value.length());
    if (!CanWrite(static_cast<size_t>(len) + 2) || len > 8192)
        return;
    Add<uint16_t>(len);
    // Allows also \0
#ifdef _MSC_VER
    memcpy_s(data_ + GetBodyPos(), MaxBodyLength, value.data(), len);
#else
    memcpy(data_ + GetBodyPos(), value.data(), len);
#endif
    pos_ += static_cast<size_t>(len);
    bodyLength_ += static_cast<size_t>(len);
}

std::string MessageBuffer::GetString()
{
    size_t len = Get<uint16_t>();
    if (len >= (MaxBodyLength - pos_))
        return std::string();

    if (!CanRead(len))
        return std::string();

    char* v = reinterpret_cast<char*>(data_) + GetBodyPos();
    pos_ += len;
    return std::string(v, len);
}

}
