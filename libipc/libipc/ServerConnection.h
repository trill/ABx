/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MessageBuffer.h"
#include <asio.hpp>
#include <memory>
#include <sa/IdGenerator.h>
#include <sa/Noncopyable.h>
#include <mutex>

namespace IPC {

class Server;

class ServerConnection : public std::enable_shared_from_this<ServerConnection>
{
    NON_COPYABLE(ServerConnection)
private:
    static sa::IdGenerator<uint32_t> sIdGen;
    asio::ip::tcp::socket socket_;
    asio::io_service::strand strand_;
    Server& server_;
    uint32_t id_;
    MessageBuffer readBuffer_;
    BufferQueue writeBuffers_;
    void HandleRead(const asio::error_code& error);
    void HandleWrite(const asio::error_code& error, size_t bytesTransferred);
    void WriteImpl(const MessageBuffer& msg);
    void Write();
public:
    ServerConnection(asio::io_service& ioService, Server& server);
    ~ServerConnection() = default;
    asio::ip::tcp::socket& GetSocket() { return socket_; }

    void Start();
    void Close();
    // Send a message to this client
    void Send(const MessageBuffer& msg);
    // This ID can be used with the IPC::Server SendTo() function
    uint32_t GetId() const { return id_; }
    uint32_t GetIP() const;
    uint16_t GetPort() const;
};

}
