/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MessageChannel.h"
#include <libcommon/IpList.h>

class MessageServer
{
private:
    asio::io_service& ioService_;
    asio::ip::tcp::acceptor acceptor_;
    MessageChannel channel_;
    Net::IpList& whiteList_;
    void StartAccept();
    void HandleAccept(std::shared_ptr<MessageSession> session,
        const asio::error_code& error);
    bool IsIpAllowed(const asio::ip::tcp::endpoint& ep);
public:
    MessageServer(asio::io_service& ioService,
        const asio::ip::tcp::endpoint& endpoint,
        Net::IpList& whiteList);
    ~MessageServer() = default;
};
