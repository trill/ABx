/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageServer.h"
#include <libcommon/StringUtils.h>

MessageServer::MessageServer(asio::io_service& ioService,
    const asio::ip::tcp::endpoint& endpoint, Net::IpList& whiteList) :
    ioService_(ioService),
    acceptor_(ioService, endpoint),
    whiteList_(whiteList)
{
    StartAccept();
}

void MessageServer::StartAccept()
{
    std::shared_ptr<MessageSession> session = std::make_shared<MessageSession>(ioService_, channel_);
    acceptor_.async_accept(session->Socket(),
        std::bind(&MessageServer::HandleAccept, this, session, std::placeholders::_1));
}

void MessageServer::HandleAccept(std::shared_ptr<MessageSession> session,
    const asio::error_code& error)
{
    if (!error)
    {
        auto endp = session->Socket().remote_endpoint();
        if (IsIpAllowed(endp))
        {
            LOG_INFO << "Connection from " << endp.address() << ":" << endp.port() << std::endl;
            session->Start();
        }
    }
    StartAccept();
}

bool MessageServer::IsIpAllowed(const asio::ip::tcp::endpoint& ep)
{
    if (whiteList_.IsEmpty())
        return true;
    const uint32_t ip = ep.address().to_v4().to_uint();
    const bool result = whiteList_.Contains(ip);
    if (!result)
        LOG_WARNING << "Connection attempt from a not allowed IP " << Utils::ConvertIPToString(ip) << std::endl;
    return result;
}
