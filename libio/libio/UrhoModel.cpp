/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UrhoModel.h"
#include <istream>
#include <sstream>
#include <fstream>
#include <iomanip>

namespace IO::Urho {

enum VertexElementType
{
    TYPE_INT = 0,
    TYPE_FLOAT,
    TYPE_VECTOR2,
    TYPE_VECTOR3,
    TYPE_VECTOR4,
    TYPE_UBYTE4,
    TYPE_UBYTE4_NORM,
    MAX_VERTEX_ELEMENT_TYPES
};

enum VertexElementSemantic
{
    SEM_POSITION = 0,
    SEM_NORMAL,
    SEM_BINORMAL,
    SEM_TANGENT,
    SEM_TEXCOORD,
    SEM_COLOR,
    SEM_BLENDWEIGHTS,
    SEM_BLENDINDICES,
    SEM_OBJECTINDEX,
    MAX_VERTEX_ELEMENT_SEMANTICS
};

struct VertexElement
{
    /// Data type of element.
    VertexElementType type_;
    /// Semantic of element.
    VertexElementSemantic semantic_;
    /// Semantic index of element, for example multi-texcoords.
    unsigned char index_;
    /// Per-instance flag.
    bool perInstance_;
    /// Offset of element from vertex start. Filled by VertexBuffer once the vertex declaration is built.
    unsigned offset_;
};

const VertexElement LEGACY_VERTEXELEMENTS[] =
    {
        { TYPE_VECTOR3, SEM_POSITION, 0, false, 0 },     // Position
        { TYPE_VECTOR3, SEM_NORMAL, 0, false, 0 },       // Normal
        { TYPE_UBYTE4_NORM, SEM_COLOR, 0, false, 0 },    // Color
        { TYPE_VECTOR2, SEM_TEXCOORD, 0, false, 0 },     // Texcoord1
        { TYPE_VECTOR2, SEM_TEXCOORD, 1, false, 0 },     // Texcoord2
        { TYPE_VECTOR3, SEM_TEXCOORD, 0, false, 0 },     // Cubetexcoord1
        { TYPE_VECTOR3, SEM_TEXCOORD, 1, false, 0 },     // Cubetexcoord2
        { TYPE_VECTOR4, SEM_TANGENT, 0, false, 0 },      // Tangent
        { TYPE_VECTOR4, SEM_BLENDWEIGHTS, 0, false, 0 }, // Blendweights
        { TYPE_UBYTE4, SEM_BLENDINDICES, 0, false, 0 },  // Blendindices
        { TYPE_VECTOR4, SEM_TEXCOORD, 4, true, 0 },      // Instancematrix1
        { TYPE_VECTOR4, SEM_TEXCOORD, 5, true, 0 },      // Instancematrix2
        { TYPE_VECTOR4, SEM_TEXCOORD, 6, true, 0 },      // Instancematrix3
        { TYPE_INT, SEM_OBJECTINDEX, 0, false, 0 }       // Objectindex
    };

const unsigned ELEMENT_TYPESIZES[] =
    {
        sizeof(int),
        sizeof(float),
        2 * sizeof(float),
        3 * sizeof(float),
        4 * sizeof(float),
        sizeof(unsigned),
        sizeof(unsigned)
    };

static ea::vector<VertexElement> GetElements(unsigned mask)
{
    ea::vector<VertexElement> result;
    for (unsigned i = 0; i < 14; ++i)
    {
        if (mask & (1u << i))
            result.push_back(LEGACY_VERTEXELEMENTS[i]);
    }

    return result;
}

static unsigned GetVertexSize(const ea::vector<VertexElement>& elements)
{
    unsigned size = 0;

    for (unsigned i = 0; i < elements.size(); ++i)
        size += ELEMENT_TYPESIZES[elements[i].type_];

    return size;
}

bool LoadModel(const std::string& filename, Math::Mesh& shape)
{
    std::fstream input(filename, std::ios::binary | std::fstream::in);
    if (!input.is_open())
        return false;

    std::string fileId;
    fileId.resize(4);
    input.read(fileId.data(), 4);
    if (fileId != "UMDL" && fileId != "UMD2")
    {
        return false;
    }
    bool hasVertexDeclarations = (fileId == "UMD2");
    unsigned numVertexBuffers = 0;
    input.read((char*)&numVertexBuffers, sizeof(unsigned));

       // We support only 1 vertex buffer. In the Blender exporter uncheck
       // "One vertex buffer per object"
    if (numVertexBuffers != 1)
    {
        return false;
    }

    ea::vector<VertexElement> elements;
    unsigned vertexCount = 0;
    input.read((char*)&vertexCount, sizeof(unsigned));
    if (!hasVertexDeclarations)
    {
        unsigned elementMask = 0;
        input.read((char*)&elementMask, sizeof(unsigned));
        elements = GetElements(elementMask);
    }
    else
    {
        unsigned numElements = 0;
        input.read((char*)&numElements, sizeof(unsigned));
        for (unsigned j = 0; j < numElements; ++j)
        {
            unsigned elementDesc = 0;
            input.read((char*)&elementDesc, sizeof(unsigned));
            auto type = (VertexElementType)(elementDesc & 0xffu);
            auto semantic = (VertexElementSemantic)((elementDesc >> 8u) & 0xffu);
            auto index = (unsigned char)((elementDesc >> 16u) & 0xffu);
            elements.push_back({ type, semantic, index, false, 0 });
        }
    }

    unsigned morphRangeStart = 0;
    input.read((char*)&morphRangeStart, sizeof(unsigned));
    unsigned morphRangeCount = 0;
    input.read((char*)&morphRangeCount, sizeof(unsigned));

    {
        unsigned vertexSize = GetVertexSize(elements);

        size_t buffSize = (size_t)vertexCount * (size_t)vertexSize;
        auto* buff = new unsigned char[buffSize];

        input.read((char*)buff, buffSize);
        for (unsigned i = 0; i < vertexCount * vertexSize; i += vertexSize)
        {
            float* vertices = (float*)(buff + i);
            shape.vertexData_.push_back({ vertices[0], vertices[1], vertices[2] });
        }
        shape.vertexCount_ = vertexCount;

        delete[] buff;
    }

    unsigned numIndexBuffers = 0;
    input.read((char*)&numIndexBuffers, sizeof(unsigned));
    if (numIndexBuffers != 1)
    {
        return false;
    }

    unsigned indexCount = 0;
    input.read((char*)&indexCount, sizeof(unsigned));
    unsigned indexSize = 0;
    input.read((char*)&indexSize, sizeof(unsigned));

    {
        size_t buffSize = (size_t)indexCount * (size_t)indexSize;
        auto* buff = new unsigned char[buffSize];
        input.read((char*)buff, buffSize);

        shape.indexData_.reserve(indexCount);
        if (indexSize == sizeof(unsigned))
        {
            unsigned* indices = (unsigned*)buff;
            for (unsigned i = 0; i < indexCount; ++i)
            {
                shape.indexData_.push_back(indices[i]);
            }
        }
        else
        {
            unsigned short* indices = (unsigned short*)buff;
            for (unsigned i = 0; i < indexCount; ++i)
            {
                shape.indexData_.push_back(indices[i]);
            }
        }

        delete[] buff;
        shape.indexCount_ = indexCount;
    }

    return true;
}

}
