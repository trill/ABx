/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/Mesh.h>

namespace IO::Urho {

bool LoadModel(const std::string& filename, Math::Mesh& shape);

}
