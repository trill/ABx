/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Heightmap.h"

#include <fstream>
#include <iomanip>
#include <istream>
#include <sstream>
#include <json.h>

namespace IO {

ea::vector<float> LoadHeightmap(const std::string& name, Math::HeightmapHeader& header)
{
    std::fstream input(name, std::ios::binary | std::fstream::in);
    if (!input.is_open())
        return {};

    char sig[4];
    input.read(sig, 4);
    if (sig[0] != 'H' || sig[1] != 'M' || sig[2] != '\0' || sig[3] != '\0')
        return {};

    ea::vector<float> result;

    input.read((char*)&header.numVertices.x_, sizeof(int32_t));
    input.read((char*)&header.numVertices.y_, sizeof(int32_t));

    input.read((char*)&header.patchSize, sizeof(int32_t));

    input.read((char*)&header.patchWorldSize.x_, sizeof(float));
    input.read((char*)&header.patchWorldSize.y_, sizeof(float));

    input.read((char*)&header.numPatches.x_, sizeof(int32_t));
    input.read((char*)&header.numPatches.y_, sizeof(int32_t));

    input.read((char*)&header.patchWorldOrigin.x_, sizeof(float));
    input.read((char*)&header.patchWorldOrigin.y_, sizeof(float));

    input.read((char*)&header.minHeight, sizeof(float));
    input.read((char*)&header.maxHeight, sizeof(float));

    uint32_t heightsCount;
    input.read((char*)&heightsCount, sizeof(uint32_t));
    result.resize((size_t)heightsCount);
    input.read((char*)result.data(), sizeof(float) * (size_t)heightsCount);
    input.close();

    return result;
}

ea::vector<float> LoadHeightmapJSON(const std::string& name, Math::HeightmapHeader& header)
{
    std::ifstream ifs(name, std::ifstream::in);
    if (!ifs)
        return {};
    ifs.seekg(0, std::ios::end);
    size_t fileSize = static_cast<size_t>((long)ifs.tellg());
    std::string json;
    json.resize(fileSize);
    ifs.seekg(0, std::ios::beg);
    auto len = ifs.read(&json[0], static_cast<std::streamsize>(fileSize)).gcount();
    if (static_cast<size_t>(len) != fileSize)
        return {};

    const sa::json::JSON obj = sa::json::Parse(json);

    int64_t dataCount = 0;
    if (obj.HasKey("nDataValues"))
        dataCount = static_cast<sa::json::Integer>(obj["nDataValues"]);
    if (dataCount == 0)
        return {};
    if (!obj.HasKey("data"))
        return {};

    if (obj.HasKey("minHeight"))
        header.minHeight = static_cast<sa::json::Float>(obj["minHeight"]);
    if (obj.HasKey("maxHeight"))
        header.maxHeight = static_cast<sa::json::Float>(obj["maxHeight"]);

    if (obj.HasKey("numPatchesX"))
        header.numPatches.x_ = static_cast<sa::json::Integer>(obj["numPatchesX"]);
    if (obj.HasKey("numPatchesY"))
        header.numPatches.y_ = static_cast<sa::json::Integer>(obj["numPatchesY"]);

    if (obj.HasKey("numVerticesX"))
        header.numVertices.x_ = static_cast<sa::json::Integer>(obj["numVerticesX"]);
    if (obj.HasKey("numVerticesY"))
        header.numVertices.y_ = static_cast<sa::json::Integer>(obj["numVerticesY"]);

    if (obj.HasKey("patchSize"))
        header.patchSize = static_cast<sa::json::Integer>(obj["patchSize"]);

    if (obj.HasKey("patchWorldOriginX"))
        header.patchWorldOrigin.x_ = static_cast<sa::json::Float>(obj["patchWorldOriginX"]);
    if (obj.HasKey("patchWorldOriginY"))
        header.patchWorldOrigin.y_ = static_cast<sa::json::Float>(obj["patchWorldOriginY"]);

    if (obj.HasKey("patchWorldSizeX"))
        header.patchWorldSize.x_ = static_cast<sa::json::Float>(obj["patchWorldSizeX"]);
    if (obj.HasKey("patchWorldSizeY"))
        header.patchWorldSize.y_ = static_cast<sa::json::Float>(obj["patchWorldSizeY"]);

    ea::vector<float> data;
    data.resize(dataCount);
    const sa::json::ArrayType& dataArr = obj["data"];
    for (int64_t i = 0; i < dataCount; ++i)
    {
        if (static_cast<std::string>(dataArr.at(i)) != "-Infinity")
            data[i] = static_cast<sa::json::Float>(dataArr.at(i));
        else
            data[i] = -Math::M_INFINITE;
    }

    return data;
}

bool SaveHeightmap(const std::string& name,
    const ea::vector<float>& data,
    const Math::HeightmapHeader& header)
{
    std::fstream output(name, std::ios::binary | std::fstream::out);
    if (!output.is_open())
        return false;

    output.write((char*)"HM\0\0", 4);
    // Height Data
    output.write((char*)&header.numVertices.x_, sizeof(int32_t));
    output.write((char*)&header.numVertices.y_, sizeof(int32_t));

    output.write((char*)&header.patchSize, sizeof(int32_t));

    output.write((char*)&header.patchWorldSize.x_, sizeof(float));
    output.write((char*)&header.patchWorldSize.y_, sizeof(float));

    output.write((char*)&header.numPatches.x_, sizeof(int32_t));
    output.write((char*)&header.numPatches.y_, sizeof(int32_t));

    output.write((char*)&header.patchWorldOrigin.x_, sizeof(float));
    output.write((char*)&header.patchWorldOrigin.y_, sizeof(float));

    output.write((char*)&header.minHeight, sizeof(float));
    output.write((char*)&header.maxHeight, sizeof(float));

    uint32_t c = (uint32_t)data.size();
    output.write((char*)&c, sizeof(uint32_t));
    output.write((char*)data.data(), (size_t)c * sizeof(float));

    output.close();
    return true;
}

bool SaveHeightmapJSON(const std::string& name,
    const ea::vector<float>& data,
    const Math::IntVector2& size,
    const Math::HeightmapHeader& header)
{
    std::ofstream out(name);
    if (!out.is_open())
        return false;

    sa::json::JSON obj = sa::json::Object();
    obj["sizeX"] = size.x_;
    obj["sizeY"] = size.y_;
    obj["minHeight"] = header.minHeight;
    obj["maxHeight"] = header.maxHeight;
    obj["numVerticesX"] = header.numVertices.x_;
    obj["numVerticesY"] = header.numVertices.y_;
    obj["patchSize"] = header.patchSize;
    obj["patchWorldSizeX"] = header.patchWorldSize.x_;
    obj["patchWorldSizeY"] = header.patchWorldSize.y_;
    obj["numPatchesX"] = header.numPatches.x_;
    obj["numPatchesY"] = header.numPatches.y_;
    obj["patchWorldOriginX"] = header.patchWorldOrigin.x_;
    obj["patchWorldOriginY"] = header.patchWorldOrigin.y_;
    obj["nDataValues"] = data.size();
    obj["data"] = sa::json::Array();

    for (auto v : data)
        obj["data"].Append(v);

    out << obj << std::endl;
    return true;
}

bool SaveHeightmapText(const std::string& name,
    const ea::vector<float>& data,
    const Math::IntVector2& size,
    const Math::HeightmapHeader& header)
{
    std::fstream output(name, std::fstream::out);
    if (!output.is_open())
        return false;
    output << std::fixed << std::setprecision(6);
    output << "size: " << size.x_ << " " << size.y_ << std::endl;
    output << "minHeight: " << header.minHeight << std::endl;
    output << "maxHeight: " << header.maxHeight << std::endl;
    output << "numVertices: " << header.numVertices << std::endl;
    output << "patchSize: " << header.patchSize << std::endl;
    output << "patchWorldSize: " << header.patchWorldSize << std::endl;
    output << "numPatches: " << header.numPatches << std::endl;
    output << "patchWorldOrigin: " << header.patchWorldOrigin << std::endl;
    output << "nDataValues: " << data.size() << std::endl;

    for (auto v : data)
    {
        if (!Math::IsNegInfinite(v))
            output << v << std::endl;
        else
            output << "-inf" << std::endl;
    }
    return true;
}

void CreateHeightmapImage(const ea::vector<float>& heights, unsigned char* data, int comps, const Math::IntVector2& size,
    float minHeight, float maxHeight, bool flip)
{
    ASSERT(!heights.empty());
    ASSERT(data);
    const float zD = maxHeight - minHeight;
    for (int y = 0; y < size.y_; ++y)
    {
        for (int x = 0; x < size.x_; ++x)
        {
            const size_t index = (size_t)y * (size_t)size.x_ + (size_t)x;
            float value = heights[index];
            if (!Math::Equals(value, -Math::M_INFINITE))
            {
                unsigned char heightValue = static_cast<unsigned char>(((value - minHeight) / zD) * 255.0f);

                const size_t _index = flip ?
                    (size_t)y * (size_t)size.x_ + (size_t)x :
                    ((size_t)size.y_ - (size_t)y - 1) * (size_t)size.x_ + (size_t)x;
                data[_index * comps] = heightValue;
                if (comps > 1)
                    data[(_index * comps) + 1] = heightValue;
                if (comps > 2)
                    data[(_index * comps) + 2] = heightValue;
            }
        }
    }
}

}
