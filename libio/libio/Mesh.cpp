/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Mesh.h"

#include <sa/StringTempl.h>
#include <fstream>
#include <iomanip>
#include <istream>
#include <sstream>

namespace IO {

bool LoadMesh(const std::string& filename, Math::Mesh& shape, Math::BoundingBox& bb)
{
    std::fstream input(filename, std::ios::binary | std::fstream::in);
    if (!input.is_open())
        return false;

    char sig[4];
    input.read(sig, 4);
    if (sig[0] != 'M' || sig[1] != 'O' || sig[2] != 'D' || sig[3] != 'L')
        return false;

    // Read bounding box
    input.read((char*)bb.min_.Data(), sizeof(float) * 3);
    input.read((char*)bb.max_.Data(), sizeof(float) * 3);

    uint32_t vertexCount = 0;
    input.read((char*)&vertexCount, sizeof(uint32_t));
    shape.vertexCount_ = vertexCount;
    shape.vertexData_.resize(shape.vertexCount_);
    for (uint32_t i = 0; i < vertexCount; ++i)
        input.read((char*)shape.vertexData_[i].Data(), sizeof(float) * 3);

    uint32_t indexCount = 0;
    input.read((char*)&indexCount, sizeof(uint32_t));
    shape.indexCount_ = indexCount;
    shape.indexData_.reserve(shape.indexCount_);
    for (uint32_t i = 0; i < indexCount; ++i)
    {
        uint32_t index = 0;
        input.read((char*)&index, sizeof(uint32_t));
        shape.indexData_.push_back(index);
    }

    return true;
}

bool SaveMesh(const std::string& filename, const Math::Mesh& shape)
{
    std::fstream f(filename, std::fstream::out | std::fstream::binary);
    if (!f.is_open())
        return false;

    Math::BoundingBox bb;
    bb.Merge(&shape.vertexData_[0], shape.vertexData_.size());

    f.write("MODL", 4);
    f.write((char*)bb.min_.Data(), sizeof(float) * 3);
    f.write((char*)bb.max_.Data(), sizeof(float) * 3);

    f.write((char*)&shape.vertexCount_, sizeof(uint32_t));
    f.write((char*)shape.vertexData_.data(), sizeof(float) * 3 * shape.vertexCount_);

    f.write((char*)&shape.indexCount_, sizeof(uint32_t));
    for (size_t i = 0; i < shape.indexCount_; ++i)
        f.write((char*)&shape.indexData_[i], sizeof(uint32_t));
    return true;
}

bool SaveMeshToOBJ(const std::string& filename, const Math::Mesh& shape, std::optional<std::string> name)
{
    std::fstream f(filename, std::fstream::out);
    f << std::fixed << std::setprecision(6);
    if (!name.has_value())
        f << "o " << name.value() << std::endl;
    f << "# vertices " << shape.vertexCount_ << std::endl;
    for (const auto& v : shape.vertexData_)
    {
        f << "v " << v.x_ << " " << v.y_ << " " << v.z_ << std::endl;
    }
    f << "# triangles " << shape.indexCount_ / 3 << std::endl;
    for (size_t i = 0; i < shape.indexCount_; i += 3)
    {
        f << "f " << shape.indexData_[i] + 1 << " " << shape.indexData_[i + 1] + 1 << " " << shape.indexData_[i + 2] + 1
          << std::endl;
    }

    f << "# EOF" << std::endl;
    f.close();
    return true;
}

bool LoadMeshFromOBJ(const std::string& filename, Math::Mesh& mesh)
{
    return LoadMeshFromOBJ(
        filename,
        [&mesh](const Math::Vector3& vertex)
        {
            mesh.vertexData_.push_back(vertex);
            ++mesh.vertexCount_;
        },
        [&mesh](uint32_t i1, uint32_t i2, uint32_t i3) { mesh.AddTriangle(i1, i2, i3); });
}

bool LoadMeshFromOBJ(const std::string& filename,
    const std::function<void(const Math::Vector3&)>& onVertex,
    const std::function<void(uint32_t i1, uint32_t i2, uint32_t i3)>& onTriangle)
{
    std::ifstream in(filename);
    if (!in.is_open())
        return false;

    std::istringstream lineStream;
    char line[1024];
    std::string op;
    while (in.good())
    {
        in.getline(line, 1023);
        lineStream.clear();
        lineStream.str(line);

        if (!(lineStream >> op))
            continue;

        if (op == "v")
        {
            Math::Vector3 vertex;
            lineStream >> vertex.x_ >> vertex.y_ >> vertex.z_;
            onVertex(vertex);
        }
        else if (op == "vt")
        {
        }
        else if (op == "vn")
        {
        }
        else if (op == "g")
        {
        }
        else if (op == "f")
        {
            std::string face1;
            std::string face2;
            std::string face3;
            lineStream >> face1 >> face2 >> face3;
            unsigned i1 = 0, i2 = 0, i3 = 0;
            if (sa::Contains(face1, "/"))
            {
                auto indexparts = sa::Split(face1, "/", true, true);
                if (!indexparts.empty())
                {
                    auto n1 = sa::ToNumber<unsigned>(indexparts[0]);
                    if (n1.has_value())
                        i1 = n1.value();
                    else
                        continue;
                }
                else
                    continue;
            }
            if (sa::Contains(face2, "/"))
            {
                auto indexparts = sa::Split(face2, "/", true, true);
                if (!indexparts.empty())
                {
                    auto n2 = sa::ToNumber<unsigned>(indexparts[0]);
                    if (n2.has_value())
                        i2 = n2.value();
                    else
                        continue;
                }
                else
                    continue;
            }
            if (sa::Contains(face3, "/"))
            {
                auto indexparts = sa::Split(face3, "/", true, true);
                if (!indexparts.empty())
                {
                    auto n3 = sa::ToNumber<unsigned>(indexparts[0]);
                    if (n3.has_value())
                        i3 = n3.value();
                    else
                        continue;
                }
                else
                    continue;
            }
            else
            {
                auto n1 = sa::ToNumber<unsigned>(face1);
                if (n1.has_value())
                    i1 = n1.value();
                else
                    continue;
                auto n2 = sa::ToNumber<unsigned>(face2);
                if (n2.has_value())
                    i2 = n2.value();
                else
                    continue;
                auto n3 = sa::ToNumber<unsigned>(face3);
                if (n3.has_value())
                    i3 = n3.value();
                else
                    continue;
            }
            // Our indices are 0-based
            onTriangle(i1 - 1, i2 - 1, i3 - 1);
        }
    }

    return true;
}

bool LoadObstacles(const std::string& filename, Math::Mesh& mesh)
{
    unsigned offset = 0;
    return LoadObstacles(
        filename,
        [&mesh, &offset]() { offset = (unsigned)mesh.vertexCount_; },
        [&mesh](const Math::Vector3& vertex)
        {
            mesh.vertexData_.push_back(vertex);
            ++mesh.vertexCount_;
        },
        [&mesh, &offset](uint32_t i1, uint32_t i2, uint32_t i3) { mesh.AddTriangle(offset + i1, offset + i2, offset + i3); });
}

bool LoadObstacles(const std::string& filename,
    const std::function<void()>& onObstacle,
    const std::function<void(const Math::Vector3&)>& onVertex,
    const std::function<void(uint32_t i1, uint32_t i2, uint32_t i3)>& onTriangle)
{
    std::fstream input(filename, std::ios::binary | std::fstream::in);
    if (!input.is_open())
        return false;
    char sig[4];
    input.read(sig, 4);
    if (sig[0] != 'O' || sig[1] != 'B' || sig[2] != 'S' || sig[3] != '\0')
        return false;

    size_t obstacleCount = 0;
    input.read((char*)&obstacleCount, sizeof(uint64_t));
    if (obstacleCount > 10000)
        // Sanity check
        return false;
    for (size_t obstacle = 0; obstacle < obstacleCount; ++obstacle)
    {
        onObstacle();
        size_t vertexCount = 0;
        input.read((char*)&vertexCount, sizeof(uint64_t));
        for (size_t vertex = 0; vertex < vertexCount; ++vertex)
        {
            float x = 0.0f, y = 0.0f, z = 0.0f;
            input.read((char*)&x, sizeof(float));
            input.read((char*)&y, sizeof(float));
            input.read((char*)&z, sizeof(float));
            onVertex({ x, y, z });
        }

        size_t indexCount = 0;
        input.read((char*)&indexCount, sizeof(uint64_t));
        for (size_t index = 0; index < indexCount; index += 3)
        {
            uint32_t i1 = 0, i2 = 0, i3 = 0;
            input.read((char*)&i1, sizeof(uint32_t));
            input.read((char*)&i2, sizeof(uint32_t));
            input.read((char*)&i3, sizeof(uint32_t));
            onTriangle(i1, i2, i3);
        }
    }
    return true;
}

ObstaclesSaver::~ObstaclesSaver()
{
    Close();
}

bool ObstaclesSaver::Open(const std::string& filename)
{
    stream_ = std::make_unique<std::fstream>(filename, std::fstream::out | std::fstream::binary);
    if (stream_->is_open())
    {
        // Magic number
        stream_->write((char*)"OBS\0", 4);

        // Placeholder for count of meshes
        countPos_ = stream_->tellg();
        uint64_t count = 0;
        stream_->write((char*)&count, sizeof(uint64_t));
        return true;
    }
    return false;
}

void ObstaclesSaver::Close()
{
    if (stream_)
    {
        stream_->seekg(countPos_, std::ios::beg);
        stream_->write((char*)&count_, sizeof(uint64_t));
        stream_->close();
        stream_.reset();
    }
}

void ObstaclesSaver::Add(const Math::Mesh& mesh)
{
    ASSERT(stream_);

    size_t vertexCount = mesh.vertexCount_;
    stream_->write((char*)&vertexCount, sizeof(uint64_t));
    for (const auto& v : mesh.vertexData_)
    {
        stream_->write((char*)&v.x_, sizeof(float));
        stream_->write((char*)&v.y_, sizeof(float));
        stream_->write((char*)&v.z_, sizeof(float));
    }

    size_t indexCount = mesh.indexCount_;
    stream_->write((char*)&indexCount, sizeof(uint64_t));
    for (const auto& i : mesh.indexData_)
    {
        stream_->write((char*)&i, sizeof(uint32_t));
    }
    ++count_;
}

}
