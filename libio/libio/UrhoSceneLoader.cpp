/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UrhoSceneLoader.h"
#include <sa/StringHash.h>
#include <sa/StringTempl.h>

namespace IO::Urho {

static std::string GetComponentAttribute(const pugi::xml_node& node, const std::string& name)
{
    for (const auto& attr : node.children("attribute"))
    {
        const pugi::xml_attribute nameAttr = attr.attribute("name");
        const pugi::xml_attribute valueAttr = attr.attribute("value");
        if (name.compare(nameAttr.as_string()) == 0)
        {
            return valueAttr.as_string();
        }
    }
    return "";
}

SceneNode::SceneNode(const Math::Node* parent) :
    Node(parent)
{ }

SceneLoader::SceneLoader() = default;

bool SceneLoader::LoadNode(const pugi::xml_node& pugiNode, const Math::Node* parent)
{
    using namespace sa::literals;

    Math::Transformation nodeTransform;

    for (const auto& attr : pugiNode.children("attribute"))
    {
        const pugi::xml_attribute nameAttr = attr.attribute("name");
        const pugi::xml_attribute valueAttr = attr.attribute("value");
        const size_t nameHash = sa::StringHash(nameAttr.as_string());
        switch (nameHash)
        {
        case "Position"_Hash:
            nodeTransform.position_ += Math::Vector3(valueAttr.as_string());
            break;
        case "Rotation"_Hash:
            nodeTransform.orientation_ *= Math::Quaternion(valueAttr.as_string()).Normal();
            break;
        case "Scale"_Hash:
            nodeTransform.scale_ *= Math::Vector3(valueAttr.as_string());
            break;
        default:
            break;
        }
    }

    // TODO:
    ea::unique_ptr<SceneNode> nd = ea::make_unique<SceneNode>(parent);
    nd->transformation_ = nodeTransform;
    SceneNode* pNode = nd.get();
    nodes_.push_back(std::move(nd));

    for (const auto& comp : pugiNode.children("component"))
    {
        const pugi::xml_attribute type_attr = comp.attribute("type");
        const size_t type_hash = sa::StringHash(type_attr.as_string());
        switch (type_hash)
        {
        case "CollisionShape"_Hash:
            if (!LoadCollisionShape(comp, *pNode))
                return false;
            break;
        case "Terrain"_Hash:
            if (!LoadTerrain(comp, *pNode))
                return false;
            break;
        case "RigidBody"_Hash:
            if (!LoadRigidBody(comp, *pNode))
                return false;
            break;
        default:
            break;
        }
    }

    for (const auto& node : pugiNode.children("node"))
    {
        if (!LoadNode(node, pNode))
            return false;
    }

    return true;
}

bool SceneLoader::LoadCollisionShape(const pugi::xml_node& pugiNode, SceneNode& node)
{
    using namespace sa::literals;

    Math::Transformation offset;
    size_t collShape = "Box"_Hash;
    for (const auto& attr : pugiNode.children())
    {
        const pugi::xml_attribute& nameAttr = attr.attribute("name");
        const size_t nameHash = sa::StringHash(nameAttr.as_string());
        const pugi::xml_attribute& valueAttr = attr.attribute("value");
        const size_t valueHash = sa::StringHash(valueAttr.as_string());
        switch (nameHash)
        {
        case "Size"_Hash:
            node.collision_.offset.scale_ = Math::Vector3(valueAttr.as_string());
            break;
        case "Offset Position"_Hash:
            node.collision_.offset.position_ = Math::Vector3(valueAttr.as_string());
            break;
        case "Offset Rotation"_Hash:
            node.collision_.offset.orientation_ = Math::Quaternion(valueAttr.as_string()).Normal();
            break;
        case "Shape Type"_Hash:
            collShape = valueHash;
            break;
        default:
            break;
        }
    }

    if (collShape == "Box"_Hash && node.collision_.offset.scale_ != Math::Vector3_Zero)
    {
        node.collision_.shapeType = Math::ShapeType::BoundingBox;
    }
    else if ((collShape == "Sphere"_Hash || collShape == "Cylinder"_Hash) &&
        node.collision_.offset.scale_ != Math::Vector3_Zero)
    {
        node.collision_.shapeType = Math::ShapeType::Sphere;
    }
    else if (collShape == "TriangleMesh"_Hash || collShape == "ConvexHull"_Hash || collShape == "Capsule"_Hash)
    {
        if (collShape == "TriangleMesh"_Hash)
            node.collision_.shapeType = Math::ShapeType::TriangleMesh;
        else
            node.collision_.shapeType = Math::ShapeType::ConvexHull;

        std::string model = GetComponentAttribute(pugiNode, "Model");
        if (!model.empty())
        {
            // Model;Models/ResurrectionShrine1.mdl
            auto parts = sa::Split(model, ";", false, false);
            if (parts.size() != 2)
            {
                lastError_ = "Wrong format of attribute value " + model;
                return false;
            }
            node.collision_.model = parts[1];
        }
        else
        {
            lastError_ = "Missing Model attribute";
            return false;
        }
    }
    return true;
}

bool SceneLoader::LoadRigidBody(const pugi::xml_node& pugiNode, SceneNode& node)
{
    using namespace sa::literals;

    for (const auto& attr : pugiNode.children())
    {
        const pugi::xml_attribute& nameAttr = attr.attribute("name");
        const size_t nameHash = sa::StringHash(nameAttr.as_string());
        const pugi::xml_attribute& valueAttr = attr.attribute("value");
        switch (nameHash)
        {
        case "Collision Layer"_Hash:
            node.collision_.layer = valueAttr.as_uint();
            break;
        case "Collision Mask"_Hash:
            node.collision_.mask = valueAttr.as_uint();
            break;
        default:
            break;
        }
    }

    return true;
}

bool SceneLoader::LoadTerrain(const pugi::xml_node& pugiNode, SceneNode& node)
{
    using namespace sa::literals;

    for (const auto& attr : pugiNode.children())
    {
        const pugi::xml_attribute& nameAttr = attr.attribute("name");
        const size_t nameHash = sa::StringHash(nameAttr.as_string());
        const pugi::xml_attribute& valueAttr = attr.attribute("value");
        switch (nameHash)
        {
        case "Vertex Spacing"_Hash:
            node.heightmap_.spacing = Math::Vector3(valueAttr.as_string());
            break;
        case "Patch Size"_Hash:
            node.heightmap_.patchSize = valueAttr.as_int();
            break;
        case "Height Map"_Hash:
        {
            // value="Image;Textures/Rhodes_Heightfield.png"
            auto parts = sa::Split(valueAttr.as_string(), ";", false, false);
            if (parts.size() != 2)
            {
                lastError_ = "Wrong format of attribute value " + std::string(valueAttr.as_string());
                return false;
            }
            node.heightmap_.heightmapFile = parts[1];
            break;
        }
        default:
            break;
        }
    }
    return true;
}

bool SceneLoader::Load(const std::string& filename)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_file(filename.c_str());
    if (result.status != pugi::status_ok)
    {
        lastError_ = "Error loading file " + filename + ": " + result.description();;
        return false;
    }
    pugi::xml_node sceneNode = doc.child("scene");
    if (!sceneNode)
    {
        lastError_ = "File " + filename + " does not have a scene node";
        return false;
    }

    for (pugi::xml_node_iterator it = sceneNode.begin(); it != sceneNode.end(); ++it)
    {
        if (strcmp((*it).name(), "node") == 0)
        {
            if (!LoadNode(*it, nullptr))
            {
                lastError_ = "Error loading scene node";
                // Can't continue
                return false;
            }
        }
    }
    if (onObject_)
    {
        for (const auto& node : nodes_)
            onObject_(*node);
    }

    return true;
}

}
