/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libmath/Mesh.h>
#include <libmath/BoundingBox.h>
#include <libmath/Point.h>
#include <libmath/HeightMapHeader.h>

namespace IO {

// Load a heightmap (.hm) file
// \param[in] name Filename
// \param[out] header
// \return The height values
ea::vector<float> LoadHeightmap(const std::string& name, Math::HeightmapHeader& header);
ea::vector<float> LoadHeightmapJSON(const std::string& name, Math::HeightmapHeader& header);

bool SaveHeightmap(const std::string& name,
    const ea::vector<float>& data,
    const Math::HeightmapHeader& header);
bool SaveHeightmapJSON(const std::string& name,
    const ea::vector<float>& data,
    const Math::IntVector2& size,
    const Math::HeightmapHeader& header);
bool SaveHeightmapText(const std::string& name,
    const ea::vector<float>& data,
    const Math::IntVector2& size,
    const Math::HeightmapHeader& header);

void CreateHeightmapImage(const ea::vector<float>& heights, unsigned char* data,
    int comps, const Math::IntVector2& size,
    float minHeight, float maxHeight, bool flip = false);

}
