/**
 * Copyright 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libmath/BoundingBox.h>
#include <libmath/CollisionShape.h>
#include <libmath/Node.h>
#include <pugixml.hpp>
#include <eastl.hpp>
#include <string>
#include <vector>
#include <functional>

namespace IO::Urho {

class SceneNode : public Math::Node
{
public:
    enum class Occlude
    {
        Unset,
        Yes,
        No
    };
    struct Heightmap
    {
        Math::Vector3 spacing;
        int patchSize = 0;
        std::string heightmapFile;
    };
    struct Collision
    {
        Math::ShapeType shapeType = Math::ShapeType::None;
        uint32_t layer = 1;
        uint32_t mask = 0xFFFFFFFF;
        Math::Transformation offset;
        std::string model;
    };

    explicit SceneNode(const Math::Node* parent);
    std::string name_;
    Occlude occluder_ = Occlude::Unset;
    Occlude occludee_ = Occlude::Unset;
    Math::BoundingBox boundingBox_;
    // Bounding sphere radius
    float spereRadius_{ 0.0f };
    std::string modelFile_;
    Collision collision_;
    Heightmap heightmap_;
};

class SceneLoader
{
public:
    SceneLoader();

    std::function<void(const SceneNode& object)> onObject_;
    bool Load(const std::string& filename);

    [[nodiscard]] const std::string& GetLastError() const { return lastError_; }
private:
    bool LoadNode(const pugi::xml_node& pugiNode, const Math::Node* parent);
    bool LoadCollisionShape(const pugi::xml_node& pugiNode, SceneNode& node);
    bool LoadRigidBody(const pugi::xml_node& pugiNode, SceneNode& node);
    bool LoadTerrain(const pugi::xml_node& pugiNode, SceneNode& node);
    ea::vector<ea::unique_ptr<SceneNode>> nodes_;
    std::string lastError_;
};

}
