/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libmath/Mesh.h>
#include <libmath/BoundingBox.h>
#include <functional>
#include <memory>
#include <optional>
#include <sa/Noncopyable.h>

namespace IO {

bool LoadMesh(const std::string& filename, Math::Mesh& shape, Math::BoundingBox& bb);
bool SaveMesh(const std::string& filename, const Math::Mesh& shape);
bool SaveMeshToOBJ(const std::string& filename, const Math::Mesh& shape, std::optional<std::string> name = {});
bool LoadMeshFromOBJ(const std::string& filename, Math::Mesh& mesh);
bool LoadMeshFromOBJ(const std::string& filename,
    const std::function<void(const Math::Vector3&)>& onVertex,
    const std::function<void(uint32_t i1, uint32_t i2, uint32_t i3)>& onTriangle);
// Loads multpile obstacles into a single mesh
bool LoadObstacles(const std::string& filename, Math::Mesh& mesh);
bool LoadObstacles(const std::string& filename,
    const std::function<void()>& onObstacle,
    const std::function<void(const Math::Vector3&)>& onVertex,
    const std::function<void(uint32_t i1, uint32_t i2, uint32_t i3)>& onTriangle);

// Saves multiple meshes into a single .obstacles file.
// So .obstacles files are just intermediate files, which are combined with the heightmap
// into a single mesh, which is handed over to navigation mesh generation.
// The merging of heightmaps and obstacles is done by the `genavmesh` utility.
class ObstaclesSaver
{
    NON_COPYABLE(ObstaclesSaver)
    NON_MOVEABLE(ObstaclesSaver)
public:
    ObstaclesSaver() = default;
    ~ObstaclesSaver();
    bool Open(const std::string& filename);
    void Close();
    void Add(const Math::Mesh& mesh);
private:
    std::unique_ptr<std::fstream> stream_;
    size_t count_{ 0 };
    size_t countPos_{ 0 };
};

}
