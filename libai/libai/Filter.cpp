/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Filter.h"

namespace AI {

Filter::Filter(const ArgumentsType&)
{ }

Filter::~Filter() = default;

}
