/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Agent.h"
#include "TimedNode.h"

namespace AI {

TimedNode::TimedNode(const ArgumentsType& arguments) :
    Action(arguments)
{
    GetArgument<uint32_t>(arguments, 0, millis_);
}

TimedNode::~TimedNode() = default;

Node::Status TimedNode::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return ReturnStatus(agent, Status::CanNotExecute);

    uint32_t timer = NOT_STARTED;
    if (agent.context_.Has<TimerType>(id_))
        timer = agent.context_.Get<TimerType>(id_);

    if (timer == NOT_STARTED)
    {
        timer = millis_;
        Status status = ExecuteStart(agent, timeElapsed);
        if (status == Status::Finished)
            timer = NOT_STARTED;
        agent.context_.Set<TimerType>(id_, timer);
        return ReturnStatus(agent, status);
    }

    if (timer - timeElapsed > 0)
    {
        timer -= timeElapsed;
        Status status = ExecuteRunning(agent, timeElapsed);
        if (status == Status::Finished)
            timer = NOT_STARTED;
        agent.context_.Set<TimerType>(id_, timer);
        return ReturnStatus(agent, status);
    }

    agent.context_.Set<TimerType>(id_, timer);
    return ReturnStatus(agent, ExecuteExpired(agent, timeElapsed));
}

Node::Status TimedNode::ExecuteStart(Agent& agent, uint32_t)
{
    SetCurrentAction(agent);
    return Status::Running;
}

Node::Status TimedNode::ExecuteRunning(Agent&, uint32_t)
{
    return Status::Running;
}

Node::Status TimedNode::ExecuteExpired(Agent& agent, uint32_t)
{
    UnsetCurrentAction(agent);
    return Status::Finished;
}

}
