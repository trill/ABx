/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Agent.h"
#include "HasSelection.h"

namespace AI {
namespace Conditions {

bool HasSelection::Evaluate(Agent& agent, const Node&)
{
    return !agent.filteredAgents_.empty();
}

}

}
