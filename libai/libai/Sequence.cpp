/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Sequence.h"
#include "Agent.h"

namespace AI {

Sequence::Sequence(const ArgumentsType& arguments) :
    Composite(arguments)
{ }

Sequence::~Sequence() = default;

Node::Status Sequence::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return ReturnStatus(agent, Status::CanNotExecute);

    Nodes::iterator nIt = agent.context_.Has<Nodes::iterator>(id_) ? agent.context_.Get<Nodes::iterator>(id_) : children_.begin();

    while (nIt != children_.end())
    {
        // Call until one failed
        Status status = (*nIt)->Execute(agent, timeElapsed);
        if (status != Status::Finished)
            return ReturnStatus(agent, status);
        ++nIt;
        agent.context_.Set(id_, nIt);
    }
    agent.context_.Delete<Nodes::iterator>(id_);
    return ReturnStatus(agent, Status::Finished);
}

}
