/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Parallel.h"

namespace AI {

Parallel::Parallel(const ArgumentsType& arguments) :
    Composite(arguments)
{ }

Parallel::~Parallel() = default;

Node::Status Parallel::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return ReturnStatus(agent, Status::CanNotExecute);

    bool totalRunning = false;
    for (auto& child : children_)
    {
        const bool isRunning = child->Execute(agent, timeElapsed) == Status::Running;
        totalRunning |= isRunning;
    }
    return ReturnStatus(agent, totalRunning ? Status::Running : Status::Finished);
}

}
