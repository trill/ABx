/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Inverter.h"

namespace AI {

Inverter::Inverter(const ArgumentsType& arguments) :
    Decorator(arguments)
{ }

Node::Status Inverter::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Decorator::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return ReturnStatus(agent, Status::CanNotExecute);

    auto status = child_->Execute(agent, timeElapsed);
    switch (status)
    {
    case Status::CanNotExecute:
        return ReturnStatus(agent, Status::Finished);
    case Status::Finished:
        return ReturnStatus(agent, Status::Failed);
    case Status::Failed:
        return ReturnStatus(agent, Status::Finished);
    default:
        return ReturnStatus(agent, Status::Running);
    }
}

}
