/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"

namespace AI {

// A node with exactly one child
class Decorator : public Node
{
protected:
    std::shared_ptr<Node> child_;
    explicit Decorator(const ArgumentsType& arguments);
public:
    bool AddNode(std::shared_ptr<Node> node) override;
    void VisitChildren(const std::function<Iteration(const Node&)>&) const override;
};

}
