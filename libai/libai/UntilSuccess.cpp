/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "UntilSuccess.h"

namespace AI {

Node::Status UntilSuccess::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Node::Status::CanNotExecute)
        return ReturnStatus(agent, Node::Status::CanNotExecute);
    if (!child_)
        return ReturnStatus(agent, Node::Status::CanNotExecute);
    auto status = child_->Execute(agent, timeElapsed);
    if (status == Status::Finished)
        return ReturnStatus(agent, Status::Finished);
    return ReturnStatus(agent, Status::Running);
}

}
