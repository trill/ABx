/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LogicConditions.h"
#include "Node.h"
#include <sstream>
#include <sa/StringTempl.h>

namespace AI {
namespace Conditions {

FalseCondition::FalseCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{ }

bool FalseCondition::Evaluate(Agent&, const Node&)
{
    return false;
}

std::string FalseCondition::GetFriendlyName() const
{
    return "False";
}

TrueCondition::TrueCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{ }

bool TrueCondition::Evaluate(Agent&, const Node&)
{
    return true;
}

std::string TrueCondition::GetFriendlyName() const
{
    return "True";
}

AndCondition::AndCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{ }

bool AndCondition::AddCondition(std::shared_ptr<Condition> condition)
{
    conditions_.push_back(std::move(condition));
    return true;
}

std::string AndCondition::GetFriendlyName() const
{
    std::vector<std::string> conds;
    VisitConditions([&](const Condition& current)
    {
        conds.push_back(current.GetFriendlyName());
        return Iteration::Continue;
    });
    return sa::CombineString(conds, std::string(" And "));
}

bool AndCondition::Evaluate(Agent& agent, const Node& node)
{
    for (auto& condition : conditions_)
    {
        if (!condition->Evaluate(agent, node))
            return false;
    }
    return true;
}

void AndCondition::VisitConditions(const std::function<Iteration(const Condition&)>& callback) const
{
    for (const auto& c : conditions_)
        if (callback(*c) == Iteration::Break)
            break;
}

OrCondition::OrCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{ }

bool OrCondition::AddCondition(std::shared_ptr<Condition> condition)
{
    conditions_.push_back(std::move(condition));
    return true;
}

bool OrCondition::Evaluate(Agent& agent, const Node& node)
{
    for (auto& condition : conditions_)
    {
        if (condition->Evaluate(agent, node))
            return true;
    }
    return false;
}

std::string OrCondition::GetFriendlyName() const
{
    std::vector<std::string> conds;
    VisitConditions([&](const Condition& current)
    {
        conds.push_back(current.GetFriendlyName());
        return Iteration::Continue;
    });
    return sa::CombineString(conds, std::string(" Or "));
}

void OrCondition::VisitConditions(const std::function<Iteration(const Condition&)>& callback) const
{
    for (const auto& c : conditions_)
        if (callback(*c) == Iteration::Break)
            break;
}

NotCondition::NotCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{ }

bool NotCondition::AddCondition(std::shared_ptr<Condition> condition)
{
    if (!condition)
        return false;
    condition_ = std::move(condition);
    return true;
}

bool NotCondition::Evaluate(Agent& agent, const Node& node)
{
    // A Not condition must have a condition assigned.
    ASSERT(condition_);
    return !condition_->Evaluate(agent, node);
}

std::string NotCondition::GetFriendlyName() const
{
    if (!condition_)
        return "";
    return "Not " + condition_->GetFriendlyName();
}

void NotCondition::VisitConditions(const std::function<Iteration(const Condition&)>& callback) const
{
    if (condition_)
        callback(*condition_);
}

}
}
