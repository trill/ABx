/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"
#include <sa/Compiler.h>

namespace AI {

// Do something useful, e.g. attacking mean foes. AKA Leaf.
// There is no factory for this because it's meant to be subclassed.
class Action : public Node, public std::enable_shared_from_this<Action>
{
protected:
    // If true, this Action is executed until it returns not running without
    // reevaluating the tree.
    bool mustComplete_{ false };
    bool IsCurrentAction(const Agent& agent) const;
    void SetCurrentAction(Agent& agent);
    void UnsetCurrentAction(Agent& agent);
    virtual Status DoAction(Agent& agent, uint32_t timeElapsed) = 0;
    explicit Action(const ArgumentsType& arguments);
public:
    ~Action() override;

    bool MustComplete() const { return mustComplete_; }
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
