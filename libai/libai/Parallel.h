/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Composite.h"

namespace AI {

// Executes all children no matter what they return
class Parallel final : public Composite
{
    NODE_CLASS(Parallel)
public:
    explicit Parallel(const ArgumentsType& arguments);
    ~Parallel() override;
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
