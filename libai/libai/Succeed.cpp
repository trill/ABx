/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Succeed.h"

namespace AI {

Succeed::Succeed(const ArgumentsType& arguments) :
    Decorator(arguments)
{ }

Node::Status Succeed::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return ReturnStatus(agent, Status::CanNotExecute);

    auto status = child_->Execute(agent, timeElapsed);
    if (status == Status::Running)
        return ReturnStatus(agent, Status::Running);

    return ReturnStatus(agent, Status::Finished);
}

}
