/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Filter.h"

namespace AI {
namespace Filters {

// Selects all agents in the Zone
class ZoneFilter final : public Filter
{
    FILTER_CLASS(ZoneFilter)
public:
    explicit ZoneFilter(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
