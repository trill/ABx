/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FirstFilter.h"

namespace AI {
namespace Filters {

FirstFilter::FirstFilter(const ArgumentsType& arguments) :
    Filter(arguments)
{ }

void FirstFilter::Execute(Agent& agent)
{
    auto& entities = GetFiltered(agent);
    if (entities.empty())
        return;

    auto value = entities.front();
    entities.clear();
    entities.push_back(value);
}

}
}
