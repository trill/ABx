/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Decorator.h"

namespace AI {

// A repeater will reprocess its child node each time its child returns a result.
// These are often used at the very base of the tree, to make the tree to run
// continuously.
class Repeater final : public Decorator
{
    NODE_CLASS(Repeater)
public:
    explicit Repeater(const ArgumentsType& arguments) :
        Decorator(arguments)
    { }
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
