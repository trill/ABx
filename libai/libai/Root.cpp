/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Root.h"

namespace AI {

Root::Root() :
    Decorator(ArgumentsType{})
{ }

Node::Status Root::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (!child_)
        return ReturnStatus(agent, Status::CanNotExecute);
    return ReturnStatus(agent, child_->Execute(agent, timeElapsed));
}

}
