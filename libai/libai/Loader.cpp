/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Loader.h"
#include "Registry.h"

namespace AI {

Loader::Loader(Registry& reg) :
    registry_(reg)
{ }

Loader::~Loader() = default;

}
