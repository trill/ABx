/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <string>
#include <CleanupNs.h>
#include <sa/Noncopyable.h>
#include <sa/Compiler.h>

namespace AI {

class Root;
class BevaviorCache;
class Registry;

class Loader
{
    NON_COPYABLE(Loader)
protected:
    Registry& registry_;
    explicit Loader(Registry& reg);
public:
    virtual ~Loader();
    virtual std::shared_ptr<Root> LoadFile(const std::string& fileName) = 0;
    virtual std::shared_ptr<Root> LoadString(const std::string& value) = 0;
    // Initialize the cache from an init script
    virtual bool InitChache(const std::string& initScript, BevaviorCache& cache) = 0;
};

}
