/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Decorator.h"

namespace AI {

// Decorator that returns either running or finished
class Succeed final : public Decorator
{
    NODE_CLASS(Succeed)
public:
    explicit Succeed(const ArgumentsType& arguments);
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
