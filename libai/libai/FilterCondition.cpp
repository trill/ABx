/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "stdafx.h"
#include "FilterCondition.h"
#include "Filter.h"

namespace AI {
namespace Conditions {

FilterCondition::FilterCondition(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<uint32_t>(arguments, 0, min_);
}

bool FilterCondition::Evaluate(Agent& agent, const Node&)
{
    // If there is a filter assigned execute it first, otherwise just see if the
    // Agent has something selected.
    if (filter_)
        filter_->Execute(agent);
    return agent.filteredAgents_.size() >= min_;
}

bool FilterCondition::SetFilter(std::shared_ptr<AI::Filter> filter)
{
    if (!filter)
        return false;
    filter_ = std::move(filter);
    return true;
}

const Filter* FilterCondition::GetFilter() const
{
    if (filter_)
        return filter_.get();
    return nullptr;
}

std::string FilterCondition::GetFriendlyName() const
{
    if (!filter_)
        return "";
    return "Filter(" + std::string(filter_->GetClassName()) + ")";
}

}
}
