/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Node.h"
#include "Agent.h"
#include "Condition.h"

namespace AI {

sa::IdGenerator<Id> Node::sIDs;

Node::Node(const ArgumentsType&) :
    id_(sIDs.Next())
{ }

Node::~Node() = default;

bool Node::AddNode(std::shared_ptr<Node>)
{
    return false;
}

void Node::SetCondition(std::shared_ptr<Condition> condition)
{
    condition_ = std::move(condition);
}

Node::Status Node::Execute(Agent& agent, uint32_t)
{
    if (condition_ && !condition_->Evaluate(agent, *this))
        return ReturnStatus(agent, Status::CanNotExecute);
    return ReturnStatus(agent, Status::Finished);
}

Node::Status Node::ReturnStatus(Agent& agent, Node::Status value)
{
    agent.context_.Set<NodeStatusType>(id_, value);
    return value;
}

void ForEachChildNode(const Node& parent, const std::function<Iteration(const Node& parent, const Node& child)>& callback)
{
    parent.VisitChildren([&](const Node& node)
    {
        callback(parent, node);
        ForEachChildNode(node, callback);
        return Iteration::Continue;
    });
}

}
