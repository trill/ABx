/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <limits>
#include <vector>
#include <string>
#include <optional>

#define ABAI_STRINGIFY_INTERNAL(x) #x
#define ABAI_STRINGIFY(x) ABAI_STRINGIFY_INTERNAL(x)

namespace AI {

typedef uint32_t Id;
static constexpr Id INVALID_ID = std::numeric_limits<Id>::max();
using ArgumentsType = std::vector<std::string>;

template <typename T, std::enable_if_t<std::is_integral<T>::value || std::is_enum<T>::value, int> = 0>
inline bool GetArgument(const ArgumentsType& arguments, size_t index, T& result)
{
    if (index < arguments.size())
    {
        result = static_cast<T>(atoi(arguments[index].c_str()));
        return true;
    }
    return false;
}

template <typename T, std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
inline bool GetArgument(const ArgumentsType& arguments, size_t index, T& result)
{
    if (index < arguments.size())
    {
        result = static_cast<T>(atof(arguments[index].c_str()));
        return true;
    }
    return false;
}

template <typename T, std::enable_if_t<!std::is_integral<T>::value && !std::is_enum<T>::value && !std::is_floating_point<T>::value, int> = 0>
inline bool GetArgument([[maybe_unused]] const ArgumentsType& arguments, [[maybe_unused]] size_t index, [[maybe_unused]] T& result)
{
    return false;
}

template<>
inline bool GetArgument<std::string>(const ArgumentsType& arguments, size_t index, std::string& result)
{
    if (index < arguments.size())
    {
        result = arguments.at(index);
        return true;
    }
    return false;
}

template<>
inline bool GetArgument<bool>(const ArgumentsType& arguments, size_t index, bool& result)
{
    if (index < arguments.size())
    {
        result = (arguments.at(index).compare("true") == 0 ||
            atoi(arguments[index].c_str()) != 0);
        return true;
    }
    return false;
}

}
