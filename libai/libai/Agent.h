/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Action.h"
#include "AiDefines.h"
#include "Context.h"
#include "Node.h"
#include <memory>
#include <sa/StrongType.h>
#include <stdint.h>
#include <vector>

namespace AI {

class Root;
class Zone;

// To create distinct types
using LimitType = sa::StrongType<size_t, struct LimitTag>;
using TimerType = sa::StrongType<uint32_t, struct TimerTag>;
using CounterType = sa::StrongType<uint32_t, struct CounterTag>;
using IdType = sa::StrongType<Id, struct IdTag>;
using NodeStatusType = sa::StrongType<Node::Status, struct NodeStatusTag>;

using AgentIds = std::vector<Id>;
// Once the BT is loaded it must not be modified, so the iterators are not invalidated.
// This is a bit unflexible when an application needs to store other types, but
// you could create a second context in the subclass.
class AgentContext : public Context<IdType, NodeStatusType, LimitType, TimerType, CounterType, Nodes::iterator>
{
public:
    std::weak_ptr<Action> currentAction_;
    bool IsActionRunning(Id id) const
    {
        if (auto ca = currentAction_.lock())
            return ca->GetId() == id;
        return false;
    }
};

class Agent
{
private:
    /// Game provided ID, not managed by the library, unlike the Node ID.
    Id id_;
    Zone* zone_{ nullptr };
protected:
    Node::Status currentStatus_{ Node::Status::Unknown };
    std::shared_ptr<Root> root_;
public:
    explicit Agent(Id id);
    virtual ~Agent();

    void Update(uint32_t timeElapsed);

    void SetBehavior(std::shared_ptr<Root> node);
    std::shared_ptr<Root> GetBehavior() const;
    Id GetId() const { return id_; }
    Zone* GetZone() const;
    void SetZone(Zone* zone);
    Node::Status GetCurrentStatus() const { return currentStatus_; }

    bool pause_{ false };
    // Selected Agent IDs
    AgentIds filteredAgents_;
    AgentContext context_;
};

}
