/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"

namespace AI {

// A node with one or more children
class Composite : public Node
{
protected:
    Nodes children_;
    explicit Composite(const ArgumentsType& arguments);
public:
    bool AddNode(std::shared_ptr<Node> node) override;
    void VisitChildren(const std::function<Iteration(const Node&)>&) const override;
};

}
