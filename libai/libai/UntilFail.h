/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Decorator.h"

namespace AI {

// Run the child until it fails. Then it retutrns Finished.
class UntilFail final : public Decorator
{
    NODE_CLASS(UntilFail)
public:
    explicit UntilFail(const ArgumentsType& arguments) :
        Decorator(arguments)
    { }
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
