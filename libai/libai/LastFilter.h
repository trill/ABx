/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Filter.h"

namespace AI {
namespace Filters {

class LastFilter final : public Filter
{
    FILTER_CLASS(LastFilter)
public:
    explicit LastFilter(const ArgumentsType& arguments);
    void Execute(Agent& agent) override;
};

}
}
