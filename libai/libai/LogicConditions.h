/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Condition.h"

namespace AI {
namespace Conditions {

class FalseCondition final : public Condition
{
    CONDITON_CLASS(FalseCondition)
public:
    explicit FalseCondition(const ArgumentsType& arguments);
    bool Evaluate(Agent&, const Node&) override;
    std::string GetFriendlyName() const override;
};

class TrueCondition final : public Condition
{
    CONDITON_CLASS(TrueCondition)
public:
    explicit TrueCondition(const ArgumentsType& arguments);
    bool Evaluate(Agent&, const Node&) override;
    std::string GetFriendlyName() const override;
};

class AndCondition final : public Condition
{
    CONDITON_CLASS(AndCondition)
private:
    std::vector<std::shared_ptr<Condition>> conditions_;
public:
    explicit AndCondition(const ArgumentsType& arguments);
    bool AddCondition(std::shared_ptr<Condition> condition) override;
    void VisitConditions(const std::function<Iteration(const Condition&)>& callback) const override;
    std::string GetFriendlyName() const override;

    bool Evaluate(Agent&, const Node&) override;
};

class OrCondition final : public Condition
{
    CONDITON_CLASS(OrCondition)
private:
    std::vector<std::shared_ptr<Condition>> conditions_;
public:
    explicit OrCondition(const ArgumentsType& arguments);
    bool AddCondition(std::shared_ptr<Condition> condition) override;
    void VisitConditions(const std::function<Iteration(const Condition&)>& callback) const override;
    std::string GetFriendlyName() const override;

    bool Evaluate(Agent&, const Node&) override;
};

class NotCondition final : public Condition
{
    CONDITON_CLASS(NotCondition)
private:
    std::shared_ptr<Condition> condition_;
public:
    explicit NotCondition(const ArgumentsType& arguments);
    bool AddCondition(std::shared_ptr<Condition> condition) override;
    void VisitConditions(const std::function<Iteration(const Condition&)>& callback) const override;
    std::string GetFriendlyName() const override;

    bool Evaluate(Agent&, const Node&) override;
};

}
}
