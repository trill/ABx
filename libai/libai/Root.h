/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Decorator.h"

namespace AI {

class Root : public Decorator
{
public:
    Root();
    const char* GetClassName() const override { return "Root"; }
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}
