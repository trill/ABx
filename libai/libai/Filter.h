/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Agent.h"
#include "Factory.h"
#include <CleanupNs.h>
#include <sa/Compiler.h>

namespace AI {

class Filter;

using FilterFactory = AbstractFactory<Filter>;

#define FILTER_CLASS(FilterName)                                                      \
public:                                                                               \
    class Factory final : public FilterFactory                                        \
    {                                                                                 \
    public:                                                                           \
        std::shared_ptr<Filter> Create(const ArgumentsType& arguments) const override \
        {                                                                             \
            std::shared_ptr<Filter> res = std::make_shared<FilterName>(arguments);    \
            return res;                                                               \
        }                                                                             \
    };                                                                                \
    static const Factory& GetFactory()                                                \
    {                                                                                 \
        static Factory sFactory;                                                      \
        return sFactory;                                                              \
    }                                                                                 \
private:                                                                              \
    FilterName(const FilterName&) = delete;                                           \
    FilterName& operator=(const FilterName&) = delete;                                \
    FilterName(FilterName&&) = delete;                                                \
    FilterName& operator=(FilterName&&) = delete;                                     \
    const char* GetClassName() const override { return ABAI_STRINGIFY(FilterName); }

class Filter
{
protected:
    std::string name_;
    explicit Filter(const ArgumentsType& arguments);
    AgentIds& GetFiltered(Agent& agent)
    {
        return agent.filteredAgents_;
    }
public:
    virtual ~Filter();

    virtual const char* GetClassName() const = 0;
    const std::string& GetName() const { return name_; }
    void SetName(const std::string& value) { name_ = value; }

    virtual void Execute(Agent&) = 0;
};

}
