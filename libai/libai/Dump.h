/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <iostream>
#include <CleanupNs.h>

namespace AI {

class Node;
class BevaviorCache;

void DumpTree(std::ostream& stream, const Node& node);
void DumpCache(std::ostream& stream, const BevaviorCache& cache);

}
