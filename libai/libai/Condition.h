/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <vector>
#include <memory>
#include <functional>
#include <sa/Iteration.h>
#include "Factory.h"
#include <CleanupNs.h>
#include <sa/Compiler.h>

namespace AI {

class Agent;
class Condition;
class Filter;
class Node;

using ConditionFactory = AbstractFactory<Condition>;

#define CONDITON_CLASS(ConditionName)                                                    \
public:                                                                                  \
    class Factory final: public ConditionFactory                                         \
    {                                                                                    \
    public:                                                                              \
        std::shared_ptr<Condition> Create(const ArgumentsType& arguments) const override \
        {                                                                                \
            std::shared_ptr<Condition> res = std::make_shared<ConditionName>(arguments); \
            return res;                                                                  \
        }                                                                                \
    };                                                                                   \
    static const Factory& GetFactory()                                                   \
    {                                                                                    \
        static Factory sFactory;                                                         \
        return sFactory;                                                                 \
    }                                                                                    \
private:                                                                                 \
    ConditionName(const ConditionName&) = delete;                                        \
    ConditionName& operator=(const ConditionName&) = delete;                             \
    ConditionName(ConditionName&&) = delete;                                             \
    ConditionName& operator=(ConditionName&&) = delete;                                  \
    const char* GetClassName() const override { return ABAI_STRINGIFY(ConditionName); }

class Condition
{
protected:
    std::string name_;
    explicit Condition(const ArgumentsType& arguments);
public:
    virtual ~Condition();

    virtual const char* GetClassName() const = 0;
    const std::string& GetName() const { return name_; }
    void SetName(const std::string& value) { name_ = value; }
    virtual std::string GetFriendlyName() const { return GetClassName(); }

    virtual bool AddCondition(std::shared_ptr<Condition>);
    // Visit first level conditions (not a whole condition tree)
    virtual void VisitConditions(const std::function<Iteration(const Condition&)>&) const { }
    virtual bool SetFilter(std::shared_ptr<Filter>);
    virtual const Filter* GetFilter() const { return nullptr; }
    /// Evaluate the condition
    /// @param agent
    /// @param node The calling node
    virtual bool Evaluate(Agent& agent, const Node& node) = 0;
};

}
