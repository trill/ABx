/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Action.h"
#include <limits>
#include <stdint.h>

namespace AI {

inline constexpr uint32_t NOT_STARTED = std::numeric_limits<uint32_t>::max();

class TimedNode : public Action
{
protected:
    uint32_t millis_{ 0 };
protected:
    // Called by Action, but we don't use it
    Status DoAction(Agent&, uint32_t) override { return Status::CanNotExecute; }
public:
    explicit TimedNode(const ArgumentsType& arguments);
    ~TimedNode() override;
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) final override;
    virtual Node::Status ExecuteStart(Agent& agent, uint32_t timeElapsed);
    virtual Node::Status ExecuteRunning(Agent& agent, uint32_t timeElapsed);
    virtual Node::Status ExecuteExpired(Agent& agent, uint32_t timeElapsed);
};

}
