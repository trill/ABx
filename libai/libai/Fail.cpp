/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Fail.h"

namespace AI {

Fail::Fail(const ArgumentsType& arguments) :
    Decorator(arguments)
{ }

Node::Status Fail::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Node::Status::CanNotExecute)
        return ReturnStatus(agent, Node::Status::CanNotExecute);

    auto status = child_->Execute(agent, timeElapsed);
    if (status == Node::Status::Running)
        return ReturnStatus(agent, Node::Status::Running);

    return ReturnStatus(agent, Node::Status::Failed);
}

}
