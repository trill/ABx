/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Repeater.h"

namespace AI {

Node::Status AI::Repeater::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Node::Status::CanNotExecute)
        return ReturnStatus(agent, Node::Status::CanNotExecute);
    if (!child_)
        return ReturnStatus(agent, Node::Status::CanNotExecute);
    child_->Execute(agent, timeElapsed);
    return ReturnStatus(agent, Status::Running);
}

}
