/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Limit.h"
#include "Agent.h"

namespace AI {

Limit::Limit(const ArgumentsType& arguments) :
    Decorator(arguments)
{
    GetArgument<uint32_t>(arguments, 0, limit_);
}

Node::Status Limit::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Node::Status::CanNotExecute)
        return ReturnStatus(agent, Node::Status::CanNotExecute);

    size_t executions = 0;
    if (agent.context_.Has<LimitType>(id_))
        executions = agent.context_.Get<LimitType>(id_);

    if (executions >= limit_)
        return ReturnStatus(agent, Node::Status::Finished);

    auto status = child_->Execute(agent, timeElapsed);

    agent.context_.Set<LimitType>(id_, executions + 1);
    if (status == Node::Status::Running)
        return ReturnStatus(agent, Node::Status::Running);

    return ReturnStatus(agent, Node::Status::Failed);
}

}
