/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Priority.h"

namespace AI {

Priority::Priority(const ArgumentsType& arguments) :
    Composite(arguments)
{ }

Priority::~Priority() = default;

Node::Status Priority::Execute(Agent& agent, uint32_t timeElapsed)
{
    if (Node::Execute(agent, timeElapsed) == Status::CanNotExecute)
        return Status::CanNotExecute;

    for (auto& child : children_)
    {
        Status status = child->Execute(agent, timeElapsed);
        if (status != Status::Failed && status != Status::CanNotExecute)
            return ReturnStatus(agent, status);
        // If failed try the next
    }
    return ReturnStatus(agent, Status::Failed);
}

}
