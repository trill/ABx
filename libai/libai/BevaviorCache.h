/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <memory>
#include <functional>
#include <sa/Iteration.h>

namespace AI {

class Root;

class BevaviorCache
{
private:
    std::map<std::string, std::shared_ptr<Root>> cache_;
public:
    BevaviorCache() = default;
    ~BevaviorCache() = default;

    std::shared_ptr<Root> Get(const std::string& name) const;
    void Add(std::shared_ptr<Root> node);
    void Remove(std::shared_ptr<Root> node);
    void VisitBehaviors(const std::function<Iteration(const std::string& name, const Root& root)>& callback) const;
};

}
