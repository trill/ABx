/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LastFilter.h"

namespace AI {
namespace Filters {

LastFilter::LastFilter(const ArgumentsType& arguments) :
    Filter(arguments)
{ }

void LastFilter::Execute(Agent& agent)
{
    auto& entities = GetFiltered(agent);
    if (entities.empty())
        return;

    auto value = entities.back();
    entities.clear();
    entities.push_back(value);
}

}
}
