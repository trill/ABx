/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Decorator.h"

namespace AI {

class UntilSuccess final : public Decorator
{
    NODE_CLASS(UntilSuccess)
public:
    explicit UntilSuccess(const ArgumentsType& arguments) :
        Decorator(arguments)
    { }
    Node::Status Execute(Agent& agent, uint32_t timeElapsed) override;
};

}

