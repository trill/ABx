project (libshared CXX)

file(GLOB SOURCES libshared/*.cpp libshared/*.h)

add_library(
    libshared
    ${SOURCES}
)

target_compile_options(libshared PRIVATE -fno-exceptions -fno-rtti)
set_property(TARGET libshared PROPERTY POSITION_INDEPENDENT_CODE ON)
target_precompile_headers(libshared PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/libshared/stdafx.h)
target_include_directories(libshared PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
