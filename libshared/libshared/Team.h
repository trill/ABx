/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Game {

enum class TeamColor
{
    Default = 0,
    Red,
    Blue,
    Yellow
};

}
