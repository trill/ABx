/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Skill.h>
#include <AB/Entities/Profession.h>

namespace Game {

// Check if this skill is available for the given combination of professions.
bool SkillProfessionMatches(const std::string& professionUuid, const AB::Entities::Profession& prof1,
    const AB::Entities::Profession* prof2);

}
