/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SkillsHelper.h"

namespace Game {

bool SkillProfessionMatches(const std::string& professionUuid, const AB::Entities::Profession& prof1,
    const AB::Entities::Profession* prof2)
{
    if (professionUuid == AB::Entities::PROFESSION_NONE_UUID)
        // Available for all professions
        return true;
    if (professionUuid == prof1.uuid)
        return true;
    if (prof2 && prof2->index != 0)
    {
        if (professionUuid == prof2->uuid)
            return true;
    }
    return false;
}

}
