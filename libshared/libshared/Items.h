/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Game {

enum class ItemStatIndex : size_t
{
    None = 0,
    MinDamage = 1,
    MaxDamage,
    DamageType,
    Attribute,           // Weapon requires this attribute
    AttributeValue,      // Weapon requires this Rank of the attribute
    Armor,               // General armor
    HealthRegen,
    EnergyRegen,
    Health,              // +/- Health
    Energy,              // +/- Energy

    PhysicalDamageReduction,
    HexDurationReduction,
    ConditionDurationReduction,
    BlindnessDurationReduction,
    WeaknessDurationReduction,
    DeseaseDurationReduction,
    PoisionDurationReduction,
    DazedDurationReduction,
    DeepWoundDurationReduction,
    BleedingDurationReduction,
    CrippledDurationReduction,

    // Type specific armor
    ArmorElemental,
    ArmorFire,
    ArmorCold,
    ArmorLightning,
    ArmorEarth,
    ArmorPhysical,
    // Special
    ArmorHoly,
    ArmorShadow,
    ArmorTypeless,
    // Other
    ArmorDark,
    ArmorChaos,

    // Consumeables
    Usages,

    // Crafing price, salvage outcome
    Material1Index = 500,
    Material1Count,
    Material2Index,
    Material2Count,
    Material3Index,
    Material3Count,
    Material4Index,
    Material4Count,

    // Items may increase the attribute rank. Bellow a list of attributes it increases.
    AttributeOffset = 1000,
    __AttributeLast = AttributeOffset + 25
};

}
