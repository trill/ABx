/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <limits>
#include <cstddef>
#include <algorithm>

// Game mechanic constants. This is here because server and client may share some
// of the values here.

namespace Game {

template<typename T>
constexpr T GetPercent(T max, T percent)
{
    return static_cast<T>((static_cast<float>(max) / 100.0f) * static_cast<float>(percent));
}

namespace Internal {
template <typename T, int N>
constexpr size_t CountOf(T(&)[N])
{
    return N;
}
}

inline constexpr int PLAYER_MAX_SKILLS = 8;
// Most profession have 4 attribute but Warrior and Elementarist have 5
inline constexpr int PLAYER_MAX_ATTRIBUTES = 10;

// For client prediction these values are also needed by the client.
inline constexpr float BASE_MOVE_SPEED = 170.0f;
// Multiplier for speed when moving side-, backwards
inline constexpr float MOVE_BACK_FACTOR = 0.7f;
inline constexpr float BASE_TURN_SPEED = 1500.0f;

// Max level a player can reach
inline constexpr uint32_t LEVEL_CAP = 20;
inline constexpr uint32_t SKILLPOINT_ADVANCE_XP = 15000;
inline constexpr uint32_t COMPANION_START_LEVEL = 5;
// Attributes
inline constexpr uint32_t MAX_ATTRIBUTES = 200;
inline constexpr uint32_t ADVANCE_ATTRIB_2_10 = 5;
inline constexpr uint32_t ADVANCE_ATTRIB_11_15 = 10;
inline constexpr uint32_t ADVANCE_ATTRIB_16_20 = 15;
inline constexpr uint32_t MAX_PLAYER_ATTRIBUTE_RANK = 12;

// Resources
inline constexpr int BASE_ENERGY = 20;
inline constexpr int MAX_HEALTH_REGEN = 10;
inline constexpr float MAX_ENERGY_REGEN = 10.0f;

// Attack speeds in ms
inline constexpr uint32_t ATTACK_SPEED_AXE        = 1330;
inline constexpr uint32_t ATTACK_SPEED_SWORD      = 1330;
inline constexpr uint32_t ATTACK_SPEED_HAMMER     = 1750;
inline constexpr uint32_t ATTACK_SPEED_FLATBOW    = 2000;
inline constexpr uint32_t ATTACK_SPEED_HORNBOW    = 2700;
inline constexpr uint32_t ATTACK_SPEED_SHORTBOW   = 2000;
inline constexpr uint32_t ATTACK_SPEED_LONGBOW    = 2400;
inline constexpr uint32_t ATTACK_SPEED_RECURVEBOW = 2400;
inline constexpr uint32_t ATTACK_SPEED_STAFF      = 1750;
inline constexpr uint32_t ATTACK_SPEED_WAND       = 1750;
inline constexpr uint32_t ATTACK_SPEED_DAGGERS    = 1330;
inline constexpr uint32_t ATTACK_SPEED_SCYTE      = 1500;
inline constexpr uint32_t ATTACK_SPEED_SPEAR      = 1500;

inline constexpr float MAX_IAS = 1.33f;                    // Increased Attack Speed
inline constexpr float MAX_DAS = 0.5f;                     // Decreased Attack Speed
inline constexpr int MAX_MORALE = 10;
inline constexpr int MIN_MORALE = -60;

// For simplicity we use a sphere collision shape with this radius for projectiles
inline constexpr float PROJECTILE_SIZE = 0.3f;

// Ranges
inline constexpr float RANGE_BASE         = 80.0f;
inline constexpr float RANGE_AGGRO        = GetPercent(RANGE_BASE, 24.0f);
inline constexpr float RANGE_COMPASS      = GetPercent(RANGE_BASE, 95.0f);
inline constexpr float RANGE_HALF_COMPASS = RANGE_COMPASS / 2.0f;
inline constexpr float RANGE_TWO_COMPASS  = RANGE_COMPASS * 2.0f;
inline constexpr float RANGE_SPIRIT       = RANGE_AGGRO * 1.6f;                   // Longbow, spirits
inline constexpr float RANGE_EARSHOT      = RANGE_AGGRO;
inline constexpr float RANGE_CASTING      = RANGE_AGGRO * 1.35f;
inline constexpr float RANGE_PROJECTILE   = RANGE_AGGRO;
inline constexpr float RANGE_INTEREST     = RANGE_COMPASS * 3.0f;                 // What happens inside this range is sent to the player

// Weapon ranges
inline constexpr float RANGE_STAFF_WAND   = RANGE_AGGRO * 1.35f;
inline constexpr float RANGE_LONGBOW      = RANGE_AGGRO * 1.6f;
inline constexpr float RANGE_FLATBOW      = RANGE_AGGRO * 1.6f;
inline constexpr float RANGE_HORNBOW      = RANGE_AGGRO * 1.35f;
inline constexpr float RANGE_RECURVEBOW   = RANGE_AGGRO * 1.35f;
inline constexpr float RANGE_SHORTBOW     = RANGE_AGGRO * 1.05f;
inline constexpr float RANGE_SPEAR        = RANGE_AGGRO * 1.05f;

// Close range
inline constexpr float RANGE_TOUCH        = 1.5f;
inline constexpr float RANGE_NEARBY       = GetPercent(RANGE_BASE, 2.5f);
inline constexpr float RANGE_ADJECENT     = GetPercent(RANGE_BASE, 3.0f);
inline constexpr float RANGE_INAREA       = RANGE_ADJECENT * 2.0f;
inline constexpr float RANGE_VISIBLE      = RANGE_AGGRO;
inline constexpr float RANGE_SELECT       = RANGE_COMPASS * 2.0f;

// Sorted by distance
enum class Range : uint8_t
{
    Touch,
    Nearby,
    Adjecent,
    InArea,
    Aggro,
    Earshot,
    Projectile,
    Visible,
    Casting,
    Spirit,
    HalfCompass,
    Compass,
    TwoCompass,
    Interest,
    Map                        // Whole map, must be last
};

// Sorted by distance
inline constexpr float RangeDistances[] = {
    RANGE_TOUCH,
    RANGE_NEARBY,
    RANGE_ADJECENT,
    RANGE_INAREA,
    RANGE_AGGRO,
    RANGE_EARSHOT,
    RANGE_PROJECTILE,
    RANGE_VISIBLE,
    RANGE_CASTING,
    RANGE_SPIRIT,
    RANGE_HALF_COMPASS,
    RANGE_COMPASS,
    RANGE_TWO_COMPASS,
    RANGE_INTEREST,
    std::numeric_limits<float>::max()
};
// Just make sure we have all values of Range in RangeDistances
static_assert(static_cast<size_t>(Range::Map) == Internal::CountOf(RangeDistances) - 1);
// Ranges must be sorted
static_assert(std::is_sorted(RangeDistances, RangeDistances + Internal::CountOf(RangeDistances)));

constexpr Range GetRangeFromDistance(float distance)
{
    size_t first = 0;
    size_t last = Internal::CountOf(RangeDistances) - 1;
    float compare = 0.0f;
    while (first <= last)
    {
        size_t current = (first + last) >> 1;
        compare = RangeDistances[current] - distance;
        // Just a float-safe compare
        if (compare + std::numeric_limits<float>::epsilon() >= 0.0f &&
            compare - std::numeric_limits<float>::epsilon() <= 0.0f)
            return static_cast<Range>(current);
        if (compare < 0.0f)
            first = current + 1;
        else if (current > 0)
            last = current - 1;
        else
            return static_cast<Range>(0);
    }
    return compare < 0.0f ? static_cast<Range>(last) : static_cast<Range>(first);
}

// https://wiki.guildwars.com/wiki/Armor_rating
inline constexpr float DamagePosChances[] = {
    0.125f,                                // Head
    0.375f,                                // Chest
    0.125f,                                // Hands
    0.25f,                                 // Legs
    0.125f,                                // Feet
};

// If the rotation of 2 actors is smaller than this, an attack is from behind
inline constexpr float BEHIND_ANGLE = 0.52f;
inline constexpr uint32_t DEFAULT_KNOCKDOWN_TIME = 2000;

// Item value at lvl20
inline constexpr unsigned int MIN_ITEM_VALUE = 10;
inline constexpr unsigned int MAX_ITEM_VALUE = 100;

// --- Inventory ---
inline constexpr uint32_t MAX_INVENTOREY_MONEY = 1000 * 1000;  // 1M
inline constexpr uint32_t DEFAULT_CHEST_MONEY = MAX_INVENTOREY_MONEY * 100; // 100M
inline constexpr uint32_t MAX_INVENTORY_STACK_SIZE = 250;
inline constexpr uint32_t MAX_CHEST_STACK_SIZE = 250;

// If the difference is smaller than this we will say it's the same position.
// Should be smaller than RANGE_TOUCH.
inline constexpr float AT_POSITION_THRESHOLD = RANGE_TOUCH - 0.1f;

}

namespace AI {

/// The AI will consider healing if HP is bellow this health/max health ratio
inline constexpr float LOW_HP_THRESHOLD = 0.8f;            // Percent
inline constexpr int CRITICAL_HP_THRESHOLD = 50;           // Absolute
inline constexpr float LOW_ENERGY_RATIO_THRESHOLD = 1.0f;  // Percent
inline constexpr int LOW_ENERGY_THRESHOLD = 5;             // Absolute

}
