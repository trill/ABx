/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

namespace Game {

int CalcAttributeCost(int rank);
uint32_t GetAttribPoints(uint32_t level);

}
