/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AttribAlgos.h"
#include "Mechanic.h"

namespace Game {

int CalcAttributeCost(int rank)
{
    static constexpr int cost[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16, 20
    };
    int result = 0;
    for (int i = 1; i <= rank; ++i)
    {
        if (i <= 12)
            result += cost[i];
        else
            result += i;
    }
    return result;
}

uint32_t GetAttribPoints(uint32_t level)
{
    auto clamp = [](uint32_t v, uint32_t min, uint32_t max) -> uint32_t
    {
        if (v < min)
            return min;
        if (v > max)
            return max;
        return v;
    };

    uint32_t result{ 0 };
    if (level > 1)
        result += ADVANCE_ATTRIB_2_10 * (clamp(level, 2, 10) - 1);
    if (level > 10)
        result += ADVANCE_ATTRIB_11_15 * (clamp(level, 11, 15) - 10);
    if (level > 15)
        result += ADVANCE_ATTRIB_16_20 * (clamp(level, 16, 20) - 15);
    if (level >= LEVEL_CAP)
        result += 30;
    if (level > LEVEL_CAP)
        result += ADVANCE_ATTRIB_16_20 * (20 - level);
    return result;
}

}
