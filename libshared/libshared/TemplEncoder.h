/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include "Attributes.h"
#include <array>
#include <AB/Entities/Profession.h>
#include <string>

#define SKILLS_TEMPLATE_HEADER_VERSION (0)
#define SKILLS_TEMPLATE_HEADER_TYPE (0xe)

namespace IO {

uint8_t GetSkillsTemplateHeader();
std::string SkillTemplateEncode(const AB::Entities::Profession& prof1, const AB::Entities::Profession& prof2,
    const Game::Attributes& attribs, const Game::SkillIndices& skills);
bool SkillTemplateDecode(const std::string& templ,
    AB::Entities::Profession& prof1, AB::Entities::Profession& prof2,
    Game::Attributes& attribs, Game::SkillIndices& skills);

}
