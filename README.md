# ABx

Ancient Greece Multiplayer Online RPG.

Official repository: https://codeberg.org/trill/ABx

Videos: https://diode.zone/c/abx/videos

Project page: https://www.gamedev.net/projects/1587-abx

Blog: https://www.gamedev.net/blogs/blog/3042-trill41/

## License

The source code is licensed under the AGPL 3.0 or later.

Assets, Data, Art made by me is licensed under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

## ETA

Christmas 2117 +/- some years.

## The Goals

* Instanced world (check, there are just not many instances)
* Primary and secondary professions/classes (check)
* Big skill pool
* Diverse skill builds and team builds
* Focus on tactics rather than kill everything that moves
* Cooperative and competitive game formats
* PvP and PvE

## Current features

* Authoritative server.
* Uses a single TCP stream for the game protocol. This is not a shooter or action game, more like a classical online RPG.
* Uses PostgreSQL as database back-end.
* Caching data server enabling frequent reads/writes without much overhead.
* Instanced world.
* Encrypted Game and Login Protocol.
* Lag compensation: Client prediction.
* Network traffic compression:
    - Delta compression (send only state changes)
    - Interest management. Send only relevant messages to each player, e.g. a Player doesn't need to know when an Actor moves far away from the Player
* The system is designed to scale:
    - It can spawn any number of server which may have heavy load (game, file server) even on different hardware.
    - Automatically spawn File and Game Server when needed.
* Chat system even across different game server:
    - Local (map) chat
    - Guild chat
    - Team chat
    - Trade chat (cross server)
    - Whisper
* Friend/ignore list
* Persistent Mail with support for attachments, send items to other Players.
* Automatic generation of server scenes (height fields, navigation meshes etc.) from Urho3D's scene files.
* Lua API to script Games, NPCs, Skills, Effects, Items etc.
* Navigation using Recast/Detour
* Server and Client runs on Linux
* Game AI, Behavior Tree
* [Mumble](https://www.mumble.info/) voice chat integration with extended support (Context, identity, overlay), which provides spatial voice chat, see [Positional-Audio](https://wiki.mumble.info/wiki/Positional-Audio).
* Item system:
    - Item drops, when foes are killed they may or may not drop a random item. Each Item has a certain drop chance on a certain map.
    - Player inventory and account chest (account wide storage).
    - Players can exchange Items and Money with other Players.
    - Players can sell items that dropped or otherwise obtained to a Merchant and gain money.
    - Players can buy items from the Merchant which the Merchant has in stock.
    - Dynamic item price taking availability and drop chance into account.
    - Players can craft new items for Materials and Money from special NPCs.
* Autoupdate client asset files: It downloads only the differences and patches existing files. This greatly reduces time and bandwidth.
* Minimap, a small map of the scene the player is in, which also shows other actors (buggy tough).
* Class system with primary and secondary class. The player has a fixed primary class and a changeable secondary class.
* Skill system. Depending on the primary and secondary class of the player, different skills are available to the player.
* Skills are implemented a simple Lua scripts and can be added/removed/changed quickly.
* Mini Pets! Well, two, now with textures and animations!
* Animal companions. Rangers can tame certain animals which follow and fight with the player.

## Screenshot

![Screenshot](Doc/screenshot.jpg?raw=true)

## Motivation

The only Computer Game I play is [Guild Wars](https://www.guildwars.com/) and I play it for
well over 12 years now. With my main account I played for over 12,000 hours, and I still
enjoy it. But the game is old, and does not get much love anymore. So maybe
it's time to create something like it.

However, the main motivation for me is, I think Guild Wars is an awesome piece of technology,
and I want to know if I can create something like it. I know the Art work is beyond
my Art skills (they are literally not existent), but with enough time it should be possible
to do the programming part.

There are some other nice side effects. It's exciting times to program in C++,
it is moving fast, but does not break things.

## Build, install, run

See [BUILD.md](BUILD.md) and [INSTALL.md](INSTALL.md).

## FAQ

**Q** Some models appear (partly) transparent/invisible.

**A** This is a known issue (https://codeberg.org/trill/ABx/issues/75),
see here https://codeberg.org/trill/ABx/issues/55 for a solution.

## Contributing

Contributions are welcome! See [CONTRIBUTING.md](CONTRIBUTING.md) for more
informations.

## Credits

See [CREDITS.md](CREDITS.md).

## Architecture

![Architecture](Doc/abschema.png?raw=true)

### Data Server (`abdata`)

The Data server provides data from the database server and caches it. It also
acts as threadsafe interserver shared memory. Connection to this server is
required for:

* File Server
* Login Server
* Game Server

There can be only *one* data server. It is the central point to which all other
servers connect, and get their data from.

### File Server (`abfile`)

The file server is a simple HTTP server providing files and other information.
The client may connect to it from time to time and download data.

It is also used to update the game client and the client assets.

There can be any number of file servers. Usually you may want to have file servers
in different regions, and not on the same machine. A file server does not need
much resources, just bandwidth. But if they run on the same machine, they must
listen on different ports.

### Login Server (`ablogin`)

Used by the client to login, create accounts and manage characters.

It also tells the client to which Game and File Server to connect.

There can be only *one* login server, since all clients connect to this server.
Once the client is authenticated, the connection to the login server is closed.

### Game Server (`abserv`)

The game server simulates the games. There can be any number of game servers, but
if they run on the same machine, the game servers must listen on different ports.

Since all game server connect to the same data server, all game server share the
same data. Because this game is designed to have an instanced world, several game
server instances appear to the player as one game server. It is even possible to
change the game server with a simple menu click.

So, in theory, how many players and games this server can handle, depends only
on how much hardware you have.

### Message Server (`abmsgs`)

This one is for inter server communication, for example to deliver messages from
a player to another player on a different game server.

### Load Balancer (`ablb`)

An optional load balancer/proxy to make it possible to have several Login Server.
It'll lookup running Login Servers and bridge client connections to one of them.

### Admin Server (`absadmin`)

A Web interface to view and edit some data. For example Admins can view running
services, games, account, can create account and IP bans, account keys etc.

On Linux this server must be run as root, because it binds the ports 80 and 443,
which are privileged ports on Linux.

## Run Server

The servers must be run in the correct order, because they depend on each other.

1. Run the data server `abdada`
2. Run the message server `abmsgs`
3. Run the file server `abfile`
4. Run the login server `ablogin`
5. Run the game server `abserv`

You can also use the `run` script in the `bin` directory.

Optional run `absadmin`.

All servers must run with the same timezone, preferably UTC.

## Run Client

1. Start `fw`
2. Create an account
3. Create a character
4. Enter world
