# Contributing

If you want to contribute to this project, please fork the repository and
submit pull requests.

If you have questions, you can reach me via Email with the Email address shown in
my profile. If you have an issue with compiling and/or running, make a new Issue.
You can also join the Telegram Channel https://t.me/trill41.

## Pull requests

The normal way to make Pull Requests (PR) is to create a separate feature branch:
~~~
git branch my-awesome-feature
~~~

Then commit your changes to this newly created branch and push it to your fork.
Then you can create a PR. After your PR has been merged you can delete this feature
branch.

## Rules

* The license is AGPL 3.0 and submitted code must not be incompatible with it.
* The code must compile without warnings and the tests must succeed.
* You are encouraged to use new C++ features. Partly this project is also a playground
to explore modern C++.
* The code should be readable and it should be obvious what it does.
* Please stick to the coding style. There is a `.clang-format` file in the root directory,
you could just run it with `clang-format -style=.clang-format [<file> ...]`.
clang-format 11 is required.

## Linux

As C++ IDE on Linux I prefer QtCreator. QtCreator has excellent support for CMake,
so you can just open the top level `CMakeLists.txt` file with it.

![QtCreator](Doc/qtcreator.png?raw=true)

Then you can configure QtCreator to build the project for you with GNU make or,
like in my case, Ninja.

![QtCreator Build](Doc/qtcreator_build.png?raw=true)

## If you don't want to code

That's also great, because a game doesn't need only code. In fact coding is the
smallest problem. It also needs people who do:

* 3D Modeling
* Animations
* Story writing
* Creating maps
* Creating skills
* Music
* Sound effects
* Particle effects
* Icons
* Concept art

and a whole lot more.
