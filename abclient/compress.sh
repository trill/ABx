#!/bin/sh

if [ -f "./fw.tar.gz" ]; then rm -f "./fw.tar.gz"; fi

tar -zcvf fw.tar.gz bin/fw bin/abupdate bin/*.pak README.md LICENSE.txt config.xml trill.png trill.svg fw.desktop
