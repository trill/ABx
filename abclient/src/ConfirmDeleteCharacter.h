/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"

URHO3D_EVENT(E_CONFIRMDELETECHAR, ConfirmDeleteChar)
{
    URHO3D_PARAM(P_OK, Ok);              // bool
    URHO3D_PARAM(P_UUID, Uuid);              // String
    URHO3D_PARAM(P_NAME, Name);              // String
}

class ConfirmDeleteCharacter : public DialogWindow
{
    URHO3D_OBJECT(ConfirmDeleteCharacter, DialogWindow)
private:
    void HandleDeleteClicked(StringHash eventType, VariantMap& eventData);
    void HandleCancelClicked(StringHash eventType, VariantMap& eventData);
    void DeleteCharacter();
    String uuid_;
    String name_;
    SharedPtr<LineEdit> nameEdit_;
public:
    static void RegisterObject(Context* context);
    ConfirmDeleteCharacter(Context* context);
    ConfirmDeleteCharacter(Context* context, const String& uuid, const String& name);
    ~ConfirmDeleteCharacter() override;
};
