/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HealthBar.h"

void HealthBar::RegisterObject(Context* context)
{
    context->RegisterFactory<HealthBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(ValueBar);
}

HealthBar::HealthBar(Context* context) :
    ValueBar(context),
    showName_(false)
{
    SetMinHeight(23);
    SetMaxHeight(23);
    SetAlignment(HA_LEFT, VA_CENTER);

    SetStyle("HealthBar");

    SetLayoutMode(LM_FREE);
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    container_ = CreateChild<UIElement>();
    container_->SetAlignment(HA_RIGHT, VA_CENTER);
    container_->SetLayoutMode(LM_HORIZONTAL);
    container_->SetPosition(-4, 0);
    container_->SetFixedHeight(16);

    auto* tex = cache->GetResource<Texture2D>("Textures/Fw-UI-Ex.png");
    condition_ = container_->CreateChild<BorderImage>("ConditionIcon");
    condition_->SetTexture(tex);
    condition_->SetImageRect({ 48, 0, 64, 16 });
    condition_->SetFixedSize(16, 16);
    condition_->SetAlignment(HA_RIGHT, VA_CENTER);
    hex_ = container_->CreateChild<BorderImage>("HexIcon");
    hex_->SetTexture(tex);
    hex_->SetImageRect({ 32, 0, 48, 16 });
    hex_->SetFixedSize(16, 16);
    hex_->SetAlignment(HA_RIGHT, VA_CENTER);
    ench_ = container_->CreateChild<BorderImage>("EnchIcon");
    ench_->SetTexture(tex);
    ench_->SetImageRect({ 16, 0, 32, 16 });
    ench_->SetFixedSize(16, 16);
    ench_->SetAlignment(HA_RIGHT, VA_CENTER);

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(HealthBar, HandleUpdate));
    SubscribeToEvent(Events::E_SET_SECPROFESSION, URHO3D_HANDLER(HealthBar, HandleActorSkillsChanged));
    SubscribeToEvent(Events::E_OBJECTPROGRESS, URHO3D_HANDLER(HealthBar, HandleObjectProgress));
}

HealthBar::~HealthBar()
{
    UnsubscribeFromAllEvents();
}

void HealthBar::SetActor(SharedPtr<Actor> actor)
{
    if (actor)
    {
        SetText(actor->GetClassLevelName());
        actor_ = actor;
    }
    else
    {
        actor_.Reset();
        SetText(String::EMPTY);
    }
}

SharedPtr<Actor> HealthBar::GetActor()
{
    return actor_.Lock();
}

void HealthBar::SetShowName(bool value)
{
    if (showName_ != value)
    {
        showName_ = value;
        SetShowText(showName_);
    }
}

void HealthBar::HandleUpdate(StringHash, VariantMap&)
{
    if (SharedPtr<Actor> a = actor_.Lock())
    {
        SetValues(a->stats_.maxHealth, a->stats_.health);
        if (showEffects_)
            UpdateEffects(*a);
    }
}

void HealthBar::UpdateEffects(const Actor& actor)
{
    const auto& effects = actor.GetEffects();

    auto hasCat = [&](const AB::Entities::EffectCategory& cat)
    {
        for (const auto& e : effects)
        {
            if (e.second_ == cat)
                return true;
        }
        return false;
    };
    condition_->SetVisible(hasCat(AB::Entities::EffectCondition));
    ench_->SetVisible(hasCat(AB::Entities::EffectEnchantment));
    hex_->SetVisible(hasCat(AB::Entities::EffectHex));
    int count = 0;
    if (condition_->IsVisible())
        ++count;
    if (hex_->IsVisible())
        ++count;
    if (ench_->IsVisible())
        ++count;
    container_->SetFixedWidth(count * 16);
    container_->UpdateLayout();
    UpdateLayout();
}

void HealthBar::HandleActorSkillsChanged(StringHash, VariantMap& eventData)
{
    if (auto a = actor_.Lock())
    {
        using namespace Events::ActorSkillsChanged;
        if (a->gameId_ != eventData[P_OBJECTID].GetUInt())
            return;
        SetText(a->GetClassLevelName());
    }
}

void HealthBar::HandleObjectProgress(StringHash, VariantMap& eventData)
{
    if (auto a = actor_.Lock())
    {
        using namespace Events::ObjectProgress;
        if (a->gameId_ != eventData[P_OBJECTID].GetUInt())
            return;
        SetText(a->GetClassLevelName());
    }
}
