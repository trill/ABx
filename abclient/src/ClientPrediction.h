/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <AB/ProtocolCodes.h>

class HeightMap;

class ClientPrediction : public LogicComponent
{
    URHO3D_OBJECT(ClientPrediction, LogicComponent)
private:
    int64_t serverTime_;
    int64_t lastStateChange_;
    Vector3 serverPos_;
    uint8_t lastMoveDir_ = 0;
    uint8_t lastTurnDir_ = 0;
    mutable SharedPtr<Terrain> terrain_;
    mutable SharedPtr<HeightMap> heightMap_;
    AB::GameProtocol::CreatureState lastState_{ AB::GameProtocol::CreatureState::Idle };
    void UpdateMove(float timeStep, uint8_t direction, float speedFactor);
    void Move(float speed, const Vector3& amount);
    void UpdateTurn(float timeStep, uint8_t direction, float speedFactor);
    void Turn(float yAngle);
    void TurnAbsolute(float yAngle);
    float GetSpeed(float timeElapsed, float baseSpeed, float speedFactor)
    {
        return ((timeElapsed * 1000.0f) / baseSpeed) * speedFactor;
    }
    bool CheckCollision(const Vector3& pos);
    float GetHeight(const Vector3& world) const;
    Terrain* GetTerrain() const;
    HeightMap* GetHeightMap() const;
public:
    static void RegisterObject(Context* context);

    ClientPrediction(Context* context);
    ~ClientPrediction() override;

    /// Called on scene update, variable timestep.
    void FixedUpdate(float timeStep) override;
    void CheckServerPosition(int64_t time, const Vector3& serverPos);
    void CheckServerRotation(int64_t time, float rad);
};

