/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Item.h>
#include <Urho3DAll.h>

class Item : public Object
{
    URHO3D_OBJECT(Item, Object)
public:
    Item(Context* context);
    ~Item() override;

    template <typename T>
    T* GetObjectResource()
    {
        if (objectFile_.Empty())
            return nullptr;
        ResourceCache* cache = GetSubsystem<ResourceCache>();
        return cache->GetResource<T>(objectFile_);
    }
    template <typename T>
    T* GetIconResource()
    {
        if (iconFile_.Empty())
            return nullptr;
        ResourceCache* cache = GetSubsystem<ResourceCache>();
        return cache->GetResource<T>(iconFile_);
    }

    String uuid_;
    uint32_t index_{ 0 };
    String name_;
    /// Prefab file
    String objectFile_;
    /// UI icon file, somewhere in /Textures
    String iconFile_;
    AB::Entities::ItemType type_{ AB::Entities::ItemType::Unknown };
    AB::Entities::ModelClass modelClass_{ AB::Entities::ModelClass::Unknown };
    bool stackAble_{ false };
    bool tradeAble_{ false };
};

String FormatMoney(uint32_t amount);
