/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MaintainedEffectsWindow.h"
#include "FwClient.h"
#include "SkillManager.h"
#include "LevelManager.h"
#include "Player.h"
#include "UIPriorities.h"

static constexpr unsigned ICON_SIZE = 40;

void MaintainedEffectsWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<MaintainedEffectsWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(UIElement);
}

MaintainedEffectsWindow::MaintainedEffectsWindow(Context* context) :
    UIElement(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile *file = cache->GetResource<XMLFile>("UI/MaintainedEffectsWindow.xml");
    LoadChildXML(file->GetRoot(), nullptr);
    SetName("MaintainedEffectsWindow");

    SetAlignment(HA_CENTER, VA_BOTTOM);
    SetPosition(220, 0);
    SetLayoutMode(LM_VERTICAL);
    SetLayoutBorder({ 4, 4, 4, 4 });
    SetLayoutSpacing(4);
    SetPivot(0, 0);
    SetOpacity(0.9f);
    SetMinSize(ICON_SIZE, ICON_SIZE);
    SetPriority(Priorities::MaintainedEffectsWindow);
    SetVisible(false);
    SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(MaintainedEffectsWindow, HandleMouseDown));
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(MaintainedEffectsWindow, HandleMouseUp));
}

MaintainedEffectsWindow::~MaintainedEffectsWindow()
{
    UnsubscribeFromAllEvents();
}

void MaintainedEffectsWindow::EffectAdded(uint32_t effectIndex, uint32_t targetId)
{
    SkillManager* sm = GetSubsystem<SkillManager>();
    const Effect* effect = sm->GetEffectByIndex(effectIndex);
    if (!effect)
    {
        URHO3D_LOGERRORF("Effect with index %u not found", effectIndex);
        return;
    }

    const auto* skill = sm->GetSkillByIndex(effectIndex);
    if (!skill)
    {
        URHO3D_LOGERRORF("Skill with index %u not found", effectIndex);
        return;
    }

    auto* lm = GetSubsystem<LevelManager>();
    auto* targetActor = lm->GetActor(targetId);
    if (!targetActor)
    {
        URHO3D_LOGERRORF("Actor with ID %u not found", targetId);
        return;
    }

    const String name = "Effect_" + String(effectIndex) + "_" + String(targetId);
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    ++effectCount_;
    Button* effectIcon = CreateChild<Button>(name);
    effectIcon->SetVar("Index", effectIndex);
    effectIcon->SetVar("Target", targetId);
    effectIcon->SetFixedSize(ICON_SIZE, ICON_SIZE);
    effectIcon->SetOpacity(1.0f);

    auto* tt = effectIcon->CreateChild<ToolTip>();
    tt->SetLayoutMode(LM_VERTICAL);
    auto* ttWindow = tt->CreateChild<Window>();

    XMLFile* xml = cache->GetResource<XMLFile>("UI/MaintainedEffectTooltip.xml");
    ttWindow->LoadXML(xml->GetRoot());

    ttWindow->SetLayoutMode(LM_VERTICAL);
    auto* ttSkillName = ttWindow->GetChildStaticCast<Text>("SkillName", true);
    ttSkillName->SetText(skill->name.c_str());
    auto* ttTTargetName = ttWindow->GetChildStaticCast<Text>("TargetName", true);
    ttTTargetName->SetText(targetActor->name_);
    ttWindow->UpdateLayout();
    tt->SetPosition(ICON_SIZE, -(int)ICON_SIZE);
    tt->SetStyleAuto();
    tt->SetFixedSize(ttWindow->GetSize());

    Texture2D* icon = cache->GetResource<Texture2D>(String(effect->icon.c_str()));
    if (!icon)
        icon = cache->GetResource<Texture2D>("Textures/Skills/placeholder.png");
    icon->SetNumLevels(1);
    icon->SetMipsToSkip(QUALITY_LOW, 0);
    effectIcon->SetTexture(icon);
    effectIcon->SetFullImageRect();


    SetHeight(ICON_SIZE * effectCount_);
    SetPosition(220, -(int)(((ICON_SIZE + 4) * effectCount_) + 4));
    SetVisible(effectCount_ != 0);
    UpdateLayout();

}

void MaintainedEffectsWindow::EffectRemoved(uint32_t effectIndex, uint32_t targetId)
{
    const String name = "Effect_" + String(effectIndex) + "_" + String(targetId);
    Button* effectIcon = GetChildStaticCast<Button>(name, true);
    if (effectIcon)
    {
        --effectCount_;
        RemoveChild(effectIcon);
        SetHeight(ICON_SIZE * effectCount_);
        SetPosition(220, -(int)(((ICON_SIZE + 4) * effectCount_) + 4));
        SetVisible(effectCount_ != 0);
        UpdateLayout();
    }
}

void MaintainedEffectsWindow::Clear()
{
    SetVisible(false);
    RemoveAllChildren();
    effectCount_ = 0;
}

void MaintainedEffectsWindow::HandleMouseDown(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonDown;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;
    if (eventData[P_CLICKS].GetInt() != 2)
        return;
    auto* input = GetSubsystem<Input>();
    if (!IsInside(input->GetMousePosition(), true))
        return;
    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || !elem->IsChildOf(this))
        return;

    uint32_t index = elem->GetVar("Index").GetUInt();
    if (index == 0)
        return;
    uint32_t target = elem->GetVar("Target").GetUInt();
    if (target == 0)
        return;

    auto* sm = GetSubsystem<SkillManager>();
    auto* skill = sm->GetSkillByIndex(index);
    if (!skill)
        return;
    if (!skill->isMaintainable)
        return;
    FwClient* client = GetSubsystem<FwClient>();
    client->RemoveMaintainableEnch(index, target);
}

void MaintainedEffectsWindow::HandleMouseUp(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;
    auto* input = GetSubsystem<Input>();
    if (!input->GetKeyDown(KEY_LCTRL))
        return;

    if (!IsInside(input->GetMousePosition(), true))
        return;

    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || !elem->IsChildOf(this))
        return;

    uint32_t index = elem->GetVar("Index").GetUInt();
    if (index == 0)
        return;
    uint32_t target = elem->GetVar("Target").GetUInt();
    if (target == 0)
        return;

    FwClient* client = GetSubsystem<FwClient>();
    client->PingMaintainedEffect(index, target);
}
