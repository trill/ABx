/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PvpCombatLevel.h"
#include "ChatWindow.h"
#include "FwClient.h"
#include "LevelManager.h"
#include "WindowManager.h"
#include "PartyWindow.h"

//#include <Urho3D/DebugNew.h>

PvpCombatLevel::PvpCombatLevel(Context* context) :
    WorldLevel(context)
{
    LevelManager* lm = GetSubsystem<LevelManager>();
    mapUuid_ = lm->GetMapUuid();
    mapType_ = lm->GetMapType();
    partySize_ = lm->GetPartySize();
    FwClient* cli = GetSubsystem<FwClient>();
    mapName_ = cli->GetGameName(mapUuid_);
    // Create the scene content
    CreateScene();

    // Create the UI content
    CreateUI();

    // Subscribe to global events for camera movement
    SubscribeToEvents();
}

void PvpCombatLevel::SubscribeToEvents()
{
    WorldLevel::SubscribeToEvents();
}

void PvpCombatLevel::SceneLoadingFinished()
{
    CreateUI();

    WorldLevel::SceneLoadingFinished();
    VariantMap& eData = GetEventDataMap();
    using namespace Events::LevelReady;
    eData[P_NAME] = "PvpCombatLevel";
    eData[P_TYPE] = static_cast<int>(mapType_);
    SendEvent(Events::E_LEVELREADY, eData);

    chatWindow_->AddLine("Entered " + mapName_, "ChatLogServerInfoText");
}

void PvpCombatLevel::CreateUI()
{
    WorldLevel::CreateUI();

    WindowManager* wm = GetSubsystem<WindowManager>();
    partyWindow_.DynamicCast(wm->GetWindow(WINDOW_PARTY));
    uiRoot_->AddChild(partyWindow_);
    partyWindow_->SetMode(PartyWindowMode::ModeOutpost);
}
