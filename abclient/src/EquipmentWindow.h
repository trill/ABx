/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class Player;
class PostProcessController;

class EquipmentWindow : public Window
{
    URHO3D_OBJECT(EquipmentWindow, Window)
private:
    bool modelLoaded_{ false };
    SharedPtr<Scene> modelScene_;
    SharedPtr<Node> characterNode_;
    SharedPtr<View3D> modelViewer_;
    SharedPtr<AnimationController> animController_;
    bool mouseDown_{ false };
    bool initialized_{ false };
    void SubscribeEvents();
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleLoadClicked(StringHash eventType, VariantMap& eventData);
    void HandleSaveClicked(StringHash eventType, VariantMap& eventData);
    void HandleCreateClicked(StringHash eventType, VariantMap& eventData);
    void HandleSceneViewerMouseMove(StringHash eventType, VariantMap& eventData);
    void HandleSceneViewerMouseDown(StringHash eventType, VariantMap& eventData);
    void HandleSceneViewerMouseUp(StringHash eventType, VariantMap& eventData);
    bool LoadObject(uint32_t itemIndex, Node* node);
public:
    static void RegisterObject(Context* context);
    EquipmentWindow(Context* context);
    ~EquipmentWindow() override;

    void UpdateEquipment(Player* player);
    void Initialize(PostProcessController& pp);
};
