/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class ValueBar : public ProgressBar
{
    URHO3D_OBJECT(ValueBar, ProgressBar)
public:
    static void RegisterObject(Context* context);

    ValueBar(Context* context);
    ~ValueBar() override = default;
    void GetBatches(Vector<UIBatch>& batches, Vector<float>& vertexData, const IntRect& currentScissor) override;
    void UpdateKnob();
    void SetValues(unsigned max, unsigned value);
    void SetText(const String& value);
    void SetShowText(bool value);

    SharedPtr<Text> text_;
    bool selectable_{ true };
};
