/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <sa/bitmap.h>

class ProgressElement : public BorderImage
{
    URHO3D_OBJECT(ProgressElement, BorderImage)
private:
    SharedPtr<Texture2D> texture_;
    SharedPtr<Image> image_;
    sa::Bitmap bitmap_;
    float current_{ 0.0f };
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleRenderUpdate(StringHash eventType, VariantMap& eventData);
    void DrawProgress();
public:
    enum class Direction
    {
        Forward,
        Backward
    };
    enum class Mode
    {
        Inc,
        Dec
    };
    static void RegisterObject(Context* context);
    ProgressElement(Context* context);
    ~ProgressElement() override;

    void Start();
    void Stop();
    void SetMaxTicks(uint32_t ticks);
    Direction direction_{ Direction::Backward };
    Mode mode_{ Mode::Inc };
    float max_{ 0.0f };
};
