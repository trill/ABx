/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

using namespace Urho3D;

/// Client internal events
namespace Events
{

URHO3D_EVENT(E_START_PROGRAM, StartProgram)
{
    URHO3D_PARAM(P_COMMAND, Command);        // String
}

URHO3D_EVENT(E_RESTART, Restart)
{
}

URHO3D_EVENT(E_UPDATESTART, UpdateStart)
{
}

URHO3D_EVENT(E_CANCELUPDATE, CancelUpdate)
{
}

URHO3D_EVENT(E_UPDATEPROGRESS, UpdateProgress)
{
    URHO3D_PARAM(P_VALUE, Value);        // int
    URHO3D_PARAM(P_MAX, Max);        // int
    URHO3D_PARAM(P_PERCENT, Percent);        // float
}

URHO3D_EVENT(E_UPDATEDOWNLOADPROGRESS, UpdateDownloadProgress)
{
    URHO3D_PARAM(P_BYTEPERSEC, BytePerSec);        // int
    URHO3D_PARAM(P_MAX, Max);        // int
    URHO3D_PARAM(P_PERCENT, Percent);        // float
}

URHO3D_EVENT(E_UPDATEFILE, UpdateFile)
{
    URHO3D_PARAM(P_INDEX, Index);        // unsigned
    URHO3D_PARAM(P_COUNT, Count);        // unsigned
    URHO3D_PARAM(P_FILENAME, FileName);        // String
}

URHO3D_EVENT(E_UPDATEDONE, UpdateDone)
{
    URHO3D_PARAM(P_SUCCESS, Success);        // bool
}

URHO3D_EVENT(E_SCREENSHOTTAKEN, ScreenshotTaken)
{
    URHO3D_PARAM(P_FILENAME, Filename);        // String
}

URHO3D_EVENT(E_SETLEVEL, SetLevel)
{
    URHO3D_PARAM(P_UPDATETICK, UpdateTick);
    URHO3D_PARAM(P_NAME, Name);                  // String
    URHO3D_PARAM(P_MAPUUID, MapUuid);            // String
    URHO3D_PARAM(P_INSTANCEUUID, InstanceUuid);  // String
    URHO3D_PARAM(P_TYPE, Type);                  // uint8_t
    URHO3D_PARAM(P_PARTYSIZE, PartySize);        // uint8_t
}

URHO3D_EVENT(E_LEVELREADY, LevelReady)
{
    URHO3D_PARAM(P_NAME, Name);        // String
    URHO3D_PARAM(P_TYPE, Type);        // int
}

URHO3D_EVENT(E_WHISPERTO, WhisperTo)
{
    URHO3D_PARAM(P_NAME, Name);
}

URHO3D_EVENT(E_SENDMAILTO, SendMailTo)
{
    URHO3D_PARAM(P_NAME, Name);
    URHO3D_PARAM(P_SUBJECT, Subject);
    URHO3D_PARAM(P_BODY, Body);
}

URHO3D_EVENT(E_ACTORCLICKED, ActorClicked)
{
    URHO3D_PARAM(P_SOURCEID, SourceId);     // unit32_t
}

URHO3D_EVENT(E_ACTORDOUBLECLICKED, ActorDoubleClicked)
{
    URHO3D_PARAM(P_SOURCEID, SourceId);     // unit32_t
}

// Audio
URHO3D_EVENT(E_AUDIOPLAY, AudioPlay)
{
    URHO3D_PARAM(P_NAME, Name);     // String
    URHO3D_PARAM(P_TYPE, Type);     // String
}

URHO3D_EVENT(E_AUDIOSTOP, AudioStop)
{
    URHO3D_PARAM(P_NAME, Name);     // String
    URHO3D_PARAM(P_TYPE, Type);     // String
}

URHO3D_EVENT(E_AUDIOSTOPALL, AudioStopAll)
{
}

URHO3D_EVENT(E_AUDIOPLAYMAPMUSIC, AudioPlayMapMusic)
{
    URHO3D_PARAM(P_MAPUUID, MapUuid);     // String
}

/// Play style from current play list
URHO3D_EVENT(E_AUDIOPLAYMUSICSTYLE, AudioPlayMusicStyle)
{
    URHO3D_PARAM(P_STYLE, Style);     // uint32_t
}

URHO3D_EVENT(E_ACTOR_SKILLS_CHANGED, ActorSkillsChanged)
{
    URHO3D_PARAM(P_OBJECTID, ObjectId);     // uint32_t
    URHO3D_PARAM(P_UPDATEALL, UpdateAll);   // uint32_t
}

URHO3D_EVENT(E_SERVER_PING, ServerPing)
{
    URHO3D_PARAM(P_NAME, Name);
    URHO3D_PARAM(P_HOST, Host);
    URHO3D_PARAM(P_PORT, Port);
    URHO3D_PARAM(P_SUCCESS, Success);
    URHO3D_PARAM(P_PING_TIME, PingTime);
}

URHO3D_EVENT(E_CHANGE_INSTANCE_REQUEST, ChangeInstanceRequest)
{
    URHO3D_PARAM(P_SERVER, Server);
    URHO3D_PARAM(P_MAP, Map);
    URHO3D_PARAM(P_CHARACTER, Character);
}

}
