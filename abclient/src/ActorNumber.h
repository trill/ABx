/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class Actor;

class ActorNumber : public Text
{
    URHO3D_OBJECT(ActorNumber, Text)
public:
    enum class NumberType
    {
        None,
        LostHealth,
        LostEnergy,
        GainedHealth,
        GainedEnergy,
        XP
    };
    static void RegisterObject(Context* context);

    ActorNumber(Context* context);
    ~ActorNumber() override;

    void SetNumber(NumberType type, int value, bool isPlayer);
    void SetTarget(SharedPtr<Actor> owner);
private:
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    WeakPtr<Actor> target_;
    NumberType type_{ NumberType::None };
    int value_{ 0 };
    float timeVisible_{ 0.0f };
    int screenPosX_{ 0 };
};
