/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class MaintainedEffectsWindow : public UIElement
{
    URHO3D_OBJECT(MaintainedEffectsWindow, UIElement)
private:
    unsigned effectCount_;
    void HandleMouseDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);

    MaintainedEffectsWindow(Context* context);
    ~MaintainedEffectsWindow() override;
    void EffectAdded(uint32_t effectIndex, uint32_t targetId);
    void EffectRemoved(uint32_t effectIndex, uint32_t targetId);
    void Clear();
};

