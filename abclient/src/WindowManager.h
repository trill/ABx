/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Dialogs.h>
#include "DialogWindow.h"

static const StringHash WINDOW_OPTIONS("OptionsWindow");
static const StringHash WINDOW_CHAT("CatWindow");
static const StringHash WINDOW_MAIL("MailWindow");
static const StringHash WINDOW_PARTY("PartyWindow");
static const StringHash WINDOW_PINGDOT("PingDot");
static const StringHash WINDOW_TARGET("TargetWindow");
static const StringHash WINDOW_SKILL_MONITOR("SkillMonitor");
static const StringHash WINDOW_NEWMAIL("NewMailWindow");
static const StringHash WINDOW_MISSIONMAP("MissionMapWindow");
static const StringHash WINDOW_SKILLBAR("SkillBarWindow");
static const StringHash WINDOW_FRIENDLIST("FriendListWindow");
static const StringHash WINDOW_GAMEMESSAGES("GameMessagesWindow");
static const StringHash WINDOW_EFFECTS("EffectsWindow");
static const StringHash WINDOW_MAINTANED_EFFECTS("MaintainedEffectsWindow");
static const StringHash WINDOW_INVENTORY("InventoryWindow");
static const StringHash WINDOW_GUILD("GuildWindow");
static const StringHash WINDOW_HERO("HeroWindow");
static const StringHash WINDOW_SKILLS("SkillsWindow");
static const StringHash WINDOW_EQUIPMENT("EquipmentWindow");
static const StringHash WINDOW_HEALTHBAR("HealthBarWindow");
static const StringHash WINDOW_ENERGYBAR("EnergyBarWindow");
static const StringHash WINDOW_XPBAR("XPBarWindow");
static const StringHash WINDOW_DAMAGE("DamageWindow");
static const StringHash WINDOW_SCORE_CHART("ScoreChartWindow");

class WindowManager : public Object
{
    URHO3D_OBJECT(WindowManager, Object)
private:
    HashMap<StringHash, SharedPtr<UIElement>> windows_;
    template <typename D>
    void CloseDialog()
    {
        UIElement* root = GetSubsystem<UI>()->GetRoot();
        DialogWindow* wnd = dynamic_cast<DialogWindow*>(root->GetChild(D::GetTypeNameStatic()));
        if (!wnd)
            return;
        wnd->Close();
    }
public:
    WindowManager(Context* context);
    ~WindowManager() override;

    const HashMap<StringHash, SharedPtr<UIElement>>& GetWindows() const
    {
        return windows_;
    }
    bool IsLoaded(const StringHash& hash) const
    {
        return windows_.Contains(hash);
    }

    SharedPtr<UIElement> GetWindow(const StringHash& hash, bool addToUi = false, bool canCreate = true);
    template<typename T>
    T* GetWindow(const StringHash& hash, bool canCreate = true)
    {
        auto w = GetWindow(hash, false, canCreate);
        if (!w)
            return nullptr;
        return dynamic_cast<T*>(w.Get());
    }
    SharedPtr<DialogWindow> GetDialog(AB::Dialogs dialog, bool canCreate = false);

    void SaveWindows();
};

