/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AddAccountKeyDialog.h"
#include "FwClient.h"
#include "LevelManager.h"
#include "BaseLevel.h"

void AddAccountKeyDialog::RegisterObject(Context* context)
{
    context->RegisterFactory<AddAccountKeyDialog>();
    URHO3D_COPY_BASE_ATTRIBUTES(DialogWindow);
}

AddAccountKeyDialog::AddAccountKeyDialog(Context* context) :
    DialogWindow(context)
{
    SetName("AddAccountKeyDialog");
    LoadLayout("UI/AddAccountKeyWindow.xml");
    SetStyleAuto();

    SetSize(320, 160);
    SetMinSize(320, 160);
    SetLayoutSpacing(10);
    UpdateLayout();
    SetMovable(false);

    MakeModal();
    BringToFront();
    accountKeyEdit_ = GetChildStaticCast<LineEdit>("AccountKeyEdit", true);
    SubscribeEvents();
}

AddAccountKeyDialog::~AddAccountKeyDialog()
{
    UnsubscribeFromAllEvents();
}

void AddAccountKeyDialog::SubscribeEvents()
{
    DialogWindow::SubscribeEvents();
    auto* addButton = GetChildStaticCast<Button>("AddButton", true);
    SubscribeToEvent(addButton, E_RELEASED, URHO3D_HANDLER(AddAccountKeyDialog, HandleAddClicked));
    auto* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(AddAccountKeyDialog, HandleCloseClicked));
}

void AddAccountKeyDialog::HandleAddClicked(StringHash, VariantMap&)
{
    auto* lm = GetSubsystem<LevelManager>();
    auto* level = lm->GetCurrentLevel<BaseLevel>();
    String accKey = accountKeyEdit_->GetText().Trimmed();
    if (accKey.Empty())
    {
        level->ShowError("Please enter or paste an account key");
        return;
    }
    if (accKey.Length() != 36)
    {
        level->ShowError("An account key has exactly 36 characters in the form of XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX");
        return;
    }

    auto* client = GetSubsystem<FwClient>();
    client->AddAccountKey(accKey);
    Close();
}

void AddAccountKeyDialog::HandleCloseClicked(StringHash, VariantMap&)
{
    Close();
}
