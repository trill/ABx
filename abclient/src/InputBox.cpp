/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InputBox.h"

void InputBox::RegisterObject(Context* context)
{
    context->RegisterFactory<InputBox>();
    URHO3D_COPY_BASE_ATTRIBUTES(DialogWindow);
}

InputBox::InputBox(Context* context) :
    DialogWindow(context)
{
}

InputBox::InputBox(Context* context, const String& title) :
    DialogWindow(context)
{
    LoadLayout("UI/InputBox.xml");
    SetStyleAuto();

    SetSize(360, 160);
    SetMinSize(360, 160);
    SetMaxSize(360, 160);
    SetLayoutSpacing(10);
    SetLayoutBorder({ 10, 10, 10, 10 });
    SetMovable(false);

    auto* caption = GetChildDynamicCast<Text>("Caption", true);
    caption->SetText(title);

    auto* okButton = GetChildDynamicCast<Button>("OkButton", true);
    okButton->SetEnabled(false);
    SubscribeToEvent(okButton, E_RELEASED, URHO3D_HANDLER(InputBox, HandleOkClicked));
    auto* cancelButton = GetChildDynamicCast<Button>("CancelButton", true);
    SubscribeToEvent(cancelButton, E_RELEASED, URHO3D_HANDLER(InputBox, HandleCancelClicked));
    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    SubscribeToEvent(edit, E_TEXTFINISHED, URHO3D_HANDLER(InputBox, HandleEditTextFinished));
    SubscribeToEvent(edit, E_TEXTCHANGED, URHO3D_HANDLER(InputBox, HandleEditTextChanged));

    MakeModal();
    Center();
    DialogWindow::SubscribeEvents();
    edit->SetFocus(true);
}

InputBox::~InputBox()
{
}

void InputBox::SelectAll()
{
    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    edit->GetTextElement()->SetSelection(0);
}

void InputBox::SetValue(const String& value)
{
    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    edit->SetText(value);
}

const String& InputBox::GetValue() const
{
    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    return edit->GetText();
}

void InputBox::HandleOkClicked(StringHash, VariantMap&)
{
    using namespace InputBoxDone;

    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    const String value = edit->GetText();
    if (value.Empty())
        return;

    VariantMap& newEventData = GetEventDataMap();
    newEventData[P_OK] = true;
    newEventData[P_VALUE] = value;
    newEventData[P_ELEMENT] = this;
    SendEvent(E_INPUTBOXDONE, newEventData);
    Close();
}

void InputBox::HandleCancelClicked(StringHash, VariantMap&)
{
    using namespace InputBoxDone;
    VariantMap& newEventData = GetEventDataMap();
    newEventData[P_OK] = false;
    newEventData[P_VALUE] = "";
    newEventData[P_ELEMENT] = this;
    SendEvent(E_INPUTBOXDONE, newEventData);
    Close();
}

void InputBox::HandleEditTextFinished(StringHash, VariantMap&)
{
    using namespace InputBoxDone;

    auto* edit = GetChildDynamicCast<LineEdit>("InputEdit", true);
    const String value = edit->GetText();
    if (value.Empty())
        return;

    VariantMap& newEventData = GetEventDataMap();
    newEventData[P_OK] = true;
    newEventData[P_VALUE] = value;
    newEventData[P_ELEMENT] = this;
    SendEvent(E_INPUTBOXDONE, newEventData);
    Close();
}

void InputBox::HandleEditTextChanged(StringHash, VariantMap& eventData)
{
    using namespace TextChanged;
    auto* okButton = GetChildDynamicCast<Button>("OkButton", true);
    okButton->SetEnabled(!eventData[P_TEXT].GetString().Empty());
}
