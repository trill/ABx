/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Conversions.h"

String ToUrhoString(const std::string& value)
{
    return String(value.data(), static_cast<unsigned>(value.length()));
}

std::string ToStdString(const String& value)
{
    return std::string(value.CString(), static_cast<size_t>(value.Length()));
}
