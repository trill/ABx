/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <AB/Entities/Profession.h>
#include "Skill.h"
#include "Effect.h"
#include <AB/Entities/Attribute.h>
#include <Urho3DAll.h>
#include <sa/Iteration.h>

class SkillManager : public Object
{
    URHO3D_OBJECT(SkillManager, Object)
public:
    SkillManager(Context* context);
    ~SkillManager() override;

    const std::map<std::string, AB::Entities::Profession>& GetProfessions() const
    {
        return professions_;
    }
    AB::Entities::Profession* GetProfessionByIndex(uint32_t index)
    {
        auto it = std::find_if(professions_.begin(), professions_.end(), [&index](const auto& current) -> bool
        {
            return current.second.index == index;
        });
        if (it != professions_.end())
            return &(*it).second;
        static AB::Entities::Profession EMPTY;
        return &EMPTY;
    }
    AB::Entities::Profession* GetProfession(const String& uuid);
    AB::Entities::AttriInfo* GetAttrInfo(AB::Entities::Profession& prof, const String& uuid);
    const AB::Entities::Attribute* GetAttributeByIndex(uint32_t index) const
    {
        auto it = attributes_.find(index);
        if (it == attributes_.end())
            return nullptr;
        return &(*it).second;
    }

    const std::map<uint32_t, Skill>& GetSkills() const { return skills_; }
    const Skill* GetSkillByIndex(uint32_t index) const
    {
        auto it = skills_.find(index);
        if (it == skills_.end())
            return nullptr;
        return &(*it).second;
    }
    const Effect* GetEffectByIndex(uint32_t index) const
    {
        auto it = effects_.find(index);
        if (it == effects_.end())
            return nullptr;
        return &(*it).second;
    }

    template <typename Callable>
    void VisistSkillsByProfession(const std::string& profUuid, const Callable& func)
    {
        std::vector<const Skill*> sorted;
        for (const auto& s : skills_)
        {
            if (s.second.index != 0 && s.second.professionUuid.compare(profUuid) == 0)
                sorted.push_back(&s.second);
        }
        std::sort(sorted.begin(), sorted.end(), [this](const auto* lhs, const auto* rhs)
        {
            const AB::Entities::Attribute* a1 = GetAttribute(lhs->attributeUuid);
            const AB::Entities::Attribute* a2 = GetAttribute(rhs->attributeUuid);
            if (a1 && a2)
            {
                int c = a1->name.compare(a2->name);
                if (c != 0)
                    return c < 0;
            }
            return lhs->name.compare(rhs->name) < 0;
        });
        for (const auto& s : sorted)
        {
            if (func(*s) == Iteration::Break)
                break;
        }
    }
    const AB::Entities::Attribute* GetAttribute(const std::string& uuid) const;

    std::map<uint32_t, Skill> skills_;
    std::map<std::string, AB::Entities::Profession> professions_;
    std::map<uint32_t, AB::Entities::Attribute> attributes_;
    std::map<uint32_t, Effect> effects_;
};
