/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "TabGroup.h"
#include <Urho3DAll.h>

class ActorXPBar;

class HeroWindow : public Window
{
    URHO3D_OBJECT(HeroWindow, Window)
private:
    SharedPtr<TabGroup> tabgroup_;
    SharedPtr<ActorXPBar> xpBar_;
    void SubscribeEvents();
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleLevelReady(StringHash eventType, VariantMap& eventData);
    void HandleObjectProgress(StringHash eventType, VariantMap& eventData);
    void LoadWindow(Window* wnd, const String& fileName);
    TabElement* CreateTab(TabGroup* tabs, const String& page);
    void CreatePageGeneral(TabElement* tabElement);
public:
    static void RegisterObject(Context* context);

    HeroWindow(Context* context);
    ~HeroWindow() override;

    void UpdateAll();
};

