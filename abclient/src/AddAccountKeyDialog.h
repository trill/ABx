/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"

class AddAccountKeyDialog : public DialogWindow
{
    URHO3D_OBJECT(AddAccountKeyDialog, DialogWindow)
private:
    void HandleAddClicked(StringHash eventType, VariantMap& eventData);
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
protected:
    void SubscribeEvents();
public:
    static void RegisterObject(Context* context);
    AddAccountKeyDialog(Context* context);
    ~AddAccountKeyDialog() override;

    SharedPtr<LineEdit> accountKeyEdit_;
};

