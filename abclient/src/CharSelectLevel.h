/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BaseLevel.h"
#include "AddAccountKeyDialog.h"

/// Character select
class CharSelectLevel final : public BaseLevel
{
    URHO3D_OBJECT(CharSelectLevel, BaseLevel)
public:
    CharSelectLevel(Context* context);
    void CreateCamera();
    SharedPtr<Window> characterWindow_;
protected:
    void SubscribeToEvents() override;
    void CreateUI() override;
    void SceneLoadingFinished() override;
private:
    void CreateScene() override;
    void HandleCharClicked(StringHash eventType, VariantMap& eventData);
    void HandleDeleteCharClicked(StringHash eventType, VariantMap& eventData);
    void HandleCreateCharClicked(StringHash eventType, VariantMap& eventData);
    void HandleBackClicked(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleAddAccountKeyClicked(StringHash eventType, VariantMap& eventData);
    void HandleOptionsClicked(StringHash eventType, VariantMap& eventData);
    void HandleAccountKeyAdded(StringHash eventType, VariantMap& eventData);
    void HandleCharacterDeleted(StringHash eventType, VariantMap& eventData);
    void EnableButtons(bool enable);
};

