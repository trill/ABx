/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "WorldLevel.h"

/// No combat area
class OutpostLevel : public WorldLevel
{
    URHO3D_OBJECT(OutpostLevel, BaseLevel)
public:
    OutpostLevel(Context* context);
protected:
    void SubscribeToEvents() override;
    void CreateUI() override;
    void SceneLoadingFinished() override;
private:
};

