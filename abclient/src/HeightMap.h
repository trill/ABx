/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class HeightMap : public Component
{
    URHO3D_OBJECT(HeightMap, Component)
public:
    static void RegisterObject(Context* context);

    HeightMap(Context* context);
    ~HeightMap() override;

    void LoadHeightmap(JSONFile* file);
    float GetRawHeight(int x, int z) const;
    Vector3 GetRawNormal(int x, int z) const;
    /// Return height at world coordinates.
    float GetHeight(const Vector3& world) const;
    Vector3 GetNormal(const Vector3& world) const;
    IntVector2 WorldToHeightmap(const Vector3& world);
    Vector3 HeightmapToWorld(const IntVector2& pixel);

    Vector3 spacing_;
private:
    Matrix4 matrix_ = Matrix4::IDENTITY;
    Matrix4 inverseMatrix_;

    int32_t patchSize_;
    float minHeight_;
    float maxHeight_;
    IntVector2 numVertices_;
    IntVector2 numPatches_;
    Vector2 patchWorldSize_;
    Vector2 patchWorldOrigin_;

    SharedArrayPtr<float> heightData_;
};

