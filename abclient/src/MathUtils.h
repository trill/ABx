/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

inline void NormalizeAngle(float& angle)
{
    angle = fmodf(angle, 360.0f);
    angle = fmodf(angle + 360.0f, 360.0f);
}

inline float NormalizedAngle(float angle)
{
    float result = angle;
    NormalizeAngle(result);
    return result;
}

template <typename T>
inline T DegToRad(T deg)
{
    return -deg * (static_cast<T>(M_PI / 180.0));
}

template <typename T>
inline T RadToDeg(T rad)
{
    return (-rad / static_cast<T>(M_PI)) * (static_cast<T>(180.0));
}
