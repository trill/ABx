/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ValueBar.h"
#include "Actor.h"
#include <Urho3DAll.h>

class HealthBar : public ValueBar
{
    URHO3D_OBJECT(HealthBar, ValueBar)
private:
    WeakPtr<Actor> actor_;
    SharedPtr<UIElement> container_;
    SharedPtr<BorderImage> condition_;
    SharedPtr<BorderImage> ench_;
    SharedPtr<BorderImage> hex_;
    bool showName_;
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleActorSkillsChanged(StringHash eventType, VariantMap& eventData);
    void HandleObjectProgress(StringHash eventType, VariantMap& eventData);
    void UpdateEffects(const Actor& actor);
public:
    static void RegisterObject(Context* context);

    HealthBar(Context* context);
    ~HealthBar() override;

    void SetActor(SharedPtr<Actor> actor);
    SharedPtr<Actor> GetActor();
    void SetShowName(bool value);
    bool showEffects_{ false };
};

