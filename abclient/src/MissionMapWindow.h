/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <AB/Entities/Game.h>
#include <sa/bitmap.h>
#include <libshared/Team.h>

class Player;
class GameObject;

class MissionMapWindow : public Window
{
    URHO3D_OBJECT(MissionMapWindow, Window)
public:
    static void RegisterObject(Context* context);

    MissionMapWindow(Context* context);
    ~MissionMapWindow() override;
    void SetScene(const SharedPtr<Scene>& scene, AB::Entities::GameType gameType, const String& sceneFile);
private:
    enum class DotType
    {
        Self,
        Ally,
        Foe,
        Other,
        Waypoint,
        PingPos,
        Marker,
        Portal,
        AccountChest,
        ResShrine
    };
    enum class PingType
    {
        None,
        Position,
        Target
    };
    static String GetMinimapFile(const String& scene);
    int64_t pingTime_{ 0 };
    uint32_t pingerId_{ 0 };
    Vector3 pingPos_;
    PingType pingType_{ PingType::None };
    WeakPtr<GameObject> target_;

    SharedPtr<Texture2D> mapTexture_;
    SharedPtr<Image> mapImage_;
    sa::Bitmap mapBitmap_;
    SharedPtr<Texture2D> heightmapTexture_;
    SharedPtr<BorderImage> terrainLayer_;
    SharedPtr<BorderImage> objectLayer_;
    Vector<Vector3> waypoints_;
    Vector3 marker_;
    bool haveMarker_{ false };
    Vector3 terrainSpacing_;
    Vector3 terrainWorldSize_;
    Vector2 terrainScaling_;
    AB::Entities::GameType gameType_{ AB::Entities::GameTypeUnknown };
    void FitTexture();
    Player* GetPlayer() const;
    void DrawObjects();
    void DrawRanges();
    void SubscribeToEvents();
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleResized(StringHash eventType, VariantMap& eventData);
    void HandlePositionPinged(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void HandleTargetPinged(StringHash eventType, VariantMap& eventData);
    IntVector2 WorldToMapPos(const Vector3& center, const Vector3& world) const;
    IntVector2 WorldToMap(const Vector3& world) const;
    Vector3 MapToWorldPos(const Vector3& center, const IntVector2& map) const;
    Vector3 MapToWorld(const IntVector2& map) const;
    void DrawObject(const IntVector2& pos, DotType type, bool isSelected, bool isDead, Game::TeamColor teamColor = Game::TeamColor::Default);
    void DrawCircle(const IntVector2& center, float radius, const sa::Color& color, int thickness = 2);
    void DrawCircle(const IntVector2& center, float radius, const sa::Color& color, float scale, int thickness = 2);
    void DrawPoint(const IntVector2& center, int size, const sa::Color& color);
};

