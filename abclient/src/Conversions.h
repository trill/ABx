/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <Urho3DAll.h>

String ToUrhoString(const std::string& value);
std::string ToStdString(const String& value);
