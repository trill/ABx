/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <stdint.h>

class PriceUIElement final : public UIElement
{
    URHO3D_OBJECT(PriceUIElement, UIElement)
private:
    unsigned itemCount_{ 0 };
public:
    static void RegisterObject(Context* context);

    PriceUIElement(Context* context);
    ~PriceUIElement() override;

    void Add(uint32_t index, uint32_t count);
};
