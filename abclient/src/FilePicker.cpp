/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FilePicker.h"
#include <AB/CommonConfig.h>

static bool CompareEntries(const FileSelectorEntry& lhs, const FileSelectorEntry& rhs)
{
    if (lhs.directory_ && !rhs.directory_)
        return true;
    if (!lhs.directory_ && rhs.directory_)
        return false;
#if defined(AB_WINDOWS)
    return lhs.name_.Compare(rhs.name_, false) < 0;
#elif defined(AB_UNIX)
    return lhs.name_.Compare(rhs.name_, true) < 0;
#endif
}

void FilePicker::RegisterObject(Context* context)
{
    context->RegisterFactory<FilePicker>();
    URHO3D_COPY_BASE_ATTRIBUTES(DialogWindow);
}

FilePicker::FilePicker(Context* context) :
    DialogWindow(context)
{ }

FilePicker::FilePicker(Context* context, const String& root, Mode mode) :
    DialogWindow(context),
    root_(AddTrailingSlash(root)),
    path_(root_),
    mode_(mode)
{
    LoadLayout("UI/FilePicker.xml");
    SetStyleAuto();

    SetSize(430, 400);
    SetMinSize(430, 400);
    SetMaxSize(430, 400);
    SetMovable(true);

    auto* caption = GetChildDynamicCast<Text>("Caption", true);
    if (mode_ == Mode::Load)
        caption->SetText("Load file");
    else
        caption->SetText("Save file");

    DialogWindow::SubscribeEvents();

    auto* newFolder = GetChildDynamicCast<Button>("NewFolderButton", true);
    newFolder->SetVisible(false);

    auto* okButton = GetChildDynamicCast<Button>("OkButton", true);
    SubscribeToEvent(okButton, E_RELEASED, URHO3D_HANDLER(FilePicker, HandleOkClicked));
    auto* cancelButton = GetChildDynamicCast<Button>("CancelButton", true);
    SubscribeToEvent(cancelButton, E_RELEASED, URHO3D_HANDLER(FilePicker, HandleCancelClicked));

    fileList_ = GetChildDynamicCast<ListView>("FilesList", true);
    SubscribeToEvent(fileList_, E_ITEMSELECTED, URHO3D_HANDLER(FilePicker, HandleFileSelected));
    SubscribeToEvent(fileList_, E_ITEMDOUBLECLICKED, URHO3D_HANDLER(FilePicker, HandleFileDoubleClicked));
    fileNameEdit_ = GetChildDynamicCast<LineEdit>("FilenameEdit", true);
    SetVisible(true);
    MakeModal();
    Center();
    SetPath(root_);
    fileNameEdit_->SetFocus(true);
}

void FilePicker::SetPath(const String& path)
{
    path_ = path;
    ScanPath();
}

void FilePicker::HandleCancelClicked(StringHash, VariantMap&)
{
    Close();
}

void FilePicker::HandleOkClicked(StringHash, VariantMap&)
{
    if (mode_ == Mode::Save)
    {
        const String& fn = fileNameEdit_->GetText();
        if (!fn.Empty())
        {
            String fileName = AddTrailingSlash(path_) + fn;
            using namespace FilePicked;

            VariantMap& eventData = GetEventDataMap();
            eventData[P_FILENAME] = fileName;
            SendEvent(E_FILEPICKED, eventData);
            Close();
            return;
        }
    }
    EnterFile();
}

void FilePicker::HandleFileSelected(StringHash, VariantMap&)
{
    int index = fileList_->GetSelection();
    if (index >= fileEntries_.Size())
        return;
    if (!fileEntries_[index].directory_)
        fileNameEdit_->SetText(fileEntries_[index].name_);
}

void FilePicker::HandleFileDoubleClicked(StringHash, VariantMap& eventData)
{
    if (eventData[ItemDoubleClicked::P_BUTTON] == MOUSEB_LEFT)
        EnterFile();
}

void FilePicker::ScanPath()
{
    auto* fileSystem = GetSubsystem<FileSystem>();

    fileList_->RemoveAllItems();
    fileEntries_.Clear();

    Vector<String> directories;
    Vector<String> files;
    fileSystem->ScanDir(directories, path_, "*", SCAN_DIRS, false);
    fileSystem->ScanDir(files, path_, "*", SCAN_FILES, false);

    for (int i = 0; i < directories.Size(); ++i)
    {
        FileSelectorEntry newEntry;
        if (directories[i] == ".")
            continue;
        if (directories[i] == "..")
        {
#if defined(AB_WINDOWS)
            if (path_.Compare(root_, false) == 0)
                continue;
#elif defined(AB_UNIX)
            if (path_.Compare(root_, true) == 0)
                continue;
#endif
        }
        newEntry.name_ = directories[i];
        newEntry.directory_ = true;
        fileEntries_.Push(newEntry);
    }

    for (int i = 0; i < files.Size(); ++i)
    {
        FileSelectorEntry newEntry = {};
        newEntry.name_ = files[i];
        newEntry.directory_ = false;
        fileEntries_.Push(newEntry);
    }

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_GCC("-Wmaybe-uninitialized")
    Sort(fileEntries_.Begin(), fileEntries_.End(), CompareEntries);
PRAGMA_WARNING_POP

    UIElement* listContent = fileList_->GetContentElement();
    listContent->DisableLayoutUpdate();
    for (int i = 0; i < fileEntries_.Size(); ++i)
    {
        String displayName;
        if (fileEntries_[i].directory_)
            displayName = "<DIR> " + fileEntries_[i].name_;
        else
            displayName = fileEntries_[i].name_;

        Text* entryText = fileList_->CreateChild<Text>();
        fileList_->AddItem(entryText);
        entryText->SetText(displayName);
        entryText->SetStyle("FileSelectorListText");
    }
    listContent->EnableLayoutUpdate();
    listContent->UpdateLayout();
}

void FilePicker::EnterFile()
{
    int index = fileList_->GetSelection();
    if (index >= fileEntries_.Size())
        return;

    if (fileEntries_[index].directory_)
    {
        // If a directory double clicked, enter it. Recognize . and .. as a special case
        const String& newPath = fileEntries_[index].name_;
        if ((newPath != ".") && (newPath != ".."))
            SetPath(path_ + newPath);
        else if (newPath == "..")
        {
            String parentPath = GetParentPath(path_);
            SetPath(parentPath);
        }

        return;
    }

    using namespace FilePicked;

    VariantMap& eventData = GetEventDataMap();
    eventData[P_FILENAME] = path_ + fileEntries_[index].name_;
    SendEvent(E_FILEPICKED, eventData);
    Close();
}
