/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"

URHO3D_EVENT(E_FILEPICKED, FilePicked)
{
    URHO3D_PARAM(P_FILENAME, FileName);              // String
}

class FilePicker : public DialogWindow
{
    URHO3D_OBJECT(FilePicker, DialogWindow)
public:
    static void RegisterObject(Context* context);
    enum class Mode
    {
        Save,
        Load
    };
    FilePicker(Context* context);
    FilePicker(Context* context, const String& root, Mode mode);
    void SetPath(const String& path);
private:
    void HandleCancelClicked(StringHash eventType, VariantMap& eventData);
    void HandleOkClicked(StringHash eventType, VariantMap& eventData);
    void HandleFileSelected(StringHash eventType, VariantMap& eventData);
    void HandleFileDoubleClicked(StringHash eventType, VariantMap& eventData);
    void ScanPath();
    void EnterFile();
    String root_;
    String path_;
    Mode mode_;
    ListView* fileList_{ nullptr };
    LineEdit* fileNameEdit_{ nullptr };
    Vector<FileSelectorEntry> fileEntries_;
};
