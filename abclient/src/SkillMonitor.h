/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "Actor.h"

class ValueBar;

class SkillMonitorItem final : public UIElement
{
    URHO3D_OBJECT(SkillMonitorItem, UIElement)
private:
    SharedPtr<ValueBar> progress_;
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    static const int ICON_SIZE = 32;
    SkillMonitorItem(Context* context);
    ~SkillMonitorItem() override;
    bool Initialize();
    // Skill index
    uint32_t index_{ 0 };
    int64_t startTick_{ 0 };
    uint32_t activation_{ 0 };
    int64_t doneSince_{ 0 };
    bool isUsing_{ false };
    bool success_{ false };
};

class SkillMonitor : public Window
{
    URHO3D_OBJECT(SkillMonitor, Window)
private:
    WeakPtr<Actor> target_;
    void HandleObjectUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectEndUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectSkillFailure(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void SkillStopped(uint32_t index, bool success);
    SkillMonitorItem* FindItem(uint32_t index);
public:
    static const int KEEP_ITEMS_MS = 5000;
    static void RegisterObject(Context* context);

    SkillMonitor(Context* context);
    ~SkillMonitor() override;
    void SetTarget(SharedPtr<Actor> target);
};

