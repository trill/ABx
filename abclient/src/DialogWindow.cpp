/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DialogWindow.h"
#include "UIPriorities.h"

static unsigned backdropCount = 0;

void DialogWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<DialogWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

DialogWindow::DialogWindow(Context* context) :
    Window(context)
{
    UI* ui = GetSubsystem<UI>();
    uiRoot_ = ui->GetRoot();
    uiRoot_->AddChild(this);
    SetFocusMode(FM_FOCUSABLE);
}

DialogWindow::~DialogWindow()
{
    UnsubscribeFromAllEvents();
}

void DialogWindow::LoadWindow(Window* wnd, const String& fileName)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* xml = cache->GetResource<XMLFile>(fileName);
    wnd->LoadXML(xml->GetRoot());
}

void DialogWindow::Close()
{
    using namespace DialogClose;

    VariantMap& eventData = GetEventDataMap();
    eventData[P_ELEMENT] = this;
    SendEvent(E_DIALOGCLOSE, eventData);

    SetVisible(false);
    if (overlay_)
    {
        overlay_->Remove();
        overlay_.Reset();
        --backdropCount;
    }
    this->Remove();
}

void DialogWindow::SubscribeEvents()
{
    Button* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    if (closeButton)
        SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(DialogWindow, HandleCloseClicked));
}

void DialogWindow::LoadLayout(const String& fileName)
{
    LoadWindow(this, fileName);
}

void DialogWindow::Center()
{
    auto* graphics = GetSubsystem<Graphics>();
    SetPosition((graphics->GetWidth() / 2) - (GetWidth() / 2), (graphics->GetHeight() / 2) - (GetHeight() / 2));
}

void DialogWindow::HandleCloseClicked(StringHash, VariantMap&)
{
    Close();
}

void DialogWindow::MakeModal()
{
    // Ugly Hack to make a pseudo modal window :/
    if (!overlay_)
    {
        auto* ui = GetSubsystem<UI>();
        UIElement* root = ui->GetRoot();

        overlay_ = root->CreateChild<Window>();

        ++backdropCount;
        overlay_->SetSize(M_MAX_INT, M_MAX_INT);
        overlay_->SetLayoutMode(LM_FREE);
        overlay_->SetAlignment(HA_LEFT, VA_TOP);
        // Black color
        overlay_->SetColor(Color(0.0f, 0.0f, 0.0f, 1.0f));
        overlay_->SetOpacity(0.5f);
        overlay_->SetPriority(Priorities::ModalBackdrop + backdropCount);
        // Make it top most
        overlay_->SetBringToBack(false);
    }
    this->SetPriority(overlay_->GetPriority() + 1);
    this->SetBringToBack(false);
}
