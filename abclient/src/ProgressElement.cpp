/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProgressElement.h"

static constexpr sa::Color PROGRESS_COLOR = { 0, 0, 0, 160 };
static constexpr int HEIGHT = 10;

void ProgressElement::RegisterObject(Context* context)
{
    context->RegisterFactory<ProgressElement>();
    URHO3D_COPY_BASE_ATTRIBUTES(BorderImage);
}

ProgressElement::ProgressElement(Context* context) :
    BorderImage(context)
{
    texture_ = MakeShared<Texture2D>(context_);
    texture_->SetSize(32, HEIGHT, Graphics::GetRGBAFormat(), TEXTURE_DYNAMIC);
    texture_->SetNumLevels(1);
    texture_->SetMipsToSkip(QUALITY_LOW, 0);
    image_ = MakeShared<Image>(context_);
    image_->SetSize(32, HEIGHT, 4);
    bitmap_.SetBitmap(32, HEIGHT, 4, image_->GetData());
    SetTexture(texture_);
    SetFullImageRect();
    SubscribeToEvent(E_RENDERUPDATE, URHO3D_HANDLER(ProgressElement, HandleRenderUpdate));
    SetVisible(false);
}

ProgressElement::~ProgressElement()
{
}

void ProgressElement::Start()
{
    if (mode_ == Mode::Inc)
        current_ = 0.0f;
    else
        current_ = max_;
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ProgressElement, HandleUpdate));
    SetVisible(true);
}

void ProgressElement::Stop()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ProgressElement, HandleUpdate));
    if (mode_ == Mode::Inc)
        current_ = 0.0f;
    else
        current_ = max_;
    current_ = 0.0f;
    SetVisible(false);
}

void ProgressElement::SetMaxTicks(uint32_t ticks)
{
    max_ = (float)ticks / 1000.0f;
}

void ProgressElement::HandleUpdate(StringHash, VariantMap& eventData)
{
    using namespace Update;
    if (mode_ == Mode::Inc)
    {
        current_ += eventData[P_TIMESTEP].GetFloat();
        if (current_ >= max_)
            Stop();
    }
    else
    {
        current_ -= eventData[P_TIMESTEP].GetFloat();
        if (current_ <= 0.0f)
            Stop();
    }
}

void ProgressElement::HandleRenderUpdate(StringHash, VariantMap&)
{
    if (!IsVisible())
        return;
    bitmap_.Clear();
    DrawProgress();
    texture_->SetData(image_, true);
}

void ProgressElement::DrawProgress()
{
    const float percent = current_ / max_;
    if (direction_ == Direction::Backward)
        bitmap_.DrawRectangle(bitmap_.Width() - (int)(bitmap_.Width() * percent), 0, bitmap_.Width(), bitmap_.Height(), PROGRESS_COLOR);
    else
        bitmap_.DrawRectangle(0, 0, (int)(bitmap_.Width() * percent), bitmap_.Height(), PROGRESS_COLOR);
}
