/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class ChatFilter : public Object
{
    URHO3D_OBJECT(ChatFilter, Object)
private:
    Vector<String> filterPatterns_;
public:
    ChatFilter(Context* context);
    ~ChatFilter() override;
    void Load();
    bool Matches(const String& value);
};

