/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <stdint.h>
#include "Skill.h"

class SkillCostElement : public UIElement
{
    URHO3D_OBJECT(SkillCostElement, UIElement)
private:
    void CreateElements(const Skill& skill);
    static String GetValueText(float value);
public:
    static void RegisterObject(Context* context);

    SkillCostElement(Context* context);
    ~SkillCostElement() override;

    void SetSkill(const Skill& skill);
};
