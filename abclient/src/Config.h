/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#undef URHO3D_ANGELSCRIPT
#undef URHO3D_DATABASE
#undef URHO3D_LUA
#undef URHO3D_NETWORK

#define MAX_CHAT_MESSAGE 120

#include <AB/CommonConfig.h>
