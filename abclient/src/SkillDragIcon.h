/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3D/Urho3DAll.h>

class SkillDragIcon : public Window
{
    URHO3D_OBJECT(SkillDragIcon, Window)
public:
    static void RegisterObject(Context* context);

    SkillDragIcon(Context* context);
    ~SkillDragIcon() override;
    void SetSkill(const UIElement& elem, Texture* tex);
};

