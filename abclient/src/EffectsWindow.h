/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class EffectsWindow : public UIElement
{
    URHO3D_OBJECT(EffectsWindow, UIElement)
private:
    unsigned effectCount_;
    void UpdatePosition();
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void HandleObjectResourceChange(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);

    EffectsWindow(Context* context);
    ~EffectsWindow() override;

    void EffectAdded(uint32_t sourceId, uint32_t effectIndex, uint32_t ticks);
    void EffectRemoved(uint32_t effectIndex);
    void Clear();
};

