/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "Effect.h"

class EffectTooltip : public ToolTip
{
    URHO3D_OBJECT(EffectTooltip, ToolTip)
private:
    SharedPtr<Window> window_;
    SharedPtr<Text> effectName_;
    SharedPtr<Text> effectDescription_;
    SharedPtr<Text> effectCategory_;
    const Effect* effect_{ nullptr };
public:
    static void RegisterObject(Context* context);
    EffectTooltip(Context* context);
    ~EffectTooltip() override;
    void SetEffect(const Effect* effect);
};

