/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ValueBar.h"

void ValueBar::RegisterObject(Context* context)
{
    context->RegisterFactory<ValueBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(ProgressBar);
}

ValueBar::ValueBar(Context* context) :
    ProgressBar(context)
{
    SetRange(0.1f);
    SetValue(0.1f);
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());

    Texture2D* tex = cache->GetResource<Texture2D>("Textures/Fw-UI-Ex.png");
    SetTexture(tex);
    SetImageRect({ 0, 16, 16, 32 });
    SetBorder(IntRect(4, 4, 4, 4));

    text_ = CreateChild<Text>();
    text_->SetInternal(true);
    text_->SetAlignment(HA_CENTER, VA_CENTER);
    text_->SetStyleAuto();
    text_->SetFontSize(10);
    text_->SetVisible(false);

    SetShowPercentText(false);
    SetEnabled(true);
}

void ValueBar::GetBatches(Vector<UIBatch>& batches, Vector<float>& vertexData, const IntRect& currentScissor)
{
    static const IntVector2 selOffset(0, 16);
    if (selectable_)
        return ProgressBar::GetBatches(batches, vertexData, currentScissor, selected_ ? selOffset : IntVector2::ZERO);

    return ProgressBar::GetBatches(batches, vertexData, currentScissor, IntVector2::ZERO);
}

void ValueBar::UpdateKnob()
{
    knob_->SetVisible(!Equals(value_, 0.0f));
}

void ValueBar::SetValues(unsigned max, unsigned value)
{
    SetRange(static_cast<float>(max));
    SetValue(static_cast<float>(value));
    UpdateKnob();
}

void ValueBar::SetText(const String& value)
{
    text_->SetText(value);
}

void ValueBar::SetShowText(bool value)
{
    text_->SetVisible(value);
}
