/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"
#include "AB/Entities/ConcreteItem.h"
#include "FwClient.h"

class CraftsmanWindow final : public DialogWindow
{
    URHO3D_OBJECT(CraftsmanWindow, DialogWindow)
private:
    uint32_t npcId_{ 0 };
    SharedPtr<ListView> items_;
    SharedPtr<DropDownList> itemTypesList_;
    SharedPtr<LineEdit> searchNameEdit_;
    SharedPtr<Text> currentPage_;
    SharedPtr<DropDownList> attribDropdown_;
    SharedPtr<UIElement> pagingContainer_;
    void CreateUI();
    void SubscribeEvents();
    void UpdateList();
    void RequestList(uint32_t page = 1, bool keepScrollPos = false);
    AB::Entities::ItemType GetSelectedItemType() const;
    uint32_t GetSelectedItemIndex() const;
    UISelectable* CreateItem(const ConcreteItem& iItem);
    void PopulateItemTypes();
    void UpdateAttributeList();
    uint32_t GetSelectedAttributeIndex() const;
    void HandleCraftsmanItems(StringHash eventType, VariantMap& eventData);
    void HandleDoItClicked(StringHash eventType, VariantMap& eventData);
    void HandleGoodByeClicked(StringHash eventType, VariantMap& eventData);
    void HandleItemTypeSelected(StringHash eventType, VariantMap& eventData);
    void HandleItemSelected(StringHash eventType, VariantMap& eventData);
    void HandleSearchItemEditTextFinished(StringHash eventType, VariantMap& eventData);
    void HandlePrevPageButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleNextPageButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleFirstPageButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleLastPageButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleSearchButtonClicked(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    CraftsmanWindow(Context* context);
    ~CraftsmanWindow() override;
    void Initialize(uint32_t npcId) override;
    void Clear();
};

