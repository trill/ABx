/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MailWindow.h"
#include "Shortcuts.h"
#include "FwClient.h"
#include "Conversions.h"
#include "ItemsCache.h"
#include "UIPriorities.h"
#include "WindowManager.h"
#include "InventoryWindow.h"
#include <sa/Compiler.h>
#include <sa/TemplateParser.h>
#include <sa/time.h>

void MailWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<MailWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

MailWindow::MailWindow(Context* context) :
    Window(context)
{
    SetName("MailWindow");

    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* file = cache->GetResource<XMLFile>("UI/MailWindow.xml");
    LoadXML(file->GetRoot());

    mailList_ = GetChildStaticCast<ListView>("MailList", true);

    Shortcuts* scs = GetSubsystem<Shortcuts>();
    Text* caption = GetChildStaticCast<Text>("CaptionText", true);
    caption->SetText(scs->GetCaption(Events::E_SC_TOGGLEMAILWINDOW, "Mail", true));

    SetSize(500, 400);
    auto* graphics = GetSubsystem<Graphics>();
    SetPosition(graphics->GetWidth() - GetWidth() - 5, graphics->GetHeight() / 2 - (GetHeight() / 2));

    UIElement* container = GetChild("EditorContainer", true);
    mailBody_ = container->CreateChild<MultiLineEdit>();
    mailBody_->SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    mailBody_->SetStyle("MultiLineEdit");
    mailBody_->SetEditable(false);
    mailBody_->SetMultiLine(true);
    mailBody_->SetTextCopyable(true);
    mailBody_->SetTextSelectable(true);
    mailBody_->SetPosition(0, 0);
    mailBody_->SetClipBorder({ 4, 4, 4, 4 });
    mailBody_->SetSize(container->GetSize());

    SetBringToBack(true);
    SetBringToFront(true);
    SetPriority(Priorities::StandardWindow);
    SetStyleAuto();

    UpdateLayout();

    SubscribeToEvents();

    Button* delButton = GetChildStaticCast<Button>("DeleteMailButton", true);
    delButton->SetDisabledOffset({ 0, 0 });
    delButton->SetEnabled(false);
    Button* repButton = GetChildStaticCast<Button>("ReplyMailButton", true);
    repButton->SetDisabledOffset({ 0, 0 });
    repButton->SetEnabled(false);
}

void MailWindow::GetHeaders()
{
    FwClient* net = GetSubsystem<FwClient>();
    net->GetMailHeaders();
}

void MailWindow::FocusMainElement()
{
    mailList_->SetFocus(true);
}

void MailWindow::SubscribeToEvents()
{
    Button* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(MailWindow, HandleCloseClicked));
    SubscribeToEvent(Events::E_MAILINBOX, URHO3D_HANDLER(MailWindow, HandleMailInboxMessage));
    SubscribeToEvent(Events::E_MAILREAD, URHO3D_HANDLER(MailWindow, HandleMailReadMessage));
    Button* newButton = GetChildStaticCast<Button>("NewMailButton", true);
    SubscribeToEvent(newButton, E_RELEASED, URHO3D_HANDLER(MailWindow, HandleNewClicked));
    Button* deleteButton = GetChildStaticCast<Button>("DeleteMailButton", true);
    SubscribeToEvent(deleteButton, E_RELEASED, URHO3D_HANDLER(MailWindow, HandleDeleteClicked));
    Button* replyButton = GetChildStaticCast<Button>("ReplyMailButton", true);
    SubscribeToEvent(replyButton, E_RELEASED, URHO3D_HANDLER(MailWindow, HandleReplyClicked));
    SubscribeToEvent(mailList_, E_ITEMSELECTED, URHO3D_HANDLER(MailWindow, HandleItemSelected));
    SubscribeToEvent(mailList_, E_ITEMDESELECTED, URHO3D_HANDLER(MailWindow, HandleItemSelected));
    SubscribeToEvent(Events::E_NEWMAIL, URHO3D_HANDLER(MailWindow, HandleNewMail));
    SubscribeToEvent(Events::E_MAILATTACHMENTDELETE, URHO3D_HANDLER(MailWindow, HandleMailAttachmentDelete));
}

void MailWindow::AddItem(const String& text, const String& style, const AB::Entities::MailHeader& header)
{
    Text* txt = mailList_->CreateChild<Text>();

    txt->SetText(text);
    txt->SetVar("uuid", String(header.uuid.c_str()));
    txt->SetVar("From Name", String(header.fromName.c_str()));
    txt->SetVar("Subject", String(header.subject.c_str()));
    txt->SetStyle(style);
    txt->EnableLayoutUpdate();
    txt->SetWordwrap(false);
    txt->UpdateLayout();
    mailList_->AddItem(txt);
    mailList_->EnableLayoutUpdate();
    mailList_->UpdateLayout();
}

void MailWindow::HandleCloseClicked(StringHash, VariantMap&)
{
    SetVisible(false);
}

void MailWindow::HandleMailInboxMessage(StringHash, VariantMap&)
{
    mailList_->RemoveAllItems();

    FwClient* client = GetSubsystem<FwClient>();
    const std::vector<AB::Entities::MailHeader>& headers = client->GetCurrentMailHeaders();
    static constexpr const char* TEMPLATE = "<${from}> on ${date}: ${subject}";
    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse(TEMPLATE);

    for (const auto& header : headers)
    {
        const std::string t = tokens.ToString([&](const sa::templ::Token& token) -> std::string
        {
            switch (token.type)
            {
            case sa::templ::Token::Type::Variable:
                if (token.value == "from")
                    return header.fromName;
                if (token.value == "date")
                    return sa::time::format_tick(header.created);
                if (token.value == "subject")
                    return header.subject.empty() ? "(No Subject)" : header.subject;
                ASSERT_FALSE();
            default:
                return token.value;
            }
        });
        AddItem(ToUrhoString(t), header.isRead ? "MailListItem" : "MailListItemUnread", header);
    }
}

void MailWindow::HandleMailReadMessage(StringHash, VariantMap&)
{
    FwClient* client = GetSubsystem<FwClient>();
    const AB::Packets::Server::MailComplete mail = client->GetCurrentMail();
    currentMailUuid_ = mail.uuid;
    UIElement* att = GetChild("AttachmentItems", true);
    ClearItems(att);
//    att->SetVisible(mail.attachmentCount != 0);
    if (mail.attachmentCount != 0)
        ShowItems(att, mail.attachments);

    mailBody_->SetText(String(mail.body.c_str()));
}

void MailWindow::ClearItems(UIElement* container)
{
    for (size_t i = 0; i < 7; ++i)
    {
        RemoveItem(container, i);
    }
}

void MailWindow::ShowItems(UIElement* container, const std::vector<AB::Packets::Server::Internal::UpgradeableItem>& items)
{
    if (items.size() > 7)
        // Shouln't happen
        return;

    int index = 0;
    for (const auto& item : items)
    {
        ConcreteItem ci;
        ci.index = item.index;
        ci.count = item.count;
        ci.value = item.value;
        LoadStatsFromString(ci.stats, item.stats);
        ci.flags = item.flags;
        CreateItem(container, index, ci);
        ++index;
    }
}

ItemUIElement* MailWindow::CreateItem(UIElement* container, int index, const ConcreteItem& iItem)
{
    auto* itemsCache = GetSubsystem<ItemsCache>();
    auto item = itemsCache->Get(iItem.index);
    if (!item)
        return nullptr;

    auto* itemContainer = container->GetChild("Item" + String(index + 1), true);
    if (!itemContainer)
        return nullptr;

    ItemUIElement* itemElem = itemContainer->CreateChild<ItemUIElement>("ItemElememt");
    itemElem->SetPosition(4, 4);
    itemElem->SetSize(itemContainer->GetSize() - IntVector2(8, 8));
    itemElem->SetMinSize(itemElem->GetSize());
    itemElem->SetName(item->name_);
    itemElem->SetIcon(item->iconFile_);
    itemElem->SetPos(iItem.pos);
    itemElem->SetIndex(iItem.index);
    itemElem->SetCount(iItem.count);
    itemElem->SetValue(iItem.value);
    itemElem->SetStats(iItem.stats);

    SubscribeToEvent(itemElem, E_DRAGMOVE, URHO3D_HANDLER(MailWindow, HandleItemDragMove));
    SubscribeToEvent(itemElem, E_DRAGBEGIN, URHO3D_HANDLER(MailWindow, HandleItemDragBegin));
    SubscribeToEvent(itemElem, E_DRAGCANCEL, URHO3D_HANDLER(MailWindow, HandleItemDragCancel));
    SubscribeToEvent(itemElem, E_DRAGEND, URHO3D_HANDLER(MailWindow, HandleItemDragEnd));

    return itemElem;
}

bool MailWindow::RemoveItem(UIElement* container, unsigned pos)
{
    auto* item = GetItemFromPos(container, pos);
    if (item == nullptr)
        return false;

    item->RemoveAllChildren();
    return true;
}

UIElement* MailWindow::GetItemFromPos(UIElement* container, unsigned pos)
{
    for (size_t i = 0; i < 7; ++i)
    {
        auto* itemContainer = container->GetChild("Item" + String(i + 1), true);
        if (!itemContainer)
            continue;
        ItemUIElement* icon = itemContainer->GetChildDynamicCast<ItemUIElement>("ItemElememt", true);
        if (!icon)
            continue;
        if (icon->pos_ == static_cast<unsigned>(pos))
            return itemContainer;
    }
    return nullptr;
}

void MailWindow::HandleItemDragBegin(StringHash, VariantMap& eventData)
{
    using namespace DragBegin;
    auto* item = static_cast<ItemUIElement*>(eventData[P_ELEMENT].GetVoidPtr());
    int buttons = eventData[P_BUTTONS].GetInt();
    int lx = eventData[P_X].GetInt();
    int ly = eventData[P_Y].GetInt();
    dragItem_ = item->GetDragItem(buttons, { lx, ly });
}

void MailWindow::HandleItemDragMove(StringHash, VariantMap& eventData)
{
    if (!dragItem_)
        return;
    using namespace DragMove;

    int buttons = eventData[P_BUTTONS].GetInt();
    auto* element = static_cast<ItemUIElement*>(eventData[P_ELEMENT].GetVoidPtr());
    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    int BUTTONS = element->GetVar("BUTTONS").GetInt();

    if (buttons == BUTTONS)
        dragItem_->SetPosition(IntVector2(X, Y) - dragItem_->GetSize() / 2);
}

void MailWindow::HandleItemDragCancel(StringHash, VariantMap&)
{
    using namespace DragCancel;
    if (!dragItem_)
        return;
    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragItem_.Get());
    dragItem_.Reset();
}

void MailWindow::HandleItemDragEnd(StringHash, VariantMap& eventData)
{
    using namespace DragEnd;
    if (!dragItem_)
        return;
    uint16_t pos = static_cast<uint16_t>(dragItem_->GetVar("Pos").GetUInt());

    auto* itemsCache = GetSubsystem<ItemsCache>();
    auto item = itemsCache->Get(dragItem_->GetVar("Index").GetUInt());
    if (!item)
        return;

    ConcreteItem ci;
    ci.pos = pos;
    ci.pos = pos;
    ci.type = item->type_;
    ci.place = AB::Entities::StoragePlace::Mail;
    ci.index = dragItem_->GetVar("Index").GetUInt();
    ci.count = dragItem_->GetVar("Count").GetUInt();
    ci.value = static_cast<uint16_t>(dragItem_->GetVar("Value").GetUInt());
    LoadStatsFromString(ci.stats, dragItem_->GetVar("Stats").GetString());

    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    if (!IsInside({ X, Y }, true))
    {
        WindowManager* wm = GetSubsystem<WindowManager>();
        auto inventory = wm->GetWindow(WINDOW_INVENTORY);
        if (inventory && inventory->IsVisible())
        {
            if (inventory->IsInside({ X, Y }, true))
            {
                auto* client = GetSubsystem<FwClient>();
                client->TakeMailAttachment(currentMailUuid_, pos);
            }
        }
    }

    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragItem_.Get());
    dragItem_.Reset();
}

void MailWindow::HandleNewClicked(StringHash, VariantMap&)
{
    using namespace Events::ToggleNewMailWindow;
    VariantMap& e = GetEventDataMap();
    e[P_RECIPIENT] = String::EMPTY;
    e[P_SUBJECT] = String::EMPTY;
    e[P_BODY] = String::EMPTY;
    SendEvent(Events::E_SC_TOGGLENEWMAILWINDOW, e);
}

void MailWindow::HandleReplyClicked(StringHash, VariantMap&)
{
    Text* sel = dynamic_cast<Text*>(mailList_->GetSelectedItem());
    if (sel)
    {
        String from = sel->GetVar("From Name").GetString();
        String subject = sel->GetVar("Subject").GetString();
        using namespace Events::ReplyMail;
        VariantMap& e = GetEventDataMap();
        e[P_RECIPIENT] = from;
        e[P_SUBJECT] = subject;
        e[P_BODY] = mailBody_->GetText();
        SendEvent(Events::E_SC_REPLYMAIL, e);
    }
}

void MailWindow::HandleDeleteClicked(StringHash, VariantMap&)
{
    Text* sel = dynamic_cast<Text*>(mailList_->GetSelectedItem());
    if (sel)
    {
        String uuid = sel->GetVar("uuid").GetString();
        FwClient* net = GetSubsystem<FwClient>();
        net->DeleteMail(std::string(uuid.CString()));
        net->GetMailHeaders();
        mailBody_->SetText(String::EMPTY);
    }
}

void MailWindow::HandleItemSelected(StringHash, VariantMap&)
{
    Text* sel = dynamic_cast<Text*>(mailList_->GetSelectedItem());
    Button* repButton = GetChildStaticCast<Button>("ReplyMailButton", true);
    Button* delButton = GetChildStaticCast<Button>("DeleteMailButton", true);
    if (sel)
    {
        String uuid = sel->GetVar("uuid").GetString();
        FwClient* net = GetSubsystem<FwClient>();
        net->ReadMail(std::string(uuid.CString()));
        // Mark read
        sel->SetStyle("MailListItem");
        delButton->SetEnabled(true);
        repButton->SetEnabled(true);
    }
    else
    {
        delButton->SetEnabled(false);
        repButton->SetEnabled(false);
    }
}

void MailWindow::HandleItemUnselected(StringHash, VariantMap&)
{
    mailBody_->SetText(String::EMPTY);
    Button* delButton = GetChildStaticCast<Button>("DeleteMailButton", true);
    delButton->SetEnabled(false);
    Button* repButton = GetChildStaticCast<Button>("ReplyMailButton", true);
    repButton->SetEnabled(false);
    UIElement* att = GetChild("AttachmentItems", true);
    ClearItems(att);
}

void MailWindow::HandleNewMail(StringHash, VariantMap&)
{
    FwClient* net = GetSubsystem<FwClient>();
    net->GetMailHeaders();
}

void MailWindow::HandleMailAttachmentDelete(StringHash, VariantMap& eventData)
{
    using namespace Events::MailAttachmentDelete;
    if (ToUrhoString(currentMailUuid_) != eventData[P_MAILUUID].GetString())
        return;
    UIElement* att = GetChild("AttachmentItems", true);
    RemoveItem(att, eventData[P_ITEMPOS].GetUInt());
}
