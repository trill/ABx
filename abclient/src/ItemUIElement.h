/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "FwClient.h"

class ItemStatsUIElement;

class ItemUIElement : public Button
{
    URHO3D_OBJECT(ItemUIElement, Button)
private:
    bool hasTooltip_{ false };
    SharedPtr<Text> tooltipLine1_;
    SharedPtr<ItemStatsUIElement> itemStats_;
    void CreateToolTip();
public:
    static void RegisterObject(Context* context);
    ItemUIElement(Context* context);
    ~ItemUIElement() override;

    Window* GetDragItem(int buttons, const IntVector2& position);
    void SetName(const String& value);
    void SetPos(unsigned value) { pos_ = value; }
    void SetIndex(unsigned value) { index_ = value; }
    void SetIcon(const String& icon);
    void SetCount(unsigned value);
    void SetValue(unsigned value);
    void SetStats(const ItemStats& value);
    void SetHasTooltip(bool value);

    String name_;
    unsigned pos_{ 0 };
    unsigned index_{ 0 };
    unsigned count_{ 0 };
    unsigned value_{ 0 };
    ItemStats stats_;
};
