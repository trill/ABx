/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Item.h"
#include <string>
#include <base64.h>
#include <locale>
#include <iostream>
#include <sa/StringTempl.h>

Item::Item(Context* context) :
    Object(context)
{
}

Item::~Item()
{
}

String FormatMoney(uint32_t amount)
{
    return String(sa::FormatWithThousandSep<char>(amount).c_str());
}
