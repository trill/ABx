/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PriceUIElement.h"
#include "Item.h"
#include "ItemsCache.h"
#include <AB/Entities/Item.h>

void PriceUIElement::RegisterObject(Context* context)
{
    context->RegisterFactory<PriceUIElement>();
    URHO3D_COPY_BASE_ATTRIBUTES(UIElement);
}

PriceUIElement::PriceUIElement(Context* context) :
    UIElement(context)
{
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    SetLayoutMode(LM_HORIZONTAL);
    SetLayoutSpacing(2);
}

PriceUIElement::~PriceUIElement() = default;

void PriceUIElement::Add(uint32_t index, uint32_t count)
{
    auto* itemsCache = GetSubsystem<ItemsCache>();
    Item* item = itemsCache->Get(index);
    if (!item)
        return;

    ++itemCount_;
    auto* container = CreateChild<UIElement>();
    container->SetLayoutMode(LM_HORIZONTAL);
    container->SetLayoutSpacing(2);

    ResourceCache* cache = GetSubsystem<ResourceCache>();
    auto* icon = container->CreateChild<Button>();
    icon->SetSize({ 16, 16 });
    icon->SetMaxSize({ 16, 16 });
    icon->SetMinSize({ 16, 16 });

    Texture2D* texture = (index != AB::Entities::MONEY_ITEM_INDEX) ? cache->GetResource<Texture2D>(item->iconFile_) :
        cache->GetResource<Texture2D>("Textures/Icons/DrachmaCoin.png");
    texture->SetNumLevels(1);
    texture->SetMipsToSkip(QUALITY_LOW, 0);
    icon->SetTexture(texture);
    icon->SetFullImageRect();
    auto* tooltip = icon->CreateChild<ToolTip>("Tooltip");
    tooltip->SetLayoutMode(LM_HORIZONTAL);
    Window* ttWindow = tooltip->CreateChild<Window>();
    ttWindow->SetLayoutMode(LM_VERTICAL);
    ttWindow->SetLayoutBorder(IntRect(4, 4, 4, 4));
    ttWindow->SetStyleAuto();
    auto* tttext = ttWindow->CreateChild<Text>();
    tttext->SetText(item->name_);
    tttext->SetStyleAuto();
    tooltip->SetPosition(IntVector2(-5, -30));

    auto* countText = container->CreateChild<Text>();
    countText->SetText(String(count));
    countText->SetStyleAuto();
    countText->SetFontSize(8);

    container->SetFixedWidth(50);
    SetFixedWidth(50 * itemCount_);
    UpdateLayout();
}
