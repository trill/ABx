/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include <sa/bitmap.h>
#include "Actor.h"

class ScoreChartWindow : public Window
{
    URHO3D_OBJECT(ScoreChartWindow, Window)
private:
    static constexpr unsigned CHART_WIDTH = 680;
    static constexpr unsigned CHART_HEIGHT = 500;
    // 1800 samples = 1/2 hour for max. 3 teams
    static constexpr int SAMPLES_TO_KEEP = 1800 * 3;
    struct Sample
    {
        Sample() {}
        Sample(const Sample& other) :
            time(other.time),
            groupId(other.groupId),
            color(other.color),
            actors(other.actors),
            morale(other.morale),
            health(other.health),
            maxHealth(other.maxHealth)
        {}
        float time{ 0.0f };
        uint32_t groupId{ 0 };
        Game::TeamColor color{ Game::TeamColor::Default };
        unsigned actors{ 0 };
        int morale{ 0 };
        int health{ 0 };
        int maxHealth{ 0 };
    };
    enum class Metrics
    {
        Health = 0,
        Morale
    };
    Metrics metrics_{ Metrics::Health };
    SharedPtr<Image> chartImage_;
    sa::Bitmap chartBitmap_;
    SharedPtr<Texture2D> chartTexture_;
    float collectTime_{ 0.0f };
    float time_{ 0.0f };
    int maxHealth_{ 0 };
    Vector<std::unique_ptr<Sample>> samples_;
    void HandleRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleLevelReady(StringHash eventType, VariantMap& eventData);
    void HandleCloseClicked(StringHash, VariantMap&);
    void DrawChart();
    void CollectData();
    void DrawPoint(const IntVector2& center, int size, const sa::Color& color);
    void DrawLine(const IntVector2& p1, const IntVector2& p2, int size, const sa::Color& color);
    int GetValue(const Sample& sample) const;
public:
    static void RegisterObject(Context* context);

    ScoreChartWindow(Context* context);
    ~ScoreChartWindow() override;

    void Clear();
};

