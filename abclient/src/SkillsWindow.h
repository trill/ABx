/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class Player;
class Actor;
class Spinner;
class FilePicker;
class SkillDragIcon;

class SkillsWindow : public Window
{
    URHO3D_OBJECT(SkillsWindow, Window)
private:
    SharedPtr<SkillDragIcon> dragSkill_;
    SharedPtr<FilePicker> filePicker_;
    void AddProfessions(const Actor& actor);
    void UpdateAttributes(const Actor& actor);
    void UpdateSkills(const Actor& actor);
    void UpdateAttribsHeader(const Actor& actor);
    Text* CreateDropdownItem(const String& text, unsigned value);
    void SubscribeEvents();
    void HandleFilePickerClosed(StringHash eventType, VariantMap& eventData);
    void HandleLoadFileSelected(StringHash eventType, VariantMap& eventData);
    void HandleSaveFileSelected(StringHash eventType, VariantMap& eventData);
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleLoadClicked(StringHash eventType, VariantMap& eventData);
    void HandleSaveClicked(StringHash eventType, VariantMap& eventData);
    void HandleProfessionSelected(StringHash eventType, VariantMap& eventData);
    void HandleSetAttribValue(StringHash eventType, VariantMap& eventData);
    void HandleSkillsChanged(StringHash eventType, VariantMap& eventData);
    UIElement* GetAttributeContainer(uint32_t index);
    LineEdit* GetAttributeEdit(uint32_t index);
    Spinner* GetAttributeSpinner(uint32_t index);
    void SetAttributeValue(const Actor& actor, uint32_t index, int value, unsigned remaining);
    void SetProfessionIndex(uint32_t index);
    bool IsChangeable() const;
    void HandleSkillDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragMove(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragEnd(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    SkillsWindow(Context* context);
    ~SkillsWindow() override;

    void UpdateAll();
    void FocusMainElement();
};
