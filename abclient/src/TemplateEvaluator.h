/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <string_view>

class Actor;

class TemplateEvaluator
{
private:
    const Actor& actor_;
public:
    TemplateEvaluator(const Actor& actor);
    std::string Evaluate(const std::string& source);
};
