/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Actor.h"
#include "HealthBar.h"

static const StringHash E_TARGETWINDOW_UNSELECT = StringHash("Target Window unselect object");

class TargetWindow : public UIElement
{
    URHO3D_OBJECT(TargetWindow, UIElement)
private:
    WeakPtr<Actor> target_;
    SharedPtr<HealthBar> healthBar_;
    SharedPtr<Button> tradeButton_;
    void HandleClearTargetClicked(StringHash eventType, VariantMap& eventData);
    void HandleTradeClicked(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);

    TargetWindow(Context* context);
    ~TargetWindow() override;

    void SetTarget(SharedPtr<Actor> target);
};

