/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "ValueBar.h"

class Actor;

class ActorResourceBar : public Window
{
    URHO3D_OBJECT(ActorResourceBar, Window)
public:
    enum class ResourceType
    {
        None,
        Health,
        Energy,
        XP
    };
private:
    WeakPtr<Actor> actor_;
    SharedPtr<ValueBar> bar_;
    ResourceType type_{ ResourceType::None };
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);

    ActorResourceBar(Context* context);
    ~ActorResourceBar() override;
    void SetActor(SharedPtr<Actor> actor);
    void SetResourceType(ResourceType type);
};

class ActorHealthBar final : public ActorResourceBar
{
    URHO3D_OBJECT(ActorHealthBar, ActorResourceBar)
public:
    static void RegisterObject(Context* context);
    ActorHealthBar(Context* context);
    ~ActorHealthBar() override;
};

class ActorEnergyBar final : public ActorResourceBar
{
    URHO3D_OBJECT(ActorEnergyBar, ActorResourceBar)
public:
    static void RegisterObject(Context* context);
    ActorEnergyBar(Context* context);
    ~ActorEnergyBar() override;
};

class ActorXPBar final : public ActorResourceBar
{
    URHO3D_OBJECT(ActorXPBar, ActorResourceBar)
public:
    static void RegisterObject(Context* context);
    ActorXPBar(Context* context);
    ~ActorXPBar() override;
};
