/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "FwClient.h"
#include "Options.h"
#include "LevelManager.h"
#include "OptionsWindow.h"
#include "ItemsCache.h"
#include "Shortcuts.h"
#include "WindowManager.h"
#include "Mumble.h"
#include "AudioManager.h"
#include "SkillManager.h"
#include <Urho3DAll.h>
#include <memory>

namespace sa {
class Process;
}

using namespace Urho3D;

class ClientApp : public Application
{
    URHO3D_OBJECT(ClientApp, Application)
public:
    /// Construct.
    ClientApp(Context* context);
    ~ClientApp() override;

    void Setup() override;

    /// Setup after engine initialization and before running the main loop.
    void Start() override;
    void Stop() override;
#ifdef DEBUG_HUD
    void CreateHUD();
#endif
private:
    SharedPtr<Options> options_;
    SharedPtr<FwClient> client_;
    SharedPtr<Shortcuts> shortcuts_;
    SharedPtr<ItemsCache> itemsCache_;
    SharedPtr<SkillManager> skillsManager_;
    SharedPtr<LevelManager> levelManager_;
    SharedPtr<WindowManager> windowManager_;
    SharedPtr<AudioManager> audioManager_;
    SharedPtr<Mumble> mumble_;
    String exeName_;
    String appPath_;
    std::unique_ptr<sa::Process> process_;

    void SetWindowTitleAndIcon();
    void SwitchScene(const String& sceneName);
#ifdef DEBUG_HUD
    void HandleToggleDebugHUD(StringHash eventType, VariantMap& eventData);
    void HandleToggleConsole(StringHash eventType, VariantMap& eventData);
#endif
    void HandleToggleOptions(StringHash eventType, VariantMap& eventData);
    void HandleTakeScreenshot(StringHash eventType, VariantMap& eventData);
    void HandleExitProgram(StringHash eventType, VariantMap& eventData);
    void HandleToggleMuteAudio(StringHash eventType, VariantMap& eventData);
    void HandleStartProgram(StringHash eventType, VariantMap& eventData);
    void HandleRestart(StringHash eventType, VariantMap& eventData);
};
