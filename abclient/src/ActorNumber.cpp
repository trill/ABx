/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ActorNumber.h"
#include "Actor.h"
#include "UIPriorities.h"

static constexpr float BASE_FONT_SIZE = 12.0f;

void ActorNumber::RegisterObject(Context* context)
{
    context->RegisterFactory<ActorNumber>();
    URHO3D_COPY_BASE_ATTRIBUTES(Text);
}

ActorNumber::ActorNumber(Context* context) :
    Text(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ActorNumber, HandleUpdate));
    SetVisible(false);
    SetOpacity(1.0f);
    SetAlignment(HA_LEFT, VA_TOP);
    SetLayoutMode(LM_HORIZONTAL);
    SetFont(cache->GetResource<Font>("Fonts/ClearSans-Bold.ttf"), BASE_FONT_SIZE);
    SetTextAlignment(HA_CENTER);
    SetStyleAuto();
    SetTextEffect(TE_STROKE);
    SetEffectColor({ 0.5, 0.5, 0.5 });
    SetPriority(Priorities::ActorNumber);
}

ActorNumber::~ActorNumber() = default;

void ActorNumber::SetNumber(NumberType type, int value, bool isPlayer)
{
    if (type == type_ && value_ == value)
        return;
    type_ = type;
    value_ = value;

    static const Color LIGHT_BLUE{ 0.2f, 0.2f, 1.0f, 1.0 };
    SetOpacity(1.0f);
    SetVisible(true);
    SetText(String(value));
    switch (type_)
    {
    case NumberType::LostHealth:
        SetColor(isPlayer ? Color::RED : Color::YELLOW);
        break;
    case NumberType::GainedHealth:
        SetColor(LIGHT_BLUE);
        break;
    case NumberType::LostEnergy:
        SetColor(Color::MAGENTA);
        break;
    case NumberType::GainedEnergy:
        SetColor(Color::MAGENTA);
        break;
    case NumberType::XP:
        SetText(String(value) + " XP");
        SetColor(Color::GREEN);
        break;
    default:
        break;
    }
    UpdateLayout();
}

void ActorNumber::SetTarget(SharedPtr<Actor> owner)
{
    if (owner)
    {
        static bool shiftLeft = true;
        Vector3 headPos = owner->GetHeadPos();
        headPos.y_ += 0.5f;
        IntVector2 headScreenPos = owner->WorldToScreenPoint(headPos);
        IntVector2 screenPos = headScreenPos;
        screenPos.y_ -= 5;

        UI* ui = GetSubsystem<UI>();
        auto* elem = ui->GetElementAt(screenPos, false);
        while (auto* an = dynamic_cast<ActorNumber*>(elem))
        {
            if (shiftLeft)
                screenPos.x_ -= (GetSize().x_ + 5);
            else
                screenPos.x_ += (an->GetSize().x_ + 5);
            elem = ui->GetElementAt(screenPos, false);
        }

        shiftLeft = !shiftLeft;
        screenPos -= GetSize() / 2;
        SetPosition(screenPos);
        screenPosX_ = headScreenPos.x_ - screenPos.x_;
        SetVisible(true);
    }
    else
        SetVisible(false);
    target_ = owner;
}

void ActorNumber::HandleUpdate(StringHash, VariantMap& eventData)
{
    using namespace Update;
    timeVisible_ += eventData[P_TIMESTEP].GetFloat();
    if (timeVisible_ > 2.0f)
    {
        Remove();
        return;
    }
    IntVector2 currPos = GetPosition();
    if (auto t = target_.Lock())
    {
        // Make the numbers stay at the head X position of the target, even when the the camera moves.
        currPos.x_ = t->WorldToScreenPoint(t->GetHeadPos()).x_ - screenPosX_;
    }
    currPos.y_ -= (int)(timeVisible_ * 1.5f);
    SetPosition(currPos);

    if (timeVisible_ > 1.0f)
    {
        SetOpacity(1.0f - (timeVisible_ - 1.0f));
    }
}
