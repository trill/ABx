/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SkillBarWindow.h"
#include "Actor.h"
#include "FwClient.h"
#include "LevelManager.h"
#include "SkillCostElement.h"
#include "SkillManager.h"
#include "WindowManager.h"
#include "SkillTooltip.h"
#include "SkillDragIcon.h"
#include "ProgressElement.h"
#include "UIPriorities.h"
#include <libshared/Mechanic.h>

void SkillBarWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<SkillBarWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

SkillBarWindow::SkillBarWindow(Context* context) :
    Window(context),
    skills_()
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile *file = cache->GetResource<XMLFile>("UI/SkillBarWindow.xml");
    LoadXML(file->GetRoot(), nullptr);
    SetName("SkillBar");

    SetSize(400, 50);

    SetVisible(true);
    SetPosition(0, 0);
    SetAlignment(HA_CENTER, VA_BOTTOM);
    SetPriority(Priorities::SkillBar);

    SetStyleAuto();
    auto createProgress = [](Button& button)
    {
        auto* pg = button.CreateChild<ProgressElement>("SkillProgress");
        pg->SetPosition({ 0, 0 });
        pg->SetFixedSize(button.GetSize());
        pg->direction_ = ProgressElement::Direction::Forward;
    };

    skill1_ = GetChildStaticCast<Button>("Skill1", true);
    createProgress(*skill1_);
    skill2_ = GetChildStaticCast<Button>("Skill2", true);
    createProgress(*skill2_);
    skill3_ = GetChildStaticCast<Button>("Skill3", true);
    createProgress(*skill3_);
    skill4_ = GetChildStaticCast<Button>("Skill4", true);
    createProgress(*skill4_);
    skill5_ = GetChildStaticCast<Button>("Skill5", true);
    createProgress(*skill5_);
    skill6_ = GetChildStaticCast<Button>("Skill6", true);
    createProgress(*skill6_);
    skill7_ = GetChildStaticCast<Button>("Skill7", true);
    createProgress(*skill7_);
    skill8_ = GetChildStaticCast<Button>("Skill8", true);
    createProgress(*skill8_);

    ResetSkillButtons();
    SubscribeEvents();
}

SkillBarWindow::~SkillBarWindow()
{
    UnsubscribeFromAllEvents();
}

void SkillBarWindow::SetActor(SharedPtr<Actor> actor)
{
    actor_ = actor;
}

void SkillBarWindow::UpdateSkill(unsigned pos, uint32_t index)
{
    auto actor = actor_.Lock();
    if (!actor)
        return;

    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Texture2D* defTexture = cache->GetResource<Texture2D>("Textures/Skills/no_skill_disabled.png");
    defTexture->SetNumLevels(1);
    defTexture->SetMipsToSkip(QUALITY_LOW, 0);

    SkillManager* sm = GetSubsystem<SkillManager>();

    bool iconSet = false;
    Button* btn = GetButtonFromIndex(pos);
    btn->SetVar("SkillPos", pos);
    const Skill* skill = sm->GetSkillByIndex(index);
    if (skill && skill->index != 0)
    {
        btn->SetVar("SkillIndex", skill->index);
        Texture2D* icon = cache->GetResource<Texture2D>(String(skill->icon.c_str()));
        if (icon)
        {
            icon->SetNumLevels(1);
            icon->SetMipsToSkip(QUALITY_LOW, 0);
            btn->SetTexture(icon);
            btn->SetFullImageRect();
            btn->SetBorder(IntRect(4, 4, 4, 4));
            if (!IsChangeable())
            {
                btn->SetHoverOffset(IntVector2(-2, -2));
                btn->SetPressedOffset(IntVector2(2, 2));
            }
            else
            {
                btn->SetHoverOffset(IntVector2(0, 0));
                btn->SetPressedOffset(IntVector2(0, 0));
            }
            iconSet = true;
        }
        SkillTooltip* tooltip = btn->GetChildStaticCast<SkillTooltip>("SkillTooltip", true);
        tooltip->SetSkill(skill, actor.Get());
        btn->SetEnabled(true);
    }
    else
    {
        btn->SetVar("SkillIndex", 0);
        SkillTooltip* tooltip = btn->GetChildStaticCast<SkillTooltip>("SkillTooltip", true);
        if (tooltip)
        {
            tooltip->SetSkill(nullptr, actor.Get());
        }
        btn->SetEnabled(false);
    }
    if (!iconSet)
    {
        btn->SetTexture(defTexture);
        btn->SetFullImageRect();
        btn->SetBorder(IntRect(4, 4, 4, 4));
        btn->SetHoverOffset(IntVector2(0, 0));
        btn->SetPressedOffset(IntVector2(0, 0));
    }
}

void SkillBarWindow::SetSkills(const Game::SkillIndices& skills)
{
    ResetSkillButtons();
    auto actor = actor_.Lock();
    if (!actor)
        return;

    skills_ = skills;
    uint32_t i = 1;
    for (const auto& s : skills_)
    {
        UpdateSkill(i, s);
        ++i;
    }
}

void SkillBarWindow::DropSkill(const IntVector2& pos, uint32_t skillIndex)
{
    IntRect screenRect(GetScreenPosition(), GetScreenPosition() + GetSize());
    if (!screenRect.IsInside(pos))
        return;

    IntVector2 clientPos = pos - GetScreenPosition();
    unsigned skillPos = GetSkillPosFromClientPos(clientPos);
    if (skillPos > Game::PLAYER_MAX_SKILLS - 1)
        return;
    auto* client = GetSubsystem<FwClient>();
    client->EquipSkill(skillIndex, static_cast<uint8_t>(skillPos));
}

void SkillBarWindow::SubscribeEvents()
{
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(SkillBarWindow, HandleMouseUp));
    SubscribeToEvent(skill1_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill1Clicked));
    SubscribeToEvent(skill2_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill2Clicked));
    SubscribeToEvent(skill3_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill3Clicked));
    SubscribeToEvent(skill4_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill4Clicked));
    SubscribeToEvent(skill5_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill5Clicked));
    SubscribeToEvent(skill6_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill6Clicked));
    SubscribeToEvent(skill7_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill7Clicked));
    SubscribeToEvent(skill8_, E_RELEASED, URHO3D_HANDLER(SkillBarWindow, HandleSkill8Clicked));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(SkillBarWindow, HandleUpdate));
    SubscribeToEvent(Events::E_SET_SKILL, URHO3D_HANDLER(SkillBarWindow, HandleSetSkill));
    for (unsigned i = 1; i <= Game::PLAYER_MAX_SKILLS; ++i)
    {
        auto* button = GetButtonFromIndex(i);
        SubscribeToEvent(button, E_DRAGMOVE, URHO3D_HANDLER(SkillBarWindow, HandleSkillDragMove));
        SubscribeToEvent(button, E_DRAGBEGIN, URHO3D_HANDLER(SkillBarWindow, HandleSkillDragBegin));
        SubscribeToEvent(button, E_DRAGCANCEL, URHO3D_HANDLER(SkillBarWindow, HandleSkillDragCancel));
        SubscribeToEvent(button, E_DRAGEND, URHO3D_HANDLER(SkillBarWindow, HandleSkillDragEnd));
    }
    SubscribeToEvent(Events::E_OBJECTUSESKILL, URHO3D_HANDLER(SkillBarWindow, HandleObjectUseSkill));
    SubscribeToEvent(Events::E_OBJECTENDUSESKILL, URHO3D_HANDLER(SkillBarWindow, HandleObjectEndUseSkill));
    SubscribeToEvent(Events::E_OBJECTSKILLSRECHARGED, URHO3D_HANDLER(SkillBarWindow, HandleObjectSkillsRecharged));
    SubscribeToEvent(Events::E_LEVELREADY, URHO3D_HANDLER(SkillBarWindow, HandleLevelReady));
}

void SkillBarWindow::HandleUpdate(StringHash, VariantMap&)
{
}

void SkillBarWindow::HandleMouseUp(StringHash, VariantMap& eventData)
{
    if (!IsChangeable())
        return;

    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;

    UI* ui = GetSubsystem<UI>();
    auto* input = GetSubsystem<Input>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || (elem != this && !elem->IsChildOf(this)))
        return;

    if (!IsInside(input->GetMousePosition(), true))
        return;

    if (input->GetKeyDown(KEY_LCTRL))
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->PingSkills();
    }
    else
        ShowSkillsWindow();
}

void SkillBarWindow::HandleSkill1Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(1);
    }
}

void SkillBarWindow::HandleSkill2Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(2);
    }
}

void SkillBarWindow::HandleSkill3Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(3);
    }
}

void SkillBarWindow::HandleSkill4Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(4);
    }
}

void SkillBarWindow::HandleSkill5Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(5);
    }
}

void SkillBarWindow::HandleSkill6Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(6);
    }
}

void SkillBarWindow::HandleSkill7Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(7);
    }
}

void SkillBarWindow::HandleSkill8Clicked(StringHash, VariantMap&)
{
    if (IsUseable())
    {
        FwClient* client = GetSubsystem<FwClient>();
        client->UseSkill(8);
    }
}

void SkillBarWindow::HandleSetSkill(StringHash, VariantMap& eventData)
{
    using namespace Events::SetSkill;
    uint32_t id = eventData[P_OBJECTID].GetUInt();
    if (id != actor_->gameId_)
        return;
    uint32_t skillIndex = eventData[P_SKILLINDEX].GetUInt();
    unsigned skillPos = eventData[P_SKILLPOS].GetUInt();
    UpdateSkill(skillPos + 1, skillIndex);
}

void SkillBarWindow::HandleObjectUseSkill(StringHash, VariantMap& eventData)
{
    using namespace Events::ObjectUseSkill;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    auto a = actor_.Lock();
    if (!a)
        return;
    if (objectId != a->gameId_)
        return;

    int barIndex = eventData[P_SKILLINDEX].GetInt();
    auto* button = GetButtonFromIndex(barIndex);
    if (!button)
        return;

    auto* progress = button->GetChildStaticCast<ProgressElement>("SkillProgress", true);
    if (!progress)
        return;
    progress->Stop();
    auto activation = eventData[P_ACTIVATION].GetUInt();
    progress->SetMaxTicks(activation);
    progress->mode_ = ProgressElement::Mode::Inc;
    progress->Start();
}

void SkillBarWindow::HandleObjectEndUseSkill(StringHash, VariantMap& eventData)
{
    using namespace Events::ObjectEndUseSkill;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    auto a = actor_.Lock();
    if (!a)
        return;
    if (objectId != a->gameId_)
        return;

    int barIndex = eventData[P_SKILLINDEX].GetInt();
    auto* button = GetButtonFromIndex(barIndex);
    if (!button)
        return;

    auto* progress = button->GetChildStaticCast<ProgressElement>("SkillProgress", true);
    if (!progress)
        return;
    progress->Stop();
    auto recharge = eventData[P_RECHARGE].GetUInt();
    progress->SetMaxTicks(recharge);
    progress->mode_ = ProgressElement::Mode::Dec;
    progress->Start();
}

void SkillBarWindow::HandleObjectSkillsRecharged(StringHash, VariantMap& eventData)
{
    using namespace Events::ObjectSkillsRecharged;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    auto a = actor_.Lock();
    if (!a)
        return;
    if (objectId != a->gameId_)
        return;

    unsigned skills = eventData[P_SKILLS].GetUInt();
    for (unsigned i = 0; i < Game::PLAYER_MAX_SKILLS; ++i)
    {
        if ((skills >> i) & 1)
        {
            auto* button = GetButtonFromIndex(i + 1);
            if (!button)
                continue;
            auto* progress = button->GetChildStaticCast<ProgressElement>("SkillProgress", true);
            if (!progress)
                continue;
            progress->Stop();
        }
    }
}

void SkillBarWindow::HandleLevelReady(StringHash, VariantMap&)
{
    for (uint32_t i = 1; i <= Game::PLAYER_MAX_SKILLS; ++i)
    {
        Button* btn = GetButtonFromIndex(i);
        auto* progress = btn->GetChildStaticCast<ProgressElement>("SkillProgress", true);
        if (progress)
            progress->Stop();
    }
}

Button* SkillBarWindow::GetButtonFromIndex(uint32_t index)
{
    switch (index)
    {
    case 1:
        return skill1_;
    case 2:
        return skill2_;
    case 3:
        return skill3_;
    case 4:
        return skill4_;
    case 5:
        return skill5_;
    case 6:
        return skill6_;
    case 7:
        return skill7_;
    case 8:
        return skill8_;
    default:
        return nullptr;
    }
}

void SkillBarWindow::ResetSkillButtons()
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Texture2D* defTexture = cache->GetResource<Texture2D>("Textures/UI.png");
    for (uint32_t i = 1; i <= Game::PLAYER_MAX_SKILLS; ++i)
    {
        Button* btn = GetButtonFromIndex(i);
        btn->SetTexture(defTexture);
        btn->SetImageRect(IntRect(16, 0, 32, 16));
        btn->SetBorder(IntRect(4, 4, 4, 4));
        btn->SetHoverOffset(IntVector2(0, 0));
        btn->SetEnabled(false);
    }
}

IntVector2 SkillBarWindow::GetButtonSize() const
{
    return skill1_->GetSize();
}

unsigned SkillBarWindow::GetSkillPosFromClientPos(const IntVector2& clientPos)
{
    return clientPos.x_ / (GetButtonSize().x_ + GetLayoutSpacing());
}

bool SkillBarWindow::IsChangeable() const
{
    auto* lm = GetSubsystem<LevelManager>();
    if (!lm)
        return false;
    return AB::Entities::IsOutpost(lm->GetMapType());
}

bool SkillBarWindow::IsUseable() const
{
    auto* lm = GetSubsystem<LevelManager>();
    if (!lm)
        return false;
    return AB::Entities::IsBattle(lm->GetMapType());
}


void SkillBarWindow::ShowSkillsWindow()
{
    auto* wm = GetSubsystem<WindowManager>();
    auto s = wm->GetWindow(WINDOW_SKILLS);
    if (!s->IsVisible())
    {
        VariantMap& e = GetEventDataMap();
        SendEvent(Events::E_SC_TOGGLESKILLSWINDOW, e);
    }
}

void SkillBarWindow::HandleSkillDragBegin(StringHash, VariantMap& eventData)
{
    if (!IsChangeable())
        return;

    using namespace DragBegin;

    auto* element = static_cast<Button*>(eventData[P_ELEMENT].GetVoidPtr());
    if (!element)
        return;
    UIElement* root = GetSubsystem<UI>()->GetRoot();

    dragSkill_ = root->CreateChild<SkillDragIcon>();
    dragSkill_->SetPriority(Priorities::DragElement);
    dragSkill_->SetSkill(*element, element->GetTexture());

    int lx = eventData[P_X].GetInt();
    int ly = eventData[P_Y].GetInt();
    dragSkill_->SetPosition(IntVector2(lx, ly) - dragSkill_->GetSize() / 2);

    int buttons = eventData[P_BUTTONS].GetInt();
    element->SetVar("BUTTONS", buttons);
}

void SkillBarWindow::HandleSkillDragMove(StringHash, VariantMap& eventData)
{
    if (!IsChangeable())
        return;

    if (!dragSkill_)
        return;
    using namespace DragMove;

    int buttons = eventData[P_BUTTONS].GetInt();
    auto* element = static_cast<UISelectable*>(eventData[P_ELEMENT].GetVoidPtr());
    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    int BUTTONS = element->GetVar("BUTTONS").GetInt();

    if (buttons == BUTTONS)
        dragSkill_->SetPosition(IntVector2(X, Y) - dragSkill_->GetSize() / 2);
}

void SkillBarWindow::HandleSkillDragCancel(StringHash, VariantMap&)
{
    if (!IsChangeable())
        return;

    using namespace DragCancel;
    if (!dragSkill_)
        return;
    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragSkill_.Get());
    dragSkill_.Reset();
}

void SkillBarWindow::HandleSkillDragEnd(StringHash, VariantMap& eventData)
{
    if (!IsChangeable())
        return;

    using namespace DragEnd;
    if (!dragSkill_)
        return;
    uint32_t skillIndex = dragSkill_->GetVar("SkillIndex").GetUInt();

    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    IntVector2 pos = IntVector2(X, Y);
    IntRect screenRect(GetScreenPosition(), GetScreenPosition() + GetSize());
    if (!screenRect.IsInside(pos))
    {
        // Dropping anywhere -> remove skill
        uint32_t skillPos = dragSkill_->GetVar("SkillPos").GetUInt() - 1;
        auto* client = GetSubsystem<FwClient>();
        client->EquipSkill(0, static_cast<uint8_t>(skillPos));
    }
    else
        DropSkill(pos, skillIndex);

    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragSkill_.Get());
    dragSkill_.Reset();
}
