/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"
#include "AB/Entities/ConcreteItem.h"
#include "FwClient.h"

static const unsigned CHEST_COLS_PER_ROW = 10;
static const int CHEST_ITEM_SIZE_X = 48;
static const int CHEST_ITEM_SIZE_Y = 48;

class Item;
class NumberInputBox;

class AccountChestDialog final : public DialogWindow
{
    URHO3D_OBJECT(AccountChestDialog, DialogWindow)
private:
    bool initializted_;
    SharedPtr<Window> dragItem_;
    SharedPtr<NumberInputBox> inputBox_;
    void HandleChest(StringHash eventType, VariantMap& eventData);
    void HandleChestItemUpdate(StringHash eventType, VariantMap& eventData);
    void HandleChestItemRemove(StringHash eventType, VariantMap& eventData);
    void HandleItemDoubleClick(StringHash eventType, VariantMap& eventData);
    void HandleItemDragMove(StringHash eventType, VariantMap& eventData);
    void HandleItemDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleItemDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleItemDragEnd(StringHash eventType, VariantMap& eventData);
    void HandleDepositClicked(StringHash eventType, VariantMap& eventData);
    void HandleWithdrawClicked(StringHash eventType, VariantMap& eventData);
    void HandleWithdrawDone(StringHash eventType, VariantMap& eventData);
    void HandleDepositDone(StringHash eventType, VariantMap& eventData);
    void HandleDialogClosed(StringHash eventType, VariantMap& eventData);
    void HandleItemCountDone(StringHash eventType, VariantMap& eventData);
    uint16_t GetItemPosFromClientPos(const IntVector2& clientPos);
    BorderImage* GetItemContainer(uint16_t pos);
    void SetItem(Item* item, const ConcreteItem& iItem);
protected:
    void SubscribeEvents();
public:
    static void RegisterObject(Context* context);
    AccountChestDialog(Context* context);
    ~AccountChestDialog() override;
    void Initialize(uint32_t) override;
    void DropItem(const IntVector2& screenPos, const ConcreteItem& ci);
    void Clear();

    uint32_t money_{ 0 };
};

