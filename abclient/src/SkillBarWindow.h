/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libshared/TemplEncoder.h>
#include <Urho3DAll.h>

class Actor;
class SkillDragIcon;

class SkillBarWindow : public Window
{
    URHO3D_OBJECT(SkillBarWindow, Window)
private:
    SharedArrayPtr<Button> skillButtons_;
    SharedPtr<Button> skill1_;
    SharedPtr<Button> skill2_;
    SharedPtr<Button> skill3_;
    SharedPtr<Button> skill4_;
    SharedPtr<Button> skill5_;
    SharedPtr<Button> skill6_;
    SharedPtr<Button> skill7_;
    SharedPtr<Button> skill8_;
    WeakPtr<Actor> actor_;
    Game::SkillIndices skills_;
    SharedPtr<SkillDragIcon> dragSkill_;
    void SubscribeEvents();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void HandleSkill1Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill2Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill3Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill4Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill5Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill6Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill7Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSkill8Clicked(StringHash eventType, VariantMap& eventData);
    void HandleSetSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectEndUseSkill(StringHash eventType, VariantMap& eventData);
    void HandleObjectSkillsRecharged(StringHash eventType, VariantMap& eventData);
    void HandleLevelReady(StringHash eventType, VariantMap& eventData);
    Button* GetButtonFromIndex(uint32_t index);
    void ResetSkillButtons();
    IntVector2 GetButtonSize() const;
    unsigned GetSkillPosFromClientPos(const IntVector2& clientPos);
    void UpdateSkill(unsigned pos, uint32_t index);
    bool IsChangeable() const;
    bool IsUseable() const;
    void ShowSkillsWindow();
    void HandleSkillDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragMove(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleSkillDragEnd(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);

    SkillBarWindow(Context* context);
    ~SkillBarWindow() override;

    void SetActor(SharedPtr<Actor> actor);
    void SetSkills(const Game::SkillIndices& skills);
    void DropSkill(const IntVector2& pos, uint32_t skillIndex);
};

