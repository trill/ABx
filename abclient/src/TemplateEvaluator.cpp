/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TemplateEvaluator.h"
#include <libshared/Attributes.h>
#include "Actor.h"
#include <sstream>
#include <algorithm>
#include <sa/TemplateParser.h>
#define POWER_OPERATOR
#include <sa/ev.h>

TemplateEvaluator::TemplateEvaluator(const Actor& actor) :
    actor_(actor)
{
}

std::string TemplateEvaluator::Evaluate(const std::string& source)
{
    // Syntax:
    // This awesome skill heals by $(healing * 2 + 3} and makes ${death * 3} damage.
    if (source.empty())
        return "";

PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4244)
    auto toLower = [](const std::string& str) -> std::string
    {
        std::string result;
        result.resize(str.length());
        std::transform(str.begin(), str.end(), result.begin(), ::tolower);
        return result;
    };
PRAGMA_WARNING_POP
    sa::ev::Context<float> ctx;
#define ENUMERATE_ATTRIBUTE(v) ctx.constants[toLower(#v)] = static_cast<float>(actor_.GetAttributeRank(Game::Attribute::v));
    ENUMERATE_ATTRIBUTES
#undef ENUMERATE_ATTRIBUTE

    sa::templ::Parser parser;
    parser.quotesSupport_ = false;
    const auto tokens = parser.Parse(source);
    return tokens.ToString([&ctx](const sa::templ::Token& token) -> std::string
    {
        if (token.type == sa::templ::Token::Type::Variable)
            return std::to_string(static_cast<int>(sa::ev::Evaluate(token.value, ctx)));
        return "";
    });
}
