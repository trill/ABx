/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "Skill.h"
#include "SkillCostElement.h"
#include "Actor.h"
#include <AB/Entities/Effect.h>

class SkillTooltip : public ToolTip
{
    URHO3D_OBJECT(SkillTooltip, ToolTip)
private:
    SharedPtr<Window> window_;
    SharedPtr<Text> skillName_;
    SharedPtr<Text> skillDescription_;
    SharedPtr<Text> skillAttribute_;
    SharedPtr<SkillCostElement> skillCost_;
    SharedPtr<Text> addText_;
    WeakPtr<Actor> actor_;
    const Skill* skill_{ nullptr };
    void HandleSetAttribValue(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    SkillTooltip(Context* context);
    ~SkillTooltip() override;

    void SetSkill(const Skill* skill, Actor* actor, AB::Entities::EffectCategory cat = AB::Entities::EffectNone);
    void SetAdditionalText(const String& txt);
};
