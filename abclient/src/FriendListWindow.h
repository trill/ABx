/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Packets/ServerPackets.h>
#include <Urho3DAll.h>
#include <libclient/Receiver.h>

class Player;
class InputBox;

class FriendListWindow : public Window
{
    URHO3D_OBJECT(FriendListWindow, Window)
private:
    SharedPtr<ListView> friendList_;
    SharedPtr<ListView> ignoreList_;
    SharedPtr<LineEdit> addFriendEdit_;
    SharedPtr<LineEdit> addIgnoreEdit_;
    SharedPtr<Menu> friendPopup_;
    SharedPtr<Menu> ignorePopup_;
    SharedPtr<DropDownList> statusDropdown_;
    SharedPtr<InputBox> inputBox_;
    bool initialized_{ false };

    void CreateMenus();
    void CreateFriendMenu();
    void CreateIgnoreMenu();
    void SubscribeEvents();
    void HandleStatusDropdownSelected(StringHash eventType, VariantMap& eventData);
    void HandleGotFriendList(StringHash eventType, VariantMap& eventData);
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleAddFriendClicked(StringHash eventType, VariantMap& eventData);
    void HandleAddIgnoreClicked(StringHash eventType, VariantMap& eventData);
    void HandleGotPlayerInfo(StringHash eventType, VariantMap& eventData);
    void HandleFriendAdded(StringHash eventType, VariantMap& eventData);
    void HandleFriendRemoved(StringHash eventType, VariantMap& eventData);
    void HandleFriendRemoveClicked(StringHash eventType, VariantMap& eventData);
    void HandleIgnoredRemoveClicked(StringHash eventType, VariantMap& eventData);
    void HandleFriendWhisperClicked(StringHash eventType, VariantMap& eventData);
    void HandleFriendSendMailClicked(StringHash eventType, VariantMap& eventData);
    void HandleFriendRenameClicked(StringHash eventType, VariantMap& eventData);
    void HandleIgnoredRenameClicked(StringHash eventType, VariantMap& eventData);
    void HandleFriendItemClicked(StringHash eventType, VariantMap& eventData);
    void HandleIgnoredItemClicked(StringHash eventType, VariantMap& eventData);
    void HandleDialogClosed(StringHash eventType, VariantMap& eventData);
    void HandleRenameFriendDialogDone(StringHash eventType, VariantMap& eventData);
    void HandleRenameIgnoredDialogDone(StringHash eventType, VariantMap& eventData);
    void HandleFriendRenamed(StringHash eventType, VariantMap& eventData);
    void HandleFriendCopyNameClicked(StringHash eventType, VariantMap& eventData);
    void HandleIgnoredCopyNameClicked(StringHash eventType, VariantMap& eventData);
    void UpdateItem(ListView* lv, const AB::Packets::Server::PlayerInfo& f);
    void UpdateAll();
    void UpdateSelf(const AB::Packets::Server::PlayerInfo& acc);
    void AddFriend();
    void AddIgnore();
public:
    static void RegisterObject(Context* context);

    FriendListWindow(Context* context);
    ~FriendListWindow() override;

    void GetList();
    void Clear();
};

