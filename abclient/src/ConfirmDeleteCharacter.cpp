/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ConfirmDeleteCharacter.h"
#include "FwClient.h"

void ConfirmDeleteCharacter::RegisterObject(Context* context)
{
    context->RegisterFactory<ConfirmDeleteCharacter>();
    URHO3D_COPY_BASE_ATTRIBUTES(DialogWindow);
}
ConfirmDeleteCharacter::ConfirmDeleteCharacter(Context* context) :
    DialogWindow(context)
{
}

ConfirmDeleteCharacter::ConfirmDeleteCharacter(Context* context, const String& uuid, const String& name) :
    DialogWindow(context),
    uuid_(uuid),
    name_(name)
{
    LoadLayout("UI/DeleteCharacterConfirmation.xml");
    SetStyleAuto();

    SetSize(400, 230);
    SetMinSize(400, 230);
    SetMaxSize(400, 230);
    SetLayoutSpacing(10);
    SetLayoutBorder({ 10, 10, 10, 10 });
    SetMovable(false);

    auto* deleteButton = GetChildDynamicCast<Button>("DeleteButton", true);
    SubscribeToEvent(deleteButton, E_RELEASED, URHO3D_HANDLER(ConfirmDeleteCharacter, HandleDeleteClicked));
    auto* cancelButton = GetChildDynamicCast<Button>("CancelButton", true);
    SubscribeToEvent(cancelButton, E_RELEASED, URHO3D_HANDLER(ConfirmDeleteCharacter, HandleCancelClicked));

    nameEdit_ = GetChildDynamicCast<LineEdit>("NameEdit", true);

    auto* messageText = GetChildStaticCast<Text>("MessageText", true);
    String message;
    message.AppendWithFormat("If you are sure that you want to delete %s enter %s:", name_.CString(), name_.CString());
    messageText->SetText(message);

    MakeModal();
    BringToFront();

    Center();
    DialogWindow::SubscribeEvents();

    SubscribeToEvent(this, E_MODALCHANGED, URHO3D_HANDLER(ConfirmDeleteCharacter, HandleCancelClicked));
    nameEdit_->SetFocus(true);
}

ConfirmDeleteCharacter::~ConfirmDeleteCharacter()
{
}

void ConfirmDeleteCharacter::HandleDeleteClicked(StringHash, VariantMap&)
{
    using namespace ConfirmDeleteChar;

    String name = nameEdit_->GetText();
    if (name.Compare(name_) != 0)
    {
        using MsgBox = Urho3D::MessageBox;
        /* MsgBox* msgBox = */ new MsgBox(context_, "The entered name does not match the Character name.",
            "Error");
        return;
    }

    VariantMap& newEventData = GetEventDataMap();
    newEventData[P_OK] = true;
    newEventData[P_UUID] = uuid_;
    newEventData[P_NAME] = name_;
    SendEvent(E_CONFIRMDELETECHAR, newEventData);
    DeleteCharacter();
    Close();
}

void ConfirmDeleteCharacter::HandleCancelClicked(StringHash, VariantMap&)
{
    using namespace ConfirmDeleteChar;

    VariantMap& newEventData = GetEventDataMap();
    newEventData[P_OK] = false;
    newEventData[P_UUID] = uuid_;
    newEventData[P_NAME] = name_;
    SendEvent(E_CONFIRMDELETECHAR, newEventData);
    Close();
}

void ConfirmDeleteCharacter::DeleteCharacter()
{
    auto* client = GetSubsystem<FwClient>();
    client->DeleteCharacter(uuid_);
}
