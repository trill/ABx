/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WindowManager.h"
#include "AccountChestDialog.h"
#include "ActorResourceBar.h"
#include "ChatWindow.h"
#include "CraftsmanWindow.h"
#include "DamageWindow.h"
#include "EffectsWindow.h"
#include "MaintainedEffectsWindow.h"
#include "EquipmentWindow.h"
#include "FriendListWindow.h"
#include "GameMenu.h"
#include "GameMessagesWindow.h"
#include "GuildWindow.h"
#include "HeroWindow.h"
#include "InventoryWindow.h"
#include "MailWindow.h"
#include "MapWindow.h"
#include "MerchantWindow.h"
#include "MissionMapWindow.h"
#include "NewMailWindow.h"
#include "Options.h"
#include "OptionsWindow.h"
#include "PartyWindow.h"
#include "PingDot.h"
#include "ScoreChartWindow.h"
#include "SkillBarWindow.h"
#include "SkillsWindow.h"
#include "TargetWindow.h"
#include "SkillMonitor.h"

WindowManager::WindowManager(Context* context) :
    Object(context)
{
}

WindowManager::~WindowManager() = default;

SharedPtr<UIElement> WindowManager::GetWindow(const StringHash& hash, bool addToUi /* = false */, bool canCreate /* = true */)
{
    Options* opts = GetSubsystem<Options>();
    if (!windows_.Contains(hash) && canCreate)
    {
        if (hash == WINDOW_OPTIONS)
        {
            SharedPtr<OptionsWindow> wnd = MakeShared<OptionsWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_CHAT)
        {
            SharedPtr<ChatWindow> wnd = MakeShared<ChatWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_MAIL)
        {
            SharedPtr<MailWindow> wnd = MakeShared<MailWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_NEWMAIL)
        {
            SharedPtr<NewMailWindow> wnd = MakeShared<NewMailWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_INVENTORY)
        {
            SharedPtr<InventoryWindow> wnd = MakeShared<InventoryWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
            if (wnd->IsVisible())
                wnd->GetInventory();
        }
        else if (hash == WINDOW_EQUIPMENT)
        {
            SharedPtr<EquipmentWindow> wnd = MakeShared<EquipmentWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_SKILLS)
        {
            SharedPtr<SkillsWindow> wnd = MakeShared<SkillsWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_SCORE_CHART)
        {
            SharedPtr<ScoreChartWindow> wnd = MakeShared<ScoreChartWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_PARTY)
        {
            SharedPtr<PartyWindow> wnd = MakeShared<PartyWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_FRIENDLIST)
        {
            SharedPtr<FriendListWindow> wnd = MakeShared<FriendListWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
            if (wnd->IsVisible())
                wnd->GetList();
        }
        else if (hash == WINDOW_GUILD)
        {
            SharedPtr<GuildWindow> wnd = MakeShared<GuildWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
            if (wnd->IsVisible())
                wnd->UpdateAll();
        }
        else if (hash == WINDOW_HERO)
        {
            SharedPtr<HeroWindow> wnd = MakeShared<HeroWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
            if (wnd->IsVisible())
                wnd->UpdateAll();
        }
        else if (hash == WINDOW_PINGDOT)
        {
            SharedPtr<PingDot> wnd = MakeShared<PingDot>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_TARGET)
        {
            SharedPtr<TargetWindow> wnd = MakeShared<TargetWindow>(context_);
            wnd->SetVisible(false);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_SKILL_MONITOR)
        {
            SharedPtr<SkillMonitor> wnd = MakeShared<SkillMonitor>(context_);
            wnd->SetVisible(false);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_GAMEMESSAGES)
        {
            SharedPtr<GameMessagesWindow> wnd = MakeShared<GameMessagesWindow>(context_);
            wnd->SetVisible(false);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_EFFECTS)
        {
            SharedPtr<EffectsWindow> wnd = MakeShared<EffectsWindow>(context_);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_MAINTANED_EFFECTS)
        {
            SharedPtr<MaintainedEffectsWindow> wnd = MakeShared<MaintainedEffectsWindow>(context_);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_MISSIONMAP)
        {
            SharedPtr<MissionMapWindow> wnd = MakeShared<MissionMapWindow>(context_);
            wnd->SetVisible(true);
            opts->LoadWindow(wnd);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_SKILLBAR)
        {
            SharedPtr<SkillBarWindow> wnd = MakeShared<SkillBarWindow>(context_);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_HEALTHBAR)
        {
            SharedPtr<ActorHealthBar> wnd = MakeShared<ActorHealthBar>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_ENERGYBAR)
        {
            SharedPtr<ActorEnergyBar> wnd = MakeShared<ActorEnergyBar>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_XPBAR)
        {
            SharedPtr<ActorXPBar> wnd = MakeShared<ActorXPBar>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
        else if (hash == WINDOW_DAMAGE)
        {
            SharedPtr<DamageWindow> wnd = MakeShared<DamageWindow>(context_);
            opts->LoadWindow(wnd);
            wnd->SetVisible(true);
            windows_[hash] = wnd;
        }
    }

    if (windows_.Contains(hash))
    {
        auto wnd = windows_[hash];
        if (addToUi)
        {
            UIElement* root = GetSubsystem<UI>()->GetRoot();
            if (!root->GetChild(wnd->GetName()))
                root->AddChild(wnd);
        }
        return wnd;
    }
    return SharedPtr<UIElement>();
}

SharedPtr<DialogWindow> WindowManager::GetDialog(AB::Dialogs dialog, bool canCreate)
{
    SharedPtr<DialogWindow> result;
    switch (dialog)
    {
    case AB::DialogUnknown:
        break;
    case AB::DialogAccountChest:
    {
        UIElement* root = GetSubsystem<UI>()->GetRoot();
        UIElement* wnd = root->GetChild(AccountChestDialog::GetTypeNameStatic());
        if (!wnd && canCreate)
        {
            wnd = new AccountChestDialog(context_);
            root->AddChild(wnd);
        }
        result = dynamic_cast<DialogWindow*>(wnd);
        break;
    }
    case AB::DialogMerchantItems:
    {
        CloseDialog<CraftsmanWindow>();
        UIElement* root = GetSubsystem<UI>()->GetRoot();
        UIElement* wnd = root->GetChild(MerchantWindow::GetTypeNameStatic());
        if (!wnd && canCreate)
        {
            wnd = new MerchantWindow(context_);
            root->AddChild(wnd);
        }
        result = dynamic_cast<DialogWindow*>(wnd);
        break;
    }
    case AB::DialogCraftsmanItems:
    {
        CloseDialog<MerchantWindow>();
        UIElement* root = GetSubsystem<UI>()->GetRoot();
        UIElement* wnd = root->GetChild(CraftsmanWindow::GetTypeNameStatic());
        if (!wnd && canCreate)
        {
            wnd = new CraftsmanWindow(context_);
            root->AddChild(wnd);
        }
        result = dynamic_cast<DialogWindow*>(wnd);
        break;
    }
    default:
        break;
    }
    return result;
}

void WindowManager::SaveWindows()
{
    Options* opts = GetSubsystem<Options>();
    for (const auto& wnd : windows_)
    {
        opts->SaveWindow(wnd.second_);
    }
}
