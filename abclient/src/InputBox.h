/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"

URHO3D_EVENT(E_INPUTBOXDONE, InputBoxDone)
{
    URHO3D_PARAM(P_OK, Ok);              // bool
    URHO3D_PARAM(P_VALUE, Value);        // String
    URHO3D_PARAM(P_ELEMENT, Element);
}

class InputBox : public DialogWindow
{
    URHO3D_OBJECT(InputBox, DialogWindow)
private:
    void HandleOkClicked(StringHash eventType, VariantMap& eventData);
    void HandleCancelClicked(StringHash eventType, VariantMap& eventData);
    void HandleEditTextFinished(StringHash eventType, VariantMap& eventData);
    void HandleEditTextChanged(StringHash eventType, VariantMap& eventData);
public:
    static void RegisterObject(Context* context);
    InputBox(Context* context);
    InputBox(Context* context, const String& title);
    ~InputBox() override;
    void SelectAll();
    void SetValue(const String& value);
    const String& GetValue() const;
};
