/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TargetWindow.h"
#include "Actor.h"
#include "FwClient.h"

void TargetWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<TargetWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(UIElement);
}

TargetWindow::TargetWindow(Context* context) :
    UIElement(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile *chatFile = cache->GetResource<XMLFile>("UI/TargetWindow.xml");
    LoadChildXML(chatFile->GetRoot(), nullptr);
    SetName("TargetWindow");

    SetAlignment(HA_CENTER, VA_TOP);

    healthBar_ = GetChildStaticCast<HealthBar>("TargetHealthBar", true);
    healthBar_->SetAlignment(HA_LEFT, VA_TOP);
    healthBar_->SetStyle("HealthBar");
    healthBar_->SetShowName(true);
    healthBar_->showEffects_ = true;

    tradeButton_ = GetChildStaticCast<Button>("TradeButton", true);
    tradeButton_->SetVisible(false);
    tradeButton_->SetFocusMode(FM_NOTFOCUSABLE);

    Button* clearTarget = GetChildStaticCast<Button>("ClearTargetButton", true);
    SubscribeToEvent(clearTarget, E_RELEASED, URHO3D_HANDLER(TargetWindow, HandleClearTargetClicked));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(TargetWindow, HandleUpdate));

    SubscribeToEvent(tradeButton_, E_RELEASED, URHO3D_HANDLER(TargetWindow, HandleTradeClicked));
    UpdateLayout();
}

TargetWindow::~TargetWindow()
{
    UnsubscribeFromAllEvents();
}

void TargetWindow::HandleClearTargetClicked(StringHash, VariantMap&)
{
    VariantMap& e = GetEventDataMap();
    SendEvent(E_TARGETWINDOW_UNSELECT, e);
}

void TargetWindow::HandleTradeClicked(StringHash, VariantMap&)
{
    if (!tradeButton_->IsVisible())
        return;
    if (SharedPtr<Actor> a = target_.Lock())
    {
        if (a->objectType_ != ObjectType::Player)
            return;

        auto* client = GetSubsystem<FwClient>();
        client->TradeRequest(a->gameId_);
    }
}

void TargetWindow::HandleUpdate(StringHash, VariantMap&)
{
}

void TargetWindow::SetTarget(SharedPtr<Actor> target)
{
    target_ = target;
    healthBar_->SetActor(target);
    if (target.NotNull())
    {
        if (target->objectType_ == ObjectType::Player)
            tradeButton_->SetVisible(true);
        else
            tradeButton_->SetVisible(false);
        SetVisible(true);
    }
    else
        SetVisible(false);
}
