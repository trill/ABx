/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Actor.h"
#include "ActorResourceBar.h"
#include "FwClient.h"
#include "UIPriorities.h"
#include <libshared/Mechanic.h>
#include <sa/StringTempl.h>
#include <sa/StringTempl.h>

void ActorResourceBar::RegisterObject(Context* context)
{
    context->RegisterFactory<ActorResourceBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
    ActorHealthBar::RegisterObject(context);
    ActorEnergyBar::RegisterObject(context);
    ActorXPBar::RegisterObject(context);
}

ActorResourceBar::ActorResourceBar(Context* context) :
    Window(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    SetLayoutMode(LM_VERTICAL);
    SetPivot(0, 0);
    SetOpacity(1.0f);
    SetPosition({ 0, 0 });
    SetSize({ 200, 14 });
    SetMinSize({ 200, 14 });
    Texture2D* tex = cache->GetResource<Texture2D>("Textures/UI.png");
    SetTexture(tex);
    SetImageRect({ 48, 0, 64, 16 });
    SetBorder(IntRect(4, 4, 4, 4));
    SetPriority(Priorities::ValueBar);
    SetBringToFront(false);
    // FIXME: Why isn't it resizeable/moveable anymore?
    SetResizable(true);
    SetMovable(true);

    bar_ = CreateChild<ValueBar>();
    bar_->selectable_ = false;
    bar_->SetShowText(true);
    bar_->text_->SetFontSize(8);

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ActorResourceBar, HandleUpdate));
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(ActorResourceBar, HandleMouseUp));
}

ActorResourceBar::~ActorResourceBar()
{
    UnsubscribeFromAllEvents();
}

void ActorResourceBar::SetActor(SharedPtr<Actor> actor)
{
    actor_ = actor;
}

void ActorResourceBar::SetResourceType(ResourceType type)
{
    if (type_ == type)
        return;

    type_ = type;
    switch (type_)
    {
    case ResourceType::Health:
        bar_->SetStyle("HealthBar");
        break;
    case ResourceType::Energy:
        bar_->SetStyle("EnergyBar");
        break;
    case ResourceType::XP:
        bar_->SetStyle("XPBar");
        break;
    case ResourceType::None:
        break;
    }
}

void ActorResourceBar::HandleUpdate(StringHash, VariantMap&)
{
    if (auto actor = actor_.Lock())
    {
        String text;
        switch (type_)
        {
        case ResourceType::Health:
            bar_->SetValues(actor->stats_.maxHealth, actor->stats_.health);
            text.AppendWithFormat("%s %d %s",
                (actor->stats_.healthRegen < 0) ? sa::StringOfChar<char>(-actor->stats_.healthRegen, '<').c_str() : "",
                actor->stats_.health,
                (actor->stats_.healthRegen > 0) ? sa::StringOfChar<char>(actor->stats_.healthRegen, '>').c_str() : ""
                );
            break;
        case ResourceType::Energy:
            bar_->SetValues(actor->stats_.maxEnergy, actor->stats_.energy);
            text.AppendWithFormat("%s %d %s",
                (actor->stats_.energyRegen < 0) ? sa::StringOfChar<char>(-actor->stats_.energyRegen, '<').c_str() : "",
                actor->stats_.energy,
                (actor->stats_.energyRegen > 0) ? sa::StringOfChar<char>(actor->stats_.energyRegen, '>').c_str() : ""
            );
            break;
        case ResourceType::XP:
            bar_->SetValues(Game::SKILLPOINT_ADVANCE_XP, actor->stats_.xp % Game::SKILLPOINT_ADVANCE_XP);
            text.AppendWithFormat("%s XP", sa::FormatWithThousandSep<char>(actor->stats_.xp).c_str());
            break;
        case ResourceType::None:
            break;
        }
        bar_->SetText(text);
    }
}

void ActorResourceBar::HandleMouseUp(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;
    auto* input = GetSubsystem<Input>();
    if (!input->GetKeyDown(KEY_LCTRL))
        return;

    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || (elem != this && !elem->IsChildOf(this)))
        return;

    if (!IsInside(input->GetMousePosition(), true))
        return;
    FwClient* client = GetSubsystem<FwClient>();
    switch (type_)
    {
    case ResourceType::Health:
        client->PingHealth();
        break;
    case ResourceType::Energy:
        client->PingEnergy();
        break;
    case ResourceType::XP:
        client->PingXP();
        break;
    default:
        break;
    }
}

void ActorHealthBar::RegisterObject(Context* context)
{
    context->RegisterFactory<ActorHealthBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(ActorResourceBar);
}

ActorHealthBar::ActorHealthBar(Context* context) :
    ActorResourceBar(context)
{
    SetName("ActorHealthBar");
    SetResourceType(ResourceType::Health);
}

ActorHealthBar::~ActorHealthBar()
{
}

void ActorEnergyBar::RegisterObject(Context* context)
{
    context->RegisterFactory<ActorEnergyBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(ActorResourceBar);
}

ActorEnergyBar::ActorEnergyBar(Context* context) :
    ActorResourceBar(context)
{
    SetName("ActorEnergyBar");
    SetResourceType(ResourceType::Energy);
}

ActorEnergyBar::~ActorEnergyBar()
{
}

void ActorXPBar::RegisterObject(Context* context)
{
    context->RegisterFactory<ActorXPBar>();
    URHO3D_COPY_BASE_ATTRIBUTES(ActorResourceBar);
}

ActorXPBar::ActorXPBar(Context* context) :
    ActorResourceBar(context)
{
    SetName("ActorXPBar");
    SetResourceType(ResourceType::XP);
}

ActorXPBar::~ActorXPBar()
{
}
