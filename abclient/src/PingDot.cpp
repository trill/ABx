/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PingDot.h"
#include "FwClient.h"
#include "Conversions.h"
#include "UIPriorities.h"
#include <sa/Compiler.h>
#include <sa/TemplateParser.h>

//#include <Urho3D/DebugNew.h>

const IntRect PingDot::PING_NONE(0, 0, 32, 32);
const IntRect PingDot::PING_GOOD(32, 0, 64, 32);
const IntRect PingDot::PING_OKAY(64, 0, 96, 32);
const IntRect PingDot::PING_BAD(96, 0, 128, 32);

void PingDot::RegisterObject(Context* context)
{
    context->RegisterFactory<PingDot>();
    URHO3D_COPY_BASE_ATTRIBUTES(Button);
}

PingDot::PingDot(Context* context) :
    Button(context),
    lastUpdate_(0.0f)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile *chatFile = cache->GetResource<XMLFile>("UI/PingTooltip.xml");
    LoadChildXML(chatFile->GetRoot(), nullptr);
    SetName("PingDot");
    tooltipText_ = GetChildStaticCast<Text>("TooltipText", true);
    auto tex = cache->GetResource<Texture2D>("Textures/PingDot.png");
    SetTexture(tex);
    SetImageRect(PingDot::PING_NONE);
    SetPriority(Priorities::PingDot);
    SetSize(IntVector2(16, 16));
    SetAlignment(HA_RIGHT, VA_BOTTOM);
}

PingDot::~PingDot()
{
    UnsubscribeFromAllEvents();
}

void PingDot::Update(float timeStep)
{
    lastUpdate_ += timeStep;
    if (lastUpdate_ >= 1.0f)
    {
        FwClient* c = GetSubsystem<FwClient>();
        Time* t = GetSubsystem<Time>();
        int fps = static_cast<int>(1.0f / t->GetTimeStep());
        int lastPing = c->GetLastPing();
        if (lastPing < 110)
            SetImageRect(PingDot::PING_GOOD);
        else if (lastPing < 300)
            SetImageRect(PingDot::PING_OKAY);
        else
            SetImageRect(PingDot::PING_BAD);

        static sa::templ::Tokens tokens;
        if (tokens.IsEmpty())
        {
            static constexpr const char* TEMPLATE = "FPS: ${fps}\n\nAverage Ping: ${avg_ping}ms\nLast Ping: ${last_ping}ms";
            sa::templ::Parser parser;
            tokens = parser.Parse(TEMPLATE);
        }
        const std::string text = tokens.ToString([&](const sa::templ::Token& token) -> std::string
        {
            switch (token.type)
            {
            case sa::templ::Token::Type::Variable:
                if (token.value == "fps")
                    return std::to_string(fps);
                if (token.value == "avg_ping")
                    return std::to_string(c->GetAvgPing());
                if (token.value == "last_ping")
                    return std::to_string(c->GetLastPing());
                ASSERT_FALSE();
            default:
                return token.value;
            }
        });
        tooltipText_->SetText(ToUrhoString(text));

        lastUpdate_ = 0.0f;
    }
    Button::Update(timeStep);
}
