/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "MultiLineEdit.h"
#include <Urho3DAll.h>
#include <map>
#include "FwClient.h"
#include "AB/Entities/ConcreteItem.h"
#include "ItemUIElement.h"

inline constexpr size_t MAX_ATTACHMENTS = 7;

class NewMailWindow : public Window
{
    URHO3D_OBJECT(NewMailWindow, Window)
public:
    static void RegisterObject(Context* context);

    NewMailWindow(Context* context);
    ~NewMailWindow() override;
    void SetRecipient(const String& value);
    void SetSubject(const String& value);
    const String& GetSubject() const;
    void FocusBody();
    void SetBody(const String& value);
    void FocusRecipient();
    void FocusSubject();
    bool DropItem(const IntVector2& screenPos, ConcreteItem&& ci);
    void ClearAttachments();
private:
    SharedPtr<MultiLineEdit> mailBody_;
    SharedPtr<LineEdit> recipient_;
    SharedPtr<LineEdit> subject_;
    std::map<uint16_t, ConcreteItem> attachments_;
    SharedPtr<Window> dragItem_;

    void SubscribeToEvents();
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleSendClicked(StringHash eventType, VariantMap& eventData);
    void HandleItemDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleItemDragMove(StringHash eventType, VariantMap& eventData);
    void HandleItemDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleItemDragEnd(StringHash eventType, VariantMap& eventData);
    int FindFreeSlot(UIElement* container);
    bool RemoveItem(UIElement* container, unsigned pos);
    UIElement* GetItemFromPos(UIElement* container, unsigned pos);
    ItemUIElement* CreateItem(UIElement* container, int index, const ConcreteItem& iItem);
};

