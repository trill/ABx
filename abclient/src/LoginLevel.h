/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "BaseLevel.h"

class LoginLevel final : public BaseLevel
{
    URHO3D_OBJECT(LoginLevel, BaseLevel)
public:
    /// Construct.
    LoginLevel(Context* context);
    void CreateCamera();
    void CreateEnvironmentsList();
    void ShowError(const String& message, const String& title = "Error") override;
protected:
    void SubscribeToEvents() override;
    void CreateUI() override;
    void SceneLoadingFinished() override;
private:
    struct LoginServers
    {
        String name;
        String host;
        uint16_t port;
    };
    Vector<LoginServers> servers_;
    int64_t lastPing_{ 0 };
    bool loggingIn_;
    SharedPtr<LineEdit> nameEdit_;
    SharedPtr<LineEdit> passEdit_;
    SharedPtr<Button> button_;
    SharedPtr<Button> createAccountButton_;
    SharedPtr<DropDownList> environmentsList_;
    SharedPtr<Button> pingDot_;
    SharedPtr<Text> pingText_;
    void PingServers();
    void CreateScene() override;
    void HandleLoginClicked(StringHash eventType, VariantMap& eventData);
    void HandleCreateAccountClicked(StringHash eventType, VariantMap& eventData);
    void HandleExitClicked(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleTextFinished(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleServerPing(StringHash eventType, VariantMap& eventData);
    void DoLogin();
    void SetEnvironment();
    Text* CreateDropdownItem(const String& text, const String& value);
};
