/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

URHO3D_EVENT(E_DIALOGCLOSE, DialogClose)
{
    URHO3D_PARAM(P_ELEMENT, Element);
}

class DialogWindow : public Window
{
    URHO3D_OBJECT(DialogWindow, Window)
private:
    SharedPtr<Window> overlay_;
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
protected:
    void LoadWindow(Window* wnd, const String& fileName);
    UIElement* uiRoot_;
    void SubscribeEvents();
    void LoadLayout(const String& fileName);
    void MakeModal();
public:
    static void RegisterObject(Context* context);
    DialogWindow(Context* context);
    ~DialogWindow() override;
    virtual void Initialize(uint32_t) { }
    void Close();
    void Center();
};

