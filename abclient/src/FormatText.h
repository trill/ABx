/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class FormatTextElelement : public Text
{
    URHO3D_OBJECT(FormatTextElelement, Text)
public:
    FormatTextElelement(Context* context);
    ~FormatTextElelement() override;
    String linkTarget_;
};

class FormatText : public UISelectable
{
    URHO3D_OBJECT(FormatText, UISelectable);
public:
    static void RegisterObject(Context* context);

    FormatText(Context* context);
    ~FormatText() override;

    void SetText(const String& value);
private:
};
