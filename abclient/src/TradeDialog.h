/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "DialogWindow.h"
#include "AB/Entities/ConcreteItem.h"
#include <set>
#include "FwClient.h"

class Actor;
class Player;
class NumberInputBox;
class ItemUIElement;

inline constexpr size_t MAX_TRADE_ITEMS = 7;

class TradeDialog : public DialogWindow
{
    URHO3D_OBJECT(TradeDialog, DialogWindow)
private:
    WeakPtr<Player> player_;
    WeakPtr<Actor> partner_;
    SharedPtr<Window> dragItem_;
    SharedPtr<NumberInputBox> inputBox_;
    std::map<uint16_t, ConcreteItem> ourOffer_;
    std::set<uint32_t> partnerOffer_;
    bool offered_{ false };
    bool gotOffer_{ false };
    void HandleOfferClicked(StringHash eventType, VariantMap& eventData);
    void HandleAcceptClicked(StringHash eventType, VariantMap& eventData);
    void HandleCancelClicked(StringHash eventType, VariantMap& eventData);
    void HandleMoneyEditTextEntry(StringHash eventType, VariantMap& eventData);
    void HandlePartnersOffer(StringHash eventType, VariantMap& eventData);
    void HandleItemDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleItemDragMove(StringHash eventType, VariantMap& eventData);
    void HandleItemDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleItemDragEnd(StringHash eventType, VariantMap& eventData);
    void HandleInputDialogClosed(StringHash eventType, VariantMap& eventData);
    void HandleInputDialogDone(StringHash eventType, VariantMap& eventData);

    uint32_t GetOfferedMoney() const;
    ItemUIElement* CreateItem(UIElement* container, int index, const ConcreteItem& iItem);
    int FindFreeSlot(UIElement* container);
    bool RemoveItem(UIElement* container, unsigned pos);
    UIElement* GetItemFromPos(UIElement* container, unsigned pos);
    void EnableOfferButton(bool value);
    void EnableAcceptButton();
    void ShowCountDialog(uint16_t pos, int max);
public:
    static void RegisterObject(Context* context);
    TradeDialog(Context* context);
    TradeDialog(Context* context, SharedPtr<Player> player, SharedPtr<Actor> partner);
    ~TradeDialog() override;
    bool DropItem(const IntVector2& screenPos, ConcreteItem&& ci);
};
