/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

class FadeWindow : public Window
{
    URHO3D_OBJECT(FadeWindow, Window)
public:
    FadeWindow(Context* context);
    ~FadeWindow() override;
    void SetScene(Scene* scene);
    void SetBackground(const String& uuid);
private:
    Scene* scene_{ nullptr };
    SharedPtr<BorderImage> bgContainer_;
    String uuid_;
    void FitBackground();
    void HandleAsyncLoadProgress(StringHash eventType, VariantMap& eventData);
    void HandleAsyncLoadFinished(StringHash eventType, VariantMap& eventData);
};

