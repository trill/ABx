/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3D/Urho3DAll.h>
#include <libshared/Items.h>
#include "FwClient.h"

class ItemStatsUIElement final : public UIElement
{
    URHO3D_OBJECT(ItemStatsUIElement, UIElement)
public:
    static void RegisterObject(Context* context);

    ItemStatsUIElement(Context* context);
    ~ItemStatsUIElement() override;

    void SetStats(const ItemStats& value);
private:
    void AddDamageStats(const ItemStats& stats);
    void AddArmorStats(const ItemStats& stats);
    void AddAttributeIncreaseStats(const ItemStats& stats);
    void AddOtherStats(const ItemStats& stats);
};

