/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Skill.h>
#include <sa/Bits.h>

struct Skill
{
    std::string uuid;
    uint32_t index{ 0 };
    uint32_t access{ AB::Entities::SkillAccessNone };
    std::string name;
    std::string attributeUuid;
    std::string professionUuid;
    AB::Entities::SkillType type{ AB::Entities::SkillTypeSkill };
    bool isElite{ false };
    bool isMaintainable{ false };
    std::string description;
    std::string shortDescription;
    std::string icon;
    std::string soundEffect;
    std::string particleEffect;
    int32_t activation{ 0 };
    int32_t recharge{ 0 };
    int32_t costEnergy{ 0 };
    int32_t costEnergyRegen{ 0 };
    int32_t costAdrenaline{ 0 };
    int32_t costOvercast{ 0 };
    int32_t costHp{ 0 };
};

inline bool IsSkillType(const Skill& skill, AB::Entities::SkillType type)
{
    if (type == AB::Entities::SkillTypeSkill)
        return true;
    return sa::bits::is_set(skill.type, type);
}
