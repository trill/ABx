/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

struct Priorities
{
    enum
    {
        Default = 0,
        PingDot,
        ChatWindow,
        SkillBar,
        EffectsWindow,
        MaintainedEffectsWindow,
        DamageWindow,
        SkillMonitor = DamageWindow,
        ValueBar,
        StandardWindow,

        ActorName,
        ActorNumber,
        SpeechBubble,
        GameMenu,

        GameMessages,
        DragElement,

        ModalBackdrop,

        MapWindow,

        FadeWindow = 2000
    };
};
