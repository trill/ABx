/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "EffectsWindow.h"
#include "FwClient.h"
#include "SkillManager.h"
#include "LevelManager.h"
#include "Player.h"
#include "SkillTooltip.h"
#include "EffectTooltip.h"
#include "ProgressElement.h"
#include "UIPriorities.h"

static constexpr unsigned ICON_SIZE = 50;

void EffectsWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<EffectsWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(UIElement);
}

EffectsWindow::EffectsWindow(Context* context) :
    UIElement(context),
    effectCount_(0)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile *file = cache->GetResource<XMLFile>("UI/EffectsWindow.xml");
    LoadChildXML(file->GetRoot(), nullptr);
    SetName("EffectsWindow");

    SetAlignment(HA_CENTER, VA_BOTTOM);
    SetPosition(0, -120);
    SetLayoutMode(LM_HORIZONTAL);
    SetLayoutBorder(IntRect(4, 4, 4, 4));
    SetLayoutSpacing(4);
    SetPivot(0, 0);
    SetOpacity(0.9f);
    SetMinSize(ICON_SIZE, ICON_SIZE);
    SetVisible(false);
    SetPriority(Priorities::EffectsWindow);
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(EffectsWindow, HandleMouseUp));
    SubscribeToEvent(Events::E_OBJECTRESOURCECHANGED, URHO3D_HANDLER(EffectsWindow, HandleObjectResourceChange));
}

EffectsWindow::~EffectsWindow()
{
    UnsubscribeFromAllEvents();
}

void EffectsWindow::HandleObjectResourceChange(StringHash, VariantMap& eventData)
{
    LevelManager* lm = GetSubsystem<LevelManager>();
    Player* player = lm->GetPlayer();
    if (!player)
        return;
    using namespace Events::ObjectResourceChanged;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    if (objectId != player->gameId_)
        return;
    AB::GameProtocol::ResourceType resType = static_cast<AB::GameProtocol::ResourceType>(eventData[P_RESTYPE].GetUInt());
    if (resType != AB::GameProtocol::ResourceType::Morale)
        return;

    int32_t value = eventData[P_VALUE].GetInt();
    auto* txt = GetChildDynamicCast<Text>("MoraleValueText", true);
    if (txt)
        txt->SetText(String(value) + "%");
}

void EffectsWindow::EffectAdded(uint32_t sourceId, uint32_t effectIndex, uint32_t ticks)
{
    SkillManager* sm = GetSubsystem<SkillManager>();
    const Effect* effect = sm->GetEffectByIndex(effectIndex);
    if (!effect)
        return;

    String name = "Effect_" + String(effectIndex);
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    ++effectCount_;

    Button* effectIcon = GetChildStaticCast<Button>(name, true);
    if (!effectIcon)
        effectIcon = CreateChild<Button>(name);

    auto* progress = effectIcon->GetChildStaticCast<ProgressElement>("ProgressElement", true);
    if (!progress)
        progress = effectIcon->CreateChild<ProgressElement>("ProgressElement");

    effectIcon->SetVar("Index", effectIndex);
    effectIcon->SetVar("Ticks", ticks);
    effectIcon->SetFixedSize(ICON_SIZE, ICON_SIZE);
    effectIcon->SetOpacity(1.0f);

    progress->SetVar("Index", effectIndex);
    progress->SetPosition({ 0, 0 });
    progress->SetFixedSize(ICON_SIZE, ICON_SIZE);
    progress->SetMaxTicks(ticks);
    progress->Start();

    bool isEnch = false;
    bool isHex = false;

    const auto* skill = sm->GetSkillByIndex(effectIndex);
    if (skill)
    {
        auto* lm = GetSubsystem<LevelManager>();
        auto* sourceActor = lm->GetActor(sourceId);
        SkillTooltip* stt = effectIcon->GetChildStaticCast<SkillTooltip>("SkillTooltip", true);
        if (!stt)
            stt = effectIcon->CreateChild<SkillTooltip>("SkillTooltip");
        stt->SetSkill(skill, sourceActor, effect->category);
        if (IsSkillType(*skill, AB::Entities::SkillTypeEnchantment))
            isEnch = true;
        else if (IsSkillType(*skill, AB::Entities::SkillTypeHex))
            isHex = true;
    }
    else
    {
        EffectTooltip* ett = effectIcon->GetChildStaticCast<EffectTooltip>("EffectTooltip", true);
        if (!ett)
            ett = effectIcon->CreateChild<EffectTooltip>("EffectTooltip");
        ett->SetEffect(effect);
    }

    bool haveOverlay = false;
    if (isEnch || isHex)
    {
        Texture2D* tex = nullptr;
        if (isEnch)
            tex = cache->GetResource<Texture2D>("Textures/Effects/enchantment_frame.png");
        else if (isHex)
            tex = cache->GetResource<Texture2D>("Textures/Effects/hex_frame.png");
        if (tex)
        {
            auto* overlay = effectIcon->GetChildStaticCast<BorderImage>("OvlerayElement", true);
            if (!overlay)
            {
                overlay = effectIcon->CreateChild<BorderImage>("OvlerayElement");
                overlay->SetFixedSize(effectIcon->GetSize());
            }
            overlay->SetVar("Index", effectIndex);
            overlay->SetTexture(tex);
            overlay->SetFullImageRect();
            haveOverlay = true;
        }
    }

    Texture2D* icon = cache->GetResource<Texture2D>(String(effect->icon.c_str()));
    if (!icon)
        icon = cache->GetResource<Texture2D>("Textures/Skills/placeholder.png");
    icon->SetNumLevels(1);
    icon->SetMipsToSkip(QUALITY_LOW, 0);
    effectIcon->SetTexture(icon);
    if (haveOverlay)
        effectIcon->SetImageRect({ -6, -6, icon->GetWidth() + 6, icon->GetHeight() + 6 });
    else
        effectIcon->SetFullImageRect();
    effectIcon->SetLayoutMode(LM_FREE);
    if (effectIndex == AB::Entities::EFFECT_INDEX_MORALE)
    {
        LevelManager* lm = GetSubsystem<LevelManager>();
        Player* player = lm->GetPlayer();
        Text* value = effectIcon->CreateChild<Text>("MoraleValueText");
        value->SetAlignment(HA_LEFT, VA_BOTTOM);
        value->SetInternal(true);
        value->SetPosition({ 0, 0 });
        value->SetStyleAuto();
        value->SetTextEffect(TE_STROKE);
        value->SetEffectColor({ 0.2f, 0.2f, 0.2f });
        value->SetText(String(player->stats_.morale) + "%");
    }
    SetWidth(ICON_SIZE * effectCount_);
    SetVisible(effectCount_ != 0);
    UpdateLayout();
    UpdatePosition();
}

void EffectsWindow::EffectRemoved(uint32_t effectIndex)
{
    String name = "Effect_" + String(effectIndex);
    Button* effectIcon = GetChildStaticCast<Button>(name, true);
    if (effectIcon)
    {
        --effectCount_;
        RemoveChild(effectIcon);
        SetWidth(ICON_SIZE * effectCount_);
        SetVisible(effectCount_ != 0);
        UpdateLayout();
        UpdatePosition();
    }
}

void EffectsWindow::Clear()
{
    SetVisible(false);
    RemoveAllChildren();
    effectCount_ = 0;
    SetWidth(0);
}

void EffectsWindow::UpdatePosition()
{
    if (!IsVisible())
        return;
//    if (GetHorizontalAlignment() == HA_CENTER)
    {
        IntVector2 pos = GetPosition();
        pos.x_ = -200;
        SetPosition(pos);
    }
}

void EffectsWindow::HandleMouseUp(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;
    auto* input = GetSubsystem<Input>();
    if (!input->GetKeyDown(KEY_LCTRL))
        return;

    if (!IsInside(input->GetMousePosition(), true))
        return;

    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || !elem->IsChildOf(this))
        return;

    uint32_t index = elem->GetVar("Index").GetUInt();
    if (index == 0)
        return;

    FwClient* client = GetSubsystem<FwClient>();
    client->PingEffect(index);
}
