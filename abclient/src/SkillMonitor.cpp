/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "stdafx.h"
#include "SkillMonitor.h"
#include "SkillManager.h"
#include <sa/time.h>
#include "ValueBar.h"
#include "FwClient.h"
#include "SkillTooltip.h"
#include "UIPriorities.h"

void SkillMonitor::RegisterObject(Context* context)
{
    SkillMonitorItem::RegisterObject(context);
    context->RegisterFactory<SkillMonitor>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

SkillMonitor::SkillMonitor(Context* context) :
    Window(context)
{
    SetName("SkillMonitor");
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetAlignment(HA_CENTER, VA_TOP);
    SetLayoutMode(LM_VERTICAL);
    SetFocusMode(FM_NOTFOCUSABLE);
    Texture2D* tex = cache->GetResource<Texture2D>("Textures/UI.png");
    SetTexture(tex);
    SetImageRect(IntRect(0, 16, 16, 32));
    SetBorder(IntRect(4, 4, 4, 4));

    SetMinSize({ 300, 32 });
    SetPosition({ 5, 100 });
    SetPriority(Priorities::SkillMonitor);

    SubscribeToEvent(Events::E_OBJECTUSESKILL, URHO3D_HANDLER(SkillMonitor, HandleObjectUseSkill));
    SubscribeToEvent(Events::E_OBJECTENDUSESKILL, URHO3D_HANDLER(SkillMonitor, HandleObjectEndUseSkill));
    SubscribeToEvent(Events::E_OBJECTSKILLINTERRUPTED, URHO3D_HANDLER(SkillMonitor, HandleObjectSkillFailure));
    SubscribeToEvent(Events::E_SKILLFAILURE, URHO3D_HANDLER(SkillMonitor, HandleObjectSkillFailure));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(SkillMonitor, HandleUpdate));
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(SkillMonitor, HandleMouseUp));
}

SkillMonitor::~SkillMonitor()
{
}

void SkillMonitor::SetTarget(SharedPtr<Actor> target)
{
    target_ = target;
    RemoveAllChildren();
    if (target.Null())
        SetVisible(false);
}

void SkillMonitor::HandleObjectUseSkill(StringHash, VariantMap& eventData)
{
    auto target = target_.Lock();
    if (!target)
        return;

    using namespace Events::ObjectUseSkill;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    if (objectId != target->gameId_)
        return;

    int barIndex = eventData[P_SKILLINDEX].GetInt();
    uint32_t skillIndex = target->GetSkillIndex(barIndex);
    if (skillIndex == 0)
        return;

    auto* item = CreateChild<SkillMonitorItem>();
    item->startTick_ = sa::time::tick();
    item->index_ = skillIndex;
    item->activation_ = eventData[P_ACTIVATION].GetUInt();
    item->isUsing_ = true;
    if (!item->Initialize())
    {
        RemoveChild(item);
        return;
    }

    SetVisible(true);
}

void SkillMonitor::HandleObjectEndUseSkill(StringHash, VariantMap& eventData)
{
    auto target = target_.Lock();
    if (!target)
        return;

    using namespace Events::ObjectUseSkill;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    if (objectId != target->gameId_)
        return;

    int barIndex = eventData[P_SKILLINDEX].GetInt();
    uint32_t skillIndex = target->GetSkillIndex(barIndex);
    SkillStopped(skillIndex, true);
}

void SkillMonitor::HandleObjectSkillFailure(StringHash, VariantMap& eventData)
{
    auto target = target_.Lock();
    if (!target)
        return;

    using namespace Events::SkillFailure;
    uint32_t objectId = eventData[P_OBJECTID].GetUInt();
    if (objectId != target->gameId_)
        return;

    int barIndex = eventData[P_SKILLINDEX].GetInt();
    uint32_t skillIndex = target->GetSkillIndex(barIndex);
    SkillStopped(skillIndex, false);
}

void SkillMonitor::HandleUpdate(StringHash, VariantMap&)
{
    if (IsVisible() && GetNumChildren() == 0)
        SetVisible(false);
}

void SkillMonitor::HandleMouseUp(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetInt() != MouseButton::MOUSEB_LEFT)
        return;
    auto* input = GetSubsystem<Input>();
    if (!input->GetKeyDown(KEY_LCTRL))
        return;

    if (!IsInside(input->GetMousePosition(), true))
        return;

    auto target = target_.Lock();
    if (!target)
        return;

    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (!elem || !elem->IsChildOf(this))
        return;
    SkillMonitorItem* item = dynamic_cast<SkillMonitorItem*>(elem);
    if (!item)
    {
        for (elem = elem->GetParent(); elem && !item; elem = elem->GetParent())
            item = dynamic_cast<SkillMonitorItem*>(elem);
    }
    if (!item)
        return;

    uint32_t index = item->index_;
    if (index == 0)
        return;

    FwClient* client = GetSubsystem<FwClient>();
    client->PingTargetUsingSkill(target->gameId_, index);
}

void SkillMonitor::SkillStopped(uint32_t index, bool success)
{
    auto* item = FindItem(index);
    if (!item)
        return;

    item->success_ = success;
    item->isUsing_ = false;
}

void SkillMonitorItem::RegisterObject(Context* context)
{
    context->RegisterFactory<SkillMonitorItem>();
    URHO3D_COPY_BASE_ATTRIBUTES(UIElement);
}

SkillMonitorItem* SkillMonitor::FindItem(uint32_t index)
{
    for (auto& item : GetChildren())
    {
        SkillMonitorItem* smi = dynamic_cast<SkillMonitorItem*>(item.Get());
        if (!smi)
            continue;

        if (smi->index_ == index)
            return smi;
    }
    return nullptr;
}

SkillMonitorItem::SkillMonitorItem(Context* context) :
    UIElement(context)
{
    SetLayoutMode(LM_HORIZONTAL);
    SetLayoutBorder({ 4, 4, 4, 4 });
    SetLayoutSpacing(4);
    SetMaxHeight(ICON_SIZE);
}

SkillMonitorItem::~SkillMonitorItem()
{
}

bool SkillMonitorItem::Initialize()
{
    RemoveAllChildren();
    auto* sm = GetSubsystem<SkillManager>();
    auto* skill = sm->GetSkillByIndex(index_);
    if (!skill)
        return false;

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    Texture2D* icon = cache->GetResource<Texture2D>(String(skill->icon.c_str()));
    if (icon)
    {
        icon->SetNumLevels(1);
        icon->SetMipsToSkip(QUALITY_LOW, 0);
        Button* skillIcon = CreateChild<Button>("SkillIcon");
        skillIcon->SetInternal(true);
        skillIcon->SetTexture(icon);
        skillIcon->SetFullImageRect();
        skillIcon->SetBorder({ 4, 4, 4, 4 });
        skillIcon->SetMinSize(ICON_SIZE, ICON_SIZE);
        skillIcon->SetMaxSize(ICON_SIZE, ICON_SIZE);
        skillIcon->SetAlignment(HA_LEFT, VA_CENTER);
        SkillTooltip* tooltip = skillIcon->CreateChild<SkillTooltip>();
        tooltip->SetSkill(skill, nullptr);
        tooltip->SetPosition({ 0, skillIcon->GetHeight() });
    }

    progress_ = CreateChild<ValueBar>();
    progress_->SetStyle("SkillMonitorProgressUsing");
    progress_->SetAlignment(HA_LEFT, VA_CENTER);
    progress_->SetRange((float)activation_);
    progress_->SetValue(0.0f);
    progress_->SetText(skill->name.c_str());
    progress_->SetShowText(true);
    progress_->SetMaxHeight(18);

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(SkillMonitorItem, HandleUpdate));

    return true;
}

void SkillMonitorItem::HandleUpdate(StringHash, VariantMap&)
{
    if (isUsing_)
    {
        int64_t p = sa::time::tick() - startTick_;
        progress_->SetValue((float)p);
        if (p > (int64_t)(activation_) + 250)
        {
            // Didn't we get an end using skill event?
            doneSince_ = sa::time::tick();
            isUsing_ = false;
            success_ = true;
        }
    }
    else if (doneSince_ == 0)
    {
        doneSince_ = sa::time::tick();
        if (success_)
        {
            progress_->SetValue((float)activation_);
            progress_->SetStyle("SkillMonitorProgressSuccess");
        }
        else
            progress_->SetStyle("SkillMonitorProgressFail");
    }
    else
    {

        if (sa::time::time_elapsed(doneSince_) > SkillMonitor::KEEP_ITEMS_MS)
        {
            auto* input = GetSubsystem<Input>();
            UI* ui = GetSubsystem<UI>();
            auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
            bool hover = elem && (elem == this || elem->IsChildOf(this));
            if (!hover)
                Remove();
        }
    }
}
