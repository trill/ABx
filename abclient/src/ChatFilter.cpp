/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ChatFilter.h"
#include "Options.h"
#include <fstream>
#include <sa/StringTempl.h>

ChatFilter::ChatFilter(Context* context) :
    Object(context)
{ }

ChatFilter::~ChatFilter()
{ }

void ChatFilter::Load()
{
    auto* options = GetSubsystem<Options>();
    String filename = AddTrailingSlash(options->GetPrefPath()) + "chat_filter.txt";
    std::ifstream file(filename.CString());
    if (!file.is_open())
        return;
    std::string line;
    while (std::getline(file, line))
    {
        if (line.empty())
            continue;
        String pattern;
        pattern.AppendWithFormat("*%s*", line.c_str());
        filterPatterns_.Push(std::move(pattern));
    }
}

bool ChatFilter::Matches(const String& value)
{
    if (filterPatterns_.Size() == 0)
        return false;

    const std::string str = std::string(value.CString());
    for (const auto& pattern : filterPatterns_)
    {
        if (sa::PatternMatch(str, std::string(pattern.CString())))
            return true;
    }
    return false;
}
