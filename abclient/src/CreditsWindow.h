/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>

class CreditsWindow : public Window
{
    URHO3D_OBJECT(CreditsWindow, Window)
private:
    Vector<SharedPtr<UIElement>> credits_;
    SharedPtr<UIElement> creditsBase_;
    int totalCreditsHeight_;
    int creditLengthInSeconds_;
    Timer timer_;
    void CreateUI();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void CreateSingleLine(const String& content, int fontSize, bool bold = false);
    void CreateLogo(const String& file, float scale);
    void AddCredits();
public:
    static void RegisterObject(Context* context);
    static String NAME;
    CreditsWindow(Context* context);
    ~CreditsWindow() override;
    void Close();
};

