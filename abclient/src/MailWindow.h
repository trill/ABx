/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/MailList.h>
#include <AB/Packets/ServerPackets.h>
#include "MultiLineEdit.h"
#include "ItemUIElement.h"
#include <Urho3DAll.h>

class MailWindow : public Window
{
    URHO3D_OBJECT(MailWindow, Window)
public:
    static void RegisterObject(Context* context);

    MailWindow(Context* context);
    ~MailWindow() override
    {
        UnsubscribeFromAllEvents();
    }
    void GetHeaders();
    void FocusMainElement();
private:
    SharedPtr<ListView> mailList_;
    SharedPtr<MultiLineEdit> mailBody_;
    SharedPtr<Window> dragItem_;
    std::string currentMailUuid_;
    void SubscribeToEvents();
    void AddItem(const String& text, const String& style, const AB::Entities::MailHeader& header);
    void ClearItems(UIElement* container);
    void ShowItems(UIElement* container, const std::vector<AB::Packets::Server::Internal::UpgradeableItem>& items);
    bool RemoveItem(UIElement* container, unsigned pos);
    UIElement* GetItemFromPos(UIElement* container, unsigned pos);
    ItemUIElement* CreateItem(UIElement* container, int index, const ConcreteItem& iItem);
    void HandleItemDragBegin(StringHash eventType, VariantMap& eventData);
    void HandleItemDragMove(StringHash eventType, VariantMap& eventData);
    void HandleItemDragCancel(StringHash eventType, VariantMap& eventData);
    void HandleItemDragEnd(StringHash eventType, VariantMap& eventData);
    void HandleCloseClicked(StringHash eventType, VariantMap& eventData);
    void HandleMailInboxMessage(StringHash eventType, VariantMap& eventData);
    void HandleMailReadMessage(StringHash eventType, VariantMap& eventData);
    void HandleNewClicked(StringHash eventType, VariantMap& eventData);
    void HandleReplyClicked(StringHash eventType, VariantMap& eventData);
    void HandleDeleteClicked(StringHash eventType, VariantMap& eventData);
    void HandleItemSelected(StringHash eventType, VariantMap& eventData);
    void HandleItemUnselected(StringHash eventType, VariantMap& eventData);
    void HandleNewMail(StringHash eventType, VariantMap& eventData);
    void HandleMailAttachmentDelete(StringHash eventType, VariantMap& eventData);
};

