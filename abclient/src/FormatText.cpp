/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FormatText.h"
#include <Urho3D/ThirdParty/PugiXml/pugixml.hpp>

void FormatText::RegisterObject(Context* context)
{
    context->RegisterFactory<FormatText>();
    context->RegisterFactory<FormatTextElelement>();
}

FormatTextElelement::FormatTextElelement(Context* context) :
    Text(context)
{
}

FormatTextElelement::~FormatTextElelement()
{ }

FormatText::FormatText(Context* context) :
    UISelectable(context)
{
    SetLayoutMode(LM_FREE);
}

FormatText::~FormatText()
{
}

void FormatText::SetText(const String& value)
{
    (void)value;
}
