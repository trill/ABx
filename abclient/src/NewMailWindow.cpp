/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NewMailWindow.h"
#include "Shortcuts.h"
#include "FwClient.h"
#include "UIPriorities.h"
#include "Conversions.h"
#include "ItemsCache.h"
#include "ItemUIElement.h"

void NewMailWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<NewMailWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

NewMailWindow::NewMailWindow(Context* context) :
    Window(context)
{
    SetName("NewMailWindow");

    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* file = cache->GetResource<XMLFile>("UI/NewMailWindow.xml");
    LoadXML(file->GetRoot());

    Shortcuts* scs = GetSubsystem<Shortcuts>();
    Text* caption = GetChildStaticCast<Text>("CaptionText", true);
    caption->SetText(scs->GetCaption(Events::E_SC_TOGGLENEWMAILWINDOW, "New Mail", true));

    SetSize(500, 400);
    auto* graphics = GetSubsystem<Graphics>();
    SetPosition(graphics->GetWidth() - GetWidth() - 5, graphics->GetHeight() / 2 - (GetHeight() / 2));

    recipient_ = GetChildStaticCast<LineEdit>("RecipientEdit", true);
    subject_ = GetChildStaticCast<LineEdit>("SubjectEdit", true);

    UIElement* container = GetChild("EditorContainer", true);
    mailBody_ = container->CreateChild<MultiLineEdit>();
    mailBody_->SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    mailBody_->SetStyle("MultiLineEdit");
    mailBody_->SetPosition(0, 0);
    mailBody_->SetClipBorder({ 4, 4, 4, 4 });
    mailBody_->SetSize(container->GetSize());
    mailBody_->SetEditable(true);
    mailBody_->SetMultiLine(true);
    mailBody_->SetTextCopyable(true);
    mailBody_->SetTextSelectable(true);

    SetBringToBack(true);
    SetBringToFront(true);
    SetPriority(Priorities::StandardWindow);
    SetStyleAuto();

    UpdateLayout();

    SubscribeToEvents();
}

NewMailWindow::~NewMailWindow()
{
    UnsubscribeFromAllEvents();
}

void NewMailWindow::SubscribeToEvents()
{
    Button* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(NewMailWindow, HandleCloseClicked));
    Button* sendButton = GetChildStaticCast<Button>("SendMailButton", true);
    SubscribeToEvent(sendButton, E_RELEASED, URHO3D_HANDLER(NewMailWindow, HandleSendClicked));
}

void NewMailWindow::HandleCloseClicked(StringHash, VariantMap&)
{
    SetVisible(false);
}

void NewMailWindow::HandleSendClicked(StringHash, VariantMap&)
{
    const String recipient = recipient_->GetText().Trimmed();
    if (recipient.Empty())
    {
        recipient_->SetFocus(true);
        return;
    }
    String body = mailBody_->GetText().Trimmed();
    if (body.Empty())
    {
        mailBody_->SetFocus(true);
        return;
    }
    String subject = subject_->GetText();
    if (subject.Length() > AB::Entities::MAX_MAIL_SUBJECT)
        subject.Resize(AB::Entities::MAX_MAIL_SUBJECT);
    if (body.Length() > AB::Entities::MAX_MAIL_MESSAGE)
        body.Resize(AB::Entities::MAX_MAIL_MESSAGE);

    std::vector<uint16_t> att;
    for (const auto& a : attachments_)
        att.push_back(a.first);
    FwClient* client = GetSubsystem<FwClient>();
    client->SendMail(ToStdString(recipient), ToStdString(subject), ToStdString(body), att);

    SetVisible(false);
    recipient_->SetText(String::EMPTY);
    subject_->SetText(String::EMPTY);
    mailBody_->SetText(String::EMPTY);
}

void NewMailWindow::SetRecipient(const String& value)
{
    recipient_->SetText(value);
}

void NewMailWindow::SetSubject(const String& value)
{
    subject_->SetText(value);
}

const String& NewMailWindow::GetSubject() const
{
    return subject_->GetText();
}

void NewMailWindow::FocusBody()
{
    mailBody_->SetFocus(true);
}

void NewMailWindow::SetBody(const String& value)
{
    mailBody_->SetText(value);
}

void NewMailWindow::FocusRecipient()
{
    recipient_->GetTextElement()->SetSelection(0);
    recipient_->SetFocus(true);
}

void NewMailWindow::FocusSubject()
{
    subject_->GetTextElement()->SetSelection(0);
    subject_->SetFocus(true);
}

void NewMailWindow::ClearAttachments()
{
    attachments_.clear();
    auto* container = GetChild("AttachmentItems", true);
    for (size_t i = 0; i < MAX_ATTACHMENTS; ++i)
    {
        RemoveItem(container, i);
    }
}

bool NewMailWindow::DropItem(const IntVector2 &screenPos, ConcreteItem &&ci)
{
    if (ci.place != AB::Entities::StoragePlace::Inventory)
        return false;
    auto* itemsCache = GetSubsystem<ItemsCache>();
    auto item = itemsCache->Get(ci.index);
    if (!item)
        return false;
    if (!item->tradeAble_)
        return false;

    if (attachments_.size() >= MAX_ATTACHMENTS)
        return false;
    auto pos = ci.pos;
    if (attachments_.find(pos) != attachments_.end())
        return false;

    auto* container = GetChild("AttachmentItems", true);
    if (!container->IsInside(screenPos, true))
        return false;

    int freeSlot = FindFreeSlot(container);
    if (freeSlot == -1)
        return false;
    ItemUIElement* icon = CreateItem(container, freeSlot, ci);
    if (icon == nullptr)
        return false;

    attachments_.emplace(pos, std::move(ci));

    return true;
}

int NewMailWindow::FindFreeSlot(UIElement* container)
{
    for (size_t i = 0; i < MAX_ATTACHMENTS; ++i)
    {
        auto* itemContainer = container->GetChild("Item" + String(i + 1), true);
        if (!itemContainer)
            return -1;
        if (itemContainer->GetChildren().Size() == 0)
            return (int)i;
    }
    return -1;
}

ItemUIElement* NewMailWindow::CreateItem(UIElement* container, int index, const ConcreteItem& iItem)
{
    auto* itemsCache = GetSubsystem<ItemsCache>();
    auto item = itemsCache->Get(iItem.index);
    if (!item)
        return nullptr;

    auto* itemContainer = container->GetChild("Item" + String(index + 1), true);
    if (!itemContainer)
        return nullptr;

    ItemUIElement* itemElem = itemContainer->CreateChild<ItemUIElement>("ItemElememt");
    itemElem->SetPosition(4, 4);
    itemElem->SetSize(itemContainer->GetSize() - IntVector2(8, 8));
    itemElem->SetMinSize(itemElem->GetSize());
    itemElem->SetName(item->name_);
    itemElem->SetIcon(item->iconFile_);
    itemElem->SetPos(iItem.pos);
    itemElem->SetIndex(iItem.index);
    itemElem->SetCount(iItem.count);
    itemElem->SetValue(iItem.value);
    itemElem->SetStats(iItem.stats);

    SubscribeToEvent(itemElem, E_DRAGMOVE, URHO3D_HANDLER(NewMailWindow, HandleItemDragMove));
    SubscribeToEvent(itemElem, E_DRAGBEGIN, URHO3D_HANDLER(NewMailWindow, HandleItemDragBegin));
    SubscribeToEvent(itemElem, E_DRAGCANCEL, URHO3D_HANDLER(NewMailWindow, HandleItemDragCancel));
    SubscribeToEvent(itemElem, E_DRAGEND, URHO3D_HANDLER(NewMailWindow, HandleItemDragEnd));

    return itemElem;
}

void NewMailWindow::HandleItemDragBegin(StringHash, VariantMap& eventData)
{
    using namespace DragBegin;
    auto* item = static_cast<ItemUIElement*>(eventData[P_ELEMENT].GetVoidPtr());
    int buttons = eventData[P_BUTTONS].GetInt();
    int lx = eventData[P_X].GetInt();
    int ly = eventData[P_Y].GetInt();
    dragItem_ = item->GetDragItem(buttons, { lx, ly });
}

void NewMailWindow::HandleItemDragMove(StringHash, VariantMap& eventData)
{
    if (!dragItem_)
        return;
    using namespace DragMove;

    int buttons = eventData[P_BUTTONS].GetInt();
    auto* element = static_cast<ItemUIElement*>(eventData[P_ELEMENT].GetVoidPtr());
    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    int BUTTONS = element->GetVar("BUTTONS").GetInt();

    if (buttons == BUTTONS)
        dragItem_->SetPosition(IntVector2(X, Y) - dragItem_->GetSize() / 2);
}

void NewMailWindow::HandleItemDragCancel(StringHash, VariantMap&)
{
    using namespace DragCancel;
    if (!dragItem_)
        return;
    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragItem_.Get());
    dragItem_.Reset();
}

void NewMailWindow::HandleItemDragEnd(StringHash, VariantMap& eventData)
{
    using namespace DragEnd;
    if (!dragItem_)
        return;
    uint16_t pos = static_cast<uint16_t>(dragItem_->GetVar("Pos").GetUInt());

    int X = eventData[P_X].GetInt();
    int Y = eventData[P_Y].GetInt();
    if (!IsInside({ X, Y }, true))
    {
        // If dropped outside of the trading dialog remove it
        auto* container = GetChild("AttachmentItems", true);
        if (RemoveItem(container, static_cast<unsigned>(pos)))
            attachments_.erase(pos);
    }

    UIElement* root = GetSubsystem<UI>()->GetRoot();
    root->RemoveChild(dragItem_.Get());
    dragItem_.Reset();
}

bool NewMailWindow::RemoveItem(UIElement* container, unsigned pos)
{
    auto* item = GetItemFromPos(container, pos);
    if (item == nullptr)
        return false;

    item->RemoveAllChildren();
    return true;
}

UIElement* NewMailWindow::GetItemFromPos(UIElement* container, unsigned pos)
{
    for (size_t i = 0; i < MAX_ATTACHMENTS; ++i)
    {
        auto* itemContainer = container->GetChild("Item" + String(i + 1), true);
        if (!itemContainer)
            continue;
        ItemUIElement* icon = itemContainer->GetChildDynamicCast<ItemUIElement>("ItemElememt", true);
        if (!icon)
            continue;
        if (icon->pos_ == static_cast<unsigned>(pos))
            return itemContainer;
    }
    return nullptr;
}
