/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <Urho3DAll.h>
#include "Skill.h"
#include "Effect.h"
#include <libshared/Damage.h>

class DamageWindowItem final : public UIElement
{
    URHO3D_OBJECT(DamageWindowItem, UIElement)
private:
    SharedPtr<Text> text_;
    void InitializeSkill(const Skill* skill);
    void InitializeEffect(const Effect* effect);
    void CreateText();
public:
    static void RegisterObject(Context* context);
    static const int ICON_SIZE = 32;
    DamageWindowItem(Context* context);
    ~DamageWindowItem() override;
    bool Initialize();
    void Touch();
    unsigned count_{ 0 };
    int64_t damageTick_{ 0 };
    int64_t startTick_{ 0 };
    uint32_t index_{ 0 };
    unsigned value_{ 0 };
    Game::DamageType damageType_{ Game::DamageType::Unknown };
};

class DamageWindow final : public Window
{
    URHO3D_OBJECT(DamageWindow, Window)
private:
    static const int KEEP_ITEMS_MS = 10000;
    Vector<SharedPtr<DamageWindowItem>> items_;
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleMouseUp(StringHash eventType, VariantMap& eventData);
    void HandleObjectDamaged(StringHash eventType, VariantMap& eventData);
    DamageWindowItem* FindItem(uint32_t index);
    DamageWindowItem* GetItemAt(const IntVector2& screenPos);
public:
    static void RegisterObject(Context* context);

    DamageWindow(Context* context);
    ~DamageWindow() override;
    void Clear();
};
