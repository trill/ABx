/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PartyItem.h"

void PartyItem::RegisterObject(Context* context)
{
    context->RegisterFactory<PartyItem>();
    URHO3D_COPY_BASE_ATTRIBUTES(HealthBar);
}

PartyItem::PartyItem(Context* context) :
    HealthBar(context),
    type_(MemberType::Invitee)
{
    SetEnabled(true);
}

PartyItem::~PartyItem()
{
}
