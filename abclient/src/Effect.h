/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Effect.h>

struct Effect
{
    std::string uuid;
    uint32_t index = 0;
    std::string name;
    AB::Entities::EffectCategory category = AB::Entities::EffectNone;
    std::string icon;
    std::string soundEffect;
    std::string particleEffect;
    std::string description;
};
