/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Acceptor.h"
#include <AB/Entities/Service.h>
#include <libcommon/DataClient.h>
#include <libcommon/ServerApp.h>

class Application final : public ServerApp
{
private:
    using ServiceItem = std::pair<std::string, uint16_t>;
    std::vector<ServiceItem> serviceList_;
    asio::io_service ioService_;
    AB::Entities::ServiceType lbType_;
    std::unique_ptr<IO::DataClient> dataClient_;
    std::unique_ptr<Acceptor> acceptor_;
    bool enablePingServer_{ true };
    void PrintServerInfo();
    bool LoadMain();
    bool GetServiceCallback(AB::Entities::Service& svc);
    bool GetServiceCallbackList(AB::Entities::Service& svc);
    bool ParseServerList(const std::string& fileName);
    void ShowLogo();
protected:
    void ShowVersion() override;
public:
    Application();
    ~Application() override;

    bool Initialize(const std::vector<std::string>& args) override;
    void Run() override;
    void Stop() override;
};

