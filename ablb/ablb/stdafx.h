/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#include "targetver.h"
#include <sa/Compiler.h>

PRAGMA_WARNING_DISABLE_MSVC(4307)

#include <stdio.h>

#include <string>
#include <vector>
#include <memory>
#include <iostream>

#include <AB/CommonConfig.h>
#include <libcommon/DebugConfig.h>

#if !defined(ASIO_STANDALONE)
#define ASIO_STANDALONE
#endif

PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4592)
#   include <asio.hpp>
PRAGMA_WARNING_POP

#include <libcommon/Utils.h>
#include <libcommon/Logger.h>
