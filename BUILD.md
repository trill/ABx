# Build

## Supported Platforms/Tools

This project needs a C++20 compiler.

It is known to compile on the following platforms with the following tools chains:

* Arch, Manjaro, Debian 11 Linux with GCC 10 and newer.

Since I do not use Windows anymore, I can not make sure it also compiles and runs on Windows,
but it did at some time.

## Prerequisites

* C++20
* GNU make or Ninja
* CMake at least version 3.16
* ccache (optional)
* Linux

## Dependencies

* `asio` [Asio](https://archlinux.org/packages/extra/any/asio/) standalone. On Arch you can install it with `sudo pacman -S asio`
* `glew` when compiled with `SCENE_VIEWER`: `sudo pacman -S glew`
* [Catch2](https://github.com/catchorg/Catch2), on Arch you can install it with `sudo pacman -S catch2`
* https://github.com/yhirose/cpp-httplib
* `uuid-dev`
* `libpq-dev` (`postgresql-server-dev-all` when using CMake on Ubuntu)
* `libssl-dev`
* `libldap2-dev` for PostgreSQL, when building with `USE_PGSQL`
* `libgsasl7-dev` for PostreSQL, when building with `USE_PGSQL`
* `libkrb5-dev` for PostgreSQL, when building with `USE_PGSQL`
* `libncurses-dev` for the Debug client (`dbgclient`).

Your distribution may have different names for these libraries.

I didn't try any other database backend than PostgreSQL since some time, so the others may or may
not work.

## Install GCC and Dependencies on Ubuntu

~~~sh
# Install GCC 10
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update -qq
sudo apt-get install g++-10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 90
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 90
# Install Dependencies
sudo -E apt-get -yq --no-install-suggests --no-install-recommends install uuid-dev libpq-dev libssl-dev libldap2-dev libgsasl7-dev libkrb5-dev lua5.3 lua5.3-dev libncurses-dev
~~~

If you get a link error, like "Library -lz not found" or a compile error `fatal error: zlib.h: No such file or directory`, you also need to install `zlib1g-dev`.

If CMake complains about missing PostgreSQL although you installed `libpq-dev`, also install
`postgresql-server-dev-all`, see https://stackoverflow.com/questions/13920383/findpostgresql-cmake-wont-work-on-ubuntu#40027295.

If it still can't find PostgreSQL, try to add `-DPostgreSQL_TYPE_INCLUDE_DIR=/usr/include/postgresql` to the CMake commandline:

~~~
cmake -DPostgreSQL_TYPE_INCLUDE_DIR=/usr/include/postgresql -DCMAKE_BUILD_TYPE=Release ..
~~~

See see here https://community.greenbone.net/t/hint-postgresql-13-issues-when-building-gvmd-or-running-pg-upgradecluster/7167.
This worked for me on Debian 11 (Bullseye) with PostgreSQL 13 and CMake 3.18.

## Build

1. Install the required packages.
2. Use CMake to build it (assuming you want to use GNU make):
~~~sh
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
# Copy executables to ../bin and ../abclient/bin
$ cmake --install . --component runtime
~~~

To build the server without the client, turn `ABX_BUILD_CLIENT` off, e.g. run CMake with the following arguments:
~~~sh
$ cmake -DABX_BUILD_CLIENT=OFF -DCMAKE_BUILD_TYPE=Release ..
~~~

After that, you can update easily with just (1) pulling the latest HEAD, (2) `cd` into the previously created `build` directory and (3) type `make`.
If files were added or removed, you may need to re run CMake.

### CMake options

* `ABX_ASSERT` Enable ASSERT() in Release mode
* `ABX_BUILD_CLIENT` Build Game Client (requires Urho3D to be installed)
* `ABX_BUILD_SERVER` Build Servers
* `ABS_BUILD_TOOLS` Build Tools
* `ABX_NO_PROFILING` No Profiling
* `ABX_SCENE_VIEWER` Compile Game Server with Scene viewer
* `ABX_CLIENT_NO_AUTOUPDATE` Disable client autoupdating
* `ABX_CLIENT_LOGGING` Enable client logging
* `ABX_CLIENT_DEBUGHUD` Enable client Debug HUD

## Client

### Dependencies

Required lib files for the client not in this repository:

* `libUrho3D.a` on Linux

Clone Urho3D from the official GitHub repository (https://github.com/urho3d/Urho3D, use the latest master).

Note: When you build Urho3D under Linux by GCC-10, you may get this error: [multiple definition of WAYLAND_wl_xxx](https://discourse.urho3d.io/t/linux-version-cant-build-multiple-definition-of/6490/2), it can be solved by add the fellow code to CMakeLists of Urho3D:
```
SET(GCC_FCOMMON_COMPILE_FLAGS "-fcommon")

add_definitions(${GCC_FCOMMON_COMPILE_FLAGS})
```

Build Urho3D and put the library files in `./lib`. Symlink the directory with Urho3D header files to `include/`,
so there is a `include/Urho3D` directory, e.g:
~~~sh
$ ln -s /home/you/src/Urho3D/build/include/Urho3D/ /home/you/src/ABx/include/
~~~

Copy Urho3D assets to `abclient/bin` directory:
~~~plain
bin
  - Autoload
  - CoreData
  - Data
~~~

### Build

Since everything has the same top level `CMakeLists.txt` file, the client is built with the server (unless `ABX_BUILD_CLIENT` is turned off).

## Updating the source

If you pull to update your source tree, you may need to:

1. Re-run CMake when source files were added or removed.
2. Run `bin/dbtool update` when there is a DB update in the `sql` folder.
3. Check [OneDrive](https://1drv.ms/f/s!Ajy_fJI3BLBobOAOXZ47wtBgdBg) if `client_data.7z` and/or `sound_data.7z` has changed.
4. Maybe run `genassets.sh` in the `bin` directory for regenerating the server assets.
