INSERT INTO public.game_skills VALUES (public.random_guid(), 23, 'Power Spike', 'e8e16f7e-50f5-11e8-a7ca-02100700d6f0', 512, 0, 'Spell: If target foe is casting, that skill is interrupted and target takes ${30 + (domination * 6)} damage', 'Spell: Interrupts a spell and deals 30..120 damage when interrupted', 'Textures/Skills/power_spike.png', '/scripts/skills/domination_magic/power_spike.lua', '85d0939b-50f4-11e8-a7ca-02100700d6f0', '', '', 1, 250, 12000, 5, 0, 0, 0, 0);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

UPDATE public.versions SET value = 45 WHERE name = 'schema';
