INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('faea495c-46e0-459f-9fce-866d0c9c4181', 5017, '/scripts/skills/earth_magic/ward_against_magic.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('fee9c2b9-ec90-45a3-8ecd-eda9ce915198', 5018, '/scripts/skills/earth_magic/ward_against_missiles.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('d11372df-ba9f-4e75-a383-53aaeaa89d82', 5019, '/scripts/skills/energy_storage/destructive_singularity.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('168e98bf-4503-42a7-8c25-a1761a25a303', 5020, '/scripts/skills/energy_storage/doublecast.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('6eb1486e-3b6b-4f98-915a-56c46b819e86', 5021, '/scripts/skills/energy_storage/glyph_of_greater_concentration.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('d3ddbd29-67f8-4a9e-bedd-2ebf9de30a54', 5017, '/scripts/effects/ward/ward_against_magic.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f89f8b8a-2a10-453d-b639-ab2bd5600ff4', 5018, '/scripts/effects/ward/ward_against_missiles.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('fd18fc05-9775-4687-a732-a8d7655d18f5', 5019, '/scripts/effects/enchantment/destructive_singularity.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('f179a0ab-6ba8-403e-8c06-23504e7be3dc', 10008, '/scripts/effects/condition/deep_wound.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('33185f4b-8c58-4e7c-a65a-d16db5d31d6f', 5020, '/scripts/effects/skill/doublecast.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('4bb45fae-57e0-4370-a416-805b50a8ade3', 5021, '/scripts/effects/glyph/glyph_of_greater_concentration.lua');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 59 WHERE name = 'schema';
