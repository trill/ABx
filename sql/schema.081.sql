-- List of concrete item UUIDs separated by ;
ALTER TABLE public.mails ADD COLUMN attachments text NOT NULL DEFAULT ''::character varying;
-- If attached to a mail this is the mail UUID
ALTER TABLE public.concrete_items ADD mail_uuid bpchar(36) NULL DEFAULT ''::bpchar;

UPDATE public.versions SET value = 81 WHERE name = 'schema';
