INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('fc342b45-ce7c-4508-aea7-7b1ce242fb31', 5114, '/scripts/skills/domination_magic/dark_pattern.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('bddabc61-87c5-4174-b2b0-efdabe8f08c1', 5022, '/scripts/skills/energy_storage/mana_beam.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('4a1290ea-7c11-4f6c-a330-f72b690fb5a6', 5030, '/scripts/skills/fire_magic/arsonists_aura.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('3e21fca4-e4d1-4e60-8d96-883cc449fb66', 5031, '/scripts/skills/fire_magic/delay_blast.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('92625cd1-58b9-46c7-a2e0-59eeb96a8fd9', 5032, '/scripts/skills/fire_magic/eclipsinas_wish.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('869a6a35-f9b8-4292-b5e3-80c95a9fa01e', 5033, '/scripts/skills/fire_magic/flame_wave.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b4ebcde6-a20c-44f4-a592-4f357b5a3fb9', 5034, '/scripts/skills/fire_magic/flame_whip.lua', 1);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('2188fc93-58ec-483b-9b48-04ba59249c09', 5114, '/scripts/effects/hex/dark_pattern.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('ae059e41-480c-4bd9-a6d5-58e8df1f1178', 5030, '/scripts/effects/enchantment/arsonists_aura.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('b683f1d6-f91d-43d3-a4b2-450f7105e634', 5031, '/scripts/effects/hex/delay_blast.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('9b0a810e-afec-4b0c-be70-d517d6ab6166', 15032, '/scripts/effects/hex/eclipsinas_wish.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('85c92575-41a1-4171-9e9e-ec410e0b3592', 5032, '/scripts/effects/enchantment/eclipsinas_wish.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('0f3f0b84-391d-4622-8267-fe9952e6e179', 1002, '/scripts/effects/general/immobilized.lua');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 60 WHERE name = 'schema';
