INSERT INTO public.game_items VALUES ('cc03f63a-5ace-4a1c-9c95-73ad8a5183b2', 17, 'Brambles', '', 'Objects/RangerSpirit.xml', '', 1, 0, 0, 103, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/spirits/brambles.lua', 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

INSERT INTO public.game_skills VALUES (public.random_guid(), 947, 'Brambles', '67bffc43-50f5-11e8-a7ca-02100700d6f0', 131200, 0, 'Nature Ritual: Create level ${wildernesssurvival / 1.5 + 1} spirit.', 'Nature Ritual: Create spirit.', 'Textures/Skills/placeholder.png', '/scripts/skills/wilderness_survival/brambles.lua', '59f4b30b-50f4-11e8-a7ca-02100700d6f0', '', '', 1, 5000, 60000, 10, 0, 0, 0, 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects VALUES (public.random_guid(), 947, 'Brambles', 5, '/scripts/effects/spirit/brambles.lua', 'Textures/Skills/placeholder.png');
INSERT INTO public.game_effects VALUES (public.random_guid(), 10006, 'Bleeding', 1, '/scripts/effects/condition/beeding.lua', 'Textures/Skills/placeholder.png');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 50 WHERE name = 'schema';
