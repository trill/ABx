INSERT INTO public.game_items VALUES ('ef0d1930-f605-4d1f-b33c-aed59969c9b1', 16, 'Bone Minion', '', 'Objects/Placeholder.xml', '', 1, 0, 0, 13, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/bone_minion.lua', 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

INSERT INTO public.game_skills VALUES (public.random_guid(), 85, 'Animate Bone Minion', 'd1e52aa1-50f5-11e8-a7ca-02100700d6f0', 512, 0, 'Spell: Animate Bone Minion with level ${ceil(death * 0.82)} from nearest corpse.', 'Spell: Create a level 1..17 Bone Minion.', 'Textures/Skills/placeholder.png', '/scripts/skills/death_magic/animate_bone_minion.lua', '7315cbd6-50f4-11e8-a7ca-02100700d6f0', '', '', 1, 3000, 3000, 15, 0, 0, 0, 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

UPDATE public.versions SET value = 49 WHERE name = 'schema';
