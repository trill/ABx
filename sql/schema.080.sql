INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('12a6e515-1996-4ec1-bb8d-ba35de0d956b', 5139, '/scripts/skills/illusion_magic/haphephobia.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('c25211a9-6db5-4659-9c6c-2029ec132e22', 5139, '/scripts/effects/hex/haphephobia.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 80 WHERE name = 'schema';
