UPDATE public.game_items SET object_file = 'Objects/BoneMinion.xml' WHERE idx = 16;

INSERT INTO public.game_items VALUES ('a3ee56e0-bd5c-4330-907f-cc3c6be13da0', 18, 'Trill Chan Mini', '/scripts/items/trill_chan_mini.lua', 'Objects/TrillChan.xml', 'Textures/Icons/Items/TrillChan.png', 1003, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '', 52);

INSERT INTO public.game_items VALUES ('d5ff47dd-c766-4d56-b3da-c3f48f2a6f8c', 19, 'Trill Chan', '', 'Objects/TrillChan.xml', 'Textures/Icons/Items/TrillChan.png', 1, 0, 0, 9, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/mini_pets/trill_chan.lua', 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

UPDATE public.versions SET value = 51 WHERE name = 'schema';
