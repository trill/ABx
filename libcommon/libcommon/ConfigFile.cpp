/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ConfigFile.h"
#include <iostream>
#include <fstream>

namespace IO {

ConfigFile::ConfigFile() = default;

ConfigFile::~ConfigFile() = default;

bool ConfigFile::Load(const std::string& filename)
{
    entires_.clear();

    std::ifstream in(filename);
    if (!in.is_open())
        return false;

    std::string line;
    while (std::getline(in, line))
    {
        line = sa::Trim<char>(line, " \t");
        if (line.empty() || line[0] == '#')
            continue;

        auto parts = sa::Split(line, "=", false, false);
        if (parts.size() != 2)
            continue;

        entires_.emplace(sa::Trim<char>(parts[0]), sa::Trim<char>(parts[1]));
    }

    return true;
}

}
