/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Variant.h"
#include <functional>
#include <lua.hpp>
#include <map>
#include <sa/Iteration.h>
#include <sa/Noncopyable.h>
#include <string>

namespace IO {

class SimpleConfigManager
{
    NON_COPYABLE(SimpleConfigManager)
private:
    lua_State* L{ nullptr };
    bool loaded_{ false };
protected:
    void RegisterString(const std::string& name, const std::string& value);
public:
    SimpleConfigManager() = default;
    ~SimpleConfigManager();
    std::string GetGlobalString(const std::string& ident, const std::string& def);
    int64_t GetGlobalInt(const std::string& ident, int64_t def);
    float GetGlobalFloat(const std::string& ident, float def);
    bool GetGlobalBool(const std::string& ident, bool def);
    bool GetGlobalTable(const std::string& ident,
        const std::function<Iteration(const std::string& name, const Utils::Variant& value)>& callback);
    std::map<std::string, Utils::Variant> GetGlobalTable(const std::string& ident);

    void AddSearchPath(const std::string& path);

    bool Load(const std::string& file);
    void Close();
};

}
