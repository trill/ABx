/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace Navigation {

enum PolyAreas
{
    POLYAREA_GROUND,
    POLYAREA_WATER,
    POLYAREA_OBSTACLE,
};

enum PolyFlags
{
    POLYFLAGS_WALK = 0x01,      // Ability to walk (ground, grass, road)
    POLYFLAGS_SWIM = 0x02,      // Ability to swim (water).
    POLYFLAGS_DISABLED = 0x10,  // Disabled polygon
    POLYFLAGS_ALL = 0xffff      // All abilities.
};

}
