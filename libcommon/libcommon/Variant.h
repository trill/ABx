/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <sa/PropStream.h>
#include <string_view>

namespace Utils {

enum class VariantType
{
    None = 0,
    Int,
    Int64,
    Boolean,
    Float,
    String,
    VoidPtr
};

union VariantValue
{
    int intValue;
    int64_t int64Value;
    bool boolValue;
    float floatValue;
    void* ptrValue;
};

class Variant
{
private:
    VariantType type_{ VariantType::None };
    VariantValue value_{};
    std::string stringValue_;
public:
    Variant() noexcept = default;
    Variant(const Variant& variant) noexcept = default;
    Variant(Variant&& variant) noexcept = default;

    Variant(int value) noexcept : type_(VariantType::Int), value_{ .intValue = value } {} //NOLINT
    Variant(unsigned value) noexcept : type_(VariantType::Int), value_{ .intValue = static_cast<int>(value) } {} //NOLINT
    Variant(long long value) noexcept : type_(VariantType::Int64), value_{ .int64Value = value } {} //NOLINT
    Variant(unsigned long long value) noexcept : type_(VariantType::Int64), value_{ .int64Value = static_cast<long long>(value) } {} //NOLINT
    Variant(bool value) noexcept : type_(VariantType::Boolean), value_{ .boolValue = value } {} //NOLINT
    Variant(float value) noexcept : type_(VariantType::Float), value_{ .floatValue = value } {} //NOLINT
    Variant(const std::string& value) noexcept : type_(VariantType::String), value_{}, stringValue_(value) {} //NOLINT
    Variant(std::string&& value) noexcept : type_(VariantType::String), value_{}, stringValue_(std::move(value)) {} //NOLINT
    Variant(const char* value) noexcept : type_(VariantType::String), value_{}, stringValue_(value) {} //NOLINT
    Variant(std::string_view value) noexcept : type_(VariantType::String), value_{}, stringValue_(value) {} //NOLINT
    ~Variant() = default;

    constexpr VariantType GetType() const { return type_; }
    void SetType(VariantType value) { type_ = value; }
    /// Check if it has a value
    constexpr bool IsEmpty() const
    {
        return type_ != VariantType::None;
    }

    /// Assigning from other
    Variant& operator =(const Variant& other);
    Variant& operator =(Variant&& other) noexcept;
    Variant& operator =(int other)
    {
        SetType(VariantType::Int);
        value_.intValue = other;
        return *this;
    }
    Variant& operator =(unsigned other)
    {
        SetType(VariantType::Int);
        value_.intValue = static_cast<int>(other);
        return *this;
    }
    Variant& operator =(bool other)
    {
        SetType(VariantType::Boolean);
        value_.boolValue = other;
        return *this;
    }
    Variant& operator =(float other)
    {
        SetType(VariantType::Float);
        value_.floatValue = other;
        return *this;
    }
    Variant& operator =(long long other)
    {
        SetType(VariantType::Int64);
        value_.int64Value = other;
        return *this;
    }
    Variant& operator =(unsigned long long other)
    {
        SetType(VariantType::Int64);
        value_.int64Value = static_cast<long long>(other);
        return *this;
    }
    Variant& operator =(const std::string& other)
    {
        SetType(VariantType::String);
        stringValue_ = other;
        return *this;
    }
    Variant& operator =(std::string&& other)
    {
        SetType(VariantType::String);
        stringValue_ = std::move(other);
        return *this;
    }
    Variant& operator =(std::string_view other)
    {
        SetType(VariantType::String);
        stringValue_ = other;
        return *this;
    }
    Variant& operator =(const char* other)
    {
        SetType(VariantType::String);
        stringValue_ = other;
        return *this;
    }
    Variant& operator =(void* other)
    {
        SetType(VariantType::VoidPtr);
        value_.ptrValue = other;
        return *this;
    }

    /// Test for equality with another variant.
    bool operator ==(const Variant& other) const;
    /// Test for equality. To return true, both the type and value must match.
    constexpr bool operator ==(int other) const { return type_ == VariantType::Int ? value_.intValue == other : false; }
    constexpr bool operator ==(unsigned other) const { return type_ == VariantType::Int ? value_.intValue == static_cast<int>(other) : false; }
    constexpr bool operator ==(bool other) const { return type_ == VariantType::Boolean ? value_.boolValue == other : false; }
    constexpr bool operator ==(float other) const { return type_ == VariantType::Float ? (value_.floatValue + std::numeric_limits<float>::epsilon() >= other &&
        value_.floatValue - std::numeric_limits<float>::epsilon() <= other) : false; }
    constexpr bool operator ==(long long other) const { return type_ == VariantType::Int64 ? value_.int64Value == other : false; }
    constexpr bool operator ==(unsigned long long other) const { return type_ == VariantType::Int64 ? value_.int64Value == static_cast<long long>(other) : false; }
    bool operator ==(const std::string& other) const { return type_ == VariantType::String ? (stringValue_ == other) : false; }
    bool operator ==(const char* other) const { return type_ == VariantType::String ? stringValue_ == other : false; }
    bool operator ==(void* other) const { return type_ == VariantType::VoidPtr ? value_.ptrValue == other : false; }
    /// Test for inequality.
    bool operator !=(const Variant& other) const { return !(*this == other); }
    constexpr bool operator !=(int other) const { return !(*this == other); }
    constexpr bool operator !=(unsigned int other) const { return !(*this == other); }
    constexpr bool operator !=(bool other) const { return !(*this == other); }
    constexpr bool operator !=(float other) const { return !(*this == other); }
    constexpr bool operator !=(long long other) const { return !(*this == other); }
    constexpr bool operator !=(unsigned long long other) const { return !(*this == other); }
    bool operator !=(const std::string& other) const { return !(*this == other); }
    bool operator !=(const char* other) const { return !(*this == other); }
    bool operator !=(void* other) const { return !(*this == other); }

    std::string ToString() const;
    constexpr int GetInt() const
    {
        return (type_ == VariantType::Int) ? value_.intValue : 0;
    }
    constexpr bool GetBool() const
    {
        return (type_ == VariantType::Boolean) ? value_.boolValue : false;
    }
    constexpr float GetFloat() const
    {
        return (type_ == VariantType::Float) ? value_.floatValue : 0.0f;
    }
    constexpr long long GetInt64() const
    {
        return (type_ == VariantType::Int64) ? value_.int64Value : 0;
    }
    const std::string& GetString() const
    {
        static const std::string STRING_EMPTY = "";
        return (type_ == VariantType::String) ? stringValue_ : STRING_EMPTY;
    }
    void* GetPtr() const
    {
        return (type_ == VariantType::VoidPtr) ? value_.ptrValue : nullptr;
    }

    constexpr operator bool() const
    {
        return GetBool();
    }
    constexpr operator int() const
    {
        return GetInt();
    }
    constexpr operator unsigned() const
    {
        return static_cast<unsigned>(GetInt());
    }
    constexpr operator long long() const
    {
        return GetInt64();
    }
    constexpr operator unsigned long long() const
    {
        return static_cast<unsigned long long>(GetInt64());
    }
    constexpr operator float() const
    {
        return GetFloat();
    }
    operator std::string() const
    {
        return GetString();
    }
    friend std::ostream& operator << (std::ostream& os, const Variant& value)
    {
        return os << value.ToString();
    }

    /// Empty variant.
    static const Variant Empty;
};

using VariantMap = std::map<size_t, Variant>;
/// Empty variant map
static const VariantMap VariantMap_Empty;

bool VariantMapRead(VariantMap& vMap, sa::PropReadStream& stream);
void VariantMapWrite(const VariantMap& vMap, sa::PropWriteStream& stream);

}
