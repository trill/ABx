/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <sa/Compiler.h>
#include <optional>
#include "NetworkMessage.h"

namespace Net {

class MessageDecoder
{
private:
    const uint8_t* ptr_;
    size_t size_;
    size_t pos_;
    bool CanRead(size_t size) const
    {
        return pos_ + size <= size_;
    }
    std::string GetString();
public:
    explicit MessageDecoder(const NetworkMessage& message);
    template <typename T>
    T Get()
    {
        if (!CanRead(sizeof(T)))
            return { };
        T v;
#ifdef SA_MSVC
        memcpy_s(&v, sizeof(T), ptr_ + pos_, sizeof(T));
#else
        memcpy(&v, ptr_ + pos_, sizeof(T));
#endif
        pos_ += sizeof(T);
        return v;
    }

    std::optional<AB::GameProtocol::ServerPacketType> GetNext();
};

template <>
inline std::string MessageDecoder::Get<std::string>()
{
    return GetString();
}
template <>
inline bool MessageDecoder::Get<bool>()
{
    return Get<uint8_t>() != 0;
}

}
