/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Compiler.h>
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4702 4127)
#   include <lua.hpp>
#   include <kaguya/kaguya.hpp>
PRAGMA_WARNING_POP
#include <AB/Entities/Effect.h>

namespace IO {

class LuaEffect
{
private:
    kaguya::State state_;
public:
    LuaEffect();
    bool Execute(const std::string& script);
    std::string GetUuid();
    int32_t GetIndex();
    std::string GetName();
    std::string GetIcon();
    std::string GetSoundEffect();
    std::string GetParticleEffect();
    AB::Entities::EffectCategory GetCategory();
    std::string GetDescription();
    bool IsInternal();
};

}
