/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

namespace IO {

enum class OpCodes : uint8_t
{
    None = 0,
    // Requests
    Lock,
    Unlock,
    Create,
    Update,
    Read,
    Delete,
    // Invalidate a cache item. Flushes modified data. Next read will load it from the DB.
    Invalidate,
    Preload,
    Exists,
    // Clear all cache
    Clear,
    // Responses
    Status,
    Data
};

enum class ErrorCodes : uint8_t
{
    Ok,
    NoSuchKey,
    KeyTooBig,
    DataTooBig,
    OtherErrors,
    NotExists
};

}
