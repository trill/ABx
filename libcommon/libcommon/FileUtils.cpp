/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FileUtils.h"
#include <fstream>
#include <sa/StringTempl.h>
#include <AB/CommonConfig.h>
#ifdef AB_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <linux/limits.h>
#include <unistd.h>
#endif
#include <filesystem>
#ifdef AB_UNIX
#include <wordexp.h>
#endif

namespace Utils {

bool FileCopy(const std::string& src, const std::string& dst)
{
#ifdef AB_WINDOWS
    return ::CopyFileA(src.c_str(), dst.c_str(), FALSE);
#else
    std::ifstream fsrc(src, std::ios::binary);
    if (!fsrc.is_open())
        return false;
    std::ofstream fdst(dst, std::ios::binary);
    if (!fdst.is_open())
        return false;
    fdst << fsrc.rdbuf();
    return true;
#endif
}

std::string NormalizeFilename(const std::string& filename)
{
    std::string normal_name(filename);
    sa::ReplaceSubstring<char>(normal_name, "\\", "/");
    while (sa::ReplaceSubstring<char>(normal_name, "//", "/"));
    if (!normal_name.empty() && normal_name[0] != '/' && !sa::Contains(normal_name, ":"))
        normal_name = "/" + normal_name;
    return normal_name;
}

bool FileExists(const std::string& name)
{
#ifdef AB_WINDOWS
    DWORD dwAttrib = ::GetFileAttributesA(name.c_str());
    return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
        !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
#else
    std::ifstream infile(name);
    return infile.good();
#endif
}

std::string AddSlash(const std::string& dir)
{
    if (dir.back() != '\\' && dir.back() != '/')
        return dir + "/";
    return dir;
}

bool IsHiddenFile(const std::string& path)
{
    if (path.empty())
        return false;
    if ((path != "..") &&
        (path != ".") &&
        ((path[0] == '.') || (path.find("/.") != std::string::npos)))
        return true;
#if defined(AB_WINDOWS)
    DWORD attributes = ::GetFileAttributesA(path.c_str());
    if (attributes & FILE_ATTRIBUTE_HIDDEN)
        return true;
    if (attributes & FILE_ATTRIBUTE_SYSTEM)
        return true;
#endif
    return false;
}

std::string GetExeName()
{
#ifdef AB_WINDOWS
    char buff[MAX_PATH];
    GetModuleFileNameA(NULL, buff, MAX_PATH);
    return std::string(buff);
#else
    char buff[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", buff, PATH_MAX);
    return std::string(buff, (count > 0) ? count : 0);
#endif
}

bool EnsureDirectory(const std::string& dir)
{
    namespace fs = std::filesystem;
    fs::path p(dir);
    if (fs::exists(p))
        return true;
    return fs::create_directories(p);
}

bool IsSourceNewerThanDest(const std::string& source, const std::string& dest)
{
    namespace fs = std::filesystem;
    if (!FileExists(source))
        return false;
    if (!FileExists(dest))
        return true;
    auto sourceFt = fs::last_write_time(fs::path(source));
    auto destFt = fs::last_write_time(fs::path(dest));
    return sourceFt > destFt;
}

std::string ExpandPath(const char* path)
{
#ifdef AB_UNIX
    wordexp_t p;
    wordexp(path, &p, 0);
    std::string result;
    if (p.we_wordc != 0)
    {
        result = p.we_wordv[0];
    }
    wordfree(&p);
    return result;
#else
    return path;
#endif
}

}
