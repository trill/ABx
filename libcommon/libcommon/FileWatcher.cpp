/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FileWatcher.h"

namespace IO {

FileWatcher::FileWatcher(const std::string& fileName, void* userData, std::function<void(const std::string, void*)>&& onChanged, bool defer) :
    fileName_(fileName),
    path_(fileName),
    userData_(userData),
    onChanged_(std::move(onChanged))
{
    if (!defer)
        Start();
}

FileWatcher::~FileWatcher()
{
    enabled_ = false;
}

void FileWatcher::Start()
{
    if (enabled_)
        return;
    enabled_ = true;
}

void FileWatcher::Stop()
{
    enabled_ = false;
}

void FileWatcher::Update()
{
    if (!enabled_)
        return;
    auto lastWriteTime = fs::last_write_time(path_);
    if (lastWriteTime > lastTime_)
    {
        if (lastTime_ != fs::file_time_type::min())
        {
            if (onChanged_)
                onChanged_(fileName_, userData_);
        }
        lastTime_ = lastWriteTime;
    }
}

}
