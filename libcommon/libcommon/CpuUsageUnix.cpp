/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(AB_UNIX)

#include <sys/times.h>
#include "CpuUsage.h"
#include "Utils.h"
#include <sa/time.h>

namespace System {

CpuUsage::CpuUsage() = default;

unsigned CpuUsage::GetUsage()
{
    unsigned nCpuCopy = cpuUsage_;
    if (!EnoughTimePassed())
        return nCpuCopy;

    if (!IsFirstRun())
    {
        double percent = 0.0;
        struct tms buf{};
        clock_t now = times(&buf);
        if (now <= lastCPU_ || buf.tms_stime < lastSysCPU_ || buf.tms_utime < lastUserCPU_)
            percent = -1.0;
        else
        {
            percent = static_cast<double>((buf.tms_stime - lastSysCPU_) + (buf.tms_utime - lastUserCPU_));
            percent /= static_cast<double>(now - lastCPU_);
            percent *= 100;
        }
        lastCPU_ = now;
        lastSysCPU_ = buf.tms_stime;
        lastUserCPU_ = buf.tms_utime;

        cpuUsage_ = static_cast<unsigned>(percent);
        nCpuCopy = cpuUsage_;
    }

    lastRun_ = sa::time::tick();

    return nCpuCopy;
}

bool CpuUsage::EnoughTimePassed() const
{
    static const unsigned minElapsedMS = 250;   // milliseconds
    return sa::time::time_elapsed(lastRun_) > minElapsedMS;
}

}

#endif
