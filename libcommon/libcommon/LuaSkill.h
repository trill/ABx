/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sa/Compiler.h>
PRAGMA_WARNING_PUSH
PRAGMA_WARNING_DISABLE_MSVC(4702 4127)
#   include <lua.hpp>
#   include <kaguya/kaguya.hpp>
PRAGMA_WARNING_POP
#include <stdint.h>
#include <AB/Entities/Skill.h>
#include <AB/Entities/Profession.h>

namespace IO {

class LuaSkill
{
private:
    kaguya::State state_;
public:
    LuaSkill();
    bool Execute(const std::string& script);
    std::string GetUuid();
    int32_t GetIndex();
    std::string GetName();
    uint32_t GetAttribute();
    AB::Entities::SkillType GetSkillType();
    bool IsElite();
    bool IsMaintainable();
    std::string GetDescription();
    std::string GetShortDescription();
    std::string GetIcon();
    AB::Entities::ProfessionIndex GetProfession();
    std::string GetSoundEffect();
    std::string GetParticleEffect();
    AB::Entities::SkillAccess GetAccess();

    int32_t GetEnergy();
    int32_t GetEnergyRegen();
    int32_t GetAdrenaline();
    int32_t GetActivation();
    int32_t GetOvercast();
    int32_t GetHp();
    int32_t GetRecharge();
};

}
