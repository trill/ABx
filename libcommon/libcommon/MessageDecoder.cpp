/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MessageDecoder.h"

namespace Net {

MessageDecoder::MessageDecoder(const NetworkMessage& message) :
    ptr_(message.GetBuffer()),
    size_(message.GetSize() + NetworkMessage::INITIAL_BUFFER_POSITION),
    pos_(NetworkMessage::INITIAL_BUFFER_POSITION)
{ }

std::optional<AB::GameProtocol::ServerPacketType> MessageDecoder::GetNext()
{
    if (!CanRead(1))
        return {};
    return static_cast<AB::GameProtocol::ServerPacketType>(ptr_[pos_++]);
}

std::string MessageDecoder::GetString()
{
    size_t len = Get<uint16_t>();
    if (len >= (NetworkMessage::NETWORKMESSAGE_BUFFER_SIZE - pos_))
        return std::string();
    if (!CanRead(len))
        return std::string();

    const char* v = reinterpret_cast<const char*>(ptr_) + pos_;
    pos_ += len;
    return std::string(v, len);
}

}
