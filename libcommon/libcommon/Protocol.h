/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Logger.h"
#include <abcrypto.hpp>
#include <cstring>
#include <sa/SmartPtr.h>
#include <sa/Noncopyable.h>

namespace Net {

class Connection;
class OutputMessage;
class OutputMessagePool;
class NetworkMessage;

class Protocol : public std::enable_shared_from_this<Protocol>
{
    NON_COPYABLE(Protocol)
protected:
    friend class Connection;
    friend class OutputMessagePool;

    std::weak_ptr<Connection> connection_;
    sa::SharedPtr<OutputMessage> outputBuffer_;
    bool encryptionEnabled_;
    DH_KEY encKey_;

    void XTEAEncrypt(OutputMessage& msg) const;
    bool XTEADecrypt(NetworkMessage& msg) const;
    void Disconnect() const;
    void Send(sa::SharedPtr<OutputMessage>&& message);
    void OnRecvMessage(NetworkMessage& message);
    virtual void Release() {}
    virtual bool OnSendMessage(OutputMessage& message) const;
    virtual void OnRecvFirstMessage(NetworkMessage& msg) = 0;
    virtual void OnConnect() {}
    virtual void ParsePacket(NetworkMessage&) {}
public:
    explicit Protocol(std::shared_ptr<Connection> connection);
    virtual ~Protocol();

    void SetEncKey(const uint32_t* key)
    {
        memcpy(&encKey_, key, sizeof(encKey_));
    }

    bool IsConnectionExpired() const { return connection_.expired(); }
    std::shared_ptr<Connection> GetConnection() const { return connection_.lock(); }

    sa::SharedPtr<OutputMessage> GetOutputBuffer(size_t size);
    void ResetOutputBuffer();
    uint32_t GetIP() const;
    sa::SharedPtr<OutputMessage> TakeCurrentBuffer();
};

}
