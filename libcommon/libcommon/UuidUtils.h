/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace Utils::Uuid {

static constexpr const char* EMPTY_UUID = "00000000-0000-0000-0000-000000000000";
bool IsEmpty(const std::string& uuid);
bool IsEqual(const std::string& u1, const std::string& u2);
std::string New();
static constexpr int MAX_UUID = 36;

}
