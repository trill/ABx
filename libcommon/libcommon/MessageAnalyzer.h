/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include "NetworkMessage.h"
#include <sa/StringHash.h>
#include <sa/TypeName.h>
#include <functional>

namespace Net {

class MessageAnalyzer
{
public:
    void Analyze(const NetworkMessage& message) const;

    std::function<void(AB::GameProtocol::ServerPacketType type, void* packet)> onPacket_;
};

}
