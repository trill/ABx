/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sys/timeb.h>
#include <time.h>
#include <random>
#include <iterator>
#include <chrono>
#include <limits>

namespace Utils {

uint32_t AdlerChecksum(uint8_t* data, int32_t len);

/// Count of elements in an array
template <typename T, int N>
constexpr size_t CountOf(T(&)[N])
{
    return N;
}

}
