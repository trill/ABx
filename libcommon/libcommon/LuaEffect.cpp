/**
 * Copyright 2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LuaEffect.h"
#include "StringUtils.h"
#include "FileUtils.h"
#include "UuidUtils.h"
#include <sa/StringTempl.h>

extern std::string gDataDir;

namespace IO {

static void InitState(kaguya::State& state)
{
    state["include"] = kaguya::function([&state](const std::string& file)
    {
        std::string scriptFile = Utils::ConcatPath(gDataDir, file);
        if (!Utils::FileExists(scriptFile))
            return;

        // Make something like an include guard
        std::string ident(file);
        sa::MakeIdent(ident);
        ident = "__included_" + ident + "__";
        if (state[ident].type() == LUA_TBOOLEAN)
            return;
        if (!state.dofile(scriptFile.c_str()))
        {
            std::cerr << lua_tostring(state.state(), -1) << std::endl;
            return;
        }
        state[ident] = true;
    });
}

LuaEffect::LuaEffect()
{
    InitState(state_);
}

bool LuaEffect::Execute(const std::string& script)
{
    if (!state_.dofile(script.c_str()))
    {
        std::cerr << lua_tostring(state_.state(), -1) << std::endl;
        return false;
    }
    return true;
}

std::string LuaEffect::GetUuid()
{
    if (state_["uuid"].type() == LUA_TSTRING)
        return state_["uuid"];
    return Utils::Uuid::New();
}

int32_t LuaEffect::GetIndex()
{
    if (state_["index"].type() == LUA_TNUMBER)
        return state_["index"];
    return 0;
}

std::string LuaEffect::GetName()
{
    if (state_["name"].type() == LUA_TSTRING)
        return state_["name"];
    return "";
}

std::string LuaEffect::GetIcon()
{
    if (state_["icon"].type() == LUA_TSTRING)
        return state_["icon"];
    return "";
}

std::string LuaEffect::GetSoundEffect()
{
    if (state_["soundEffect"].type() == LUA_TSTRING)
        return state_["soundEffect"];
    return "";
}

std::string LuaEffect::GetParticleEffect()
{
    if (state_["particleEffect"].type() == LUA_TSTRING)
        return state_["particleEffect"];
    return "";
}

AB::Entities::EffectCategory LuaEffect::GetCategory()
{
    if (state_["category"].type() == LUA_TNUMBER)
        return static_cast<AB::Entities::EffectCategory>(state_["category"]);
    return AB::Entities::EffectNone;
}

std::string LuaEffect::GetDescription()
{
    if (state_["description"].type() == LUA_TSTRING)
        return state_["description"];
    return "";
}

bool LuaEffect::IsInternal()
{
    if (state_["internal"].type() == LUA_TBOOLEAN)
        return state_["internal"];
    return false;
}

}
