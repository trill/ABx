/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef AB_WINDOWS
#define AB_CONSOLE_LOGO \
    "##########  ######  ######\n" \
    "    ##          ##  ##\n" \
    "    ##  ######  ##  ##\n" \
    "    ##  ##      ##  ##\n" \
    "    ##  ##  ##  ##  ##\n" \
    "    ##  ##  ##  ##  ##\n" \
    "    ##  ##  ##  ##  ##"
#else
#define AB_CONSOLE_LOGO \
    "\033[91m##########  ######  ######\n" \
    "    ##          ##  ##\n" \
    "    ##  ######  ##  ##\n" \
    "    ##  ##      ##  ##\n" \
    "    ##  ##  ##  ##  ##\n" \
    "    ##  ##  ##  ##  ##\n" \
    "    ##  ##  ##  ##  ##\033[39m"
#endif
