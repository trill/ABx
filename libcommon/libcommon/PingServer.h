/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <thread>

namespace Net {

// Simple UPD echo server
class PingServer
{
private:
    std::shared_ptr<asio::io_service> ioService_;
    std::atomic<bool> running_{ false };
    std::thread thread_;
    void ThreadFunc();
public:
    PingServer();
    explicit PingServer(std::shared_ptr<asio::io_service> ioService);

    void Start();
    void Stop();

    uint16_t port_{ 0 };
};

}
