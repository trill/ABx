/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <filesystem>
#include <functional>

namespace IO {

namespace fs = std::filesystem;

class FileWatcher
{
private:
    std::string fileName_;
    fs::path path_;
    void* userData_{ nullptr };
    std::function<void(const std::string&, void*)> onChanged_;
    fs::file_time_type lastTime_{ fs::file_time_type::min() };
    bool enabled_{ false };
public:
    FileWatcher(const std::string& fileName, void* userData, std::function<void(const std::string, void*)>&& onChanged, bool defer);
    ~FileWatcher();
    void Update();
    void Start();
    void Stop();
    bool IsEnabled() const { return enabled_; }
};

}
