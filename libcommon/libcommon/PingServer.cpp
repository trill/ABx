/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PingServer.h"
#include "Logger.h"

namespace Net {

PingServer::PingServer() :
    ioService_(std::make_shared<asio::io_service>())
{
}

PingServer::PingServer(std::shared_ptr<asio::io_service> ioService) :
    ioService_(std::move(ioService))
{
}

void PingServer::Stop()
{
    if (!running_)
        return;
    running_ = false;
    thread_.detach();
}

void PingServer::Start()
{
    if (port_ == 0)
    {
        LOG_ERROR << "Port can not be 0" << std::endl;
        return;
    }
    running_ = true;
    thread_ = std::thread(&PingServer::ThreadFunc, this);
}

void PingServer::ThreadFunc()
{
    asio::ip::udp::socket socket{ *ioService_, asio::ip::udp::endpoint(asio::ip::udp::v4(), port_) };

    constexpr size_t MAX_DATA_SIZE = 64;
    char data[MAX_DATA_SIZE] = {};
    asio::ip::udp::endpoint senderEndpoint;
    while (running_)
    {
        asio::error_code ec;
        size_t count = socket.receive_from(asio::buffer(data, MAX_DATA_SIZE), senderEndpoint, 0, ec);
        if (!ec)
        {
            asio::error_code ec2;
            socket.send_to(asio::buffer(data, count), senderEndpoint, 0, ec2);
            if (!ec2 && ec2.value() != 0)
            {
                LOG_ERROR << "Error " << ec2.value() << ": " << ec2.message() << std::endl;
            }
        }
    }
    asio::error_code ec;
    socket.close(ec);
}

}
