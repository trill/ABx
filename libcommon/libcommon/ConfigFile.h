/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <map>
#include <sa/StringTempl.h>
#include <sa/Noncopyable.h>
#include <optional>

namespace IO {

class ConfigFile
{
    NON_COPYABLE(ConfigFile)
    NON_MOVEABLE(ConfigFile)
private:
    std::map<std::string, std::string> entires_;
public:
    ConfigFile();
    ~ConfigFile();
    bool Load(const std::string& filename);
    template<typename T>
    inline T Get(const std::string& name, T def) const;
};

template<>
inline std::string ConfigFile::Get<std::string>(const std::string& name, std::string def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    return (*it).second;
}

template<>
inline bool ConfigFile::Get<bool>(const std::string& name, bool def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    std::string lowerValue = sa::StringToLower<char>(it->second);
    return (lowerValue == "true" || lowerValue == "1" || lowerValue == "yes");
}

template<typename T>
inline T ConfigFile::Get(const std::string& name, T def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    const auto res = sa::ToNumber<T>((*it).second);
    if (res.has_value())
        return  res.value();
    return def;
}

}
