/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace Utils {

bool FileCopy(const std::string& src, const std::string& dst);
std::string NormalizeFilename(const std::string& filename);
bool FileExists(const std::string& name);
std::string AddSlash(const std::string& dir);
/// Files with a leading dot are hidden
bool IsHiddenFile(const std::string& path);
std::string GetExeName();
bool EnsureDirectory(const std::string& dir);
bool IsSourceNewerThanDest(const std::string& source, const std::string& dest);
std::string ExpandPath(const char* path);

}
