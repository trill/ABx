/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Protocol.h"
#include "NetworkMessage.h"
#include "OutputMessage.h"
#include "Scheduler.h"
#include <AB/ProtocolCodes.h>
#include <abcrypto.hpp>
#include "Connection.h"
#include "OutputMessage.h"
#include "MessageAnalyzer.h"

namespace Net {

Protocol::Protocol(std::shared_ptr<Connection> connection) :
    connection_(std::move(connection)),
    encryptionEnabled_(false)
{ }

Protocol::~Protocol() = default;

void Protocol::Disconnect() const
{
    if (auto conn = GetConnection())
        conn->Close();
}

uint32_t Protocol::GetIP() const
{
    if (auto c = GetConnection())
        return c->GetIP();
    return 0;
}

void Protocol::Send(sa::SharedPtr<OutputMessage>&& message)
{
#if 0
    MessageAnalyzer ma;
    ma.onPacket_ = [](auto type, void*)
    {
        if (type != AB::GameProtocol::ServerPacketType::GameUpdate)
            LOG_DEBUG << "Code " << static_cast<int>(type) << " " << AB::GameProtocol::GetServerPacketTypeName(type) << std::endl;
    };
    ma.Analyze(*message);
#endif
    if (auto conn = GetConnection())
    {
        conn->Send(std::forward<sa::SharedPtr<OutputMessage>>(message));
        return;
    }
    LOG_ERROR << "Connection expired, disconnecting" << std::endl;
    Release();
}

sa::SharedPtr<OutputMessage> Protocol::TakeCurrentBuffer()
{
    sa::SharedPtr<Net::OutputMessage> curr = std::move(outputBuffer_);
    ResetOutputBuffer();
    return curr;
}

void Protocol::XTEAEncrypt(OutputMessage& msg) const
{
    // The message must be a multiple of 8
    size_t paddingBytes = msg.GetSize() % 8;
    if (paddingBytes != 0)
        msg.AddPaddingBytes(static_cast<uint32_t>(8 - paddingBytes));

    uint32_t* buffer = reinterpret_cast<uint32_t*>(msg.GetOutputBuffer());
    const uint32_t messageLength = static_cast<uint32_t>(msg.GetSize());
    xxtea_enc(buffer, (messageLength / 4), reinterpret_cast<const uint32_t*>(&encKey_));
}

bool Protocol::XTEADecrypt(NetworkMessage& msg) const
{
    int32_t length = (int)msg.GetSize() - 6;
    if ((length & 7) != 0)
        return false;

    uint32_t* buffer = reinterpret_cast<uint32_t*>(msg.GetBuffer() + 6);
    xxtea_dec(buffer, static_cast<uint32_t>(length / 4), reinterpret_cast<const uint32_t*>(&encKey_));

    return true;
}

bool Protocol::OnSendMessage(OutputMessage& message) const
{
#ifdef DEBUG_NET
//    LOG_DEBUG << "Sending message with size " << message.GetSize() << std::endl;
#endif
    if (encryptionEnabled_)
        XTEAEncrypt(message);
    return message.AddCryptoHeader(true);
}

void Protocol::OnRecvMessage(NetworkMessage& message)
{
#ifdef DEBUG_NET
//    LOG_DEBUG << "Receiving message with size " << message.GetMessageLength() << std::endl;
#endif
    if (encryptionEnabled_)
    {
        if (!XTEADecrypt(message))
        {
#ifdef DEBUG_NET
            LOG_DEBUG << "Failed to decrypt message." << std::endl;
#endif
            return;
        }
    }
    ParsePacket(message);
}

sa::SharedPtr<OutputMessage> Protocol::GetOutputBuffer(size_t size)
{
    // Dispatcher Thread
    if (!outputBuffer_)
    {
        outputBuffer_ = OutputMessagePool::GetOutputMessage();
    }
    else if (outputBuffer_->GetSpace() <= size)
    {
        Send(std::move(outputBuffer_));
        outputBuffer_ = OutputMessagePool::GetOutputMessage();
    }
    return outputBuffer_;
}

void Protocol::ResetOutputBuffer()
{
    outputBuffer_ = OutputMessagePool::GetOutputMessage();
}

}
