#include <catch2/catch.hpp>

#include <sa/ArgParser.h>

TEST_CASE("ArgParser Success")
{
    sa::ArgParser::Cli opts{ {
        {"inputfile", { "-i", "--input-file" }, "Input file", true, true, sa::ArgParser::OptionType::String},
        {"outputfile", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String},
        {"number", { "-n", "--number" }, "A number", true, true, sa::ArgParser::OptionType::Integer},
        {"help", { "-h", "-help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None}
    } };
    std::vector<std::string> args;
    args.emplace_back("-i");
    args.emplace_back("input_file");
    args.emplace_back("-o");
    args.emplace_back("output_file");
    args.emplace_back("-n");
    args.emplace_back("42");

    sa::ArgParser::Values result;

    auto res = sa::ArgParser::Parse(args, opts, result);
    REQUIRE(res.success);
    {
        auto val = sa::ArgParser::GetValue<int>(result, "number");
        REQUIRE(val.has_value());
        REQUIRE(val.value() == 42);
    }
    {
        auto val = sa::ArgParser::GetValue<std::string>(result, "inputfile");
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "input_file");
    }
}

TEST_CASE("ArgParser multiple arg")
{
    sa::ArgParser::Cli opts{ {
        {"outputfile", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String},
    } };
    std::vector<std::string> args;
    args.emplace_back("file1");
    args.emplace_back("file2");
    args.emplace_back("file3");
    args.emplace_back("file4");
    args.emplace_back("file5");

    sa::ArgParser::Values result;

    auto res = sa::ArgParser::Parse(args, opts, result);
    REQUIRE(res.success);
    REQUIRE(result.size() == 5);
}

TEST_CASE("ArgParser Fail")
{
    sa::ArgParser::Cli opts{ {
        {"inputfile", { "-i", "--input-file" }, "Input file", true, true, sa::ArgParser::OptionType::String},
        {"outputfile", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String},
        {"number", { "-n", "--number" }, "A number", true, true, sa::ArgParser::OptionType::Integer},
        {"help", { "-h", "-help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None}
    } };
    std::vector<std::string> args;
    args.emplace_back("-o");
    args.emplace_back("output_file");

    sa::ArgParser::Values result;

    auto res = sa::ArgParser::Parse(args, opts, result);
    REQUIRE(!res.success);
}

TEST_CASE("ArgParser Fail missing arg")
{
    sa::ArgParser::Cli opts{ {
        {"inputfile", { "-i", "--input-file" }, "Input file", true, true, sa::ArgParser::OptionType::String},
        {"outputfile", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String},
        {"number", { "-n", "--number" }, "A number", true, true, sa::ArgParser::OptionType::Integer},
        {"help", { "-h", "-help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None}
    } };
    std::vector<std::string> args;
    args.emplace_back("-o");
    args.emplace_back("output_file");
    args.emplace_back("-n");
    args.emplace_back("-i");

    sa::ArgParser::Values result;

    auto res = sa::ArgParser::Parse(args, opts, result);
    REQUIRE(!res.success);
}

TEST_CASE("ArgParser name=value")
{
    sa::ArgParser::Cli opts{ {
        {"inputfile", { "-i", "--input-file" }, "Input file", true, true, sa::ArgParser::OptionType::String},
        {"outputfile", { "-o", "--output-file" }, "Output file", false, true, sa::ArgParser::OptionType::String},
        {"number", { "-n", "--number" }, "A number", true, true, sa::ArgParser::OptionType::Integer},
        {"help", { "-h", "-help", "-?" }, "Show help", false, false, sa::ArgParser::OptionType::None}
    } };
    std::vector<std::string> args;
    args.emplace_back("-i=input_file");
    args.emplace_back("-o=output_file");
    args.emplace_back("-n=42");

    sa::ArgParser::Values result;

    auto res = sa::ArgParser::Parse(args, opts, result);
    REQUIRE(res.success);
    {
        auto val = sa::ArgParser::GetValue<int>(result, "number");
        REQUIRE(val == 42);
    }
    {
        auto val = sa::ArgParser::GetValue<std::string>(result, "inputfile");
        REQUIRE(*val == "input_file");
    }
}
