#include <catch2/catch.hpp>

#include <libmath/Matrix4.h>
#include <libmath/Transformation.h>

TEST_CASE("Matrix4 Construct")
{
    Math::Matrix4 mat;
    XMath::XMMATRIX xmat = (XMath::XMMATRIX)mat;
    Math::Matrix4 mat2(xmat);
    REQUIRE(mat.Translation() == mat2.Translation());
    REQUIRE(mat.Scaling() == mat2.Scaling());
    REQUIRE(mat.Rotation() == mat2.Rotation());
}

TEST_CASE("Matrix4 FromQuaternion")
{
    Math::Quaternion q = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIF);
    Math::Matrix4 m1 = Math::Matrix4::FromQuaternion(q);
    Math::Matrix4 m2;
    m2.SetRotation(q.Conjugate());
    for (size_t i = 0; i < 16; ++i)
    {
        INFO("i = " << i)
        REQUIRE(m1.m_[i] == Approx(m2.m_[i]));
    }
}

TEST_CASE("Matrix4 Decompose")
{
    Math::Quaternion orient = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIHALF);
    Math::Transformation trans1(Math::Vector3_One, orient, Math::Vector3(0.5f, 0.5f, 0.5f));
    Math::Matrix4 matrix = trans1.GetMatrix();
    Math::Vector3 transl1;
    Math::Vector3 scaling1;
    Math::Quaternion orient1;
    matrix.Decompose(&transl1, &orient1, &scaling1);

    Math::Vector3 transl2 = matrix.Translation();
    Math::Vector3 scaling2 = matrix.Scaling();
    Math::Quaternion orient2 = matrix.Rotation();

    REQUIRE(transl1.Equals(transl2));
    REQUIRE(scaling1.Equals(scaling2));
    REQUIRE(orient1.Equals(orient2));
}

TEST_CASE("Matrix4 Rotation")
{
    Math::Quaternion orient = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIHALF);
    Math::Matrix4 matrix = orient.GetMatrix();
    Math::Quaternion orient2 = matrix.Rotation();

    REQUIRE(orient.Equals(orient2));
}
