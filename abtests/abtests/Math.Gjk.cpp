/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include <libmath/BoundingBox.h>
#include <libmath/Sphere.h>
#include <libmath/Gjk.h>

TEST_CASE("GJK AABB")
{
    Math::BoundingBox bb1(-2.0f, 2.0f);
    Math::BoundingBox bb2(0.0f, 1.0f);
    REQUIRE(Math::Gjk::StaticIntersects(bb1.GetMesh(), bb2.GetMesh()));
}

TEST_CASE("GJK AABB 2")
{
    Math::BoundingBox bb1(-2.0f, 2.0f);
    Math::BoundingBox bb2(2.5f, 3.0f);
    REQUIRE(!Math::Gjk::StaticIntersects(bb1.GetMesh(), bb2.GetMesh()));
}

TEST_CASE("GJK OBB")
{
    Math::BoundingBox bb1(-2.0f, 2.0f);
    Math::BoundingBox bb2(0.0f, 1.0f);
    auto orient = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIFOURTH);
    auto mesh = bb2.Transformed(orient.GetMatrix()).GetMesh();
    REQUIRE(Math::Gjk::StaticIntersects(bb1.GetMesh(), mesh));
}

TEST_CASE("GJK OBB 2")
{
    Math::BoundingBox bb1(-2.0f, 2.0f);
    Math::BoundingBox bb2(2.0f, 3.0f);
    auto orient = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIFOURTH);
    auto mesh = bb2.Transformed(orient.GetMatrix()).GetMesh();
    REQUIRE(!Math::Gjk::StaticIntersects(bb1.GetMesh(), mesh));
}

TEST_CASE("GJK Sphere")
{
    Math::Sphere s1({ 0.0f, 0.0f, 0.0f }, 2.0f);
    Math::Sphere s2({ 0.5f, 0.5f, 0.5f }, 1.0f);
    REQUIRE(Math::Gjk::StaticIntersects(s1.GetMesh(), s2.GetMesh()));
}

TEST_CASE("GJK Sphere 1")
{
    Math::Sphere s1({ 0.0f, 0.0f, 0.0f }, 2.0f);
    Math::Sphere s2({ 2.5f, 2.5f, 2.5f }, 1.0f);
    REQUIRE(!Math::Gjk::StaticIntersects(s1.GetMesh(), s2.GetMesh()));
}
