#include <catch2/catch.hpp>

#include <sa/color.h>

TEST_CASE("Color Construct")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 1.0f, 1.0f);
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 255);
    REQUIRE(col.b_ == 255);
    REQUIRE(col.a_ == 255);
}

TEST_CASE("Color from string")
{
    sa::Color col = sa::Color::FromString("ff00ff");
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 0);
    REQUIRE(col.b_ == 255);
    REQUIRE(col.a_ == 255);

    sa::Color col2 = sa::Color::FromString("ff00ffaa");
    REQUIRE(col2.r_ == 255);
    REQUIRE(col2.g_ == 0);
    REQUIRE(col2.b_ == 255);
    REQUIRE(col2.a_ == 0xaa);
}

TEST_CASE("Color to string")
{
    sa::Color col = sa::Color::FromString("ff00ff");
    std::string str = col.ToString();
    REQUIRE(str == "ff00ffff");
}

TEST_CASE("Color to unit32_t")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 0.0f, 0.0f, 0.0f);
    uint32_t value = static_cast<uint32_t>(col);
    REQUIRE(value == 255);
}

TEST_CASE("Color to HSL")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 1.0f, 1.0f);
    const auto [h, s, l] = col.ToHsl();
    REQUIRE(h == Approx(0.0f));
    REQUIRE(s == Approx(0.0f));
    REQUIRE(l == Approx(1.0f));
}

TEST_CASE("Color from HSL")
{
    sa::Color col = sa::Color::FromHsl(0.0f, 0.0f, 1.0f);
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 255);
    REQUIRE(col.b_ == 255);
    REQUIRE(col.a_ == 255);
}

TEST_CASE("Color from HSL2")
{
    sa::Color col = sa::Color::FromHsl(0.0f, 1.0f, 0.5f);
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 0);
    REQUIRE(col.b_ == 0);
}

TEST_CASE("Color to HSV")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 1.0f, 1.0f);
    const auto [h, s, v] = col.ToHsv();
    REQUIRE(h == Approx(0.0f));
    REQUIRE(s == Approx(0.0f));
    REQUIRE(v == Approx(1.0f));
}

TEST_CASE("Color from HSV")
{
    sa::Color col = sa::Color::FromHsl(0.0f, 0.0f, 1.0f);
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 255);
    REQUIRE(col.b_ == 255);
    REQUIRE(col.a_ == 255);
}

TEST_CASE("Color tint")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 0.0f, 0.0f);
    col.Filter<sa::filter::Tint>({ 0.5f });
    REQUIRE(col.r_ == 255);
    REQUIRE(col.g_ == 127);
    REQUIRE(col.b_ == 127);
}

TEST_CASE("Color shade")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 0.0f, 0.0f);
    col.Filter<sa::filter::Shade>({ 0.5f });
    REQUIRE(col.r_ == 127);
    REQUIRE(col.g_ == 0);
    REQUIRE(col.b_ == 0);
}

TEST_CASE("Color invert")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 0.2f, 0.0f);
    col.Filter<sa::filter::Invert>({});
    REQUIRE(col.r_ == 0);
    REQUIRE(col.g_ == 204);
    REQUIRE(col.b_ == 255);
}

TEST_CASE("Color scale")
{
    sa::Color col = sa::Color::FromRgb(0.6f, 0.2f, 0.4f);
    col.Filter<sa::filter::Scale>({ 0.5f });
    REQUIRE(col.r_ == 203);
    REQUIRE(col.g_ == 151);
    REQUIRE(col.b_ == 177);

    sa::Color col2 = sa::Color::FromRgb(0.6f, 0.2f, 0.4f);
    col2.Filter<sa::filter::Scale>({ -0.5f });
    REQUIRE(col2.r_ == 75);
    REQUIRE(col2.g_ == 25);
    REQUIRE(col2.b_ == 50);

    sa::Color col3 = sa::Color::FromRgb(0.6f, 0.2f, 0.4f);
    col3.Filter<sa::filter::Scale>({ 2.5f, 2.0f, 3.0f });
    REQUIRE(col3.r_ == 203);
    REQUIRE(col3.g_ == 151);
    REQUIRE(col3.b_ == 177);
}

TEST_CASE("Color lerp")
{
    sa::Color col = sa::Color::FromRgb(1.0f, 0.2f, 0.0f);
    sa::Color col2 = sa::Color::FromRgb(0.0f, 0.2f, 1.0f);
    sa::Color col3 = col.Filtered<sa::filter::Lerp>({ 0.5f }, col2);
    REQUIRE(col3.r_ == 127);
    REQUIRE(col3.g_ == 51);
    REQUIRE(col3.b_ == 127);
}

TEST_CASE("Color blend")
{
    sa::Color red = sa::Color::FromRgb(1.0f, 0.0f, 0.0f);
    sa::Color green50 = sa::Color::FromRgb(0.0f, 1.0f, 0.0f, 0.5f);
    sa::Color reg_green50 = red.Filtered<sa::filter::AlphaBlend>({}, green50);
    REQUIRE(reg_green50.r_ == 127);
    REQUIRE(reg_green50.g_ == 127);
    REQUIRE(reg_green50.b_ == 0);

    sa::Color blue30 = sa::Color::FromRgb(0.0f, 0.0f, 1.0f, 0.3f);
    sa::Color reg_blue30 = red.Filtered<sa::filter::AlphaBlend>({}, blue30);
    REQUIRE(reg_blue30.r_ == 179);
    REQUIRE(reg_blue30.g_ == 0);
    REQUIRE(reg_blue30.b_ == 76);

    sa::Color green50_blue30 = green50.Filtered<sa::filter::AlphaBlend>({}, blue30);
    REQUIRE(green50_blue30.r_ == 0);
    REQUIRE(green50_blue30.g_ == 179);
    REQUIRE(green50_blue30.b_ == 76);
}

TEST_CASE("Color gray_scale")
{
    sa::Color red = sa::Color::FromRgb(1.0f, 0.0f, 0.0f);
    red.Filter<sa::filter::GreyScale>({});
    REQUIRE(red.r_ == 147);
    REQUIRE(red.g_ == 147);
    REQUIRE(red.b_ == 147);
}
