#include <catch2/catch.hpp>

#include <AB/Entities/Serializer.h>
#include <string>
#include <vector>

struct TestStruct
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(b);
        s.value(i);
        s.value(str);
    }
    bool b = true;
    int i = 42;
    std::string str = "foo";
};

struct SubStruct : public TestStruct
{
    template<typename S>
    void Serialize(S& s)
    {
        // Call base class
        TestStruct::Serialize(s);

        s.value(b2);
        s.value(i2);
        s.value(str2);
    }
    bool b2 = true;
    int i2 = 42;
    std::string str2 = "baz";
};

struct ContainerStruct
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(items);
    }
    std::vector<std::string> items;
};

struct NestedStruct
{
    template<typename S>
    void Serialize(S& s)
    {
        s1.Serialize(s);
        c1.Serialize(s);
        s.value(m);
    }
    SubStruct s1;
    ContainerStruct c1;
    std::string m = "foo";
};

struct NestedStructValue
{
    template<typename S>
    void Serialize(S& s)
    {
        // value() can use the Serialize() function of members when they have it.
        s.value(s1);
        s.value(c1);
        s.value(m);
    }
    SubStruct s1;
    ContainerStruct c1;
    std::string m = "foo";
};

struct UDTContainer
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(items);
    }
    std::vector<TestStruct> items;
};

struct ArrayContainer
{
    template<typename S>
    void Serialize(S& s)
    {
        s.value(items);
    }
    std::array<uint32_t, 16> items;
};

TEST_CASE("Serializer Add/Get")
{
    AB::Entities::Serializer::DataBuff buff;
    TestStruct test{ .b = false, .i = 48, .str = "bar" };

    AB::Entities::Serializer::Set(test, buff);

    TestStruct result = AB::Entities::Serializer::Get<TestStruct>(buff);
    REQUIRE(result.b == test.b);
    REQUIRE(result.i == test.i);
    REQUIRE(result.str == test.str);
}

TEST_CASE("Serializer Inheritance")
{
    AB::Entities::Serializer::DataBuff buff;
    SubStruct test;
    test.b = false;
    test.i = 96;
    test.str = "bar";

    test.b2 = false;
    test.i2 = 128;
    test.str2 = "barbaz";

    AB::Entities::Serializer::Set(test, buff);

    SubStruct result = AB::Entities::Serializer::Get<SubStruct>(buff);
    REQUIRE(result.b == test.b);
    REQUIRE(result.i == test.i);
    REQUIRE(result.str == test.str);
    REQUIRE(result.b2 == test.b2);
    REQUIRE(result.i2 == test.i2);
    REQUIRE(result.str2 == test.str2);
}

TEST_CASE("Serializer Container")
{
    AB::Entities::Serializer::DataBuff buff;
    ContainerStruct test;
    for (size_t i = 0; i < 16; ++i)
        test.items.push_back(std::to_string(i));

    AB::Entities::Serializer::Set(test, buff);

    ContainerStruct result = AB::Entities::Serializer::Get<ContainerStruct>(buff);
    REQUIRE(result.items.size() == test.items.size());
    for (size_t i = 0; i < result.items.size(); ++i)
        REQUIRE(result.items[i] == test.items[i]);
}

TEST_CASE("Serializer Nested")
{
    AB::Entities::Serializer::DataBuff buff;
    NestedStruct test;
    test.s1.b = false;
    test.s1.i = 96;
    test.s1.str = "bar";

    test.s1.b2 = false;
    test.s1.i2 = 128;
    test.s1.str2 = "barbaz";

    for (size_t i = 0; i < 16; ++i)
        test.c1.items.push_back(std::to_string(i));

    AB::Entities::Serializer::Set(test, buff);

    NestedStruct result = AB::Entities::Serializer::Get<NestedStruct>(buff);

    REQUIRE(result.m == test.m);
    REQUIRE(result.s1.b == test.s1.b);
    REQUIRE(result.s1.i == test.s1.i);
    REQUIRE(result.s1.str == test.s1.str);
    REQUIRE(result.s1.b2 == test.s1.b2);
    REQUIRE(result.s1.i2 == test.s1.i2);
    REQUIRE(result.s1.str2 == test.s1.str2);

    REQUIRE(result.c1.items.size() == test.c1.items.size());
    for (size_t i = 0; i < result.c1.items.size(); ++i)
        REQUIRE(result.c1.items[i] == test.c1.items[i]);
}

TEST_CASE("Serializer Nested value()")
{
    AB::Entities::Serializer::DataBuff buff;
    NestedStructValue test;
    test.s1.b = false;
    test.s1.i = 96;
    test.s1.str = "bar";

    test.s1.b2 = false;
    test.s1.i2 = 128;
    test.s1.str2 = "barbaz";

    for (size_t i = 0; i < 16; ++i)
        test.c1.items.push_back(std::to_string(i));

    AB::Entities::Serializer::Set(test, buff);

    NestedStructValue result = AB::Entities::Serializer::Get<NestedStructValue>(buff);

    REQUIRE(result.m == test.m);
    REQUIRE(result.s1.b == test.s1.b);
    REQUIRE(result.s1.i == test.s1.i);
    REQUIRE(result.s1.str == test.s1.str);
    REQUIRE(result.s1.b2 == test.s1.b2);
    REQUIRE(result.s1.i2 == test.s1.i2);
    REQUIRE(result.s1.str2 == test.s1.str2);

    REQUIRE(result.c1.items.size() == test.c1.items.size());
    for (size_t i = 0; i < result.c1.items.size(); ++i)
        REQUIRE(result.c1.items[i] == test.c1.items[i]);
}

TEST_CASE("Serializer UDT container")
{
    AB::Entities::Serializer::DataBuff buff;
    UDTContainer test;
    for (size_t i = 0; i < 16; ++i)
    {
        test.items.push_back({ .b = false, .i = (int)i, .str = std::to_string(i) });
    }
    AB::Entities::Serializer::Set(test, buff);
    UDTContainer result = AB::Entities::Serializer::Get<UDTContainer>(buff);
    REQUIRE(test.items.size() == result.items.size());
    for (size_t i = 0; i < result.items.size(); ++i)
    {
        REQUIRE(result.items[i].b == test.items[i].b);
        REQUIRE(result.items[i].i == test.items[i].i);
        REQUIRE(result.items[i].str == test.items[i].str);
    }
}

TEST_CASE("Serializer array container")
{
    AB::Entities::Serializer::DataBuff buff;
    ArrayContainer test{};
    for (size_t i = 0; i < test.items.size(); ++i)
    {
        test.items[i] = (uint32_t)i;
    }
    AB::Entities::Serializer::Set(test, buff);
    ArrayContainer result = AB::Entities::Serializer::Get<ArrayContainer>(buff);
    REQUIRE(test.items.size() == result.items.size());
    for (size_t i = 0; i < result.items.size(); ++i)
    {
        REQUIRE(result.items[i] == test.items[i]);
    }
}
