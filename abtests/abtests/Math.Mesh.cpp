#include <catch2/catch.hpp>

#include <libmath/Mesh.h>
#include <libmath/BoundingBox.h>
#include <libmath/Sphere.h>

TEST_CASE("IsTriangles")
{
    Math::BoundingBox bb(-2.0f, 2.0f);
    Math::Mesh bbShape = bb.GetMesh();
    REQUIRE(bbShape.IsTriangles());
}
