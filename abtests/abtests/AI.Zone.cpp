#include <catch2/catch.hpp>

#include <libai/LuaLoader.h>
#include <libai/Registry.h>
#include <libai/Zone.h>
#include <libai/Agent.h>

TEST_CASE("Zone")
{
    AI::Registry reg;
    reg.Initialize();
    AI::LuaLoader loader(reg);
    std::string script = R"lua(
function init(root)
    local nd = node("Parallel")
    root:AddNode(nd)
end
)lua";
    auto root = loader.LoadString(script);
    REQUIRE(root);

    auto agent = std::make_shared<AI::Agent>(1);
    agent->SetBehavior(root);
    AI::Zone zone("test");
    zone.AddAgent(agent);
    zone.Update(0);

}
