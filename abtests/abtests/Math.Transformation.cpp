#include <catch2/catch.hpp>

#include <libmath/Transformation.h>
#include <libmath/Matrix4.h>

TEST_CASE("Transformation Basic")
{
    SECTION("Construct")
    {
        Math::Transformation trans;
        REQUIRE(trans.position_.Equals(Math::Vector3_Zero));
        REQUIRE(trans.scale_.Equals(Math::Vector3_One));
        REQUIRE(trans.orientation_.Equals(Math::Quaternion_Identity));
    }
    SECTION("Rotation")
    {
        Math::Transformation trans;
        trans.SetYRotation(Math::M_PIHALF);
        REQUIRE(trans.GetYRotation() == Approx(Math::M_PIHALF));
    }
    SECTION("GetMatrix")
    {
        Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
        Math::Matrix4 matrix = trans.GetMatrix();
        Math::Vector3 transl;
        Math::Vector3 scaling;
        Math::Quaternion orient;
        matrix.Decompose(&transl, &orient, &scaling);
        INFO("transl = " << transl.ToString());
        REQUIRE(transl.Equals(Math::Vector3_One));
        REQUIRE(scaling.Equals(Math::Vector3(0.5f, 0.5f, 0.5f)));
        Math::Quaternion orient2 = Math::Quaternion::FromAxisAngle(Math::Vector3_UnitY, Math::M_PIHALF);
        REQUIRE(orient.Equals(orient2));
    }
}

TEST_CASE("Transformation Move")
{
    float speed = 0.5f;
    const Math::Vector3& amount = Math::Vector3_UnitZ;
    SECTION("Move")
    {
        Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
        trans.Move(speed, amount);
        REQUIRE(trans.position_.x_ == Approx(0.5f));
        REQUIRE(trans.position_.y_ == Approx(1.0f));
        REQUIRE(trans.position_.z_ == Approx(1.0f));
    }
    SECTION("Move Internal")
    {
        Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
        Math::Matrix4 m = Math::Matrix4::FromQuaternion(trans.orientation_.Inverse());
        Math::Vector3 a = amount * speed;
        Math::Vector3 v = m * a;
        trans.position_ += v;
        REQUIRE(trans.position_.x_ == Approx(0.5f));
        REQUIRE(trans.position_.y_ == Approx(1.0f));
        REQUIRE(trans.position_.z_ == Approx(1.0f));
    }
    SECTION("Move XMath")
    {
        Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
        XMath::XMMATRIX m = XMath::XMMatrixRotationAxis(Math::Vector3_UnitY, -trans.GetYRotation());
        Math::Vector3 a = amount * speed;
        XMath::XMVECTOR v = XMath::XMVector3Transform(a, m);
        trans.position_.x_ += XMath::XMVectorGetX(v);
        trans.position_.y_ += XMath::XMVectorGetY(v);
        trans.position_.z_ += XMath::XMVectorGetZ(v);
        REQUIRE(trans.position_.x_ == Approx(0.5f));
        REQUIRE(trans.position_.y_ == Approx(1.0f));
        REQUIRE(trans.position_.z_ == Approx(1.0f));
    }
}

TEST_CASE("Transformation Turn")
{
    Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
    trans.Turn(Math::M_PIHALF);
    REQUIRE(trans.GetYRotation() == Approx(Math::M_PIF));
}

TEST_CASE("Transformation Matrix")
{
    Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3(0.5f, 0.5f, 0.5f));
    Math::Matrix4 matrix1 = trans.GetMatrix();
    Math::Matrix4 matrix2(trans.position_, trans.orientation_, trans.scale_);
    for (size_t i = 0; i < 16; ++i)
    {
        INFO("i = " << i)
        // Approx may still fail on CI:
        // /home/vsts/work/1/s/abtests/abtests/Math.Transformation.cpp:112: FAILED:
        //     REQUIRE(matrix1.m_[i] == Approx(matrix2.m_[i]))
        // with expansion :
        //     0.0f == Approx(0.0000000171)
        // with message :
        //     0
        REQUIRE(Math::Equals(fabs(matrix1.m_[i] - matrix2.m_[i]), 0.0f, 0.0000001f));
    }
}

TEST_CASE("Transformation Matrix multiply")
{
    Math::Transformation trans(Math::Vector3_One, 0.0f, Math::Vector3_One);
    Math::Transformation trans2(Math::Vector3_One, 0.0f, Math::Vector3_One);
    Math::Matrix4 matrix1 = trans2.GetMatrix() * trans.GetMatrix();
    Math::Transformation result = Math::Transformation(matrix1);
    REQUIRE(result.position_.x_ == Approx(2.0f));
    REQUIRE(result.position_.y_ == Approx(2.0f));
    REQUIRE(result.position_.y_ == Approx(2.0f));
    auto euler = result.orientation_.EulerAngles();
    REQUIRE(euler.x_ == Approx(0.0f));
    REQUIRE(euler.y_ == Approx(0.0f));
    REQUIRE(euler.z_ == Approx(0.0f));
    REQUIRE(result.scale_.x_ == Approx(1.0f));
    REQUIRE(result.scale_.y_ == Approx(1.0f));
    REQUIRE(result.scale_.z_ == Approx(1.0f));
}

TEST_CASE("Transformation Matrix multiply 2")
{
    Math::Transformation trans(Math::Vector3_One, Math::M_PIHALF, Math::Vector3_One);
    Math::Transformation trans2(Math::Vector3_One, -Math::M_PIF, Math::Vector3_One);
    Math::Matrix4 matrix1 = trans2.GetMatrix() * trans.GetMatrix();
    Math::Transformation result = Math::Transformation(matrix1);
    REQUIRE(result.position_.x_ == Approx(2.0f));
    REQUIRE(result.position_.y_ == Approx(2.0f));
    REQUIRE(Math::Equals(result.position_.z_, 0.0f));
    auto euler = result.orientation_.EulerAngles();
    REQUIRE(euler.x_ == Approx(0.0f));
    REQUIRE(euler.y_ == Approx(-Math::M_PIHALF));
    REQUIRE(euler.z_ == Approx(0.0f));
    REQUIRE(result.scale_.x_ == Approx(1.0f));
    REQUIRE(result.scale_.y_ == Approx(1.0f));
    REQUIRE(result.scale_.z_ == Approx(1.0f));
}
