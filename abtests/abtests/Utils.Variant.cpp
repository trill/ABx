#include <catch2/catch.hpp>
#include <variant>
#include <libcommon/Variant.h>

TEST_CASE("Variant assign")
{
    Utils::Variant i32 = 1;
    REQUIRE(i32.GetType() == Utils::VariantType::Int);
    REQUIRE(i32.GetInt() == 1);
    Utils::Variant i64 = 1ll;
    REQUIRE(i64.GetType() == Utils::VariantType::Int64);
    REQUIRE(i64.GetInt64() == 1ll);
    Utils::Variant s = "foo";
    REQUIRE(s.GetType() == Utils::VariantType::String);
    REQUIRE(s.GetString() == "foo");

    constexpr std::string_view sv = "bar";
    Utils::Variant vsv = sv;
    REQUIRE(vsv.GetType() == Utils::VariantType::String);
    REQUIRE(vsv.GetString() == "bar");

}
