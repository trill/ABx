#include <catch2/catch.hpp>

#include <kaguya/kaguya.hpp>

namespace details {
class Actor
{
public:
    int GetProp() const { return prop_; }
    void SetProp(int value) { prop_ = value; }
private:
    int prop_{ 0 };
};
}

TEST_CASE("kaguya Property RW")
{
    kaguya::State state;
    state["Actor"].setClass(std::move(kaguya::UserdataMetatable<details::Actor>()
        .addProperty("Prop", &details::Actor::GetProp, &details::Actor::SetProp)
    ));

    details::Actor actor;
    state["self"] = &actor;

    REQUIRE(actor.GetProp() == 0);
    static constexpr const char* Source = R"lua(
print(self.Prop)
self.Prop = 1
print(self.Prop)
)lua";

    state.dostring(Source);
    REQUIRE(actor.GetProp() == 1);
}

TEST_CASE("kaguya Property RW2")
{
    kaguya::State state;
    state["Actor"].setClass(std::move(kaguya::UserdataMetatable<details::Actor>()
        .addProperty("Prop", &details::Actor::GetProp, &details::Actor::SetProp)
    ));

    details::Actor actor;
    state["self"] = &actor;

    REQUIRE(actor.GetProp() == 0);
    static constexpr const char* Source = R"lua(
function setProp(v)
    self.Prop = v
end
)lua";

    state.dostring(Source);
    state["setProp"](5);
    REQUIRE(actor.GetProp() == 5);
}

TEST_CASE("kaguya setClass()")
{
    kaguya::State state;
    state["Actor"].setClass(kaguya::UserdataMetatable<details::Actor>()
        .addProperty("Prop", &details::Actor::GetProp, &details::Actor::SetProp)
    );

    details::Actor actor;
    state["self"] = &actor;

    REQUIRE(actor.GetProp() == 0);
    static constexpr const char* Source = R"lua(
function setProp(v)
    self.Prop = v
end
)lua";

    state.dostring(Source);
    state["setProp"](5);
    REQUIRE(actor.GetProp() == 5);
}

TEST_CASE("kaguya Property RO")
{
    kaguya::State state;
    state["Actor"].setClass(std::move(kaguya::UserdataMetatable<details::Actor>()
        .addProperty("Prop", &details::Actor::GetProp)
    ));

    details::Actor actor;
    state["self"] = &actor;

    REQUIRE(actor.GetProp() == 0);
    static constexpr const char* Source = R"lua(
print(self.Prop)
self.Prop = 1
print(self.Prop)
)lua";

    state.dostring(Source);
    // Still 0
    REQUIRE(actor.GetProp() == 0);
}
