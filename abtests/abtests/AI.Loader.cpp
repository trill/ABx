#include <catch2/catch.hpp>

#include <libai/LuaLoader.h>
#include <libai/Registry.h>

TEST_CASE("Load")
{
    AI::Registry reg;
    reg.Initialize();
    AI::LuaLoader loader(reg);
    std::string script = R"lua(
function init(root)
    local nd = node("Parallel", {1, "xx"})
    root:AddNode(nd)
end
)lua";
    auto root = loader.LoadString(script);
    REQUIRE(root);
}
