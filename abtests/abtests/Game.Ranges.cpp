#include <catch2/catch.hpp>

#include <libshared/Mechanic.h>
#include <array>
#include <vector>
#include <algorithm>
#include <libmath/MathUtils.h>

TEST_CASE("Ranges sorted")
{
    // Ranges must be sorted
    std::array<float, Game::Internal::CountOf(Game::RangeDistances)> sorted;
    std::copy(std::begin(Game::RangeDistances), std::end(Game::RangeDistances), std::begin(sorted));
    std::sort(sorted.begin(), sorted.end());
    REQUIRE(sorted.size() == Game::Internal::CountOf(Game::RangeDistances));
    for (size_t i = 0; i < Game::Internal::CountOf(Game::RangeDistances); ++i)
    {
        REQUIRE(Math::Equals(sorted[i], Game::RangeDistances[i]));
    }
}

TEST_CASE("GetRangeFromDistance")
{
    std::vector<float> unique;
    unique.resize(Game::Internal::CountOf(Game::RangeDistances));
    std::copy(std::begin(Game::RangeDistances), std::end(Game::RangeDistances), std::begin(unique));

    for (size_t i = 0; i < unique.size(); ++i)
    {
        float dt = unique[i];
        Game::Range r = Game::GetRangeFromDistance(dt);
        float d = Game::RangeDistances[static_cast<size_t>(r)];
        REQUIRE(Math::Equals(d, dt));
    }

    {
        Game::Range r2 = Game::GetRangeFromDistance(1.0f);
        REQUIRE(r2 == Game::Range::Touch);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(3.0f);
        REQUIRE(r2 == Game::Range::Adjecent);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(27.0f);
        REQUIRE(r2 == Game::Range::Casting);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(50.0f);
        REQUIRE(r2 == Game::Range::HalfCompass);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(300.0f);
        REQUIRE(r2 == Game::Range::Map);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(500.0f);
        REQUIRE(r2 == Game::Range::Map);
    }
    {
        Game::Range r2 = Game::GetRangeFromDistance(std::numeric_limits<float>::max());
        REQUIRE(r2 == Game::Range::Map);
    }
    {
        constexpr Game::Range r2 = Game::GetRangeFromDistance(3.0f);
        static_assert(r2 == Game::Range::Adjecent);
    }
}
