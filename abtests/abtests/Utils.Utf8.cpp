#include <catch2/catch.hpp>

#include <libcommon/StringUtils.h>

TEST_CASE("UTF-8 lowercase")
{
    std::string s1 = "ÖÄÜ";
    std::string s1lower = Utils::Utf8ToLower(s1);
    REQUIRE(s1lower.compare("öäü") == 0);
}

TEST_CASE("UTF-8 lowercase 2")
{
    std::string s1 = "Άρθουρ Ντεντ";
    std::string s1lower = Utils::Utf8ToLower(s1);
    REQUIRE(s1lower.compare("άρθουρ ντεντ") == 0);
}

TEST_CASE("UTF-8 ci equls")
{
    REQUIRE(Utils::SameName("öäü", "ÖäÜ"));
}

TEST_CASE("UTF-8 ci equls 2")
{
    REQUIRE(Utils::SameName("Άρθουρ Ντεντ", "άρθουρ ντεντ"));
}

TEST_CASE("Wide to UTF-8")
{
    std::wstring wstr = L"Άρθουρ Ντεντ";
    std::string utf8 = Utils::WStringToUtf8(wstr);
    REQUIRE(utf8.compare("Άρθουρ Ντεντ") == 0);
}

TEST_CASE("UTF-8 to Wide")
{
    std::string utf8 = "Άρθουρ Ντεντ";
    std::wstring wstr = Utils::Utf8ToWString(utf8);
    REQUIRE(wstr.compare(L"Άρθουρ Ντεντ") == 0);
}
