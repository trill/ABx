/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <libmath/Color.h>

TEST_CASE("Math::Color Construct", "[color]")
{
    SECTION("Parse from string no alpha")
    {
        Math::Color vec("1.0 2.0 1.0");
        REQUIRE(vec.r_ == 1.0f);
        REQUIRE(vec.g_ == 2.0f);
        REQUIRE(vec.b_ == 1.0f);
        REQUIRE(vec.a_ == 1.0f);
    }
    SECTION("Parse from string alpha")
    {
        Math::Color vec("1.0 2.0 1.0 0.5f");
        REQUIRE(vec.r_ == 1.0f);
        REQUIRE(vec.g_ == 2.0f);
        REQUIRE(vec.b_ == 1.0f);
        REQUIRE(vec.a_ == 0.5f);
    }
}

TEST_CASE("Math::Color Compare", "[color]")
{
    SECTION("Compare equality")
    {
        Math::Color vec1(1.0f, 2.0f, 1.0f);
        Math::Color vec2(1.0f, 2.0f, 1.0f);
        REQUIRE(vec1 == vec2);
    }
    SECTION("Compare inequality")
    {
        Math::Color vec1(1.0f, 2.0f, 1.0f);
        Math::Color vec2(1.0f, 1.0f, 1.0f);
        REQUIRE(vec1 != vec2);
    }
}
