#include <catch2/catch.hpp>

#include <sa/Checked.h>

TEST_CASE("OverflowsAdd(max)")
{
    REQUIRE(sa::OverflowsAdd<int>(10, 1, 10));
    REQUIRE(!sa::OverflowsAdd<int>(9, 1, 10));

    REQUIRE(sa::OverflowsAdd<int>(-10, -1, -10));
    REQUIRE(!sa::OverflowsAdd<int>(-9, -1, -10));
}

TEST_CASE("OverflowsAdd()")
{
    REQUIRE(sa::OverflowsAdd<int>(std::numeric_limits<int>::max(), 1));
    REQUIRE(!sa::OverflowsAdd<int>(std::numeric_limits<int>::max() - 1, 1));

    REQUIRE(sa::OverflowsAdd<int>(std::numeric_limits<int>::min(), -1));
    REQUIRE(!sa::OverflowsAdd<int>(std::numeric_limits<int>::min() + 1, -1));
}

TEST_CASE("CheckedAdd()")
{
    REQUIRE(sa::ClampedAdd<int>(10, 5) == 15);
    REQUIRE(sa::ClampedAdd<int>(std::numeric_limits<int>::max(), 10) == std::numeric_limits<int>::max());
    REQUIRE(sa::ClampedAdd<unsigned>(std::numeric_limits<unsigned>::max(), 10) == std::numeric_limits<unsigned>::max());
    REQUIRE(sa::ClampedAdd<unsigned>(10, 10) == 20);
}
