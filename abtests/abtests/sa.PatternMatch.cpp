#include <catch2/catch.hpp>

#include <sa/StringTempl.h>
#include <string>

TEST_CASE("PatternMatch simple")
{
    std::string s1 = "foo";
    std::string s2 = "foo";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch *")
{
    std::string s1 = "foobarnstuff";
    std::string s2 = "foo*";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch * 2")
{
    std::string s1 = "foobarbaz";
    std::string s2 = "foo*baz";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch * 3")
{
    std::string s1 = "foobarbaz";
    std::string s2 = "*bar*";
    std::string s3 = "bar*";
    REQUIRE(sa::PatternMatch(s1, s2));
    REQUIRE(!sa::PatternMatch(s1, s3));
}

TEST_CASE("PatternMatch * 4")
{
    std::string s1 = "foobarbaz";
    std::string s2 = "*foo*";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch * 5")
{
    std::string s1 = "Insignia";
    std::string s2 = "*ins*";
    REQUIRE(sa::PatternMatch(s1, s2));
    std::string s3 = "*ignia*";
    REQUIRE(sa::PatternMatch(s1, s3));
}

TEST_CASE("PatternMatch wchar_t *")
{
    std::wstring s1 = L"foobarbaz";
    std::wstring s2 = L"*bar*";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch ?")
{
    std::string s1 = "fouo";
    std::string s2 = "fo?o";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch CI *")
{
    std::string s1 = "foobarnstuff";
    std::string s2 = "fOo*";
    REQUIRE(sa::PatternMatch(s1, s2));
}

TEST_CASE("PatternMatch CI wchar_t *")
{
    std::wstring s1 = L"foobarbaz";
    std::wstring s2 = L"*bAr*";
    REQUIRE(sa::PatternMatch(s1, s2));
}
