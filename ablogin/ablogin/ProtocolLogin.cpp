/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ProtocolLogin.h"
#include <libaccount/Account.h>
#include <libaccount/Character.h>
#include "IOGame.h"
#include "IOService.h"
#include <AB/CommonConfig.h>
#include <AB/Entities/Account.h>
#include <AB/Entities/Game.h>
#include <AB/Packets/Packet.h>
#include <AB/ProtocolCodes.h>
#include <libcommon/BanManager.h>
#include <libcommon/DataClient.h>
#include <libcommon/Dispatcher.h>
#include <libcommon/Connection.h>
#include <libcommon/OutputMessage.h>
#include <libcommon/Scheduler.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <functional>
#include <uuid.h>

//#define DEBUG_NET

namespace Net {

void ProtocolLogin::OnRecvFirstMessage(NetworkMessage& message)
{
    std::shared_ptr<Connection> conn = GetConnection();
    uint32_t clientIp = conn->GetIP();

    message.Skip(2);    // Client OS

    uint16_t version = message.Get<uint16_t>();
    if (version != AB::PROTOCOL_VERSION)
    {
        LOG_WARNING << "Wrong protocol version from client " << Utils::ConvertIPToString(clientIp) << std::endl;
        DisconnectClient(AB::ErrorCodes::WrongProtocolVersion);
        return;
    }

    uint8_t recvByte = message.GetByte();
    switch (recvByte)
    {
    case AB::LoginProtocol::LoginLogin:
        HandleLoginPacket(message);
        break;
    case AB::LoginProtocol::LoginCreateAccount:
        HandleCreateAccountPacket(message);
        break;
    case AB::LoginProtocol::LoginCreateCharacter:
        HandleCreateCharacterPacket(message);
        break;
    case AB::LoginProtocol::LoginDeleteCharacter:
        HandleDeleteCharacterPacket(message);
        break;
    case AB::LoginProtocol::LoginAddAccountKey:
        HandleAddAccountKeyPacket(message);
        break;
    case AB::LoginProtocol::LoginGetOutposts:
        HandleGetOutpostsPacket(message);
        break;
    case AB::LoginProtocol::LoginGetGameServers:
        HandleGetServersPacket(message);
        break;
    default:
        LOG_ERROR << Utils::ConvertIPToString(clientIp) << ": Unknown packet header: 0x" <<
            std::hex << static_cast<uint16_t>(recvByte) << std::dec << std::endl;
        // Shenanigan, don't tell the client whats wrong
        DelayedDisconnectClient(AB::ErrorCodes::UnknownError);
        break;
    }
}

void ProtocolLogin::HandleLoginPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::Login>(message);
    if (packet.accountName.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Account name is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccountName);
        return;
    }
    if (packet.password.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Password is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidPassword);
        return;
    }

    loginAttempts_ = 0;
    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::AuthenticateSendCharacterList, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleCreateAccountPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::CreateAccount>(message);
    if (packet.accountName.empty())
    {
        DisconnectClient(AB::ErrorCodes::InvalidAccountName);
        return;
    }
    if (packet.accountName.length() < ACCOUNT_NAME_MIN || packet.accountName.length() > ACCOUNT_NAME_MAX)
    {
        DisconnectClient(AB::ErrorCodes::InvalidAccountName);
        return;
    }
    if (packet.password.length() < PASSWORD_LENGTH_MIN || packet.password.length() > PASSWORD_LENGTH_MAX)
    {
        DisconnectClient(AB::ErrorCodes::InvalidPassword);
        return;
    }
#ifdef EMAIL_MANDATORY
    if (packet.email.empty())
    {
        DisconnectClient(AB::ErrorCodes::InvalidEmail);
        return;
    }
    if (packet.email.length() < 3)
    {
        DisconnectClient(AB::ErrorCodes::InvalidEmail);
        return;
    }
    if (packet.email.length() > EMAIL_LENGTH_MAX)
    {
        DisconnectClient(AB::ErrorCodes::InvalidEmail);
        return;
    }
#endif
    if (packet.accountKey.empty())
    {
        DisconnectClient(AB::ErrorCodes::InvalidAccountKey);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::CreateAccount, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleCreateCharacterPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::CreatePlayer>(message);
    if (packet.accountUuid.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Account UUID id empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }
    if (packet.authToken.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Auth token is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }
    if (packet.charName.empty())
    {
        DisconnectClient(AB::ErrorCodes::InvalidCharacterName);
        return;
    }
    if (packet.charName.length() < CHARACTER_NAME_NIM || packet.charName.length() > CHARACTER_NAME_MAX)
    {
        DisconnectClient(AB::ErrorCodes::InvalidCharacterName);
        return;
    }
    if (packet.charName.find_first_of(RESTRICTED_NAME_CHARS) != std::string::npos)
    {
        DisconnectClient(AB::ErrorCodes::InvalidCharacterName);
        return;
    }

    if (packet.sex < static_cast<uint8_t>(AB::Entities::CharacterSex::Female) ||
        packet.sex > static_cast<uint8_t>(AB::Entities::CharacterSex::Male))
    {
        DisconnectClient(AB::ErrorCodes::InvalidPlayerSex);
        return;
    }
    if (Utils::Uuid::IsEmpty(packet.profUuid))
    {
        DisconnectClient(AB::ErrorCodes::InvalidProfession);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::CreatePlayer, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleDeleteCharacterPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::DeleteCharacter>(message);
    if (packet.accountUuid.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Account UUID is empy" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }
    if (packet.authToken.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Auth token is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }
    if (Utils::Uuid::IsEmpty(packet.charUuid))
    {
        DisconnectClient(AB::ErrorCodes::InvalidCharacter);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::DeletePlayer, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleAddAccountKeyPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::AddAccountKey>(message);
    if (packet.accountUuid.empty())
    {
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }
    if (packet.authToken.empty())
    {
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }
    if (packet.accountKey.empty())
    {
        DisconnectClient(AB::ErrorCodes::InvalidAccountKey);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::AddAccountKey, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleGetOutpostsPacket(NetworkMessage& message)
{
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::GetOutposts>(message);
    if (packet.accountUuid.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Account UUID is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }
    if (packet.authToken.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Auth Token is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::SendOutposts, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::HandleGetServersPacket(NetworkMessage& message)
{
#ifdef DEBUG_NET
    LOG_DEBUG << "Sending server list" << std::endl;
#endif
    const auto packet = AB::Packets::Get<AB::Packets::Client::Login::GetServers>(message);
    if (packet.accountUuid.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Account UUID is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        return;
    }
    if (packet.authToken.empty())
    {
#ifdef DEBUG_NET
        LOG_ERROR << "Auth Token is empty" << std::endl;
#endif
        DelayedDisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    GetSubsystem<Asynch::Dispatcher>()->Add(
        Asynch::CreateTask(std::bind(
            &ProtocolLogin::SendServers, thisPtr,
            packet
        ))
    );
}

void ProtocolLogin::AuthenticateSendCharacterList(AB::Packets::Client::Login::Login request)
{
    AB::Entities::Account account;
    account.name = request.accountName;
    Account::PasswordAuthResult res = Account::PasswordAuth(request.password, account);

    auto* banMan = GetSubsystem<Auth::BanManager>();
    switch (res)
    {
    case Account::PasswordAuthResult::InvalidAccount:
        DelayedDisconnectClient(AB::ErrorCodes::InvalidAccount);
        banMan->AddLoginAttempt(GetIP(), false);
        return;
    case Account::PasswordAuthResult::PasswordMismatch:
        DelayedDisconnectClient(AB::ErrorCodes::NamePasswordMismatch);
        banMan->AddLoginAttempt(GetIP(), false);
        return;
    case Account::PasswordAuthResult::AlreadyLoggedIn:
    {
        ++loginAttempts_;
        if (loginAttempts_ < 2)
        {
            // When the client quickly logs out and logs in (e.g. when selecting a character) the record in the DB may not be updated yet, so try again once.
            std::shared_ptr<ProtocolLogin> thisPtr = std::static_pointer_cast<ProtocolLogin>(shared_from_this());
            GetSubsystem<Asynch::Scheduler>()->Add(
                Asynch::CreateScheduledTask(500, std::bind(
                    &ProtocolLogin::AuthenticateSendCharacterList, thisPtr,
                    std::move(request)
                ))
            );
            return;
        }

        DelayedDisconnectClient(AB::ErrorCodes::AlreadyLoggedIn);
        banMan->AddLoginAttempt(GetIP(), false);
        return;
    }
    case Account::PasswordAuthResult::AccountBanned:
        DelayedDisconnectClient(AB::ErrorCodes::AccountBanned);
        banMan->AddLoginAttempt(GetIP(), false);
        return;
    case Account::PasswordAuthResult::InternalError:
        DelayedDisconnectClient(AB::ErrorCodes::UnknownError);
        return;
    default:
        break;
    }

    AB::Entities::Service gameServer;
    if (!IO::EnsureService(
        AB::Entities::ServiceTypeGameServer,
        gameServer,
        account.currentServerUuid))
    {
        DisconnectClient(AB::ErrorCodes::AllServersFull);
        return;
    }

    AB::Entities::Service fileServer;
    if (!IO::EnsureService(
        AB::Entities::ServiceTypeFileServer,
        fileServer))
    {
        DisconnectClient(AB::ErrorCodes::AllServersFull);
        return;
    }

    banMan->AddLoginAttempt(GetIP(), true);

    LOG_INFO << Utils::ConvertIPToString(GetIP(), true) << ": " << request.accountName << " logged in" << std::endl;

    auto output = OutputMessagePool::GetOutputMessage();

    output->AddByte(AB::LoginProtocol::CharacterList);

    AB::Packets::Server::Login::CharacterList packet;
    packet.accountUuid = account.uuid;
    packet.authToken = account.authToken;
    packet.accountType = static_cast<uint8_t>(account.type);
    packet.serverHost = gameServer.host;
    packet.serverPort = gameServer.port;
    packet.fileHost = fileServer.host;
    packet.filePort = fileServer.port;
    packet.charSlots = static_cast<uint16_t>(account.charSlots);
    packet.charCount = static_cast<uint16_t>(account.characterUuids.size());
    packet.characters.reserve(packet.charCount);

    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    const std::string landingGame = IO::GetLandingGameUuid();
    for (const std::string& characterUuid : account.characterUuids)
    {
        AB::Entities::Character character;
        character.uuid = characterUuid;
        if (!Account::LoadCharacter(character))
            continue;

        // HACK: Reset party ID here :(
        // Otherwise people may find themselves in the same party days after.
        character.partyUuid = Utils::Uuid::EMPTY_UUID;
        client->Update(character);

        packet.characters.push_back({
            character.uuid,
            character.level,
            character.name,
            character.profession,
            character.profession2,
            static_cast<uint8_t>(character.sex),
            character.modelIndex,
            (Utils::Uuid::IsEmpty(character.lastOutpostUuid) ? landingGame : character.lastOutpostUuid)
        });
    }
    AB::Packets::Add(packet, *output);

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::SendOutposts(AB::Packets::Client::Login::GetOutposts request)
{
    AB::Entities::Account account;
    account.uuid = request.accountUuid;
    bool res = Account::TokenAuth(request.authToken, account);
    if (!res)
    {
        DisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::LoginProtocol::OutpostList);
    const std::vector<AB::Entities::Game> games = IO::GetGameList({
        AB::Entities::GameType::GameTypeOutpost,
        AB::Entities::GameType::GameTypeTown });

    AB::Packets::Server::Login::OutpostList packet;
    packet.count = static_cast<uint16_t>(games.size());
    packet.outposts.reserve(packet.count);
    for (const AB::Entities::Game& game : games)
    {
        packet.outposts.push_back({
            game.uuid,
            game.name,
            game.type,
            game.partySize,
            game.mapCoordX,
            game.mapCoordY
        });
    }
    AB::Packets::Add(packet, *output);

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::SendServers(AB::Packets::Client::Login::GetServers request)
{
    AB::Entities::Account account;
    account.uuid = request.accountUuid;
    bool res = Account::TokenAuth(request.authToken, account);
    if (!res)
    {
        DisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::LoginProtocol::ServerList);

    std::vector<AB::Entities::Service> services;
    AB::Packets::Server::Login::ServerList packet;
    packet.count = static_cast<uint16_t>(IO::GetServices(AB::Entities::ServiceTypeGameServer, services));
    packet.servers.reserve(packet.count);
    for (const AB::Entities::Service& service : services)
    {
        packet.servers.push_back({
            service.type,
            service.uuid,
            service.host,
            service.port,
            service.location,
            service.name
        });
    }
    AB::Packets::Add(packet, *output);

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::CreateAccount(AB::Packets::Client::Login::CreateAccount request)
{
    Account::CreateAccountResult res = Account::CreateAccount(
        request.accountName, request.password, request.email, request.accountKey);

    auto output = OutputMessagePool::GetOutputMessage();

    if (res == Account::CreateAccountResult::OK)
    {
        output->AddByte(AB::LoginProtocol::CreateAccountSuccess);
    }
    else
    {
        output->AddByte(AB::LoginProtocol::CreateAccountError);
        AB::Packets::Server::Login::Error packet{};
        switch (res)
        {
        case Account::CreateAccountResult::NameExists:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::AccountNameExists);
            break;
        case Account::CreateAccountResult::InvalidAccountKey:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidAccountKey);
            break;
        case Account::CreateAccountResult::PasswordError:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidPassword);
            break;
        case Account::CreateAccountResult::EmailError:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidEmail);
            break;
        default:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::UnknownError);
            break;
        }
        AB::Packets::Add(packet, *output);
    }

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::CreatePlayer(AB::Packets::Client::Login::CreatePlayer request)
{
    AB::Entities::Account account;
    account.uuid = request.accountUuid;
    bool ses = Account::TokenAuth(request.authToken, account);
    if (!ses)
    {
        DisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    std::string uuid;
    Account::CreateCharacterResult res = Account::CreateCharacter(
        account.uuid, request.charName, request.profUuid, request.itemIndex,
        static_cast<AB::Entities::CharacterSex>(request.sex), request.pvp, uuid
    );

    auto output = OutputMessagePool::GetOutputMessage();

    if (res == Account::CreateCharacterResult::OK)
    {
        output->AddByte(AB::LoginProtocol::CreatePlayerSuccess);
        AB::Packets::Server::Login::CreateCharacterSuccess packet = {
            uuid,
            IO::GetLandingGameUuid()
        };
        AB::Packets::Add(packet, *output);
    }
    else
    {
        output->AddByte(AB::LoginProtocol::CreatePlayerError);
        AB::Packets::Server::Login::Error packet{};
        switch (res)
        {
        case Account::CreateCharacterResult::NameExists:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::PlayerNameExists);
            break;
        case Account::CreateCharacterResult::InvalidAccount:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidAccount);
            break;
        case Account::CreateCharacterResult::NoMoreCharSlots:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::NoMoreCharSlots);
            break;
        case Account::CreateCharacterResult::InvalidProfession:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidProfession);
            break;
        case Account::CreateCharacterResult::InvalidName:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidCharacterName);
            break;
        default:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::UnknownError);
            break;
        }
        AB::Packets::Add(packet, *output);
    }

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::AddAccountKey(AB::Packets::Client::Login::AddAccountKey request)
{
    AB::Entities::Account account;
    account.uuid = request.accountUuid;
    bool authres = Account::TokenAuth(request.authToken, account);
    if (!authres)
    {
        DisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    Account::CreateAccountResult res = Account::AddAccountKey(account, request.accountKey);
    auto output = OutputMessagePool::GetOutputMessage();

    if (res == Account::CreateAccountResult::OK)
    {
        output->AddByte(AB::LoginProtocol::AddAccountKeySuccess);
    }
    else
    {
        output->AddByte(AB::LoginProtocol::AddAccountKeyError);
        AB::Packets::Server::Login::Error packet{};
        switch (res)
        {
        case Account::CreateAccountResult::NameExists:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::AccountNameExists);
            break;
        case Account::CreateAccountResult::InvalidAccountKey:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidAccountKey);
            break;
        case Account::CreateAccountResult::InvalidAccount:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::InvalidAccount);
            break;
        case Account::CreateAccountResult::AlreadyAdded:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::AccountKeyAlreadyAdded);
            break;
        default:
            packet.code = static_cast<uint8_t>(AB::ErrorCodes::UnknownError);
            break;
        }
        AB::Packets::Add(packet, *output);
    }

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::DeletePlayer(AB::Packets::Client::Login::DeleteCharacter request)
{
    AB::Entities::Account account;
    account.uuid = request.accountUuid;
    bool authRes = Account::TokenAuth(request.authToken, account);
    if (!authRes)
    {
        DisconnectClient(AB::ErrorCodes::TokenAuthFailure);
        return;
    }

    bool res = Account::DeleteCharacter(
        account.uuid, request.charUuid
    );

    auto output = OutputMessagePool::GetOutputMessage();

    if (res)
    {
        output->AddByte(AB::LoginProtocol::DeletePlayerSuccess);
        AB::Packets::Server::Login::CharacterDeleted packet = {
            request.charUuid
        };
        AB::Packets::Add(packet, *output);
    }
    else
    {
        output->AddByte(AB::LoginProtocol::DeletePlayerError);
        AB::Packets::Server::Login::Error packet =  {
            static_cast<uint8_t>(AB::ErrorCodes::InvalidCharacter)
        };
        AB::Packets::Add(packet, *output);
    }

    LOG_INFO << Utils::ConvertIPToString(GetIP()) << ": "
        << request.accountUuid << " deleted character with UUID " << request.charUuid << std::endl;

    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::DisconnectClient(AB::ErrorCodes error)
{
    auto output = OutputMessagePool::GetOutputMessage();
    output->AddByte(AB::LoginProtocol::LoginError);
    AB::Packets::Server::Login::Error packet = {
        static_cast<uint8_t>(error)
    };
    AB::Packets::Add(packet, *output);
    Send(std::move(output));
    Disconnect();
}

void ProtocolLogin::DelayedDisconnectClient(AB::ErrorCodes error)
{
    auto self = GetPtr();
    GetSubsystem<Asynch::Scheduler>()->Add(Asynch::CreateScheduledTask(2000, [self, error]()
    {
        self->DisconnectClient(error);
    }));
}

void ProtocolLogin::OnConnect()
{
}

}
