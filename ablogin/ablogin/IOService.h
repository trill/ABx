/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Service.h>
#include <libcommon/UuidUtils.h>
#include <vector>

namespace IO {

/// Get the service with the least load.
bool GetService(AB::Entities::ServiceType type,
    AB::Entities::Service& service,
    const std::string& preferredUuid = Utils::Uuid::EMPTY_UUID);
bool GetServiceOnMachine(AB::Entities::ServiceType type, const std::string& machine,
    AB::Entities::Service& service);
bool EnsureService(AB::Entities::ServiceType type,
    AB::Entities::Service& service,
    const std::string& preferredUuid = Utils::Uuid::EMPTY_UUID);
int GetServices(AB::Entities::ServiceType type,
    std::vector<AB::Entities::Service>& services);
bool SpawnService(AB::Entities::ServiceType type);

}
