/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOService.h"
#include <AB/Entities/ServiceList.h>
#include <libcommon/DataClient.h>
#include <libcommon/Logger.h>
#include <libcommon/MessageClient.h>
#include <libcommon/StringUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <sa/ConditionSleep.h>
#include <sa/Process.h>
#include <sa/time.h>
#include "Application.h"

namespace IO {

bool GetService(AB::Entities::ServiceType type,
    AB::Entities::Service& service,
    const std::string& preferredUuid /* = Utils::Uuid::EMPTY_UUID */)
{
    DataClient* dc = GetSubsystem<IO::DataClient>();

    AB::Entities::ServiceList sl;
    // Don't cache service list
    dc->Invalidate(sl);
    if (!dc->Read(sl))
    {
        LOG_ERROR << "Error reading service list" << std::endl;
        return false;
    }

    std::vector<AB::Entities::Service> services;

    for (const std::string& uuid : sl.uuids)
    {
        AB::Entities::Service s;
        s.uuid = uuid;
        if (!dc->Read(s))
            continue;
        if (s.status != AB::Entities::ServiceStatusOnline)
            continue;
        if (s.type == AB::Entities::ServiceTypeFileServer || s.type == AB::Entities::ServiceTypeGameServer)
        {
            // File and game server send a heart beat. Look if they are still alive.
            if (sa::time::time_elapsed(s.heartbeat) > AB::Entities::HEARTBEAT_INTERVAL * 2)
            {
                // Maybe dead
                LOG_INFO << "No heart beat from service " << s.uuid << " for " << sa::time::time_elapsed(s.heartbeat)
                         << " ms" << std::endl;
                continue;
            }
        }
        if (s.type == type)
        {
            // Use preferred server is possible
            if (Utils::Uuid::IsEqual(s.uuid, preferredUuid) == 0 && s.load < 90)
            {
                service = s;
                return true;
            }
            services.push_back(s);
        }
    }

    if (!services.empty())
    {
        std::sort(services.begin(),
            services.end(),
            [](AB::Entities::Service const& a, AB::Entities::Service const& b) { return a.load < b.load; });
        if (services[0].type == AB::Entities::ServiceTypeFileServer || services[0].load < 100)
        {
            service = services[0];
            return true;
        }
    }

    LOG_WARNING << "No server of type " << static_cast<int>(type) << " online" << std::endl;
    return false;
}

bool EnsureService(AB::Entities::ServiceType type, AB::Entities::Service& service, const std::string& preferredUuid)
{
    if (GetService(type, service, preferredUuid))
        return true;
    if (!SpawnService(type))
        return false;
    return GetService(type, service, preferredUuid);
}

int GetServices(AB::Entities::ServiceType type, std::vector<AB::Entities::Service>& services)
{
    DataClient* dc = GetSubsystem<IO::DataClient>();

    AB::Entities::ServiceList sl;
    // Don't cache service list
    dc->Invalidate(sl);
    if (!dc->Read(sl))
    {
        LOG_ERROR << "Error reading service list" << std::endl;
        return 0;
    }

    int result = 0;
    for (const std::string& uuid : sl.uuids)
    {
        AB::Entities::Service s;
        s.uuid = uuid;
        if (!dc->Read(s))
        {
            LOG_ERROR << "Error service with UUID " << uuid << std::endl;
            continue;
        }
        if (s.status != AB::Entities::ServiceStatusOnline)
            continue;
        if (s.type == AB::Entities::ServiceTypeFileServer || s.type == AB::Entities::ServiceTypeGameServer)
        {
            // File and game server send a heart beat. Look if they are still alive.
            if (sa::time::time_elapsed(s.heartbeat) > AB::Entities::HEARTBEAT_INTERVAL * 2)
            {
                // Maybe dead
                LOG_INFO << "No heart beat from service " << s.uuid << " for " << sa::time::time_elapsed(s.heartbeat)
                         << " ms" << std::endl;
                s.status = AB::Entities::ServiceStatusOffline;
                dc->Update(s);
                continue;
            }
        }

        if (s.type == type)
        {
            services.push_back(s);
            ++result;
        }
    }
    return result;
}

bool GetServiceOnMachine(AB::Entities::ServiceType type, const std::string& machine, AB::Entities::Service& service)
{
    DataClient* dc = GetSubsystem<IO::DataClient>();

    AB::Entities::ServiceList sl;
    // Don't cache service list
    dc->Invalidate(sl);
    if (!dc->Read(sl))
    {
        LOG_ERROR << "Error reading service list" << std::endl;
        return false;
    }

    std::vector<AB::Entities::Service> services;

    for (const std::string& uuid : sl.uuids)
    {
        service.uuid = uuid;
        if (!dc->Read(service))
            continue;
        if (service.type == type && service.machine == machine)
            return true;
    }
    LOG_WARNING << "No server of type " << static_cast<int>(type) << " on machine " << machine << std::endl;
    return false;
}

bool SpawnService(AB::Entities::ServiceType type)
{
    auto* msgClient = GetSubsystem<Net::MessageClient>();
    if (!msgClient)
        return false;

    std::vector<AB::Entities::Service> services;
    if (GetServices(type, services) == 0)
    {
        // If there is no service running, start one on this machine.
        const std::string& thisMachine = Application::GetMachineName();
        AB::Entities::Service service;
        if (!GetServiceOnMachine(type, thisMachine, service))
            return false;
        std::stringstream ss;
        ss << Utils::EscapeArguments(service.file);
        ss << " " << service.arguments;
        const std::string cmdLine = ss.str();

        if (!sa::Process::Run(cmdLine))
        {
            LOG_ERROR << "Error spawning process with command line: " << cmdLine << std::endl;
            return false;
        }
    }
    else
    {
        // If there is already a service of this type running, just tell it to spawn a new one.
        Net::MessageMsg msg;
        msg.type_ = Net::MessageType::Spawn;
        msg.SetBodyString(services[0].uuid);
        if (!msgClient->Write(msg))
            return false;
    }

    auto isNew = [&](const std::vector<AB::Entities::Service>& _services)
    {
        for (const auto& s : _services)
        {
            const auto it = std::find_if(services.begin(),
                services.end(),
                [&s](const AB::Entities::Service& current) { return s.uuid == current.uuid; });
            if (it == services.end())
                return true;
        }
        return false;
    };

    // Wait for it
    return sa::ConditionSleep(
        [&]()
        {
            std::vector<AB::Entities::Service> services2;
            if (GetServices(type, services2) > 0)
            {
                // If there is a service previously not in the services list,
                // assume we spawned it and return success.
                return isNew(services2);
            }
            return false;
        },
        2000);
}

}
