/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Game.h>
#include <set>
#include <vector>

namespace IO {

std::string GetLandingGameUuid();
AB::Entities::GameType GetGameType(const std::string& mapName);
/// Get a list of games of a certain type. If types is empty it returns all games.
std::vector<AB::Entities::Game> GetGameList(const std::set<AB::Entities::GameType>& types);

}
