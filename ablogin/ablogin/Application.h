/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/ServerApp.h>
#include <libcommon/Service.h>
#include <libcommon/MessageClient.h>

class Application final : public ServerApp
{
private:
    std::shared_ptr<asio::io_service> ioService_;
    std::unique_ptr<Net::ServiceManager> serviceManager_;
    bool enablePingServer_{ true };
    bool LoadMain();
    void PrintServerInfo();
    void HeartBeatTask();
    void ShowLogo();
    void HandleMessage(const Net::MessageMsg& msg);
protected:
    void ShowVersion() override;
public:
    Application();
    ~Application() override;

    bool Initialize(const std::vector<std::string>& args) override;
    void Run() override;
    void Stop() override;
    std::string GetKeysFile() const;
};

