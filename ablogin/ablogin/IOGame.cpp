/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "IOGame.h"
#include "Application.h"
#include <AB/Entities/GameList.h>
#include <libcommon/DataClient.h>
#include <libcommon/Logger.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>

namespace IO {

std::string GetLandingGameUuid()
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::GameList gl;
    if (!client->Read(gl))
    {
        LOG_ERROR << "Error reading game list" << std::endl;
        return Utils::Uuid::EMPTY_UUID;
    }
    if (gl.gameUuids.empty())
    {
        LOG_ERROR << "Game list is empty" << std::endl;
        return Utils::Uuid::EMPTY_UUID;
    }

    for (const std::string& uuid : gl.gameUuids)
    {
        AB::Entities::Game g;
        g.uuid = uuid;
        if (client->Read(g) && g.landing)
            return g.uuid;
    }
    LOG_ERROR << "No landing game found" << std::endl;
    return Utils::Uuid::EMPTY_UUID;
}

AB::Entities::GameType GetGameType(const std::string& mapName)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Game g;
    g.name = mapName;
    if (!client->Read(g))
    {
        LOG_ERROR << "Error reading game " << mapName << std::endl;
        return AB::Entities::GameTypeUnknown;
    }
    return g.type;
}

std::vector<AB::Entities::Game> GetGameList(const std::set<AB::Entities::GameType>& types)
{
    std::vector<AB::Entities::Game> result;

    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::GameList gl;
    if (!client->Read(gl))
    {
        LOG_ERROR << "Error reading game list" << std::endl;
        return result;
    }

    for (const std::string& uuid : gl.gameUuids)
    {
        AB::Entities::Game g;
        g.uuid = uuid;
        if (!client->Read(g))
            continue;
        if (types.empty() || types.find(g.type) != types.end())
            result.push_back(g);
    }

    if (result.empty())
        LOG_WARNING << "No Games found!" << std::endl;
    return result;
}

}
