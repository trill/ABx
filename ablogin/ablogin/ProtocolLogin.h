/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Character.h>
#include <AB/Packets/LoginPackets.h>
#include <AB/ProtocolCodes.h>
#include <libcommon/Protocol.h>

namespace Net {

class ProtocolLogin final : public Protocol
{
public:
    // static protocol information
    static constexpr bool ServerSendsFirst = false;
    static constexpr uint8_t ProtocolIdentifier = AB::ProtocolLoginId;
    static constexpr bool UseChecksum = true;
    static const char* ProtocolName() { return "Login Protocol"; }
public:
    explicit ProtocolLogin(std::shared_ptr<Connection> connection) :
        Protocol(std::forward<std::shared_ptr<Connection>>(connection))
    {
        SetEncKey(AB::ENC_KEY);
    }

    void OnRecvFirstMessage(NetworkMessage& message) override;
private:
    int loginAttempts_{ 0 };
    std::shared_ptr<ProtocolLogin> GetPtr()
    {
        return std::static_pointer_cast<ProtocolLogin>(shared_from_this());
    }

    void DisconnectClient(AB::ErrorCodes error);
    void DelayedDisconnectClient(AB::ErrorCodes error);
    void OnConnect() override;

    void HandleLoginPacket(NetworkMessage& message);
    void HandleCreateAccountPacket(NetworkMessage& message);
    void HandleCreateCharacterPacket(NetworkMessage& message);
    void HandleDeleteCharacterPacket(NetworkMessage& message);
    void HandleAddAccountKeyPacket(NetworkMessage& message);
    void HandleGetOutpostsPacket(NetworkMessage& message);
    void HandleGetServersPacket(NetworkMessage& message);

    // These are passed by value because they are executed by the dispatcher later
    void AuthenticateSendCharacterList(AB::Packets::Client::Login::Login request);
    void SendOutposts(AB::Packets::Client::Login::GetOutposts request);
    void SendServers(AB::Packets::Client::Login::GetServers request);
    void CreateAccount(AB::Packets::Client::Login::CreateAccount request);
    void AddAccountKey(AB::Packets::Client::Login::AddAccountKey request);
    void CreatePlayer(AB::Packets::Client::Login::CreatePlayer request);
    void DeletePlayer(AB::Packets::Client::Login::DeleteCharacter request);
};

}
