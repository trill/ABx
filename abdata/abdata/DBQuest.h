/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Quest.h>

namespace DB {

class DBQuest
{
public:
    DBQuest() = delete;
    ~DBQuest() = delete;

    static bool Create(AB::Entities::Quest&);
    static bool Load(AB::Entities::Quest&);
    static bool Save(const AB::Entities::Quest&);
    static bool Delete(const AB::Entities::Quest&);
    static bool Exists(const AB::Entities::Quest&);
};

}
