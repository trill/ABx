/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/AccountBan.h>

namespace DB {

class DBAccountBan
{
public:
    DBAccountBan() = delete;
    ~DBAccountBan() = delete;

    static bool Create(AB::Entities::AccountBan& ban);
    static bool Load(AB::Entities::AccountBan& ban);
    static bool Save(const AB::Entities::AccountBan& ban);
    static bool Delete(const AB::Entities::AccountBan& ban);
    static bool Exists(const AB::Entities::AccountBan& ban);
};

}
