/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBVersionList.h"

namespace DB {

bool DBVersionList::Create(AB::Entities::VersionList&)
{
    return true;
}

bool DBVersionList::Load(AB::Entities::VersionList& vl)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT * FROM versions WHERE internal = 0";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        vl.versions.push_back({
            result->GetString("uuid"),
            result->GetString("name"),
            result->GetUInt("value"),
            false
        });
    }
    return true;
}

bool DBVersionList::Save(const AB::Entities::VersionList&)
{
    return true;
}

bool DBVersionList::Delete(const AB::Entities::VersionList&)
{
    return true;
}

bool DBVersionList::Exists(const AB::Entities::VersionList&)
{
    return true;
}

}
