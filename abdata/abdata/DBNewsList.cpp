/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBNewsList.h"

namespace DB {

template <size_t _Limit>
static bool LoadNews(AB::Entities::NewsList<_Limit>& pl)
{
    Database* db = GetSubsystem<Database>();

    std::string query;
    if constexpr (_Limit == 0)
        query = "SELECT uuid FROM news ORDER BY created DESC";
    else
        query = "SELECT uuid FROM news ORDER BY created DESC LIMIT " + std::to_string(_Limit);
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        pl.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBNewsList::Create(AB::Entities::LatestNewsList&)
{
    return true;
}

bool DBNewsList::Load(AB::Entities::LatestNewsList& pl)
{
    return LoadNews<AB::Entities::LatestNewsList::Limit>(pl);
}

bool DBNewsList::Save(const AB::Entities::LatestNewsList&)
{
    return true;
}

bool DBNewsList::Delete(const AB::Entities::LatestNewsList&)
{
    return true;
}

bool DBNewsList::Exists(const AB::Entities::LatestNewsList&)
{
    return true;
}

bool DBNewsList::Create(AB::Entities::AllNewsList&)
{
    return true;
}

bool DBNewsList::Load(AB::Entities::AllNewsList& pl)
{
    return LoadNews<AB::Entities::AllNewsList::Limit>(pl);
}

bool DBNewsList::Save(const AB::Entities::AllNewsList&)
{
    return true;
}

bool DBNewsList::Delete(const AB::Entities::AllNewsList&)
{
    return true;
}

bool DBNewsList::Exists(const AB::Entities::AllNewsList&)
{
    return true;
}

}
