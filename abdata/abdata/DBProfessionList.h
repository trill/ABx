/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/ProfessionList.h>

namespace DB {

class DBProfessionList
{
public:
    DBProfessionList() = delete;
    ~DBProfessionList() = delete;

    static bool Create(AB::Entities::ProfessionList&);
    static bool Load(AB::Entities::ProfessionList& pl);
    static bool Save(const AB::Entities::ProfessionList&);
    static bool Delete(const AB::Entities::ProfessionList&);
    static bool Exists(const AB::Entities::ProfessionList&);
};

}
