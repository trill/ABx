/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBGameInstanceList.h"

namespace DB {

bool DBGameInstanceList::Create(AB::Entities::GameInstanceList&)
{
    return true;
}

bool DBGameInstanceList::Load(AB::Entities::GameInstanceList& game)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM instances WHERE is_running = 1 ORDER BY players DESC, start_time DESC";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        game.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBGameInstanceList::Save(const AB::Entities::GameInstanceList&)
{
    return true;
}

bool DBGameInstanceList::Delete(const AB::Entities::GameInstanceList&)
{
    return true;
}

bool DBGameInstanceList::Exists(const AB::Entities::GameInstanceList&)
{
    return true;
}

}
