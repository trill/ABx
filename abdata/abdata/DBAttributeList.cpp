/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBAttributeList.h"

namespace DB {

bool DBAttributeList::Create(AB::Entities::AttributeList&)
{
    return true;
}

bool DBAttributeList::Load(AB::Entities::AttributeList& al)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_attributes";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        al.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBAttributeList::Save(const AB::Entities::AttributeList&)
{
    return true;
}

bool DBAttributeList::Delete(const AB::Entities::AttributeList&)
{
    return true;
}

bool DBAttributeList::Exists(const AB::Entities::AttributeList&)
{
    return true;
}

}
