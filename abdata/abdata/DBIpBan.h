/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/IpBan.h>

namespace DB {

class DBIpBan
{
public:
    DBIpBan() = delete;
    ~DBIpBan() = delete;

    static bool Create(AB::Entities::IpBan& ban);
    static bool Load(AB::Entities::IpBan& ban);
    static bool Save(const AB::Entities::IpBan& ban);
    static bool Delete(const AB::Entities::IpBan& ban);
    static bool Exists(const AB::Entities::IpBan& ban);
};

}
