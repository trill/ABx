/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/GameInstanceList.h>

namespace DB {

class DBGameInstanceList
{
public:
    DBGameInstanceList() = delete;
    ~DBGameInstanceList() = delete;

    static bool Create(AB::Entities::GameInstanceList&);
    static bool Load(AB::Entities::GameInstanceList& game);
    static bool Save(const AB::Entities::GameInstanceList&);
    static bool Delete(const AB::Entities::GameInstanceList&);
    static bool Exists(const AB::Entities::GameInstanceList&);
};

}
