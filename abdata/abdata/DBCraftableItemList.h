/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/CraftableItemList.h>

namespace DB {

class DBCraftableItemList
{
public:
    DBCraftableItemList() = delete;
    ~DBCraftableItemList() = delete;

    static bool Create(AB::Entities::CraftableItemList& il);
    static bool Load(AB::Entities::CraftableItemList& il);
    static bool Save(const AB::Entities::CraftableItemList& il);
    static bool Delete(const AB::Entities::CraftableItemList& il);
    static bool Exists(const AB::Entities::CraftableItemList& il);
};

}
