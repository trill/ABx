/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/AccountBanList.h>

namespace DB {

class DBAccountBanList
{
public:
    DBAccountBanList() = delete;
    ~DBAccountBanList() = delete;

    static bool Create(AB::Entities::AccountBanList& li);
    static bool Load(AB::Entities::AccountBanList& il);
    static bool Save(const AB::Entities::AccountBanList& il);
    static bool Delete(const AB::Entities::AccountBanList& il);
    static bool Exists(const AB::Entities::AccountBanList& il);
};

}
