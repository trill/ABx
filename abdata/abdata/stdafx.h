/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#include "targetver.h"
#include <sa/Compiler.h>

PRAGMA_WARNING_DISABLE_MSVC(4307)

#include <stdio.h>

#include <iostream>
#include <string>

#include <AB/CommonConfig.h>
#include <libcommon/DebugConfig.h>

#if !defined(ASIO_STANDALONE)
#define ASIO_STANDALONE
#endif

PRAGMA_WARNING_PUSH
    PRAGMA_WARNING_DISABLE_MSVC(4592)
#   include <asio.hpp>
PRAGMA_WARNING_POP

#include <uuid.h>
#include <base64.h>
#define BRIGAND_NO_BOOST_SUPPORT
#include <brigand/brigand.hpp>
#include <multi_index/hashed_index.hpp>
#include <multi_index/member.hpp>
#include <multi_index/ordered_index.hpp>
#include <multi_index_container.hpp>

#define PROFILING

#include <libcommon/Logger.h>
#include <libcommon/UuidUtils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/Utils.h>
#include <libcommon/StringUtils.h>
#include <libdb/Database.h>

#define MAX_DATA_SIZE (1024 * 1024)
#define MAX_KEY_SIZE 256

#if !defined(HAVE_MYSQL) && !defined(HAVE_PGSQL) && !defined(HAVE_ODBC) && !defined(HAVE_SQLITE)
#error "Define at least one database driver"
#endif
