/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBFriendedMe.h"
#include <sa/TemplateParser.h>

namespace DB {

bool DBFriendedMe::Create(AB::Entities::FriendedMe& fl)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(fl.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBFriendedMe::Load(AB::Entities::FriendedMe& fl)
{
    if (Utils::Uuid::IsEmpty(fl.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT * FROM friend_list WHERE friend_uuid = ${friend_uuid}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, [&db, &fl](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "friend_uuid")
                return db->EscapeString(fl.uuid);
            LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    fl.friends.clear();
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        fl.friends.push_back({
            result->GetString("account_uuid"),
            static_cast<AB::Entities::FriendRelation>(result->GetUInt("relation"))
        });
    }
    return true;
}

bool DBFriendedMe::Save(const AB::Entities::FriendedMe& fl)
{
    if (Utils::Uuid::IsEmpty(fl.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBFriendedMe::Delete(const AB::Entities::FriendedMe& fl)
{
    if (Utils::Uuid::IsEmpty(fl.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBFriendedMe::Exists(const AB::Entities::FriendedMe& fl)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(fl.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

}
