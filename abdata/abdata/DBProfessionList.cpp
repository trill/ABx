/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBProfessionList.h"

namespace DB {

bool DBProfessionList::Create(AB::Entities::ProfessionList&)
{
    return true;
}

bool DBProfessionList::Load(AB::Entities::ProfessionList& pl)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_professions";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        pl.profUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBProfessionList::Save(const AB::Entities::ProfessionList&)
{
    return true;
}

bool DBProfessionList::Delete(const AB::Entities::ProfessionList&)
{
    return true;
}

bool DBProfessionList::Exists(const AB::Entities::ProfessionList&)
{
    return true;
}

}
