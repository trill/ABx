/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/SkillList.h>

namespace DB {

class DBSkillList
{
public:
    DBSkillList() = delete;
    ~DBSkillList() = delete;

    static bool Create(AB::Entities::SkillList&);
    static bool Load(AB::Entities::SkillList& sl);
    static bool Save(const AB::Entities::SkillList&);
    static bool Delete(const AB::Entities::SkillList&);
    static bool Exists(const AB::Entities::SkillList&);
};

}
