/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBAccountItemList.h"
#include <sa/TemplateParser.h>

namespace DB {

template<AB::Entities::StoragePlace _Place>
static bool LoadList(AB::Entities::_AccountItemList<_Place>& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT uuid FROM concrete_items WHERE account_uuid = ${player_uuid} AND deleted = 0");
    if (il.storagePlace != AB::Entities::StoragePlace::None)
        parser.Append(" AND storage_place = ${storage_place}", tokens);
    const std::string query = tokens.ToString([&db, &il](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "player_uuid")
                return db->EscapeString(il.uuid);
            if (token.value == "storage_place")
                return std::to_string(static_cast<int>(il.storagePlace));
            LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        il.itemUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBAccountItemList::Create(AB::Entities::AccountItemList& li)
{
    if (Utils::Uuid::IsEmpty(li.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Load(AB::Entities::AccountItemList& il)
{
    return LoadList<AB::Entities::AccountItemList::Place>(il);
}

bool DBAccountItemList::Save(const AB::Entities::AccountItemList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Delete(const AB::Entities::AccountItemList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Exists(const AB::Entities::AccountItemList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Create(AB::Entities::ChestItems& li)
{
    if (Utils::Uuid::IsEmpty(li.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Load(AB::Entities::ChestItems& il)
{
    return LoadList<AB::Entities::ChestItems::Place>(il);
}

bool DBAccountItemList::Save(const AB::Entities::ChestItems& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Delete(const AB::Entities::ChestItems& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBAccountItemList::Exists(const AB::Entities::ChestItems& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

}
