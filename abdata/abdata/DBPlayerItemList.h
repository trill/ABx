/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/PlayerItemList.h>

namespace DB {

class DBPlayerItemList
{
public:
    DBPlayerItemList() = delete;
    ~DBPlayerItemList() = delete;

    static bool Create(AB::Entities::PlayerItemList& il);
    static bool Load(AB::Entities::PlayerItemList& il);
    static bool Save(const AB::Entities::PlayerItemList& il);
    static bool Delete(const AB::Entities::PlayerItemList& il);
    static bool Exists(const AB::Entities::PlayerItemList& il);

    static bool Create(AB::Entities::EquippedItems& il);
    static bool Load(AB::Entities::EquippedItems& il);
    static bool Save(const AB::Entities::EquippedItems& il);
    static bool Delete(const AB::Entities::EquippedItems& il);
    static bool Exists(const AB::Entities::EquippedItems& il);

    static bool Create(AB::Entities::InventoryItems& il);
    static bool Load(AB::Entities::InventoryItems& il);
    static bool Save(const AB::Entities::InventoryItems& il);
    static bool Delete(const AB::Entities::InventoryItems& il);
    static bool Exists(const AB::Entities::InventoryItems& il);
};

}
