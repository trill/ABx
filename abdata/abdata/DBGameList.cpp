/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBGameList.h"

namespace DB {

bool DBGameList::Create(AB::Entities::GameList&)
{
    return true;
}

bool DBGameList::Load(AB::Entities::GameList& game)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_maps";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        game.gameUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBGameList::Save(const AB::Entities::GameList&)
{
    return true;
}

bool DBGameList::Delete(const AB::Entities::GameList&)
{
    return true;
}

bool DBGameList::Exists(const AB::Entities::GameList&)
{
    return true;
}

}
