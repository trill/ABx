/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/News.h>

namespace DB {

class DBNews
{
public:
    DBNews() = delete;
    ~DBNews() = delete;

    static bool Create(AB::Entities::News&);
    static bool Load(AB::Entities::News& v);
    static bool Save(const AB::Entities::News&);
    static bool Delete(const AB::Entities::News&);
    static bool Exists(const AB::Entities::News& v);
};

}
