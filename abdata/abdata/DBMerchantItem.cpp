/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBMerchantItem.h"
#include <AB/Entities/ConcreteItem.h>
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::MerchantItem& item, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "item_uuid")
            return db->EscapeString(item.uuid);
        if (token.value == "storage_place")
            return std::to_string(static_cast<int>(AB::Entities::StoragePlace::Merchant));

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBMerchantItem::Create(AB::Entities::MerchantItem&)
{
    return true;
}

bool DBMerchantItem::Load(AB::Entities::MerchantItem& item)
{
    if (Utils::Uuid::IsEmpty(item.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT uuid FROM concrete_items WHERE "
        "deleted = 0 AND item_uuid = ${item_uuid} "
        "AND storage_place = ${storage_place}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, item, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    item.concreteUuid = result->GetString("uuid");
    return true;
}

bool DBMerchantItem::Save(const AB::Entities::MerchantItem&)
{
    return true;
}

bool DBMerchantItem::Delete(const AB::Entities::MerchantItem&)
{
    return true;
}

bool DBMerchantItem::Exists(const AB::Entities::MerchantItem& item)
{
    if (Utils::Uuid::IsEmpty(item.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT COUNT(*) AS count concrete_items WHERE "
        "deleted = 0 AND item_uuid = ${item_uuid} AND storage_place = ${storage_place}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, item, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    return result->GetUInt("count") != 0;
}

}
