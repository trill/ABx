/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "CacheIndex.h"

IO::DataKey CacheIndex::Next()
{
    auto& rankIndex = dataItems_.get<1>();
    auto first = rankIndex.begin();
    IO::DataKey retVal = first->key;
    dataItems_.erase(retVal);
    return retVal;
}

void CacheIndex::Add(const IO::DataKey& key)
{
    dataItems_.insert({ key, GetNextRank() });
}

void CacheIndex::Refresh(const IO::DataKey& key)
{
    uint64_t next = GetNextRank();
    auto keyItr = dataItems_.find(key);
    dataItems_.modify(keyItr, [next](DataItem& d)
    {
        d.rank = next;
    });
}

void CacheIndex::Delete(const IO::DataKey& key)
{
    auto keyItr = dataItems_.find(key);
    if (keyItr != dataItems_.end())
    {
        dataItems_.erase(keyItr);
    }
}

void CacheIndex::Clear()
{
    dataItems_.clear();
    currentRank_ = 0;
}
