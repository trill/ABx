/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBSkill.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::Skill& skill, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "uuid")
            return db->EscapeString(skill.uuid);
        if (token.value == "idx")
            return std::to_string(skill.index);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBSkill::Create(AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Load(AB::Entities::Skill& skill)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT * FROM game_skills WHERE ");
    if (!Utils::Uuid::IsEmpty(skill.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else
        // 0 is a valid index, it is the None skill
        parser.Append("idx = ${idx}", tokens);
    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, skill, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    skill.uuid = result->GetString("uuid");
    skill.index = result->GetUInt("idx");
    skill.script = result->GetString("script");
    skill.access = result->GetUInt("access");

    return true;
}

bool DBSkill::Save(const AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Delete(const AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Exists(const AB::Entities::Skill& skill)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT COUNT(*) AS count FROM game_skills WHERE ");
    if (!Utils::Uuid::IsEmpty(skill.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else
        // 0 is a valid index, it is the None skill
        parser.Append("idx = ${idx}", tokens);
    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, skill, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    return result->GetUInt("count") != 0;
}

}
