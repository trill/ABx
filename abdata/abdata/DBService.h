/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Service.h>

namespace DB {

class DBService
{
public:
    DBService() = delete;
    ~DBService() = delete;

    static bool Create(AB::Entities::Service& s);
    static bool Load(AB::Entities::Service& s);
    static bool Save(const AB::Entities::Service& s);
    static bool Delete(const AB::Entities::Service& s);
    static bool Exists(const AB::Entities::Service& s);

    static bool StopAll();
};

}
