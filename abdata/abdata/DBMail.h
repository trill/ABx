/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Mail.h>

namespace DB {

class DBMail
{
private:
    static uint32_t GetMailCount(AB::Entities::Mail& mail);
public:
    DBMail() = delete;
    ~DBMail() = delete;

    static bool Create(AB::Entities::Mail& mail);
    static bool Load(AB::Entities::Mail& mail);
    static bool Save(const AB::Entities::Mail& mail);
    static bool Delete(const AB::Entities::Mail& mail);
    static bool Exists(const AB::Entities::Mail& mail);
};

}
