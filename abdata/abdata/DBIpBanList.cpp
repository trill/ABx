/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBIpBanList.h"

namespace DB {

bool DBIpBanList::Create(AB::Entities::IpBanList&)
{
    return true;
}

bool DBIpBanList::Load(AB::Entities::IpBanList& il)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM ip_bans";

    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        il.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBIpBanList::Save(const AB::Entities::IpBanList&)
{
    return true;
}

bool DBIpBanList::Delete(const AB::Entities::IpBanList&)
{
    return true;
}

bool DBIpBanList::Exists(const AB::Entities::IpBanList&)
{
    return true;
}

}
