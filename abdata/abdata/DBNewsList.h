/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/NewsList.h>

namespace DB {

class DBNewsList
{
public:
    DBNewsList() = delete;
    ~DBNewsList() = delete;

    static bool Create(AB::Entities::LatestNewsList&);
    static bool Load(AB::Entities::LatestNewsList& pl);
    static bool Save(const AB::Entities::LatestNewsList&);
    static bool Delete(const AB::Entities::LatestNewsList&);
    static bool Exists(const AB::Entities::LatestNewsList&);

    static bool Create(AB::Entities::AllNewsList&);
    static bool Load(AB::Entities::AllNewsList& pl);
    static bool Save(const AB::Entities::AllNewsList&);
    static bool Delete(const AB::Entities::AllNewsList&);
    static bool Exists(const AB::Entities::AllNewsList&);
};

}
