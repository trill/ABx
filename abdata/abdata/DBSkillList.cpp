/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBSkillList.h"

namespace DB {

bool DBSkillList::Create(AB::Entities::SkillList&)
{
    return true;
}

bool DBSkillList::Load(AB::Entities::SkillList& sl)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_skills";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        sl.skillUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBSkillList::Save(const AB::Entities::SkillList&)
{
    return true;
}

bool DBSkillList::Delete(const AB::Entities::SkillList&)
{
    return true;
}

bool DBSkillList::Exists(const AB::Entities::SkillList&)
{
    return true;
}

}
