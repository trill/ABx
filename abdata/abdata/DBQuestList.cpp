/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBQuestList.h"

namespace DB {

bool DBQuestList::Create(AB::Entities::QuestList&)
{
    return true;
}

bool DBQuestList::Load(AB::Entities::QuestList& q)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_quests";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        q.questUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBQuestList::Save(const AB::Entities::QuestList&)
{
    return true;
}

bool DBQuestList::Delete(const AB::Entities::QuestList&)
{
    return true;
}

bool DBQuestList::Exists(const AB::Entities::QuestList&)
{
    return true;
}

}
