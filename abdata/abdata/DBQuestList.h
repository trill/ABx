/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/QuestList.h>

namespace DB {

class DBQuestList
{
public:
    DBQuestList() = delete;
    ~DBQuestList() = delete;

    static bool Create(AB::Entities::QuestList&);
    static bool Load(AB::Entities::QuestList&);
    static bool Save(const AB::Entities::QuestList&);
    static bool Delete(const AB::Entities::QuestList&);
    static bool Exists(const AB::Entities::QuestList&);
};

}
