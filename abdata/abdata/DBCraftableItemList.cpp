/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBCraftableItemList.h"
#include <AB/Entities/Item.h>
#include <sa/Assert.h>
#include <sa/TemplateParser.h>

namespace DB {

bool DBCraftableItemList::Create(AB::Entities::CraftableItemList&)
{
    return true;
}

bool DBCraftableItemList::Load(AB::Entities::CraftableItemList& il)
{
    Database* db = GetSubsystem<Database>();
    static std::string statement;
    if (statement.empty())
    {
        static constexpr const char* SQL = "SELECT uuid, idx, type, item_flags, name, value "
            "FROM game_items WHERE item_flags & ${item_flags} = ${item_flags} ORDER BY type DESC, name ASC";
        statement = sa::templ::Parser::Evaluate(SQL, [](const sa::templ::Token& token) -> std::string
        {
            switch (token.type)
            {
            case sa::templ::Token::Type::Variable:
                if (token.value == "item_flags")
                    return std::to_string(static_cast<int>(AB::Entities::ItemFlagCraftable));
                LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
                return "";
            default:
                return token.value;
            }
        });
    }
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(statement); result; result = result->Next())
    {
        il.items.push_back({ result->GetUInt("idx"),
            static_cast<AB::Entities::ItemType>(result->GetUInt("type")),
            result->GetString("name"),
            result->GetString("uuid"),
            static_cast<uint16_t>(result->GetUInt("value")),
            result->GetUInt("item_flags") });
    }
    return true;
}

bool DBCraftableItemList::Save(const AB::Entities::CraftableItemList&)
{
    return true;
}

bool DBCraftableItemList::Delete(const AB::Entities::CraftableItemList&)
{
    return true;
}

bool DBCraftableItemList::Exists(const AB::Entities::CraftableItemList&)
{
    return true;
}

}
