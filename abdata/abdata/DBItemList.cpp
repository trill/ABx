/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBItemList.h"

namespace DB {

bool DBItemList::Create(AB::Entities::ItemList&)
{
    return true;
}

bool DBItemList::Load(AB::Entities::ItemList& il)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_items";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        il.itemUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBItemList::Save(const AB::Entities::ItemList&)
{
    return true;
}

bool DBItemList::Delete(const AB::Entities::ItemList&)
{
    return true;
}

bool DBItemList::Exists(const AB::Entities::ItemList&)
{
    return true;
}

}
