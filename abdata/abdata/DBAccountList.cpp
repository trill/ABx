/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBAccountList.h"

namespace DB {

bool DBAccountList::Create(AB::Entities::AccountList&)
{
    return true;
}

bool DBAccountList::Load(AB::Entities::AccountList& al)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM accounts";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        al.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBAccountList::Save(const AB::Entities::AccountList&)
{
    return true;
}

bool DBAccountList::Delete(const AB::Entities::AccountList&)
{
    return true;
}

bool DBAccountList::Exists(const AB::Entities::AccountList&)
{
    return true;
}

}
