/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ConnectionManager.h"

void ConnectionManager::Start(ea::shared_ptr<Connection> c)
{
    connections_.insert(c);
    c->Start();
}

void ConnectionManager::Stop(ea::shared_ptr<Connection> c)
{
    connections_.erase(c);
    c->Stop();
}

void ConnectionManager::StopAll()
{
    ea::for_each(connections_.begin(), connections_.end(),
        std::bind(&Connection::Stop, std::placeholders::_1));
    connections_.clear();
}
