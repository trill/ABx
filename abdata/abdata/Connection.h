/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdint.h>
#include <vector>
#include "StorageProvider.h"
#include <libcommon/Dispatcher.h>
#include <libcommon/DataKey.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataCodes.h>
#include <asio.hpp>
#include <eastl.hpp>
#include <mutex>
#include <sa/IdGenerator.h>

class ConnectionManager;

class Connection : public ea::enable_shared_from_this<Connection>
{
public:
    Connection(asio::io_service& io_service, ConnectionManager& manager,
        StorageProvider& storage, size_t maxData, uint16_t maxKeySize_);
    ~Connection();
    asio::ip::tcp::socket& GetSocket();
    void Start();
    void Stop();
    uint32_t GetId() const { return id_; }
private:
    static sa::IdGenerator<uint32_t> idGenerator;
    template <typename Callable, typename... Args>
    void AddTask(Callable&& function, Args&&... args)
    {
        GetSubsystem<Asynch::Dispatcher>()->Add(
            Asynch::CreateTask(std::bind(std::move(function), shared_from_this(), std::forward<Args>(args)...))
        );
    }

    void StartLockOperation();
    void StartUnlockOperation();
    void StartCreateOperation();
    void StartUpdateDataOperation();
    void StartReadOperation();
    void StartDeleteOperation();
    void StartInvalidateOperation();
    void StartPreloadOperation();
    void StartExistsOperation();
    void StartClearOperation();

    void CreateDataTask();
    void ReadDataTask();
    void ExistsDataTask();
    void UpdateDataTask();

    void HandleUpdateReadRawData(const asio::error_code& error, size_t bytes_transferred, size_t expected);
    void HandleCreateReadRawData(const asio::error_code& error, size_t bytes_transferred, size_t expected);
    void HandleReadReadRawData(const asio::error_code& error, size_t bytes_transferred, size_t expected);
    void HandleExistsReadRawData(const asio::error_code& error, size_t bytes_transferred, size_t expected);
    void HandleWriteReqResponse(const asio::error_code& error);

    void StartClientRequestedOp();
    void StartReadKey(uint16_t keySize);
    void SendResponseAndStart(std::vector<asio::mutable_buffer>& resp, size_t size);
    void SendStatusAndRestart(IO::ErrorCodes code, const std::string& message);

    static inline uint32_t ToInt32(const DataBuff& intBytes, size_t start)
    {
        return (intBytes[start + 3] << 24) | (intBytes[start + 2] << 16) | (intBytes[start + 1] << 8) | intBytes[start];
    }
    static inline uint16_t ToInt16(const DataBuff& intBytes, size_t start)
    {
        return  (intBytes[start + 1] << 8) | intBytes[start];
    }

    uint32_t id_;
    bool started_{ false };
    size_t maxDataSize_;
    uint16_t maxKeySize_;
    asio::ip::tcp::socket socket_;
    ConnectionManager& connectionManager_;
    StorageProvider& storageProvider_;
    IO::OpCodes opcode_{ IO::OpCodes::None };
    std::mutex lock_;

    IO::DataKey key_;
    ea::shared_ptr<DataBuff> data_;
};
