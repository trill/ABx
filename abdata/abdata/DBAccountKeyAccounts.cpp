/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBAccountKeyAccounts.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::AccountKeyAccounts& ak, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "account_uuid")
            return db->EscapeString(ak.accountUuid);
        if (token.value == "account_key_uuid")
            return db->EscapeString(ak.uuid);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBAccountKeyAccounts::Create(AB::Entities::AccountKeyAccounts& ak)
{
    if (Utils::Uuid::IsEmpty(ak.uuid) || Utils::Uuid::IsEmpty(ak.accountUuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "INSERT INTO account_account_keys ("
            "account_uuid, account_key_uuid"
        ") VALUES ( "
            "${account_uuid}, ${account_key_uuid}"
        ")";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ak, std::placeholders::_1));

    DBTransaction transaction(db);
    if (!transaction.Begin())
        return false;

    if (!db->ExecuteQuery(query))
        return false;

    // End transaction
    if (!transaction.Commit())
        return false;

    return true;
}

bool DBAccountKeyAccounts::Load(AB::Entities::AccountKeyAccounts& ak)
{
    if (Utils::Uuid::IsEmpty(ak.uuid) || Utils::Uuid::IsEmpty(ak.accountUuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT * FROM account_account_keys WHERE "
        "account_uuid = ${account_uuid} "
        "AND account_key_uuid = ${account_key_uuid}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ak, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    ak.uuid = result->GetString("account_key_uuid");
    ak.accountUuid = result->GetString("account_uuid");
    return true;
}

bool DBAccountKeyAccounts::Save(const AB::Entities::AccountKeyAccounts&)
{
    // Not possible
    return true;
}

bool DBAccountKeyAccounts::Delete(const AB::Entities::AccountKeyAccounts&)
{
    // No possible
    return true;
}

bool DBAccountKeyAccounts::Exists(const AB::Entities::AccountKeyAccounts& ak)
{
    if (Utils::Uuid::IsEmpty(ak.uuid) || Utils::Uuid::IsEmpty(ak.accountUuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT COUNT(*) AS count FROM account_account_keys WHERE "
        "account_uuid = ${account_uuid} "
        "AND account_key_uuid = ${account_key_uuid}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ak, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    return result->GetUInt("count") != 0;
}

}
