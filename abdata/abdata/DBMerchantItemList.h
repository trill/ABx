/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/MerchantItemList.h>

namespace DB {

class DBMerchantItemList
{
public:
    DBMerchantItemList() = delete;
    ~DBMerchantItemList() = delete;

    static bool Create(AB::Entities::MerchantItemList& il);
    static bool Load(AB::Entities::MerchantItemList& il);
    static bool Save(const AB::Entities::MerchantItemList& il);
    static bool Delete(const AB::Entities::MerchantItemList& il);
    static bool Exists(const AB::Entities::MerchantItemList& il);
};

}
