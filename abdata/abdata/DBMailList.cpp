/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBMailList.h"
#include <sa/TemplateParser.h>
#include <sa/StringTempl.h>

namespace DB {

bool DBMailList::Create(AB::Entities::MailList& ml)
{
    if (Utils::Uuid::IsEmpty(ml.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBMailList::Load(AB::Entities::MailList& ml)
{
    if (Utils::Uuid::IsEmpty(ml.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    // Oldest first because the chat window scrolls down
    static constexpr const char* SQL = "SELECT uuid, from_name, subject, created, is_read, attachments FROM mails "
        "WHERE to_account_uuid = ${to_account_uuid} ORDER BY created ASC";
    Database* db = GetSubsystem<Database>();

    const std::string query = sa::templ::Parser::Evaluate(SQL, [&db, &ml](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "to_account_uuid")
                return db->EscapeString(ml.uuid);
            ASSERT_FALSE();
        default:
            return token.value;
        }
    });
    ml.mails.clear();

    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        const auto attachments = sa::Split(result->GetString("attachments"), ";", false, false);
        ml.mails.emplace_back(
            result->GetString("uuid"),
            result->GetString("from_name"),
            result->GetString("subject"),
            result->GetLong("created"),
            result->GetUInt("is_read") != 0,
            attachments.size()
        );
    }

    return true;
}

bool DBMailList::Save(const AB::Entities::MailList& ml)
{
    if (Utils::Uuid::IsEmpty(ml.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    // Do nothing
    return true;
}

bool DBMailList::Delete(const AB::Entities::MailList& ml)
{
    if (Utils::Uuid::IsEmpty(ml.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBMailList::Exists(const AB::Entities::MailList& ml)
{
    if (Utils::Uuid::IsEmpty(ml.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

}
