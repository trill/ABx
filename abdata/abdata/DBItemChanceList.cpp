/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBItemChanceList.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::ItemChanceList& il, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "map_uuid")
            return db->EscapeString(il.uuid);
        if (token.value == "empty_map_uuid")
            return db->EscapeString(Utils::Uuid::EMPTY_UUID);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBItemChanceList::Create(AB::Entities::ItemChanceList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBItemChanceList::Load(AB::Entities::ItemChanceList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "SELECT item_uuid, chance, can_drop FROM game_item_chances WHERE "
        "map_uuid = ${map_uuid} OR map_uuid = ${empty_map_uuid}";

    static const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, il, std::placeholders::_1));
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        il.items.push_back({
            result->GetString("item_uuid"),
            result->GetUInt("chance"),
            result->GetUInt("can_drop") == 1
        });
    }
    return true;
}

bool DBItemChanceList::Save(const AB::Entities::ItemChanceList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBItemChanceList::Delete(const AB::Entities::ItemChanceList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

bool DBItemChanceList::Exists(const AB::Entities::ItemChanceList& il)
{
    if (Utils::Uuid::IsEmpty(il.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    return true;
}

}
