/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBMusicList.h"

namespace DB {

bool DBMusicList::Create(AB::Entities::MusicList&)
{
    return true;
}

bool DBMusicList::Load(AB::Entities::MusicList& il)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM game_music ORDER BY sorting";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        il.musicUuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBMusicList::Save(const AB::Entities::MusicList&)
{
    return true;
}

bool DBMusicList::Delete(const AB::Entities::MusicList&)
{
    return true;
}

bool DBMusicList::Exists(const AB::Entities::MusicList&)
{
    return true;
}

}
