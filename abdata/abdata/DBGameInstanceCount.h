/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/GameInstanceCount.h>

namespace DB {

class DBGameInstanceCount
{
public:
    DBGameInstanceCount() = delete;
    ~DBGameInstanceCount() = delete;

    static bool Create(AB::Entities::GameInstanceCount&);
    static bool Load(AB::Entities::GameInstanceCount&);
    static bool Save(const AB::Entities::GameInstanceCount&);
    static bool Delete(const AB::Entities::GameInstanceCount&);
    static bool Exists(const AB::Entities::GameInstanceCount&);
};

}
