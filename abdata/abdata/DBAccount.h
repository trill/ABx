/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Account.h>

namespace DB {

class DBAccount
{
private:
    static void LoadCharacters(AB::Entities::Account& account);
public:
    DBAccount() = delete;
    ~DBAccount() = delete;

    static bool Create(AB::Entities::Account& account);
    /// Load an account identified by id or name.
    static bool Load(AB::Entities::Account& account);
    static bool Save(const AB::Entities::Account& account);
    static bool Delete(const AB::Entities::Account& account);
    static bool Exists(const AB::Entities::Account& account);
    static bool LogoutAll();
};

}
