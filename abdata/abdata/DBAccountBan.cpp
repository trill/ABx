/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBAccountBan.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::AccountBan& ban, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "uuid")
            return db->EscapeString(ban.uuid);
        if (token.value == "ban_uuid")
            return db->EscapeString(ban.banUuid);
        if (token.value == "account_uuid")
            return db->EscapeString(ban.accountUuid);
        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBAccountBan::Create(AB::Entities::AccountBan& ban)
{
    if (Utils::Uuid::IsEmpty(ban.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();
    DBTransaction transaction(db);
    if (!transaction.Begin())
        return false;

    static constexpr const char* SQL = "INSERT INTO account_bans ("
                "uuid, ban_uuid, account_uuid"
            ") VALUES ("
                "${uuid}, ${ban_uuid}, ${account_uuid}"
            ")";

    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ban, std::placeholders::_1));

    if (!db->ExecuteQuery(query))
        return false;

    return transaction.Commit();
}

bool DBAccountBan::Load(AB::Entities::AccountBan& ban)
{
    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL_UUID = "SELECT * FROM account_bans WHERE uuid = ${uuid}";
    static constexpr const char* SQL_ACCOUNT = "SELECT * FROM account_bans WHERE account_uuid = ${account_uuid}";

    const char* sql = nullptr;
    if (!Utils::Uuid::IsEmpty(ban.uuid))
        sql = SQL_UUID;
    else if (!ban.accountUuid.empty())
        sql = SQL_ACCOUNT;
    else
    {
        LOG_ERROR << "UUID and name are empty" << std::endl;
        return false;
    }
    const std::string query = sa::templ::Parser::Evaluate(sql, std::bind(&PlaceholderCallback, db, ban, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    ban.uuid = result->GetString("uuid");
    ban.banUuid = result->GetString("ban_uuid");
    ban.accountUuid = result->GetString("account_uuid");

    return true;
}

bool DBAccountBan::Save(const AB::Entities::AccountBan& ban)
{
    if (Utils::Uuid::IsEmpty(ban.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL = "UPDATE account_bans SET "
        "ban_uuid = ${ban_uuid}, "
        "account_uuid = ${account_uuid}, "
        "WHERE uuid = ${uuid}";

    DBTransaction transaction(db);
    if (!transaction.Begin())
        return false;

    if (!db->ExecuteQuery(sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ban, std::placeholders::_1))))
        return false;

    return transaction.Commit();
}

bool DBAccountBan::Delete(const AB::Entities::AccountBan& ban)
{
    if (Utils::Uuid::IsEmpty(ban.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }

    Database* db = GetSubsystem<Database>();
    static constexpr const char* SQL = "DELETE FROM account_bans WHERE uuid = ${uuid}";
    const std::string query = sa::templ::Parser::Evaluate(SQL, std::bind(&PlaceholderCallback, db, ban, std::placeholders::_1));

    DBTransaction transaction(db);
    if (!transaction.Begin())
        return false;

    if (!db->ExecuteQuery(query))
        return false;

    return transaction.Commit();
}

bool DBAccountBan::Exists(const AB::Entities::AccountBan& ban)
{
    Database* db = GetSubsystem<Database>();

    static constexpr const char* SQL_UUID = "SELECT COUNT(*) AS count FROM account_bans WHERE uuid = ${uuid}";
    static constexpr const char* SQL_ACCOUNT = "SELECT COUNT(*) AS count FROM account_bans WHERE account_uuid = ${account_uuid}";

    const char* sql = nullptr;
    if (!Utils::Uuid::IsEmpty(ban.uuid))
        sql = SQL_UUID;
    else if (!ban.accountUuid.empty())
        sql = SQL_ACCOUNT;
    else
    {
        LOG_ERROR << "UUID and Account UUID are empty" << std::endl;
        return false;
    }

    const std::string query = sa::templ::Parser::Evaluate(sql, std::bind(&PlaceholderCallback, db, ban, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    return result->GetUInt("count") != 0;
}

}
