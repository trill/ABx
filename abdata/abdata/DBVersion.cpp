/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBVersion.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::Version& v, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "uuid")
            return db->EscapeString(v.uuid);
        if (token.value == "name")
            return db->EscapeString(v.name);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBVersion::Create(AB::Entities::Version&)
{
    // Do nothing
    return true;
}

bool DBVersion::Load(AB::Entities::Version& v)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT * FROM versions WHERE ");

    if (!Utils::Uuid::IsEmpty(v.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else if (!v.name.empty())
        parser.Append("name = ${name}", tokens);
    else
    {
        LOG_ERROR << "UUID and name are empty" << std::endl;
        return false;
    }

    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, v, std::placeholders::_1));
    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    v.uuid = result->GetString("uuid");
    v.name = result->GetString("name");
    v.value = result->GetUInt("value");
    v.isInternal = result->GetUInt("internal") != 0;

    return true;
}

bool DBVersion::Save(const AB::Entities::Version&)
{
    // Do nothing
    return true;
}

bool DBVersion::Delete(const AB::Entities::Version&)
{
    // Do nothing
    return true;
}

bool DBVersion::Exists(const AB::Entities::Version& v)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT COUNT(*) AS count FROM versions WHERE ");

    if (!Utils::Uuid::IsEmpty(v.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else if (!v.name.empty())
        parser.Append("name = ${name}", tokens);
    else
    {
        LOG_ERROR << "UUID and name are empty" << std::endl;
        return false;
    }

    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, v, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    return result->GetUInt("count") != 0;
}

}
