/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/ItemPrice.h>

namespace DB {

class DBItemPrice
{
private:
    static uint32_t GetDropChance(const std::string& itemUuid);
    static uint32_t GetAvgValue(const std::string& itemUuid);
    static uint32_t GetAvailable(const std::string& itemUuid);
public:
    DBItemPrice() = delete;

    static bool Create(AB::Entities::ItemPrice& item);
    static bool Load(AB::Entities::ItemPrice& item);
    static bool Save(const AB::Entities::ItemPrice& item);
    static bool Delete(const AB::Entities::ItemPrice& item);
    static bool Exists(const AB::Entities::ItemPrice& item);
};

}
