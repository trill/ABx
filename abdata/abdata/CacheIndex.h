/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <multi_index_container.hpp>
#include <multi_index/hashed_index.hpp>
#include <multi_index/member.hpp>
#include <multi_index/identity.hpp>
#include <multi_index/ordered_index.hpp>
#include <libcommon/DataKey.h>

class CacheIndex
{
private:
    struct DataItem
    {
        IO::DataKey key;
        uint64_t rank;
        bool operator<(const DataItem& e) const
        {
            return rank < e.rank;
        }
    };
    using DataItemContainer = multi_index::multi_index_container
    <
        DataItem,
        multi_index::indexed_by
        <
            multi_index::hashed_unique<multi_index::member<DataItem, IO::DataKey, &DataItem::key>>,
            multi_index::ordered_unique<multi_index::member<DataItem, uint64_t, &DataItem::rank>>
        >
    >;
public:
    CacheIndex() :
        currentRank_(0)
    { }
    IO::DataKey Next();
    void Add(const IO::DataKey&);
    void Refresh(const IO::DataKey&);
    void Delete(const IO::DataKey&);
    void Clear();
private:
    uint64_t GetNextRank()
    {
        // NOTE: Not thread safe
        return currentRank_++;
    }
    uint64_t currentRank_;
    DataItemContainer dataItems_;
};
