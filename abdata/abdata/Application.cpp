/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Application.h"
#include "Version.h"
#include "Server.h"
#include <libcommon/Logo.h>
#include <libdb/Database.h>
#include <libcommon/StringUtils.h>
#include <libcommon/SimpleConfigManager.h>
#include <libcommon/FileUtils.h>
#include <libcommon/Scheduler.h>
#include <AB/Entities/Service.h>
#include <AB/Entities/ServiceList.h>
#include <libcommon/Utils.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <filesystem>
#include <sa/StringTempl.h>
#include <sa/time.h>

Application::Application() :
    listenIp_(0),
    maxSize_(0),
    readonly_(false),
    ioService_(),
    flushInterval_(FLUSH_CACHE_MS),
    cleanInterval_(CLEAN_CACHE_MS)
{
    programDescription_ = SERVER_PRODUCT_NAME;
    serverType_ = AB::Entities::ServiceTypeDataServer;
    Subsystems::Instance.CreateSubsystem<Asynch::Dispatcher>();
    Subsystems::Instance.CreateSubsystem<Asynch::Scheduler>();
    Subsystems::Instance.CreateSubsystem<IO::SimpleConfigManager>();

    std::stringstream dbDrivers;
#ifdef HAVE_SQLITE
    dbDrivers << " sqlite";
#endif
#ifdef HAVE_MYSQL
    dbDrivers << " mysql";
#endif
#ifdef HAVE_PGSQL
    dbDrivers << " pgsql";
#endif
#ifdef HAVE_ODBC
    dbDrivers << " odbc";
#endif

    cli_.push_back({ "maxsize", { "-maxsize", "--maxsize-cache" }, "Maximum cache size in bytes",
        false, true, sa::ArgParser::OptionType::Integer });
    cli_.push_back({ "readonly", { "-readonly", "--readonly-mode" }, "Read only mode. Do not write to DB",
        false, false, sa::ArgParser::OptionType::None });
    cli_.push_back({ "dbdriver", { "-dbdriver", "--database-driver" }, "Database driver, possible value(s):" + dbDrivers.str(),
        false, true, sa::ArgParser::OptionType::String });
    cli_.push_back({ "dbhost", { "-dbhost", "--database-host" }, "Host name of database server",
        false, true, sa::ArgParser::OptionType::String });
    cli_.push_back({ "dbport", { "-dbport", "--database-port" }, "Port to connect to",
        false, true, sa::ArgParser::OptionType::Integer });
    cli_.push_back({ "dbname", { "-dbname", "--database-name" }, "Name of database",
        false, true, sa::ArgParser::OptionType::String });
    cli_.push_back({ "dbuser", { "-dbuser", "--database-user" }, "User name for database",
        false, true, sa::ArgParser::OptionType::String });
    cli_.push_back({ "dbpass", { "-dbpass", "--database-password" }, "Password for database",
        false, true, sa::ArgParser::OptionType::String });
}

Application::~Application()
{
    if (running_)
        Stop();
    GetSubsystem<Asynch::Scheduler>()->Stop();
    GetSubsystem<Asynch::Dispatcher>()->Stop();
}

void Application::ShowLogo()
{
    std::cout << "This is " << SERVER_PRODUCT_NAME << std::endl;
    std::cout << "Version " << SERVER_VERSION_MAJOR << "." << SERVER_VERSION_MINOR;
#ifdef _DEBUG
    std::cout << " DEBUG";
#endif
    std::cout << std::endl;
    std::cout << "Copyright (C) 2017-" << CURRENT_YEAR << " " << AB_AUTHOR << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
    std::cout << std::endl;

    std::cout << AB_CONSOLE_LOGO << std::endl;

    std::cout << std::endl;
}

bool Application::ParseCommandLine()
{
    if (!ServerApp::ParseCommandLine())
        return false;

    if (!serverIp_.empty())
        listenIp_ = Utils::ConvertStringToIP(serverIp_);
    if (!logDir_.empty())
        IO::Logger::logDir_ = logDir_;

    maxSize_ = sa::ArgParser::GetValue<size_t>(parsedArgs_, "maxsize", maxSize_);
    readonly_ = sa::ArgParser::GetValue<bool>(parsedArgs_, "readonly", false);
    DB::Database::driver_ = sa::ArgParser::GetValue<std::string>(parsedArgs_, "dbdriver", DB::Database::driver_);
    DB::Database::dbHost_ = sa::ArgParser::GetValue<std::string>(parsedArgs_, "dbhost", DB::Database::dbHost_);
    DB::Database::dbName_ = sa::ArgParser::GetValue<std::string>(parsedArgs_, "dbname", DB::Database::dbName_);
    DB::Database::dbUser_ = sa::ArgParser::GetValue<std::string>(parsedArgs_, "dbuser", DB::Database::dbUser_);
    DB::Database::dbPass_ = sa::ArgParser::GetValue<std::string>(parsedArgs_, "dbpass", DB::Database::dbPass_);
    DB::Database::dbPort_ = sa::ArgParser::GetValue<uint16_t>(parsedArgs_, "dbport", DB::Database::dbPort_);

    return true;
}

void Application::ShowVersion()
{
    std::cout << SERVER_PRODUCT_NAME << " " << SERVER_VERSION_MAJOR << "." << SERVER_VERSION_MINOR << std::endl;
#ifdef _DEBUG
    std::cout << " DEBUG";
#endif
    std::cout << std::endl;
}

bool Application::LoadConfig()
{
    if (configFile_.empty())
        configFile_ = Utils::ConcatPath(path_, "abdata.lua");

    auto* config = GetSubsystem<IO::SimpleConfigManager>();
    if (Utils::FileExists(configFile_))
    {
        if (!config->Load(configFile_))
        {
            LOG_ERROR << "Error loading config file" << std::endl;
            return false;
        }
    }

    if (Utils::Uuid::IsEmpty(serverId_))
        serverId_ = config->GetGlobalString("server_id", Utils::Uuid::EMPTY_UUID);
    if (serverName_.empty())
        serverName_ = config->GetGlobalString("server_name", "abdata");
    if (serverLocation_.empty())
        serverLocation_ = config->GetGlobalString("location", "--");

    if (serverPort_ == std::numeric_limits<uint16_t>::max())
        serverPort_ = static_cast<uint16_t>(config->GetGlobalInt("data_port", 0ll));
    if (listenIp_ == 0)
        listenIp_ = Utils::ConvertStringToIP(config->GetGlobalString("data_ip", ""));
    if (serverHost_.empty())
        serverHost_ = config->GetGlobalString("data_host", "");
    if (maxSize_ == 0)
        maxSize_ = static_cast<size_t>(config->GetGlobalInt("max_size", 0ll));
    if (!readonly_)
        readonly_ = config->GetGlobalBool("read_only", false);

    std::string ips = config->GetGlobalString("allowed_ips", "");
    whiteList_.AddList(ips);

    if (IO::Logger::logDir_.empty())
        IO::Logger::logDir_ = config->GetGlobalString("log_dir", "");
    if (DB::Database::driver_.empty())
        DB::Database::driver_ = config->GetGlobalString("db_driver", "");
    if (DB::Database::dbHost_.empty())
        DB::Database::dbHost_ = config->GetGlobalString("db_host", "");
    if (DB::Database::dbName_.empty())
        DB::Database::dbName_ = config->GetGlobalString("db_name", "");
    if (DB::Database::dbUser_.empty())
        DB::Database::dbUser_ = config->GetGlobalString("db_user", "");
    if (DB::Database::dbPass_.empty())
        DB::Database::dbPass_ = config->GetGlobalString("db_pass", "");
    if (DB::Database::dbPort_ == 0)
        DB::Database::dbPort_ = static_cast<uint16_t>(config->GetGlobalInt("db_port", 0ll));

    flushInterval_ = static_cast<uint32_t>(config->GetGlobalInt("flush_interval", flushInterval_));
    cleanInterval_ = static_cast<uint32_t>(config->GetGlobalInt("clean_interval", cleanInterval_));

    if (serverPort_ == 0)
    {
        LOG_ERROR << "Port is 0" << std::endl;
        return false;
    }
    if (maxSize_ == 0)
    {
        LOG_ERROR << "Cache size is 0" << std::endl;
        return false;
    }
    return true;
}

void Application::PrintServerInfo()
{
    LOG_INFO << "Server config:" << std::endl;
    LOG_INFO << "  Server ID: " << GetServerId() << std::endl;
    LOG_INFO << "  Name: " << serverName_ << std::endl;
    LOG_INFO << "  Machine: " << machine_ << std::endl;
    LOG_INFO << "  OS: " << GetOsVersion() << std::endl;
    LOG_INFO << "  Location: " << serverLocation_ << std::endl;
    LOG_INFO << "  Config file: " << (configFile_.empty() ? "(empty)" : configFile_) << std::endl;
    LOG_INFO << "  Listening: " << Utils::ConvertIPToString(listenIp_) << ":" << serverPort_ << std::endl;
    LOG_INFO << "  Cache size: " << Utils::ConvertSize(maxSize_) << std::endl;
    LOG_INFO << "  Log dir: " << (IO::Logger::logDir_.empty() ? "(empty)" : IO::Logger::logDir_) << std::endl;
    LOG_INFO << "  Readonly mode: " << (readonly_ ? "TRUE" : "false") << std::endl;
    LOG_INFO << "  Allowed IPs: ";
    if (whiteList_.IsEmpty())
        LOG_INFO << "(all)";
    else
        LOG_INFO << whiteList_.ToString();
    LOG_INFO << std::endl;
    LOG_INFO << "Database drivers:";
#ifdef HAVE_SQLITE
    LOG_INFO << " SQLite";
#endif
#ifdef HAVE_MYSQL
    LOG_INFO << " MySQL";
#endif
#ifdef HAVE_PGSQL
    LOG_INFO << " PostgreSQL";
#endif
#ifdef HAVE_ODBC
    LOG_INFO << " ODBC";
#endif
    LOG_INFO << std::endl;
    LOG_INFO << "Database config:" << std::endl;
    LOG_INFO << "  Driver: " << DB::Database::driver_ << std::endl;
    LOG_INFO << "  Host: " << DB::Database::dbHost_ << std::endl;
    LOG_INFO << "  Name: " << DB::Database::dbName_ << std::endl;
    LOG_INFO << "  Port: " << DB::Database::dbPort_ << std::endl;
    LOG_INFO << "  User: " << DB::Database::dbUser_ << std::endl;
    LOG_INFO << "  Password: " << (DB::Database::dbPass_.empty() ? "(empty)" : "***********") << std::endl;
}

int Application::GetDatabaseVersion()
{
    auto* db = GetSubsystem<DB::Database>();
    std::ostringstream query;
#ifdef HAVE_PGSQL
    if (DB::Database::driver_ == "pgsql")
        query << "SET search_path TO schema, public; ";
#endif
    query << "SELECT value FROM versions WHERE ";
    query << "name = " << db->EscapeString("schema");

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query.str());
    if (!result)
    {
        // No such record means not even the first file (file 0) was imported
        return -1;
    }

    return static_cast<int>(result->GetUInt("value"));
}

bool Application::CheckDatabaseVersion()
{
    namespace fs = std::filesystem;

    auto* config = GetSubsystem<IO::SimpleConfigManager>();
    if (!config->GetGlobalBool("db_version_check", true))
    {
        return true;
    }

    std::string schemasDir = config->GetGlobalString("db_schema_dir", "");
    if (schemasDir.empty())
    {
        LOG_WARNING << "Schemas directory is empty, skillping database check" << std::endl;
        return true;
    }

    LOG_INFO << "Checking Database version...";

    if (!fs::is_directory(schemasDir))
    {
        LOG_INFO << "[WARN]" << std::endl;
        LOG_WARNING << "SQL directory " << schemasDir << " does not exist" << std::endl;
        return true;
    }
    int expectedVer = -1;
    for (const auto& entry : fs::directory_iterator(schemasDir))
    {
        const auto path = entry.path().string();
        const std::string filename = Utils::ExtractFileName(path);
        unsigned int version = 0;
#ifdef _MSC_VER
        if (sscanf_s(filename.c_str(), "schema.%u.sql", &version) != 1)
            continue;
#else
        if (sscanf(filename.c_str(), "schema.%u.sql", &version) != 1)
            continue;
#endif
        if (expectedVer < static_cast<int>(version))
            expectedVer = static_cast<int>(version);
    }

    int currVer = GetDatabaseVersion();
    if (currVer == -1)
    {
        LOG_INFO << "[FAIL]" << std::endl;
        LOG_ERROR << "Database is not initialized. Run `dbtool update`" << std::endl;
        return false;
    }
    if (expectedVer == -1)
    {
        LOG_INFO << "[WARN]" << std::endl;
        LOG_WARNING << "No *.sql files found in " << schemasDir << std::endl;
        // Do not fail when there is no sql directory
        return true;
    }
    if (currVer != expectedVer)
    {
        LOG_INFO << "[FAIL]" << std::endl;
        LOG_ERROR << "Database has wrong version. Expected " << expectedVer << " but got " << currVer <<
            ". Please run `dbtool update`" << std::endl;
        return false;
    }
    LOG_INFO << "[done]" << std::endl;

    return true;
}

bool Application::Initialize(const std::vector<std::string>& args)
{
    if (!ServerApp::Initialize(args))
        return false;
    if (!ParseCommandLine())
        return false;

    if (!sa::ArgParser::GetValue<bool>(parsedArgs_, "nologo", false))
        ShowLogo();

    LOG_INFO << "Reading config file...";
    if (!LoadConfig())
    {
        LOG_INFO << "[FAIL]" << std::endl;
        return false;
    }
    LOG_INFO << "[done]" << std::endl;

    if (!IO::Logger::logDir_.empty())
        IO::Logger::Close();

    LOG_INFO << "Connecting to database...";
    DB::Database* db = DB::Database::CreateInstance(DB::Database::driver_,
        DB::Database::dbHost_, DB::Database::dbPort_,
        DB::Database::dbUser_, DB::Database::dbPass_,
        DB::Database::dbName_
    );
    if (!db || !db->IsConnected())
    {
        LOG_INFO << "[FAIL]" << std::endl;
        LOG_ERROR << "Database connection failed" << std::endl;
        return false;
    }
    Subsystems::Instance.RegisterSubsystem<DB::Database>(db);
    LOG_INFO << "[done]" << std::endl;
    if (!CheckDatabaseVersion())
        return false;

    PrintServerInfo();

    return true;
}

void Application::Run()
{
    GetSubsystem<Asynch::Dispatcher>()->Start();
    GetSubsystem<Asynch::Scheduler>()->Start();

    server_ = ea::make_unique<Server>(ioService_, listenIp_, serverPort_, maxSize_, readonly_, whiteList_);
    auto& provider = server_->GetStorageProvider();
    provider.flushInterval_ = flushInterval_;
    provider.cleanInterval_ = cleanInterval_;

    AB::Entities::Service serv;
    serv.uuid = GetServerId();
    provider.EntityRead(serv);

    UpdateService(serv);
    serv.status = AB::Entities::ServiceStatusOnline;
    serv.type = serverType_;
    serv.startTime = sa::time::tick();
    serv.heartbeat = serv.startTime;
    serv.version = AB_SERVER_VERSION;
    provider.EntityUpdateOrCreate(serv);

    AB::Entities::ServiceList sl;
    provider.EntityInvalidate(sl);

    running_ = true;
    LOG_INFO << "Server is running" << std::endl;
    ioService_.run();
}

void Application::Stop()
{
    if (!running_)
        return;

    running_ = false;
    LOG_INFO << "Server shutdown..." << std::endl;

    auto& provider = server_->GetStorageProvider();
    AB::Entities::Service serv;
    serv.uuid = GetServerId();
    if (provider.EntityRead(serv))
    {
        serv.status = AB::Entities::ServiceStatusOffline;
        serv.stopTime = sa::time::tick();
        if (serv.startTime != 0)
            serv.runTime += (serv.stopTime - serv.startTime) / 1000;
        provider.EntityUpdate(serv);

        AB::Entities::ServiceList sl;
        provider.EntityInvalidate(sl);
    }
    else
        LOG_ERROR << "Error reading service" << std::endl;

    server_->Shutdown();
}
