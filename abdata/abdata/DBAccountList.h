/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/AccountList.h>

namespace DB {

class DBAccountList
{
public:
    DBAccountList() = delete;
    ~DBAccountList() = delete;

    static bool Create(AB::Entities::AccountList&);
    static bool Load(AB::Entities::AccountList& al);
    static bool Save(const AB::Entities::AccountList&);
    static bool Delete(const AB::Entities::AccountList&);
    static bool Exists(const AB::Entities::AccountList&);
};

}
