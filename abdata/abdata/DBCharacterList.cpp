/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBCharacterList.h"

namespace DB {

bool DBCharacterList::Create(AB::Entities::CharacterList&)
{
    return true;
}

bool DBCharacterList::Load(AB::Entities::CharacterList& al)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT uuid FROM players";
    for (std::shared_ptr<DB::DBResult> result = db->StoreQuery(query); result; result = result->Next())
    {
        al.uuids.push_back(result->GetString("uuid"));
    }
    return true;
}

bool DBCharacterList::Save(const AB::Entities::CharacterList&)
{
    return true;
}

bool DBCharacterList::Delete(const AB::Entities::CharacterList&)
{
    return true;
}

bool DBCharacterList::Exists(const AB::Entities::CharacterList&)
{
    return true;
}

}
