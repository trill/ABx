/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBGameInstanceCount.h"

namespace DB {

bool DBGameInstanceCount::Create(AB::Entities::GameInstanceCount&)
{
    return true;
}

bool DBGameInstanceCount::Load(AB::Entities::GameInstanceCount& count)
{
    Database* db = GetSubsystem<Database>();

    static const std::string query = "SELECT COUNT(*) AS count FROM instances";
    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    count.count = result->GetUInt("count");
    return true;
}

bool DBGameInstanceCount::Save(const AB::Entities::GameInstanceCount&)
{
    return true;
}

bool DBGameInstanceCount::Delete(const AB::Entities::GameInstanceCount&)
{
    return true;
}

bool DBGameInstanceCount::Exists(const AB::Entities::GameInstanceCount&)
{
    return true;
}

}
