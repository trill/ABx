/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <mutex>
#include <vector>
#include <sa/Compiler.h>
#include "CacheIndex.h"
#include "NameIndex.h"
#include <eastl.hpp>
#include <sa/StringHash.h>
#include <libcommon/DataKey.h>
#include <sa/CallableTable.h>
#include <sa/Bits.h>
#include <sa/Iteration.h>
#include <AB/Entities/Serializer.h>

// Clean cache every 10min
#define CLEAN_CACHE_MS (1000 * 60 * 10)
// Flush cache every minute
#define FLUSH_CACHE_MS (1000 * 60)
// Clear prices all 1 minute, is this a good value?
#define CLEAR_PRICES_MS (1000 * 60)

struct CacheFlag
{
    enum
    {
        Created = 1u,              // It exists in DB
        Modified = 1u << 1u,
        Deleted = 1u << 2u,
    };
};

using CacheFlags = uint32_t;

inline bool IsCreated(CacheFlags flags)
{
    return sa::bits::is_set(flags, CacheFlag::Created);
}
inline bool IsModified(CacheFlags flags)
{
    return sa::bits::is_set(flags, CacheFlag::Modified);
}
inline bool IsDeleted(CacheFlags flags)
{
    return sa::bits::is_set(flags, CacheFlag::Deleted);
}

using AB::Entities::Serializer::DataBuff;

class StorageProvider
{
public:
    StorageProvider(size_t maxSize, bool readonly);

    bool Lock(uint32_t clientId, const IO::DataKey& key);
    bool Unlock(uint32_t clientId, const IO::DataKey& key);
    bool Create(uint32_t clientId, const IO::DataKey& key, ea::shared_ptr<DataBuff> data);
    bool Update(uint32_t clientId, const IO::DataKey& key, ea::shared_ptr<DataBuff> data);
    bool Read(uint32_t clientId, const IO::DataKey& key, ea::shared_ptr<DataBuff> data);
    bool Delete(uint32_t clientId, const IO::DataKey& key);
    bool Invalidate(uint32_t clientId, const IO::DataKey& key);
    bool Preload(uint32_t clientId, const IO::DataKey& key);
    bool Exists(uint32_t clientId, const IO::DataKey& key, ea::shared_ptr<DataBuff> data);
    bool Clear(uint32_t clientId, const IO::DataKey& key);

    // Client compatible Methods
    template<typename E>
    bool EntityRead(E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        ea::shared_ptr<DataBuff> data = ea::make_shared<DataBuff>();
        SetEntity<E>(entity, *data);
        if (!Read(MY_CLIENT_ID, aKey, data))
            return false;
        return GetEntity(*data, entity);
    }
    template<typename E>
    bool EntityDelete(const E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        return Delete(MY_CLIENT_ID, aKey);
    }
    template<typename E>
    bool EntityUpdateOrCreate(E& entity)
    {
        if (EntityExists(entity))
            return EntityUpdate(entity);
        return EntityCreate(entity);
    }
    template<typename E>
    bool EntityUpdate(const E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        ea::shared_ptr<DataBuff> data = ea::make_shared<DataBuff>();
        if (SetEntity<E>(entity, *data) == 0)
            return false;
        return Update(MY_CLIENT_ID, aKey, data);
    }
    template<typename E>
    bool EntityCreate(E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        ea::shared_ptr<DataBuff> data = ea::make_shared<DataBuff>();
        if (SetEntity<E>(entity, *data) == 0)
            return false;
        return Create(MY_CLIENT_ID, aKey, data);
    }
    template<typename E>
    bool EntityPreload(const E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        return Preload(MY_CLIENT_ID, aKey);
    }
    template<typename E>
    bool EntityExists(const E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        ea::shared_ptr<DataBuff> data = ea::make_shared<DataBuff>();
        if (SetEntity<E>(entity, *data) == 0)
            return false;
        return Exists(MY_CLIENT_ID, aKey, data);
    }
    /// Flushes an entity and removes it from cache
    template<typename E>
    bool EntityInvalidate(const E& entity)
    {
        const IO::DataKey aKey(E::KEY(), uuids::uuid(entity.uuid));
        return Invalidate(MY_CLIENT_ID, aKey);
    }
    void Shutdown();
    void UnlockAll(uint32_t clientId);
    uint32_t flushInterval_;
    uint32_t cleanInterval_;
private:
    static constexpr uint32_t MY_CLIENT_ID = std::numeric_limits<uint32_t>::max();
    struct CacheItem
    {
        CacheFlags flags{ 0 };
        uint32_t locker{ 0 };
        ea::shared_ptr<DataBuff> data;
    };
    sa::CallableTable<size_t, bool, DataBuff&> exitsCallables_;
    sa::CallableTable<size_t, bool, CacheItem&> flushCallables_;
    sa::CallableTable<size_t, bool, const uuids::uuid&, DataBuff&> loadCallables_;
    template<typename D, typename E>
    void AddEntityClass()
    {
        static constexpr size_t hash = sa::StringHash(E::KEY());
        // For each Entity there are 3 methods that we need:
        // 1. Does the Entity exist
        // 2. Write (also delete) the entity to the DB
        // 3. Load the Entity from the DB
        // Yes, I love templates too!
        exitsCallables_.Add(hash, [this](auto& data) -> bool
        {
            return ExistsInDB<D, E>(data);
        });
        flushCallables_.Add(hash, [this](CacheItem& data) -> bool
        {
            return FlushRecord<D, E>(data);
        });
        loadCallables_.Add(hash, [this](const auto& id, auto& data) -> bool
        {
            return LoadFromDB<D, E>(id, data);
        });
    }
    void InitEnitityClasses();

    /// Read UUID from data
    static uuids::uuid GetUuid(const DataBuff& data);

    bool EnoughSpace(size_t size) const;
    void CreateSpace(size_t size);
    void CacheData(const std::string& table, const uuids::uuid& id,
        ea::shared_ptr<DataBuff> data,
        CacheFlags flags);
    bool RemoveData(uint32_t clientId, const IO::DataKey& key);
    void PreloadTask(IO::DataKey key);
    bool ExistsData(const IO::DataKey& key, DataBuff& data);
    /// If the data is a player and it's in playerNames_ remove it from playerNames_
    void RemovePlayerFromCache(const IO::DataKey& key);

    void CleanCache();
    void CleanTask();
    void FlushCache();
    void FlushCacheTask();
    void ClearPrices();
    void ClearPricesTask();

    /// Loads Data from DB
    bool LoadData(const IO::DataKey& key, DataBuff& data);
    template<typename D, typename E>
    bool LoadFromDB(const uuids::uuid& id, DataBuff& data)
    {
        E e{};
        if (!id.nil())
            // An UUID is all we need to identify a record
            e.uuid = id.to_string();
        else
        {
            // If not, get it from the data
            if (!GetEntity<E>(data, e))
                return false;
        }
        if (D::Load(e))
            return (SetEntity<E>(e, data) != 0);
        return false;
    }

    /// Save data to DB or delete from DB.
    /// Depending on the data header calls CreateInDB(), SaveToDB() and/or DeleteFromDB()
    bool FlushData(uint32_t clientId, const IO::DataKey& key);
    template<typename D, typename E>
    bool FlushRecord(CacheItem& data)
    {
        bool succ = true;
        // These flags are not mutually exclusive, howerer creating it implies it is no longer modified
        if (!IsCreated(data.flags))
        {
            succ = CreateInDB<D, E>(*data.data);
            if (succ)
            {
                sa::bits::set(data.flags, CacheFlag::Created);
                sa::bits::un_set(data.flags, CacheFlag::Modified);
            }
        }
        else if (IsModified(data.flags))
        {
            succ = SaveToDB<D, E>(*data.data);
            if (succ)
                sa::bits::un_set(data.flags, CacheFlag::Modified);
        }
        if (IsDeleted(data.flags))
            succ = DeleteFromDB<D, E>(*data.data);

        return succ;
    }
    template<typename D, typename E>
    bool CreateInDB(DataBuff& data)
    {
        E e{};
        if (GetEntity<E>(data, e))
        {
            if (D::Create(e))
            {
                // Update data, id may have changed
                return (SetEntity<E>(e, data) != 0);
            }
        }
        return false;
    }
    template<typename D, typename E>
    bool SaveToDB(DataBuff& data)
    {
        E e{};
        if (GetEntity<E>(data, e))
            return D::Save(e);
        return false;
    }
    template<typename D, typename E>
    bool DeleteFromDB(DataBuff& data)
    {
        E e{};
        if (GetEntity<E>(data, e))
            return D::Delete(e);
        return false;
    }
    template<typename D, typename E>
    bool ExistsInDB(DataBuff& data)
    {
        E e{};
        if (GetEntity<E>(data, e))
            return D::Exists(e);
        return false;
    }

    template<typename E>
    static bool GetEntity(DataBuff& data, E& e)
    {
        return AB::Entities::Serializer::Get(e, data);
    }
    // Returns size in bytes
    template<typename E>
    static size_t SetEntity(const E& e, DataBuff& buffer)
    {
        return AB::Entities::Serializer::Set(e, buffer);
    }
    // Iterate over cache items of a certain type
    // Example:
    //   VisitCache<AB::Entities::Game>([](auto& game) {
    //       do_something_with(game);
    //       // If it was changed, update it
    //       EntityUpdate(game);
    //       return Iteration::Continue;
    //   });
    template<typename E, typename Callback>
    void VisitCache(Callback&& callback)
    {
        for (auto& item : cache_)
        {
            std::string table;
            uuids::uuid id;
            if (!item.first.decode(table, id))
                continue;

            if (table != E::KEY())
                continue;

            auto& data = *item.second.data;
            E e{};
            if (GetEntity<E>(data, e))
            {
                if (callback(e) != Iteration::Continue)
                    break;
            }
        }
    }

    static bool IsUnlockedFor(uint32_t clientId, const CacheItem& item);

    bool readonly_;
    bool running_;
    size_t maxSize_;
    size_t currentSize_;

    ea::unordered_map<IO::DataKey, CacheItem, std::hash<IO::DataKey>> cache_;
    /// Name (Playername, Guildname etc.) -> Cache Key
    NameIndex namesIndex_;
    CacheIndex index_;
};
