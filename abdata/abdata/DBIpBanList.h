/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/IpBanList.h>

namespace DB {

class DBIpBanList
{
public:
    DBIpBanList() = delete;
    ~DBIpBanList() = delete;

    static bool Create(AB::Entities::IpBanList& li);
    static bool Load(AB::Entities::IpBanList& il);
    static bool Save(const AB::Entities::IpBanList& il);
    static bool Delete(const AB::Entities::IpBanList& il);
    static bool Exists(const AB::Entities::IpBanList& il);
};

}
