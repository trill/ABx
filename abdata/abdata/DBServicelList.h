/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/ServiceList.h>

namespace DB {

class DBServicelList
{
public:
    DBServicelList() = delete;
    ~DBServicelList() = delete;

    static bool Create(AB::Entities::ServiceList&);
    static bool Load(AB::Entities::ServiceList& sl);
    static bool Save(const AB::Entities::ServiceList&);
    static bool Delete(const AB::Entities::ServiceList&);
    static bool Exists(const AB::Entities::ServiceList&);
};

}
