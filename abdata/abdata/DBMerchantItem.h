/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/MerchantItem.h>

namespace DB {

class DBMerchantItem
{
public:
    DBMerchantItem() = delete;

    static bool Create(AB::Entities::MerchantItem& item);
    static bool Load(AB::Entities::MerchantItem& item);
    static bool Save(const AB::Entities::MerchantItem& item);
    static bool Delete(const AB::Entities::MerchantItem& item);
    static bool Exists(const AB::Entities::MerchantItem& item);
};

}
