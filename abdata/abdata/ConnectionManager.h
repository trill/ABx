/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <eastl.hpp>
#include "Connection.h"
#include <sa/Noncopyable.h>

class ConnectionManager
{
    NON_COPYABLE(ConnectionManager)
public:
    ConnectionManager() = default;

    /// Add the specified connection to the manager and start it.
    void Start(ea::shared_ptr<Connection> c);

    /// Stop the specified connection.
    void Stop(ea::shared_ptr<Connection> c);

    /// Stop all connections.
    void StopAll();

private:
    ea::set<ea::shared_ptr<Connection>> connections_;
};
