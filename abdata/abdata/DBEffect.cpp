/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DBEffect.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::Effect& effect, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "uuid")
            return db->EscapeString(effect.uuid);
        if (token.value == "idx")
            return std::to_string(effect.index);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBEffect::Create(AB::Entities::Effect& effect)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(effect.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBEffect::Load(AB::Entities::Effect& effect)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT * FROM game_effects WHERE ");
    if (!Utils::Uuid::IsEmpty(effect.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else if (effect.index != 0)
        parser.Append("idx = ${idx}", tokens);
    else
    {
        LOG_ERROR << "UUID and index are empty" << std::endl;
        return false;
    }

    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, effect, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    effect.uuid = result->GetString("uuid");
    effect.index = result->GetUInt("idx");
    effect.script = result->GetString("script");

    return true;
}

bool DBEffect::Save(const AB::Entities::Effect& effect)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(effect.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBEffect::Delete(const AB::Entities::Effect& effect)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(effect.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBEffect::Exists(const AB::Entities::Effect& effect)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT COUNT(*) AS count FROM game_effects WHERE ");
    if (!Utils::Uuid::IsEmpty(effect.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else if (effect.index != 0)
        parser.Append("idx = ${idx}", tokens);
    else
    {
        LOG_ERROR << "UUID and index are empty" << std::endl;
        return false;
    }

    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, effect, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    return result->GetUInt("count") != 0;
}

}
