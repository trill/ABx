/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libcommon/IpList.h>
#include <libcommon/ServerApp.h>
#include <asio.hpp>
#include <eastl.hpp>

class Server;

class Application final : public ServerApp
{
private:
    uint32_t listenIp_;
    size_t maxSize_;
    bool readonly_;
    asio::io_service ioService_;
    ea::unique_ptr<Server> server_;
    uint32_t flushInterval_;
    uint32_t cleanInterval_;
    Net::IpList whiteList_;
    bool LoadConfig();
    void PrintServerInfo();
    void ShowLogo();
    static bool CheckDatabaseVersion();
    static int GetDatabaseVersion();
protected:
    bool ParseCommandLine() override;
    void ShowVersion() override;
public:
    Application();
    ~Application() override;

    bool Initialize(const std::vector<std::string>& args) override;
    void Run() override;
    void Stop() override;
};

