/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <sa/hash.h>

namespace Sync {

using Sha256Hash = sa::Hash<unsigned char, 32>;

void CreateFileHash(const std::string& filename, const std::string& outfile);
Sha256Hash GetFileHash(const std::string& filename);

}
