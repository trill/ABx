/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string_view>

namespace Sync {

static constexpr std::string_view META_FILE_EXT = ".meta";
static constexpr std::string_view HASH_FILE_EXT = ".sha256";

}
