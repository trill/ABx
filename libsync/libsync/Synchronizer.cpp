/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Synchronizer.h"
#include "Partition.h"
#include "Operation.h"
#include <fstream>
#include "SyncDefs.h"

namespace Sync {

Synchronizer::Synchronizer(Destination& local, Source& remote) :
    local_(local),
    remote_(remote)
{}

bool Synchronizer::Synchronize(const std::string& localFile, const std::string& remoteFile)
{
    // Calculate hashes for local file
    const auto localHashes = PartitionFile(localFile, {});

    // Download JSON from remote (<filename>.META_FILE_EXT)
    const auto remoteJson = remote_.GetChunk(remoteFile + std::string(META_FILE_EXT));
    if (remoteJson.empty())
        // Does not exist on server
        return false;
    const auto remoteHashes = LoadBoundaryList(remoteJson);

    const auto delta = CompareFiles(localHashes, remoteHashes);
    different_ = !delta.empty();
    if (!different_)
        return true;

    size_t i = 0;
    for (const auto& op : delta)
    {
        const std::vector<char> buffer = (op.local == nullptr) ?
            // Download
            remote_.GetChunk(remoteFile, op.remote->start, op.remote->length) :
            // Copy
            local_.GetChunk(localFile, op.local->start, op.local->length);
        if (buffer.size() != op.remote->length)
            return false;
        if (op.local == nullptr)
            downloaded_ += op.remote->length;
        else
            copied_ += op.remote->length;
        if (!local_.WriteChunk(localFile, buffer, op.remote->start, op.remote->length))
            return false;
        ++i;
        if (!CallProgress(i, delta.size()))
            return false;
    }

    filesize_ = remoteHashes.back().start + remoteHashes.back().length;
    local_.Truncate(localFile, filesize_);

    return true;
}

bool Synchronizer::CallProgress(size_t value, size_t max) const
{
    if (!onProgress_)
        return true;
    return onProgress_(value, max);
}

}
