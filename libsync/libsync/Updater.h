/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <cstdint>
#include <memory>
#include "HttpSource.h"
#include "Hash.h"

namespace Sync {

class FileDestination;

class Updater
{
private:
    struct RemoteFile
    {
        // <platform>/file
        std::string remotePath;
        // The path of the file wihtout the platform dir. This should be the local path.
        std::string basePath;
        Sha256Hash remoteHash;
    };
    std::string localDir_;
    std::string indexFile_;
    std::unique_ptr<FileDestination> localBackend_;
    std::unique_ptr<HttpSource> remoteBackend_;
    std::unique_ptr<httplib::Headers> remoteHeaders_;
    std::vector<RemoteFile> remoteFiles_;
    size_t currentFile_{ 0 };
    bool cancelled_{ false };
    bool ProcessFile(const RemoteFile& file);
public:
    enum class ErrorType
    {
        Remote,
        Local
    };
    Updater(const std::string& remoteHost, uint16_t remotePort, const std::string& remoteAuth,
        std::string localDir, std::string indexFile);
    bool Execute();
    void Cancel() { cancelled_ = true; }
    bool DownloadRemoteFiles();
    [[nodiscard]] const std::vector<RemoteFile>& GetRemoteFiles() const { return remoteFiles_; }
    std::function<void(size_t fileIndex, size_t maxFiles, size_t value, size_t max)> onProgress_;
    // Check if file should be processed
    std::function<bool(size_t fileIndex, size_t maxFiles, const std::string& filename)> onProcessFile_;
    std::function<void(const std::string& filename)> onFailure_;
    std::function<void(const std::string& filename, bool different, size_t downloaded, size_t copied, int savings)> onDoneFile_;
    // Files are different, starts updating this file now
    std::function<void(const std::string& filename)> onStartFile_;
    std::function<void(ErrorType type, const char* message)> onError;
    std::function<void()> onUpdateStart_;
    std::function<void(bool success)> onUpdateDone_;
    std::function<void(size_t bps)> onDownloadProgress_;
    // If true it freshly downloads everything
    bool clean_{ false };
};

}
