/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <functional>
#include <sa/Compiler.h>

namespace Sync {

class Destination
{
protected:
    virtual void Error(const char* message)
    {
        if (onError_)
            onError_(message);
    }
public:
    virtual std::vector<char> GetChunk(const std::string& filename, size_t start = 0, size_t length = 0) = 0;
    virtual bool WriteChunk(const std::string& filename, const std::vector<char>& data, size_t start, size_t length) = 0;
    virtual void Truncate(const std::string& filename, size_t size) = 0;
    std::function<void(const char*)> onError_;
};

// Default local backend taking absolute filenames
class FileDestination : public Destination
{
private:
    std::string filename_;
    std::fstream stream_;
    bool OpenFile(const std::string& filename);
public:
    std::vector<char> GetChunk(const std::string& filename, size_t start = 0, size_t length = 0) override;
    bool WriteChunk(const std::string& filename, const std::vector<char>& data, size_t start, size_t length) override;
    void Truncate(const std::string& filename, size_t size) override;
    void Close();
};

}
