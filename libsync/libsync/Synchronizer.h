/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Destination.h"
#include "Source.h"
#include <string>
#include <functional>

namespace Sync {

class Synchronizer
{
private:
    Destination& local_;
    Source& remote_;
    size_t downloaded_{ 0 };
    size_t copied_{ 0 };
    size_t filesize_{ 0 };
    bool different_{ false };
    [[nodiscard]] bool CallProgress(size_t value, size_t max) const;
public:
    Synchronizer(Destination& local, Source& remote);
    bool Synchronize(const std::string& localFile, const std::string& remoteFile);

    [[nodiscard]] size_t GetDownloaded() const { return downloaded_; }
    [[nodiscard]] size_t GetCopied() const { return copied_; }
    [[nodiscard]] size_t GetFilesize() const { return filesize_; }
    [[nodiscard]] int GetSavings() const { return 100 - (int)(100.0f / (float)filesize_ * (float)downloaded_); }
    [[nodiscard]] bool IsDifferent() const { return different_; }

    std::function<bool(size_t value, size_t max)> onProgress_;
};

}
