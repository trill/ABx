/**
 * Copyright 2020-2022 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Hash.h"
extern "C" {
#include <sha256.h>
}
#include <fstream>
#include <vector>
#include <iomanip>
#include <sstream>

namespace Sync {

void CreateFileHash(const std::string& filename, const std::string& outfile)
{
    std::ifstream ifs(filename, std::ifstream::binary);
    ifs.seekg(0, std::ios::end);
    size_t fileSize = static_cast<size_t>((long)ifs.tellg());
    if (fileSize == 0)
        return;

    Sha256Hash sha_hash;
    sha256_ctx ctx;
    sha256_init(&ctx);

    ifs.seekg(0, std::ios::beg);
    std::vector<char> buffer(4096);
    while (ifs)
    {
        ifs.read(&buffer[0], 4096);
        auto readLength = ifs.gcount();
        sha256_update(&ctx, (const unsigned char*)&buffer[0], (long)readLength);
    }
    sha256_final(&ctx, sha_hash.Data());

    std::ofstream out(outfile, std::ios::out);
    out << sha_hash;
}

Sha256Hash GetFileHash(const std::string& filename)
{
    std::ifstream ifs(filename, std::ios::binary);
    ifs.seekg(0, std::ios::end);
    size_t fileSize = static_cast<size_t>((long)ifs.tellg());
    if (fileSize == 0)
        return {};

    Sha256Hash sha_hash;
    sha256_ctx ctx;
    sha256_init(&ctx);

    ifs.seekg(0, std::ios::beg);
    std::vector<char> buffer(4096);
    while (ifs)
    {
        ifs.read(&buffer[0], 4096);
        auto readLength = ifs.gcount();
        sha256_update(&ctx, (const unsigned char*)&buffer[0], (long)readLength);
    }
    sha256_final(&ctx, sha_hash.Data());
    return sha_hash;
}

}
