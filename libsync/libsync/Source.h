/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <functional>
#include <sa/Compiler.h>

namespace Sync {

class Source
{
protected:
    virtual void Error(const char* message)
    {
        if (onError_)
            onError_(message);
    }
public:
    virtual std::vector<char> GetChunk(const std::string& filename, size_t start = 0, size_t length = 0) = 0;
    std::function<void(const char*)> onError_;
};

}
