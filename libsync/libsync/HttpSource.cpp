/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HttpSource.h"
#include <sa/http_range.h>
#include <AB/CommonConfig.h>
#include <sa/http_status.h>
#include <sa/time.h>
#include <numeric>

namespace Sync {

HttpSource::HttpSource(std::string host, uint16_t port, httplib::Headers headers) :
    host_(std::move(host)),
    port_(port),
    headers_(std::move(headers))
{
}

httplib::SSLClient* HttpSource::GetHttpClient()
{
    if (!client_)
    {
        if (!host_.empty() && port_ != 0)
            client_ = std::make_unique<httplib::SSLClient>(host_, port_);
        if (client_)
            client_->enable_server_certificate_verification(HTTP_CLIENT_VERIFY_SSL);
    }
    if (client_)
        return client_.get();
    return nullptr;
}

std::vector<char> HttpSource::GetChunk(const std::string& filename, size_t start, size_t length)
{
    auto* client = GetHttpClient();
    if (client == nullptr)
        return {};

    httplib::Headers header(headers_);
    if (start != 0 || length != 0)
    {
        const sa::http::Range range{ start, start + length, (ssize_t)length };
        header.emplace("Range", sa::http::ToString(range));
    }

    std::string remoteName(filename);
    if (remoteName.front() != '/')
        remoteName = "/" + remoteName;
    std::vector<char> result;
    sa::time::timer timer;
    int64_t elapsed = 0;
    auto res = client->Get(remoteName.c_str(), header, [&](const char* data, uint64_t size) -> bool
    {
        result.reserve(result.size() + size);
        std::copy(data, data + size, std::back_inserter(result));
        // Division by zero is something really nasty!
        int64_t millis = std::max<int64_t>(1, timer.elapsed_millis());
        float bps = (float)size / (float)millis * 1000.0f;
        elapsed += millis;

        if (elapsed >= 20)
        {
            if (onDownloadProgress_)
            {
                if (!onDownloadProgress_(static_cast<size_t>(bps)))
                    return false;
            }
            elapsed = 0;
        }
        timer.restart();
        return true;
    });

    if (!res)
    {
        switch (res.error())
        {
        case httplib::Error::Connection:
            Error("Connection");
            break;
        case httplib::Error::BindIPAddress:
            Error("Bind IP Address");
            break;
        case httplib::Error::Read:
            Error("Read");
            break;
        case httplib::Error::Write:
            Error("Write");
            break;
        case httplib::Error::ExceedRedirectCount:
            Error("Exceed redirect count");
            break;
        case httplib::Error::Canceled:
            Error("Canceled");
            break;
        case httplib::Error::SSLConnection:
            Error("SSL Connection");
            break;
        case httplib::Error::SSLServerVerification:
            Error("SSL Server verification");
            break;
        default:
            break;
        }
        return {};
    }
    if (res->status == 200 || res->status == 206)
        return result;
    Error(sa::http::StatusMessage(res->status));
    return {};
}

}
