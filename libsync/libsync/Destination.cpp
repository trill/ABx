/**
 * Copyright 2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Destination.h"
#include <sa/Assert.h>
#include <filesystem>

namespace Sync {

bool FileDestination::OpenFile(const std::string& filename)
{
    if (filename == filename_ && stream_.is_open())
        return true;
    stream_ = std::fstream(filename, std::fstream::binary | std::fstream::in | std::fstream::out);
    if (!stream_.is_open())
        // New file
        stream_ = std::fstream(filename, std::fstream::binary | std::fstream::out);
    filename_ = filename;
    bool result = stream_.is_open();
    if (!result)
        Error("Error opening file");
    return result;
}

std::vector<char> FileDestination::GetChunk(const std::string& filename, size_t start, size_t length)
{
    if (!OpenFile(filename))
        return {};
    std::vector<char> result(length);

    stream_.seekg(start, std::ios::beg);
    auto len = stream_.read(&result[0], static_cast<std::streamsize>(length)).gcount();
    ASSERT(static_cast<size_t>((long)len) == length);

    return result;
}

bool FileDestination::WriteChunk(const std::string& filename, const std::vector<char>& data, size_t start, size_t length)
{
    if (!OpenFile(filename))
        return false;
    stream_.seekg(start, std::ios::beg);
    stream_.write(&data[0], length);
    return stream_.good();
}

void FileDestination::Truncate(const std::string& filename, size_t size)
{
    std::filesystem::resize_file(filename, size);
}

void FileDestination::Close()
{
    if (stream_.is_open())
        stream_.close();
}

}
