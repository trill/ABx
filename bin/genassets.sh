#!/bin/sh

# Generate server assets from client assets

./import -scene ../abclient/bin/AbData/Scenes/Argos.xml -o data/maps/argos $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Athena.xml -o data/maps/athena $1 $2
./import -scene "../abclient/bin/AbData/Scenes/Athena Arena.xml" -o data/maps/athena_arena $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Bassae.xml -o data/maps/bassae $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Claros.xml -o data/maps/claros $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Cos.xml -o data/maps/cos $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Crete.xml -o data/maps/crete $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Delphi.xml -o data/maps/delphi $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Didymes.xml -o data/maps/didymes $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Dion.xml -o data/maps/dion $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Dodona.xml -o data/maps/dodona $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Magnesia.xml -o data/maps/magnesia $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Pergamon.xml -o data/maps/pergamon $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Rhodes.xml -o data/maps/rhodes $1 $2
./import -scene "../abclient/bin/AbData/Scenes/Rhodes Arena.xml" -o data/maps/rhodes_arena $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Samos.xml -o data/maps/samos $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Spartha.xml -o data/maps/spartha $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Thebes.xml -o data/maps/thebes $1 $2
./import -scene ../abclient/bin/AbData/Scenes/Troy.xml -o data/maps/troy $1 $2
