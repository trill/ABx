--------------------------------------------------------------------------------
-- General Settings ------------------------------------------------------------
--------------------------------------------------------------------------------

server_id = "548350e7-5919-4bcf-a885-2d2888db05ca"
location = "AT"
server_name = "AB Admin"

admin_ip = ""         -- Listen on all
admin_host = ""       -- emtpy use same host as for login
admin_port = 8443

-- Create self signing Key/Cert with something like this:
-- openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout server.key -out server.crt
server_key = "server.key"
server_cert = "server.crt"

-- Thread pool size
num_threads = 4
root_dir = "admin/root"
-- 60min
session_lifetime = 1000 * 60 * 60
-- Static textfiles files smaller this value are sent directly to the client. Tney are also compressed and an ETag header is added.
small_file_size = 400 * 1024
-- Known content types: file extension -> content type
content_types = {
    [".css"]   = "text/css",
    [".html"]  = "text/html",
    [".lpp"]   = "text/html",
    [".xml"]   = "application/xml",
    [".js"]    = "application/javascript",
    [".json"]  = "application/json",
    [".pdf"]   = "application/pdf",
    [".zip"]   = "application/zip",
    [".7z"]    = "application/x-7z-compressed",
    [".tar"]   = "application/x-tar",
    [".gz"]    = "application/gzip",
    [".gif"]   = "image/gif",
    [".jpg"]   = "image/jpeg",
    [".jpeg"]  = "image/jpeg",
    [".png"]   = "image/png",
    [".svg"]   = "image/svg+xml",
    [".ico"]   = "image/x-icon",
    [".otf"]   = "font/otf",
    [".sfnt"]  = "font/sfnt",
    [".ttf"]   = "font/ttf",
    [".woff"]  = "font/woff",
    [".woff2"] = "font/woff2",
}

local COOKIE_SAMESITE_LAX = 0
local COOKIE_SAMESITE_STRICT = 1
local COOKIE_SAMESITE_NONE = 2

cookie_samesite = COOKIE_SAMESITE_NONE
cookie_secure = true

require("config/data_server")
require("config/msg_server")
require("config/login")
