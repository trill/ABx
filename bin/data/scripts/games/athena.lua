--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/create_npcs.lua")

local dorothea_samara_waypoints = {
  {  -2.4, 0.0,  -9.9 },
  { -20.0, 0.0, -32.7 },
  { -47.6, 0.0, -47.8 },
  {   6.2, 0.0, -12.2 },
  {  38.2, 0.0, -11.2 },
  {   5.8, 0.0,   9.9 },
}
local marianna_gani_waypoints = {
  {  48.5, 0.0,  19.4 },
  {  35.9, 0.0,  47.8 },
  { -25.9, 0.0,  41.2 },
  {  -6.1, 0.0,  -9.5 },
  {  40.1, 0.0, -35.4 },
  {  75.9, 0.0, -28.8 },
}

local function createGoatHerd()
  -- Why can't I create 10???
  for i = 1, 10, 1 do
    local goat = self:AddNpc("/scripts/actors/npcs/goat.lua")
    if (goat ~= nil) then
      goat:AddFriendFoe(GROUPMASK_ALL, 0)
      local x = 57.0 + Random(-2, 2)
      local z = -33.0 + Random(-2, 2)
      local y = self:GetTerrainHeight(x, z)
      goat.Position = {x, y, z}
      goat.Rotation = Random(360)
      goat:SetScaleSimple(Random(0.9, 1.2))
    end
  end
end

local function createSheepHerd()
  -- Why can't I create 10???
  for i = 1, 10, 1 do
    local goat = self:AddNpc("/scripts/actors/npcs/sheep.lua")
    if (goat ~= nil) then
      goat:AddFriendFoe(GROUPMASK_ALL, 0)
      local x = 50.0 + Random(-2, 2)
      local z = -54.0 + Random(-2, 2)
      local y = self:GetTerrainHeight(x, z)
      goat.Position = {x, y, z}
      goat.Rotation = Random(360)
      goat:SetScaleSimple(Random(0.9, 1.2))
    end
  end
end

-- Game start up
function onStart()
  createPortal(self, -32.6, 25.0, 16.0, "Athena Arena", "3c081fd5-3966-433a-bc61-50a33084eac2")
--  createPortal(self, -20.059, 26.7, -0.00870347, "Rhodes Arena", "a13b71f8-fe19-4bf5-bba7-c7642c796c0f")

  local smith = self:AddNpc("/scripts/actors/npcs/smith.lua")
  if (smith ~= nil) then
    local x = -18.4
    local z = 14.0
    local y = self:GetTerrainHeight(x, z)
    smith.Position = {x, y, z}
    smith.Rotation = 180
    smith.HomePos = {x, y, z}
    smith:AddFriendFoe(GROUPMASK_1 | GROUPMASK_2, 0)
  end
  local merchant = self:AddNpc("/scripts/actors/npcs/merchant.lua")
  if (merchant ~= nil) then
    local x = -9.4
    local z = 16.0
    local y = self:GetTerrainHeight(x, z)
    merchant.Position = {x, y, z}
    merchant.Rotation = 180
    merchant.HomePos = {x, y, z}
    merchant:AddFriendFoe(GROUPMASK_1 | GROUPMASK_2, 0)
  end
  local ped = self:AddNpc("/scripts/actors/npcs/marianna_gani.lua")
  if (ped ~= nil) then
    local x = 50.1
    local z = 20.0
    local y = self:GetTerrainHeight(x, z)
    ped.Position = {x, y, z}
    ped.Rotation = 90
    ped.HomePos = {x, y, z}
    ped:AddFriendFoe(GROUPMASK_1 | GROUPMASK_2, 0)
    ped.Wander = true
    ped:AddWanderPoints(marianna_gani_waypoints)
  end
  local ped2 = self:AddNpc("/scripts/actors/npcs/dorothea_samara.lua")
  if (ped2 ~= nil) then
    local x = -8.0
    local z = 3.9
--    local x = 4.1
--    local z = 8.1
    local y = self:GetTerrainHeight(x, z)
    ped2.Position = {x, y, z}
    ped2.HomePos = {x, y, z}
    ped2:AddFriendFoe(GROUPMASK_1 | GROUPMASK_2, 0)
    -- Add current position as first point
--    ped2:AddWanderPoint({x, y, z})
    ped2:AddWanderPoints(dorothea_samara_waypoints)
  end

  local chest = createChest(self, -12.3, 11.4)
  if (chest ~= nil) then
    chest.Rotation = 180
  end

  -- Add some poison
  self:AddAreaOfEffect(
    "/scripts/actors/aoe/general/magic_mushroom.lua",
    nil, 10001, {-50.1, 0.0, 33.0})

  local pdl = self:AddNpc("/scripts/actors/npcs/poison_dart_launcher.lua")
  if (pdl ~= nil) then
    local x = -50.4
    local z = 22.96
    local y = 28.8
--    local y = self:GetTerrainHeight(x, z)
    pdl.Position = {x, y, z}
    -- NPCs group mask 1 -> don't shoot at NPCs
    -- Foe with 2 -> shoot at players
    pdl:AddFriendFoe(GROUPMASK_1, GROUPMASK_2)
  end
  local turtle = self:AddNpc("/scripts/actors/npcs/turtle.lua")
  if (turtle ~= nil) then
    turtle:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = -87
    local z = -40
    local y = self:GetTerrainHeight(x, z)
    turtle.Position = {x, y, z}
  end
  createGoatHerd()
  createSheepHerd()
  local horse = self:AddNpc("/scripts/actors/npcs/horse.lua")
  if (horse ~= nil) then
    horse:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = 48.0
    local z = 62.0
    local y = self:GetTerrainHeight(x, z)
    horse.Position = {x, y, z}
    horse.Rotation = 90
  end
  local donkey = self:AddNpc("/scripts/actors/npcs/donkey.lua")
  if (donkey ~= nil) then
    donkey:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = 45.0
    local z = 60.0
    local y = self:GetTerrainHeight(x, z)
    donkey.Position = {x, y, z}
    donkey.Rotation = 90
  end
  local camel = self:AddNpc("/scripts/actors/npcs/camel.lua")
  if (camel ~= nil) then
    camel:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = 66
    local z = -25
    local y = self:GetTerrainHeight(x, z)
    camel.Position = {x, y, z}
    camel.Rotation = 90
  end

  local cow = self:AddNpc("/scripts/actors/npcs/cow.lua")
  if (cow ~= nil) then
    cow:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = 49.0
    local z = 80.0
    local y = self:GetTerrainHeight(x, z)
    cow.Position = {x, y, z}
    cow.Rotation = -70
  end
  local bull = self:AddNpc("/scripts/actors/npcs/bull.lua")
  if (bull ~= nil) then
    bull:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = 48.0
    local z = 75.0
    local y = self:GetTerrainHeight(x, z)
    bull.Position = {x, y, z}
    bull.Rotation = -90
  end
  local spot = self:AddNpc("/scripts/actors/npcs/spot.lua")
  if (spot ~= nil) then
    spot:AddFriendFoe(GROUPMASK_ALL, 0)
    local x = -7
    local z = 15
    local y = self:GetTerrainHeight(x, z)
    spot.Position = {x, y, z}
    spot.State = CREATURESTATE_EMOTE_SIT
    spot.Rotation = 180
  end
end

-- Game stopping
function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
  if (player ~= nil) then
    player:AddEffect(empty, 1000, 0)
    player:AddFriendFoe(GROUPMASK_2, 0)
  end
end

function onPlayerLeave(player)
  if (player ~= nil) then
    player:RemoveEffect(1000)
  end
end

-- Game Update
function onUpdate(timeElapsed)
end
