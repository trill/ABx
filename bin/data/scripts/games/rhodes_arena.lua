--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")

local point_blue = {
  -20.0, 0.0, -27.0
}

local point_red = {
  -19.0, 0.0, -44.0
}

local point_yellow = {
  -5.8, 0.0, -35.5
}

local function createTeam(spawn, frnd, foe, rot, color, name_suffix)
  -- To make them allies set the same group ID. Adding NPCs to a Crowd sets the group ID.
  local crowd = self:AddGroup()
  crowd.Color = color
  local guildLord = self:AddNpc("/scripts/actors/npcs/guild_lord.lua")
  if (guildLord ~= nil) then
    guildLord.Name = guildLord.Name .. " " .. name_suffix
    crowd:Add(guildLord)
    local x = spawn[1] + Random(-1, 1)
    local z = spawn[3] + Random(-1, 1)
    local y = self:GetTerrainHeight(x, z)
    guildLord.Position = {x, y, z}
    guildLord.Rotation = rot
    guildLord:AddFriendFoe(frnd, foe)
  end

  local ped2 = self:AddNpc("/scripts/actors/npcs/dorothea_samara.lua")
  if (ped2 ~= nil) then
    ped2.Name = ped2.Name .. " " .. name_suffix
    crowd:Add(ped2)
    local x = spawn[1] + Random(-1, 1)
    local z = spawn[3] + Random(-1, 1)
    local y = self:GetTerrainHeight(x, z)
    ped2.Position = {x, y, z}
    ped2.Rotation = rot
    ped2:AddFriendFoe(frnd, foe)
  end
  local ped3 = self:AddNpc("/scripts/actors/npcs/electra_staneli.lua")
  if (ped3 ~= nil) then
    ped3.Name = ped3.Name .. " " .. name_suffix
    crowd:Add(ped3)
    local x = spawn[1] + Random(-1, 1)
    local z = spawn[3] + Random(-1, 1)
    local y = self:GetTerrainHeight(x, z)
    ped3.Position = {x, y, z}
    ped3.Rotation = rot
    ped3:AddFriendFoe(frnd, foe)
  end

  local priest = self:AddNpc("/scripts/actors/npcs/priest.lua")
  if (priest ~= nil) then
    priest.Name = priest.Name .. " " .. name_suffix
    crowd:Add(priest)
    local x = spawn[1] + Random(-1, 1)
    local z = spawn[3] + Random(-1, 1)
    local y = self:GetTerrainHeight(x, z)
    priest.Position = {x, y, z}
    priest.Rotation = rot
    priest:AddFriendFoe(frnd, foe)
  end
end

function onStart()
  createTeam(point_blue, GROUPMASK_1, GROUPMASK_2 | GROUPMASK_3, -90, TEAMCOLOR_BLUE, "(Blue)")
  createTeam(point_red, GROUPMASK_2, GROUPMASK_1 | GROUPMASK_3, -90, TEAMCOLOR_RED, "(Red)")
  createTeam(point_yellow, GROUPMASK_3, GROUPMASK_1 | GROUPMASK_2, -90, TEAMCOLOR_YELLOW, "(Yellow)")
end

function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
  player:AddEffect(empty, 1000, 0)

  player:AddFriendFoe(GROUPMASK_3, GROUPMASK_1 | GROUPMASK_2)
end

function onPlayerLeave(player)
end

-- Game Update
function onUpdate(timeElapsed)
end

function onActorDied(actor, killer)
  if (killer ~= nil) then
    local group = killer.Group
    if (group ~= nil) then
      group:IncreaseMorale()
    end
  end
end

function onActorResurrected(actor)
end
