--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/create_npcs.lua")

function onStart()
  createPortal(self, 149.139, 22.5063, -31.5364, "Rhodes Arena", "a13b71f8-fe19-4bf5-bba7-c7642c796c0f")
end

function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
end

function onPlayerLeave(player)
end

-- Game Update
function onUpdate(timeElapsed)
end
