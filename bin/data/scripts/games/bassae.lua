--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/create_npcs.lua")

function onStart()
  local ped2 = self:AddNpc("/scripts/actors/npcs/marianna_gani.lua")
  if (ped2 ~= nil) then
    local x = 4.92965
    local z = 5.2049
    local y = self:GetTerrainHeight(x, z)
    ped2.Position = {x, y, z}
    ped2:AddFriendFoe(GROUPMASK_1 | GROUPMASK_2, 0)
  end
end

function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
end

function onPlayerLeave(player)
end

-- Game Update
function onUpdate(timeElapsed)
end
