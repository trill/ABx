--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

function onStart()
  local portal = self:AddNpc("/scripts/actors/logic/portal.lua")
  if (portal ~= nil) then
    portal.Name = "Rhodes Arena"
    -- Map ID where this portal leads to
    portal:SetVarString("destination", "a13b71f8-fe19-4bf5-bba7-c7642c796c0f")
    local x = 93.8996
    local z = -35.5445
    local y = 32.1627
    portal.Position = {x, y, z}
  end
end

function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
end

function onPlayerLeave(player)
end

-- Game Update
function onUpdate(timeElapsed)
end
