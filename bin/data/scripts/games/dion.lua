--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

function onStart()
end

function onStop()
end

function onAddObject(object)
end

function onRemoveObject(object)
end

function onPlayerJoin(player)
end

function onPlayerLeave(player)
end

-- Game Update
function onUpdate(timeElapsed)
end
