--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")

name = "Brambles"
itemIndex = 17
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE

local lifeTime = 0
local alive = 0

function onInit()
  self.Species = SPECIES_SPIRIT
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  -- Can not resurrect spirits
  self.Resurrectable = false
  -- Can not make minions from spirits
  self.Recycleable = false
  -- A spirit is not member of the group
  self.GroupId = 0
  lifeTime = math.floor((self:GetLevel() * 8 + 30) * 1000)
  local myscript = self:GetScriptFile()
  -- We are not yet in the octree so lets query master's range
  local master = self.Master
  local actors = master:GetAlliesInRange(RANGE_SPIRIT)
  for i, actor in ipairs(actors) do
    local npc = actor:AsNpc()
    if (npc ~= nil) then
      -- Only one spirit of the same type
      if (npc:GetScriptFile() == myscript) then
        npc:Die(nil)
      end
    end
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead() == false) then
    alive = alive + timeElapsed
    if (alive >= lifeTime) then
      self:Die(nil)
    end
  end
end

function onDied(killer)
  self:RemoveIn(500)
end

function onEnterRange(range, actor)
  if (range ~= RANGE_SPIRIT) then
    return
  end
  if (self:IsEnemy(actor)) then
    local duration = math.floor(lifeTime - alive)
    actor:AddEffect(self, 947, duration)
  end
end

function onLeaveRange(range, actor)
  if (range ~= RANGE_SPIRIT) then
    return
  end
  if (self:IsEnemy(actor)) then
    actor:RemoveEffect(947)
  end
end
