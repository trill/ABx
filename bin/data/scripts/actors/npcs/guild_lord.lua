--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/chat.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Guild Lord"
level = 20
itemIndex = 5     -- Smith body model
sex = SEX_MALE
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_WARRIOR
prof2Index = PROFESSIONINDEX_RANGER
behavior = "guild_lord"
companion = { 
  script = "/scripts/actors/npcs/cheetah.lua",
  level = 20,
  name = "Friendly Pet" 
}
    
function onInit()
  self.Species = SPECIES_HUMAN
  self.CombatMode = COMBAT_MODE_FIGHT
  local skillBar = self:GetSkillBar()
  -- Add a heal skill
--  skillBar:AddSkill(1)

  skillBar:AddSkill(5700)
  skillBar:SetAttributeRank(ATTRIB_TACTICS, 12)
  skillBar:SetAttributeRank(ATTRIB_STRENGTH, 12)
  return true
end

function onUpdate(timeElapsed)
end

function onClicked(creature)
end

-- self was selected by creature
function onSelected(creature)
end

-- creature collides with self
function onCollide(creature)
end

function onDied(killer)
  if (self:GetDeaths() < 2) then
    self:DropRandomItem()
  end
end
