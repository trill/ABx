--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Spot Mini"
itemIndex = 37
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "mini_pet"

local lastPosUpdate = 0

function onInit(master, corpse)
  self.CombatMode = COMBAT_MODE_GUARD
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self.Resurrectable = false
  self.Recycleable = false
  self.Undestroyable = true
  self.GroupId = 0
  self.CollisionMask = 0
  self.CollisionLayer = 0
  self:AddEffect(nil, 900000, TIME_FOREVER)

  if (master ~= nil) then
    self.HomePos = master.Position
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastPosUpdate = lastPosUpdate + timeElapsed
  local master = self.Master
  if (master == nil) then
    self:Remove()
    return
  end
  if (lastPosUpdate >= 1000) then
    if (master ~= nil) then
      self.HomePos = master.Position
    end
    lastPosUpdate = 0
  end
end
