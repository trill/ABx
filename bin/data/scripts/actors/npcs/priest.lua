--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/chat.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Priest"
level = 20
itemIndex = 2
sex = SEX_MALE
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_MONK
prof2Index = PROFESSIONINDEX_NONE
behavior = "priest"

function onInit()
  self.Species = SPECIES_HUMAN
  self.CombatMode = COMBAT_MODE_AVOID
  local skillBar = self:GetSkillBar()
  skillBar:SetAttributeRank(ATTRIB_HEALING, 12)
  skillBar:SetAttributeRank(ATTRIB_DEVINE_FAVOUR, 12)
  skillBar:AddSkill(5217)
  skillBar:AddSkill(5201)
  skillBar:AddSkill(5212)
  skillBar:AddSkill(5208)
  skillBar:AddSkill(5209)
  skillBar:AddSkill(5210)
  skillBar:AddSkill(5211)
  skillBar:AddSkill(5200)
  return true
end

function onUpdate(timeElapsed)
end

function onClicked(creature)
end

-- self was selected by creature
function onSelected(creature)
end

-- creature collides with self
function onCollide(creature)
end

function onDied(killer)
  if (self:GetDeaths() < 2) then
    self:DropRandomItem()
  end
end
