--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/chat.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")

name = "Dorothea Samara"
level = 20
itemIndex = 12    -- Female Pedestrian 1 body model
sex = SEX_FEMALE
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_MESMER
prof2Index = PROFESSIONINDEX_NONE
behavior = "dorothea_samara"

local startTick

function onInit()
  self.Species = SPECIES_HUMAN
  startTick = Tick()
  self.Speed = 0.5
  self.CombatMode = COMBAT_MODE_FIGHT

  local skillBar = self:GetSkillBar()
  skillBar:SetAttributeRank(ATTRIB_FASTCAST, 10)
  skillBar:SetAttributeRank(ATTRIB_INSPIRATION, 8)
  skillBar:SetAttributeRank(ATTRIB_DOMINATION, 12)

  skillBar:AddSkill(5100)
  skillBar:AddSkill(5101)
  skillBar:AddSkill(5102)
  skillBar:AddSkill(5103)
  skillBar:AddSkill(5104)
  skillBar:AddSkill(5105)
  skillBar:AddSkill(5114)
  skillBar:AddSkill(5120)

  return true
end

function onUpdate(timeElapsed)
  if (Tick() - startTick > 10000 and self.State == CREATURESTATE_IDLE) then
    startTick = Tick()
  end
end

function onArrived()
end

function onClicked(creature)
end

-- self was selected by creature
function onSelected(creature)
end

-- creature collides with self
function onCollide(creature)
end

function onDied(killer)
  if (self:GetDeaths() < 2) then
    self:DropRandomItem()
  end
end

function onResurrected()
end
