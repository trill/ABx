--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Bitterbeetle"
itemIndex = 22
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "ghoul"

local lastPosUpdate = 0
local lastRegenUpdate = 0
local effectTime = 0

function onInit(master, corpse)
  self.Species = SPECIES_UNDEAD
  self.CollisionMask = 0
  self.CollisionLayer = 0
  self.CombatMode = COMBAT_MODE_FIGHT
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetHealthRegen(-1)
  -- Can not resurrect minions
  self.Resurrectable = false
  -- Can not make minions from minions
  self.Recycleable = false
  -- A Minion is not member of the group
  self.GroupId = 0

  if (master ~= nil) then
    self.HomePos = master.Position
    local death = master:GetAttributeRank(ATTRIB_DEATH)
    effectTime = math.floor(death * ((8 - 2) / 12) + 2)
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastRegenUpdate = lastRegenUpdate + timeElapsed
  lastPosUpdate = lastPosUpdate + timeElapsed
  if (lastRegenUpdate >= 20000) then
    self:SetHealthRegen(-1)
    lastRegenUpdate = 0
  end
  local master = self.Master
  if (lastPosUpdate >= 1000) then
    if (master ~= nil) then
      self.HomePos = master.Position
    end
    lastPosUpdate = 0
  end
  if (master ~= nil) then
    if (master:IsDead()) then
      self.Master = nil
    end
  end
end

function onDied(killer)
  self:RemoveIn(2000)
end

function getAttackDamage(critical)
  local level = self:GetLevel()
  -- 50% damage
  local damage = (Random(level / 2, level) / 2)
  if (critical) then
    return math.floor(damage * math.sqrt(2))
  end
  return math.floor(damage)
end

function onSourceAttackSuccess(source, target, type, damage)
  target:AddEffect(self, 10007, effectTime)
end

function getDamageType()
  return DAMAGETYPE_BLUNT
end

function getAttackSpeed()
  return 3100
end

function getArmorEffect(damageType, damagePos, penetration)
  if (damageType == DAMAGETYPE_HOLY) then
    return 2.0
  end
  if (damageType == DAMAGETYPE_FIRE) then
    return 1.3
  end
  return 1.0
end

function getBaseArmor(damageType, damagePos)
  return math.floor(2.9 * self:GetLevel() + 1.25)
end
