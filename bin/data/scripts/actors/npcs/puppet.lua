--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Poppet"
itemIndex = 23
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "minion"

local lastPosUpdate = 0
local lastRegenUpdate = 0
local masterId = 0
local absorb = 0

function onInit(master, corpse)
  local soulreaping = master:GetAttributeRank(ATTRIB_SOUL_REAPING)
  absorb = math.floor(soulreaping * (3) / 12) / 100

  self.Species = SPECIES_UNDEAD
  -- The Viper Egg minion is passive and does not attack foes.
  self.CombatMode = COMBAT_MODE_AVOID
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetHealthRegen(-1)
  -- Can not resurrect minions
  self.Resurrectable = false
  -- Can not make minions from minions
  self.Recycleable = false
  -- A Minion is not member of the group
  self.GroupId = 0

  if (master ~= nil) then
    self.HomePos = master.Position
    masterId = master.Id
  end
  return true
end

function onDied(killer)
  self:RemoveIn(2000)
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastRegenUpdate = lastRegenUpdate + timeElapsed
  lastPosUpdate = lastPosUpdate + timeElapsed
  if (lastRegenUpdate >= 20000) then
    self:SetHealthRegen(-1)
    lastRegenUpdate = 0
  end
  local master = self.Master
  if (lastPosUpdate >= 1000) then
    if (master ~= nil) then
      self.HomePos = master.Position
    end
    lastPosUpdate = 0
  end
  if (master ~= nil) then
    if (master:IsDead()) then
      self.Master = nil
    end
  end
end

function getArmorEffect(damageType, damagePos, penetration)
  if (damageType == DAMAGETYPE_HOLY) then
    return 2.0
  end
  if (damageType == DAMAGETYPE_FIRE) then
    return 1.3
  end
  return 1.0
end

function getBaseArmor(damageType, damagePos)
  return math.floor(2.9 * self:GetLevel() + 1.25)
end

function onMasterGettingDamage(source, skill, type, value)
  if (self:IsDead()) then
    return value
  end
  local d = math.ceil(value * absorb)
  local si = 0
  if (skill ~= nil) then
    si = skill.Index
  end
  self:Damage(source, si, type, d, 0.0)
  return value - d
end
