--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

name = "Donkey"
level = 5
itemIndex = 29
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "pet"
tameable = true

local lastPosUpdate = 0

function onInit(master, corpse)
  self.CombatMode = COMBAT_MODE_GUARD
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self.GroupId = 0

  if (master ~= nil) then
    self.HomePos = master.Position
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastPosUpdate = lastPosUpdate + timeElapsed
  local master = self.Master
  if (lastPosUpdate >= 1000) then
    if (master ~= nil) then
      self.HomePos = master.Position
    end
    lastPosUpdate = 0
  end
end

function onMasterAttackTarget(target)
  if (target == nil) then
    return
  end
  self:Attack(target, false)
end

function getAttackDamage(critical)
  local level = self:GetLevel()
  local damage = Random(level / 2, level)
  if (critical) then
    return math.floor(damage * math.sqrt(2))
  end
  return math.floor(damage)
end

function getDamageType()
  return DAMAGETYPE_BLUNT
end

function getAttackSpeed()
  return 2000
end

function getArmorEffect(damageType, damagePos, penetration)
  return 1.0
end

function getBaseArmor(damageType, damagePos)
  return math.floor(self:GetLevel() * 3 + 20)
end
