--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

-- Poison Dart makes 33 damage and applies Poison effect
function onInit()
  self.Speed = 4.0
  return true
end

function onHitTarget(creature)
  -- TODO: Make some index for this "skill"
  local actor = creature:AsActor()
  if (actor ~= nil) then
    actor:Damage(nil, 0, DAMAGETYPE_PIERCING, 33, 0)
    actor:AddEffect(nil, 10002, 8000)
  end
end
