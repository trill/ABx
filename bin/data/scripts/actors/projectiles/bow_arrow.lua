--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/chat.lua")
include("/scripts/includes/consts.lua")

name = "Arrow"
itemIndex = 5000

function onInit()
  self.Speed = 4.0
  return true
end

function onStart(creature)
  self.State = CREATURESTATE_MOVING
end

function onHitTarget(creature)
  local source = self.Source
  local weapon = source:GetWeapon()
  -- TODO: 
end

