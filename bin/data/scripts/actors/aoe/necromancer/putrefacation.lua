--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

local lifetime = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  self.Range = RANGE_INAREA
  lifetime = math.floor((death * (15 - 5) / 12) + 5)
  self.Lifetime = (lifetime * 1000)
  return true
end

function onTrigger(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsEnemy(actor) and actor.Species ~= SPECIES_SPIRIT and actor.Species ~= SLAVE_KIND_MINION) then
    actor:AddEffect(self.Source, 5339, self:GetRemainingTime())
    if (not actor:HasEffect()) then
      actor:AddEffect(self.Source, 10009, self:GetRemainingTime())
    end
  end
end

function onLeftArea(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsEnemy(actor) and actor.Species ~= SPECIES_SPIRIT and actor.Species ~= SLAVE_KIND_MINION) then
    other:RemoveEffect(5339)
  end
end
