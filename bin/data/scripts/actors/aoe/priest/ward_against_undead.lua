--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Make an item for it
itemIndex = 6001
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

local damage = 0
local lastDamage = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end

  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  damage = math.floor(smiting * ((15 - 5) / 12) + 5)
  local lifetime = math.floor(smiting * (16 - 5) / 12 + 5)
  self.Lifetime = lifetime
  self.Range = RANGE_INAREA

  return true
end

function onUpdate(time)
  lastDamage = lastDamage + time
  if (lastDamage >= 1000) then
    local actors = self:GetActorsInRange(self.Range)
    local source = self.Source
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source) and (actor.SlaveKind == SLAVE_KIND_MINION or actor:IsControllingMinions())) then
        actor:Damage(source, 5258, DAMAGETYPE_HOLY, damage, 0.0)
      end
    end
    lastDamage = 0
  end
end

