--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")

-- https://wiki.guildwars.com/wiki/Spike_Trap

itemIndex = 6000
creatureState = CREATURESTATE_IDLE
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

-- 90 seconds
local lifeTime = 90 * 1000

local damage = 0
local crippledTime = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end
  
  self.Range = RANGE_ADJECENT
  self.Lifetime = lifeTime
  self.Trigger = true
  local attribVal = source:GetAttributeRank(ATTRIB_WILDERNESS_SURVIVAL)
  damage = 10 + (attribVal * 2)
  crippledTime = 3 + math.floor(attribVal * 1.5)
  return true
end

function onTrigger(other)
  if (self.State == CREATURESTATE_IDLE) then
    -- Not triggered yet, so let's trigger now
    self.State = CREATURESTATE_TRIGGERED
    local actors = self:GetActorsInRange(self.Range)
    for i, actor in ipairs(actors) do
      actor:Damage(self.Source, self.Index, DAMAGETYPE_PIERCING, damage)
      actor:KnockDown(self.Source, 0)
      actor:AddEffect(self.Source, 10001, crippledTime)
    end
    self:RemoveIn(2000)
  end
end
