--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")

itemIndex = 6000
effect = SkillEffectDamage
effectTarget = SkillTargetTarget

-- https://wiki.guildwars.com/wiki/Poison_Spout
local damage = 50
local poisoningMS = 12 * 1000
local lastDamage

function onInit()
  self.Range = RANGE_ADJECENT
  self.Lifetime = TIME_FOREVER
  lastDamage = Tick()
  return true
end

function onUpdate(timeElapsed)
  local tick = Tick()
  if (tick - lastDamage > 1000) then
    local actors = self:GetActorsInRange(self.Range)
    for i, actor in ipairs(actors) do
      actor:ApplyDamage(nil, self.Index, DAMAGETYPE_COLD, damage, 0)
      actor:AddEffect(nil, 10002, 12000)
    end
    lastDamage = tick
  end
end
