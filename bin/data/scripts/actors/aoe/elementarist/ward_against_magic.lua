--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Make an item for it
itemIndex = 0
effect = SkillEffectProtect
effectTarget = SkillTargetAoe

local reduce = 0
local lifetime = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end

  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  local factor = (10 - 5) / 12
  reduce = attribVal * factor + 5
  lifetime = attribVal * (17 - 5) / 12 + 5
  self.Lifetime = lifetime
  self.Range = RANGE_INAREA
  self.Trigger = true

  return true
end

function onTrigger(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsAlly(actor) and actor.Species ~= SPECIES_SPIRIT) then
    other:AddEffect(self.Source, 5017, self:GetRemainingTime())
  end
end

function onLeftArea(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsAlly(actor) and actor.Species ~= SPECIES_SPIRIT) then
    other:RemoveEffect(5017)
  end
end
