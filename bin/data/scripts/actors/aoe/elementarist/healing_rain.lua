--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectHeal
effectTarget = SkillTargetAoe

local healing = 0
local lastHealing = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end

  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  healing = math.floor(attribVal * ((30 - 10) / 12) + 10)
  lastHealing = Tick()
  self.Range = RANGE_ADJECENT
  self.Lifetime = 10000
  return true
end

function onUpdate(timeElapsed)
  local tick = Tick()
  if (tick - lastHealing >= 1000) then
    local actors = self:GetActorsInRange(self.Range)
    local source = self.Source
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsAlly(source)) then
        actor:Healing(source, 5057, healing)
      end
    end
    lastHealing = tick
  end
end
