--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Index 6001 is meteor shower, use its own item
itemIndex = 6001
effect = SkillEffectDamage
effectTarget = SkillTargetAoe

local damage = 0
local lastDamage = 0

function onInit()
  local source = self.Source
  if (source == nil) then
    return false
  end

  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  damage = math.floor(attribVal * ((25 - 5) / 12) + 5)
  lastDamage = Tick()
  self.Range = RANGE_ADJECENT
  self.Lifetime = 10000
  return true
end

local function apply(source, actor)
  local effects = actor:GetEffectsOf(EffectCategoryHex)
  local hexCount = 0
  for i, effect in ipairs(effects) do
    local effectSkill = effect:GetCausingSkill()
    if (effectSkill ~= nil) then
      if (effectSkill:GetAttribute() == ATTRIB_WATER) then
        hexCount = hexCount + 1
      end
    end
  end
  if (hexCount > 0) then
    actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
  end
  if (hexCount > 1) then
    actor:Interrupt(source)
  end
end

function onUpdate(timeElapsed)
  local tick = Tick()
  if (tick - lastDamage >= 1000) then
    local actors = self:GetActorsInRange(self.Range)
    local source = self.Source
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source)) then
        apply(source, actor)
      end
    end
    lastDamage = tick
  end
end
