--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")

name = ""
level = 20
itemIndex = 11
creatureState = CREATURESTATE_IDLE
interactionRange = RANGE_ADJECENT

function onInit()
  -- Player collides with BB. Make it a bit larget than the default BB.
  self.NpcType = NPCTYPE_PORTAL
  self:SetBoundingSize({1.0, 2.0, 2.0})
  self.Undestroyable = true
  -- Even when the collision mask is zero, it still calls the trigger even
  self.CollisionMask = 0
  -- Will call onTrigger() when it collides
  self.Trigger = true
  return true
end

local function teleport(creature)
  local player = creature:AsPlayer()
  if (player ~= nil) then
    local party = player.Party
    if (party ~= nil) then
      party:ChangeInstance(self:GetVarString("destination"))
    end
  end
end

function onTrigger(creature)
  teleport(creature)
end
