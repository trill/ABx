--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/dialogs.lua")

name = "Chest"
level = 20
itemIndex = 13
sex = SEX_UNKNOWN
creatureState = CREATURESTATE_CHEST_CLOSED
prof1Index = 0
prof2Index = 0
interactionRange = RANGE_ADJECENT

function onInit()
  self:SetBoundingSize({1.00349, 0.67497, 0.680545})
  self.Undestroyable = true
  self.NpcType = NPCTYPE_ACCOUNT_CHEST
  return true
end

local function openChest(creature)
  if (self:IsInRange(interactionRange, creature)) then
    if (self.State == CREATURESTATE_CHEST_OPEN) then
      self.State = CREATURESTATE_CHEST_CLOSED
    else
      self.State = CREATURESTATE_CHEST_OPEN
    end
    local player = creature:AsPlayer()
    if (player ~= nil) then
      player:TriggerDialog(self.Id, DIALOG_ACCOUNTCHEST)
    end
  end
end

function onClicked(creature)
  openChest(creature)
end

-- creature is interacting with self
function onInteract(creature)
  openChest(creature)
end
