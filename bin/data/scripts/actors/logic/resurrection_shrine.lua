--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")

name = "Resurrection Shrine"
level = 20
-- Res shrine model is part of the scene
itemIndex = 0
creatureState = CREATURESTATE_IDLE

local rezzInterval = 60 * 1000
local actors = {}
local timePassed = 0

function onInit()
  self.NpcType = NPCTYPE_RESURRECTION_SHRINE
  self:SetBoundingSize({8.0, 8.0, 8.0})
  self.Undestroyable = true
  self.Trigger = true
  timePassed = 0
  return true
end

function onTrigger(creature)
  local actor = creature:AsActor()
  if (actor == nil) then
    return
  end
  if (self:IsEnemy(actor) == false) then
    actors[actor.Id] = 1
  end
end

local function doTeleport()
  local pos = self.Position
  for id,value in pairs(actors) do
    if (value == 1) then
      local object = self.Game:GetObject(id)
      if (object ~= nil) then
        local actor = object:AsActor()
        if (actor ~= nil) then
          if (actor:IsDead()) then
            actor.Position = pos
            actor:ForcePositionUpdate()
          end
        end
      end
    end
  end
end

local function doRezz()
  for id,value in pairs(actors) do
    if (value == 1) then
      local object = self.Game:GetObject(id)
      if (object ~= nil) then
        local actor = object:AsActor()
        if (actor ~= nil) then
          if (actor:IsDead()) then
            actor:Resurrect(100, 100)
          end
        end
      end
    end
  end
end

function onUpdate(timeElapsed)
  timePassed = timePassed + timeElapsed
  if (timePassed >= rezzInterval) then
    doTeleport()
    doRezz()
    timePassed = 0
  end
end
