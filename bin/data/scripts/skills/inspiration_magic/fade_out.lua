--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5142
name = "Fade Out"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeSkill
isElite = true
description = "Elite Skill: Lose all conditions, hexes, stances and enchantments. For 3 seconds, effects placed on you expire ${floor((inspiration * (80 - 20) / 12) + 20)}% faster."
shortDescription = "Elite Skill: Lose all conditions, hexes, stances and enchantments. For 3 seconds, effects placed on you expire 20...80...90% faster."
icon = "Textures/Skills/Fade Out.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 0
recharge = 10000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectRemoveCondition | SkillEffectRemoveHex | SkillEffectRemoveStance | SkillEffectRemoveEnchantment
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local  effects = target:GetEffects()
  for i, effect in ipairs(effects) do
    local cat = effect:GetCategory()
    if (cat == EffectCategoryCondition or cat == EffectCategoryEnchantment or cat == EffectCategoryHex or cat == EffectCategoryStance) then
      effect:Remove()
    end
  end
  target:AddEffect(source, self.Index, 3000)

  return SkillErrorNone
end
