--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5147
name = "Pensive Incantation"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, the next spell you cast takes twice as long to cast, but you gain 50...150...160% of its Energy cost as Energy."
shortDescription = "Enchantment Spell: For 30 seconds, the next spell you cast takes twice as long to cast, but you gain 50...150...160% of its Energy cost as Energy."
icon = "Textures/Skills/Pensive Incantation.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 30000)
  return SkillErrorNone
end
