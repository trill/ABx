--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5145
name = "Mantra of Inertia"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeStance
isElite = false
description = "Stance: For ${floor(inspiration * ((25 - 1) / 12) + 1)} seconds, the next ${floor(inspiration * (2 / 12))} times you would be knocked down, you are interrupted and lose ${floor(inspiration * ((5 - 10) / 12) + 10)} Energy instead."
shortDescription = "Stance: For 1...25...30 seconds, the next 0...2...3 times you would be knocked down, you are interrupted and lose 10...5...4 Energy instead."
icon = "Textures/Skills/Mantra of Inertia.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 0
recharge = 45000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectCounterKD
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  duration = math.floor(inspiration * ((25 - 1) / 12) + 1)
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
