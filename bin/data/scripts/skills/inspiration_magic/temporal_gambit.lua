--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5149
name = "Temporal Gambit"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, every time you use a Mesmer interruption spell to successfully interrupt a foe's skill, you gain ${floor((inspiration * 7) / 12)} Energy. Every time you use a Mesmer interruption spell but fail to interrupt a skill, you lose 3 Energy."
shortDescription = "Enchantment Spell: For 30 seconds, every time you use a Mesmer interruption spell to successfully interrupt a foe's skill, you gain 0...7...9 Energy. Every time you use a Mesmer interruption spell but fail to interrupt a skill, you lose 3 Energy."
icon = "Textures/Skills/Temporal Gambit.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetTypeSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end

  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 30000)
  return SkillErrorNone
end
