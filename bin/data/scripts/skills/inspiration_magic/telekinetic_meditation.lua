--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5148
name = "Telekinetic Meditation"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, whenever target ally gains Energy from a skill, they gain an extra +${floor((inspiration * (50 - 10) / 12) + 10)}% extra Energy."
shortDescription = "Enchantment Spell: For 30 seconds, whenever target ally gains Energy from a skill, they gain an extra +10...50...75% extra Energy."
icon = "Textures/Skills/Telekinetic Meditation.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAllyWithoutSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (not self:IsInRange(target)) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end

  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  target:AddEffect(source, self.Index, 30000)
  return SkillErrorNone
end
