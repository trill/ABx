--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5143
name = "Infungibility"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeHex
isElite = false
description = "Hex: For ${floor((inspiration * (12 - 4)) / 12 + 4)} seconds, skills used by target foe which target their allies are cast at -${floor((illusion * (3 - 1)) / 12 + 1)} the attribute level."
shortDescription = "Hex: For 4...12...14 seconds, skills used by target foe which target their allies are cast at -1...3...3 the attribute level."
icon = "Textures/Skills/Infungibility.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 3000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDecAttributes
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end  
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  local duration = (inspiration * (12 - 4)) / 12 + 4
  target:AddEffect(source, self.Index, math.floor(duration * 1000))

  return SkillErrorNone
end
