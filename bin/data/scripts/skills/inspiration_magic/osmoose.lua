--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5146
name = "Osmoose"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: If target foe is casting a spell, that foe is interrupted and you are healed for 40...110...130 Health."
shortDescription = "Spell: If target foe is casting a spell, that foe is interrupted and you are healed for 40...110...130 Health."
icon = "Textures/Skills/Anosogotia.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectInterrupt
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:IsUsingSkillOfType(SkillTypeSpell, 250) == false) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local skill = target:GetCurrentSkill()
  if (skill == nil) then
    -- No skill to interrupt -> no error just nothing happens
    return SkillErrorNone
  end
  if (skill:IsType(SkillTypeSpell)) then
    if (target:InterruptSkill(source, SkillTypeSpell)) then
      local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
      local healing = (inspiration * (110- 40) / 12) + 40
      source:Healing(source, self.Index, healing)
    end
  end

  return SkillErrorNone
end
