--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5141
name = "Ether Banquet"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes in the area lose 3 Energy. For each point of Energy lost, you are healed for ${floor((inspiration * (25 - 5) / 12) + 5)} Health. For each foe affected, you gain 1 Energy."
shortDescription = "Spell: Target foe and all foes in the area lose 3 Energy. For each point of Energy lost, you are healed for 5...25...30 Health. For each foe affected, you gain 1 Energy."
icon = "Textures/Skills/Ether Banquet.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal | SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  local healing = math.floor((inspiration * (25 - 5) / 12) + 5)
  local energyDrained = 0
  local targetCount = 0

  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      local e = actor:DrainEnergy(3)
      if (e ~= 0) then
        energyDrained = energyDrained + e
        targetCount = targetCount + 1
      end
    end
  end
  source:Healing(source, self.Index, healing * energyDrained)
  source:AddEnergy(targetCount)

  return SkillErrorNone
end
