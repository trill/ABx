--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5140
name = "Anosogotia"
attribute = ATTRIB_INSPIRATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: Remove all hexes from yourself. After ${floor((inspiration * (9 - 1) / 12) + 1)} seconds, reapply all those hexes onto yourself for their remaining durations."
shortDescription = "Spell: Remove all hexes from yourself. After 1...9...10 seconds, reapply all those hexes onto yourself for their remaining durations."
icon = "Textures/Skills/Anosogotia.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 25000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectRemoveHex
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  local duration = (inspiration * (9 - 1) / 12) + 1
  source:AddEffect(source, self.Index, math.floor(duration * 1000))
  return SkillErrorNone
end
