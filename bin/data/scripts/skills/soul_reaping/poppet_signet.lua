--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5603
name = "Poppet Signet"
attribute = ATTRIB_SOUL_REAPING
skillType = SkillTypeSignet
isElite = false
description = "Signet: Create a level ${floor(soulreaping * ((12 - 1) / 12) + 1)} Poppet. The Poppet does not attack. Every time you take damage, the Poppet takes 0...3...5% of the damage instead. You can only control 1 Poppet at a time."
shortDescription = "Signet: Create a level 1...12...15 Poppet. The Poppet does not attack. Every time you take damage, the Poppet takes 0...3...5% of the damage instead. You can only control 1 Poppet at a time."
icon = "Textures/Skills/Poppet Signet.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSummonMinion
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local SCRIPT = "/scripts/actors/npcs/puppet.lua"
  local minions = source:GetMinions()
  for i, minion in ipairs(minions) do
    local npc = minion:AsNpc()
    if (npc ~= nil and not npc:IsDead() and npc:GetScriptFile() == SCRIPT) then
      npc:Die(nil)
    end
  end

  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  local level = math.floor(soulreaping * ((12 - 1) / 12) + 1)
  source:SpawnSlave(SCRIPT, nil, SLAVE_KIND_MINION, level, source.Position)
  return SkillErrorNone
end
