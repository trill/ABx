--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5601
name = "Invigorating Hatred"
attribute = ATTRIB_SOUL_REAPING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, you gain ${floor(soulreaping * ((20 - 1) / 12) + 1)} Health every time a creature in earshot dies."
shortDescription = "Enchantment Spell: For 30 seconds, you gain 1...20...25 Health every time a creature in earshot dies."
icon = "Textures/Skills/Invigorating Hatred.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddEffect(source, self.Index, 30000)

  return SkillErrorNone
end
