--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5600
name = "Aura of the Countess"
attribute = ATTRIB_SOUL_REAPING
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${floor(soulreaping * (25 / 12))} seconds, you no longer gain Energy when creatures die. You gain ${floor(soulreaping * ((7 - 1) / 12) + 1)} Energy every time you cast a spell that targets an opponent."
shortDescription = "Elite Enchantment Spell: For 0...25...30 seconds, you no longer gain Energy when creatures die. You gain 1...7...9 Energy every time you cast a spell that targets an opponent."
icon = "Textures/Skills/Aura of the Countess.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  local duration = math.floor(soulreaping * (25 / 12))
  source:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
