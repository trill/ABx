--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5604
name = "Soul Virus"
attribute = ATTRIB_SOUL_REAPING
skillType = SkillTypeHex
isElite = false
description = "Elite Hex Spell: For ${floor(soulreaping * ((6 - 1) / 12) + 1)} seconds, target foe is hexed with Soul Virus. While hexed, conditions and hexes on that foe cannot end or be removed."
shortDescription = "Elite Hex Spell: For 1...6...7 seconds, target foe is hexed with Soul Virus. While hexed, conditions and hexes on that foe cannot end or be removed."
icon = "Textures/Skills/Soul Virus.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 15000
overcast = 0
hp = 33
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  local duration = math.floor(soulreaping * ((6 - 1) / 12) + 1)
  target:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
