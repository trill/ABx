--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5602
name = "Planchette Signet"
attribute = ATTRIB_SOUL_REAPING
skillType = SkillTypeSignet
isElite = false
description = "Signet: If target foe dies within 10 seconds, you gain ${floor(soulreaping * ((50 - 20) / 12) + 20)} Health and ${floor(soulreaping * ((5 - 1) / 12) + 1)} Energy."
shortDescription = "Signet: If target foe dies within 10 seconds, you gain 20...50...70 Health and 1...5...9 Energy."
icon = "Textures/Skills/Planchette Signet.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal | SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  target:AddEffect(source, self.Index, 10000)

  return SkillErrorNone
end
