--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5002
name = "Electric Loop"
attribute = ATTRIB_AIR
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: You are enchanted with Electric Loop for 18 seconds. Every second, all foes in the area around you, but not adjacent to you, are struck for ${floor(air * ((12 - 1) / 12) + 1)} lightning damage. This spell has 25% armour penetration."
shortDescription = "Enchantment Spell: You are enchanted with Electric Loop for 18 seconds. Every second, all foes in the area around you, but not adjacent to you, are struck for 1...12...16 lightning damage. This spell has 25% armour penetration."
icon = "Textures/Skills/Electric Loop.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_LIGHTNING
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (not source:IsGroupFighting()) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 18000)
  return SkillErrorNone
end
