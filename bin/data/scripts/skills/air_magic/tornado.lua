--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5006
name = "Tornado"
attribute = ATTRIB_AIR
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes in the area are knocked down."
shortDescription = "Spell: Target foe and all foes in the area are knocked down."
icon = "Textures/Skills/Tornado.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 25000
overcast = 10
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_LIGHTNING
effect = SkillEffectKnockDown
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  target:KnockDown(source, 2000)
  local actors = target:GetEnemiesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:KnockDown(source, 2000)
    end
  end
  return SkillErrorNone
end
