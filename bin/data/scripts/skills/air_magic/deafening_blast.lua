--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5001
name = "Deafening Blast"
attribute = ATTRIB_AIR
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe is struck for ${floor(air * ((66 - 16) / 12) + 16)} cold damage. If you are overcast, that foe loses the effects of all shouts. This spell has 25% armour penetration."
shortDescription = "Spell: Target foe is struck for 16...66...78 cold damage. If you are overcast, that foe loses the effects of all shouts. This spell has 25% armour penetration."
icon = "Textures/Skills/Deafening Blast.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

-- Air Magic   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Damage     16    .    .    .    .    .    .    .    .    .    .    .   66    .    .   70    .    .    .    .    .    .

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_LIGHTNING
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local attrib = source:GetAttributeRank(ATTRIB_AIR)
  local dmgfactor = (66 - 16) / 12
  local damage = dmgfactor * attrib + 16

  target:Damage(source, self.Index, damageType, damage, 0.25)
  local overcast = source:GetResource(RESOURCE_TYPE_OVERCAST)
  if (overcast ~= 0) then
    target:RemoveAllEffectsOf(EffectCategoryShout)
  end
  return SkillErrorNone
end
