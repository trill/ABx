--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5000
name = "Bluster"
attribute = ATTRIB_AIR
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: Target foe takes ${floor(air * ((70 - 20) / 12) + 20)} cold damage. For ${floor(air * ((6 - 2) / 12) + 2)} seconds, that foe attacks 25% slower."
shortDescription = "Hex Spell: Target foe takes 20...70...82 cold damage. For 2...6...7 seconds, that foe attacks 25% slower."
icon = "Textures/Skills/Bluster.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

-- Air Magic   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Damage     20    .    .    .    .    .    .    .    .    .    .    .   70    .    .   82    .    .    .    .    .    .
-- Snare       2    .    .    .    .    .    .    .    .    .    .    .    6    .    .    7    .    .    .    .    .    .

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 8000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage | SkillEffectSnare
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local attrib = source:GetAttributeRank(ATTRIB_AIR)
  local dmgfactor = (70 - 20) / 12
  local snarefactor = (6 - 2) / 12
  local damage = math.floor(dmgfactor * attrib + 20)
  local snare = math.floor(snarefactor * attrib + 2)

  target:Damage(source, self.Index, damageType, damage, 0)
  target:AddEffect(source, self.Index, snare * 1000)

  return SkillErrorNone
end
