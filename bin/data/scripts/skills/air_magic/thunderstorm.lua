--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5005
name = "Thunderstorm"
attribute = ATTRIB_AIR
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: For 10 seconds, create a Thunderstorm at target foe's location. Each second, a foe in the area is struck for ${floor(air * (((70 - 15) / 12)) + 15)} lightning damage and is interrupted. The same foe cannot be struck twice in a row. This spell has 25% armour penetration."
shortDescription = "Elite Spell: For 10 seconds, create a Thunderstorm at target foe's location. Each second, a foe in the area is struck for 15...70...80 lightning damage and is interrupted. The same foe cannot be struck twice in a row. This spell has 25% armour penetration."
icon = "Textures/Skills/Thunderstorm.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_LIGHTNING
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  position = target.Position
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddAOE("/scripts/actors/aoe/elementarist/thunderstorm.lua", self.Index, position)
  return SkillErrorNone
end
