--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

-- For DB import ---------------------------------------------------------------
index = 5004
name = "Stormseeker"
attribute = ATTRIB_AIR
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 45 seconds, Cold and Air damage you deal gain an additional ${floor(air * (5 / 12))}% armour penetration."
shortDescription = "Enchantment Spell: For 45 seconds, Cold and Air damage you deal gain an additional 0...5...6% armour penetration."
icon = "Textures/Skills/Stormseeker.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""
--/ For DB import --------------------------------------------------------------

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 45000)
  return SkillErrorNone
end
