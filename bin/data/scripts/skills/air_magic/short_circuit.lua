--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5003
name = "Short Circuit"
attribute = ATTRIB_AIR
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all adjacent foes are struck for ${floor(air * ((55 - 14) / 12) + 14)} lightning damage. If you are overcast, foes struck by Static Shock have their signets disabled for ${floor(air * ((3 - 1) / 12) + 1)} seconds. This spell has 25% armour penetration."
shortDescription = "Spell: Target foe and all adjacent foes are struck for 14...55...70 lightning damage. If you are overcast, foes struck by Static Shock have their signets disabled for 1...3...4 seconds. This spell has 25% armour penetration."
icon = "Textures/Skills/Short Circuit.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_LIGHTNING
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local damage = 0
local disable = 0

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end

  local attrib = source:GetAttributeRank(ATTRIB_AIR)
  local dmgfactor = (55 - 14) / 12
  damage = math.floor(attrib * dmgfactor + 14)
  disable = math.floor(attrib * (((3 - 1) / 12) + 1))
  source:FaceObject(target)
  return SkillErrorNone
end

local function apply(source, target)
  target:Damage(source, self.Index, DAMAGETYPE_LIGHTNING, damage, 0.25)
  local sb = target:GetSkillBar()
  for i = 0, MAX_SKILLS - 1, 1 do
    local skill = sb:GetSkill(i)
    if (skill ~= nil) then
      if (skill:IsType(SkillTypeSignet)) then
        skill:AddRecharge(disable * 1000)
      end
    end
  end
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  apply(source, target)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      apply(source, actor)
    end
  end

  return SkillErrorNone
end
