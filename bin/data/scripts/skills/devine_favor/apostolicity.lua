--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5200
name = "Apostolicity"
attribute = ATTRIB_DEVINE_FAVOUR
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor(devinefavour * (26 / 12))} seconds, your monk spells cast on allies remove a condition. Each time a condition is removed in this way, you lose 1 Energy or Apostolicity ends."
shortDescription = "Enchantment Spell: For 0...26...30 seconds, your monk spells cast on allies remove a condition. Each time a condition is removed in this way, you lose 1 Energy or Apostolicity ends."
icon = "Textures/Skills/Apostolicity.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 45000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectRemoveCondition
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

local time = 0

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (not source:IsGroupFighting()) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DEVINE_FAVOUR)
  time = math.floor(attrib * (26 / 12))
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, time * 1000)
  return SkillErrorNone
end
