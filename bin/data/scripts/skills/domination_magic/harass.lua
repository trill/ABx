--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5103
name = "Harass"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeHex
isElite = false
description = "Hex: Target foe is hexed with Harass for ${floor(domination * ((6 - 2) / 12) + 2)} seconds. The next time they use a skill, a different random skill is disabled for ${floor(domination * ((18 - 5) / 12) + 5)} seconds."
shortDescription = "Hex: Target foe is hexed with Harass for 2...6...7 seconds. The next time they use a skill, a different random skill is disabled for 5...18...20 seconds."
icon = "Textures/Skills/Harass.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

-- Domination  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Duration    2    .    .    .    .    .    .    .    .    .    .    .    6    .    .    7    .    .    .    .    .    .
-- TTime       5    .    .    .    .    .    .    .    .    .    .    .   18    .    .   20    .    .    .    .    .    .

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local duration = math.floor(domination * ((6 - 2) / 12) + 2)
  target:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
