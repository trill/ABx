--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5116
name = "Intimidate"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeHex
isElite = true
description = "Elite Hex: For 10 seconds, target can not use interrupt skills with ${floor(max((domination * 100) / 12, 100))}% chance."
shortDescription = "Elite Hex: For 10 seconds, target can not use interrupt skills with 0..100..100% chance."
icon = "Textures/Skills/Intimidate.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 2000
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_NONE
effect = SkillEffectCounterInterrupt
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  local sb = target:GetSkillBar()
  local interruptSkills = sb:GetSkillsWithEffect(SkillEffectInterrupt)
  if (#interruptSkills == 0) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  target:AddEffect(source, self.Index, 10000)

  return SkillErrorNone
end
