--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5102
name = "Chaos Ring"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: All foes adjacent to target ally lose a hex. Each foe that loses a hex in this way takes ${floor(domination * ((110 - 30) / 12) + 30)} damage and loses ${floor(domination * ((9 - 3) / 12) + 3)} Energy."
shortDescription = "Spell: All foes adjacent to target ally lose a hex. Each foe that loses a hex in this way takes 30...110...130 damage and loses 3...9...12 Energy."
icon = "Textures/Skills/Chaos Ring.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

-- Domination  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Damage     30    .    .    .    .    .    .    .    .    .    .    .  110    .    .  130    .    .    .    .    .    .
-- Energy      3    .    .    .    .    .    .    .    .    .    .    .    9    .    .   12    .    .    .    .    .    .

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local damage = 0
local energy = 0

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (not target:HasEffectOf(EffectCategoryHex)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone and err ~= SkillErrorNotAppropriate) then
    return err
  end

  source:FaceObject(target)
  local attrib = source:GetAttributeRank(ATTRIB_DOMINATION)
  local dmgfactor = (110 - 30) / 12
  damage = attrib * dmgfactor + 30
  local efactor = (9 - 3) / 12
  energy = attrib * efactor + 3
  return SkillErrorNone
end

local function apply(source, target)
  local effect = target:GetLastEffect(EffectCategoryHex)
  if (effect ~= nil) then
    target:Damage(source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
    target:DrainEnergy(energy)
  end
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  apply(source, target)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      apply(source, actor)
    end
  end

  return SkillErrorNone
end
