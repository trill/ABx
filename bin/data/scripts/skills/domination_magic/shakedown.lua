--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5111
name = "Shakedown"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeSkill
isElite = false
description = "Touch Skill: If target foe is casting a spell, that foe is interrupted and all of the spells associated with that spell's attribute are disabled for ${floor((domination * 1) / 12)} seconds."
shortDescription = "Touch Skill: If target foe is casting a spell, that foe is interrupted and all of the spells associated with that spell's attribute are disabled for 0...1...2 seconds."
icon = "Textures/Skills/Shakedown.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 250
recharge = 25000
overcast = 0
hp = 0
range = RANGE_TOUCH
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDisableSkill
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local sb = target:GetSkillBar()
  if (not sb:HasSkillsOfType(SkillTypeSpell, false)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local disableTime = math.floor((domination * (6 - 1)) / 12 + 1)
  local sb = target:GetSkillBar()
  local skills = sb:GetSkills();
  local randomSkill = skills[math.random(#skills)]
  randomSkill:AddRecharge(disableTime * 1000)

  return SkillErrorNone
end
