--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5104
name = "Indiscrimination"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeHex
isElite = false
description = "Hex: For ${floor(domination * ((10 - 3) / 12) + 3)} seconds, the next time target foe successfully casts a spell, that foe and all adjacent foes take ${floor(domination * ((70 - 5) / 12) + 5)} damage."
shortDescription = "Hex: For 3...10...12 seconds, the next time target foe successfully casts a spell, that foe and all adjacent foes take 5...70...80 damage."
icon = "Textures/Skills/Indiscrimination.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

-- Domination  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Damage      5    .    .    .    .    .    .    .    .    .    .    .   70    .    .   80   .    .    .    .    .    .
-- Time        3    .    .    .    .    .    .    .    .    .    .    .   10    .    .   12    .    .    .    .    .    .

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  local sb = target:GetSkillBar()
  if (not sb:HasSkillsOfType(SkillTypeSpell, false)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local duration = math.floor(domination * ((10 - 3) / 12) + 3)
  target:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
