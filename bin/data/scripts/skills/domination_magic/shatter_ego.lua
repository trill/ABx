--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5112
name = "Shatter Ego"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor(domination * ((9 - 1) / 12) + 1)} seconds, the next ${floor(domination * ((3 - 1) / 12) + 1)} times target foe takes damage, that foe is interrupted."
shortDescription = "Hex Spell: For 1...9...10 seconds, the next 1...3...4 times target foe takes damage, that foe is interrupted."
icon = "Textures/Skills/Shatter Ego.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectInterrupt
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local duration = math.floor(domination * ((9 - 1) / 12) + 1)
  target:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
