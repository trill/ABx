--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5109
name = "Power Plug"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: If target foe is casting a spell, that foe is interrupted and all of the spells associated with that spell's attribute are disabled for ${floor((domination * 1) / 12)} seconds."
shortDescription = "Spell: If target foe is casting a spell, that foe is interrupted and all of the spells associated with that spell's attribute are disabled for 0...1...2 seconds."
icon = "Textures/Skills/Power Plug.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 250
recharge = 25000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectInterrupt | SkillEffectDisableSkill
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone and err ~= SkillErrorNotAppropriate) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local disableTime = math.floor((domination * 1) / 12)
  local succes = target:InterruptSkill(source, SkillTypeSpell)
  if (success) then
    local currentSkill = target:GetCurrentSkill()
    if (currentSkill) then
      local sb = target:GetSkillBar()
      local skills = sb:GetSkills();
      for i, _skill in ipairs(skills) do
        if (_skill:GetAttribute() == currentSkill:GetAttribute()) then
          _skill:AddRecharge(disableTime * 1000)
        end
      end
    end
  end

  return SkillErrorNone
end
