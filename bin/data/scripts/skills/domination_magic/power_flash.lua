--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5115
name = "Power Flash"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeSpell
isElite = false
description = "Spell: If target foe is casting a spell, that spell is interrupted and target takes ${floor(((domination * (4 - 1)) / 12 + 1))} damage per energy point of that target."
shortDescription = "Spell: If target foe is casting a spell, that spell is interrupted and target takes 1...4...5 damage per energy point of that target."
icon = "Textures/Skills/Power Flash.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectInterrupt | SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  local damagePerEnergy = math.floor(((domination * (4 - 1)) / 12) + 1)
  local success = target:InterruptSkill(source, SkillTypeSpell)
  if (success) then
    local energy = target:GetResource(RESOURCE_TYPE_ENERGY)
    target:Damage(source, self.Index, damageType, energy * damagePerEnergy, 0.0)
  end

  return SkillErrorNone
end
