--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5121
name = "Mantra of Indignation"
attribute = ATTRIB_FASTCAST
skillType = SkillTypeStance
isElite = false
description = "Stance: For ${floor(fastcast * ((50 - 10) / 12) + 10)} seconds, every time you are interrupted, the source of the interruption has all of their skills disabled for 2 seconds."
shortDescription = "Stance: For 10...50...55 seconds, every time you are interrupted, the source of the interruption has all of their skills disabled for 2 seconds."
icon = "Textures/Skills/Mantra of Indignation.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 0
recharge = 60000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDisableSkill
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  local fastcast = source:GetAttributeRank(ATTRIB_FASTCAST)
  duration = math.floor(fastcast * ((50 - 10) / 12) + 10)
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
