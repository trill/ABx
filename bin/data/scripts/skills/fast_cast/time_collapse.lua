--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5123
name = "Time Collapse"
attribute = ATTRIB_FASTCAST
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes in the area are forcibly Shadow Stepped to a random location in the area. (100% chance of failure with Fast Casting 8 or less)."
shortDescription = "Spell: Target foe and all foes in the area are forcibly Shadow Stepped to a random location in the area. (100% chance of failure with Fast Casting 8 or less)."
icon = "Textures/Skills/Time Collapse.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = 0
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

local function apply(actor)
  local maxDist = 19.0    -- ~ Aggro range
  local pos = actor.Position
  pos[1] = pos[1] + Random(-maxDist, maxDist)
  pos[3] = pos[3] + Random(-maxDist, maxDist)
  return actor:ShadowStep(pos)
end

function onSuccess(source, target)
  local fastcast = source:GetAttributeRank(ATTRIB_FASTCAST)
  if (fastcast < 8) then
    return SkillErrorFailed
  end

  apply(target)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      apply(actor)
    end
  end
  return SkillErrorNone
end
