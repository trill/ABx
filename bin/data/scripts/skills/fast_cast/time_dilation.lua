--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5124
name = "Time Dilation"
attribute = ATTRIB_FASTCAST
skillType = SkillTypeStance
isElite = false
description = "Stance: For ${floor(fastcast * (10 / 12))} seconds, spells others cast on you take twice as long to activate."
shortDescription = "Stance: For 0...10...13 seconds, spells others cast on you take twice as long to activate."
icon = "Textures/Skills/Time Dilation.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 0
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = 0
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  local fastcast = source:GetAttributeRank(ATTRIB_FASTCAST)
  duration = math.floor(fastcast * (10 / 12))
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
