--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5122
name = "Signet of Superiority"
attribute = ATTRIB_FASTCAST
skillType = SkillTypeStance
isElite = false
description = "Signet: For 20 seconds, your mesmer spells activate 50% more slowly but are cast at +${floor(fastcast * (2 / 12))} attribute."
shortDescription = "Signet: For 20 seconds, your mesmer spells activate 50% more slowly but are cast at +0...2...3 attribute."
icon = "Textures/Skills/Signet of Superiority.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectIncAttributes
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 20000)
  return SkillErrorNone
end
