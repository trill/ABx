--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5400
name = "Implant Parasite"
attribute = ATTRIB_BLOOD
skillType = SkillTypeHex
isElite = false
description = "Touch Hex Spell: For 10 seconds, foe has -${floor(blood * ((4 - 1) / 12) + 1)} health regeneration, you have +${floor(blood * ((4 - 1) / 12) + 1)} health regeneration."
shortDescription = "Touch Hex Spell: For 10 seconds, foe has -1...4 health regeneration, you have +1...4 health regeneration."
icon = "Textures/Skills/Implant Parasite.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 500
recharge = 10000
overcast = 0
hp = 0

range = RANGE_TOUCH
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  target:AddEffect(source, self.Index, 10000)

  return SkillErrorNone
end
