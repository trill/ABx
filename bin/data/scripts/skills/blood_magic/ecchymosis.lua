--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5402
name = "Ecchymosis"
attribute = ATTRIB_BLOOD
skillType = SkillTypeSpell
isElite = false
description = "Spell: Deal ${floor(blood * ((70 - 15) / 12) + 15)} cold damage to target foe. This spell recharges twice as quickly if that foe is bleeding or has the deep wound condition."
shortDescription = "Spell: Deal 15...70...80 cold damage to target foe. This spell recharges twice as quickly if that foe is bleeding or has the deep wound condition."
icon = "Textures/Skills/Ecchymosis.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

local faster = false

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  local damage = math.floor(blood * ((70 - 15) / 12) + 15)
  target:Damage(source, self.Index, damageType, damage, 0.0)
  if (target:HasEffect(10006) or target:HasEffect(10008)) then
    faster = true
  end

  return SkillErrorNone
end

function getRecharge(value)
  if (faster) then
    return math.floor(value * 0.5)
  end
  return value
end
