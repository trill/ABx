--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5406
name = "Succubus Kiss"
attribute = ATTRIB_BLOOD
skillType = SkillTypeSkill
isElite = false
description = "Touch Skill: Steal ${floor(blood * ((70 - 15) / 12) + 15)} Health and ${floor(blood * ((5 - 1) / 12) + 1)} Energy from target touched foe."
shortDescription = "Touch Skill: Steal 15...70...75 Health and 1...5...6 Energy from target touched foe."
icon = "Textures/Skills/Succubus Kiss.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 750
recharge = 10000
overcast = 0
hp = 0

range = RANGE_TOUCH
damageType = DAMAGETYPE_LIFEDRAIN
effect = SkillEffectGainEnergy | SkillEffectDamage | SkillEffectHeal
effectTarget = SkillTargetSelf | SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  local energy = math.floor(blood * ((5 - 1) / 12) + 1)
  local life = math.floor(blood * ((70 - 15) / 12) + 15)
  local de = target:DrainEnergy(energy)
  source:AddEnergy(de)
  local dl = target:DrainLife(source, self.Index, life)
  source:AddLife(dl)

  return SkillErrorNone
end

