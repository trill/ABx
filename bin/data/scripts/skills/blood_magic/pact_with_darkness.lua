--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5405
name = "Pact With Darkness"
attribute = ATTRIB_BLOOD
skillType = SkillTypeSpell
isElite = false
description = "Spell: Gain ${floor(blood * ((8 - 1) / 12) + 1)} Energy."
shortDescription = "Spell: Gain 1...8...10 Energy."
icon = "Textures/Skills/Pact With Darkness.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 1
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0
hp = 20

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  local energy = math.floor(blood * ((8 - 1) / 12) + 1)
  source:AddEnergy(energy)

  return SkillErrorNone
end

