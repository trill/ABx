--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5401
name = "Demonic Contract"
attribute = ATTRIB_BLOOD
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor(blood * ((10 - 5) / 12) + 5)} seconds, you gain +1 Energy regeneration. If an enemy dies, Demonic Contract ends and you gain 5 Energy. If Demonic Contract does not end early in this way, you lose 20% of your maximum Health."
shortDescription = "Enchantment Spell: For 5...10...12 seconds, you gain +1 Energy regeneration. If an enemy dies, Demonic Contract ends and you gain 5 Energy. If Demonic Contract does not end early in this way, you lose 20% of your maximum Health."
icon = "Textures/Skills/Demonic Contract.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  local duration = math.floor(blood * ((10 - 5) / 12) + 5)
  source:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
