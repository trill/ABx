--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5407
name = "Viper Eyes"
attribute = ATTRIB_BLOOD
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${floor(blood * ((50 - 5) / 12) + 5)} seconds, you can no longer deal damage, but your life stealing effects steal an extra ${floor(blood * ((150 - 75) / 12) + 75)}% Health."
shortDescription = "Elite Enchantment Spell: For 5...50...70 seconds, you can no longer deal damage, but your life stealing effects steal an extra 75...150...175% Health."
icon = "Textures/Skills/Viper Eyes.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 250
recharge = 12000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  local duration = math.floor(blood * ((50 - 5) / 12) + 5)
  source:AddEffect(source, self.Index, duration)
  return SkillErrorNone
end
