--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5403
name = "Mucronate Shell"
attribute = ATTRIB_BLOOD
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 40 seconds, you gain +0...5...6 armour. You gain an additional +0...5...6 armour every time a non-spirit creature in the area dies (max: +50 armour). Recasting this enchantment resets the bonus back to its initial value."
shortDescription = "Enchantment Spell: For 40 seconds, you gain +0...5...6 armour. You gain an additional +0...5...6 armour every time a non-spirit creature in the area dies (max: +50 armour). Recasting this enchantment resets the bonus back to its initial value."
icon = "Textures/Skills/Mucronate Shell.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 45000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddEffect(source, self.Index, 40000)

  return SkillErrorNone
end
