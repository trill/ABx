--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5404
name = "Order of the Wasp"
attribute = ATTRIB_BLOOD
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For 6 seconds, all party members steal 1 Energy when they successfully hit with an attack. (50% chance of failure with Blood Magic 4 or less.)"
shortDescription = "Elite Enchantment Spell: For 6 seconds, all party members steal 1 Energy when they successfully hit with an attack. (50% chance of failure with Blood Magic 4 or less.)"
icon = "Textures/Skills/Order of the Wasp.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 0
overcast = 0
hp = 10

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetParty
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  if (blood <= 4) then
    if (Random() < 0.5) then
      return SkillErrorFailed
    end
  end

  local group = source.Group
  if (grooup ~= nil) then
    local actors = group:GetMembers()
    for i, actor in ipairs(actors) do
      if (not actor:IsDead()) then
        actor:AddEffect(source, self.Index, 6000)
      end
    end
  else
    source:AddEffect(source, self.Index, 6000)
  end

  return SkillErrorNone
end
