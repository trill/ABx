--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5161
name = "Power Fizzle"
attribute = ATTRIB_NONE
skillType = SkillTypeSpell
isElite = false
description = "Spell. If target foe is casting a spell, interrupt that spell and remove an enchantment from that foe."
shortDescription = "Spell. If target foe is casting a spell, interrupt that spell and remove an enchantment from that foe."
icon = "Textures/Skills/Power Fizzle.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectInterrupt
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (not target:IsUsingSkillOfType(SkillTypeSpell, 250)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local skill = target:GetCurrentSkill()
  if (skill == nil) then
    -- No skill to interrupt -> no error just nothing happens
    return SkillErrorNone
  end

  if (skill:IsType(SkillTypeSpell)) then
    if (target:InterruptSkill(source, SkillTypeSpell)) then
      target:RemoveLastEffectOf(EffectCategoryEnchantment)
    end
  end
  return SkillErrorNone
end
