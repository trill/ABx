--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5513
name = "Well of Wickedness"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = false
description = "Spell: Exploit the nearest corpse to create a Well of Wickedness at its location. For ${floor((curses * (6 - 1) / 12) + 1)} seconds, foes within the well cannot cast spells that target themselves or their allies."
shortDescription = "Spell: Exploit the nearest corpse to create a Well of Wickedness at its location. For 1...6...8 seconds, foes within the well cannot cast spells that target themselves or their allies."
icon = "Textures/Skills/Well of Wickedness.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/necromancer/well_of_wickedness.lua", self.Index, actor.Position)
  actor.Recycleable = false
  source:ExploitedCorpse(actor)

  return SkillErrorNone
end
