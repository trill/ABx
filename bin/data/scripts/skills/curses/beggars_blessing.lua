--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5500
name = "Beggar's Blessing"
attribute = ATTRIB_CURSES
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor(curses * ((20 - 5) / 12) + 5)} seconds, every time target foe either heals or is healed, you are healed for the same amount. This hex ends after you receive ${floor(curses * ((300 - 100) / 12) + 100)} Health in this way."
shortDescription = "Hex Spell: For 5...20...25 seconds, every time target foe either heals or is healed, you are healed for the same amount. This hex ends after you receive 100...300...350 Health in this way."
icon = "Textures/Skills/Beggar's Blessing.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local duration = math.floor(curses * ((20 - 5) / 12) + 5)
  target:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
