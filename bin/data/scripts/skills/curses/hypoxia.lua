--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5507
name = "Hypoxia"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes in the area take ${floor(curses * ((50 - 10) / 12) + 10)} shadow damage. Foes suffering from dazed are struck twice. Foes suffering from both dazed and disease are knocked down."
shortDescription = "Spell: Target foe and all foes in the area take 10...50...60 shadow damage. Foes suffering from dazed are struck twice. Foes suffering from both dazed and disease are knocked down."
icon = "Textures/Skills/Hypoxia.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_SHADOW
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

local function apply(source, target, damage)
  target:Damage(source, self.Index, damageType, damage, 0.0)
  if (target:HasEffect(10003)) then
    target:Damage(source, self.Index, damageType, damage, 0.0)
    if (target:HasEffect(10009)) then
      target:KnockDown(source, 0)
    end
  end
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local damage = math.floor(curses * ((50 - 10) / 12) + 10)

  apply(source, target, damage)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      apply(source, actor, damage)
    end
  end

  return SkillErrorNone
end
