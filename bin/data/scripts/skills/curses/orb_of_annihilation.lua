--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5509
name = "Orb of Annihilation"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = true
description = "TODO: Elite Spell: Create a slow-moving projectile which moves towards target foe and stays in place for 10 seconds. Every second, foes adjacent to the Orb lose ${floor(curses * ((23 - 2) / 12) + 2)} Health and suffer from Weakness and Poison for ${floor((curses * 10) / 12)} seconds."
shortDescription = "TODO: Elite Spell: Create a slow-moving projectile which moves towards target foe and stays in place for 10 seconds. Every second, foes adjacent to the Orb lose 2...23...25 Health and suffer from Weakness and Poison for 0...10...11 seconds."
icon = "Textures/Skills/Orb of Annihilation.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_SHADOW
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

local function apply(source, target)
  target:Damage(source, self.Index, damageType, damage, 0.0)
  if (target:HasEffect(10003)) then
    target:Damage(source, self.Index, damageType, damage, 0.0)
    if (target:HasEffect(10009)) then
      target:KnockDown(source, 0)
    end
  end
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  -- TODO:
  return SkillErrorNone
end
