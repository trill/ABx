--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5512
name = "Torturous Extraction"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = false
description = "Spell: Remove all conditions from target foe. For each one removed, that foe takes ${floor(curses * ((40 - 18) / 12) + 18)} damage (Maximum: 150)."
shortDescription = "Spell: Remove all conditions from target foe. For each one removed, that foe takes 18...40...50 damage (Maximum: 150)."
icon = "Textures/Skills/Torturous Extraction.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_SHADOW
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (not target:HasEffectOf(EffectCategoryCondition)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local damageMul = math.floor(curses * ((40 - 18) / 12) + 18)
  local conds = target:RemoveAllEffectsOf(EffectCategoryCondition)
  local damage = math.floor(conds * damageMul)
  if (damage > 150) then
    damage = 150
  end
  target:Damage(source, self.Index, damageType, damage, 0.0)

  return SkillErrorNone
end
