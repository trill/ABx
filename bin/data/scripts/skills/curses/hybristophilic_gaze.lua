--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5506
name = "Hybristophilic Gaze"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Both you and target foe are diseased for ${floor((curses * 5) / 12)} seconds."
shortDescription = "Elite Spell: Both you and target foe are diseased for 0...5...10 seconds."
icon = "Textures/Skills/Hybristophilic Gaze.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 1
costAdrenaline = 0
activation = 250
recharge = 3000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(10009)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local duration = math.floor((curses * 5) / 12)
  source:AddEffect(source, 10009, duration)
  target:AddEffect(source, 10009, duration)

  return SkillErrorNone
end
