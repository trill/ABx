--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5505
name = "Hell Frozen Rain"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Every second for 5 seconds, target foe and all foes in the area are hexed with Hell Frozen Rain for 10 seconds. This hex causes -${floor(curses / 12)} Health degeneration and -${floor((curses * 2) / 12)} armour penalty cumulatively for every second Hell Frozen Rain has been in effect."
shortDescription = "Elite Spell: Every second for 5 seconds, target foe and all foes in the area are hexed with Hell Frozen Rain for 10 seconds. This hex causes -0...1...2 Health degeneration and -0...2...3 armour penalty cumulatively for every second Hell Frozen Rain has been in effect."
icon = "Textures/Skills/Hell Frozen Rain.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/necromancer/hell_frozen_rain.lua", self.Index, target.Position)

  return SkillErrorNone
end
