--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5502
name = "Cachexia Signet"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = false
description = "Signet: If target foe is not suffering from Weakness, inflict Weakness for ${floor(((curses * (10 - 1)) / 12) + 1)} seconds. Otherwise, target foe loses ${floor(((curses * (7 - 1)) / 12) + 1)} Energy."
shortDescription = "Signet: If target foe is not suffering from Weakness, inflict Weakness for 1...10...12 seconds. Otherwise, target foe loses 1...7...10 Energy."
icon = "Textures/Skills/Cachexia Signet.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_DARK
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local weakness = math.floor(((curses * (10 - 1)) / 12) + 1)
  local energy = math.floor(((curses * (7 - 1)) / 12) + 1)
  if (not target:HasEffect(10010)) then
    target:AddEffect(source, 10010, weakness)
  else
    target:DrainEnergy(energy)
  end

  return SkillErrorNone
end
