--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5501
name = "Bolt of Darkness"
attribute = ATTRIB_CURSES
skillType = SkillTypeSpell
isElite = false
description = "Spell: Strike target foe for ${floor(((curses * (70 - 8)) / 12) + 8)} dark damage. If that foe is suffering from a Necromancer hex, remove an enchantment."
shortDescription = "Spell: Strike target foe for 8...70...80 dark damage. If that foe is suffering from a Necromancer hex, remove an enchantment."
icon = "Textures/Skills/Bolt of Darkness.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_DARK
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  local damage = math.floor(((curses * (70 - 8)) / 12) + 8)
  target:Damage(source, self.Index, damageType, damage, 0.0)
  local effects = target:GetEffectsOf(EffectCategoryHex)
  for i, effect in ipairs(effects) do
    local skill = effect:GetCausingSkill()
    if (skill ~= nil) then
      if (skill:GetProfession() == PROFESSIONINDEX_NECROMANCER) then
        target:RemoveLastEffectOf(EffectCategoryEnchantment)
        break
      end
    end
  end

  return SkillErrorNone
end
