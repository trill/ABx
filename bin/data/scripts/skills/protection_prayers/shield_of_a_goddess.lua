--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5244
name = "Shield of a Goddess"
attribute = ATTRIB_PROTECTION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: Target ally takes ${floor((protection * (20 - 10) / 12) + 10)}% less damage from spells for ${floor((protection * (12 - 3) / 12) + 3)} seconds."
shortDescription = "Enchantment Spell: Target ally takes 10...20...25% less damage from spells for 3...12...13 seconds."
icon = "Textures/Skills/Shield of a Goddess.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTypeAllyAndSelf
targetType = SkillTargetTarget

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  local duration = math.floor((protection * (12 - 3) / 12) + 3)
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
