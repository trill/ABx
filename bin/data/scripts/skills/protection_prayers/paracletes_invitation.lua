--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5243
name = "Paraclete's Invitation"
attribute = ATTRIB_PROTECTION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor((protection * (5 - 3) / 12) + 3)} seconds, target other ally cannot be damaged for more than 7% of their total Health. If this enchantment is not removed prematurely, that ally is healed for ${floor((protection * (85 - 20) / 12) + 20)} Health once it ends. This enchantment cannot target already enchanted allies."
shortDescription = "Enchantment Spell: For 3...5...7 seconds, target other ally cannot be damaged for more than 7% of their total Health. If this enchantment is not removed prematurely, that ally is healed for 20...85...100 Health once it ends. This enchantment cannot target already enchanted allies."
icon = "Textures/Skills/Paraclete's Invitation.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 250
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTypeAllyAndSelf
targetType = SkillTargetTarget

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffectOf(SkillTypeEnchantment)) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  local duration = math.floor((protection * (5 - 3) / 12) + 3)
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
