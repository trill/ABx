--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5245
name = "Way of the Eightfold Path"
attribute = ATTRIB_PROTECTION
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: Enchant target ally for ${floor(protection * 3) / 12)} seconds. For every enchantment spell the target has equipped, they take 5% less damage. For every hex spell, hexes expire 5% quicker. For other spell types, they take 1 less damage from spells. For every attack skill, they gain a 10% chance to block attacks. For every stance, they move 15% faster. For every ritual, they gain +10 armour. For every signet, they gain +1 Health regeneration. For every trap, conditions end 10% sooner. For every shout, they gain +1 Energy regeneration."
shortDescription = "Elite Enchantment Spell: Enchant target ally for 0...3...3 seconds. For every enchantment spell the target has equipped, they take 5% less damage. For every hex spell, hexes expire 5% quicker. For other spell types, they take 1 less damage from spells. For every attack skill, they gain a 10% chance to block attacks. For every stance, they move 15% faster. For every ritual, they gain +10 armour. For every signet, they gain +1 Health regeneration. For every trap, conditions end 10% sooner. For every shout, they gain +1 Energy regeneration."
icon = "Textures/Skills/Way of the Eightfold Path.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 7000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTypeAllyAndSelf
targetType = SkillTargetTarget

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  local duration = math.floor((protection * 3) / 12)
  source:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
