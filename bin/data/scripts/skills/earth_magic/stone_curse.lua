--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5016
name = "Stone Curse"
attribute = ATTRIB_EARTH
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor(earth * ((9 - 2) / 12) + 2)} seconds, effects controlled by target foe can cause neither interruption nor knockdown."
shortDescription = "Hex Spell: For 2...9...10 seconds, effects controlled by target foe can cause neither interruption nor knockdown."
icon = "Textures/Skills/Stone Curse.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 5
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectCounterInterrupt | SkillEffectCounterKD
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local earth = source:GetAttributeRank(ATTRIB_EARTH)
  local time = math.floor(earth * ((9 - 2) / 12) + 2)
  target:AddEffect(source, self.Index, time)

  return SkillErrorNone
end
