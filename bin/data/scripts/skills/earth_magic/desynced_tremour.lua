--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5011
name = "Desynced Tremour"
attribute = ATTRIB_EARTH
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: Target foe and all adjacent foes take ${floor(earth * ((45 - 13) / 12) + 12)} earth damage and are hexed for 3 seconds. After 3 seconds, those foes are knocked down. This hex ends prematurely on each foe if that foe moves."
shortDescription = "Hex Spell: Target foe and all adjacent foes take 13...45...50 earth damage and are hexed for 3 seconds. After 3 seconds, those foes are knocked down. This hex ends prematurely on each foe if that foe moves."
icon = "Textures/Skills/Desynced Tremour.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage | SkillEffectKnockDown
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local earth = source:GetAttributeRank(ATTRIB_EARTH)
  local damage = math.floor(earth * ((45 - 13) / 12) + 12)
  target:Damage(source, self.Index, DAMAGETYPE_EARTH, damage, 0)
  target:AddEffect(source, self.Index, 3000)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      actor:Damage(source, self.Index, DAMAGETYPE_EARTH, damage, 0)
      actor:AddEffect(source, self.Index, 3000)
    end
  end

  return SkillErrorNone
end
