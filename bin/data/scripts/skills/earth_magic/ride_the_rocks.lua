--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5014
name = "Ride the Rocks"
attribute = ATTRIB_EARTH
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Shadow Step to target foe. That foe and all adjacent foes are knocked down and suffer from Cracked Armour for ${floor(attrib * ((8 - 3) / 12) + 8)} seconds."
shortDescription = "Elite Spell: Shadow Step to target foe. That foe and all adjacent foes are knocked down and suffer from Cracked Armour for 3...8...12 seconds."
icon = "Textures/Skills/Ride the Rocks.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 10
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source.Position = target.Position
  source:ForcePositionUpdate()
  local attrib = source:GetAttributeRank(ATTRIB_EARTH)

  local time = math.floor(attrib * ((8 - 3) / 12) + 3)
  target:KnockDown(source, 2000)
  target:AddEffect(source, 10007, time)
  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:KnockDown(source, 2000)
      actor:AddEffect(source, 10007, time)
    end
  end
  return SkillErrorNone
end
