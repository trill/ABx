--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5012
name = "Dust Torrent"
attribute = ATTRIB_EARTH
skillType = SkillTypeSpell
isElite = false
description = "Spell: For 10 seconds, target foe and all adjacent foes are struck for ${floor(earth * (15 - 5) / 12 + 5)} earth damage per second."
shortDescription = "Spell: For 10 seconds, target foe and all adjacent foes are struck for 5...15...20 earth damage per second."
icon = "Textures/Skills/Dust Torrent.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  position = target.Position
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddAOE("/scripts/actors/aoe/elementarist/dust_torrent.lua", self.Index, position)
  return SkillErrorNone
end
