--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5017
name = "Ward Against Magic"
attribute = ATTRIB_EARTH
skillType = SkillTypeWardSpell
isElite = false
description = "Ward Spell: For ${earth * (17 - 5) / 12 + 5} seconds, create a Ward at your location. Hostile spells deal ${earth * (10 - 5) / 12 + 5}% less damage to non-spirit allies within."
shortDescription = "Ward Spell: For 5...17...21 seconds, create a Ward at your location. Hostile spells deal 5...10...15% less damage to non-spirit allies within."
icon = "Textures/Skills/Ward Against Magic.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddAOE("/scripts/actors/aoe/elementarist/ward_against_magic.lua", self.Index, source.Position)

  return SkillErrorNone
end
