--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5013
name = "Mind Lock"
attribute = ATTRIB_EARTH
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: Target foe is struck for ${floor(earth * (50 - 5) / 12 + 5)} earth damage. If you have more Energy than target foe, that foe and all adjacent foes are struck for an additional ${floor(earth * (50 - 5) / 12 + 5)} damage, and for ${floor(earth * (6 - 1) / 12 + 1)} seconds, you are enchanted with Mind Lock. While enchanted, you cannot be targeted by hexes."
shortDescription = "Elite Enchantment Spell: Target foe is struck for 5...50...60 earth damage. If you have more Energy than target foe, that foe and all adjacent foes are struck for an additional 5...50...60 damage, and for 1...6...8 seconds, you are enchanted with Mind Lock. While enchanted, you cannot be targeted by hexes."
icon = "Textures/Skills/Mind Lock.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 5
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  local earth = source:GetAttributeRank(ATTRIB_EARTH)
  local damage = math.floor(earth * (50 - 5) / 12 + 5)
  target:Damage(source, self.Index, DAMAGETYPE_EARTH, damage, 0)
  if (source:GetResource(RESOURCE_TYPE_ENERGY) > target:GetResource(RESOURCE_TYPE_ENERGY)) then
    local actors = target:GetActorsInRange(RANGE_ADJECENT)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source)) then
        actor:Damage(source, self.Index, DAMAGETYPE_EARTH, damage, 0.0)
      end
    end

    local time = math.floor(earth * (6 - 1) / 12 + 1)
    source:AddEffect(source, self.Index, time)
  end
  return SkillErrorNone
end
