--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5010
name = "Conjure Earth"
attribute = ATTRIB_EARTH
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 60 seconds, if you're wielding an earth weapon, your attacks strike for an additional ${floor(earth * ((17 - 5) / 12) + 12} earth damage."
shortDescription = "Enchantment Spell: For 60 seconds, if you're wielding an earth weapon, your attacks strike for an additional 5...17...20 earth damage."
icon = "Textures/Skills/Conjure Earth.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 45000
overcast = 5
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 60000)
  return SkillErrorNone
end
