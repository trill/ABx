--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5133
name = "Illusion of Exhaustion"
attribute = ATTRIB_ILLUSION
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: The next time your Energy falls below ${floor(illusion * ((10 - 1) / 12) + 1)}, you gain ${floor(illusion * ((17 - 5) / 12) + 5)} Energy and this effect ends."
shortDescription = "Enchantment Spell: The next time your Energy falls below 1...10...13, you gain 5...17...19 Energy and this effect ends."
icon = "Textures/Skills/Illusion of Exhaustion.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, TIME_FOREVER)
  return SkillErrorNone
end
