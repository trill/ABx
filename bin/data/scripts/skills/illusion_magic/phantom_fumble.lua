--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5137
name = "Phantom Fumble"
attribute = ATTRIB_ILLUSION
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor((illusion * (8 - 3) / 12) + 3)} seconds, the next time target foe hits with an attack, all other adjacent foes are interrupted and take ${floor(floor(illusion * ((80 - 20) / 12) + 20))} damage."
shortDescription = "Hex Spell: For 3...8...10 seconds, the next time target foe hits with an attack, all other adjacent foes are interrupted and take 20...80...90 damage."
icon = "Textures/Skills/Phantom Fumble.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local illusion = source:GetAttributeRank(ATTRIB_ILLUSION)
  local duration = (illusion * (8 - 3) / 12) + 3
  target:AddEffect(source, self.Index, math.floor(duration * 1000))
  return SkillErrorNone
end
