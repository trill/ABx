--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5139
name = "Haphephobia"
attribute = ATTRIB_ILLUSION
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor((illusion * (8 - 3) / 12) + 3)} seconds, when ever the target gets hit by an attack or is touched, this target is interrupted."
shortDescription = "Hex Spell: For 3...8...10 seconds, when ever the target gets hit by an attack or is touched, this target is interrupted."
icon = "Textures/Skills/Haphephobia.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 750
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectInterrupt
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local illusion = source:GetAttributeRank(ATTRIB_ILLUSION)
  local duration = (illusion * (8 - 3) / 12) + 3
  target:AddEffect(source, self.Index, math.floor(duration * 1000))
  return SkillErrorNone
end
