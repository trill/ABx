--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5230
name = "Presbyter's Edict"
attribute = ATTRIB_NONE
skillType = SkillTypeEnchantment
isElite = false
maintainable = true
description = "Enchantment Spell: Healing Prayers spells you cast on allies also heal all adjacent allies equal to your divine favour bonus. Extra allies healed this way cost you 1 extra Energy."
shortDescription = "Enchantment Spell: Healing Prayers spells you cast on allies also heal all adjacent allies equal to your divine favour bonus. Extra allies healed this way cost you 1 extra Energy."
icon = "Textures/Skills/Presbyter's Edict.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 1000
overcast = 0
hp = 0
costEnergyRegen = 1

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, TIME_FOREVER)
  return SkillErrorNone
end
