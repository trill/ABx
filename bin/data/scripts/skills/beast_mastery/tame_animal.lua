--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5700
name = "Tame Animal"
attribute = ATTRIB_BEAST_MASTERY
skillType = SkillTypeSkill
isElite = false
description = "Skill: Tame animal. This animal will become your pet."
shortDescription = "Skill: Tame animal. This animal will become your pet."
icon = "Textures/Skills/Tame Animal.png"
profession = PROFESSIONINDEX_RANGER
soundEffect = ""
particleEffect = ""
activatesCompanion = true

costEnergy = 10
costAdrenaline = 0
activation = 10000
recharge = 45000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAny

function canUse(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  local npc = target:AsNpc()
  if (npc == nil) then
    return SkillErrorInvalidTarget
  end
  if (npc:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (not npc.Tameable) then
    return SkillErrorInvalidTarget
  end
  if (npc.Master ~= nil) then
    return SkillErrorInvalidTarget
  end
  if (source:GetCompanion() ~= nil) then
    -- TODO: Wrong error
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(npc) == false) then
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local npc = target:AsNpc()
  if (npc == nil) then
    return SkillErrorInvalidTarget
  end
  if (not source:MakeCompanion(npc)) then
    return SkillErrorFailed
  end
  return SkillErrorNone
end
