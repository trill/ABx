--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5320
name = "Acid Rain"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: For 10 seconds, target foe and all foes in the area take ${floor((curses * (15 - 5) / 12) + 5)} cold damage per second. Foes suffering from a condition are dealt an additional ${floor((curses * (15 - 5) / 12) + 5)} shadow damage."
shortDescription = "Spell: For 10 seconds, target foe and all foes in the area take 5...15...20 cold damage per second. Foes suffering from a condition are dealt an additional 5...15...20 shadow damage."
icon = "Textures/Skills/Acid Rain.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 3000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/necromancer/acid_rain.lua", self.Index, target.Position)

  return SkillErrorNone
end
