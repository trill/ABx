--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5329
name = "Dark Parturition"
attribute = ATTRIB_DEATH
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor(death * ((40 - 5) / 12) + 5)} seconds, every time you create a summoned creature, you sacrifice 5% of your maximum Health and deal ${floor(death * ((40 - 20) / 12) + 20)} dark damage to all enemies near the created creature."
shortDescription = "Enchantment Spell: For 5...40...50 seconds, every time you create a summoned creature, you sacrifice 5% of your maximum Health and deal 20...40...50 dark damage to all enemies near the created creature."
icon = "Textures/Skills/Dark Parturition.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 25000
overcast = 0
hp = 0
range = RANGE_MAP
damageType = DAMAGETYPE_DARK
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    return SkillErrorInvalidTarget
  end
  if (actor:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local duration = math.floor(death * ((40 - 5) / 12) + 5)

  source:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
