--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5336
name = "Lugubrist's Choice"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Heal target undead minion you control for ${floor(death * ((160 - 45) / 12) + 45)} Health. All of your other undead minions are destroyed."
shortDescription = "Spell: Heal target undead minion you control for 45...160...180 Health. All of your other undead minions are destroyed."
icon = "Textures/Skills/Lugubrist's Choice.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAllyWithoutSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsMyMinion(target)) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  healing = math.floor(death * ((160 - 45) / 12) + 45)

  local minions = source:GetMinions()
  for i, minion in ipairs(minions) do
    if (minion ~= nil and not minion:IsDead()) then
      if (minion.Id == target.Id) then
        minion:Healing(source, self.Index, healing)
      else
        minion:Die(source)
      end
    end
  end

  return SkillErrorNone
end
