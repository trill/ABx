--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5335
name = "Lacrymactory Signet"
attribute = ATTRIB_DEATH
skillType = SkillTypeSignet
isElite = false
description = "Signet: This signet initially has no effect. Every time a non-spirit creature dies, a charge counter is stored in Lachrymactory Signet (Maximum: 10). When the signet is used again, you and your minions gain ${floor(death * ((10 - 1) / 12) + 1)} Health per charge counter and the number of counters is reset to zero. (Current Charges: 0)"
shortDescription = "Signet: This signet initially has no effect. Every time a non-spirit creature dies, a charge counter is stored in Lachrymactory Signet (Maximum: 10). When the signet is used again, you and your minions gain 1...10...15 Health per charge counter and the number of counters is reset to zero. (Current Charges: 0)"
icon = "Textures/Skills/Lacrymactory Signet.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 1000
recharge = 0
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectHeal
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    return SkillErrorInvalidTarget
  end
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddEffect(source, self.Index, TIME_FOREVER)

  return SkillErrorNone
end
