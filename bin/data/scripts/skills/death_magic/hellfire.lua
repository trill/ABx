--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5334
name = "Hellfire"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Target foe and all foes in the area are struck for ${floor((death * (80 - 10) / 12 + 10))} fire damage and ${floor((death * (50 - 20) / 12 + 20))} shadow damage and are set on fire for ${floor((death * (7 - 1) / 12 + 1))} seconds. You sacrifice an additional 5% Health for each additional foe struck after the first. You cannot sacrifice more than 50% of your total maximum Health by this spell."
shortDescription = "Elite Spell: Target foe and all foes in the area are struck for 10...80...90 fire damage and 20...50...60 shadow damage and are set on fire for 1...7...8 seconds. You sacrifice an additional 5% Health for each additional foe struck after the first. You cannot sacrifice more than 50% of your total maximum Health by this spell."
icon = "Textures/Skills/Hellfire.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 20
range = RANGE_CASTING
damageType = DAMAGETYPE_SHADOW
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local additionalActors = 0

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local fireDamage = math.floor((death * (80 - 10) / 12 + 10))
  local shadowDamage = math.floor((death * (50 - 20) / 12 + 20))
  local fireTime = math.floor((death * (7 - 1) / 12 + 1))

  target:Damage(source, self.Index, DAMAGETYPE_FIRE, fireDamage, 0)
  target:Damage(source, self.Index, DAMAGETYPE_SHADOW, shadowDamage, 0)
  target:AddEffect(source, 10004, fireTime * 1000)

  local actors = target:GetActorsInRange(RANGE_NEARBY)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      additionalActors = additionalActors + 1
      actor:Damage(source, self.Index, DAMAGETYPE_FIRE, fireDamage, 0)
      actor:Damage(source, self.Index, DAMAGETYPE_SHADOW, shadowDamage, 0)
      actor:AddEffect(source, 10004, fireTime * 1000)
    end
  end

  return SkillErrorNone
end

function getSacrifies(hp)
  local result = hp + math.floor(additionalActors * 5)
  if (result > 50) then
    result = 50
  end
  return result
end
