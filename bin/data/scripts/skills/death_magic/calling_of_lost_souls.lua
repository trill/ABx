--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5327
name = "Calling of Lost Souls"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Shadow step all of your summoned creatures to your location and heal them all for ${floor(death * ((30 - 10) / 12) + 10)} Health. (50% chance of failure with Death Magic 4 or less)."
shortDescription = "Spell: Shadow step all of your summoned creatures to your location and heal them all for 10...30...35 Health. (50% chance of failure with Death Magic 4 or less)."
icon = "Textures/Skills/Calling of Lost Souls.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 10000
overcast = 0
hp = 0
range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone


function canUse(source, target)
  if (not source:IsControllingMinions()) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (actor:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)

  if (death <= 4) then
    if (Random() < 0.5) then
      return SkillErrorFailed
    end
  end

  local healing = math.floor(death * ((30 - 10) / 12) + 10)
  local minions = source:GetMinions()
  for i, minion in ipairs(minions) do
    local npc = minions:AsNpc()
    if (npc ~= nil and not npc:IsDead()) then
      npc:ShadowStep(source.Position)
      npc.Healing(source, self.Index, healing)
    end
  end
  return SkillErrorNone
end
