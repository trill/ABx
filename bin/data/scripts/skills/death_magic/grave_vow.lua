--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5333
name = "Grave Vow"
attribute = ATTRIB_DEATH
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, you gain ${floor((death * 5) / 12)} Energy every time you exploit a corpse."
shortDescription = "Enchantment Spell: For 30 seconds, you gain 0...5...8 Energy every time you exploit a corpse."
icon = "Textures/Skills/Grave Vow.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_MAP
damageType = DAMAGETYPE_DARK
effect = SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    return SkillErrorInvalidTarget
  end
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddEffect(source, self.Index, 30000)

  return SkillErrorNone
end
