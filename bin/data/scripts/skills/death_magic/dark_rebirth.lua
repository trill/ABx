--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5330
name = "Dark Rebirth"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Create a level ${floor(death * ((12 - 1) / 12) + 1)} undead clone of target dead party member which has all of their non-elite skills and lives for ${floor(death * ((25 - 5) / 12) + 5)} seconds. The creature is destroyed if that party member is resurrected."
shortDescription = "Spell: Create a level 1...12...15 undead clone of target dead party member which has all of their non-elite skills and lives for 5...25...30 seconds. The creature is destroyed if that party member is resurrected."
icon = "Textures/Skills/Dark Rebirth.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSummonMinion
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  local attribVal = source:GetAttributeRank(ATTRIB_DEATH)
  local level = math.floor((attribVal * (12 - 1) / 12 + 1))
  source:SpawnSlave("/scripts/actors/npcs/dark_rebirth_clone.lua", actor, SLAVE_KIND_MINION, level, actor.Position)
  actor.Recycleable = false
  source:ExploitedCorpse(actor)
  return SkillErrorNone
end
