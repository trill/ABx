--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5326
name = "Animate Wailing Banshee"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Exploit the nearest corpse to create a level ${floor(death * ((20 - 1) / 12) + 1)} Wailing Banshee which can cast Deathly Chill, Death Nova and Fetid Ground. You can only control one Wailing Banshee at a time."
shortDescription = "Elite Spell: Exploit the nearest corpse to create a level 1...20...23 Wailing Banshee which can cast Deathly Chill, Death Nova and Fetid Ground. You can only control one Wailing Banshee at a time."
icon = "Textures/Skills/Animate Wailing Banshee.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSummonMinion
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)
  local SCRIPT = "/scripts/actors/npcs/wailing_banshee.lua"

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  -- You can only control one Wailing Banshee at a time.
  local minions = source:GetMinions()
  for i, minion in ipairs(minions) do
    local npc = minions:AsNpc()
    if (npc ~= nil and not npc:IsDead() and npc:GetScriptFile() == SCRIPT) then
      npc:Die(nil)
    end
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local level = math.floor(death * ((20 - 1) / 12) + 1)
  source:SpawnSlave(SCRIPT, actor, SLAVE_KIND_MINION, level, actor.Position)
  actor.Recycleable = false
  source:ExploitedCorpse(actor)
  return SkillErrorNone
end
