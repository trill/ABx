--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5339
name = "Putrefaction"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Exploits a corpes and creates a Well of Putrefaction for ${floor((death * (15 - 5) / 12) + 5)} seconds which causes ${floor((death * (5 - 1) / 12) + 1)} health degeneration and the Desease condition."
shortDescription = "Spell: Exploits a corpes and creates a Well of Putrefaction for 0...15...18 seconds which causes 1...5...6 health degeneration and the Desease condition."
icon = "Textures/Skills/Putrefacation.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/necromancer/putrefacation.lua", self.Index, actor.Position)
  actor.Recycleable = false
  source:ExploitedCorpse(actor)

  return SkillErrorNone
end
