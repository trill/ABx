--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5324
name = "Animate Ghoul"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Exploit the nearest corpse to create a level ${ceil(death * (15 / 12) + 1)} Ghoul which can cast Shudder."
shortDescription = "Spell: Exploit the nearest corpse to create a level 1...15...20 Ghoul which can cast Shudder."
icon = "Textures/Skills/Animate Ghoul.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 3000
recharge = 5000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSummonMinion
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  local attribVal = source:GetAttributeRank(ATTRIB_DEATH)
  local level = math.ceil(attribVal * (15 / 12) + 1)
  source:SpawnSlave("/scripts/actors/npcs/ghoul.lua", actor, SLAVE_KIND_MINION, level, actor.Position)
  actor.Recycleable = false
  source:ExploitedCorpse(actor)
  return SkillErrorNone
end
