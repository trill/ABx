--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5331
name = "Demon Form"
attribute = ATTRIB_DEATH
skillType = SkillTypeForm
isElite = true
description = "Elite Form: For ${floor(death * ((50 - 5) / 12) + 5)} seconds, assume Demon Form. You move, attack, and gain adrenaline 25% faster and your attacks deal dark damage. You take an additional ${floor((death * (25 - 50) / 12) + 50)}% damage from holy sources and you gain ${floor((death * (10 - 15) / 12) + 15)}% less benefit from healing."
shortDescription = "Elite Form: For 5...50...70 seconds, assume Demon Form. You move, attack, and gain adrenaline 25% faster and your attacks deal dark damage. You take an additional 50...25...25% damage from holy sources and you gain 15...10...10% less benefit from healing."
icon = "Textures/Skills/Demon Form.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 3000
recharge = 60000
overcast = 0
hp = 0
range = RANGE_MAP
damageType = DAMAGETYPE_DARK
effect = SkillEffectNone
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    return SkillErrorInvalidTarget
  end
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local duration = math.floor(death * ((50 - 5) / 12) + 5)
  source:AddEffect(source, self.Index, duration * 1000)

  return SkillErrorNone
end
