--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5328
name = "Dance of the Dead"
attribute = ATTRIB_DEATH
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor(death * ((25 - 10) / 12) + 10)} seconds, undead minions you create move 25% faster."
shortDescription = "Enchantment Spell: For 10...25...29 seconds, undead minions you create move 25% faster."
icon = "Textures/Skills/Dance of the Dead.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 30000
overcast = 0
hp = 0
range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source == nil) then
    return SkillErrorInvalidTarget
  end
  if (actor:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local duration = math.floor(death * ((25 - 10) / 12) + 10)

  source:AddEffect(source, self.Index, duration)

  return SkillErrorNone
end
