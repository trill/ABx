--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5332
name = "Detonate Corpse"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
description = "Spell: Exploit the corpse nearest to target foe. All nearby foes are struck for ${floor((death * (50 - 15) / 12 + 15))} cold damage and are interrupted."
shortDescription = "Spell: Exploit the corpse nearest to target foe. All nearby foes are struck for 15...50...70 cold damage and are interrupted."
icon = "Textures/Skills/Detonate Corpse.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeFoe

local function getDeadActor(target)
  local deadActors = target:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor.Recycleable) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(target)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(target)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local corpse = getDeadActor(source)

  if (corpse == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(corpse) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (not corpse:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (corpse.Recycleable == false) then
    return SkillErrorInvalidTarget
  end

  local death = source:GetAttributeRank(ATTRIB_DEATH)
  local damage = math.floor((death * (50 - 15) / 12 + 15))
  local actors = corpse:GetActorsInRange(RANGE_NEARBY)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      actor:Damage(source, self.Index, damageType, damage, 0.0)
      actor:Interrupt(source)
    end
  end

  corpse.Recycleable = false
  source:ExploitedCorpse(corpse)
  return SkillErrorNone
end
