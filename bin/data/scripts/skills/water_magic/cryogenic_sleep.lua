--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5052
name = "Cryogenic Sleep"
attribute = ATTRIB_WATER
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 3 seconds, you cannot move, take 10 less damage from all sources, suffer -10 Health degeneration and -10 Energy degeneration. After 3 seconds, you are healed for ${floor(water * (150 - 60) / 12 + 60)} Health and gain ${floor(water * (24 - 3) / 12 + 3)} Energy. This has no effect if this enchantment ends early."
shortDescription = "Enchantment Spell: For 3 seconds, you cannot move, take 10 less damage from all sources, suffer -10 Health degeneration and -10 Energy degeneration. After 3 seconds, you are healed for 60...150...170 Health and gain 3...24...27 Energy. This has no effect if this enchantment ends early."
icon = "Textures/Skills/Cryogenic Sleep.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 35000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect | SkillEffectHeal
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 3000)
  return SkillErrorNone
end
