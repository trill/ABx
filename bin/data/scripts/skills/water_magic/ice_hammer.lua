--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5058
name = "Ice Hammer"
attribute = ATTRIB_WATER
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: Strike target foe for ${floor((water * (90 - 15) / 12) + 15)} cold damage. For ${floor((water * (5 - 1) / 12) + 1)} seconds, target foe moves 66% more slowly."
shortDescription = "Hex Spell: Strike target foe for 15...90...110 cold damage. For 1...5...7 seconds, target foe moves 66% more slowly."
icon = "Textures/Skills/Ice Hammer.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 7000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  local damage = math.floor((attrib * (90 - 15) / 12) + 15)
  local effectTime = math.floor((attrib * (5 - 1) / 12) + 1)

  target:Damage(source, self.Index, damageType, damage, 0)
  target:AddEffect(source, self.Index, effectTime)

  return SkillErrorNone
end
