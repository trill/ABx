--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5062
name = "Shatter Flesh"
attribute = ATTRIB_WATER
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: Target foe is hexed with Shatter Flesh for ${floor((water * (25 - 1) / 12) + 1)} seconds. When that foe dies, their corpse is exploited and all nearby foes are struck for ${floor((water * (75 - 10) / 12) + 10)} cold damage and move 66% more slowly for ${floor((water * 3) / 12)} seconds."
shortDescription = "Hex Spell: Target foe is hexed with Shatter Flesh for 1...25...30 seconds. When that foe dies, their corpse is exploited and all nearby foes are struck for 10...75...90 cold damage and move 66% more slowly for 0...3...3 seconds."
icon = "Textures/Skills/Shatter Flesh.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  local effectTime = math.floor((attrib * (25 - 1) / 12) + 1)

  target:AddEffect(source, self.Index, effectTime)

  return SkillErrorNone
end
