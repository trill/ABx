--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5064
name = "Tsunami"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: For two seconds, target foe and all foes in the area take ${floor(water * ((100 - 25) / 12) + 25)} cold damage every second."
shortDescription = "Elite Spell: For two seconds, target foe and all foes in the area take 25...100...120 cold damage every second."
icon = "Textures/Skills/Tsunami.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 15000
overcast = 5
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetAoe | SkillTargetAoe
targetType = SkillTargetTypeFoe

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  position = target.Position
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/elementarist/tsunami.lua", self.Index, position)
  return SkillErrorNone
end
