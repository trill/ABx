--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5057
name = "Healing Rain"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = false
description = "Spell: For 10 seconds, target ally and all adjacent allies are healed for ${floor(water * ((30 - 10) / 12) + 10)} Health per second."
shortDescription = "Spell: For 10 seconds, target ally and all adjacent allies are healed for 10...30...40 Health per second."
icon = "Textures/Skills/Healing Rain.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1500
recharge = 25000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeAllyWithoutSelf

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsAlly(target)) then
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  position = target.Position
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/elementarist/healing_rain.lua", self.Index, position)
  return SkillErrorNone
end
