--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5056
name = "Hailstorm"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = false
description = "Spell: Create a Hailstorm at target foe's location for 10 seconds. Foes in the area are struck for ${floor(water * ((15 - 5) / 12) + 15)} cold damage per second. Foes under the influence of a Water Magic Hex also bleed for ${floor(water * (3 - 1) / 12 + 1)} seconds."
shortDescription = "Spell: Create a Hailstorm at target foe's location for 10 seconds. Foes in the area are struck for 5...15...20 cold damage per second. Foes under the influence of a Water Magic Hex also bleed for 1...3...4 seconds."
icon = "Textures/Skills/Hailstorm.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 25000
overcast = 10
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  position = target.Position
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/elementarist/hailstorm.lua", self.Index, position)
  return SkillErrorNone
end
