--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5066
name = "Winter Rose"
attribute = ATTRIB_WATER
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: Target foe is struck for ${floor(water * ((60 - 10) / 12) + 10)} cold damage. If that foe was using a skill, that foe moves 66% slower for ${ceil(water * ((5 - 1) / 12) + 1)} seconds."
shortDescription = "Hex Spell: Target foe is struck for 10...60...70 cold damage. If that foe was using a skill, that foe moves 66% slower for 1...1...2 seconds."
icon = "Textures/Skills/Winter Rose.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 4000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  local effectTime = math.ceil(attrib / 12)
  target:AddEffect(source, self.Index, effectTime * 1000)

  return SkillErrorNone
end
