--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5063
name = "Snowblind"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = false
description = "Spell: For 3 seconds, target foe and all foes between you and that foe are struck for ${floor(water * ((60 - 10) / 12) + 10)} cold damge per second and are blinded for ${floor(water * ((4 - 1) / 12) + 1)} seconds. You cannot move for 3 seconds."
shortDescription = "Spell: For 3 seconds, target foe and all foes between you and that foe are struck for 10...60...70 cold damge per second and are blinded for 1...4...5 seconds. You cannot move for 3 seconds."
icon = "Textures/Skills/Snowblind.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 750
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  
  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  local blindTime = math.floor(attribVal * ((4 - 1) / 12) + 1)
  
  local objects = source:RaycastToObject(target)
  for i, object in ipairs(objects) do
    local actor = object:AsActor()
    if (actor ~= nil) then
      if (not actor:IsDead() and source:IsEnemy(actor)) then
        actor:AddEffect(source, self.Index, 3000)
        actor:AddEffect(source, 10005, blindTime)
      end
    end
  end
  source:AddEffect(source, 1002, 3000)
  return SkillErrorNone
end
