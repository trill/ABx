--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5051
name = "Chilling Glyph"
attribute = ATTRIB_WATER
skillType = SkillTypeGlyph
isElite = false
description = "Glyph: The next ${floor(water * (2 - 1) / 12 + 1)} spells you cast which target a foe also apply a Water Magic hex which slows the target's movement rate by ${floor(water * (40 - 10) / 12 + 10)}% for ${floor(water * (40 - 10) / 12 + 10)} seconds."
shortDescription = "Glyph: The next 1...2...3 spells you cast which target a foe also apply a Water Magic hex which slows the target's movement rate by 10...40...50% for 1...4...6 seconds."
icon = "Textures/Skills/Chilling Glyph.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSnare
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, TIME_FOREVER)
  return SkillErrorNone
end
