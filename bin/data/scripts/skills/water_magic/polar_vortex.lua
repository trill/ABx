--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5060
name = "Polar Vortex"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = false
description = "Spell: Create a Polar Vortex at target foe's location for 10 seconds. Foes in the area under the influence of at least one Water Magic hex take ${floor(water * ((25 - 5) / 12) + 5)} damage per second. Foes under the influence of at least two Water Magic hexes are also interrupted."
shortDescription = "Spell: Create a Polar Vortex at target foe's location for 10 seconds. Foes in the area under the influence of at least one Water Magic hex take 5...25...30 damage per second. Foes under the influence of at least two Water Magic hexes are also interrupted."
icon = "Textures/Skills/Polar Vortex.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeFoe

local position

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  position = target.Position
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  source:AddAOE("/scripts/actors/aoe/elementarist/polar_vortex.lua", self.Index, position)
  return SkillErrorNone
end
