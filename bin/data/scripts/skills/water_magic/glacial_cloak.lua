--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5055
name = "Glacial Cloak"
attribute = ATTRIB_WATER
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For ${floor(water * (8 - 3) / 12 + 3)} seconds, you gain +${floor((water * (25 - 1) / 12 + 1))} armour, +${floor((water * 3) / 12)} damage reduction and +${floor((water * (4 - 1) / 12 + 1))} Health regeneration, but you move 50% slower. This effect ends if you use a skill."
shortDescription = "Enchantment Spell: For 3...8...9 seconds, you gain +1...25...30 armour, +0...3...5 damage reduction and +1...4...6 Health regeneration, but you move 50% slower. This effect ends if you use a skill."
icon = "Textures/Skills/Glacial Cloak.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 15000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect | SkillEffectHeal
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  local time = math.floor(attribVal * (8 - 3) / 12 + 3)
  source:AddEffect(source, self.Index, time * 1000)
  return SkillErrorNone
end
