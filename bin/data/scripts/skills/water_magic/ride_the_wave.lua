--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5061
name = "Ride the Wave"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Shadow Step to target foe. That foe and all adjacent foes take ${floor(water * ((70 - 15) / 12) + 15)} cold damage and are interrupted."
shortDescription = "Elite Spell: Shadow Step to target foe. That foe and all adjacent foes take 15...70...90 cold damage and are interrupted."
icon = "Textures/Skills/Ride the Wave.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 5
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage | SkillEffectInterrupt
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  source:ShadowStep(target.Position)
  local attribVal = target:GetAttributeRank(ATTRIB_WATER)
  local damage = math.floor(attribVal * ((70 - 15) / 12) + 15)
  local actors = source:GetEnemiesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
      actor:Interrupt(source)
    end
  end
  return SkillErrorNone
end
