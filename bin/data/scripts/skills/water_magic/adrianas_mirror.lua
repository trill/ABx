--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5050
name = "Adriana's Mirror"
attribute = ATTRIB_WATER
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${floor(water * (45 - 10) / 12 + 10)} seconds, whenever you take damage from an enemy spell, the damage is reduced by ${floor((water * (50 - 5) / 12 + 5) / 100)}% and the source of that damage takes ${floor(water * (50 - 5) / 12 + 5)}% of the damage that would have been dealt to you. (Maximum: 100)"
shortDescription = "Elite Enchantment Spell: For 10...45...60 seconds, whenever you take damage from an enemy spell, the damage is reduced by 5...50...65% and the source of that damage takes 5...50...65% of the damage that would have been dealt to you. (Maximum: 100)"
icon = "Textures/Skills/Adriana's Mirror.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  local time = math.floor(attribVal * (45 - 10) / 12 + 10)
  source:AddEffect(source, self.Index, time * 1000)
  return SkillErrorNone
end
