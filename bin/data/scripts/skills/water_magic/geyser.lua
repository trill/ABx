--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5054
name = "Geyser"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = false
description = "Spell: After 2 seconds, target foe and all foes in the area are affected by Cracked Armour for ${floor((water * (15 - 3) / 12) + 3)} seconds, take ${floor((water * (100 - 10) / 12) + 10)} cold damage and move 66% slower for ${floor(water * 2 / 12)} seconds."
shortDescription = "Spell: After 2 seconds, target foe and all foes in the area are affected by Cracked Armour for 3...15...18 seconds, take 10...100...110 cold damage and move 66% slower for 0...2...2 seconds."
icon = "Textures/Skills/Geyser.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 2000
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage | SkillEffectSnare
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  target:AddEffect(source, self.Index, 4000)
  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:AddEffect(source, self.Index, 4000)
    end
  end

  return SkillErrorNone
end
