--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5059
name = "Mind Wash"
attribute = ATTRIB_WATER
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Deal ${floor((water * (40 - 1) / 12) + 1)} cold damage to target foe. If you have more Energy than that foe, you deal an additional ${floor((water * (40 - 1) / 12) + 1)} cold damage and you and all nearby allies are healed for ${floor((water * (99 - 40) / 12) + 40)} Health."
shortDescription = "Elite Spell: Deal 1...40...50 cold damage to target foe. If you have more Energy than that foe, you deal an additional 1...40...50 cold damage and you and all nearby allies are healed for 40...99...125 Health."
icon = "Textures/Skills/Mind Wash.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_COLD
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  local damage = math.floor((attrib * (40 - 1) / 12) + 1)
  local healing = math.floor((attrib * (99 - 40) / 12) + 40)
  target:Damage(source, self.Index, damageType, damage, 0)
  if (source:GetResource(RESOURCE_TYPE_ENERGY) > target:GetResource(RESOURCE_TYPE_ENERGY)) then
    local actors = source:GetAlliesInRange(RANGE_NEARBY)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead()) then
        actor:Healing(source, self.Index, healing)
      end
    end
  end

  return SkillErrorNone
end
