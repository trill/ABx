--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 9996
name = "Instant Rezz"
attribute = ATTRIB_NONE
skillType = SkillTypeSpell
isElite = false
description = "Instant Rezz"
shortDescription = "Instantly resurrects the target"
icon = "Textures/Skills/Monster Skill.png"
profession = PROFESSIONINDEX_NONE
soundEffect = ""
particleEffect = ""
access = SkillAccessGM

costEnergy = 0
costAdrenaline = 0
activation = 2000
recharge = 1000
overcast = 0
hp = 0
range = RANGE_ADJECENT
effect = SkillEffectResurrect
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAny

function onStartUse(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (target:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead() == false) then
    return SkillErrorInvalidTarget
  end

  target:Resurrect(25, 100)
  return SkillErrorNone
end

function onCancelled(source, target)
end

function onInterrupted(source, target)
end
