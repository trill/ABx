--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

index = 0
name = "None"
attribute = ATTRIB_NONE
skillType = 0
isElite = false
description = "None"
shortDescription = "None"
icon = ""
profession = PROFESSIONINDEX_NONE
soundEffect = ""
particleEffect = ""
access = SkillAccessNone

costEnergy = 0
costAdrenaline = 0
activation = 0
recharge = 0
overcast = 0
hp = 0
