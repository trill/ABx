--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 9997
name = "Instant Kill"
attribute = ATTRIB_NONE
skillType = SkillTypeSpell
isElite = false
description = "Instant Kill"
shortDescription = "Instantly kills the target"
icon = "Textures/Skills/Monster Skill.png"
profession = PROFESSIONINDEX_NONE
soundEffect = ""
particleEffect = ""
access = SkillAccessGM

costEnergy = 0
costAdrenaline = 0
activation = 0
recharge = 0
overcast = 0
-- HP cost
hp = 0
range = RANGE_MAP
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAny

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  target:Die(source)
  return SkillErrorNone
end
