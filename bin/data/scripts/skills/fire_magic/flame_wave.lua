--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5033
name = "Flame Wave"
attribute = ATTRIB_FIRE
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Send out a Flame Wave in the direction of target foe which travels for 100'. All foes in its path are struck for ${floor((fire * (80 - 10) / 12) + 10)} fire damage."
shortDescription = "Elite Spell: Send out a Flame Wave in the direction of target foe which travels for 100'. All foes in its path are struck for 10...80...90 fire damage."
icon = "Textures/Skills/Flame Wave.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1500
recharge = 7000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_FIRE)
  local damage = math.floor((attrib * (80 - 10) / 12) + 10)

  local objects = source:RaycastToObject(target)
  for i, object in ipairs(objects) do
    local actor = object:AsActor()
    if (actor ~= nil) then
      if (not actor:IsDead() and source:IsEnemy(actor)) then
        actor:Damage(source, self.Index, damageType, damage, 0)
      end
    end
  end
  return SkillErrorNone
end
