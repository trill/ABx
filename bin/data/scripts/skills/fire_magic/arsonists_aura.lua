--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5030
name = "Arsonist's Aura"
attribute = ATTRIB_FIRE
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 25 seconds, every time you cast a Fire Magic spell, all foes adjacent to you are struck for ${floor((fire * (40 - 5) / 12) + 5)} fire damage and are set on fire for 1 second."
shortDescription = "Enchantment Spell: For 25 seconds, every time you cast a Fire Magic spell, all foes adjacent to you are struck for 5...40...45 fire damage and are set on fire for 1 second."
icon = "Textures/Skills/Arsonist's Aura.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 25000)
  return SkillErrorNone
end
