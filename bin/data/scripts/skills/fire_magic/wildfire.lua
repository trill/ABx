--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5040
name = "Wildfire"
attribute = ATTRIB_FIRE
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor((fire * (12 - 1) / 12) + 1)} seconds, target foe suffers from -7 Health degeneration. This hex spreads itself between adjacent foes for its remaining duration."
shortDescription = "Hex Spell: For 1...12...15 seconds, target foe suffers from -7 Health degeneration. This hex spreads itself between adjacent foes for its remaining duration."
icon = "Textures/Skills/Wildfire.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  local attrib = source:GetAttributeRank(ATTRIB_FIRE)
  local duration = math.floor((attrib * (12 - 1) / 12) + 1)

  target:AddEffect(source, self.Index, duration)
  return SkillErrorNone
end
