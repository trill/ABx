--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5034
name = "Flame Whip"
attribute = ATTRIB_FIRE
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes between you and that foe are struck for ${floor((fire * (90 - 20) / 12) + 20)} fire damage and are crippled for ${floor(((fire * 1) / 12)} seconds, and you cannot move for 1 second."
shortDescription = "Spell: Target foe and all foes between you and that foe are struck for 20...70...90 fire damage and are crippled for 0...1...2 seconds, and you cannot move for 1 second."
icon = "Textures/Skills/Flame Whip.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 18000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_FIRE)
  local damage = math.floor((attrib * (90 - 20) / 12) + 20)
  local crippledTime = math.floor((attrib * 1) / 12)

  local objects = source:RaycastToObject(target)
  for i, object in ipairs(objects) do
    local actor = object:AsActor()
    if (actor ~= nil) then
      if (not actor:IsDead() and source:IsEnemy(actor)) then
        actor:Damage(source, self.Index, damageType, damage, 0)
        actor:AddEffect(source, 10001, crippledTime)
        -- Immobilized
        actor:AddEffect(source, 1002, 1000)
      end
    end
  end
  return SkillErrorNone
end
