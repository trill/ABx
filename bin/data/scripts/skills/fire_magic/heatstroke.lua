--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5036
name = "Heatstroke"
attribute = ATTRIB_FIRE
skillType = SkillTypeHex
isElite = false
description = "Hex Spell: For ${floor(fire * (15 - 5) / 12 + 5)} seconds, every time target foe has been on fire consecutively for 5 seconds, that foe is knocked down."
shortDescription = "Hex Spell: For 5...15...18 seconds, every time target foe has been on fire consecutively for 5 seconds, that foe is knocked down."
icon = "Textures/Skills/Heatstroke.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectKnockDown
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_FIRE)
  local time = math.floor(attribVal * (15 - 5) / 12 + 5)
  target:AddEffect(source, self.Index, time * 1000)
  return SkillErrorNone
end
