--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5037
name = "Hellraiser's Haste"
attribute = ATTRIB_FIRE
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${floor(fire * (28 - 10) / 12 + 10)} seconds, your Fire Magic spells are cast and recharge 66% more quickly while you are on fire."
shortDescription = "Elite Enchantment Spell: For 10...28...33 seconds, your Fire Magic spells are cast and recharge 66% more quickly while you are on fire."
icon = "Textures/Skills/Hellraiser's Haste.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 750
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_FIRE
effect = SkillEffectFasterCast
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_FIRE)
  local time = math.floor(attribVal * (28 - 10) / 12 + 10)
  target:AddEffect(source, self.Index, time * 1000)
  return SkillErrorNone
end
