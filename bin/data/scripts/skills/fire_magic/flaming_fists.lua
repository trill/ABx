--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5035
name = "Flaming Fists"
attribute = ATTRIB_FIRE
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 30 seconds, your touch range and point blank range Fire Magic spells cost 5 more Energy but deal +${floor(fire * (20 - 10) / 12 + 10)}% more damage."
shortDescription = "Enchantment Spell: For 30 seconds, your touch range and point blank range Fire Magic spells cost 5 more Energy but deal +10...20...25% more damage."
icon = "Textures/Skills/Flaming Fists.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 45000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 30000)
  return SkillErrorNone
end
