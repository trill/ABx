--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5038
name = "Ride the Flames"
attribute = ATTRIB_FIRE
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Shadow Step to target foe. Set yourself on fire for ${floor(fire * (3 - 1) / 12 + 1)} seconds. Set all nearby foes on fire for ${floor(fire * (8 - 2) / 12 + 2)} seconds. After two seconds, all foes in the area around you take ${floor(fire * (120 - 15) / 12 + 15)} fire damage."
shortDescription = "Elite Spell: Shadow Step to target foe. Set yourself on fire for 1...3...4 seconds. Set all nearby foes on fire for 2...8...10 seconds. After two seconds, all foes in the area around you take 15...120...140 fire damage."
icon = "Textures/Skills/Ride the Flames.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 8000
overcast = 10
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  source:ShadowStep(target.Position)
  local attribVal = target:GetAttributeRank(ATTRIB_FIRE)
  local fireTimeSelf = math.floor(attribVal * (3 - 1) / 12 + 1)
  source:AddEffect(source, 10004, fireTimeSelf * 1000)

  local fireTimeFoes = math.floor(attribVal * (8 - 2) / 12 + 2)
  local damage = math.floor(attribVal * (120 - 15) / 12 + 15)

  -- This effect makes damage after 2 seconds around us (source)
  source:AddEffect(source, self.Index, 2000)
  target:AddEffect(source, 10004, fireTimeFoes * 1000)

  local actors = source:GetEnemiesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:AddEffect(source, 10004, fireTimeFoes * 1000)
    end
  end

  return SkillErrorNone
end
