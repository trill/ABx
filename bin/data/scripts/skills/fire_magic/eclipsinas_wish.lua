--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5032
name = "Eclipsina's Wish"
attribute = ATTRIB_FIRE
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 90 seconds, if target ally dies while enchanted with Eclipsina's Wish, all adjacent foes are knocked down, set on fire for ${floor(fire * ((9 - 1) / 12) + 1)} seconds, and are hexed with Eclipsina's Wish for 8 seconds. Foes hexed with Eclipsina's Wish take ${floor(fire * ((20 - 1) / 12) + 1)} fire damage per second while on fire."
shortDescription = "Enchantment Spell: For 90 seconds, if target ally dies while enchanted with Eclipsina's Wish, all adjacent foes are knocked down, set on fire for 1...5...9 seconds, and are hexed with Eclipsina's Wish for 8 seconds. Foes hexed with Eclipsina's Wish take 1...20...25 fire damage per second while on fire."
icon = "Textures/Skills/Eclipsina's Wish.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 10
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_FIRE
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target)) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  target:AddEffect(source, self.Index, 90000)
  return SkillErrorNone
end
