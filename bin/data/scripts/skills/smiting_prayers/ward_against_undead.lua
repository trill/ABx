--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5258
name = "Ward Against Undead"
attribute = ATTRIB_SMITING
skillType = SkillTypeWardSpell
isElite = false
description = "Ward Spell: Create a Ward at your location. For ${floor(smiting * (16 - 5) / 12 + 5)} seconds, undead enemies and those who control undead minions take ${floor(smiting * ((15 - 5) / 12) + 5)} holy damage per second while in range."
shortDescription = "Ward Spell: Create a Ward at your location. For 5...16...18 seconds, undead enemies and those who control undead minions take 5...15...20 holy damage per second while in range."
icon = "Textures/Skills/Ward Against Undead.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 2000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectDamage
effectTarget = SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddAOE("/scripts/actors/aoe/priest/ward_against_undead.lua", self.Index, source.Position)

  return SkillErrorNone
end
