--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5254
name = "Erase Evil"
attribute = ATTRIB_SMITING
skillType = SkillTypeSpell
isElite = false
description = "Spell: Deal ${floor(smiting * ((50 - 5) / 12) + 5)} holy damage to target foe. If that foe is either undead or controls a minion, this skill recharges 50% faster."
shortDescription = "Spell: Deal 5...50...60 holy damage to target foe. If that foe is either undead or controls a minion, this skill recharges 50% faster."
icon = "Textures/Skills/Erase Evil.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 18000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_HOLY
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

local rechargeFaster = false

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  local damage = math.floor(smiting * ((29 - 5) / 12) + 5)
  target:Damage(source, self.Index, damageType, damage, 0)
  if (target.Species == SPECIES_UNDEAD or target:IsControllingMinions()) then
    rechargeFaster = true
  end

  return SkillErrorNone
end

function getRecharge(recharge)
  if (rechargeFaster) then
    return math.floor(recharge * 0.5)
  end
  return recharge
end
