--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5251
name = "Advisor's Intervention"
attribute = ATTRIB_SMITING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 6 seconds, the next time target ally would have taken damage, up to 1...50...60 damage is prevented and that ally gains 1...2...3 Energy. If the damage dealt would have been fatal, that ally is also healed for 50...90...100 Health."
shortDescription = "Enchantment Spell: For 6 seconds, the next time target ally would have taken damage, up to 1...50...60 damage is prevented and that ally gains 1...2...3 Energy. If the damage dealt would have been fatal, that ally is also healed for 50...90...100 Health."
icon = "Textures/Skills/Advisor's Intervention.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTypeAllyAndSelf
targetType = SkillTargetTarget

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, 6000)
  return SkillErrorNone
end
