--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5255
name = "Exile"
attribute = ATTRIB_SMITING
skillType = SkillTypeSpell
isElite = false
description = "Spell: Destroy target summoned creature. This spell will recharge in ${floor(smiting * ((20 - 60) / 12) + 60)} seconds."
shortDescription = "Spell: Destroy target summoned creature. This spell will recharge in 60...20...15 seconds."
icon = "Textures/Skills/Exile.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = TIME_FOREVER
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_HOLY
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

local rechargeTime = 0

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (target.SlaveKind ~= SLAVE_KIND_MINION or target.SlaveKind ~= SLAVE_KIND_SPIRIT) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  target:Die(source)
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  rechargeTime = math.floor(smiting * ((20 - 60) / 12) + 60)

  return SkillErrorNone
end

function getRecharge(recharge)
  return rechargeTime
end
