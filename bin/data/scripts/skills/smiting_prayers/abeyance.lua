--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5250
name = "Abeyance"
attribute = ATTRIB_SMITING
skillType = SkillTypeSpell
isElite = false
description = "Spell: If target foe is casting a spell, that foe is interrupted and takes ${floor(smiting * ((70 - 5) / 12) + 5)} holy damage. If a Necromancer or Mesmer spell is interrupted in this way this skill recharges 50% faster."
shortDescription = "Spell: If target foe is casting a spell, that foe is interrupted and takes 5...70...80 holy damage. If a Necromancer or Mesmer spell is interrupted in this way this skill recharges 50% faster."
icon = "Textures/Skills/Abeyance.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 15000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_HOLY
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

local skillProfession = PROFESSIONINDEX_NONE

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (not target:IsUsingSkillOfType(SkillTypeSpell, 250)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local skill = target:GetCurrentSkill()
  if (skill == nil) then
    -- No skill to interrupt -> no error just nothing happens
    return SkillErrorNone
  end

  if (skill:IsType(SkillTypeSpell)) then
    if (target:InterruptSkill(source, SkillTypeSpell)) then
      local smiting = source:GetAttributeRank(ATTRIB_SMITING)
      local damage = math.floor(smiting * ((70 - 5) / 12) + 5)
      target:Damage(source, self.Index, damageType, damage, 0.0)
      skillProfession = skill:GetProfession()
    end
  end

  return SkillErrorNone
end

function getRecharge(recharge)
  if (skillProfession == PROFESSIONINDEX_NECROMANCER or skillProfession == PROFESSIONINDEX_MESMER) then
    return math.floor(recharge * 0.5)
  end
  return recharge
end
