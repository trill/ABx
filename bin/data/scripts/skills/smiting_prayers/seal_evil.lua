--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5257
name = "Seal Evil"
attribute = ATTRIB_SMITING
skillType = SkillTypeHex
isElite = true
description = "Elite Hex Spell: For ${floor(smiting * ((9 - 3) / 12) + 3)} seconds, chaos, dark, shadow, and typeless damage, direct Health loss and life stealing caused by target foe is ${floor(smiting * ((70 - 15) / 12) + 15)}% less effective."
shortDescription = "Elite Hex Spell: For 3...9...11 seconds, chaos, dark, shadow, and typeless damage, direct Health loss and life stealing caused by target foe is 15...70...80% less effective."
icon = "Textures/Skills/Seal Evil.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 18000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (not source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  local duration = math.floor(smiting * ((9 - 3) / 12) + 3)
  target:AddEffect(source, self.Index, duration * 1000)
  return SkillErrorNone
end
