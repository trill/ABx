--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5256
name = "Intercession"
attribute = ATTRIB_SMITING
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For 6 seconds, the next ${floor((smiting * (6 - 1) / 12) + 1)} times target other ally would take damage, 75% of that damage is redirected to you instead as holy damage, and that ally gains Health equal to your divine favour bonus. All of your non-Smiting Prayers skills are disabled for 10 seconds."
shortDescription = "Elite Enchantment Spell: For 6 seconds, the next 1...6...7 times target other ally would take damage, 75% of that damage is redirected to you instead as holy damage, and that ally gains Health equal to your divine favour bonus. All of your non-Smiting Prayers skills are disabled for 10 seconds."
icon = "Textures/Skills/Intercession.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 4000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetTypeAllyWithoutSelf
targetType = SkillTargetTarget

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, 6000)
  return SkillErrorNone
end
