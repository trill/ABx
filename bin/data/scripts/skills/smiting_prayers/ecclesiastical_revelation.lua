--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5253
name = "Ecclesiastical Revelation"
attribute = ATTRIB_SMITING
skillType = SkillTypeStance
isElite = false
description = "Stance: For 20 seconds, your Smiting Prayers spells activate and recharge ${floor(smiting * ((29 - 5) / 12) + 5)}% faster. All of your non-Smiting Prayers skills are disabled for 20 seconds."
shortDescription = "Stance: For 20 seconds, your Smiting Prayers spells activate and recharge 5...29...33% faster. All of your non-Smiting Prayers skills are disabled for 20 seconds."
icon = "Textures/Skills/Ecclesiastical Revelation.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 0
recharge = 14000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectFasterCast
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end

  source:AddEffect(source, self.Index, 20000)
  return SkillErrorNone
end

