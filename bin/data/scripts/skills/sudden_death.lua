--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 9998
name = "Sudden Death"
attribute = ATTRIB_NONE
skillType = 0
isElite = false
description = "Sudden Death"
shortDescription = "Instant suicide"
icon = "Textures/Skills/Monster Skill.png"
profession = PROFESSIONINDEX_NONE
soundEffect = ""
particleEffect = ""
access = SkillAccessGM

costEnergy = 0
costAdrenaline = 0
activation = 0
recharge = 0
overcast = 0
effect = SkillEffectDamage
effectTarget = SkillTargetSelf

function onStartUse(source, target)
  if (source:IsDead()) then
    -- Redundant check, because you can't use skills when you are dead
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:Die()
  return SkillErrorNone
end
