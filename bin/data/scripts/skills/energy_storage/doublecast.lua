--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5020
name = "Doublecast"
attribute = ATTRIB_ENERGY_STORAGE
skillType = SkillTypeSkill
isElite = true
description = "Elite Skill: For 15 seconds, the next spell you cast is cast twice, the second at ${energystorage * (90 - 25) / 12 + 25}% of the spell's attribute power."
shortDescription = "Elite Skill: For 15 seconds, the next spell you cast is cast twice, the second at 25...90...100% of the spell's attribute power."
icon = "Textures/Skills/Doublecast.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 1000
recharge = 60000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectNone
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self.Index, 15000)
  return SkillErrorNone
end
