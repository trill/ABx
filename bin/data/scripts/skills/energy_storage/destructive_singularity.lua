--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5019
name = "Destructive Singularity"
attribute = ATTRIB_ENERGY_STORAGE
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${energystorage * (60 - 10) / 12 + 10} seconds, your elemental spells gain an additional ${energystorage * (15 - 6) / 12 + 5}% armour penetration."
shortDescription = "Elite Enchantment Spell: For 10...60...70 seconds, your elemental spells gain an additional 5...15...20% armour penetration."
icon = "Textures/Skills/Destructive Singularity.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 1000
recharge = 60000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
-- TODO
effect = SkillEffectNone
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_ENERGY_STORAGE)
  local time = math.floor(attrib * (60 - 10) / 12 + 10)
  source:AddEffect(source, self.Index, time)
  return SkillErrorNone
end
