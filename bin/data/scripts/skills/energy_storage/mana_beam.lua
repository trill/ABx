--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5022
name = "Mana Beam"
attribute = ATTRIB_ENERGY_STORAGE
skillType = SkillTypeSpell
isElite = false
description = "Spell: Target foe and all foes between you and your target are struck for ${floor((energystorage * (90 - 30) / 12) + 30)} armour ignoring damage. You gain 1 Energy for every foe struck."
shortDescription = "Spell: Target foe and all foes between you and your target are struck for 30...90...110 armour ignoring damage. You gain 1 Energy for every foe struck."
icon = "Textures/Skills/Mana Beam.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 3000
recharge = 7000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_TYPELESS
effect = SkillEffectDamage
effectTarget = SkillTargetTarget | SkillTargetAoe
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source.Id == target.Id) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end

  local attrib = source:GetAttributeRank(ATTRIB_ENERGY_STORAGE)
  local damage = math.floor((attrib * (90 - 30) / 12) + 30)

  local count = 0
  local objects = source:RaycastToObject(target)
  for i, object in ipairs(objects) do
    local actor = object:AsActor()
    if (actor ~= nil) then
      if (not actor:IsDead() and source:IsEnemy(actor)) then
        actor:Damage(source, self.Index, damageType, damage, 0)
        count = count + 1
      end
    end
  end
  source:AddEnergy(count)
  return SkillErrorNone
end
