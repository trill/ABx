--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5216
name = "Pure Euthymia"
attribute = ATTRIB_HEALING
skillType = SkillTypeSpell
isElite = false
description = "Spell: Heal target ally for ${floor((healing * (80 - 15) / 12) + 15)} Health. Heal for an additional +${floor((healing * (40 - 10) / 12) + 10)} if that ally has no hexes or conditions."
shortDescription = "Spell: Heal target ally for 15...80...100 Health. Heal for an additional +10...40...50 if that ally has no hexes or conditions."
icon = "Textures/Skills/Pure Euthymia.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 750
recharge = 6000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local healing = source:GetAttributeRank(ATTRIB_HEALING)
  local hp = math.floor(healing * ((80 - 15) / 12) + 15)
  target:Healing(source, self.Index, hp)
  if (not target:HasEffectOf(EffectCategoryHex) and not target:HasEffectOf(EffectCategoryCondition)) then
    local moreHp = math.floor(healing * ((40 - 10) / 10) + 15)
    target:Healing(source, self.Index, moreHp)
  end

  local bonus = math.floor(getDevineFavorHealBonus(source) * 3)
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end

  return SkillErrorNone
end
