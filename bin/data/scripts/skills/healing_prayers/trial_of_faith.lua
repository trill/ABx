--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5218
name = "Trial of Faith"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: Enchant yourself with Oversee the Herd for 20 seconds. The next time the Health of any enchanted party member in earshot falls below 30%, this enchantment ends and that party member is healed for ${floor((healing * (90 - 15) / 12) + 15)} Health."
shortDescription = "Enchantment Spell: Enchant yourself with Oversee the Herd for 20 seconds. The next time the Health of any enchanted party member in earshot falls below 30%, this enchantment ends and that party member is healed for 15...90...110 Health."
icon = "Textures/Skills/Trial of Faith.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 12000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  target:AddEffect(source, self.Index, 10000)
  return SkillErrorNone
end
