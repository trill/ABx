--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5213
name = "Great Gospel"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For 10 seconds, all allies in earshot gain +${floor((healing * (5 - 3) / 12) + 3)} Health regeneration and are healed for an additional ${floor((healing * (5 - 3) / 12) + 3)} Health per second."
shortDescription = "Elite Enchantment Spell: For 10 seconds, all allies in earshot gain +3...5...6 Health regeneration and are healed for an additional 3...5...6 Health per second."
icon = "Textures/Skills/Great Gospel.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 10000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, 10000)
  return SkillErrorNone
end
