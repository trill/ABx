--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5217
name = "Tetragram Signet"
attribute = ATTRIB_HEALING
skillType = SkillTypeSignet
isElite = true
description = "Signet: Target other ally is healed for ${floor((healing * (12 - 4) / 12) + 4)} Health for every spell they have equipped."
shortDescription = "Signet: Target other ally is healed for 4...12...14 Health for every spell they have equipped."
icon = "Textures/Skills/Tetragram Signet.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 0
costAdrenaline = 0
activation = 250
recharge = 10000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target)) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local healing = source:GetAttributeRank(ATTRIB_HEALING)
  local hpFactor = math.floor(healing * ((12 - 4) / 12) + 4)
  local spellCount = 0
  local sb = target:GetSkillBar()
  local skillposs = sb:GetSkillsOfType(SkillTypeSpell)
  local hp = hpFactor * #skillposs

  target:Healing(source, self.Index, hp)

  local bonus = math.floor(getDevineFavorHealBonus(source) * 3)
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end

  return SkillErrorNone
end
