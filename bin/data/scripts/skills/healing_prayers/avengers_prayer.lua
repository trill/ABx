--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5207
name = "Avenger's Prayer"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = false
maintainable = true
description = "Enchantment Spell: While you maintain this enchantment, every time target ally hits a foe, all of their party members in earshot healed for ${floor(healing * (7 / 12) + 0.5)} Health."
shortDescription = "Enchantment Spell: While you maintain this enchantment, every time target ally hits a foe, all of their party members in earshot healed for 0...7...10 Health."
icon = "Textures/Skills/Avenger's Prayer.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0
costEnergyRegen = 1

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetParty
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (target:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  target:AddEffect(source, self.Index, TIME_FOREVER)
  return SkillErrorNone
end
