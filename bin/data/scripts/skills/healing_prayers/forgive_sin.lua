--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5211
name = "Forgive Sin"
attribute = ATTRIB_HEALING
skillType = SkillTypeSpell
isElite = false
description = "Spell: Heal target ally for ${floor(healing * (55 / 12) + 10)} Health and an additional +${floor(healing * (15 / 12) + 1)} Health for every hex spell that ally has equipped."
shortDescription = "Spell: Heal target ally for 10...55...70 Health and an additional +1...15...20 Health for every hex spell that ally has equipped."
icon = "Textures/Skills/Forgive Sin.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 6000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local attribVal = source:GetAttributeRank(ATTRIB_HEALING)
  local hp = math.floor(attribVal * (55 / 12) + 10)
  target:Healing(source, self.Index, hp)
  local sb = target:GetSkillBar()

  local attackSkills = 0
  for i = 0, MAX_SKILLS - 1, 1 do
    local skill = sb:GetSkill(i)
    if (skill ~= nil) then
      if (skill:IsType(SkillTypeHex)) then
        attackSkills = attackSkills + 1
      end
    end
  end
  if (attackSkills ~= 0) then
    local abhp = math.floor(attribVal * (15 / 12) + 1) * attackSkills
    target:Healing(source, self.Index, abhp)
  end

  local bonus = math.floor(getDevineFavorHealBonus(source))
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end
  return SkillErrorNone
end
