--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5208
name = "Concilliatory Prayer"
attribute = ATTRIB_HEALING
skillType = SkillTypeSpell
isElite = false
description = "Spell: Heal target other ally for ${floor(healing * (50 / 12) + 5)} Health, and an additional ${floor(healing * (70 / 12) + 10)} Health if that ally has less Health than you."
shortDescription = "Spell: Heal target other ally for 5...50...60 Health, and an additional 10...70...80 Health if that ally has less Health than you."
icon = "Textures/Skills/Concilliatory Prayer.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 4000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not heal what's dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local attribVal = source:GetAttributeRank(ATTRIB_HEALING)
  local hp = math.floor(attribVal * (50 / 12) + 5)
  target:Healing(source, self.Index, hp)
  if (source:GetResource(RESOURCE_TYPE_HEALTH) > target:GetResource(RESOURCE_TYPE_HEALTH)) then
    local addhp = math.floor(attribVal * (70 / 12) + 10)
    target:Healing(source, self.Index, addhp)
  end
  local bonus = math.floor(getDevineFavorHealBonus(source))
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end
  return SkillErrorNone
end
