--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5215
name = "Oversee the Herd"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: Enchant yourself with Oversee the Herd for 20 seconds. The next time the Health of any enchanted party member in earshot falls below 30%, this enchantment ends and that party member is healed for ${floor((healing * (90 - 15) / 12) + 15)} Health."
shortDescription = "Enchantment Spell: Enchant yourself with Oversee the Herd for 20 seconds. The next time the Health of any enchanted party member in earshot falls below 30%, this enchantment ends and that party member is healed for 15...90...110 Health."
icon = "Textures/Skills/Oversee the Herd.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 250
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, 20000)
  return SkillErrorNone
end
