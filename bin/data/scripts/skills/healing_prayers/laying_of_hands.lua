--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5214
name = "Laying of Hands"
attribute = ATTRIB_HEALING
skillType = SkillTypeSpell
isElite = true
description = "Elite Spell: Heal target touched ally for ${floor(healing * ((110 - 40) / 12) + 40)} Health. If the target is not yourself, also remove a condition and a hex. The Divine Favor bonus from this spell is tripled."
shortDescription = "Elite Spell: Heal target touched ally for 40...110...125 Health. If the target is not yourself, also remove a condition and a hex. The Divine Favor bonus from this spell is tripled."
icon = "Textures/Skills/Laying of Hands.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 750
recharge = 5000
overcast = 0
hp = 0

range = RANGE_TOUCH
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeAllyAndSelf

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == true) then
    -- Targets only allies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end
  local healing = source:GetAttributeRank(ATTRIB_HEALING)
  local hp = math.floor(healing * ((110 - 40) / 12) + 40)
  target:Healing(source, self.Index, hp)

  local bonus = math.floor(getDevineFavorHealBonus(source) * 3)
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end

  if (source.Id ~= target.Id) then
    target:RemoveLastEffectOf(EffectCategoryCondition)
    target:RemoveLastEffectOf(EffectCategoryHex)
  end
  return SkillErrorNone
end
