--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

index = 5212
name = "Glorify"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 3 seconds, the next time you heal an ally with a Monk spell, all allies adjacent to the target are healed for ${floor((healing * (80 - 10) / 12) + 10)}% of the spell's healing effect."
shortDescription = "Enchantment Spell: For 3 seconds, the next time you heal an ally with a Monk spell, all allies adjacent to the target are healed for 10...80...90% of the spell's healing effect."
icon = "Textures/Skills/Glorify.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 8000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetTarget | SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function canUse(source, target)
  if (source:HasEffect(self.Index)) then
    return SkillErrorNotAppropriate
  end
  if (not source:IsGroupFighting()) then
    return SkillErrorNotAppropriate
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (isSkillError(err)) then
    return err
  end
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  source:AddEffect(source, self.Index, 3000)
  return SkillErrorNone
end
