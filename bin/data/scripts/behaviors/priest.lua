--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/behaviors/shared.lua")

function init(root)
  local prio = node("Priority")
    prio:AddNode(stayAlive())
    prio:AddNode(rezzAlly())
    prio:AddNode(avoidSelfDamage())
    prio:AddNode(healAlly())
    prio:AddNode(utilitySkill())
    prio:AddNode(checkEnergy())
    prio:AddNode(defend())
    prio:AddNode(goHome())

  root:AddNode(prio)
end
