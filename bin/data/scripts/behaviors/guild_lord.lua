--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/behaviors/shared.lua")

function init(root)
  local prio = node("Priority")
    prio:AddNode(stayAlive())
    prio:AddNode(avoidSelfDamage())
    prio:AddNode(defend())
    prio:AddNode(damageSkill())
    prio:AddNode(attackAggro())
    prio:AddNode(checkEnergy())
    prio:AddNode(utilitySkill())
    prio:AddNode(goHome())

  root:AddNode(prio)
end
