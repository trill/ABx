--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/behaviors/shared.lua")

function init(root)
  local prio = node("Priority")
    prio:AddNode(goHomeIfNotThere(RANGE_EARSHOT))
    local sequence = node("Sequence")
      sequence:AddNode(idle(2000))
      sequence:AddNode(goRandomPos())
    prio:AddNode(sequence)
  root:AddNode(prio)
end
