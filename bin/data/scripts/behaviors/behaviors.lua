--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- Autoload these behavior trees into the cache

function init(cache)
  -- tree(name, script) creates and loads a whole BT, returns a Root node
  -- NPCs use this name to set the bevavior
  cache:Add(tree("guild_lord", "/scripts/behaviors/guild_lord.lua"))
  cache:Add(tree("marianna_gani", "/scripts/behaviors/marianna_gani.lua"))
  cache:Add(tree("dorothea_samara", "/scripts/behaviors/dorothea_samara.lua"))
  cache:Add(tree("priest", "/scripts/behaviors/priest.lua"))
  cache:Add(tree("smith", "/scripts/behaviors/smith.lua"))
  cache:Add(tree("elementarist", "/scripts/behaviors/elementarist.lua"))
  cache:Add(tree("minion", "/scripts/behaviors/minion.lua"))
  cache:Add(tree("ghoul", "/scripts/behaviors/ghoul.lua"))
  cache:Add(tree("mini_pet", "/scripts/behaviors/mini_pet.lua"))
  cache:Add(tree("pet", "/scripts/behaviors/pet.lua"))
  cache:Add(tree("turtle", "/scripts/behaviors/turtle.lua"))
end
