--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/behaviors/shared.lua")

function init(root)
  -- Minions are extremely silly
  local prio = node("Priority")
    prio:AddNode(damageSkill())
    prio:AddNode(attackAggro())
    prio:AddNode(goHome())

  root:AddNode(prio)
end
