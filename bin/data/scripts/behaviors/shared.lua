--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

local function avoidInterrupt()
  local result = node("Succeed")
    local nd = node("CounterInterrupt")
      nd:SetCondition(condition("WasInterrupted"))
    result:AddNode(nd)
  return result
end

local function useSkill(action)
  local result = node("Sequence")
    result:AddNode(avoidInterrupt())

    local skill = node(action)
      skill:SetCondition(condition("IsInSkillRange"))
    result:AddNode(skill)
  return result
end

local function _aoeDamage()
  local result = node("Sequence")
    result:AddNode(avoidInterrupt())
    local cond = condition("Filter")
      cond:SetFilter(filter("SelectMob"))
    result:SetCondition(cond)

    local skill = node("UseDamageSkill", { SkillTargetAoe })

    result:AddNode(skill)
  return result
end

local function _singleDamage()
  local result = node("Sequence")
    result:AddNode(avoidInterrupt())

    local cond = condition("Filter")
      cond:SetFilter(filter("SelectAttackTarget"))

    result:SetCondition(cond)

    local skill = node("UseDamageSkill", { SkillTargetTarget })
    result:AddNode(skill)
  return result
end

local function _interruptSpell()
  local result = node("Interrupt", { SkillTypeSpell })

    local cond = condition("Filter")
      cond:SetFilter(filter("SelectTargetUsingSkill", { SkillTypeSpell, "foe", 250 }))

    result:SetCondition(cond)
  return result
end

function interruptSpell()
  local result = node("Priority")
    result:AddNode(_interruptSpell())

    -- If out of range move to target
    local move = node("MoveIntoSkillRange")
      -- Only move there when not in range
      local notinrange = condition("Not")
        notinrange:AddCondition(condition("IsInSkillRange"))

      move:SetCondition(notinrange)
    result:AddNode(move)

  return result
end

local function _interruptAttack()
  local result = node("Interrupt", { SkillTypeAttack })

    local orCond = condition("Or")
      local skillCond = condition("Filter")
        skillCond:SetFilter(filter("SelectTargetUsingSkill", { SkillTypeAttack, "foe" }))
      orCond:AddCondition(skillCond)

      local attackCond = condition("Filter")
        attackCond:SetFilter(filter("SelectTargetAttacking"))
      orCond:AddCondition(attackCond)

    result:SetCondition(orCond)
  return result
end

function interruptAttack()
  local result = node("Priority")
    result:AddNode(_interruptAttack())

    -- If out of range move to target
    local move = node("MoveIntoSkillRange")
      -- Only move there when not in range
      local notinrange = condition("Not")
        notinrange:AddCondition(condition("IsInSkillRange"))

      move:SetCondition(notinrange)
    result:AddNode(move)

  return result
end

local function avoidSelfMeleeDamage()
  -- Dodge melee attacks
  local result = node("Priority")
    result:AddNode(node("Block"))

    local flee = node("Flee")
      flee:SetCondition(condition("IsSelfHealthCritical"))

    result:AddNode(flee)

  result:SetCondition(condition("IsMeleeTarget"))
  return result
end

local function avoidSelfAoeDamage()
  -- Move out of AOE
  local nd = node("MoveOutAOE")
    nd:SetCondition(condition("IsInAOE"))
  return nd
end

-- Make any damage to a target
function damageSkill()
  local result = node("Priority")
    result:AddNode(_aoeDamage())
    result:AddNode(_singleDamage())
    result:AddNode(_interruptSpell())
    result:AddNode(_interruptAttack())

    -- If out of range move to target
    local move = node("MoveIntoSkillRange")
      -- Only move there when not in range
      local notinrange = condition("Not")
        notinrange:AddCondition(condition("IsInSkillRange"))

      move:SetCondition(notinrange)
    result:AddNode(move)

    result:SetCondition(condition("IsCombatMode", { COMBAT_MODE_FIGHT }))
  return result
end

function checkEnergy()
  local result = useSkill("GainEnergy")
    result:SetCondition(condition("IsEnergyLow"))
  return result
end

function utilitySkill()
  return useSkill("UseUtilitySkill")
end

function idle(time)
  return node("Idle", { time })
end

function goHome()
  local nd = node("GoHome")
    nd:SetCondition(condition("HaveHome"))
  return nd
end

function goHomeIfNotThere(range)
  local nd = node("GoHome")
    local andCond = condition("And")
      andCond:AddCondition(condition("HaveHome"))
      
      local notthere = condition("Not")
        notthere:AddCondition(condition("IsAtHome", { range }))
      
      andCond:AddCondition(notthere)

    nd:SetCondition(andCond)
  return nd
end

function goRandomPos()
  return node("GotoRandomPos")
end

function wander()
  local nd = node("Wander")
    nd:SetCondition(condition("HaveWanderRoute"))
  return nd
end

function avoidSelfDamage()
  local nd = node("Priority")
    nd:AddNode(avoidSelfMeleeDamage())
    nd:AddNode(avoidSelfAoeDamage())
  return nd
end

function stayAlive()
  -- Execute the first child that does not fail
  local result = node("Priority")
    local low = useSkill("HealSelf")
      low:SetCondition(condition("IsSelfHealthLow"))

    result:AddNode(low)

    local critical = node("Flee")
      critical:SetCondition(condition("IsSelfHealthCritical"))
    result:AddNode(critical)
  return result
end

function defend()
  local nd = node("AttackSelection")
    -- If we are getting attacked AND there is an attacker
    local andCond = condition("And")
      andCond:AddCondition(condition("IsAttacked"))
      local haveAttackers = condition("Filter")
        haveAttackers:SetFilter(filter("SelectAttackers"))
      andCond:AddCondition(haveAttackers)
    local notAvoid = condition("Not")
        notAvoid:AddCondition(condition("IsCombatMode", { COMBAT_MODE_AVOID }))
      andCond:AddCondition(notAvoid)
    nd:SetCondition(andCond)
  return nd
end

function healAlly()
  -- Priority: Execute the first child that does not fail, either HealOther or MoveTo
  local nd = node("Priority")
    local andCond = condition("And")
      andCond:AddCondition(condition("IsAllyHealthLow"))
      local haveTargets = condition("Filter")
        haveTargets:SetFilter(filter("SelectLowHealth"))
      andCond:AddCondition(haveTargets)
    nd:SetCondition(andCond)
    -- Heal fails if out of range
    nd:AddNode(useSkill("HealOther"))

    -- If out of range move to target
    local move = node("MoveIntoSkillRange")
      -- Only move there when not in range
      local notinrange = condition("Not")
        notinrange:AddCondition(condition("IsInSkillRange"))

      move:SetCondition(notinrange)

    -- If out of range move to target
    nd:AddNode(move)

  return nd
end

function attackAggro()
  -- Melee attack without skills
  local result = node("AttackSelection")
      local andCond = condition("And")
        andCond:AddCondition(condition("IsCombatMode", { COMBAT_MODE_FIGHT }))

        local haveAggro = condition("Filter")
          haveAggro:SetFilter(filter("SelectAttackTarget"))
        andCond:AddCondition(haveAggro)
      
    result:SetCondition(andCond)
  return result
end

function rezzAlly()
  local nd = node("Priority")
    nd:SetCondition(condition("HaveResurrection"))

    local haveDeadAllies = condition("Filter")
      haveDeadAllies:SetFilter(filter("SelectDeadAllies"))
    nd:SetCondition(haveDeadAllies)

    nd:AddNode(useSkill("ResurrectSelection"))

    -- If out of range move to target
    local move = node("MoveIntoSkillRange")
      -- Only move there when not in range
      local notinrange = condition("Not")
        notinrange:AddCondition(condition("IsInSkillRange"))

      move:SetCondition(notinrange)
    nd:AddNode(move)

  return nd
end
