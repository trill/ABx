--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/chat.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/commands.lua")

function onInit()
end

function onUpdate(timeElapsed)
end

function onEnter()
  self:Command(Command.Sit, "")
end
