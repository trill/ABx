--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/monk.lua")

isPersistent = false

index = 5209
name = "Fleeting Spirit"
icon = "Textures/Skills/Fleeting Spirit.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

isPersistent = false

function getDuration(source, target)
  return 2000
end

function onStart(source, target)
  return true
end

function onEnd(source, target)
  -- No effect when it was removed before ended
  if (target == nil) then
    return
  end
  if (target:IsDead()) then
    return
  end
  local attribVal = source:GetAttributeRank(ATTRIB_HEALING)
  local hp = math.floor(attribVal * (85 / 12) + 5)
  target:Healing(source, self.Index, hp)
  if (target:IsMoving()) then
    target:Healing(source, self.Index, math.floor(hp * 0.5))
  end
  local bonus = math.floor(getDevineFavorHealBonus(source))
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end
end
