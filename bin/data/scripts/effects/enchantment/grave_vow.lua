--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5333
name = "Grave Vow"
icon = "Textures/Skills/Grave Vow.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local energyGain = 0

function onStart(source, target)
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  energyGain = math.floor((death * 5) / 12)
  return true
end

function onExploitedCorpse(source, corpse)
  source:AddEnergy(energyGain)
end
