--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5407
name = "Viper Eyes"
icon = "Textures/Skills/Viper Eyes.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local bonus = 0

function onStart(source, target)
  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  bonus = (blood * ((150 - 75) / 12) + 75) / 100
  return true
end

function onDealingDamage(source, target, type, value)
  return true, value + math.floor(value * bonus)
end
