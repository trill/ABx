--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5240
name = "Aegis Against Ailments"
icon = "Textures/Skills/Aegis Against Ailments.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function onAddEffect(source, target, effect, time)
  if (effect.Index == 10002 or effect.Index == 10009 or effect.Index == 10010) then
    return false, time
  end
  return true, time
end
