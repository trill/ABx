--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5404
name = "Order of the Wasp"
icon = "Textures/Skills/Order of the Wasp.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function onSourceAttackSuccess(source, target, type, damage)
  source:AddEnergy(1)
end
