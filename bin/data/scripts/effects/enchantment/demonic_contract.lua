--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5401
name = "Demonic Contract"
icon = "Textures/Skills/Demonic Contract.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function getRegeneration(healthRegen, energyRegen)
  return healthRegen, energyRegen + 1
end

function onEnd(source, target)
  target:AddEnergy(5)
end

function onRemove(source, target)
  local maxHealth = target:GetResource(RESOURCE_TYPE_MAXHEALTH)
  local loss = math.floor(maxHealth * 0.2)
  target:DrainLife(loss)
end
