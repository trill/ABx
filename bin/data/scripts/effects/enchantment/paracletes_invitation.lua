--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5243
name = "Paraclete's Invitation"
icon = "Textures/Skills/Paraclete's Invitation.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local maxDamage = 0

function onStart(source, target)
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  maxDamage = math.floor((protection * (150 - 5) / 12) + 5)
  return true
end

function onGettingDamage(source, target, skill, type, value)
  local maxHealth = target:GetResource(RESOURCE_TYPE_MAXHEALTH)
  local maxDamage = math.floor(maxHealth * 0.07)
  if (maxDamage > value) then
    return maxDamage
  end
  return value
end

function onRemove(source, target)
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  local healing = math.floor((protection * (85 - 20) / 12) + 20)
  target:Healing(source, self.Index, healing)
end
