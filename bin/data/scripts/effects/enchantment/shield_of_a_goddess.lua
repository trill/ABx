--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5244
name = "Shield of a Goddess"
icon = "Textures/Skills/Shield of a Goddess.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local reduction = 0

function onStart(source, target)
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  reduction = math.floor((protection * (20 - 10) / 12) + 10) / 100
  return true
end

function onGettingDamage(source, target, skill, type, value)
  return value * reduction
end

