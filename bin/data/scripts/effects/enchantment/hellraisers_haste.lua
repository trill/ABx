--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5037
name = "Hellraiser's Haste"
icon = "Textures/Skills/Hellraiser's Haste.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  if (skill:IsType(SkillTypeSpell) and skill:GetAttribute() == ATTRIB_FIRE) then
    return math.floor(activation * 0.66), energy, adrenaline, overcast, hp
  end
  return activation, energy, adrenaline, overcast, hp
end

function getSkillRecharge(skill, recharge)
  if (skill:IsType(SkillTypeSpell) and skill:GetAttribute() == ATTRIB_FIRE) then
    return math.floor(recharge * 0.66)
  end
  return recharge
end
