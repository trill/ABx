--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5147
name = "Pensive Incantation"
icon = "Textures/Skills/Pensive Incantation.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local energyGain = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  energyGain = ((inspiration * (150 - 50) / 12) + 50) / 100
  return true
end

function getSkillActivation(skill, activation)
  return math.floor(activation * 2)
end

function onEndUse(source, target, skill)
  local energy = skill:GetEnergy()
  source:AddEnergy(math.floor(energy * energyGain))
end
