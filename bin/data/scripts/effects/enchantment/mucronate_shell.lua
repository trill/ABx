--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5403
name = "Mucronate Shell"
icon = "Textures/Skills/Mucronate Shell.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local addArmor = 0
local armorAddition = 0
local maxArmor = 0

function onStart(source, target)
  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  armorAddition = math.floor((blood * 5) / 12)
  addArmor = armorAddition
  maxArmor = math.floor((target:GetLevel() * 3) * 0.5)
  return true
end

function getArmor(type, value)
  return value + addArmor
end

function onActorDied(actor, killer)
  addArmor = addArmor + armorAddition
  if (addArmor > maxArmor) then
    addArmor = maxArmor
  end
end
