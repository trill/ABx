--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5035
name = "Flaming Fists"
icon = "Textures/Skills/Flaming Fists.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damageBonus = 0

function onStart(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_FIRE)
  damageBonus = (attribVal * (20 - 10) / 12 + 10) / 100
  return true
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  if (skill:GetRange() == RANGE_TOUCH and skill:GetAttribute() == ATTRIB_FIRE) then
    return activation, math.floor(energy + 5), adrenaline, overcast, hp
  end
  return activation, energy, adrenaline, overcast, hp
end

function getSkillDamageBonus(source, target, skill, damageType, value)
  if (skill:GetRange() == RANGE_TOUCH and skill:GetAttribute() == ATTRIB_FIRE) then
    return math.floor(value + (value * damageBonus))
  end
  return value
end
