--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5200
name = "Apostolicity"
icon = "Textures/Skills/Apostolicity.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function onEndUseSkill(source, target, skill)
  if (target == nil) then
    return
  end
  if (not source:IsAlly(target)) then
    return
  end
  if (skill:IsType(SkillTypeSpell) and isSkillSuccess(skill:GetLastError())) then
    local effect = target:GetLastEffect(EffectCategoryCondition)
    if (effect ~= nil) then
      target:RemoveEffect(effect.Index)
      local energy = self:DrainEnergy(5)
      if (energy ~= 5) then
        source:RemoveEffect(self.Index)
      end
    end
  end
end
