--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5242
name = "Gardenia's Gaze"
icon = "Textures/Skills/Gardenia's Gaze.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local maxDamage = 0
local preventedDamage = 0

function onStart(source, target)
  local protection = source:GetAttributeRank(ATTRIB_PROTECTION)
  maxDamage = math.floor((protection * (150 - 5) / 12) + 5)
  return true
end

function onGettingDamage(source, target, skill, type, value)
  preventedDamage = preventedDamage + value
  if (preventedDamage <= maxDamage) then
    return 0
  end
  self:Remove()
  return value
end
