--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/monk.lua")

isPersistent = false

index = 5218
name = "Trial of Faith"
icon = "Textures/Skills/Trial of Faith.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healingFactor = 0
local damage = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_HEALING)
  healingFactor = math.floor((attrib * (45 - 1) / 12) + 1) / 100
  return true
end

function onGotDamage(source, target, skill, type, value)
  if (type ~= DAMAGETYPE_LIFEDRAIN) then
    damage = damage + value
  end
end

function onEnd(source, target)
  -- No effect when it was removed before ended
  if (target == nil) then
    return
  end
  if (target:IsDead()) then
    return
  end
  local hp = math.floor(healingFactor * damage)
  target:Healing(source, self.Index, hp)

  local bonus = math.floor(getDevineFavorHealBonus(source))
  if (bonus ~= 0) then
    target:Healing(source, self.Index, bonus)
  end
end

