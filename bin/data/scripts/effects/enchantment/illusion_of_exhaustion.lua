--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5133
name = "Illusion of Exhaustion"
icon = "Textures/Skills/Illusion of Exhaustion"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local bellow = 0
local gain = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_ILLUSION)
  bellow = math.floor(attribVal * ((10 - 1) / 12) + 1)
  gain = math.floor(attribVal * ((17 - 5) / 12) + 5)
  return true
end

function onUpdate(source, target, time)
  if (source:GetResource(RESOURCE_TYPE_ENERGY) < bellow) then
    source:AddEnergy(gain)
    source:RemoveEffect(self.Index)
  end
end
