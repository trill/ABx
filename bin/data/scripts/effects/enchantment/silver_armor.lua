--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5015
name = "Silver Armor"
icon = "Textures/Skills/Silver Armor.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local numberFoes = 0
local damage = 0

function onStart(source, target)
  local earth = source:GetAttributeRank(ATTRIB_EARTH)
  numberFoes = math.floor(earth * (8 / 12))
  damage = math.floor(earth * ((45 - 8) / 12) + 8)
  return true
end


-- Source targeted us successfully
-- source: Source of skill.
-- target: Target of skill, the actor with this effect.
function onSkillTargeted(source, target, skill)
  -- TODO: Adjecent to source or target? Armor would imply target.
  if (target:IsEnemy(source)) then
    local count = 0
    local actors = source:GetAlliesInRange(RANGE_ADJECENT)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead()) then
        actor:Damage(target, self.Index, DAMAGETYPE_EARTH, damage, 0.0)
        count = count + 1
        if (count > numberFoes) then
          return
        end
      end
    end
  end
end
