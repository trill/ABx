--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5201
name = "Divine Perfection"
icon = "Textures/Skills/Divine Perfection.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local increase = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DEVINE_FAVOUR)
  increase = math.floor(attrib * (1 / 12) + 0.5)

  return true
end

function getAttributeRank(index, value)
  if (index == ATTRIB_HEALING or index == ATTRIB_SMITING or index == ATTRIB_PROTECTION or index == ATTRIB_DEVINE_FAVOUR) then
    return value + increase
  end
  return value
end

function onUpdate(source, target, time)
  local diff = source:GetResource(RESOURCE_TYPE_HEALTH) - source:GetResource(RESOURCE_TYPE_MAXHEALTH)
  if (diff < 0) then
    self:Remove()
  end
end
