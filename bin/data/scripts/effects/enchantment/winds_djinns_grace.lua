--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5007
name = "Wind Djinn's Grace"
icon = "Textures/Skills/Wind Djinn's Grace.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local armor = 60
local lastTime = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_AIR)
  armor = attribVal * ((45 - 10) / 12) * 10
  return true
end

function getArmor(type, value)
  return armor
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation * 0.75), energy, adrenaline, overcast, hp
end

function onUpdate(source, target, time)
  lastTime = lastTime + time
  if (lastTime >= 1000) then
    if (source:IsMoving()) then
      local energy = math.ceil(source:GetAttributeRank(ATTRIB_ENERGY_STORAGE) / 10)
      source:AddEnergy(energy)
    end
    lastTime = 0
  end
end
