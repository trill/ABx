--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5030
name = "Arsonist's Aura"
icon = "Textures/Skills/Arsonist's Aura.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damage = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_FIRE)
  damage = attribVal * ((40 - 5) / 12) + 5
  return true
end

function onStartUseSkill(source, target, skill)
  if (skill:GetAttribute() == ATTRIB_FIRE) then
    local actors = source:GetActorsInRange(RANGE_ADJECENT)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(target)) then
        actor:Damage(source, self.Index, DAMAGETYPE_FIRE, damage, 0.0)
        actor:AddEffect(source, 10004, 1000)
      end
    end
  end
end
