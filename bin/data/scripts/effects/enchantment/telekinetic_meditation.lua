--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5148
name = "Telekinetic Meditation"
icon = "Textures/Skills/Telekinetic Meditation.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local energyGain = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  energyGain = ((inspiration * (50 - 10) / 12) + 10) / 100
  return true
end

function onEndUse(source, target, skill)
  local energy = skill:GetEnergy()
  source:AddEnergy(math.floor(energy * energyGain))
end
