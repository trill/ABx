--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5600
name = "Aura of the Countess"
icon = "Textures/Skills/Aura of the Countess.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local energy = 0

function onStart(source, target)
  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  energy = math.floor(soulreaping * ((7 - 1) / 12) + 1)
  return true
end

function getEnergyFromSoulReaping(source, target, value)
  return 0
end

function onEndUseSkill(source, target, skill)
  if (souce:IsEnemy(target)) then
    souce:AddEnergy(energy)
  end
end
