--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5019
name = "Destructive Singularity"
icon = "Textures/Skills/Destructive Singularity.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local penetration = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_ENERGY_STORAGE)
  penetration = attribVal * ((15 - 5) / 12) + 5
  return true
end

function getSourceArmorPenetration(target, damageType, value)
  return value + (value * penetration)
end
