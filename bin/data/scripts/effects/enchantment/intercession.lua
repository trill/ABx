--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5256
name = "Intercession"
icon = "Textures/Skills/Intercession.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local times = 0
local count = 0

function onStart(source, target)
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  times = math.floor((smiting * (6 - 1) / 12) + 1)

  local sb = source:GetSkillBar()

  for i = 0, MAX_SKILLS - 1, 1 do
    local skill = sb:GetSkill(i)
    if (skill ~= nil) then
      if (skill:GetAttribute() ~= ATTRIB_SMITING) then
        skill:AddRecharge(10000)
      end
    end
  end

  return true
end

function onGettingDamage(source, target, skill, type, value)
  count = count + 1
  if (times > count) then
    self:Remove()
    return value
  end

  local per = math.floor(value * 0.75)
  self.Source:Damage(source, self.Index, DAMAGETYPE_HOLY, per, 0.0)
  return value - per
end

