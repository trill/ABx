--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5013
name = "Mind Lock"
icon = "Textures/Skills/Mind Lock.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

-- source adds the effect to target
function onAddEffect(source, target, effect, time)
  if (effect:GetCategory() == EffectCategoryHex) then
    return false, time
  end
  return true, time
end
