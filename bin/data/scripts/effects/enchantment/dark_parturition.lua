--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

isPersistent = false

index = 5329
name = "Dark Parturition"
icon = "Textures/Skills/Dark Parturition.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damage = 0

function onStart(source, target)
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  damage = math.floor(death * ((40 - 20) / 12) + 20)
  return true
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  if (skill:HasEffect(SkillEffectSummonMinion)) then
    return skill, activation, energy, adrenaline, overcast, hp + 5
  end
  return skill, activation, energy, adrenaline, overcast, hp
end

function onSpawnedSlave(source, slave)
  if (slave.SlaveKind == SLAVE_KIND_MINION) then
    local actors = slave:GetActorsInRange(RANGE_NEARBY)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(source)) then
        actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
      end
    end
  end
end
