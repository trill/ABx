--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

isPersistent = false

index = 5508
name = "Netherworldly Benefactor"
icon = "Textures/Skills/Netherworldly Benefactor.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local curses = 0

function onStart(source, target)
  curses = target:GetAttributeRank(ATTRIB_CURSES)
  return true
end

function getAttributeRank(index, value)
  -- Careful: Can't call Actor:GetAttributeRank() here, because of recusrion!
  return curses
end
