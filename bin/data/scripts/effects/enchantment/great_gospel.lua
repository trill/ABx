--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/monk.lua")

isPersistent = false

index = 5213
name = "Great Gospel"
icon = "Textures/Skills/Great Gospel.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local bonus = 0
local lastHealing = 0

function onStart(source, target)
  local healing = source:GetAttributeRank(ATTRIB_HEALING)
  bonus = math.floor((healing * (5 - 3) / 12) + 3)
  return true
end

function getRegeneration(health, energy)
  return health + bonus, energy
end

function onUpdate(source, target, time)
  lastHealing = lastHealing + time
  if (lastHealing >= 1000) then
    target:Healing(source, self.Index, bonus)
    lastHealing = 0
  end
end

