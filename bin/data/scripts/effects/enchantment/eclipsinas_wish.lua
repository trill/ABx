--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5032
name = "Eclipsina's Wish"
icon = "Textures/Skills/Eclipsina's Wish.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local fireTime = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_FIRE)
  fireTime = math.floor(attribVal * ((9 - 1) / 12) + 1)
  return true
end

function onDied(actor, killer)
  local actors = actor:GetEnemiesInRange(RANGE_ADJECENT)
  for i, a in ipairs(actors) do
    if (not a:IsDead()) then
      a:KnockDown(self.Source, 0)
      a:AddEffect(self.Source, 10004, fireTime)
      -- This is interesting there is a Eclipsina's Wish enchantment and a Eclipsina's Wish hex
      a:AddEffect(self.Source, 15032, 8000)
    end
  end

end
