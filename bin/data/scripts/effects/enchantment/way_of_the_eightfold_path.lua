--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5245
name = "Way of the Eightfold Path"
icon = "Textures/Skills/Way of the Eightfold Path.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local enchSpells = 0
local hexSpells = 0
local otherSpells = 0
local attackSkills = 0
local stances = 0
local rituals = 0
local signets = 0
local traps = 0
local shouts = 0

function onStart(source, target)
  local sb = target:GetSkillBar()
  local _enchs = sb:GetSkillsOfType(SkillTypeEnchantment)
  enchSpells = #_enchs
  local _hexes = sb:GetSkillsOfType(SkillTypeHex)
  hexSpells = #_hexes
  local _spells = sb:GetSkillsOfType(SkillTypeSpell)
  otherSpells = #_spells - enchSpells - hexSpells
  local _attacks = sb:GetSkillsOfType(SkillTypeAttack)
  attackSkills = #_attacks
  local _stances = sb:GetSkillsOfType(SkillTypeStance)
  stances = #_stances
  local _rituals = sb:GetSkillsOfType(SkillTypeRitual)
  rituals = #_rituals
  local _signets = sb:GetSkillsOfType(SkillTypeSignet)
  signets = #_signets
  local _traps = sb:GetSkillsOfType(SkillTypeTrap)
  traps = #_traps
  local _shouts = sb:GetSkillsOfType(SkillTypeShout)
  shouts = #_shouts

  return true
end

function onGettingDamage(source, target, skill, type, value)
  if (skill ~= nil) then
    value = value - otherSpells
  end
  return value - (value * (0.05 * enchSpells))
end

function onAddEffect(source, target, effect, time)
  local t = time
  if (effect:GetCategory(EffectCategoryHex)) then
    t = t - (t * (0.05 * hexSpells))
  end
  if (effect:GetCategory(EffectCategoryCondition)) then
    t = t - (t * (0.1 * traps))
  end
  return true, t
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor + (speedFactor * (0.15 * stances)), false
end

function getRegeneration(health, energy)
  return health + signets, energy + shouts
end

function onAttacked(source, target, type, damage)
  if (Random() <= ((stances * 10) / 100)) then
    return ATTACK_ERROR_BLOCKED
  end
  return ATTACK_ERROR_NONE
end
