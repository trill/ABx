--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5004
name = "Stormseeker"
icon = "Textures/Skills/Stormseeker.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local penetration

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_AIR)
  penetration = attribVal * (5 / 12)
  return true
end

function getArmorPenetration(damageType, value)
  if (damageType == DAMAGETYPE_COLD or damageType == DAMAGETYPE_LIGHTNING) then
    return value + (penetration / 100)
  end
  return value
end
