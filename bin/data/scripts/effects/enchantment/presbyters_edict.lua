--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/monk.lua")

isPersistent = false

index = 5230
name = "Presbyter's Edict"
icon = "Textures/Skills/Presbyter's Edict.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function onHealedTarget(source, target, skill, value)
  if (target == nil) then
    return
  end
  if (skill == nil) then
    return
  end
  if (skill:GetAttribute() ~= ATTRIB_HEALING) then
    return
  end
  local bonus = getDevineFavorHealBonus(source)
  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  local count = 0
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      target:Healing(source, self.Index, bonus)
      count = count + 1
    end
  end
  source:DrainEnergy(count)
end

function onMaintainerEnergyZero(source)
  self:Remove()
end

function getMaintainerRegeneration(health, energy)
  return health, energy - 1
end
