--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5008
name = "Basalt Bastion"
icon = "Textures/Skills/Basalt Bastion.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local factor = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  factor = (attribVal * ((30 - 25) / 12) + 25) / 100
  return true
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor - factor, false
end

function onInterruptingSkill(source, target, type, skill)
  return false
end

