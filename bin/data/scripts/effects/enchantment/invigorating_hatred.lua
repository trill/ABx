--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5601
name = "Invigorating Hatred"
icon = "Textures/Skills/Invigorating Hatred.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healing = 0

function onStart(source, target)
  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  healing = math.floor(soulreaping * ((20 - 1) / 12) + 1)
  return true
end

function onActorDied(actor, killer)
  local target = self.Target
  if (target:IsInRange(RANGE_EARSHOT, actor)) then
    target:Healing(self.Source, self.Index, healing)
  end
end

