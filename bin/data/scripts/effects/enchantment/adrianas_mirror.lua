--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5050
name = "Adriana's Mirror"
icon = "Textures/Skills/Adriana's Mirror.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local reduction = 0

function onStart(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_WATER)
  reduction = math.floor((attribVal * (50 - 5) / 12 + 5) / 100)
  return true
end

function onGettingDamage(source, target, skill, damageType, value)
  if (skill:IsType(SkillTypeSpell)) then
    local result = math.floor(value - (value * reduction))
    if (not source:HasEffect(self.Index)) then
      -- When both have this effect don't apply a damage -> would recursivley call this
      local damage = math.max(math.floor((result / 100) * reduction), 100)
      source:Damage(target, self.Index, damageType, damage, 0)
    end
    return result
  end
  return value
end
