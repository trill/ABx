--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5207
name = "Avenger's Prayer"
icon = "Textures/Skills/Avenger's Prayer.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healing = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_HEALING)
  healing = math.floor(attrib * (7 / 12) + 0.5)
  return true
end

function onSourceAttackSuccess(source, target, damageType, damage)
  local actors = source:GetAlliesInRange(RANGE_EARSHOT)
  local effectSource = self.Source
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Healing(effectSource, self.Index, healing)
    end
  end
end

function onMaintainerEnergyZero(source)
  self:Remove()
end

function getMaintainerRegeneration(health, energy)
  return health, energy - 1
end
