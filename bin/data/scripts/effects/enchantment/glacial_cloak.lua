--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5055
name = "Glacial Cloak"
icon = "Textures/Skills/Glacial Cloak.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local armorAdd = 0
local reduction = 0
local healthRegen = 0

function onStart(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_WATER)
  armorAdd = math.floor((attribVal * (25 - 1) / 12 + 1))
  reduction = math.floor((attribVal * 3) / 12)
  healthRegen = math.floor((attribVal * (4 - 1) / 12 + 1))
  return true
end

function getRegeneration(health, energy)
  return health + healthRegen, energy
end

function getArmor(type, value)
  return math.floor(value + armorAdd)
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor - 0.5, false
end

function onStartUseSkill(source, target, skill)
  source:RemoveEffect(self.Index)
end

function onGettingDamage(source, target, skill, damageType, value)
  return math.floor(math.max(value - reduction, 0))
end
