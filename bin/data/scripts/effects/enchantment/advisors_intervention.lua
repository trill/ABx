--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5251
name = "Advisor's Intervention"
icon = "Textures/Skills/Advisor's Intervention.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local reduction = 0
local energyAdd = 0
local healing = 0

function onStart(source, target)
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  reduction = math.floor((smiting * (50 - 1) / 12) + 1)
  energyAdd = math.floor((smiting * (2 - 1) / 12) + 2)
  healing = math.floor((smiting * (90 - 50) / 12) + 50)
  return true
end

function onGettingDamage(source, target, skill, type, value)
  target:AddEnergy(energyAdd)
  local hp = target:GetResource(RESOURCE_TYPE_HEALTH)
  if (value >= hp) then
      actor:Healing(source, self.Index, healing)
  end
  return value - reduction
end

