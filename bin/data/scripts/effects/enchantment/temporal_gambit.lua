--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5149
name = "Temporal Gambit"
icon = "Textures/Skills/Temporal Gambit.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local energyGain = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  energyGain = math.floor((inspiration * 7) / 12)
  return true
end

function onInterrupted(source, target, skill, success)
  if (skill == nil) then
    return
  end
  if (skill:GetProfession() ~= PROFESSIONINDEX_MESMER) then
    return
  end
  if (success) then
    source:AddEnergy(energyGain)
  else
    source:DrainEnergy(3)
  end

end
