--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5052
name = "Cryogenic Sleep"
icon = "Textures/Skills/Cryogenic Sleep.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healing = 0
local energyGain = 0

function onStart(source, target)
  local attribVal = target:GetAttributeRank(ATTRIB_WATER)
  healing = math.floor(attribVal * (150 - 60) / 12 + 60)
  energyGain = math.floor(attribVal * (24 - 3) / 12 + 3)

  return true
end

function getRegeneration(health, energy)
  return health - 10, energy - 10
end

function onEnd(source, target)
  target:Healing(source, self.Index, healing)
  target:AddEnergy(energyGain)
end

function getSpeedFactor(speedFactor)
  -- Set speed to zero and stop iterating other effects
  return 0.0, true
end

function onGettingDamage(source, target, skill, damageType, value)
  return math.max(value - 10, 0)
end

