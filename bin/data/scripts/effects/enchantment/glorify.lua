--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5212
name = "Glorify"
icon = "Textures/Skills/Glorify.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local bonus = 0

function onStart(source, target)
  local healing = source:GetAttributeRank(ATTRIB_HEALING)
  bonus = ((healing * (80 - 10) / 12) + 10) / 100
  return true
end

function onHealedTarget(source, target, skill, value)
  if (isSkillError(skill:GetLastError())) then
    return
  end
  if (skill:GetProfession() ~= PROFESSIONINDEX_MONK) then
    return
  end
  if (not skill:HasEffect(SkillEffectHeal)) then
    return
  end
  if (skill.Index == self.Index) then
    return
  end
  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Healing(source, self.Index, math.floor(value * bonus))
    end
  end
end
