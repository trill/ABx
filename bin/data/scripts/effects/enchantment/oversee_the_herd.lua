--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/monk.lua")

isPersistent = false

index = 5215
name = "Oversee the Herd"
icon = "Textures/Skills/Oversee the Herd.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healing = 0
local lastCheck = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_HEALING)
  healing = math.floor((attrib * (90 - 15) / 12) + 15)
  return true
end

function onUpdate(source, target, time)
  lastCheck = lastCheck + time
  if (lastCheck >= 500) then
    local bonus = getDevineFavorHealBonus(source)
    local actors = source:GetGroupMembersRange(RANGE_EARSHOT)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:HasEffectOf(EffectCategoryEnchantment) and actor:GetHealthRatio() < 0.3) then
        actor:Healing(source, self.Index, healing)
        actor:Healing(source, self.Index, bonus)
      end
    end
    lastCheck = 0
  end
end

