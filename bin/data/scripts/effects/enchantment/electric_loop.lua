--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5002
name = "Electric Loop"
icon = "Textures/Skills/Electric Loop.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damage = 0
local lastDamage = 0

function onStart(source, target)
  -- for 1...12...16

  local air = target:GetAttributeRank(ATTRIB_AIR)
  damage = math.floor((air * (12 - 1) / 12) + 1)
  return true
end

function onUpdate(source, target, time)
  if (target == nil) then
    return
  end
  lastDamage = lastDamage + time
  if (lastDamage >= 1000) then
    local actors = target:GetActorsInRange(RANGE_TOUCH)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and actor:IsEnemy(target)) then
        actor:Damage(target, self.Index, DAMAGETYPE_LIGHTNING, damage, 0.25)
      end
    end
    lastDamage = 0
  end
end
