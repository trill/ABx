--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5162
name = "Power Struggle"
icon = "Textures/Skills/Power Struggle.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

isPersistent = false
internal = true

function onStart(source, target)
  return true
end

function onInterrupted(source, target, skill, success)
  if (not success) then
    return
  end
  local me = self:GetTTarget()
  local sb = me:GetSkillBar()
  local s = sb:GetSkillBySkillIndex(self.Index)
  if (s ~= nil) then
    s:SetRecharged(0)
  end
  self:Remove()
end
