--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5142
name = "Fade Out"
icon = "Textures/Skills/Fade Out.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

isPersistent = false
internal = false

local shorten = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  shorten = ((inspiration * (80 - 20) / 12) + 20) / 100
  return true
end

-- source adds the effect to target
function onAddEffect(source, target, effect, time)
  if (effect.Index == self.Index) then
    return true, time
  end
  local cat = effect:GetCategory()
  if (cat == EffectCategoryCondition or cat == EffectCategoryHex) then
    return true, math.floor(time * shorten)
  end

  return true, time
end

