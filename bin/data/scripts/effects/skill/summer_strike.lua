--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5039
name = "Summer Strike"
icon = ""
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

isPersistent = false
internal = true
local ticks = 0
local damage = 0

function getDuration(source, target)
  return 2000
end

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_FIRE)
  damage = math.floor((attrib * (100 - 18) / 12) + 18)
  return true
end

function onUpdate(source, target, time)
  ticks = ticks + time
  if (ticks >= 2000) then
    ticks = 0
    target:Damage(source, self.Index, DAMAGETYPE_TYPELESS, damage, 0.0)
    local actors = target:GetActorsInRange(RANGE_ADJECENT)
    for i, actor in ipairs(actors) do
      if (source:IsEnemy(actor)) then
        actor:Damage(source, self.Index, DAMAGETYPE_TYPELESS, damage, 0.0)
      end
    end

    target:RemoveEffect(self.Index)
  end
end

