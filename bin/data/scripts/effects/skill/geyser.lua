--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5054
name = "Geyser"
icon = ""
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

isPersistent = false
internal = true
local ticks = 0
local damage = 0
local snareTime = 0
local damaged = false
local crackedArmorTime = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  damage = math.floor((attrib * (100 - 10) / 12) + 10)
  snareTime = math.floor(attrib * 2 / 12)
  crackedArmorTime = math.floor((attrib * (15 - 3) / 12) + 3)
  return true
end

function onUpdate(source, target, time)
  ticks = ticks + time
  if (ticks >= 2000 and not damaged) then
    target:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
    -- Cracked armor
    if (crackedArmorTime > 0) then
      target:AddEffect(source, 10007)
    end
    damaged = true
  elseif (ticks >= 2000 + snareTime) then
    ticks = 0
    target:RemoveEffect(self.Index)
  end
end

function getSpeedFactor(speedFactor)
  if (not damaged) then
    return speedFactor, false
  end
  return speedFactor - 0.66, false
end

