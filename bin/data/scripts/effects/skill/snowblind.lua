--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5063
name = "Snowblind"
icon = ""
category = EffectCategortySkill
soundEffect = ""
particleEffect = ""

isPersistent = false
local damage = 0
local lastDamage = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_WATER)
  damage = math.floor(attribVal * ((60 - 10) / 12) + 10)
  return true
end

local function apply(source, actor)
  actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
end

function onUpdate(source, target, time)
  ticks = ticks + time
  if (ticks >= 1000) then
    ticks = 0
    target:Damage(source, self.Index, DAMAGETYPE_TYPELESS, damage, 0.0)
    local actors = target:GetActorsInRange(RANGE_ADJECENT)
    for i, actor in ipairs(actors) do
      if (not actor:IsDead() and source:IsEnemy(actor)) then
        apply(source, actor)
      end
    end
  end
end

