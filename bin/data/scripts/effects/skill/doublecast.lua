--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5020
name = "Doublecast"
icon = "Textures/Skills/Doublecast.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

function onStart(source, target)
  return true
end

function onEndUseSkill(source, target, skill)
  -- Execute skill again
  if (skill:GetLastError() == SkillErrorNone) then
    local attrib = source:GetAttributeRank(ATTRIB_ENERGY_STORAGE)
    local reduceFactor = math.floor(attrib * (90 - 25) / 12 + 25) / 100
    local skillAttrib = skill:GetAttribute()
    local sb = source:GetSkillBar()
    local oldRank = sb:GetAttributeRank(skillAttrib)
    if (skillAttrib ~= ATTRIB_NONE) then
      sb:SetAttributeRank(skillAttrib, math.floor(oldRank * reduceFactor))
    end

    skill:Execute()

    if (skillAttrib ~= ATTRIB_NONE) then
      sb:SetAttributeRank(skillAttrib, math.floor(oldRank))
    end
  end
end
