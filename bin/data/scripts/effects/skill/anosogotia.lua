--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")

index = 5140
name = "Anosogotia"
icon = "Textures/Skills/Anosogotia.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortySkill

isPersistent = false
internal = false

local hexes = {}

function onStart(source, target)
  local  effects = target:GetEffectsOf(EffectCategoryHex)
  for i, effect in ipairs(effects) do
    hexes[effect.Index] = effect:GetRemainingTime()
  end
  for index, ticks in pairs(hexes) do
    target:RemoveEffect(index)
  end

  return true
end

function onEnd(source, target)
  for index, ticks in pairs(hexes) do
    target:AddEffect(source, index, ticks)
  end
end
