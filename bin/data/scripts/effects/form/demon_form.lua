--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5331
name = "Demon Form"
icon = "Textures/Skills/Demon Form.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryForm

local addDamage = 0
local healMalus = 0

-- TODO: Actor should also get another appearance

function onStart(source, target)
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  addDamage = ((death * (25 - 50) / 12) + 50) / 100
  healMalus = ((death * (10 - 15) / 12) + 15) / 100
  return true
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor + 0.25, false
end

function getAttackSpeed(weapon, value)
  return math.floor(value * 0.5)
end

function getAttackDamageType(origType)
  return DAMAGETYPE_DARK
end

function onGettingDamage(source, target, skill, type, value)
  if (type == DAMAGETYPE_HOLY) then
    return value + math.floor(value * addDamage)
  end
  return value
end

function onHealing(source, target, value)
  return value - math.floor(value * healMalus)
end
