--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5335
name = "Lacrymactory Signet"
icon = "Textures/Skills/Lacrymactory Signet.png"
category = EffectCategorySignet
soundEffect = ""
particleEffect = ""

isPersistent = false

local counter = 0
local healing = 0

function onStart(source, target)
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  healing = math.floor(death * ((10 - 1) / 12) + 1)
  return true
end

function onActorDied(actor, killer)
  if (actor.Species ~= SPECIES_SPIRIT) then
    if (counter < 10) then
      counter = counter + 1
    end
  end
end

function onEnd(source, targt)
  target:Healing(source, self.Index, math.floor(counter * healing))
  local minions = source:GetMinions()
  for i, minion in ipairs(minions) do
    local npc = minions:AsNpc()
    if (npc ~= nil and not npc:IsDead()) then
      npc:Healing(source, self.Index, math.floor(counter * healing))
    end
  end
end

