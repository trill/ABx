--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5122
name = "Signet of Superiority"
icon = "Textures/Skills/Signet of Superiority.png"
category = EffectCategorySignet
soundEffect = ""
particleEffect = ""

isPersistent = false

local increase = 0

function onStart(source, target)
  local fastcast = source:GetAttributeRank(ATTRIB_FASTCAST)
  increase = math.floor(fastcast * (2 / 12))
  return true
end

function getAttributeRank(index, value)
  return value + increase
end

function getSkillActivation(skill, activation)
  return math.floor(activation + (activation * 0.5))
end
