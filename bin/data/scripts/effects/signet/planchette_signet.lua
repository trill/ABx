--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5602
name = "Planchette Signet"
icon = "Textures/Skills/Planchette Signet.png"
category = EffectCategorySignet
soundEffect = ""
particleEffect = ""

isPersistent = false

local healing = 0
local energy = 0

function onStart(source, target)
  local soulreaping = source:GetAttributeRank(ATTRIB_SOUL_REAPING)
  healing = math.floor(soulreaping * ((50 - 20) / 12) + 20)
  energy = math.floor(soulreaping * ((5 - 1) / 12) + 1)
  return true
end

function onDied(source, killer)
  local src = self.Source
  if (src ~= nil and not src:IsDead()) then
    src:Healing(src, self.Index, healing)
    src:AddEnergy(energy)
  end
end
