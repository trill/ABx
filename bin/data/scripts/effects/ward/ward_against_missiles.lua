--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5018
name = "Ward Against Missiles"
icon = "Textures/Skills/Ward Against Missiles.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryWard

function onStart(source, target)
  return true
end

function onTargetAttackStart(source, target)
  if (target.Species ~= SPECIES_SPIRIT) then
    local r = Random()
    return r >= 0.5
  end
  return true
end
