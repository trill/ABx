--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5017
name = "Ward Against Magic"
icon = "Textures/Skills/Ward Against Magic.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryWard

local reduce = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  local factor = (10 - 5) / 12
  reduce = attribVal * factor + 5
  return true
end

function getDamage(type, value, critical)
  return math.floor(value * reduce), critical
end
