--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 1000
name = "PvP"
description = "This is a PvP area."
icon = "Textures/Effects/pvp.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnvironment
soundEffect = ""
particleEffect = ""

-- PvP is forever on this map
isPersistent = false

function getDuration(source, target)
  return TIME_FOREVER
end

function onStart(source, target)
  return true
end

function onEnd(source, target)
end

function onRemove(source, target)
end
