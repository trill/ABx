--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10008
name = "Deep Wound"
description = "Your maximum health is reduced by 20% and you receive less benefit from healing."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function onStart(source, target)
  return true
end

function getResources(maxHealth, maxEnergy)
  local reduce = maxHealth * 0.2
  if (reduce > 100) then
    reduce = 100
  end
  return maxHealth - reduce, maxEnergy
end
