--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")

index = 10010
name = "Weakness"
description = "You deal 66% less damage and yooour attributes are reduced by 1."
icon = "Textures/Icons/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function onStart(source, target)
  return true
end

function getAttributeRank(index, value)
  return value - 1
end

function getAttackDamage(value)
  return math.floor(value * 0.66)
end
