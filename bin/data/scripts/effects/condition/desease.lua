--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")

index = 10009
name = "Disease"
description = "You lose health over time. You spread disease to other creatures of the same kind."
icon = "Textures/Effects/disease.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

local ticks = 0

function onStart(source, target)
  ticks = self:GetTicks()
  return true
end

function getRegeneration(health, energy)
  return health - 4, energy
end

function onUpdate(source, target, time)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:HasEffect(self.Index) and actor.Species == target.Species) then
      actor:AddEffect(source, self.Index, ticks)
    end
  end
end
