--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")

index = 10003
name = "Dazed"
description = "Your casts take twice as long and your spells are easily interrupted."
icon = "Textures/Effects/dazed.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation * 2), energy, adrenaline, overcast, hp
end

function onTargetAttackEnd(source, target, type, damage)
  target:InterruptSkill(source, SkillTypeSkill)
  return ATTACK_ERROR_NONE
end
