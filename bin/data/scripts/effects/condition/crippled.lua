--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10001
name = "Crippled"
description = "You move 50% slower."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

-- 2 Sec
local factor = -0.5

function getSpeedFactor(speedFactor)
  -- Increase speed by factor and continue iterating other effects
  return speedFactor + factor, false
end
