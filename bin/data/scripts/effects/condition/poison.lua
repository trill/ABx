--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")

index = 10002
name = "Poison"
description = "You lose health over time."
icon = "Textures/Effects/poison.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function getDuration(source, target)
  return 0
end

function onStart(source, target)
  return true
end

function getRegeneration(health, energy)
  return health - 4, energy
end
