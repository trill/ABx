--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10005
name = "Blind"
description = "Your melee and missile attacks miss with 90% chance."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

function onAttacked(source, target, damageType, damage)
  return ATTACK_ERROR_TARGET_MISSED
end
