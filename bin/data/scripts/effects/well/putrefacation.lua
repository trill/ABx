--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5339
name = "Putrefaction"
icon = "Textures/Skills/Putrefacation.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortyWell

local degen = 0

function onStart(source, target)
  local death = source:GetAttributeRank(ATTRIB_DEATH)
  degen = math.floor((death * (5 - 1) / 12) + 1)
  return true
end

function getRegeneration(healthRegen, energyRegen)
  return healthRegen - degen, energyRegen
end
