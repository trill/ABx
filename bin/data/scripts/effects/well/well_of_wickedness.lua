--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5513
name = "Well of Wickedness"
icon = "Textures/Skills/Well of Wickedness.png"
soundEffect = ""
particleEffect = ""
category = EffectCategortyWell

function onStart(source, target)
  return true
end

function onCanUseSkill(source, target, skill)
  if (not skill:IsType(SkillTypeSpell)) then
    return true
  end
  if (source:IsAlly(target)) then
    return false
  end
  return true
end
