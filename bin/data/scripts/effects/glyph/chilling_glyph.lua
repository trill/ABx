--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5051
name = "Chilling Glyph"
icon = "Textures/Skills/Chilling Glyph.png"
category = EffectCategoryGlyphe
soundEffect = ""
particleEffect = ""

isPersistent = false

local count = 0
local usedSkills = 0
local snareTime = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  count = math.floor(attrib * (2 - 1) / 12 + 1)
  snareTime = math.floor(attrib * (4 - 1) / 12 + 1)
  return true
end

function onStartUseSkill(source, target, skill)
  if (skill:IsType(SkillTypeSpell)) then
    target:AddEffect(source, 50511, snareTime)
    return
  end
  usedSkills = usedSkills + 1
end

function onEndUseSkill(source, target, skill)
  if (usedSkills >= count) then
    source:RemoveEffect(self.Index)
  end
end

