--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5021
name = "Glyph of Greater Concentration"
icon = "Textures/Skills/Glyph of Greater Concentration.png"
category = EffectCategoryGlyphe
soundEffect = ""
particleEffect = ""

isPersistent = false

local count = 0
local usedSkills = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_ENERGY_STORAGE)
  count = math.ceil(attrib * (3 - 1) / 12 + 1)
  return true
end

function onStartUseSkill(source, target, skill)
  if (not source:HasEffectOf(EffectCategoryEnchantment)) then
    source:RemoveEffect(self.Index)
    return
  end
  usedSkills = usedSkills + 1
end

function onEndUseSkill(source, target, skill)
  if (usedSkills >= count) then
    source:RemoveEffect(self.Index)
  end
end

function onInterruptingSkill(source, target, type, skill)
  if (count > usedSkills) then
    return false
  end
  return true
end

function onKnockingDown(source, target, time)
  if (not source:IsUsingSkillOfType(SkillTypeSkill)) then
    -- Must be using a skill to prevent KD
    return true
  end
  if (count > usedSkills) then
    return false
  end
  return true
end
