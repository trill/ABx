--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 5125
name = "Time Walk"
icon = "Textures/Skills/Time Walk.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

local position

-- TODO: ... and all of the spells cast during Time Walk begin to recharge 33% faster than usual.
--       Recharge faster for how long?
function onStart(source, target)
  position = source.Position
  return true
end

function onEnd(source, target)
  source:ShadowStep(position)
end

function getSkillCost(source, skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation + (activation * 0.33)), energy, adrenaline, overcast, hp
end

function getSkillRecharge(skill, recharge)
  return TIME_FOREVER
end
