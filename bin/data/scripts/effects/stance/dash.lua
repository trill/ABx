--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 1043
name = "Dash"
icon = "Textures/Skills/Dash.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

local boost = 0.5

function getDuration(source, target)
  return 3000
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor + boost, false
end
