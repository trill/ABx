--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")

index = 5253
name = "Ecclesiastical Revelation"
icon = "Textures/Skills/Ecclesiastical Revelation.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

local faster = 0

function onStart(source, target)
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  faster = (smiting * ((29 - 5) / 12) + 5) / 100
  return true
end

function getSkillRecharge(skill, recharge)
  return math.floor(recharge * faster)
end

function getSkillActivation(skill, activation)
  return math.floor(activation * faster)
end
