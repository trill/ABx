--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 5121
name = "Mantra of Indignation"
icon = "Textures/Skills/Mantra of Indignation.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

function onWasInterrupted(source, target, skill)
  local sb = source:GetSkillBar()
  local skills = sb:GetSkills()
  for i, _skill in ipairs(skills) do
    _skill:AddRecharge(2000)
  end
end
