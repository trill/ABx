--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 5124
name = "Time Dilation"
icon = "Textures/Skills/Time Dilation.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

function getTargetSkillCost(source, skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation + (activation * 0.5)), energy, adrenaline, overcast, hp
end
