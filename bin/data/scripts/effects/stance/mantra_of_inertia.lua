--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 5145
name = "Mantra of Inertia"
icon = "Textures/Skills/Mantra of Inertia.png"
category = EffectCategoryStance
soundEffect = ""
particleEffect = ""

isPersistent = false

local count = 0
local times = 0
local energyLoss = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  times = math.floor(inspiration * (2 / 12))
  energyLoss = math.floor(inspiration * ((5 - 10) / 12) + 10)
  return true
end

function onKnockingDown(source, target, time)
  count = count + 1
  if (count > times) then
    self:Remove()
    return true
  end
  if (target:GetResource(RESOURCE_TYPE_ENERGY) < energyLoss) then
    target:RemoveEffect(self.Index)
    return true
  end
  target:DrainEnergy(energyLoss)
  return false
end

