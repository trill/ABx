--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5150
name = "Wastrel's Woe"
icon = "Textures/Skills/Wastrel's Woe.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local energyLoss = 0
local last = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  energyLoss = math.floor(inspiration / 12)
  return true
end

function onUpdate(source, target, time)
  last = last + time
  if (lastDamage >= 1000) then
    target:DrainEnergy(energyLoss)
    last = 0
  end
end

function onStartUseSkill(source, target, skill)
  source:RemoveEffect(self.Index)
end
