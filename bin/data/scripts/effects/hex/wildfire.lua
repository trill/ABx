--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5040
name = "Wildfire"
icon = "Textures/Skills/Wildfire.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function getRegeneration(health, energy)
  return health - 7, energy
end

function onUpdate(source, target, time)
  local remaining = self:GetRemainingTime()
  if (remaining == 0) then
    return
  end
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (source:IsEnemy(actor) and not actor:HasEffect(self.Index)) then
      actor:AddEffect(source, self.Index, self:GetRemainingTime())
    end
  end
end

