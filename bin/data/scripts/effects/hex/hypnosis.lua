--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5132
name = "Hypnosis"
icon = "Textures/Skills/Hypnosis.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0


function onWasInterrupted(source, target, skill)
  target:RemoveEffect(self.Index)
end

function onKnockedDown(target, time)
  target:RemoveEffect(self.Index)
end

function getSkillActivation(skill, activation)
  return math.floor(activation * 2)
end

function getSpeedFactor(speedFactor)
  return speedFactor + (speedFactor * 0.5)
end
