--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5116
name = "Intimidate"
icon = "Textures/Skills/Intimidate.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local chance = 0

function onStart(source, target)
  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  chance = math.floor(math.max(domination / 12, 100))  
  return true
end

function onCanUseSkill(source, target, skill)
  if (not skill:HasEffect(SkillEffectInterrupt)) then
    return true;
  end
  if (Random() >= chance) then
    return false
  end
  return true
end
