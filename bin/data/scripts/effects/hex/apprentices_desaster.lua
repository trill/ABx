--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5100
name = "Apprentice's Disaster"
icon = "Textures/Skills/Apprentice's Disaster.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DOMINATION)
  local dmgfactor = (130 - 45) / 12
  damage = math.floor(dmgfactor * attrib + 45)
  return true
end

function getSkillActivation(skill, activation)
  return math.floor(activation * 0.33)
end

function onEndUseSkill(source, target, skill)
  if (skill:IsType(SkillTypeSpell)) then
    source:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
  end
end
