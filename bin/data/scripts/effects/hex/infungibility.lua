--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5143
name = "Infungibility"
icon = "Textures/Skills/Infungibility.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local reduction = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  reduction = math.floor((illusion * (3 - 1)) / 12 + 1)
  return true
end

function getAttributeRank(index, value)
  return math.max(value - reduction, 0)
end

