--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5400
name = "Implant Parasite"
icon = "Textures/Skills/Implant Parasite.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local degen = 0

function onStart(source, target)
  local blood = source:GetAttributeRank(ATTRIB_BLOOD)
  degen = math.floor(blood * ((4 - 1) / 12) + 1)
  source:SetHealthRegen(degen)
  return true
end

function getRegeneration(health, energy)
  return health - degen, energy
end

function onRemove(source, target)
  source:SetHealthRegen(-degen)
end

function onEnd(source, target)
  source:SetHealthRegen(-degen)
end
