--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5104
name = "Indiscrimination"
icon = "Textures/Skills/Indiscrimination.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  damage = math.floor(domination * ((70 - 5) / 12) + 5)
  return true
end

-- Here source is the actor that used the skill and target is the target of the skill.
-- self.Source returns the actor that caused this effect
function onEndUseSkill(source, target, skill)
  if (skill:GetLastError() ~= SkillErrorNone) then
    return
  end
  if (not skill:IsType(SkillTypeSpell)) then
    return
  end

  source:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
  local actors = source:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
    end
  end
end
