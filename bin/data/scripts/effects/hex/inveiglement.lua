--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5144
name = "Inveiglement"
icon = "Textures/Skills/Inveiglement.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local gain = 0

function onStart(source, target)
  local inspiration = source:GetAttributeRank(ATTRIB_INSPIRATION)
  gain = math.floor((illusion * (15 - 6)) / 12 + 6)
  return true
end

function onEndUseSkill(source, target, skill)
  if (isSkillError(skill:GetLastError())) then
    return
  end
  local src = self.Source
  if (src ~= nil and not src:IsDead()) then
    src:AddEnergy(gain)
  end
  source:RemoveEffect(self.Index)
end
