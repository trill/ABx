--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5136
name = "Minor Mind Maze"
icon = "Textures/Skills/Minor Mind Maze.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local rechargeAdd = 0

function onStart(source, target)
  local illusion = source:GetAttributeRank(ATTRIB_ILLUSION)
  rechargeAdd = (illusion * ((40 - 20) / 12) + 20) / 100
  return true
end

function getSkillRecharge(skill, recharge)
  return math.floor(recharge + (recharge * rechargeAdd))
end
