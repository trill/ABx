--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5066
name = "Winter Rose"
icon = "Textures/Skills/Winter Rose.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  local damage = math.floor(attrib * ((60 - 10) / 12) + 10)
  target:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
  return true
end

function getSpeedFactor(speedFactor)
  return speedFactor * 0.66, false
end

