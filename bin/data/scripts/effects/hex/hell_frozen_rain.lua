--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5505
name = "Hell Frozen Rain"
icon = "Textures/Skills/Hell Frozen Rain.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local degen = 0
local armorReduction = 0
local totalArmorReduction = 0
local lastCheck = 0

function onStart(source, target)
  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  degen = math.floor(curses / 12)
  armorReduction = math.floor((curses * 2) / 12)
  totalArmorReduction = armorReduction
  return true
end

function onUpdate(time)
  lastCheck = lastCheck + time
  if (lastCheck >= 1000) then
    totalArmorReduction = totalArmorReduction + armorReduction
    lastCheck = 0
  end
end

function getRegeneration(healthRegen, energyRegen)
  return healthRegen - degen, energyRegen
end

function getArmor(type, value)
  return value - armorReduction
end
