--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 15032
name = "Eclipsina's Wish"
icon = "Textures/Skills/Eclipsina's Wish.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damage = 0
local lastDamage = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_FIRE)
  damage = math.floor(attribVal * ((20 - 1) / 12) + 1)
  return true
end

function onUpdate(source, target, time)
  lastDamage = lastDamage + time
  if (lastDamage >= 1000) then
    if (target:HasEffect(10004)) then
      target:Damage(self.Source, self.Index, DAMAGETYPE_FIRE, damage, 0.0)
    end
    lastDamage = 0
  end
end
