--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5139
name = "Haphephobia"
icon = "Textures/Skills/Haphephobia.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onTargetAttackEnd(source, target, type, damage)
  if (not target:IsDead()) then
    target:Interrupt(self.Source)
  end
end

function onTargetSkillSuccess(source, target, skill)
  if (skill == nil) then
    return    
  end
  if (skill:GetRange() ~= RANGE_TOUCH) then
    return
  end
  -- Touch skill
  if (not target:IsDead()) then
    target:Interrupt(self.Source)
  end
end
