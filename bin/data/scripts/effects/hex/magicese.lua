--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5105
name = "Magicese"
icon = "Textures/Skills/Magicese.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  if (skill:IsType(SkillTypeSpell)) then
    return skill, activation, energy + 5, adrenaline, overcast, hp
  end
  return skill, activation, energy, adrenaline, overcast, hp
end

function onEndUseSkill(source, target, skill)
  if (skill:GetLastError() == SkillErrorNone) then
    source:RemoveEffect(self.Index)
  end
end
