--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5101
name = "Betrayal"
icon = "Textures/Skills/Betrayal.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DOMINATION)
  local dmgfactor = (90 - 30) / 12
  damage = math.floor(dmgfactor * attrib + 30)
  return true
end

function onEndUseSkill(source, target, skill)
  if (target == nil or source == nil) then
    return
  end
  if (source:IsAlly(target) and (source.Id ~= target.Id)) then
    source:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
  end
end
