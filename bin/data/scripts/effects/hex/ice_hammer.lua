--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5058
name = "Ice Hammer"
icon = "Textures/Skills/Ice Hammer.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor * 0.66, false
end

