--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5500
name = "Beggar's Blessing"
icon = "Textures/Skills/Beggar's Blessing.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local maxHealing = 0
local healed = 0

function onStart(source, target)
  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  maxHealing = math.floor(curses * ((300 - 100) / 12) + 100)
  return true
end

local function apply(value)
  healed = healed + value
  if (healed > maxHealing) then
    self:Remove()
    return value
  end
  self.Source.Healing(source, self.Index, value)
  return 0
end

function onHealing(source, target, value)
  return apply(value)
end

function onHealingTarget(source, target, value)
  return apply(value)
end
