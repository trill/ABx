--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5137
name = "Phantom Fumble"
icon = "Textures/Skills/Phantom Fumble.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_ILLUSION)
  damage = math.floor(attrib * ((80 - 20) / 12) + 20)
  return true
end

function onSourceAttackSuccess(source, target, type, damage)
  local actors = target:GetAlliesInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Interrupt(self.Source)
      actor:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
    end
  end
end
