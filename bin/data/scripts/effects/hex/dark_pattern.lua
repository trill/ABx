--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5114
name = "Dark Pattern"
icon = "Textures/Skills/Dark Pattern.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onDealingDamage(source, target, damageType, value)
  local r = Random();
  if (r > 0.3) then
    return true, value
  end
  source:Damage(nil, 0, damageType, value, 0.0)
  target:Healing(nil, 0, value)
  return false, value
end
