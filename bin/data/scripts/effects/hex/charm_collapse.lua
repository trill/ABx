--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5160
name = "Charm Collapse"
icon = "Textures/Skills/Charm Collapse.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local energyLoss = 0
local last = 0

function onStart(source, target)
  return true
end

function onUpdate(source, target, time)
  local energy = target:GetResource(RESOURCE_TYPE_ENERGY)
  if (energy <= 0) then
    target:RemoveAllEffectsOf(EffectCategoryEnchantment)
    target:RemoveEffect(self.Index)
  end
end

