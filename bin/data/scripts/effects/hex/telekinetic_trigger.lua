--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5113
name = "Telekinetic Trigger"
icon = "Textures/Skills/Telekinetic Trigger.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  damage = math.floor(domination * ((79 - 35) / 12) + 35)
  return true
end

function onRemove()
  self.Target:Damage(self.Source, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
end
