--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5257
name = "Seal Evil"
icon = "Textures/Skills/Seal Evil.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local reduction = 0

function onStart(source, target)
  local smiting = source:GetAttributeRank(ATTRIB_SMITING)
  reduction = (smiting * ((70 - 15) / 12) + 15) / 100
  return true
end

function onDealingDamage(source, target, damageType, value)
  if (damageType == DAMAGETYPE_CHAOS or damageType == DAMAGETYPE_DARK or damageType == DAMAGETYPE_SHADOW or damageType == DAMAGETYPE_TYPELESS or damageType == DAMAGETYPE_LIFEDRAIN) then
    return true, value - math.floor(value * reduction)
  end
  return true, value
end

