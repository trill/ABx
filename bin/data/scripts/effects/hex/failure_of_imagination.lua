--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5130
name = "Failure of Imagination"
icon = "Textures/Skills/Failure of Imagination.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damageFactor = 0

function onStart(source, target)
  local illusion = source:GetAttributeRank(ATTRIB_ILLUSION)
  damageFactor = math.floor(illusion * ((24 - 8) / 12) + 8)

  return true
end

function onEnd(source, target)
  local count = 0
  local sb = source:GetSkillBar()
  local skills = sb:GetSkills()
  for i, _skill in ipairs(skills) do
    if (not _skill:IsRecharged()) then
      count = count + 1
    end
  end

  target:Damage(source, self.Index, DAMAGETYPE_CHAOS, damageFactor * count, 0.0)
end

