--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5503
name = "Chilled Barbs"
icon = "Textures/Skills/Chilled Barbs.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local curses = source:GetAttributeRank(ATTRIB_CURSES)
  damage = math.floor(curses * ((10 - 1) / 12) + 1)
  return true
end

function onGotDamage(source, target, skill, type, value)
  if (skill == nil) then
    return
  end
  if (skill.Index == self.Index) then
    return
  end
  target:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
  local actors = target:GetActorsInRange(RANGE_ADJECENT)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead() and actor:IsEnemy(source)) then
      actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
    end
  end
end
