--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5107
name = "Paranoia"
icon = "Textures/Skills/Paranoia.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local energyLoss = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DOMINATION)
  energyLoss = math.floor(attrib * ((4 - 1) / 12) + 1)
  return true
end

function onGettingSkillTarget(source, target, skill)
  if (target:IsAlly(source)) then
    target:DrainEnergy(energyLoss)
  end
end
