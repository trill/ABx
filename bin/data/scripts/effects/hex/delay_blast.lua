--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5031
name = "Delay Blast"
icon = "Textures/Skills/Delay Blast.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local fire = source:GetAttributeRank(ATTRIB_FIRE)
  damage = math.floor(fire * ((90 - 20) / 12) + 20)
  return true
end

function onEnd(source, target)
  target:Damage(self.Source, self.Index, DAMAGETYPE_FIRE, damage, 0.0)
  target:AddEffect(source, 10004, 3000)
  local actors = target:GetAlliesInRange(RANGE_NEARBY)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Damage(self.Source, self.Index, DAMAGETYPE_FIRE, damage, 0.0)
      actor:AddEffect(source, 10004, 3000)
    end
  end
end
