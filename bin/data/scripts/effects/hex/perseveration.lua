--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5108
name = "Perseveration"
icon = "Textures/Skills/Perseveration.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local disableTime = 0

function onStart(source, target)
  local domination = source:GetAttributeRank(ATTRIB_DOMINATION)
  disableTime = math.floor(domination * ((7 - 1) / 12) + 1)
  return true
end

function onEndUseSkill(source, target, skill)
  local sb = source:GetSkillBar()
  local skills = sb:GetSkills()
  for i, _skill in ipairs(skills) do
    _skill:AddRecharge(disableTime * 1000)
  end
  skill:SetRecharged(0)
end
