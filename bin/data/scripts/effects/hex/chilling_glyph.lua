--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 50511
name = "Chilling Glyph"
icon = "Textures/Skills/Chilling Glyph.png"
category = EffectCategoryHex
soundEffect = ""
particleEffect = ""

isPersistent = false

local snareFactor = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  snareFactor = math.floor(attrib * (40 - 10) / 12 + 10) / 100
  return true
end

function getSpeedFactor(speedFactor)
  -- Increase speed by boost and continue iterating other effects
  return speedFactor - snareFactor, false
end

