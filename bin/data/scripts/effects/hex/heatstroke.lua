--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5036
name = "Heatstroke"
icon = "Textures/Skills/Heatstroke.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local fireTime = 0

function onStart(source, target)
  return true
end

function onUpdate(source, target, time)
  if (target:HasEffect(10004)) then
    fireTime = fireTime + time
    if (fireTime >= 5000) then
      target:KnockDown(source, 0)
      fireTime = 0
    end
  else
    fireTime = 0
  end
end

