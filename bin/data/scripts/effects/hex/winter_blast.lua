--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5065
name = "Winter Blast"
icon = "Textures/Skills/Winter Blast.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  damage = math.floor(attrib * ((70 - 15) / 12) + 15)
  return true
end

function getSpeedFactor(speedFactor)
  return speedFactor * 0.66, false
end

function onEnd(source, target)
  target:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
end

function onRemove(source, target)
  target:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
end

