--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5131
name = "Haze of Mirrors"
icon = "Textures/Skills/Haze of Mirrors.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0

function onStart(source, target)
  local illusion = source:GetAttributeRank(ATTRIB_ILLUSION)
  damage = math.floor(illusion * ((45 - 3) / 12) + 3)

  return true
end

function onWasInterrupted(source, target, skill)
  source:Damage(target, self.Index, DAMAGETYPE_CHAOS, damage, 0.0)
end

