--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5062
name = "Shatter Flesh"
icon = "Textures/Skills/Shatter Flesh.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

local damage = 0
local slowdownTime = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_WATER)
  damage = math.floor((attrib * (75 - 10) / 12) + 10)
  slowdownTime = math.floor((attrib * 3) / 12)
  return true
end

function onDied(source, killer)
  source.Recycleable = false
  local actors = source:GetAlliesInRange(RANGE_NEARBY)
  for i, actor in ipairs(actors) do
    if (not actor:IsDead()) then
      actor:Damage(source, self.Index, DAMAGETYPE_COLD, damage, 0.0)
      actor:SetVarNumber("SlowdownFactor", 0.66)
      actor:AddEffect(self.Source, 1003, slowdownTime)
    end
  end
end
