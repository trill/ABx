--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 1003
name = "Slowdown"
description = "You move slower."
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryGeneral
soundEffect = ""
particleEffect = ""

isPersistent = false
internal = false

local factor = 0.0

function onStart(source, target)
  factor = source:GetVarNumber("SlowdownFactor")
  return true
end

function getSpeedFactor(speedFactor)
  -- Set speed to zero and stop iterating other effects
  return speedFactor * factor, true
end
