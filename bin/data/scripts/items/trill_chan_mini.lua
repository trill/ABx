--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/consts.lua")
include("/scripts/includes/functions.lua")
include("/scripts/includes/item_functions.lua")

materialStats = {}
materialStats[1] = { 9999999, 1000 }
materialStats[2] = { 0, 0 }
materialStats[3] = { 0, 0 }
materialStats[4] = { 0, 0 }

function onDoubleClick(source)
  local game =  source.Game
  if (game == nil) then
    return;
  end
  if (isPvPBattle(game)) then
    return
  end

  local SCRIPT = "/scripts/actors/npcs/mini_pets/trill_chan.lua"
  local currentMiniId = source:GetVarNumber("current_mini")
  local currentMiniScript = ""
  if (currentMiniId ~= 0) then
    local currentMini = source.Game:GetObject(currentMiniId)
    if (currentMini ~= nil) then
      local npc = currentMini:AsNpc()
      currentMiniScript = npc:GetScriptFile()
      if (npc ~= nil) then
        npc:Remove()
        source:SetVarNumber("current_mini", 0)
      end
    end
  end
  if (currentMiniScript ~= SCRIPT) then
    local mini = source:SpawnSlave(SCRIPT, source, SLAVE_KIND_MINI_PET, 1, source.Position)
    source:SetVarNumber("current_mini", mini.Id)
  end
end
