--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- Included by weapons to get damage value
function getDamage(baseMin, baseMax, critical)
  if (critical == true) then
    return math.floor(baseMax)
  end
  return math.floor(((baseMax - baseMin) * Random()) + baseMin)
end
