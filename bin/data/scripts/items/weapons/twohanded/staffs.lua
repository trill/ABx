--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/damage.lua")
-- Include drop stats etc.
include("/scripts/includes/staff_defaults.lua")
include("/scripts/includes/item_functions.lua")

-- getDamage()
include("/scripts/items/weapons/damage.lua")
