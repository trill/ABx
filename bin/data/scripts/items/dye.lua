--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/item_functions.lua")

dropStats = {}
dropStats["Usages"] = 1

function onConsume()
  -- TODO: Implement this. self:Index can be used to get the color
  return false
end
