--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

PLAYER_MAX_ATTRIBUTES = 10

-- Attribute indices

-- Mesmer
ATTRIB_FASTCAST = 0
ATTRIB_ILLUSION = 1
ATTRIB_DOMINATION = 2
ATTRIB_INSPIRATION = 3
-- Necromancer
ATTRIB_BLOOD = 4
ATTRIB_DEATH = 5
ATTRIB_SOUL_REAPING = 6
ATTRIB_CURSES = 7
-- Elementarist
ATTRIB_AIR = 8
ATTRIB_EARTH = 9
ATTRIB_FIRE = 10
ATTRIB_WATER = 11
ATTRIB_ENERGY_STORAGE = 12
-- Monk
ATTRIB_HEALING = 13
ATTRIB_SMITING = 14
ATTRIB_PROTECTION = 15
ATTRIB_DEVINE_FAVOUR = 16
-- Warrior
ATTRIB_STRENGTH = 17
ATTRIB_AXE_MASTERY = 18
ATTRIB_HAMMER_MASTERY = 19
ATTRIB_SWORDS_MANSHIP = 20
ATTRIB_TACTICS = 21
-- Ranger
ATTRIB_BEAST_MASTERY = 22
ATTRIB_EXTERTISE = 23
ATTRIB_WILDERNESS_SURVIVAL = 24
ATTRIB_MARK_MANSSHIP = 25

-- None
ATTRIB_NONE = 99

function getMaxAttributeRank(points)
  if (points < 3) then
    return 1
  elseif (points < 6) then
    return 2
  else if (points < 10) then
    return 3
  elseif (points < 15) then
    return 4
  elseif (points < 21) then
    return 5
  elseif (points < 28) then
    return 6
  elseif (points < 37) then
    return 7
  elseif (points < 48) then
    return 8
  elseif (points < 61) then
    return 9
  elseif (points < 77) then
    return 10
  elseif (points < 97) then
    return 11
  else
    return 12
  end
end
end
