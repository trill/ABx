--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- Some shortucts to create NPCs

-- Create and account chest
function createChest(game, x, z)
  local chest = game:AddNpc("/scripts/actors/logic/account_chest.lua")
  if (chest ~= nil) then
    local y = game:GetTerrainHeight(x, z)
    chest.Position = {x, y, z}
    return chest
  end
  return nil
end

-- Create a portal to a map
function createPortal(game, x, y, z, name, destination)
  local portal = game:AddNpc("/scripts/actors/logic/portal.lua")
  if (portal ~= nil) then
    portal.Name = name
    -- Map ID where this portal leads to
    portal:SetVarString("destination", destination)
    portal.Position = {x, y, z}
    return portal
  end
  return nil
end

function createResShrine(game, x, z)
  local npc = game:AddNpc("/scripts/actors/logic/resurrection_shrine.lua")
  if (npc ~= nil) then
    local y = game:GetTerrainHeight(x, z)
    npc.Position = {x, y, z}
    return npc
  end
  return nil
end
