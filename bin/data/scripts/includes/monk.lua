--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- Monk specific functions

function getDevineFavorHealBonus(source)
  local attr = source:GetAttributeRank(ATTRIB_DEVINE_FAVOUR)
  return math.floor(attr * 3.2)
end
