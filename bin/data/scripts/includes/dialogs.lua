--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

-- Dialog IDs. Keep in synch with Include/AB/Dialogs.h
DIALOG_UNKNOWN = 0
DIALOG_ACCOUNTCHEST = 1
DIALOG_MERCHANT_ITEMS = 2
DIALOG_CRAFTSMAN_ITEMS = 3
