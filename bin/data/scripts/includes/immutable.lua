--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

function readonlytable(table)
  return setmetatable({}, {
    __index = table,
    __newindex = function(table, key, value)
      error("Attempt to modify read-only table")
    end,
    __metatable = false
  });
end
