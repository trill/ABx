--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

CHAT_CHANNEL_GENERAL  = 1
CHAT_CHANNEL_GUILD    = 2
CHAT_CHANNEL_PARTY    = 3
CHAT_CHANNEL_TRADE    = 4
CHAT_CHANNEL_WHISPER  = 5
