--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

--------------------------------------------------------------------------------
-- Helper functions ------------------------------------------------------------
--------------------------------------------------------------------------------

-- Get absolute value from percent
function getAbsolute(max, percent)
  return (max / 100) * percent
end

function isBattle(game)
  if (game == nil) then
    return false
  end
  return game.Type >= GAMETYPE_PVPCOMBAT
end

function isOutpost(game)
  if (game == nil) then
    return false
  end
  local gt = game.Type
  return (gt >= GAMETYPE_OUTPOST) and (gt <= GAMETYPE_GUILDHALL)
end

function isPvPBattle(game)
  if (game == nil) then
    return false
  end
  return game.Type == GAMETYPE_PVPCOMBAT
end
