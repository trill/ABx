--
-- Copyright 2017-2021, Stefan Ascher
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
--

include("/scripts/includes/enum.lua")

-- Keep in synch with AB::GameProtocol::CommandType
Command = enum {
  "Unknown",
  "ChatGeneral",
  "ChatGuild",
  "ChatParty",
  "ChatTrade",
  "ChatWhisper",
  "Resign",
  "Stuck",
  "Age",
  "Deaths",
  "Health",
  "Xp",
  "Pos",
  "Roll",
  "Sit",
  "Stand",
  "Cry",
  "Taunt",
  "Ponder",
  "Wave",
  "Laugh",
  "Shrug",
  "Phew",
  "Sigh",
  "PetName",
}
