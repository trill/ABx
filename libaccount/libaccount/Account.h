/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Account.h>
#include <AB/Entities/Character.h>
#include <AB/Entities/GuildMembers.h>
#include <stdint.h>
#include <string>

namespace Account {

enum class PasswordAuthResult
{
    OK = 0,
    InvalidAccount,
    AlreadyLoggedIn,
    PasswordMismatch,
    InternalError,
    AccountBanned
};

enum AccountKeyStatus : uint8_t
{
    NotActivated = 0,
    ReadyForUse = 1,
    Banned = 2
};

enum AccountKeyType : uint8_t
{
    KeyTypeAccount = 0,
    KeyTypeCharSlot = 1,
};

enum class CreateAccountResult
{
    OK = 0,
    NameExists,
    InvalidAccountKey,
    InvalidAccount,
    InternalError,
    EmailError,
    PasswordError,
    AlreadyAdded,
};

CreateAccountResult CreateAccount(const std::string& name,
    const std::string& pass,
    const std::string& email,
    const std::string& accKey);
CreateAccountResult AddAccountKey(AB::Entities::Account& account, const std::string& accKey);
PasswordAuthResult PasswordAuth(const std::string& pass, AB::Entities::Account& account);
bool TokenAuth(const std::string& token, AB::Entities::Account& account);
bool IsNameAvailable(const std::string& name, const std::string& forAccountUuid);

bool GameWorldAuth(const std::string& accountUuid, const std::string& authToken, const std::string& charUuid);
bool AccountLogout(const std::string& uuid);
bool GetAccountInfo(AB::Entities::Account& account, AB::Entities::Character& character);
bool GetGuildMemberInfo(const AB::Entities::Account& account, AB::Entities::GuildMember& g);

}
