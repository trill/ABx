/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Account.h"

#include <AB/CommonConfig.h>
#include <AB/Entities/AccountKey.h>
#include <AB/Entities/AccountKeyAccounts.h>
#include <AB/Entities/AccountList.h>
#include <AB/Entities/ReservedName.h>
#include <libcommon/BanManager.h>
#include <libcommon/DataClient.h>
#include <libcommon/Logger.h>
#include <libcommon/Subsystems.h>
#include <libcommon/UuidUtils.h>
#include <libshared/Mechanic.h>
#include <sa/Assert.h>
#include <sa/time.h>
#include <abcrypto.hpp>

namespace Account {

CreateAccountResult CreateAccount(const std::string& name,
    const std::string& pass,
    const std::string& email,
    const std::string& accKey)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    if (name.empty())
        return CreateAccountResult::NameExists;
    if (pass.empty())
        return CreateAccountResult::PasswordError;
#if defined(EMAIL_MANDATORY)
    if (email.empty())
        return CreateAccountResult::EmailError;
#endif

    uuids::uuid accUuid(accKey);
    if (accUuid.nil())
        return CreateAccountResult::InvalidAccountKey;

    AB::Entities::AccountKey akey;
    akey.uuid = accUuid.to_string();
    akey.status = AB::Entities::AccountKeyStatus::KeryStatusReadyForUse;
    akey.type = AB::Entities::AccountKeyType::KeyTypeAccount;
    if (!client->Read(akey))
        return CreateAccountResult::InvalidAccountKey;
    if (akey.used + 1 > akey.total)
        return CreateAccountResult::InvalidAccountKey;

    // Create the account
    char pwhash[61];
    if (bcrypt_newhash(pass.c_str(), 10, pwhash, 61) != 0)
    {
        LOG_ERROR << "bcrypt_newhash() failed" << std::endl;
        return CreateAccountResult::InternalError;
    }
    std::string passwordHash(pwhash, 61);

    AB::Entities::Account acc;
    acc.name = name;
    if (client->Exists(acc))
        return CreateAccountResult::NameExists;
    acc.uuid = Utils::Uuid::New();
    acc.password = passwordHash;
    acc.email = email;
    acc.authToken = Utils::Uuid::New();
    acc.authTokenExpiry = sa::time::tick() + Auth::AUTH_TOKEN_EXPIRES_IN;
    acc.type = AB::Entities::AccountType::Normal;
    acc.status = AB::Entities::AccountStatus::AccountStatusActivated;
    acc.creation = sa::time::tick();
    acc.chest_size = AB::Entities::DEFAULT_CHEST_SIZE;
    if (!client->Create(acc))
    {
        LOG_ERROR << "Creating account with name " << name << " failed" << std::endl;
        return CreateAccountResult::InternalError;
    }

    // Bind account to key
    AB::Entities::AccountKeyAccounts aka;
    aka.uuid = akey.uuid;
    aka.accountUuid = acc.uuid;
    if (!client->Create(aka))
    {
        LOG_ERROR << "Creating account - account key failed" << std::endl;
        client->Delete(acc);
        return CreateAccountResult::InternalError;
    }

    // Update account key
    akey.used++;
    if (!client->Update(akey))
    {
        LOG_ERROR << "Updating account key failed" << std::endl;
        client->Delete(aka);
        client->Delete(acc);
        return CreateAccountResult::InternalError;
    }

    AB::Entities::AccountList al;
    client->Invalidate(al);

    return CreateAccountResult::OK;
}

CreateAccountResult AddAccountKey(AB::Entities::Account& account, const std::string& accKey)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    uuids::uuid accUuid(accKey);
    if (accUuid.nil())
        return CreateAccountResult::InvalidAccountKey;
    AB::Entities::AccountKey ak;
    ak.uuid = accUuid.to_string();
    if (!client->Read(ak))
        return CreateAccountResult::InvalidAccountKey;
    if (ak.used + 1 > ak.total)
        return CreateAccountResult::InvalidAccountKey;

    // Bind account to key
    AB::Entities::AccountKeyAccounts aka;
    aka.uuid = ak.uuid;
    aka.accountUuid = account.uuid;
    if (!client->Create(aka))
    {
        LOG_ERROR << "Creating account - account key failed" << std::endl;
        return CreateAccountResult::AlreadyAdded;
    }

    // Update account key
    ak.used++;
    if (!client->Update(ak))
    {
        LOG_ERROR << "Updating account key failed" << std::endl;
        return CreateAccountResult::InternalError;
    }

    switch (ak.type)
    {
    case AB::Entities::KeyTypeCharSlot:
    {
        account.charSlots++;
        if (!client->Update(account))
        {
            LOG_ERROR << "Account update failed " << account.uuid << std::endl;
            return CreateAccountResult::InternalError;
        }
        break;
    }
    case AB::Entities::KeyTypeChestSlots:
    {
        account.chest_size += AB::Entities::CHEST_SLOT_INCREASE;
        if (!client->Update(account))
        {
            LOG_ERROR << "Account update failed " << account.uuid << std::endl;
            return CreateAccountResult::InternalError;
        }
        break;
    }
    default:
        return CreateAccountResult::InvalidAccountKey;
    }

    return CreateAccountResult::OK;
}

PasswordAuthResult PasswordAuth(const std::string& pass, AB::Entities::Account& account)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(account))
    {
        LOG_ERROR << "Unable to read account UUID " << account.uuid << " name " << account.name << std::endl;
        return PasswordAuthResult::InvalidAccount;
    }
    IO::EntityLocker locker(*client, account);
    // Since we may read the account by name (not UUID) we must lock it after reading it into cache.
    if (!locker.Lock())
    {
        LOG_ERROR << "Unable to lock account" << std::endl;
        return PasswordAuthResult::InternalError;
    }
    // Using password auth invalidates previous auth token
    account.authTokenExpiry = 0;
    client->Update(account);

    if (account.status != AB::Entities::AccountStatusActivated)
    {
        LOG_ERROR << "Account not activated UUID " << account.uuid << std::endl;
        return PasswordAuthResult::InvalidAccount;
    }

    if (Auth::BanManager::IsAccountBanned(uuids::uuid(account.uuid)))
        return PasswordAuthResult::AccountBanned;

    if (bcrypt_checkpass(pass.c_str(), account.password.c_str()) != 0)
        return PasswordAuthResult::PasswordMismatch;

    if (account.onlineStatus != AB::Entities::OnlineStatusOffline)
        return PasswordAuthResult::AlreadyLoggedIn;

    account.authToken = Utils::Uuid::New();
    account.authTokenExpiry = sa::time::tick() + Auth::AUTH_TOKEN_EXPIRES_IN;
    if (!client->Update(account))
        return PasswordAuthResult::InternalError;

    return PasswordAuthResult::OK;
}

bool TokenAuth(const std::string& token, AB::Entities::Account& account)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(account))
    {
        LOG_ERROR << "Unable to read account UUID " << account.uuid << " name " << account.name << std::endl;
        return false;
    }
    if (!Utils::Uuid::IsEqual(account.authToken, token))
        return false;
    if (sa::time::is_expired(account.authTokenExpiry))
        return false;

    // Refresh token
    account.authTokenExpiry = sa::time::tick() + Auth::AUTH_TOKEN_EXPIRES_IN;
    client->Update(account);

    return true;
}

bool IsNameAvailable(const std::string& name, const std::string& forAccountUuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Character ch;
    ch.name = name;
    // Check if player with the same name exists
    if (client->Exists(ch))
        return false;

    AB::Entities::ReservedName rn;
    rn.name = name;
    rn.isReserved = true;
    if (client->Read(rn))
    {
        // Temporarily reserved for an account
        if (rn.expires != 0)
        {
            if (rn.expires < sa::time::tick())
            {
                // Expired -> Delete it
                client->Delete(rn);
                return true;
            }
            // Not expired yet
            return Utils::Uuid::IsEqual(forAccountUuid, rn.reservedForAccountUuid);
        }
        // Exists in table and does not expire, so it's not available.
        return false;
    }
    return true;
}

bool GameWorldAuth(const std::string& accountUuid, const std::string& authToken, const std::string& charUuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Account acc;
    acc.uuid = accountUuid;
    if (!TokenAuth(authToken, acc))
        // At this point the account must exist.
        return false;

    AB::Entities::Character ch;
    ch.uuid = charUuid;
    if (!client->Read(ch))
    {
        LOG_ERROR << "Error reading character " << charUuid << std::endl;
        return false;
    }
    if (!Utils::Uuid::IsEqual(ch.accountUuid, acc.uuid))
    {
        LOG_ERROR << "Character " << charUuid << " does not belong to account " << accountUuid << std::endl;
        return false;
    }

    return true;
}

bool AccountLogout(const std::string& uuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Account acc;
    acc.uuid = uuid;
    if (!client->Read(acc))
    {
        LOG_ERROR << "Error reading account " << uuid << std::endl;
        return false;
    }
    acc.onlineStatus = AB::Entities::OnlineStatus::OnlineStatusOffline;
    return client->Update(acc);
}

bool GetAccountInfo(AB::Entities::Account& account, AB::Entities::Character& character)
{
    auto* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(account))
        return false;
    if (Utils::Uuid::IsEmpty(account.currentCharacterUuid))
        return false;
    character.uuid = account.currentCharacterUuid;
    return client->Read(character);
}

bool GetGuildMemberInfo(const AB::Entities::Account& account, AB::Entities::GuildMember& g)
{
    if (Utils::Uuid::IsEmpty(account.guildUuid))
        return false;
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::GuildMembers gms;
    gms.uuid = account.guildUuid;
    if (!client->Read(gms))
        return false;
    const auto it = std::find_if(gms.members.begin(),
        gms.members.end(),
        [&](const AB::Entities::GuildMember& current)
        { return Utils::Uuid::IsEqual(account.uuid, current.accountUuid); });
    if (it == gms.members.end())
        return false;
    g = (*it);
    return true;
}

}
