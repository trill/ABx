/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Character.h"
#include "Account.h"
#include <AB/CommonConfig.h>
#include <libcommon/Subsystems.h>
#include <libcommon/DataClient.h>
#include <libcommon/UuidUtils.h>
#include <libcommon/Logger.h>
#include <AB/Entities/Account.h>
#include <AB/Entities/FriendedMe.h>
#include <AB/Entities/GuildMembers.h>
#include <AB/Entities/Character.h>
#include <AB/Entities/PlayerItemList.h>
#include <AB/Entities/Profession.h>
#include <AB/Entities/ReservedName.h>
#include <libshared/Mechanic.h>
#include <unordered_set>
#include <sa/Container.h>

namespace Account {

bool LoadCharacter(AB::Entities::Character& ch)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    if (!client->Read(ch))
    {
        LOG_ERROR << "Error reading player data" << std::endl;
        return false;
    }
    return true;
}

bool GetPlayerInfoByName(const std::string& name, AB::Entities::Character& player)
{
    auto* client = GetSubsystem<IO::DataClient>();
    player.name = name;
    return client->Read(player);
}

bool GetPlayerInfoByAccount(const std::string& accountUuid, AB::Entities::Character& player)
{
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Account acc;
    acc.uuid = accountUuid;
    if (!client->Read(acc))
        return false;
    if (!Utils::Uuid::IsEmpty(acc.currentCharacterUuid))
        player.uuid = acc.currentCharacterUuid;
    else if (!acc.characterUuids.empty())
        player.uuid = acc.characterUuids[0];
    else
        return false;
    return client->Read(player);
}

AB::Entities::FriendRelation GetRelationToMe(const std::string& meUuid, const std::string& uuid)
{
    // Get realtion of `uuid` to `meUuid`
    auto* client = GetSubsystem<IO::DataClient>();

    // Those have me in their friendlist, be it friended or ignored
    AB::Entities::FriendedMe fme;
    fme.uuid = meUuid;
    if (client->Read(fme))
    {
        for (const auto& f : fme.friends)
        {
            if (Utils::Uuid::IsEqual(uuid, f.accountUuid))
                return f.relation;
        }
    }
    return AB::Entities::FriendRelationUnknown;
}

bool IsIgnoringMe(const std::string& meUuid, const std::string& uuid)
{
    return GetRelationToMe(meUuid, uuid) == AB::Entities::FriendRelationIgnore;
}

bool HasFriendedMe(const std::string& meUuid, const std::string& uuid)
{
    return GetRelationToMe(meUuid, uuid) == AB::Entities::FriendRelationFriend;
}

size_t GetInterestedParties(const std::string& accountUuid, std::vector<std::string>& accounts)
{
    auto* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Account acc;
    acc.uuid = accountUuid;
    if (!client->Read(acc))
        return 0;

    accounts.clear();
    std::unordered_set<std::string> ignored;

    // I friended those
    AB::Entities::FriendList fl;
    fl.uuid = accountUuid;
    if (client->Read(fl))
    {
        for (const auto& f : fl.friends)
        {
            if (f.relation == AB::Entities::FriendRelationFriend)
                accounts.push_back(f.friendUuid);
            else if (f.relation == AB::Entities::FriendRelationIgnore)
                ignored.emplace(f.friendUuid);
        }
    }
    // Those friended me
    AB::Entities::FriendedMe fme;
    fme.uuid = accountUuid;
    if (client->Read(fme))
    {
        for (const auto& f : fme.friends)
        {
            if (f.relation == AB::Entities::FriendRelationFriend)
                accounts.push_back(f.accountUuid);
            else if (f.relation == AB::Entities::FriendRelationIgnore)
                ignored.emplace(f.accountUuid);
        }
    }

    auto isIgnored = [&ignored](const std::string& uuid)
    {
        const auto it = ignored.find(uuid);
        return it != ignored.end();
    };

    if (!Utils::Uuid::IsEmpty(acc.guildUuid))
    {
        // If this guy is in a guild, also inform guild members
        AB::Entities::GuildMembers gms;
        gms.uuid = acc.guildUuid;
        if (client->Read(gms))
        {
            for (const auto& gm : gms.members)
            {
                if (!Utils::Uuid::IsEqual(accountUuid, gm.accountUuid) && !isIgnored(gm.accountUuid))
                    // Don't add self and ignored
                    accounts.push_back(gm.accountUuid);
            }
        }
    }
    sa::DeleteDuplicates(accounts);
    return accounts.size();
}

CreateCharacterResult CreateCharacter(const std::string& accountUuid,
    const std::string& name,
    const std::string& profUuid,
    uint32_t modelIndex,
    AB::Entities::CharacterSex sex,
    bool isPvp,
    std::string& uuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    AB::Entities::Account acc;
    acc.uuid = accountUuid;
    if (!client->Read(acc))
        return CreateCharacterResult::InvalidAccount;
    if (acc.characterUuids.size() + 1 > acc.charSlots)
        return CreateCharacterResult::NoMoreCharSlots;

    AB::Entities::Profession pro;
    pro.uuid = profUuid;
    if (!client->Read(pro))
        return CreateCharacterResult::InvalidProfession;

    if (!IsNameAvailable(name, accountUuid))
        return CreateCharacterResult::NameExists;
    if (name.find_first_of(RESTRICTED_NAME_CHARS, 0) != std::string::npos)
        return CreateCharacterResult::InvalidName;

    AB::Entities::Character ch;
    ch.uuid = Utils::Uuid::New();
    ch.name = name;
    ch.modelIndex = modelIndex;
    ch.profession = pro.abbr;
    ch.professionUuid = pro.uuid;
    ch.sex = sex;
    ch.pvp = isPvp;
    ch.level = isPvp ? Game::LEVEL_CAP : 1;
    ch.creation = sa::time::tick();
    ch.inventorySize = AB::Entities::DEFAULT_INVENTORY_SIZE;
    ch.accountUuid = accountUuid;
    if (!client->Create(ch))
    {
        LOG_ERROR << "Create character failed" << std::endl;
        return CreateCharacterResult::InternalError;
    }
    // To reload the character list
    client->Invalidate(acc);

    // If in reserved names table we must delete it now
    AB::Entities::ReservedName rn;
    rn.name = ch.name;
    client->DeleteIfExists(rn);

    uuid = ch.uuid;
    return CreateCharacterResult::OK;
}

bool DeleteCharacter(const std::string& accountUuid, const std::string& playerUuid)
{
    IO::DataClient* client = GetSubsystem<IO::DataClient>();
    AB::Entities::Character ch;
    ch.uuid = playerUuid;
    if (!client->Read(ch))
        return false;
    if (!Utils::Uuid::IsEqual(ch.accountUuid, accountUuid))
        return false;
    AB::Entities::Account account;
    account.uuid = accountUuid;
    if (!client->Read(account))
    {
        LOG_ERROR << "Error reading account " << accountUuid << std::endl;
        return false;
    }

    bool succ = client->Delete(ch);
    if (!succ)
    {
        LOG_ERROR << "Error deleting player with UUID " << playerUuid << std::endl;
        return false;
    }

    // Everything that belongs to the Player (not the Account) should be deleted too.
    // TODO: Check if there is more to delete.
    const auto deleteItems = [&](const std::vector<std::string>& items)
    {
        for (const auto& uuid : items)
        {
            AB::Entities::ConcreteItem item;
            item.uuid = uuid;
            if (!client->Delete(item))
                LOG_WARNING << "Error deleting concrete item with UUID " << uuid << std::endl;
        }
    };

    AB::Entities::EquippedItems equip;
    equip.uuid = playerUuid;
    succ = client->Read(equip);
    if (succ)
        deleteItems(equip.itemUuids);
    else
        LOG_WARNING << "Error reading equipment for player with UUID " << playerUuid << std::endl;

    AB::Entities::InventoryItems inv;
    inv.uuid = playerUuid;
    succ = client->Read(inv);
    if (succ)
        deleteItems(inv.itemUuids);
    else
        LOG_ERROR << "Error reading inventory for player with UUID " << playerUuid << std::endl;

    if (succ)
    {
        // Reserve the character name for some time for this user
        AB::Entities::ReservedName rn;
        rn.name = ch.name;
        client->DeleteIfExists(rn);
        client->Invalidate(rn);
        rn.uuid = Utils::Uuid::New();
        rn.isReserved = true;
        rn.reservedForAccountUuid = accountUuid;
        rn.name = ch.name;
        rn.expires = sa::time::tick() + Auth::NAME_RESERVATION_EXPIRES_MS;
        client->Create(rn);
    }
    // Update character list
    client->Invalidate(ch);
    client->Invalidate(account);
    return succ;
}

}
