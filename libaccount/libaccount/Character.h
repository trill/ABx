/**
 * Copyright 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <AB/Entities/Character.h>
#include <AB/Entities/FriendList.h>
#include <string>

namespace Account {

enum class CreateCharacterResult
{
    OK = 0,
    NameExists,
    InvalidAccount,
    NoMoreCharSlots,
    InvalidProfession,
    InternalError,
    InvalidName
};

bool LoadCharacter(AB::Entities::Character& ch);
bool GetPlayerInfoByName(const std::string& name, AB::Entities::Character& player);
bool GetPlayerInfoByAccount(const std::string& accountUuid, AB::Entities::Character& player);
AB::Entities::FriendRelation GetRelationToMe(const std::string& meUuid, const std::string& uuid);
// Check if `uuid` is ignoring `meUuid`
bool IsIgnoringMe(const std::string& meUuid, const std::string& uuid);
bool HasFriendedMe(const std::string& meUuid, const std::string& uuid);
size_t GetInterestedParties(const std::string& accountUuid, std::vector<std::string>& accounts);

CreateCharacterResult CreateCharacter(const std::string& accountUuid,
    const std::string& name,
    const std::string& profUuid,
    uint32_t modelIndex,
    AB::Entities::CharacterSex sex,
    bool isPvp,
    std::string& uuid);
bool DeleteCharacter(const std::string& accountUuid, const std::string& playerUuid);

}
