/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#if defined(_MSC_VER)
#pragma once
#endif

#if defined(_MSC_VER)
// Decorated name length exceeded
#pragma warning(disable: 4503 4307)
#endif

#include "targetver.h"

#if !defined(USE_STANDALONE_ASIO)
#define USE_STANDALONE_ASIO
#endif
#include "Servers.h"

#include <stdio.h>
#include <stdint.h>

#include <memory>
#include <limits>
#include <iostream>

#include <pugixml.hpp>
#include <abcrypto.hpp>
#include <AB/CommonConfig.h>
#include <libcommon/DebugConfig.h>
#include <libcommon/Utils.h>
#include <libcommon/Profiler.h>

#define PROFILING
